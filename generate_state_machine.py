#!/usr/bin/env python3
# Copyright (c) Ryax Technologies


from ryax.runner.application.handlers.execution_connection_handlers import (
    ExecutionConnectionStateMachine,
)
from ryax.runner.application.handlers.action_deployment_handlers import (
    ActionDeploymentStateMachine,
)

for stateMachines in [
    ExecutionConnectionStateMachine,
    ActionDeploymentStateMachine,
]:
    module_name = stateMachines.__module__
    print(f"# {stateMachines.__name__}", module_name)

    # Create graph
    mermaid_graph = stateMachines.get_mermaid()
    # j = json.dumps({"code": mermaid_graph, "mermaid": {"theme": "default"}}).encode()
    # b64 = base64.b64encode(j).decode()
    # url = "https://mermaidjs.github.io/mermaid-live-editor/#/view/" + b64

    # Create transition table
    transition_table = stateMachines.get_transition_table()

    print(
        f"""
## State Machine Graph

```mermaid
{mermaid_graph}
```

## State Machine Transition Table

{transition_table}
"""
    )
#    f = open(f"docs/apidoc/ryax_core.{module_name}.rst", "r")
#    contents = f.readlines()
#    f.close()
#
#    contents.insert(
#        2,
#        f"""
# State Machine Graph
# -------------------
#
# `Finite-State Machine of {module_name} <{url}>`_
#
# State Machine Transition Table
# ------------------------------
#
# {transition_table}
#
# Documentation
# -------------
# """,
#    )
#
#    f = open(f"docs/apidoc/ryax_core.{module_name}.rst", "w")
#    contents = "".join(contents)
#    f.write(contents)
#    print(contents)
#    f.close()
