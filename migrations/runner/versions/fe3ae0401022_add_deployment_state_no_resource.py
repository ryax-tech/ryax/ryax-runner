"""add deployment state no resource

Revision ID: fe3ae0401022
Revises: 363335de0828
Create Date: 2023-10-10 16:02:53.718379

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = 'fe3ae0401022'
down_revision = '363335de0828'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    with op.get_context().autocommit_block():
        op.execute("ALTER TYPE action_deployment_state ADD VALUE 'NO_RESOURCES_AVAILABLE'")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
