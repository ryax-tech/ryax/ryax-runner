"""add indexes

Revision ID: f7f81b98c121
Revises: 105a035d5e6f
Create Date: 2023-11-14 10:45:11.315260

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = 'f7f81b98c121'
down_revision = '105a035d5e6f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_index(op.f('ix_action_run_workflow_run_id'), 'action_run', ['workflow_run_id'], unique=False)
    op.create_index(op.f('ix_execution_connection_action_deployment_id'), 'execution_connection', ['action_deployment_id'], unique=False)
    op.create_index(op.f('ix_execution_connection_parent_execution_result_id'), 'execution_connection', ['parent_execution_result_id'], unique=False)
    op.create_index(op.f('ix_execution_connection_workflow_action_id'), 'execution_connection', ['workflow_action_id'], unique=False)
    op.create_index(op.f('ix_execution_result_action_run_id'), 'execution_result', ['action_run_id'], unique=False)
    op.create_index(op.f('ix_execution_result_end_of_log_delimiter'), 'execution_result', ['end_of_log_delimiter'], unique=False)
    op.create_index(op.f('ix_execution_result_association_children'), 'execution_result_association', ['children'], unique=False)
    op.create_index(op.f('ix_execution_result_association_parents'), 'execution_result_association', ['parents'], unique=False)
    op.create_index(op.f('ix_workflow_action_action_definition_id'), 'workflow_action', ['action_definition_id'], unique=False)
    op.create_index(op.f('ix_workflow_action_workflow_deployment_id'), 'workflow_action', ['workflow_deployment_id'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_workflow_action_workflow_deployment_id'), table_name='workflow_action')
    op.drop_index(op.f('ix_workflow_action_action_definition_id'), table_name='workflow_action')
    op.drop_index(op.f('ix_execution_result_association_parents'), table_name='execution_result_association')
    op.drop_index(op.f('ix_execution_result_association_children'), table_name='execution_result_association')
    op.drop_index(op.f('ix_execution_result_end_of_log_delimiter'), table_name='execution_result')
    op.drop_index(op.f('ix_execution_result_action_run_id'), table_name='execution_result')
    op.drop_index(op.f('ix_execution_connection_workflow_action_id'), table_name='execution_connection')
    op.drop_index(op.f('ix_execution_connection_parent_execution_result_id'), table_name='execution_connection')
    op.drop_index(op.f('ix_execution_connection_action_deployment_id'), table_name='execution_connection')
    op.drop_index(op.f('ix_action_run_workflow_run_id'), table_name='action_run')
    # ### end Alembic commands ###
