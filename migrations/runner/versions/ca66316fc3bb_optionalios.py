"""OptionalIOs

Revision ID: ca66316fc3bb
Revises: 50c7a55351f7
Create Date: 2023-11-30 16:47:16.606863

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ca66316fc3bb'
down_revision = '50c7a55351f7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('action_input_types', sa.Column('optional', sa.Boolean(), nullable=False, server_default='f'))
    op.add_column('action_output_types', sa.Column('optional', sa.Boolean(), nullable=False, server_default='f'))
    op.add_column('workflow_action_dynamic_outputs', sa.Column('optional', sa.Boolean(), nullable=False, server_default='f'))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('workflow_action_dynamic_outputs', 'optional')
    op.drop_column('action_output_types', 'optional')
    op.drop_column('action_input_types', 'optional')
    # ### end Alembic commands ###
