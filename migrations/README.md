Generic single-database configuration.

# Set Up

## For Postgres (Runner)
You can get a local database working easily with docker however this database must be on the previous version, previous to your changes.

```shell
export $(grep -v '^#' default.env | xargs)
docker-compose up -d
```

## For SQlite

Reset the database with:

```shell
rm /tmp/worker.db
```

# Upgrade

To upgrade use the 2 command below. This will get a new script on `migrations/versions` folder.
The script might need some edit, the downgrade and upgrade sections for instance.

For Runner:

```shell
alembic --config ./migrations/runner/alembic.ini upgrade head
alembic --config ./migrations/runner/alembic.ini revision -m xxx --autogenerate
alembic --config ./migrations/runner/alembic.ini upgrade head
```

For Worker(old)

```shell
 alembic --config ./migrations/worker/alembic.ini upgrade head
 alembic --config ./migrations/worker/alembic.ini revision -m init --autogenerate
```

# Hard Reset (NOT TO DO ON PROD)

You might hard reset the init script if you are on a development phase, however this procedure is not
supposed to be used on prod, so  unless you know what you are doing DO NOT DO THIS. To do this first
delete the migration scripts.

WARNING: read the above first.

```shell
# WARNING: did you read the above?
rm -fr migration/runner/versions/*
```

Now you have to restart an empty database.

```shell
docker-compose down -v
docker-compose up
```

Finally use the command to create the revision.

```shell
alembic --config ./migrations/runner upgrade head
alembic  --config ./migrations/runner revision -m init --autogenerate
```

Now commit the creation of the new script and deletion of the previous.

 # Troubleshooting

Sometimes you need to import sqlalchemy_utils on the .py files on migrations/versions/...py