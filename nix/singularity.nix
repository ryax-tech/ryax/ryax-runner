{ dockerTools
, singularity
, bash
, coreutils
, callPackage
, tag ? "latest"
,
}:
let
  singularity-modified = singularity.overrideAttrs (attrs: {
    installPhase = attrs.installPhase + ''
      mv $out/libexec/singularity/bin/starter-suid $out/libexec/singularity/bin/starter-suid.orig
      ln -s /run/wrappers/bin/singularity-suid $out/libexec/singularity/bin/starter-suid
    '';
  });
  wrapperDir = "/run/wrappers/bin";
  parentWrapperDir = dirOf wrapperDir;
  securityWrapper = callPackage ./wrapper.nix {
    inherit parentWrapperDir;
  };
in
dockerTools.buildImage {
  name = "singularity";
  inherit tag;
  contents = [ singularity-modified coreutils bash ];

  runAsRoot =
    let
      setuid = true;
      setgid = false;
      permissions = "u+rx,g+x,o+x";
      owner = "root";
      group = "root";
      source = "${singularity-modified}/libexec/singularity/bin/starter-suid.orig";
      program = "singularity-suid";
    in
    ''
      ${dockerTools.shadowSetup}
      mkdir -m 1777 /tmp
      mkdir -m 1777 /var/tmp
      mkdir -m 700 /root
      groupadd -r singularity
      useradd -r -g singularity --home /data singularity
      mkdir /data
      chown singularity:singularity /data

      # Make Singularity an suid executable
      wrapperDir=${wrapperDir}
      mkdir -p $wrapperDir
      cp ${securityWrapper}/bin/security-wrapper "$wrapperDir/${program}"
      echo -n "${source}" > "$wrapperDir/${program}.real"
      chown ${owner}:${group} "$wrapperDir/${program}"
      chmod "u${if setuid then "+" else "-"}s,g${if setgid then "+" else "-"}s,${permissions}" "$wrapperDir/${program}"

      chmod 755 "${parentWrapperDir}"
      chmod a+rx "$wrapperDir"

      mkdir -m 0770 -p /var/singularity/mnt/session
      mkdir -m 0770 -p /var/singularity/mnt/final
      mkdir -m 0770 -p /var/singularity/mnt/overlay
      mkdir -m 0770 -p /var/singularity/mnt/container
      mkdir -m 0770 -p /var/singularity/mnt/source

      cat ${./localtime} > /etc/localtime
      cat > /etc/subuid <<EOF
      singularity:165536:65536
      EOF
      cat > /etc/subgid <<EOF
      singularity:165536:65536
      EOF
    '';

  config = {
    EntryPoint = [ "/bin/singularity" ];
    Workdir = "/data";
    User = "singularity";
    Env = [ "SINGULARITY_CACHEDIR=/tmp" ];
  };
}
