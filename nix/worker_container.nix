{ nix2container
, pkgs
, appDir
, depsDir
, myTool
, python
, tag ? "latest"
, user ? "ryax"
, group ? "ryax"
, uid ? 1200
, gid ? 1200
, userHome ? "/home/${user}"
}:
let
  wrapperDir = "/run/wrappers/bin";
  parentWrapperDir = dirOf wrapperDir;
  program = "singularity-suid";
  singularityConfig =
    let
      # Migration data must go into the `data` directory which is the working dir in the container.
      source = "${singularity-modified}/libexec/singularity/bin/starter-suid.orig";
      securityWrapper = pkgs.callPackage ./wrapper.nix {
        inherit parentWrapperDir;
      };
    in
    runCommand "singularity-setup" { } ''
      set -x
      mkdir -p $out/etc
      mkdir -p $out/root

      # Make Singularity an suid executable
      wrapperDir=$out${wrapperDir}
      mkdir -p $wrapperDir
      cp ${securityWrapper}/bin/security-wrapper "$wrapperDir/${program}"
      echo -n "${source}" > "$wrapperDir/${program}.real"
      ls -al "$wrapperDir/${program}"

      mkdir -m 0770 -p $out/var/singularity/mnt/session
      mkdir -m 0770 -p $out/var/singularity/mnt/final
      mkdir -m 0770 -p $out/var/singularity/mnt/overlay
      mkdir -m 0770 -p $out/var/singularity/mnt/container
      mkdir -m 0770 -p $out/var/singularity/mnt/source

      cat ${./localtime} > $out/etc/localtime
      cat > $out/etc/subuid <<EOF
      ryax:165536:65536
      EOF
      cat > $out/etc/subgid <<EOF
      ryax:165536:65536
      EOF
    '';
  singularity-modified = pkgs.singularity.override ({ enableSuid = true; starterSuidPath = "/run/wrappers/bin/singularity-suid"; });
  extraTools = nix2container.buildLayer {
    copyToRoot =
      [
        (pkgs.buildEnv {
          name = "root";
          paths = with pkgs; [ singularity-modified skopeo pkgs.gnutar shadow libuuid ];
          pathsToLink = [ "/bin" ];
        })
        singularityConfig
      ];
    perms = [
      {
        path = singularityConfig;
        regex = "/var/singularity/mnt/*";
        mode = "0770";
      }
      {
        path = singularityConfig;
        regex = parentWrapperDir;
        mode = "755";
      }
      {
        path = singularityConfig;
        regex = wrapperDir;
        mode = "755";
      }
      {
        path = singularityConfig;
        regex = "${wrapperDir}/${program}";
        mode = "4755";
        uid = 0;
        gid = 0;
        uname = "root";
        gname = "root";
      }
    ];

  };


  uidStr = builtins.toString uid;
  gidStr = builtins.toString gid;

  inherit (pkgs) runCommand cacert coreutils;
  defaultConfig = runCommand "base-config" { } ''
    mkdir -p $out/etc/ssl/certs/
    ln -s ${cacert}/etc/ssl/certs/ca-bundle.crt $out/etc/ssl/certs/ca-certificates.crt

    # Create temporary directories
    mkdir $out/tmp
    mkdir -p $out/var/tmp

    mkdir -p $out/usr/bin
    ln -s ${coreutils}/bin/env $out/usr/bin/env

    # Fix localhost DNS resolution
    cat > $out/etc/nsswitch.conf <<EOF
    hosts: files dns
    EOF

    # create the Ryax user
    mkdir -p $out/etc/pam.d
    echo "${user}:x:${uidStr}:${gidStr}:Ryax User:${userHome}:/bin/bash" > $out/etc/passwd
    echo "${user}:!x:::::::" > $out/etc/shadow
    echo "${group}:x:${gidStr}:" > $out/etc/group
    echo "${group}:x::" > $out/etc/gshadow
    cat > $out/etc/pam.d/other <<EOF
    account sufficient pam_unix.so
    auth sufficient pam_rootok.so
    password requisite pam_unix.so nullok sha512
    session required pam_unix.so
    EOF
    touch $out/etc/login.defs
    mkdir -p $out/${userHome}

    # create the Root user
    echo "root:x:0:0:Ryax User:/root:/bin/bash" >> $out/etc/passwd
    echo "root:!x:::::::" >> $out/etc/shadow
    echo "root:x:0:" >> $out/etc/group
    echo "root:x::" >> $out/etc/gshadow
    mkdir -p $out/root
  '';
  base = nix2container.buildLayer {
    perms = [
      {
        path = defaultConfig;
        regex = "/tmp";
        mode = "1777";
      }
      {
        path = defaultConfig;
        regex = "/var/tmp";
        mode = "1777";
      }
      {
        path = defaultConfig;
        regex = userHome;
        mode = "1777";
        uid = uid;
        gid = gid;
        uname = user;
        gname = group;
      }
    ];

    copyToRoot =
      [
        (pkgs.buildEnv {
          name = "root";
          paths = with pkgs; [ coreutils python bashInteractive findutils procps gnutar gnugrep gdb ];
          pathsToLink = [ "/bin" ];
        })
        defaultConfig
      ];
  };
  dependencies = nix2container.buildLayer {
    copyToRoot = runCommand "stack" { } ''
      set -x
      mkdir -p $out/data
      echo Install python environment created by pip prior to this build
      if [ -d ${depsDir}/.env ]; then
        cp -vr ${depsDir}/.env $out/data/.env
      fi
    '';
    reproducible = false;
  };
  app = nix2container.buildLayer {
    copyToRoot = runCommand "app" { } ''
      echo Install the app
      mkdir -p $out/data
      cp -vr ${appDir}/ryax $out/data/ryax

      mkdir -p $out/bin
      cp -v ${appDir}/${myTool} $out/bin/${myTool}

      if [ -f ${appDir}/${myTool}-update-db ]
      then
        echo Database migrations detected, install it
        cp -v ${appDir}/${myTool}-update-db $out/bin/${myTool}-update-db
        mkdir -p $out/data/migrations
        cp -vr ${appDir}/migrations/worker $out/data/migrations
      fi
    '';
    reproducible = false;
  };
in
nix2container.buildImage {
  name = myTool;
  inherit tag;

  layers = [ base extraTools dependencies app ];

  config.User = user;
  config.EntryPoint = [ myTool ];
  config.Env = [
    "PYTHONPATH=/data/.env:/data"
    "LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath [pkgs.stdenv.cc.cc.lib]}"
  ];
  config.WorkingDir = "/data";
  config.Labels = {
    "ryax.tech" = myTool;
  };
}
