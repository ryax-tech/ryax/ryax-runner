import asyncio

from logging import getLogger

from dependency_injector.wiring import Provide

from ryax.addons.addon_entities import RyaxAddOns
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupCommand,
)
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.deployment.deployment_entities import (
    Deployment,
    Resources,
)
from ryax.worker.domain.deployment.deployment_events import (
    DeploymentNewLogLinesEvent,
    DeploymentUpdatedEvent,
    ApplyDeploymentCommand,
    UndeployCommand,
    DeploymentGetCurrentStateCommand,
    CreateDeploymentCommand,
    RecommendationsVPAUpdateCommand,
)
from ryax.worker.domain.deployment.deployment_exceptions import (
    DeploymentNotFoundError,
    DeploymentError,
    RecommendationsVPAUpdateFailureError,
)
from ryax.worker.infrastructure.deployment.k8s.k8s_recommendation_service import (
    RecommendationService,
)
from ryax.worker.domain.deployment.deployment_service import IDeploymentService
from ryax.worker.domain.deployment.deployment_values import DeploymentState
from ryax.common.domain.internal_messaging.event_publisher import IPublisherService
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.internal_messaging.querier import IQuerierService
from ryax.worker.domain.unit_of_work import IUnitOfWork

logger = getLogger(__name__)


async def on_new_execution_log_line(
    event: DeploymentNewLogLinesEvent,
    publisher: IPublisherService = Provide[ApplicationContainer.messaging_publisher],
) -> None:
    """Publish new log line for this deployment"""
    await publisher.publish([event])


async def on_deployment_updated(
    event: DeploymentUpdatedEvent,
    publisher: IPublisherService = Provide[ApplicationContainer.messaging_publisher],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    """Publish new log line for this deployment"""
    if event.state.is_stopped():
        with uow:
            deployment = uow.deployment_repository.get(event.deployment_id)
        deploy_service = _get_deploy_service(deployment)
        await deploy_service.unwatch(deployment)

    await publisher.publish([event])


def _get_deploy_service(
    deployment: Deployment,
    # WARNING: Inject the container and not the factory aggregate, or it fails to resolve dependencies
    container: ApplicationContainer = Provide[ApplicationContainer],
) -> IDeploymentService:
    logger.debug(f"Getting deployment_type {deployment.deployment_type}")
    deploy_service = container.action_deployment_factory(deployment.deployment_type)
    return deploy_service


async def on_startup(
    _: ApplicationStartupCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        deployments = uow.deployment_repository.list()
        for deployment in deployments:
            deploy_service = _get_deploy_service(deployment)
            await deploy_service.watch(deployment)


async def on_create(
    command: CreateDeploymentCommand,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    deployment = command.deployment
    with uow:
        uow.deployment_repository.add(deployment)
        uow.commit()
    await message_bus.handle_command(
        ApplyDeploymentCommand(deployment_id=deployment.id)
    )


async def on_deploy(
    command: ApplyDeploymentCommand,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        deployment = uow.deployment_repository.get(deployment_id=command.deployment_id)
        deploy_service = _get_deploy_service(deployment)
        logger.debug(f"Deploying using type {deployment.deployment_type}")
        try:
            logger.debug("Calling hook_action_before_deploy_on_worker for addons")
            try:
                await addons.run_hook_action_before_deploy_on_worker(
                    deployment, deploy_service
                )
            except Exception as err:
                logger.exception(f"Addon failed with exception {err}")

            await deploy_service.deploy(deployment)

            logger.debug("Calling hook_action_after_deploy_on_worker for addons")

            try:
                await addons.run_hook_action_after_deploy_on_worker(
                    deployment, deploy_service
                )
            except Exception as err:
                logger.exception(f"Addon failed with exception {err}")

            await deploy_service.watch(deployment)
            uow.commit()
        except Exception as err:
            await message_bus.handle_event(
                DeploymentUpdatedEvent(
                    command.deployment_id,
                    state=DeploymentState.ERROR,
                    logs=f"Unexpected error while deploying Action: {err}",
                )
            )
            raise


async def on_undeploy(
    command: UndeployCommand,
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        deployment = uow.deployment_repository.get(command.deployment_id)
        deploy_service = _get_deploy_service(deployment)
        logger.debug("Calling hook_action_deployment_delete_on_worker for addons")
        try:
            await addons.run_hook_action_deployment_delete_on_worker(
                deployment, deploy_service
            )
        except Exception as err:
            logger.exception(f"Addon failed with exception {err}")
        await deploy_service.unwatch(deployment)
        await deploy_service.delete(deployment)
        uow.deployment_repository.delete(deployment.id)
        uow.commit()


async def on_get_current_state(
    command: DeploymentGetCurrentStateCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> DeploymentState:
    with uow:
        try:
            deployment = uow.deployment_repository.get(command.deployment_id)
        except DeploymentNotFoundError:
            return DeploymentState.ERROR
        deploy_service = _get_deploy_service(deployment)
        try:
            return await deploy_service.get_current_state(deployment)
        except DeploymentError:
            return DeploymentState.ERROR


async def on_recommendations_vpa_update(
    command: RecommendationsVPAUpdateCommand,
    messaging_querier: IQuerierService = Provide[ApplicationContainer.message_querier],
    recommendation_service: RecommendationService = Provide[
        ApplicationContainer.recommendation_service
    ],
) -> None:
    # Update the VPA recommendation in worker memory (all the fields are valid under OOM and regular update)
    for recommendation in command.recommendations:
        recommendation_service.update_recommendation(
            image=recommendation.container_image,
            new_recommended_resources=Resources(
                cpu=recommendation.cpu if recommendation.cpu > 0.001 else None,
                memory=recommendation.memory if recommendation.memory > 1000 else None,
            ),
            timestamp=recommendation.timestamp,
        )

    # Send the recommendations to the runner
    try:
        error_message = await messaging_querier.query(command)
        if error_message:
            logger.info(
                "Sending VPA recommendations to runner failed! %s", error_message
            )
    except asyncio.exceptions.TimeoutError:
        raise RecommendationsVPAUpdateFailureError(
            "The Worker failed to update VPA recommendation to runner, check that the Runner is running."
        )
