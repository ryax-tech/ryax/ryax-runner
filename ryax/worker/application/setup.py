# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupCommand,
    ApplicationStartupEvent,
    StopApplicationCommand,
)
from ryax.worker.container import ApplicationContainer
from ryax.worker.application import (
    deployment_handlers,
    execution_handlers,
    registration_handler,
)
from ryax.worker.domain.deployment.deployment_events import (
    DeploymentNewLogLinesEvent,
    DeploymentUpdatedEvent,
    UndeployCommand,
    ApplyDeploymentCommand,
    DeploymentGetCurrentStateCommand,
    CreateDeploymentCommand,
    RecommendationsVPAUpdateCommand,
)
from ryax.worker.domain.execution.execution_events import (
    StartExecutionCommand,
    InitTaskCommand,
    ExecutionEndedEvent,
    TaskCompletedEvent,
    StopExecutionCommand,
    CreateExecutionCommand,
    TaskNewLogLinesEvent,
    GetExecutionStateCommand,
)
from ryax.worker.domain.registration.registration_events import (
    RegisterCommand,
    UpdateRegistrationEvent,
)

logger = logging.getLogger(__name__)


def setup(container: ApplicationContainer) -> None:
    """Method to setup addons and messaging mapper (event type/messages mapping)"""

    container.wire(
        modules=[deployment_handlers, execution_handlers, registration_handler]
    )
    message_bus = container.message_bus()
    # Registration
    message_bus.register_command_handler(
        ApplicationStartupCommand, registration_handler.on_startup
    )
    message_bus.register_command_handler(
        RegisterCommand, registration_handler.on_register
    )
    message_bus.register_event_handler(
        UpdateRegistrationEvent, registration_handler.on_gossip
    )

    # Deployment
    # command handlers
    message_bus.register_command_handler(
        UndeployCommand, deployment_handlers.on_undeploy
    )
    message_bus.register_command_handler(
        CreateDeploymentCommand, deployment_handlers.on_create
    )
    message_bus.register_command_handler(
        ApplyDeploymentCommand, deployment_handlers.on_deploy
    )
    message_bus.register_command_handler(
        DeploymentGetCurrentStateCommand, deployment_handlers.on_get_current_state
    )
    message_bus.register_command_handler(
        RecommendationsVPAUpdateCommand,
        deployment_handlers.on_recommendations_vpa_update,
    )

    # events handlers
    message_bus.register_event_handler(
        ApplicationStartupEvent, deployment_handlers.on_startup
    )
    message_bus.register_event_handler(
        DeploymentNewLogLinesEvent, deployment_handlers.on_new_execution_log_line
    )
    message_bus.register_event_handler(
        DeploymentUpdatedEvent, deployment_handlers.on_deployment_updated
    )

    # Execution
    message_bus.register_command_handler(
        CreateExecutionCommand, execution_handlers.on_create_execution
    )
    message_bus.register_command_handler(
        StartExecutionCommand, execution_handlers.on_start_execution
    )
    message_bus.register_command_handler(
        GetExecutionStateCommand, execution_handlers.on_get_execution_state
    )
    message_bus.register_command_handler(
        StopExecutionCommand, execution_handlers.on_stop_execution
    )
    message_bus.register_command_handler(
        InitTaskCommand, execution_handlers.on_init_task
    )
    message_bus.register_event_handler(
        TaskNewLogLinesEvent, execution_handlers.on_task_new_log_line
    )
    message_bus.register_command_handler(
        StopApplicationCommand, execution_handlers.on_application_stop
    )

    message_bus.register_event_handler(
        ApplicationStartupEvent, execution_handlers.on_startup
    )
    message_bus.register_event_handler(
        TaskCompletedEvent, execution_handlers.on_task_completed_event
    )
    message_bus.register_event_handler(
        ExecutionEndedEvent, execution_handlers.on_execution_ended_event
    )

    # Initialize addons available on the worker side as well

    from ryax.addons import ryax_http_service

    ryax_http_service.HttpServiceAddOn(container.addons())

    from ryax.addons import ryax_http_api

    ryax_http_api.HttpAPIAddOn(container.addons())

    from ryax.addons import ryax_hpc

    ryax_hpc.HpcServiceAddOn(container.addons())

    from ryax.addons import ryax_kubernetes

    ryax_kubernetes.KubernetesAddOn(container.addons())
