import logging

import yaml

from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
    ObjectiveScores,
    GPU_MODE_FULL,
)
from ryax.worker.domain.exceptions import WorkerConfigurationError
from ryax.worker.domain.registration.registration_events import RegisterCommand
from ryax.worker.infrastructure.deployment.k8s.k8s_recommendation_service import (
    RecommendationService,
)

logger = logging.getLogger(__name__)


def memory_string_to_bytes(memory_string: str) -> int:
    # Define conversion factors
    conversion_factors = {"K": 1024, "M": 1024**2, "G": 1024**3, "T": 1024**4}

    # Remove spaces and extract the numeric part and unit
    memory_string = memory_string.strip()

    if not memory_string:
        raise ValueError("Memory MUST be s string is empty.")

    # Find the unit (last character) and the number (all but the last character)
    unit = memory_string[-1]
    number = memory_string[:-1]

    # Check if the unit is valid
    if unit not in conversion_factors:
        raise ValueError(
            f"Unknown memory unit: {unit}'. Accepted units are {', '.join(conversion_factors)}"
        )

    try:
        # Convert the number to float and then to bytes
        bytes_value = int(float(number) * conversion_factors[unit])
        return bytes_value
    except ValueError:
        raise ValueError("Invalid numeric value in input string.")


def time_string_to_seconds(time_string: str) -> int:
    # Define conversion factors
    conversion_factors = {
        "s": 1,  # seconds
        "m": 60,  # minutes to seconds
        "h": 3600,  # hours to seconds
        "d": 86400,  # days to seconds
        "w": 604800,  # weeks to seconds
    }

    # Remove spaces and extract the numeric part and unit
    time_string = time_string.strip()

    if not time_string:
        raise ValueError("Input string is empty.")

    # Find the unit (last character) and the number (all but the last character)
    unit = time_string[-1].lower()
    number = int(time_string[:-1])

    # Check if the unit is valid
    if unit not in conversion_factors:
        raise ValueError(
            f"Unknown time unit: '{unit}'. Accepted units are {', '.join(conversion_factors)}"
        )

    try:
        # Convert the number to float and then to seconds
        return int(number * conversion_factors[unit])
    except ValueError:
        raise ValueError("Invalid numeric value in input string.")


def validata_hpc_spec(spec: dict) -> dict:
    """
    Validate HPC configuration spec.
    TODO: Use a proper schema validation method
    """
    if "partitions" not in spec:
        raise WorkerConfigurationError(
            "Site configuration of HPC type must contain a 'partitions' entry"
        )
    node_pools_raw = spec["partitions"]
    example_conf = [
        {
            "name": "large",
            "cpu": 8,
            "memory": "8G",
            "gpu": 1,
            "time": "1h",
            "objectiveScores": {
                "energy": 20,
                "cost": 51,
                "performance": 83,
            },
        },
        {
            "name": "small",
            "cpu": 2,
            "memory": "4096M",
        },
    ]

    if len(node_pools_raw) == 0:
        raise WorkerConfigurationError(
            f"You must declare at least one partition! Example: {yaml.dump(example_conf)}"
        )
    if any(
        [
            "cpu" not in pool or "name" not in pool or "memory" not in pool
            for pool in node_pools_raw
        ]
    ):
        raise WorkerConfigurationError(
            f'Missing one or more "cpu", "memory", or "name" entry in a node pool. Example: {yaml.dump(example_conf)}'
        )
    return node_pools_raw


def validata_kubernetes_spec(spec: dict) -> dict:
    if "nodePools" not in spec:
        raise WorkerConfigurationError(
            "Site configuration spec of KUBERNETES type must contain a 'nodePools' entry. Found spec: %s",
            spec,
        )

    example_conf = [
        {
            "name": "large",
            "cpu": 8,
            "memory": "8G",
            "gpu": 1,
            "time": "1h",
            "selector": {"my.provider.com/pool-name": "default"},
            "objectiveScores": {
                "energy": 20,
                "cost": 51,
                "performance": 83,
            },
        },
        {
            "name": "small",
            "cpu": 2,
            "memory": "4096M",
            "selector": {"my.provider.com/pool-name": "small"},
        },
    ]
    node_pools_raw = spec["nodePools"]
    if len(node_pools_raw) == 0:
        raise WorkerConfigurationError(
            f"You must declare at least one node pool! Example: {yaml.dump(example_conf)}"
        )
    if any(
        [
            "cpu" not in pool
            or "name" not in pool
            or "memory" not in pool
            or ("selector" not in pool)
            for pool in node_pools_raw
        ]
    ):
        raise WorkerConfigurationError(
            f'Missing one or more "cpu", "memory", or "selector" entry in a node pool. Example: {yaml.dump(example_conf)}'
        )
    return node_pools_raw


async def validate_config(
    site_config: dict,
    site_id: str | None,
    recommendation_service: RecommendationService,
) -> RegisterCommand:
    # TODO use a proper tool for format validation
    site_name = site_config["name"]
    site_type_raw = site_config.get("type")
    if site_type_raw is None:
        site_type = SiteType.KUBERNETES
        logger.warning("Not site type provided, assuming KUBERNETES site type")
    else:
        if site_type_raw.upper() == "SLURM_SSH":
            site_type = SiteType.SLURM_SSH
        elif site_type_raw.upper() == "KUBERNETES":
            site_type = SiteType.KUBERNETES
        else:
            raise WorkerConfigurationError(
                f"Site type '{site_type_raw}' is not supported. Valid site types: HPC, KUBERNETES"
            )
    if site_type == SiteType.SLURM_SSH:
        node_pools_raw = validata_hpc_spec(site_config["spec"])
    elif site_type == SiteType.KUBERNETES:
        node_pools_raw = validata_kubernetes_spec(site_config["spec"])
    else:
        raise WorkerConfigurationError(
            f"Not supported site type: {site_config['spec']}"
        )

    node_pools = [
        RegisterCommand.RegistrationNodePool(
            name=pool["name"],
            cpu=float(pool["cpu"]),
            memory=int(
                memory_string_to_bytes(pool["memory"])
                if isinstance(pool["memory"], str)
                else pool["memory"]
            ),
            selector=pool.get("selector") or {},
            instance_type=pool.get("instanceType"),
            gpu=int(pool.get("gpu", 0)),
            maximum_time_in_sec=(
                time_string_to_seconds(pool["time"])
                if pool.get("time") and isinstance(pool["time"], str)
                else None
            ),
            architecture=NodeArchitecture[pool.get("arch", "X86_64").upper()],
            objective_scores=(
                ObjectiveScores(
                    id=ObjectiveScores.new_id(),
                    performance=pool["objectiveScores"].get("performance"),
                    energy=pool["objectiveScores"].get("energy"),
                    cost=pool["objectiveScores"].get("cost"),
                )
                if pool.get("objectiveScores")
                else None
            ),
            gpu_mode=pool.get("gpu_mode", GPU_MODE_FULL),
            gpu_num_total=await recommendation_service.get_gpu_num_by_nodepool(
                pool.get("selector") or {}
            )
            if site_type == SiteType.KUBERNETES
            else pool.get("gpu", 0),
            filter_no_gpu_action=pool.get("filter_no_gpu_action", True),
        )
        for pool in node_pools_raw
    ]
    return RegisterCommand(
        id=site_id,
        name=site_name,
        type=site_type,
        node_pools=node_pools,
    )
