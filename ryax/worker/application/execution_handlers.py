import asyncio
from logging import getLogger
from typing import Tuple

from dependency_injector.wiring import Provide

from ryax.addons.addon_entities import RyaxAddOns
from ryax.common.application.utils import retry_with_backoff
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupEvent,
    StopApplicationCommand,
    BaseMessage,
)
from ryax.common.domain.internal_messaging.event_publisher import (
    IPublisherService,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.internal_messaging.querier import IQuerierService
from ryax.worker.domain.deployment.deployment_exceptions import DeploymentNotFoundError
from ryax.worker.domain.execution.execution_exceptions import (
    ExecutionNotFoundError,
    TaskInitializationError,
)
from ryax.worker.domain.execution.execution_values import ExecutionState
from ryax.worker.infrastructure.deployment.k8s.k8s_recommendation_service import (
    RecommendationService,
)
from ryax.worker.domain.execution.execution_entities import Execution
from ryax.worker.domain.execution.execution_service import (
    IExecutionService,
)
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.execution.execution_events import (
    StartExecutionCommand,
    InitTaskCommand,
    TaskCompletedEvent,
    ExecutionEndedEvent,
    StopExecutionCommand,
    CreateExecutionCommand,
    TaskNewLogLinesEvent,
    GetExecutionStateCommand,
)
from ryax.worker.domain.unit_of_work import IUnitOfWork

logger = getLogger(__name__)


async def on_startup(
    _: ApplicationStartupEvent,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        executions = uow.execution_repository.list_running()
    await message_bus.handle(
        [StartExecutionCommand(execution.id) for execution in executions]
    )


async def on_create_execution(
    command: CreateExecutionCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    with uow:
        execution = command.execution
        uow.execution_repository.add(execution)
        uow.commit()
    await message_bus.handle_command(
        StartExecutionCommand(execution.id), blocking=False
    )


async def on_start_execution(
    command: StartExecutionCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    container: ApplicationContainer = Provide[ApplicationContainer],
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
) -> None:
    with uow:
        execution = uow.execution_repository.get(command.execution_id)
        deployment = uow.deployment_repository.get_eager(execution.deployment_id)
        service: IExecutionService = container.execution_service_factory(
            deployment.execution_type.value,
        )
        try:
            await addons.run_hook_execution_on_start_on_worker(deployment)
        except Exception as err:
            logger.exception(f"Addon failed with exception {err}")
        await service.start(execution, deployment)
        execution.set_running()
        uow.commit()


@retry_with_backoff(retries=5, backoff_in_ms=2000)
async def on_init_task(
    command: InitTaskCommand,
    querier: IQuerierService = Provide[ApplicationContainer.message_querier],
) -> Tuple[str, str]:
    try:
        return await querier.query(command, timeout=30)
    except asyncio.exceptions.TimeoutError:
        raise TaskInitializationError(
            f"Unable to initialize task for execution {command.execution_id}: connection with the Runner timed out for 5 times",
        )


async def on_task_completed_event(
    event: TaskCompletedEvent,
    publisher: IPublisherService = Provide[ApplicationContainer.messaging_publisher],
) -> None:
    await publisher.publish([event])


async def on_execution_ended_event(
    event: ExecutionEndedEvent,
    publisher: IPublisherService = Provide[ApplicationContainer.messaging_publisher],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    recommendation_service: RecommendationService = Provide[
        ApplicationContainer.recommendation_service
    ],
    resource_recommendation_enabled: bool = Provide[
        ApplicationContainer.configuration.resource_recommendation_enabled
    ],
) -> None:
    with uow:
        execution: Execution | None = None
        try:
            execution = uow.execution_repository.get(event.execution_id)
        except ExecutionNotFoundError:
            logger.warning(
                "Ending a non-exiting execution: %s. Continuing...", event.execution_id
            )
        else:
            try:
                deployment = uow.deployment_repository.get(execution.deployment_id)
            except DeploymentNotFoundError:
                logger.warning(
                    "Unable to apply recommendation because the deployment does not exists: %s",
                    execution.deployment_id,
                )
            else:
                if resource_recommendation_enabled:
                    try:
                        recommended_resources = (
                            await recommendation_service.apply_recommendation(execution)
                        )
                        if recommended_resources is not None:
                            logger.info(
                                f"FOUND RECOMMENDATION at the end of execution for {deployment.id}"
                            )
                            if recommended_resources.cpu is not None:
                                logger.info(
                                    f"SETTING RECOMMENDED VALUE FOR CPU = {recommended_resources.cpu}"
                                )
                                deployment.resources.cpu = recommended_resources.cpu
                            if recommended_resources.memory is not None:
                                logger.info(
                                    f"SETTING RECOMMENDED VALUE FOR MEMORY = {recommended_resources.memory}"
                                )
                                deployment.resources.memory = (
                                    recommended_resources.memory
                                )
                    except (TimeoutError, asyncio.exceptions.CancelledError) as err:
                        logger.error(
                            "Failed to apply recommendation with error: %s", err
                        )
        if execution:
            uow.execution_repository.delete(event.execution_id)
        await publisher.publish([event])
        uow.commit()


async def on_stop_execution(
    command: StopExecutionCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    container: ApplicationContainer = Provide[ApplicationContainer],
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    with uow:
        try:
            execution: Execution = uow.execution_repository.get(command.execution_id)
        except ExecutionNotFoundError:
            logger.info(
                "Trying to stop execution %s that does not exist. Responding with an error",
                command.execution_id,
            )
            await message_bus.handle_event(
                ExecutionEndedEvent(
                    command.execution_id,
                    status=ExecutionState.ERROR,
                    error_message="Execution not found in the Worker",
                )
            )
            return
        try:
            deployment = uow.deployment_repository.get(execution.deployment_id)
            service: IExecutionService = container.execution_service_factory(
                deployment.execution_type.value,
            )
            await service.stop(execution)
        except DeploymentNotFoundError:
            logger.error(
                "Failed to stop execution because the allocated deployment cannot be found: %s",
                execution.deployment_id,
            )
            await message_bus.handle_event(
                ExecutionEndedEvent(
                    command.execution_id,
                    status=ExecutionState.ERROR,
                    error_message="Deployment allocated to the Execution not found in the Worker",
                )
            )
        uow.commit()


async def on_application_stop(
    _: StopApplicationCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    messages: list[BaseMessage] = []
    with uow:
        executions = uow.execution_repository.list_running()
        for execution in executions:
            messages.append(StopExecutionCommand(execution.id))
    await message_bus.handle(messages)


async def on_task_new_log_line(
    event: TaskNewLogLinesEvent,
    publisher: IPublisherService = Provide[ApplicationContainer.messaging_publisher],
) -> None:
    await publisher.publish([event])


async def on_get_execution_state(
    command: GetExecutionStateCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> ExecutionState:
    with uow:
        try:
            return uow.execution_repository.get(command.execution_id).state
        except ExecutionNotFoundError:
            return ExecutionState.UNKNOWN
