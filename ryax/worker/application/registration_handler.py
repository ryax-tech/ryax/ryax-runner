import asyncio
import logging

from dependency_injector.wiring import Provide

from ryax.common.application.utils import retry_with_backoff
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupCommand,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.internal_messaging.querier import IQuerierService
from ryax.worker.application.registration_service import validate_config
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.node_pool.node_pool_entities import NodePool
from ryax.worker.domain.node_pool.node_pool_repository import NodePoolNotFoundError
from ryax.worker.domain.registration.registration_entities import Registration
from ryax.worker.domain.registration.registration_events import (
    RegisterCommand,
    UpdateRegistrationEvent,
)
from ryax.worker.domain.registration.registration_exceptions import (
    RegistrationFailureError,
)
from ryax.worker.infrastructure.deployment.k8s.k8s_recommendation_service import (
    RecommendationService,
)
from ryax.worker.domain.unit_of_work import IUnitOfWork

logger = logging.getLogger(__name__)


async def on_startup(
    event: ApplicationStartupCommand,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
    site_config: dict = Provide[ApplicationContainer.configuration.site],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    recommendation_service: RecommendationService = Provide[
        ApplicationContainer.recommendation_service
    ],
) -> None:
    site_id = None
    with uow:
        registration = uow.registration_repository.get()
        if registration:
            site_id = registration.site_id

    register_command = await validate_config(
        site_config, site_id, recommendation_service
    )

    new_site_id = await message_bus.handle_command(register_command)
    if site_id is not None and new_site_id != site_id:
        logger.warning("Ryax master site changed, new registration id: %s", new_site_id)
    logger.info(
        "Registration of the  Worker Site '%s' to Ryax successful with id: %s",
        register_command.name,
        new_site_id,
    )


async def on_gossip(
    event: UpdateRegistrationEvent,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
    site_config: dict = Provide[ApplicationContainer.configuration.site],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    recommendation_service: RecommendationService = Provide[
        ApplicationContainer.recommendation_service
    ],
) -> None:
    with uow:
        registration = uow.registration_repository.get()
    assert registration is not None

    register_command = await validate_config(
        site_config, registration.site_id, recommendation_service
    )

    new_site_id = await message_bus.handle_command(register_command)
    logger.debug(
        "Registration of the Worker Site '%s' to Ryax updated with id: %s",
        register_command.name,
        new_site_id,
    )


@retry_with_backoff(retries=5, backoff_in_ms=5000, max_backoff_in_ms=10 * 60 * 1000)
async def on_register(
    command: RegisterCommand,
    messaging_querier: IQuerierService = Provide[ApplicationContainer.message_querier],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    logger.info("Registering Worker Site '%s' to Ryax", command.name)
    with uow:
        for node_pool in command.node_pools:
            try:
                inner_node_pool = uow.node_pool_repository.get_by_name(node_pool.name)
                # If selector changes update it
                inner_node_pool.selector = node_pool.selector
            except NodePoolNotFoundError:
                uow.node_pool_repository.add(
                    NodePool(
                        id=NodePool.new_id(),
                        name=node_pool.name,
                        selector=node_pool.selector,
                    )
                )
        try:
            site_id, error_message = await messaging_querier.query(command)
        except asyncio.exceptions.TimeoutError:
            raise RegistrationFailureError(
                "The Worker registration did not succeed, check that the Runner is running."
            )
        if error_message:
            raise Exception(
                f"Worker registration request fails with error: {error_message}"
            )
        uow.registration_repository.set(Registration(site_id=site_id))
        uow.commit()
        return site_id
