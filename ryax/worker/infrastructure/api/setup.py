from aiohttp import web

from ryax.worker.container import ApplicationContainer
from ryax.worker.infrastructure.api import monitoring_controller


def setup(app: web.Application, container: ApplicationContainer) -> None:
    """Method to setup api"""
    # Configure application container for wiring
    container.wire(
        modules=[
            monitoring_controller,
        ]
    )

    # Configure api routing
    app.add_routes(
        [
            web.get("/healthz", monitoring_controller.health_check, allow_head=False),
        ]
    )
