# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import ryax.worker.infrastructure.messaging.handlers.deployment_rmq_handlers
import ryax.worker.infrastructure.messaging.handlers.execution_rmq_handlers
import ryax.worker.infrastructure.messaging.handlers.registration_rmq_handlers
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.deployment.deployment_events import (
    RecommendationsVPAUpdateCommand,
    DeploymentNewLogLinesEvent,
    DeploymentUpdatedEvent,
)
from ryax.worker.domain.execution.execution_events import (
    TaskNewLogLinesEvent,
    InitTaskCommand,
    ExecutionEndedEvent,
    TaskCompletedEvent,
)
from ryax.worker.domain.registration.registration_events import RegisterCommand
from ryax.worker.infrastructure.messaging.handlers import (
    deployment_rmq_handlers,
    execution_rmq_handlers,
    registration_rmq_handlers,
)
from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    Deploy,
    StartExecution,
    Undeploy,
    InitTaskReply,
    GetDeploymentState,
    StopExecution,
    RegisterReply,
    RecommendationsVPAReply,
    GetExecutionState,
)


def setup(
    container: ApplicationContainer,
) -> None:
    """Method to setup messaging mapper (event type/messages mapping)"""
    container.wire(
        modules=[
            deployment_rmq_handlers,
            execution_rmq_handlers,
            registration_rmq_handlers,
        ]
    )

    ## Consumer
    messaging_consumer = container.messaging_consumer()
    runner_routing_key = f"Runner.{container.configuration.site.name()}"
    # Register message controller
    messaging_consumer.register_handler(
        Deploy,
        ryax.worker.infrastructure.messaging.handlers.deployment_rmq_handlers.on_deploy,
        runner_routing_key,
    )
    messaging_consumer.register_handler(
        Undeploy,
        ryax.worker.infrastructure.messaging.handlers.deployment_rmq_handlers.on_undeploy,
        runner_routing_key,
    )
    messaging_consumer.register_handler(
        StartExecution,
        execution_rmq_handlers.on_execute,
        runner_routing_key,
    )
    messaging_consumer.register_handler(
        StopExecution,
        execution_rmq_handlers.on_stop_execution,
        runner_routing_key,
    )
    # Query response
    messaging_consumer.register_handler(
        GetDeploymentState,
        deployment_rmq_handlers.on_get_deployment_state,
        runner_routing_key,
    )
    messaging_consumer.register_handler(
        GetExecutionState,
        execution_rmq_handlers.on_get_execution_state,
        runner_routing_key,
    )

    ## Publisher
    messaging_publisher = container.messaging_publisher()
    # Register message converter

    messaging_publisher.register_handler(
        DeploymentUpdatedEvent,
        deployment_rmq_handlers.on_deployment_updated,
    )
    messaging_publisher.register_handler(
        DeploymentNewLogLinesEvent,
        deployment_rmq_handlers.on_deployment_new_log_line,
    )

    messaging_publisher.register_handler(
        TaskCompletedEvent,
        execution_rmq_handlers.on_task_completed,
    )
    messaging_publisher.register_handler(
        TaskNewLogLinesEvent,
        execution_rmq_handlers.on_task_new_log_line,
    )
    messaging_publisher.register_handler(
        ExecutionEndedEvent,
        execution_rmq_handlers.on_execution_ended,
    )

    ## Querier
    message_querier = container.message_querier()

    message_querier.register_request_handler(
        RegisterCommand,
        registration_rmq_handlers.on_register,
        "Worker",
    )
    message_querier.register_reply_handler(
        RegisterReply,
        registration_rmq_handlers.on_registered,
    )
    message_querier.register_request_handler(
        InitTaskCommand,
        execution_rmq_handlers.on_init_execution,
        "Worker",
    )
    message_querier.register_reply_handler(
        InitTaskReply,
        execution_rmq_handlers.on_init_execution_response,
    )
    message_querier.register_request_handler(
        RecommendationsVPAUpdateCommand,
        deployment_rmq_handlers.on_recommendations_vpa_update,
        "Worker",
    )
    message_querier.register_reply_handler(
        RecommendationsVPAReply,
        deployment_rmq_handlers.on_recommendations_vpa_reply,
    )
