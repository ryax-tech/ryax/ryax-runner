from typing import cast
from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide
from google.protobuf.json_format import MessageToDict

from ryax.common.domain.internal_messaging.base_messages import BaseCommand
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.infrastructure.messaging.message_handler import ProtobufMessage
from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    Deploy,
    Undeploy,
    GetDeploymentState,
    RecommendationsVPAReply,
)
from ryax.common.infrastructure.messaging.messages.worker_messages_pb2 import (
    ActionDeploymentCurrentState,
    DeploymentState,
    ActionDeploymentUpdated,
    ActionDeploymentNewLogLines,
    RecommendationsVPA,
)
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.deployment.deployment_entities import (
    Deployment,
    Resources,
    Kind,
)
from ryax.worker.domain.deployment.deployment_events import (
    UndeployCommand,
    DeploymentGetCurrentStateCommand,
    DeploymentUpdatedEvent,
    DeploymentNewLogLinesEvent,
    CreateDeploymentCommand,
    RecommendationsVPAUpdateCommand,
)
from ryax.worker.domain.deployment.deployment_values import ExecutionType
from ryax.worker.domain.unit_of_work import IUnitOfWork


async def on_deploy(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    message_content = Deploy()
    message_content.ParseFromString(message.body)

    with uow:
        node_pool = uow.node_pool_repository.get_by_name(message_content.node_pool_name)
    await message_bus.handle_command(
        CreateDeploymentCommand(
            Deployment(
                id=message_content.id,
                name=message_content.name,
                log_level=Deploy.LogLevel.Name(message_content.log_level),
                version=message_content.version,
                resources=Resources(
                    cpu=message_content.resources.cpu,
                    memory=message_content.resources.memory,
                    time=message_content.resources.time,
                    gpu=message_content.resources.gpu,
                ),
                container_image=message_content.container_image,
                execution_type=ExecutionType[message_content.execution_type],
                deployment_type=message_content.deployment_type,
                addons={
                    key: value
                    for key, value in MessageToDict(
                        message_content.parameters, preserving_proto_field_name=True
                    ).items()
                },
                kind=Kind[Deploy.Kind.Name(message_content.kind)],
                node_pool=node_pool,
            )
        )
    )


async def on_undeploy(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    message_content = Undeploy()
    message_content.ParseFromString(message.body)

    await message_bus.handle_command(
        UndeployCommand(message_content.id), blocking=False
    )


async def on_get_deployment_state(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> ProtobufMessage:
    message_content = GetDeploymentState()
    message_content.ParseFromString(message.body)
    state = await message_bus.handle_command(
        DeploymentGetCurrentStateCommand(deployment_id=message_content.deployment_id)
    )
    return ActionDeploymentCurrentState(
        deployment_state=DeploymentState.Value(state.name)
    )


def on_deployment_updated(
    event: DeploymentUpdatedEvent,
) -> ProtobufMessage:
    return ActionDeploymentUpdated(
        deployment_id=event.deployment_id,
        deployment_state=DeploymentState.Value(event.state.name),
        logs=event.logs,
    )


def on_deployment_new_log_line(
    event: DeploymentNewLogLinesEvent,
) -> ProtobufMessage:
    return ActionDeploymentNewLogLines(
        deployment_id=event.deployment_id,
        logs=event.logs,
        executor_id=event.executor_id,
    )


def on_recommendations_vpa_update(
    event: BaseCommand,
) -> ProtobufMessage:
    event = cast(RecommendationsVPAUpdateCommand, event)
    recommendations = []
    for r in event.recommendations:
        recommendations.append(
            RecommendationsVPA.Recommendation(
                # Not pass the timestamp to runner
                container_image=r.container_image,
                # Not pass cpu values to runner
                # cpu=r.cpu,
                memory=r.memory,
                gpu_mig_instance=r.gpu_mig_instance,
                is_oom_bumpup=r.is_oom_bumpup,
                is_gpuoom_bumpup=r.is_gpuoom_bumpup,
            )
        )
    return RecommendationsVPA(recommendations=recommendations)


async def on_recommendations_vpa_reply(
    message: AbstractIncomingMessage,
) -> str | None:
    message_content = RecommendationsVPAReply()
    message_content.ParseFromString(message.body)
    return message_content.error_message
