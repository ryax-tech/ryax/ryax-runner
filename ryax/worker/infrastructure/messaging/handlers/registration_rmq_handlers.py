from typing import cast, Tuple

from aio_pika.abc import AbstractIncomingMessage

from ryax.common.domain.internal_messaging.base_messages import BaseCommand
from ryax.common.infrastructure.messaging.message_handler import ProtobufMessage
from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    RegisterReply,
)
from ryax.common.infrastructure.messaging.messages.worker_messages_pb2 import Register
from ryax.worker.domain.registration.registration_events import RegisterCommand


def on_register(
    event: BaseCommand,
) -> ProtobufMessage:
    event = cast(RegisterCommand, event)
    return Register(
        site_id=event.id,
        site_name=event.name,
        node_pools=[
            Register.NodePool(
                cpu=node_pool.cpu,
                gpu=node_pool.gpu,
                memory=node_pool.memory,
                maximum_time_in_sec=node_pool.maximum_time_in_sec,
                architecture=node_pool.architecture.name,
                name=node_pool.name,
                instance_type=node_pool.instance_type,
                objective_scores=(
                    Register.ObjectiveScores(
                        energy=node_pool.objective_scores.energy,
                        performance=node_pool.objective_scores.performance,
                        cost=node_pool.objective_scores.cost,
                    )
                    if node_pool.objective_scores
                    else None
                ),
                gpu_mode=node_pool.gpu_mode,
                gpu_num_total=node_pool.gpu_num_total,
                filter_no_gpu_action=node_pool.filter_no_gpu_action,
            )
            for node_pool in event.node_pools
        ],
        type=Register.SiteType.Value(event.type.name),
    )


async def on_registered(
    message: AbstractIncomingMessage,
) -> Tuple[str, str]:
    message_content = RegisterReply()
    message_content.ParseFromString(message.body)
    return message_content.site_id, message_content.error_message
