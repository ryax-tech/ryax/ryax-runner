from typing import Tuple, cast

from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide
from google.protobuf.struct_pb2 import Struct
from google.protobuf.timestamp_pb2 import Timestamp

from ryax.common.domain.internal_messaging.base_messages import BaseCommand
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.infrastructure.messaging.message_handler import ProtobufMessage

from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    InitTaskReply,
    StartExecution,
    StopExecution,
    GetExecutionState,
)
from ryax.common.infrastructure.messaging.messages.worker_messages_pb2 import (
    InitTaskRequest,
    TaskCompleted,
    TaskNewLogLines,
    ExecutionEnded,
    GetExecutionStateReply,
)
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.execution.execution_entities import (
    Execution,
    InputDefinition,
    IOType,
    OutputDefinition,
)
from ryax.worker.domain.execution.execution_events import (
    InitTaskCommand,
    CreateExecutionCommand,
    StopExecutionCommand,
    TaskCompletedEvent,
    TaskNewLogLinesEvent,
    ExecutionEndedEvent,
    GetExecutionStateCommand,
)


# Query handlers
async def on_init_execution_response(
    message: AbstractIncomingMessage,
) -> Tuple[str, str]:
    message_content = InitTaskReply()
    message_content.ParseFromString(message.body)

    return message_content.task_id, message_content.workflow_run_id


def on_init_execution(
    command: BaseCommand,
) -> ProtobufMessage:
    command = cast(InitTaskCommand, command)
    return InitTaskRequest(
        execution_id=command.execution_id,
        executor_id=command.executor_id,
        log_delimiter=command.log_delimiter,
    )


# Receive handlers
async def on_execute(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    message_content = StartExecution()
    message_content.ParseFromString(message.body)
    await message_bus.handle_command(
        CreateExecutionCommand(
            Execution(
                id=message_content.execution_id,
                deployment_id=message_content.deployment_id,
                inputs_definitions=[
                    InputDefinition(
                        id=InputDefinition.new_id(),
                        name=input.name,
                        optional=input.optional,
                        type=IOType[StartExecution.IOType.Name(input.type)],
                    )
                    for input in message_content.inputs_definitions
                ],
                outputs_definitions=[
                    OutputDefinition(
                        id=OutputDefinition.new_id(),
                        name=output.name,
                        optional=output.optional,
                        type=IOType[StartExecution.IOType.Name(output.type)],
                    )
                    for output in message_content.outputs_definitions
                ],
                inputs={key: value for key, value in message_content.inputs.items()},
                time_allotment=message_content.time_allotment,
            )
        )
    )


async def on_stop_execution(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    message_content = StopExecution()
    message_content.ParseFromString(message.body)
    await message_bus.handle_command(
        StopExecutionCommand(execution_id=message_content.execution_id)
    )


# Send handlers
def on_task_completed(
    event: TaskCompletedEvent,
) -> ProtobufMessage:
    end_time = Timestamp()
    end_time.FromDatetime(event.end_time)
    outputs = Struct()
    outputs.update(event.outputs)
    return TaskCompleted(
        task_id=event.id,
        error_message=event.error_message,
        end_time=end_time,
        outputs=outputs,
        output_files=[
            TaskCompleted.OutputFile(
                output_name=output_file.output_name,
                file_path=output_file.file_path,
                encoding=output_file.encoding,
                mimetype=output_file.mimetype,
                size_in_bytes=output_file.size_in_bytes,
            )
            for output_file in event.output_files
        ],
        status=TaskCompleted.Status.Value(event.status.name),
        http_status_code=event.http_status_code,
    )


def on_task_new_log_line(
    event: TaskNewLogLinesEvent,
) -> ProtobufMessage:
    return TaskNewLogLines(task_id=event.task_id, logs=event.log_lines)


def on_execution_ended(event: ExecutionEndedEvent) -> ProtobufMessage:
    return ExecutionEnded(
        execution_id=event.execution_id,
        error_message=event.error_message,
        status=ExecutionEnded.ExecutionStatus.Value(event.status.name),
    )


# Query handler
async def on_get_execution_state(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> ProtobufMessage:
    message_content = GetExecutionState()
    message_content.ParseFromString(message.body)
    execution_state = await message_bus.handle_command(
        GetExecutionStateCommand(
            execution_id=message_content.execution_id,
        )
    )
    return GetExecutionStateReply(
        execution_state=GetExecutionStateReply.State.Value(execution_state.name)
    )
