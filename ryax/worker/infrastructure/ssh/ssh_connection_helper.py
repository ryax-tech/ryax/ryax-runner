# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from asyncio import sleep
from logging import getLogger
from pathlib import Path, PurePath

import asyncssh
from asyncssh import SSHClientConnectionOptions

from ryax.worker.domain.ssh.ssh_connection_helper import (
    ISshConnectionHelper,
    ISshExecutionOutput,
    SSHCredentials,
)

logger = getLogger(__name__)


class SshHelperConnectionException(Exception):
    pass


class SshHelperExceededMaxRetriesException(Exception):
    pass


class SshHelperConfigurationError(Exception):
    pass


class SshConnectionHelper(ISshConnectionHelper):
    """
    Class that implement ssh connection, so you can send/receive files and issue commands remotely.
    This class is motivated by the ssh-slurm-execution and ssh-file action wrapper. The former is
    an action that recieve/send data using ssh files.

    You can configure this method using either a private_key or a password for the given username.
    If you use the private key, the key must be in base64 format using a single line, to test if
    your key is complient you can try the commands below:
    echo "mykeybase64" | base64 -d > pkey.tmp && chmod 400 pkey.tmp
    ssh -i pkey.tmp  $RYAX_SSH_RUNNER_USER@$RYAX_SSH_RUNNER_HOST -p $RYAX_SSH_RUNNER_PORT

    @see domain/ssh/issh_connection_helper
    """

    def __init__(
        self,
        hostname: str,
        port: int,
        username: str,
        password: str | None,
        private_key: str | None,
        config: str | None,
        retries: int = 5,
        retry_interval: int = 2,
    ):
        self.hostname = hostname.strip()
        self.port = port
        self.username = username.strip()
        self.password = password.strip() if password is not None else None
        self.private_key = private_key.strip() if private_key is not None else None
        self.config = config.strip() if config is not None else None
        self._ssh: asyncssh.SSHClientConnection | None = None
        self.retries = retries
        self.retry_interval = retry_interval

    @staticmethod
    def create_ssh_connection(
        action_full_params: dict, default_credentials: SSHCredentials
    ) -> ISshConnectionHelper:
        logger.debug(
            f"Running create_ssh_connection on parameters {action_full_params}"
        )
        password = action_full_params.get("password")
        private_key = action_full_params.get("private_key")
        if password is None and private_key is None:
            # None of them was set just use defaults
            password = default_credentials.password
            private_key = default_credentials.private_key

        username = action_full_params.get("username")
        if username is None:
            username = default_credentials.username

        port = int(default_credentials.port)
        hostname = default_credentials.server

        if not password and not private_key:
            raise SshHelperConfigurationError(
                "Need to specify at least password or private key, none parameters are set. "
                "Check the configuration or addons parameters."
            )
        # SSH Action execution
        ssh_connection = SshConnectionHelper(
            hostname=hostname,
            port=port,
            username=username,
            password=password,
            private_key=private_key,
            config=default_credentials.config if default_credentials.config else None,
        )
        return ssh_connection

    async def ssh(self) -> asyncssh.SSHClientConnection:
        if self._ssh is None:
            self._ssh = await self._connect_to_ssh()
        return self._ssh

    async def _connect_to_ssh(self) -> asyncssh.SSHClientConnection:
        """
        Establish sshslurm connection, prefer a sshslurm private key but can work with
        password too. If both are provided use the private_key file path. If none are
        provided raise SshHelperConnectionException.
        """
        try:
            # preferred method is private key
            if self.private_key:
                logger.info(
                    f"Connecting {self.hostname} with user {self.username}, using private_key file"
                )
                # Do not use base64 by default
                # pkey_data = base64.b64decode(self.private_key)
                pkey = asyncssh.import_private_key(
                    self.private_key,
                    passphrase=None,
                    unsafe_skip_rsa_key_validation=None,
                )
                ssh = await asyncssh.connect(
                    self.hostname,
                    port=self.port,
                    username=self.username,
                    client_keys=[pkey],
                    config=None,
                    options=SSHClientConnectionOptions(
                        known_hosts=None,
                        host_based_auth=False,
                        agent_path=None,
                    ),
                )
            else:
                logger.info(
                    f"Connecting {self.hostname} with user {self.username}, using password"
                )
                if self.password is None or self.password == "":
                    raise SshHelperConnectionException(
                        "Neither a password or private key file were provided!"
                    )
                ssh = await asyncssh.connect(
                    self.hostname,
                    port=self.port,
                    username=self.username,
                    password=self.password,
                    config=None,
                    options=SSHClientConnectionOptions(
                        known_hosts=None,
                        host_based_auth=False,
                        agent_path=None,
                    ),
                )
        except asyncssh.public_key.KeyImportError as err:
            logger.error(f"Invalid private key {err}")
            raise err
        except asyncssh.misc.PermissionDenied as err:
            logger.error(
                f"Permission denied cannot connect with provided private key or password : {err}"
            )
            raise err
        except Exception as err:
            raise err

        return ssh

    async def remote_file_exists(self, filepath: Path) -> bool:
        ssh = await self.ssh()
        async with ssh.start_sftp_client() as sftp:
            try:
                await sftp.stat(str(filepath))
                return True
            except asyncssh.sftp.SFTPNoSuchFile:
                return False

    async def exec_ssh_cmd(self, cmd: str) -> ISshExecutionOutput:
        ssh = await self.ssh()
        retry = self.retries
        ssh_return: asyncssh.SSHCompletedProcess | None = None
        while retry > 0:
            try:
                ssh_return = await ssh.run(cmd)
                break
            except Exception as e:
                logger.error(e)
                logger.error(f"Error executing ssh command ===> {cmd}")
                retry -= 1
                if retry <= 0:
                    logger.error(
                        f"SshHelper exceeded the maximum number of retries allowed {self.retries}"
                    )
                    raise SshHelperExceededMaxRetriesException from e
                else:
                    logger.info(
                        f"Retrying {retry}/{self.retries} in {self.retry_interval}s"
                    )
                    await sleep(self.retry_interval)
        assert ssh_return is not None
        return ISshExecutionOutput(
            str(ssh_return.stdout),
            str(ssh_return.stderr),
            ssh_return.returncode if ssh_return.returncode is not None else -1,
        )

    async def put_ssh_file(self, local_file: Path, remote_file: Path) -> None:
        ssh = await self.ssh()
        async with ssh.start_sftp_client() as sftp:
            await sftp.put(PurePath(local_file), remote_file)  # type: ignore

    async def get_ssh_file(self, remote_file: Path, local_file: Path) -> None:
        ssh = await self.ssh()
        async with ssh.start_sftp_client() as sftp:
            await sftp.get(PurePath(remote_file), local_file)  # type: ignore

    async def close(self) -> None:
        ssh = await self.ssh()
        if ssh is not None:
            ssh.close()
