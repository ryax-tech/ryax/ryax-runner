from dataclasses import dataclass

from sqlalchemy import select, delete

from ryax.common.infrastructure.database.engine import Session
from ryax.worker.domain.execution.execution_entities import Execution
from ryax.worker.domain.execution.execution_exceptions import ExecutionNotFoundError
from ryax.worker.domain.execution.execution_repository import IExecutionRepository
from ryax.worker.domain.execution.execution_values import ExecutionState


@dataclass
class DatabaseExecutionRepository(IExecutionRepository):
    session: Session

    def get(self, execution_id: str) -> Execution:
        execution = self.session.get(Execution, execution_id)
        if execution is None:
            raise ExecutionNotFoundError(execution_id)
        return execution

    def add(self, execution: Execution) -> None:
        self.session.add(execution)

    def list_running(self) -> list[Execution]:
        return (
            self.session.execute(
                select(Execution).where(Execution.state == ExecutionState.RUNNING)
            )
            .scalars()
            .unique()
            .all()
        )

    def delete(self, execution_id: str) -> None:
        self.session.execute(delete(Execution).where(Execution.id == execution_id))
