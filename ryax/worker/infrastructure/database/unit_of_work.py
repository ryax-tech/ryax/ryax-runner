import logging
import traceback
from dataclasses import dataclass
from types import TracebackType
from typing import Callable, Type

from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.worker.domain.unit_of_work import IUnitOfWork
from ryax.worker.infrastructure.database.deployment_repository import (
    DatabaseDeploymentRepository,
)
from ryax.worker.infrastructure.database.execution_repository import (
    DatabaseExecutionRepository,
)
from ryax.worker.infrastructure.database.node_pool_repository import (
    DatabaseNodePoolRepository,
)
from ryax.worker.infrastructure.database.registration_repository import (
    DatabaseRegistrationRepository,
)

logger = logging.getLogger(__name__)


@dataclass
class DatabaseUnitOfWork(IUnitOfWork):
    engine: SqlalchemyDatabaseEngine
    deployment_repository_factory: Callable[..., DatabaseDeploymentRepository]
    execution_repository_factory: Callable[..., DatabaseExecutionRepository]
    registration_repository_factory: Callable[..., DatabaseRegistrationRepository]
    node_pool_repository_factory: Callable[..., DatabaseNodePoolRepository]

    def __enter__(self) -> IUnitOfWork:
        self.session = self.engine.get_session()
        self.deployment_repository = self.deployment_repository_factory(
            session=self.session
        )
        self.execution_repository = self.execution_repository_factory(
            session=self.session
        )
        self.registration_repository = self.registration_repository_factory(
            session=self.session
        )
        self.node_pool_repository = self.node_pool_repository_factory(
            session=self.session
        )
        return self

    def __exit__(
        self,
        exc_type: Type[Exception] | None = None,
        exc_val: Exception | None = None,
        exc_tb: TracebackType | None = None,
    ) -> None:
        super().__exit__(exc_type, exc_val, exc_tb)
        self.session.close()
        if exc_type is not None:
            logger.error(
                "Error during the Unit of Work context: %s",
                "".join(traceback.format_exception(exc_type, value=exc_val, tb=exc_tb)),
            )

    def commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()
