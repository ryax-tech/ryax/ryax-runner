from sqlalchemy import (
    Table,
    MetaData,
    Column,
    String,
    Boolean,
    ForeignKey,
    Integer,
    JSON,
    Enum,
    Float,
    BigInteger,
)

from ryax.worker.domain.deployment.deployment_entities import Kind
from ryax.worker.domain.deployment.deployment_values import ExecutionType
from ryax.worker.domain.execution.execution_entities import IOType
from ryax.worker.domain.execution.execution_values import ExecutionState

metadata = MetaData()


registration_table = Table(
    "registration",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("site_id", String),
)
IOTypeEnum = Enum(IOType, name="io_type")
input_definition_table = Table(
    "input_definition",
    metadata,
    Column("id", String, primary_key=True),
    Column("name", String),
    Column("execution_id", String, ForeignKey("execution.id", ondelete="CASCADE")),
    Column("type", IOTypeEnum, nullable=False),
    Column("optional", Boolean, nullable=False),
)
output_definition_table = Table(
    "output_definition",
    metadata,
    Column("id", String, primary_key=True),
    Column("name", String),
    Column("execution_id", String, ForeignKey("execution.id", ondelete="CASCADE")),
    Column("type", IOTypeEnum, nullable=False),
    Column("optional", Boolean, nullable=False),
)

ExecutionStateEnum = Enum(
    ExecutionState,
    name="execution_state",
)
execution_table = Table(
    "execution",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("deployment_id", String),
    Column("time_allotment", Integer),
    Column("inputs", JSON),
    Column("state", ExecutionStateEnum),
)

ExecutionTypeEnum = Enum(
    ExecutionType,
    name="execution_type",
)
KindEnum = Enum(Kind, name="kind")
resources_table = Table(
    "resources",
    metadata,
    Column("cpu", Float),
    Column("time", Float),
    Column("memory", BigInteger),
    Column("gpu", Integer),
    Column(
        "deployment_id",
        String,
        ForeignKey("deployment.id", ondelete="CASCADE"),
        primary_key=True,
    ),
)
node_pool_table = Table(
    "node_pool",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, unique=True, nullable=False),
    Column("selector", JSON, nullable=False),
)
deployment_table = Table(
    "deployment",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String),
    Column("version", String),
    Column("container_image", String),
    Column("kind", KindEnum),
    Column("execution_type", ExecutionTypeEnum),
    Column("deployment_type", String),
    Column("addons", JSON),
    Column("log_level", String),
    Column("internal_endpoint", String),
    Column("node_pool_id", String, ForeignKey("node_pool.id"), nullable=True),
)
