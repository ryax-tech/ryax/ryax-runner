from dataclasses import dataclass

from sqlalchemy import select, delete
from sqlalchemy.orm import joinedload

from ryax.common.infrastructure.database.engine import Session
from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.worker.domain.deployment.deployment_exceptions import DeploymentNotFoundError
from ryax.worker.domain.deployment.deployment_repository import IDeploymentRepository


@dataclass
class DatabaseDeploymentRepository(IDeploymentRepository):
    session: Session

    def get(self, deployment_id: str) -> Deployment:
        deployment = self.session.get(Deployment, deployment_id)
        if deployment is None:
            raise DeploymentNotFoundError(deployment_id)
        return deployment

    def get_eager(self, deployment_id: str) -> Deployment:
        deployment = self.session.execute(
            select(Deployment)
            .where(Deployment.id == deployment_id)
            .options(joinedload(Deployment.node_pool))
            .options(joinedload(Deployment.resources))
        ).scalar()
        if deployment is None:
            raise DeploymentNotFoundError(deployment_id)
        return deployment

    def add(self, deployment: Deployment) -> None:
        self.session.add(deployment)

    def list(self) -> list[Deployment]:
        return self.session.execute(select(Deployment)).scalars().all()

    def delete(self, deployment_id: str) -> None:
        self.session.execute(delete(Deployment).where(Deployment.id == deployment_id))
