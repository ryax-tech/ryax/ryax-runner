# Migrate to SqlAlchemy 2.x
from sqlalchemy.engine import Engine
from sqlalchemy.orm import registry, relationship

from ryax.worker.domain.deployment.deployment_entities import Deployment, Resources
from ryax.worker.domain.execution.execution_entities import (
    Execution,
    InputDefinition,
    OutputDefinition,
)
from ryax.worker.domain.node_pool.node_pool_entities import NodePool
from ryax.worker.domain.registration.registration_entities import Registration
from ryax.worker.infrastructure.database.metadata import (
    registration_table,
    execution_table,
    input_definition_table,
    output_definition_table,
    deployment_table,
    metadata,
    resources_table,
    node_pool_table,
)

mapper_reg = registry()
mapper = mapper_reg.map_imperatively


def start_mapping() -> None:
    mapper(
        Registration,
        registration_table,
    )
    mapper(InputDefinition, input_definition_table)
    mapper(OutputDefinition, output_definition_table)
    mapper(
        Execution,
        execution_table,
        properties={
            "inputs_definitions": relationship(
                InputDefinition,
                cascade="all, delete",
                lazy=False,
                passive_deletes=True,
            ),
            "outputs_definitions": relationship(
                OutputDefinition,
                cascade="all, delete",
                lazy=False,
                passive_deletes=True,
            ),
        },
    )
    mapper(Resources, resources_table)
    mapper(NodePool, node_pool_table)
    mapper(
        Deployment,
        deployment_table,
        properties={
            "resources": relationship(
                Resources, cascade="all, delete", uselist=False, passive_deletes=True
            ),
            "node_pool": relationship(NodePool, uselist=False),
        },
    )


def map_orm(engine: Engine) -> None:
    metadata.create_all(bind=engine)
    start_mapping()
