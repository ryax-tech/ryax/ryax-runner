from dataclasses import dataclass

from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from ryax.common.infrastructure.database.engine import Session
from ryax.worker.domain.node_pool.node_pool_entities import NodePool
from ryax.worker.domain.node_pool.node_pool_repository import (
    INodePoolRepository,
    NodePoolNotFoundError,
)


@dataclass
class DatabaseNodePoolRepository(INodePoolRepository):
    session: Session

    def get_by_name(self, node_pool_name: str) -> NodePool:
        try:
            node_pool = self.session.execute(
                select(NodePool).where(NodePool.name == node_pool_name)
            ).scalar_one()
        except NoResultFound:
            raise NodePoolNotFoundError(node_pool_name)
        return node_pool

    def add(self, node_pool: NodePool) -> None:
        self.session.add(node_pool)
