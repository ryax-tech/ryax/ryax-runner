from dataclasses import dataclass

from sqlalchemy import update

from ryax.common.infrastructure.database.engine import Session
from ryax.worker.domain.registration.registration_entities import (
    Registration,
    REGISTRATION_UNIQUE_ID,
)
from ryax.worker.domain.registration.registration_repository import (
    IRegistrationRepository,
)


@dataclass
class DatabaseRegistrationRepository(IRegistrationRepository):
    session: Session

    def get(self) -> Registration | None:
        return self.session.get(Registration, REGISTRATION_UNIQUE_ID)

    def set(self, registration: Registration) -> None:
        if self.session.get(Registration, REGISTRATION_UNIQUE_ID) is None:
            self.session.add(registration)
        else:
            self.session.execute(
                update(Registration)
                .where(id == REGISTRATION_UNIQUE_ID)
                .values(site_id=registration.site_id)
            )
