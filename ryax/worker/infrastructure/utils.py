# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from urllib.parse import urlsplit, urlunsplit


def remove_password_from_URI(uri: str) -> str:
    try:
        url = urlsplit(uri)
        # Remove password from the URL if present
        if url.password is None:
            return uri

        edited_url = url._replace(netloc=url.netloc.replace(url.password, "*****"))
        return urlunsplit(edited_url)
    except ValueError:
        # This is not a valid URL
        return uri
