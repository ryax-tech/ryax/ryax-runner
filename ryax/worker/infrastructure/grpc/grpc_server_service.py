from dataclasses import dataclass, field
from typing import Optional
import logging
import grpc
import asyncio
import json

from .vpa_protocol import vpa_protocol_pb2, vpa_protocol_pb2_grpc
from google.protobuf.empty_pb2 import Empty
from ryax.common.domain.internal_messaging.message_bus import IMessageBus

from ryax.worker.domain.deployment.deployment_events import (
    RecommendationsVPAUpdateCommand,
)

logger = logging.getLogger(__name__)


@dataclass
class GrpcService(vpa_protocol_pb2_grpc.VpaServicer):
    message_bus: IMessageBus

    async def NewRecommendations(
        self,
        request: vpa_protocol_pb2.NewRecommendationsRequest,
        context: grpc.aio.ServicerContext,
    ) -> Empty:
        logger.debug(
            "Received New Recommendations from VPA: %s", request.recommendations
        )

        translated_recommendations = []
        for r in request.recommendations:
            key_dict = json.loads(r.key)
            container_image = key_dict.get("image")
            if container_image is None:
                logger.error("Missing image in key: %s", r.key)
                continue
            translated_recommendations.append(
                RecommendationsVPAUpdateCommand.RecommendationVPA(
                    container_image=container_image,
                    cpu=r.cpu,
                    memory=r.memory,
                    gpu_mig_instance=r.gpu_mig_instance,
                    timestamp=r.timestamp.ToDatetime(),
                    # Decode into 2 separate fields
                    is_oom_bumpup=vpa_protocol_pb2.Recommendation.RAMOOM
                    in r.oom_killed_resources,
                    is_gpuoom_bumpup=vpa_protocol_pb2.Recommendation.GPUOOM
                    in r.oom_killed_resources,
                )
            )
        await self.message_bus.handle_command(
            RecommendationsVPAUpdateCommand(recommendations=translated_recommendations)
        )
        return Empty()


@dataclass
class GrpcServer:
    message_bus: IMessageBus
    server_port: int
    server: Optional[grpc.aio.Server] = field(init=False, default=None)
    server_task: Optional[asyncio.Task] = field(init=False, default=None)

    def start(self) -> None:
        self.server = grpc.aio.server()
        vpa_protocol_pb2_grpc.add_VpaServicer_to_server(
            GrpcService(message_bus=self.message_bus), self.server
        )

        server_ip = f"0.0.0.0:{self.server_port}"
        self.server.add_insecure_port(server_ip)

        # Create the server task
        logging.debug(f"Starting gRPC server on {server_ip}")
        self.server_task = asyncio.create_task(self._serve())
        logging.debug("gRPC server task created")

    async def _serve(self) -> None:
        assert self.server is not None  # Ensure server is initialized
        try:
            await self.server.start()
            logging.debug("gRPC server is running")
            await self.server.wait_for_termination()
        except asyncio.CancelledError:
            await self.server.stop(grace=5)

    async def stop(self) -> None:
        if self.server_task:
            self.server_task.cancel()
            try:
                await self.server_task
            except asyncio.CancelledError:
                logging.debug("gRPC server task cancelled")
