syntax = "proto3";

import "google/protobuf/any.proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/empty.proto";

package execution_v3;

// Manage Ryax Action executions
service ActionExecution {
  // Initialize action before execution. Should be called only once.
  rpc Init(InitRequest) returns (InitReply) {};
  // Run a new execution
  // Stream inputs one by one, or even chunk by chunk for inputs that may exceed 4MB
  // Returns one (or more) execution results
  // Each output is streamed one by one. Metadata is sent on the last message (last_reply_of_this_run==True)
  // that does not contain any data.
  rpc Run(stream RunRequest) returns (stream RunReply) {};

  rpc Version(google.protobuf.Empty) returns (VersionReply) {};
}

message InitRequest {
    repeated IOType output_types = 1;
}

message InitReply {
     enum Status {
        NONE = 0;
        DONE = 1;
        ERROR = 2;
    }
    Status status = 1;
    string executor_id = 2;
    string log_delimiter = 3;
}

message RunRequest {
    // If the module has no inputs, do not set anything for <input>
    google.protobuf.Any input = 2;
}

message RunReply {
    enum Status {
        NONE = 0;
        DONE = 1;
        ERROR = 2;
    }
    Status status = 1;
    google.protobuf.Any output = 2;
    google.protobuf.Timestamp start_time = 3;
    google.protobuf.Timestamp end_time = 4;
    string next_log_delimiter = 5;
    bool last_reply_of_this_run = 6;
    optional string error_message = 7;
    int32 http_status_code = 8;

}

message IOType {
    enum Types {
        NONE=0; // As "0" is the default value, we use this to detect errors.
        BYTES=1; // non-standard
        STRING=2;
        LONGSTRING=3;
        PASSWORD=4;
        INTEGER=5;
        FLOAT=6;
        BOOLEAN=7; // non-standard
        ENUM=8;
        FILE=9;
        FILE_REFERENCE=10; // non-standard
        DIRECTORY=11;
        DIRECTORY_REFERENCE=12; // non-standard
    }
    string name = 1;
    Types type = 2;
    bool optional = 3;
}

message IOValue {
    string name = 1;
    string value = 2;
}

message IOBooleanValue {
    string name = 1;
    bool value = 2;
}

message IOBytesValue {
    string name = 1;
    bytes value = 2;
}

message IOFloatValue {
    string name = 1;
    float value = 2;
}

message IOIntegerValue {
    string name = 1;
    int32 value = 2;
}

message IOFileValue {
    string name = 1;
    bytes value = 2;
    string file_name = 3;
}

message IODirectoryValue {
    string name = 1;
    bytes value = 2;
    bool is_last_chunk = 3;  // Required to know when to unzip the directory
}

message VersionReply {
    string version = 1;
}
