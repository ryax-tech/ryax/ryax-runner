# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ryax/worker/infrastructure/execution/grpcv1/ryax_execution/execution_v3.proto
# Protobuf Python Version: 4.25.0
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import any_pb2 as google_dot_protobuf_dot_any__pb2
from google.protobuf import timestamp_pb2 as google_dot_protobuf_dot_timestamp__pb2
from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\nMryax/worker/infrastructure/execution/grpcv1/ryax_execution/execution_v3.proto\x12\x0c\x65xecution_v3\x1a\x19google/protobuf/any.proto\x1a\x1fgoogle/protobuf/timestamp.proto\x1a\x1bgoogle/protobuf/empty.proto\"9\n\x0bInitRequest\x12*\n\x0coutput_types\x18\x01 \x03(\x0b\x32\x14.execution_v3.IOType\"\x90\x01\n\tInitReply\x12.\n\x06status\x18\x01 \x01(\x0e\x32\x1e.execution_v3.InitReply.Status\x12\x13\n\x0b\x65xecutor_id\x18\x02 \x01(\t\x12\x15\n\rlog_delimiter\x18\x03 \x01(\t\"\'\n\x06Status\x12\x08\n\x04NONE\x10\x00\x12\x08\n\x04\x44ONE\x10\x01\x12\t\n\x05\x45RROR\x10\x02\"1\n\nRunRequest\x12#\n\x05input\x18\x02 \x01(\x0b\x32\x14.google.protobuf.Any\"\xea\x02\n\x08RunReply\x12-\n\x06status\x18\x01 \x01(\x0e\x32\x1d.execution_v3.RunReply.Status\x12$\n\x06output\x18\x02 \x01(\x0b\x32\x14.google.protobuf.Any\x12.\n\nstart_time\x18\x03 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12,\n\x08\x65nd_time\x18\x04 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\x1a\n\x12next_log_delimiter\x18\x05 \x01(\t\x12\x1e\n\x16last_reply_of_this_run\x18\x06 \x01(\x08\x12\x1a\n\rerror_message\x18\x07 \x01(\tH\x00\x88\x01\x01\x12\x18\n\x10http_status_code\x18\x08 \x01(\x05\"\'\n\x06Status\x12\x08\n\x04NONE\x10\x00\x12\x08\n\x04\x44ONE\x10\x01\x12\t\n\x05\x45RROR\x10\x02\x42\x10\n\x0e_error_message\"\x90\x02\n\x06IOType\x12\x0c\n\x04name\x18\x01 \x01(\t\x12(\n\x04type\x18\x02 \x01(\x0e\x32\x1a.execution_v3.IOType.Types\x12\x10\n\x08optional\x18\x03 \x01(\x08\"\xbb\x01\n\x05Types\x12\x08\n\x04NONE\x10\x00\x12\t\n\x05\x42YTES\x10\x01\x12\n\n\x06STRING\x10\x02\x12\x0e\n\nLONGSTRING\x10\x03\x12\x0c\n\x08PASSWORD\x10\x04\x12\x0b\n\x07INTEGER\x10\x05\x12\t\n\x05\x46LOAT\x10\x06\x12\x0b\n\x07\x42OOLEAN\x10\x07\x12\x08\n\x04\x45NUM\x10\x08\x12\x08\n\x04\x46ILE\x10\t\x12\x12\n\x0e\x46ILE_REFERENCE\x10\n\x12\r\n\tDIRECTORY\x10\x0b\x12\x17\n\x13\x44IRECTORY_REFERENCE\x10\x0c\"&\n\x07IOValue\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t\"-\n\x0eIOBooleanValue\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\x08\"+\n\x0cIOBytesValue\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\x0c\"+\n\x0cIOFloatValue\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\x02\"-\n\x0eIOIntegerValue\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\x05\"=\n\x0bIOFileValue\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\x0c\x12\x11\n\tfile_name\x18\x03 \x01(\t\"F\n\x10IODirectoryValue\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\x0c\x12\x15\n\ris_last_chunk\x18\x03 \x01(\x08\"\x1f\n\x0cVersionReply\x12\x0f\n\x07version\x18\x01 \x01(\t2\xcf\x01\n\x0f\x41\x63tionExecution\x12<\n\x04Init\x12\x19.execution_v3.InitRequest\x1a\x17.execution_v3.InitReply\"\x00\x12=\n\x03Run\x12\x18.execution_v3.RunRequest\x1a\x16.execution_v3.RunReply\"\x00(\x01\x30\x01\x12?\n\x07Version\x12\x16.google.protobuf.Empty\x1a\x1a.execution_v3.VersionReply\"\x00\x62\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'ryax.worker.infrastructure.execution.grpcv1.ryax_execution.execution_v3_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:
  DESCRIPTOR._options = None
  _globals['_INITREQUEST']._serialized_start=184
  _globals['_INITREQUEST']._serialized_end=241
  _globals['_INITREPLY']._serialized_start=244
  _globals['_INITREPLY']._serialized_end=388
  _globals['_INITREPLY_STATUS']._serialized_start=349
  _globals['_INITREPLY_STATUS']._serialized_end=388
  _globals['_RUNREQUEST']._serialized_start=390
  _globals['_RUNREQUEST']._serialized_end=439
  _globals['_RUNREPLY']._serialized_start=442
  _globals['_RUNREPLY']._serialized_end=804
  _globals['_RUNREPLY_STATUS']._serialized_start=349
  _globals['_RUNREPLY_STATUS']._serialized_end=388
  _globals['_IOTYPE']._serialized_start=807
  _globals['_IOTYPE']._serialized_end=1079
  _globals['_IOTYPE_TYPES']._serialized_start=892
  _globals['_IOTYPE_TYPES']._serialized_end=1079
  _globals['_IOVALUE']._serialized_start=1081
  _globals['_IOVALUE']._serialized_end=1119
  _globals['_IOBOOLEANVALUE']._serialized_start=1121
  _globals['_IOBOOLEANVALUE']._serialized_end=1166
  _globals['_IOBYTESVALUE']._serialized_start=1168
  _globals['_IOBYTESVALUE']._serialized_end=1211
  _globals['_IOFLOATVALUE']._serialized_start=1213
  _globals['_IOFLOATVALUE']._serialized_end=1256
  _globals['_IOINTEGERVALUE']._serialized_start=1258
  _globals['_IOINTEGERVALUE']._serialized_end=1303
  _globals['_IOFILEVALUE']._serialized_start=1305
  _globals['_IOFILEVALUE']._serialized_end=1366
  _globals['_IODIRECTORYVALUE']._serialized_start=1368
  _globals['_IODIRECTORYVALUE']._serialized_end=1438
  _globals['_VERSIONREPLY']._serialized_start=1440
  _globals['_VERSIONREPLY']._serialized_end=1471
  _globals['_ACTIONEXECUTION']._serialized_start=1474
  _globals['_ACTIONEXECUTION']._serialized_end=1681
# @@protoc_insertion_point(module_scope)
