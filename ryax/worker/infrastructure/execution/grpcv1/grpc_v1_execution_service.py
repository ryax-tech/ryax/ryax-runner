# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import json
import mimetypes
from dataclasses import dataclass
from datetime import timezone
from logging import getLogger
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import AsyncIterator, Dict

import grpc
from opentelemetry.instrumentation.grpc import GrpcAioInstrumentorClient

from ryax.worker.domain.execution.execution_entities import IOType as ActionIOType

from ryax.worker.application.tasks_service import TasksService
from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.worker.domain.execution.execution_entities import Execution
from ryax.worker.domain.execution.execution_events import (
    InitTaskCommand,
    TaskCompletedEvent,
)
from ryax.worker.domain.execution.execution_exceptions import (
    ExecutionIODefinitionNotFoundError,
)
from ryax.worker.domain.execution.execution_service import IExecutionService
from ryax.worker.domain.execution.execution_values import TaskState
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.storage.storage_service import IStorageService
from .ryax_execution import (
    execution_v3_pb2,
)
from .ryax_execution.execution_v3_pb2 import (
    InitReply,
    InitRequest,
    IOBooleanValue,
    IOBytesValue,
    IODirectoryValue,
    IOFileValue,
    IOFloatValue,
    IOIntegerValue,
    IOType,
    IOValue,
    RunReply,
    RunRequest,
)
from .ryax_execution.execution_v3_pb2_grpc import (
    ActionExecutionStub,
)

logger = getLogger(__name__)

GrpcAioInstrumentorClient().instrument()


class UnsupportedIOType(Exception):
    pass


class ExecutionInitializationError(Exception):
    pass


TYPE_MAPPING = {
    ActionIOType.BOOLEAN: IOType.BOOLEAN,
    ActionIOType.FLOAT: IOType.FLOAT,
    ActionIOType.INTEGER: IOType.INTEGER,
    ActionIOType.BYTES: IOType.BYTES,
    ActionIOType.STRING: IOType.STRING,
    ActionIOType.DIRECTORY: IOType.DIRECTORY,
    ActionIOType.FILE: IOType.FILE,
}

RunReply_Status_MAPPING = {
    execution_v3_pb2.RunReply.DONE: TaskState.SUCCESS,
    execution_v3_pb2.RunReply.ERROR: TaskState.ERROR,
}

FILE_BUFFER_SIZE = 4 * 1024**2 - (10 * 1024)  # 4MB - 10KB


@dataclass
class GRPCV1ExecutionService(IExecutionService):
    message_bus: IMessageBus
    storage_service: IStorageService
    tasks_service: TasksService

    async def _generate_inputs(
        self, execution: Execution, workflow_run_id: str
    ) -> AsyncIterator[RunRequest]:
        # Create inputs
        for input_name, input_value in execution.inputs.items():
            try:
                request = RunRequest()
                action_input = execution.get_input_definition(input_name)
                input_type = action_input.type

                if not input_type.is_file_type():
                    if input_type in [
                        ActionIOType.STRING,
                    ]:
                        request.input.Pack(IOValue(name=input_name, value=input_value))
                    elif input_type == ActionIOType.BYTES:
                        request.input.Pack(
                            IOBytesValue(name=input_name, value=input_value)
                        )
                    elif input_type == ActionIOType.INTEGER:
                        request.input.Pack(
                            IOIntegerValue(name=input_name, value=int(input_value))
                        )
                    elif input_type == ActionIOType.FLOAT:
                        request.input.Pack(
                            IOFloatValue(name=input_name, value=float(input_value))
                        )
                    elif input_type == ActionIOType.BOOLEAN:
                        request.input.Pack(
                            IOBooleanValue(name=input_name, value=bool(input_value))
                        )
                    else:
                        raise UnsupportedIOType(
                            f"Error while processing Execution {execution.id} input {input_name}: The execution input of type {input_type} is not supported"
                        )
                    logger.debug(
                        "Action generated input of type '%s' for the input named: '%s'",
                        input_type,
                        input_name,
                    )
                    yield request
                else:
                    if input_type == ActionIOType.FILE:
                        try:
                            with await self.storage_service.read(
                                input_value
                            ) as output_file:
                                data = output_file.read(FILE_BUFFER_SIZE)
                                while len(data) > 0:
                                    request.input.Pack(
                                        IOFileValue(
                                            name=input_name,
                                            value=data,
                                            file_name=Path(input_value).name,
                                        )
                                    )
                                    yield request
                                    data = output_file.read(FILE_BUFFER_SIZE)
                        except FileNotFoundError:
                            raise Exception(
                                f"Missing input file {input_value} for {input_name}"
                            )
                    elif input_type == ActionIOType.DIRECTORY:
                        try:
                            with await self.storage_service.read(
                                input_value
                            ) as output_file:
                                data = output_file.read(FILE_BUFFER_SIZE)
                                while len(data) > 0:
                                    request.input.Pack(
                                        IODirectoryValue(
                                            name=input_name,
                                            value=data,
                                            is_last_chunk=False,
                                        )
                                    )
                                    yield request
                                    data = output_file.read(FILE_BUFFER_SIZE)
                                request.input.Pack(
                                    IODirectoryValue(
                                        name=input_name,
                                        value=data,
                                        is_last_chunk=True,
                                    )
                                )
                                yield request
                        except FileNotFoundError:
                            raise Exception(
                                f"Missing input file {input_value} for {input_name}"
                            )
            except ExecutionIODefinitionNotFoundError:
                logger.error(
                    f"Error while generating inputs : {input_name} with value {input_value}"
                )
                raise

    async def run(self, execution: Execution, deployment: Deployment) -> None:
        server_hostname = deployment.internal_endpoint

        logger.info("Starting Execution %s client on %s", execution.id, server_hostname)

        json_config = json.dumps(
            {
                "methodConfig": [
                    {
                        "name": [
                            {
                                "service": "execution_v3.ActionExecution"  # Format is <package>.<service> defined in the execution_3.proto file
                            }
                        ],
                        "waitForReady": True,
                        "retryPolicy": {
                            "maxAttempts": 5,
                            "initialBackoff": "0.5s",
                            "maxBackoff": "2s",
                            "backoffMultiplier": 2.0,
                            "retryableStatusCodes": ["UNAVAILABLE"],
                        },
                    }
                ]
            }
        )

        async with grpc.aio.insecure_channel(
            server_hostname,
            options=(
                ("grpc.service_config", json_config),
                ("grpc.keepalive_time_ms", 10000),
                ("grpc.min_reconnect_backoff_ms", 200),
                ("grpc.max_reconnect_backoff_ms", 500),
            ),
        ) as channel:
            stub = ActionExecutionStub(channel)
            init_response: InitReply = await stub.Init(
                InitRequest(
                    output_types=[
                        IOType(
                            name=output.name,
                            type=TYPE_MAPPING[output.type],
                            optional=output.optional,
                        )
                        for output in execution.get_outputs()
                    ]
                )
            )
            logger.info(
                "Init response for Execution %s received:\n%s",
                execution.id,
                init_response,
            )
            if init_response.status == InitReply.ERROR:
                raise ExecutionInitializationError(
                    f"Error while initializing the execution {execution.id}: {init_response}"
                )
            log_delimiter = init_response.log_delimiter
            executor_id = init_response.executor_id

            (
                task_id,
                workflow_run_id,
            ) = await self.message_bus.handle_command(
                InitTaskCommand(
                    execution_id=execution.id,
                    executor_id=executor_id,
                    log_delimiter=log_delimiter,
                )
            )

            outputs: Dict[str, str] = {}
            file_outputs: Dict[str, str] = {}
            new_run_response_cycle = False
            with TemporaryDirectory() as tmp_dir:
                response: RunReply
                async for response in stub.Run(
                    self._generate_inputs(execution, workflow_run_id)
                ):
                    if new_run_response_cycle is True:
                        # Create a new execution result if this is a new Run response cycle for this connection
                        (
                            task_id,
                            workflow_run_id,
                        ) = await self.message_bus.handle_command(
                            InitTaskCommand(
                                execution_id=execution.id,
                                executor_id=executor_id,
                                log_delimiter=response.next_log_delimiter,
                            )
                        )
                        new_run_response_cycle = False

                    if response.last_reply_of_this_run:
                        logger.info("Execution %s all outputs received", execution.id)

                        output_files: list[TaskCompletedEvent.OutputFile] = []
                        # Write file outputs temporary files to the storage_service
                        for output_name, tmp_file_path in file_outputs.items():
                            size = Path(tmp_file_path).stat().st_size
                            mimetype, encoding = mimetypes.guess_type(tmp_file_path)
                            file_path = self.storage_service.generate_file_path(
                                Path(tmp_file_path).name
                            )
                            with open(tmp_file_path, "rb") as tmp_file:
                                await self.storage_service.write(file_path, tmp_file)
                            Path(tmp_file_path).unlink()

                            output_files.append(
                                TaskCompletedEvent.OutputFile(
                                    output_name=output_name,
                                    file_path=file_path,
                                    size_in_bytes=size,
                                    mimetype=mimetype,
                                    encoding=encoding,
                                )
                            )
                            outputs[output_name] = file_path

                        await self.message_bus.handle_event(
                            TaskCompletedEvent(
                                id=task_id,
                                outputs=outputs,
                                output_files=output_files,
                                status=RunReply_Status_MAPPING[response.status],
                                end_time=response.end_time.ToDatetime(
                                    tzinfo=timezone.utc
                                ),
                                error_message=response.error_message,
                                http_status_code=response.http_status_code,
                            )
                        )
                        new_run_response_cycle = True
                        # Cleanup temporary outputs
                        outputs = {}
                        file_outputs = {}

                    elif response.output:
                        output_type = getattr(
                            execution_v3_pb2, response.output.TypeName().split(".")[-1]
                        )
                        output = output_type()
                        response.output.Unpack(output)

                        if output_type in [IOFileValue, IODirectoryValue]:
                            logger.debug(
                                "Execution %s, new file output for %s",
                                execution.id,
                                output.name,
                            )
                            output_path = Path(tmp_dir) / output.name
                            if output_type is IODirectoryValue:
                                file_path = str(output_path) + ".zip"
                            else:
                                output_path.mkdir(exist_ok=True)
                                file_path = output_path / output.file_name
                            with open(file_path, mode="ab") as input_file:
                                input_file.write(output.value)
                            file_outputs[output.name] = file_path
                        else:
                            logger.debug(
                                "Execution %s, new static output for %s",
                                execution.id,
                                output.name,
                            )
                            outputs[output.name] = output.value

        logger.info("Execution %s is Finished", execution.id)
