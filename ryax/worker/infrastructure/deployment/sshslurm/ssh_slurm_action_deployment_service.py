# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio.subprocess
import json
import os
import tempfile
from dataclasses import dataclass
from enum import Enum
from logging import getLogger
from pathlib import Path
from tempfile import TemporaryFile
from typing import Optional, Tuple

from ryax.worker.domain.deployment.deployment_events import DeploymentUpdatedEvent
from ryax.worker.domain.deployment.deployment_exceptions import DeploymentError
from ryax.worker.domain.deployment.deployment_values import DeploymentState
from ryax.worker.application.tasks_service import TasksService
from ryax.worker.domain.deployment.deployment_service import IDeploymentService
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.worker.domain.ssh.ssh_connection_helper import ISshConnectionHelper
from ryax.worker.infrastructure.ssh.ssh_connection_helper import (
    SshConnectionHelper,
    SSHCredentials,
)

logger = getLogger(__name__)


class UnsupportedActionKindDeploymentError(Exception):
    pass


class SshErrorCode(Enum):
    ImageNotFound = 1
    ErrorWhileRunningProcess = 2


class HpcOffloadingDisabledError(Exception):
    pass


class HpcOffloadingConfigurationError(Exception):
    pass


@dataclass
class ShellCommandException(Exception):
    code: Optional[SshErrorCode]
    logs: Optional[str]


@dataclass
class SshSlurmActionDeploymentService(IDeploymentService):
    message_bus: IMessageBus
    tasks_service: TasksService
    ryax_cache_dir: str
    credentials: SSHCredentials
    ssh_connection: ISshConnectionHelper | None = None
    build_with_fakeroot: bool = False
    # Only necessary when using internal registry with nodePort instead of ingress
    internal_registry_override: str | None = None

    def get_sif_image(self, action_deployment: Deployment) -> str:
        action_version = action_deployment.version
        action_name = action_deployment.name
        return f"{action_name}_{action_version}.sif"

    def _get_skopeo_image_url(self, action_deployment: Deployment) -> str:
        if self.internal_registry_override:
            split_img = action_deployment.container_image.split("/")
            return (
                split_img[0]
                + "//"
                + self.internal_registry_override
                + "/"
                + split_img[3]
            )
        return f"{action_deployment.container_image}"

    def get_flag_filename(self, action_deployment: Deployment) -> Path:
        return Path(self.ryax_cache_dir) / f"{action_deployment.id}.isdeploying"

    async def _get_action_sha256(self, action_deployment: Deployment) -> str:
        image_url = self._get_skopeo_image_url(action_deployment)
        image_info = json.loads(
            await self._execute_in_shell(
                cmd_name="Retrieve image info from skopeo",
                cmd=f"skopeo inspect --tls-verify=False {image_url}",
            )
        )
        action_sha256 = image_info["Digest"].split(":")[1]
        logger.debug(f"Digest for image {image_url} is {action_sha256}")
        return action_sha256

    async def _get_remote_digest_file(self, action_deployment: Deployment) -> Path:
        action_sha256 = await self._get_action_sha256(action_deployment)
        return (
            Path(self.ryax_cache_dir)
            / f"{self.get_sif_image(action_deployment)}.{action_sha256}"
        )

    async def _set_remote_digest_file(self, action_deployment: Deployment) -> None:
        remote_digest_file = await self._get_remote_digest_file(action_deployment)
        assert self.ssh_connection is not None
        logger.debug(f"Setting digest {remote_digest_file}")
        await self.ssh_connection.exec_ssh_cmd(f"touch {remote_digest_file}")

    async def _check_digest_file(self, action_deployment: Deployment) -> bool:
        remote_digest_file = await self._get_remote_digest_file(action_deployment)
        assert self.ssh_connection is not None
        if await self.ssh_connection.remote_file_exists(remote_digest_file):
            return True
        else:
            return False

    async def _get_current_state(
        self, action_deployment: Deployment
    ) -> Tuple[DeploymentState, str]:
        remote_sif = Path(self.ryax_cache_dir) / self.get_sif_image(action_deployment)
        assert self.ssh_connection is not None
        # No error found means not yet deployed
        if await self.ssh_connection.remote_file_exists(
            remote_sif
        ) and await self._check_digest_file(action_deployment):
            return DeploymentState.READY, ""
        elif await self.ssh_connection.remote_file_exists(
            self.get_flag_filename(action_deployment)
        ):
            return DeploymentState.DEPLOYING, ""
        return DeploymentState.UNDEPLOYED, ""

    async def get_current_state(self, deployment: Deployment) -> DeploymentState:
        try:
            current_state, _ = await self._get_current_state(deployment)
            return current_state
        except Exception as err:
            raise DeploymentError(
                f"Unable to get state of the deployment {deployment.id}: {err}"
            ) from err

    async def _unflag_deploying(self, action_deployment: Deployment) -> None:
        assert self.ssh_connection is not None
        await self.ssh_connection.exec_ssh_cmd(
            f"rm -f {self.get_flag_filename(action_deployment)}"
        )

    async def _flag_deploying(self, action_deployment: Deployment) -> None:
        assert self.ssh_connection is not None
        await self.ssh_connection.exec_ssh_cmd(
            f"touch {self.get_flag_filename(action_deployment)}"
        )

    async def _execute_in_shell(self, cmd_name: str, cmd: str) -> str:
        logger.info(f">>> {cmd}")
        with TemporaryFile(mode="w+", encoding="utf-8") as stdout, TemporaryFile(
            mode="w+", encoding="utf-8"
        ) as stderr:
            proc = await asyncio.create_subprocess_shell(
                cmd, shell=True, stdout=stdout, stderr=stderr
            )
            await proc.communicate()
            stdout.seek(0)
            out = stdout.read()
            stderr.seek(0)
            err = stderr.read()
            if proc.returncode == 0:
                status = "Done"
                logger.debug(
                    f"{status}: {cmd_name} (pid = {str(proc.pid)})" f"\nSTDOUT: {out}"
                )
                return out
            else:
                status = "Failed"
                logger.error(
                    f"{status}: {cmd_name} (pid = {str(proc.pid)})"
                    f"\nSTDOUT: {out}"
                    f"\nSTDERR: {err}"
                )
                logger.error(f"Error: while running process {cmd_name}: {out}\n{err}")
                raise ShellCommandException(
                    code=SshErrorCode.ErrorWhileRunningProcess,
                    logs=f"Error: while running process {cmd_name}: {out}\n{err}",
                )

    async def apply(self, action_deployment: Deployment) -> None:
        # Check prerequisites
        hpc_addons_params = action_deployment.addons.get("hpc", {})

        fakeroot_option = ""
        if self.build_with_fakeroot is True:
            if os.geteuid() == 0:
                raise HpcOffloadingConfigurationError(
                    "HPC Offloading configuration error, using fakeroot but you are already root"
                )
            fakeroot_option = "--fakeroot"
        elif os.geteuid() != 0:
            raise HpcOffloadingConfigurationError(
                "HPC Offloading support is enabled but option fakeroot is off and you do not have root access for singularity build"
            )

        # Grab docker image from local registry
        action_version = action_deployment.version
        deployment_id = action_deployment.id

        try:
            logger.debug(f"Applying deployment with hpc parameters {hpc_addons_params}")
            self.ssh_connection = SshConnectionHelper.create_ssh_connection(
                hpc_addons_params, self.credentials
            )
            assert self.ssh_connection is not None

            # Create cache dir to store images and executions
            await self.ssh_connection.exec_ssh_cmd(f"mkdir -p {self.ryax_cache_dir}")

            # file and grpc images everytime, this selection of image should be more customizable
            # form the application perspective.
            # See:
            # action_builder/action_builder/infrastructure/builder/builder_service.py:_get_container_image_name
            # action_deployment_entities.py:get_deployment_type
            # execution_connection_entities.py:get_execution_type
            image_url = self._get_skopeo_image_url(action_deployment)
            current_state = await self.get_current_state(action_deployment)
            if current_state == DeploymentState.READY:
                logger.info("Action already deployed skipping...")
            else:
                await self._flag_deploying(action_deployment)
                with tempfile.TemporaryDirectory() as temp_dir:
                    image_tar = f"{action_deployment.name}-{action_version}.tar"
                    await self._execute_in_shell(
                        f"Retrieving from the registry image: {image_url}",
                        f"skopeo copy -a --insecure-policy --src-tls-verify=False {image_url} docker-archive:{temp_dir}/{image_tar}",
                    )

                    # Singularity image name
                    image_sif_fullpath = Path(temp_dir) / self.get_sif_image(
                        action_deployment
                    )

                    # Convert docker image into singularity sif image
                    await self._execute_in_shell(
                        f"Generating singularity image {image_sif_fullpath} from {image_tar}",
                        f"singularity build {fakeroot_option} -F {image_sif_fullpath} docker-archive:{temp_dir}/{image_tar}",
                    )

                    await self.ssh_connection.put_ssh_file(
                        image_sif_fullpath,
                        Path(self.ryax_cache_dir)
                        / self.get_sif_image(action_deployment),
                    )
                    await self._set_remote_digest_file(action_deployment)

            # Send message isReady
            logger.info(f"The action deployment '{deployment_id}' was successful.")
            await self.message_bus.handle(
                [
                    DeploymentUpdatedEvent(
                        deployment_id,
                        DeploymentState.READY,
                        "Deployment is ready to receive execution",
                    )
                ]
            )
        finally:
            try:
                await self._unflag_deploying(action_deployment)
            except AssertionError:
                logger.warning(
                    "Unable to flag the action deployment '%s' as undeploying",
                    action_deployment.id,
                )

    async def do_watch_deployment(self, action_deployment: Deployment) -> None:
        while True:
            await asyncio.sleep(10)
            deployment_state, error_msg = await self._get_current_state(
                action_deployment=action_deployment
            )
            if deployment_state == DeploymentState.READY:
                logger.debug(
                    f"The action deployment '{action_deployment.id}' succeeded."
                )
            await self.message_bus.handle(
                [
                    DeploymentUpdatedEvent(
                        action_deployment.id,
                        state=deployment_state,
                        logs=error_msg,
                    )
                ]
            )

    async def watch(self, action_deployment: Deployment) -> None:
        self.tasks_service.register_task(
            asyncio.create_task(
                self.do_watch_deployment(action_deployment),
                name=action_deployment.id + "-deploy",
            )
        )

    async def unwatch(self, action_deployment: Deployment) -> None:
        try:
            task = self.tasks_service.pop_task(action_deployment.id + "-deploy")
            task.cancel()
        except Exception:
            logger.exception(
                f"Unable to find the task to cancel for stopping action deployment: {action_deployment.id}"
            )

    async def delete(self, action_deployment: Deployment) -> None:
        if self.ssh_connection is not None:
            await self.ssh_connection.close()
            del self.ssh_connection
