# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import Dict

from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.common.infrastructure.kubernetes.k8s_utils import (
    safe_resource_name,
)


@dataclass
class K8sGRPCV1ExternalAccess:
    action_deployment_id: str
    resource_name: str
    # namespace where the user actions are deployed
    user_namespace: str
    # gRPC connection port for connection from the worker
    grpc_connection_port: int

    @staticmethod
    def create_from_deployment(
        deployment: Deployment,
        user_namespace: str,
        grpc_connection_port: int,
    ) -> "K8sGRPCV1ExternalAccess":
        return K8sGRPCV1ExternalAccess(
            action_deployment_id=deployment.id,
            resource_name=safe_resource_name(
                deployment.id, deployment.name, deployment.version
            ),
            grpc_connection_port=grpc_connection_port,
            user_namespace=user_namespace,
        )

    @property
    def grpc_endpoint(self) -> str:
        return f"{self.resource_name}.{self.user_namespace}:{self.grpc_connection_port}"

    def generate(self) -> Dict:
        return {
            "apiVersion": "v1",
            "kind": "Service",
            "metadata": {
                "name": self.resource_name,
                "deployment": self.action_deployment_id,
            },
            "spec": {
                "ports": [
                    {
                        "name": "grpcv1",
                        "port": self.grpc_connection_port,
                        "protocol": "TCP",
                    },
                ],
                "selector": {"action_deployment_id": self.action_deployment_id},
            },
        }
