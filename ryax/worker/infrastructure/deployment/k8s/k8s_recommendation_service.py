import asyncio
import logging
from dataclasses import dataclass, field
from datetime import datetime

from ryax.worker.domain.deployment.deployment_entities import (
    ResourceRecommendation,
    Resources,
)
from ryax.worker.domain.deployment.deployment_values import DeploymentState
from ryax.worker.domain.execution.execution_entities import Execution
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import K8sEngine

logger = logging.getLogger(__name__)


@dataclass
class RecommendationService:
    """Keep track recommendations for the given execution"""

    k8s_engine: K8sEngine
    site_name: str
    recommendation: list[ResourceRecommendation] = field(default_factory=list)

    async def _wait_deployment_state(
        self,
        deployment_id: str,
        state: DeploymentState,
        timeout: float,
    ) -> None:
        counter = 1
        while (
            counter <= 60
            and await self.k8s_engine.get_deployment_state(deployment_id) != state
            and timeout > 0
        ):
            await asyncio.sleep(0.01 * counter)
            timeout -= 0.01 * counter
            counter += 1
        if timeout <= 0 or counter > 60:
            raise TimeoutError(
                "while waiting for the patch to apply on recommendation service"
            )

    def get_recommendation(self, target_image: str) -> Resources | None:
        # find the recommendation for the execution
        for r in self.recommendation:
            # In future we can add to key: received workflow_id, nodepool_id from runner/worker
            if r.match(image=target_image):
                return r.resources
        return None

    def update_recommendation(
        self,
        image: str,
        new_recommended_resources: Resources,
        timestamp: datetime,
    ) -> None:
        # If the image matches, update the recommendation
        for old_recommendation in self.recommendation:
            if old_recommendation.match(image):
                logger.info(
                    "Find existing image %s, updating recommendations: %s",
                    image,
                    new_recommended_resources,
                )
                if timestamp > old_recommendation.timestamp:
                    old_recommendation.resources.memory = (
                        new_recommended_resources.memory
                    )
                    old_recommendation.resources.cpu = new_recommended_resources.cpu
                return
        # If the image doesn't match, add a new recommendation
        logger.info(
            "Adding new image %s, updating recommendations: %s",
            image,
            new_recommended_resources,
        )
        self.recommendation.append(
            ResourceRecommendation(
                image=image,
                resources=new_recommended_resources,
                timestamp=timestamp,
            )
        )

    async def apply_recommendation(self, execution: Execution) -> Resources | None:
        # get engine recommendation from deployment if exists
        deployment_k8s = await self.k8s_engine.get_deployment(execution.deployment_id)
        target_image = deployment_k8s.spec.template.spec.containers[0].image

        recommended_resources = self.get_recommendation(target_image)

        if deployment_k8s.metadata.labels["kind"] == "TRIGGER":
            logger.info("Skipping recommendation because the action Kind is TRIGGER")
        elif recommended_resources is not None:
            new_recommendation = recommended_resources.generate_deployment_spec()[
                "resources"
            ]
            previous_recommendation = deployment_k8s.spec.template.spec.containers[
                0
            ].resources
            if (
                new_recommendation["limits"] == previous_recommendation.limits
                and new_recommendation["requests"] == previous_recommendation.requests
            ):
                logger.info("Skipping recommendation because already applied")
            else:
                logger.info(
                    "Applying new resource recommendation %s to %s",
                    recommended_resources.generate_deployment_spec()["resources"],
                    execution.deployment_id,
                )
                if logger.isEnabledFor(logging.INFO):
                    limits_diff = [
                        resource
                        for resource in new_recommendation["limits"]
                        if previous_recommendation.limits
                        and new_recommendation["limits"][resource]
                        != previous_recommendation.limits[resource]
                    ]
                    requests_keys = [
                        resource
                        for resource in new_recommendation["requests"]
                        if previous_recommendation.requests
                        and new_recommendation["requests"][resource]
                        != previous_recommendation.requests[resource]
                    ]
                    diff = []
                    for k in limits_diff:
                        diff.append(
                            f"Limit {k} : {new_recommendation['limits'][k]} -> {previous_recommendation.limits[k]}"
                        )

                    for k in requests_keys:
                        diff.append(
                            f"Request {k} : {new_recommendation['requests'][k]} -> {previous_recommendation.requests[k]}"
                        )
                    logger.info("Difference in the recommendation: %s", "\n".join(diff))

                # Merging the old request and new request, because new request only contains CPU and Memory, we don't want
                # other resource requests to be deleted in patch_deployment...
                new_recommendation["requests"] = (
                    previous_recommendation.requests | new_recommendation["requests"]
                    if previous_recommendation.requests
                    else new_recommendation["requests"]
                )
                new_recommendation["limits"] = (
                    previous_recommendation.limits | new_recommendation["limits"]
                    if previous_recommendation.limits
                    else new_recommendation["limits"]
                )

                await self.k8s_engine.patch_deployment(
                    deployment_id=execution.deployment_id,
                    operations=[
                        {
                            "op": "replace",
                            "path": "/spec/template/spec/containers/0/resources",
                            "value": new_recommendation,
                        },
                    ],
                )

                await self._wait_deployment_state(
                    execution.deployment_id, DeploymentState.READY, 180.0
                )
        return recommended_resources

    async def get_gpu_num_by_nodepool(
        self, nodepool_label_selectors: dict[str, str]
    ) -> int:
        total_gpu = 0
        nodes = await self.k8s_engine.get_all_nodes_with_selectors(
            nodepool_label_selectors
        )
        for node in nodes:
            if "nvidia.com/gpu" not in node.status.allocatable:
                continue
            total_gpu += int(node.status.allocatable["nvidia.com/gpu"])

        return total_gpu
