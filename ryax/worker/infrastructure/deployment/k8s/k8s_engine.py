# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
from asyncio import CancelledError
from datetime import datetime, timedelta, timezone
from logging import getLogger
from typing import Dict, List, Tuple, Union

import aiohttp
import kubernetes_asyncio as k8s
from aiohttp import ClientConnectorError
from kubernetes_asyncio.client import (
    CoreV1Api,
    V1Deployment,
    V1Service,
    V1DeploymentList,
    V1PodCondition,
    V1PodList,
    V1Node,
    V1Pod,
)
from opentelemetry.instrumentation.aiohttp_client import AioHttpClientInstrumentor

from ryax.common.application.utils import retry_with_backoff
from ryax.worker.domain.deployment.deployment_events import (
    DeploymentUpdatedEvent,
    DeploymentNewLogLinesEvent,
)
from ryax.worker.domain.deployment.deployment_exceptions import DeploymentError
from ryax.worker.domain.deployment.deployment_values import DeploymentState
from ryax.common.domain.internal_messaging.message_bus import IMessageBus

logger = getLogger(__name__)

# Enable instrumentation
AioHttpClientInstrumentor().instrument()

K8sApiException = k8s.client.ApiException


def _pod_is_not_PodScheduled(conditions: List[V1PodCondition]) -> bool:
    return (
        len(
            list(
                filter(
                    lambda c: c.type == "PodScheduled" and c.status == "False",
                    conditions,
                )
            )
        )
        == 1
    )


def _pod_is_not_ContainersReady(conditions: List[V1PodCondition]) -> bool:
    return (
        len(
            list(
                filter(
                    lambda c: c.type == "ContainersReady" and c.status == "False",
                    conditions,
                )
            )
        )
        == 1
    )


def _deploy_is_ReplicaFailure(conditions: List[V1PodCondition]) -> bool:
    return (
        len(
            list(
                filter(
                    lambda c: c.type == "ReplicaFailure" and c.status == "True",
                    conditions,
                )
            )
        )
        == 1
    )


async def _do_get_deployment_state(
    core_v1: CoreV1Api,
    deployment: V1Deployment,
) -> Tuple[DeploymentState, str]:
    status = deployment.status
    if status.conditions is not None and _deploy_is_ReplicaFailure(status.conditions):
        failure_condition: V1PodCondition = list(
            filter(
                lambda c: c.type == "ReplicaFailure" and c.status == "True",
                status.conditions,
            )
        )[0]
        return DeploymentState.ERROR, failure_condition.message

    if (
        status.ready_replicas is not None
        and status.replicas is not None
        and status.available_replicas is not None
        and status.ready_replicas == status.replicas == status.available_replicas
    ):
        return DeploymentState.READY, "All the pods are running."

    # The deployment is not ready, so we check the pods for errors
    match_labels = deployment.spec.selector.match_labels
    label_selector = ",".join([f"{key}={value}" for key, value in match_labels.items()])
    pods: V1PodList = await core_v1.list_namespaced_pod(
        deployment.metadata.namespace, label_selector=label_selector
    )

    if len(pods.items) != 1:
        # The deployment may be created, but not the pods yet
        deployment_state = DeploymentState.DEPLOYING
        message = f"Unexpected number of pods. Found {len(pods.items)}, expected 1."
        return deployment_state, message

    pod_status = pods.items[0].status
    conditions = pod_status.conditions

    if pod_status.phase in ["Failed", "Succeeded"]:
        return (
            DeploymentState.ERROR,
            "The container ended its execution, as it is a server, it should never ends",
        )

    if conditions is None and pod_status.phase == "Pending":
        # The pod is still waiting to be deployed
        return (
            DeploymentState.DEPLOYING,
            "The pod is still waiting to be deployed",
        )
    if (
        len(conditions) == 1
        and conditions[0].type == "PodScheduled"
        and conditions[0].status == "True"
    ):
        # Pod was just scheduled and is still deploying
        return (
            DeploymentState.DEPLOYING,
            "Pod was just scheduled and is still deploying",
        )

    if _pod_is_not_PodScheduled(conditions):
        pod_scheduled_condition: V1PodCondition = list(
            filter(
                lambda c: c.type == "PodScheduled" and c.status == "False",
                conditions,
            )
        )[0]
        if pod_scheduled_condition.reason == "Unschedulable":
            return (
                DeploymentState.NO_RESOURCES_AVAILABLE,
                "Unable to deploy Action: " + pod_scheduled_condition.message,
            )
        return DeploymentState.DEPLOYING, "Pod not scheduled yet"

    if len(pod_status.container_statuses) > 1:
        # We assume pods only have one container (which is always the
        # case for now).
        raise DeploymentError(
            f"Unexpected number of containers on the pod (Expected 1, found {len(pod_status.container_statuses)})."
            f" Deployment {deployment.metadata.name}, pod {pods.items[0].metadata.name}"
        )

    if pod_status.container_statuses == 0:
        return DeploymentState.DEPLOYING, "Container not deployed yet"

    if _pod_is_not_ContainersReady(conditions):
        # There are three mutually exclusive possible states: "waiting",
        # "running", "terminated". A container that is terminated simply
        # means it ran successfully, which does not tell us much about the
        # deployment status.
        if pod_status.container_statuses[0].state.waiting is not None:
            waiting_state = pod_status.container_statuses[0].state.waiting
            if waiting_state.reason in [
                "ImagePullBackOff",
                "ErrImagePull",
                "CrashLoopBackOff",
                "RunContainerError",
            ]:
                return (
                    DeploymentState.ERROR,
                    f"{waiting_state.reason}: {waiting_state.message}",
                )
            elif waiting_state.reason == "ContainerCreating":
                return DeploymentState.DEPLOYING, str(waiting_state.reason)

    # If pod state is OOM Killed: let action deployment be [Deploying].
    # ActionDeployment works on K8s deployment level, it manages the consistency of pod existence.
    # When OOM happens a new pod will be created. It will be automatically be available again.

    # Drawback:
    # But when we want to immediately use the OOM Killed deployment, there's a quick pod terminating and creating
    # this will cause the execution to internal error (during building execution environment in the pod).
    return DeploymentState.DEPLOYING, ""


class PodInitializingError(Exception):
    pass


class LokiAPIError(Exception):
    pass


class ActionAddLabelsToDeploymentError(Exception):
    pass


class K8sEngine:
    def __init__(
        self,
        user_namespace: str,
        message_bus: IMessageBus,
        loki_url: str | None,
        log_query_interval_in_seconds: float = 1,
        max_log_line_length_in_bytes: int = 1024 * 1024,
    ) -> None:
        self.max_log_line_length_in_bytes = max_log_line_length_in_bytes
        self.log_query_interval_in_seconds = log_query_interval_in_seconds
        self.user_namespace = user_namespace
        self.message_bus: IMessageBus = message_bus
        self.loki_url = loki_url
        self.cancelled = False

    async def init(self) -> None:
        try:
            await k8s.config.load_kube_config()
            logger.info("Kubernetes config loaded from KUBECONFIG")
        except k8s.config.config_exception.ConfigException as err:
            logger.debug(
                "Unable to load Kubernetes configuration from KUBECONFIG: %s",
                err,
            )
            k8s.config.load_incluster_config()
            logger.info("Kubernetes config loaded from In-cluster config")

    def stop(self) -> None:
        self.cancelled = True

    async def create_user_namespace(self) -> None:
        try:
            async with k8s.client.ApiClient() as api:
                core_api = k8s.client.CoreV1Api(api)
                await core_api.create_namespace(
                    body={
                        "apiVersion": "v1",
                        "kind": "Namespace",
                        "metadata": {"name": self.user_namespace},
                    }
                )
        except k8s.client.ApiException as err:
            if err.status != 409:
                raise

    async def apply_ingress_definition(self, k8s_definition: Dict) -> None:
        async with k8s.client.ApiClient() as api:
            net_api = k8s.client.NetworkingV1Api(api)
            await net_api.create_namespaced_ingress(
                namespace=self.user_namespace, body=k8s_definition
            )

    async def apply_middleware_definition(self, k8s_middleware_def: Dict) -> None:
        async with k8s.client.ApiClient() as api:
            crd_api = k8s.client.CustomObjectsApi(api)
            await crd_api.create_namespaced_custom_object(
                group="traefik.containo.us",
                version="v1alpha1",
                namespace=self.user_namespace,
                plural="middlewares",
                body=k8s_middleware_def,
            )

    async def apply_service_definition(self, k8s_definition: Dict) -> None:
        async with k8s.client.ApiClient() as api:
            net_api = k8s.client.CoreV1Api(api)
            await net_api.create_namespaced_service(
                namespace=self.user_namespace, body=k8s_definition
            )

    async def apply_deployment_definition(self, k8s_definition: Dict) -> None:
        async with k8s.client.ApiClient() as api:
            net_api = k8s.client.AppsV1Api(api)
            await net_api.create_namespaced_deployment(
                namespace=self.user_namespace, body=k8s_definition
            )

    @retry_with_backoff()
    async def get_deployment_state(self, deployment_id: str) -> DeploymentState:
        async with k8s.client.ApiClient() as api:
            apps_v1 = k8s.client.AppsV1Api(api)
            core_v1 = k8s.client.CoreV1Api(api)
            deployments = await apps_v1.list_namespaced_deployment(
                self.user_namespace,
                label_selector=f"action_deployment_id={deployment_id}",
            )
            if len(deployments.items) == 0:
                return DeploymentState.UNDEPLOYED
            if len(deployments.items) > 1:
                raise DeploymentError(
                    f"Deployment {deployment_id}: There are two deployments with the same id."
                )
            else:
                deployment_state, _ = await _do_get_deployment_state(
                    core_v1, deployments.items[0]
                )
                return deployment_state

    async def get_annotations(self, deployment_id: str) -> dict:
        deployment = await self.get_deployment(deployment_id)
        return deployment.metadata.annotations

    @retry_with_backoff()
    async def get_deployment(self, deployment_id: str) -> V1Deployment | None:
        async with k8s.client.ApiClient() as api:
            apps_v1 = k8s.client.AppsV1Api(api)
            deployments = await apps_v1.list_namespaced_deployment(
                self.user_namespace,
                label_selector=f"action_deployment_id={deployment_id}",
            )
            if len(deployments.items) != 1:
                raise ActionAddLabelsToDeploymentError(
                    f"Deployment {deployment_id}: there are {len(deployments.items)} instances of the selected deployment action_deployment_id={deployment_id}, impossible to edit!"
                )
            deployment = deployments.items[0]
            return await apps_v1.read_namespaced_deployment(
                namespace=self.user_namespace,
                name=deployment.metadata.name,
            )
        return None

    async def patch_deployment(
        self, deployment_id: str, operations: list[dict]
    ) -> None:
        deployment = await self.get_deployment(deployment_id)
        await self._patch_deployment(
            namespace=self.user_namespace,
            name=deployment.metadata.name,
            operations=operations,
        )

    async def _patch_deployment(
        self, namespace: str, name: str, operations: list[dict]
    ) -> None:
        """
        Patch the current deployment and run operations on the list.
        CAUTION: everytime a deployment changes all pod replicas are restarted!
        Example of operations:
        operations = [
            { # add labels to spec/template/metadata, overwrite current values
                "op": "add",
                "path": "/spec/template/metadata/labels",
                "value": { "lab1":"val1", "lab2":"val2" },
            },
            { # add annotations to spec/template/metadata, overwrite current values
                "op": "add",
                "path": "/spec/template/metadata/annotations",
                "value": { "anot1":"val1", "anot2":"val2" },
            },
            { # associate a service_account_name
                "op": "add",
                "path": "/spec/template/spec/serviceAccountName",
                "value": "my_account_name",
            },
        ]
        """
        async with k8s.client.ApiClient() as api:
            apps_v1 = k8s.client.AppsV1Api(api)
            await apps_v1.patch_namespaced_deployment(
                namespace=namespace,
                name=name,
                body=operations,
            )

    @retry_with_backoff(retries=-1)
    async def watch_deployment(self, deployment_id: str) -> None:
        # WARNING: EKS seems to cut the watch after 15 minutes.
        # Reload the configuration to avoid 401 error
        await self.init()
        logger.debug(f"Start watching deployment {deployment_id}")
        async with k8s.client.ApiClient() as api:
            apps_v1 = k8s.client.AppsV1Api(api)
            core_v1 = k8s.client.CoreV1Api(api)
            async with k8s.watch.Watch().stream(
                apps_v1.list_namespaced_deployment,
                self.user_namespace,
                label_selector=f"action_deployment_id={deployment_id}",
            ) as stream:
                async for event in stream:
                    evt: str = event["type"]
                    deployment: V1Deployment = event["object"]
                    logger.debug(
                        "%s deployment %s in namespace %s",
                        evt,
                        deployment.metadata.name,
                        deployment.metadata.namespace,
                    )
                    try:
                        deployment_state, logs = await _do_get_deployment_state(
                            core_v1, deployment
                        )
                        await self.message_bus.handle(
                            [
                                DeploymentUpdatedEvent(
                                    deployment_id,
                                    deployment_state,
                                    logs,
                                )
                            ]
                        )
                    except DeploymentError:
                        logger.exception(
                            f"Transitive error during the action deployment '{deployment_id}'. Waiting for the next event..."
                        )

    @staticmethod
    def _second_to_nanosecond(timestamp: Union[int, float]) -> int:
        return int(timestamp * 10**9)

    async def _watch_pod_logs_within_range(
        self,
        session: aiohttp.ClientSession,
        deployment_id: str,
        # Datetime does not support nanoseconds so use int timestamps here.
        # https://bugs.python.org/issue15443
        start_timestamp: int,
    ) -> int:
        """
        Fetches the logs from the given container since given timestamp. Returns the timestamp until which the logs were fetched
        """
        #  Will crash if start timestamp was more than 721h ago (Loki's maximum range)

        max_time_for_query: int = self._second_to_nanosecond(
            timedelta(hours=720).total_seconds()
        )
        now = self._second_to_nanosecond(datetime.now(timezone.utc).timestamp())
        if now - start_timestamp > max_time_for_query:
            start_timestamp = now - max_time_for_query
        next_timestamp = start_timestamp

        loki_query_range_endpoint = "/loki/api/v1/query_range"
        params: dict[str, str | int] = {
            "query": f'{{action_deployment_id="{deployment_id}",namespace="{self.user_namespace}"}}',
            "start": start_timestamp,
            "direction": "forward",
        }
        logger.debug(f"Loki query parameters: {params}")
        try:
            async with session.get(
                f"http://{self.loki_url}{loki_query_range_endpoint}", params=params
            ) as async_response:
                logger.debug(f"Loki query URL: {async_response.url}")
                if async_response.status != 200:
                    error_message = f"{async_response.status} {async_response.reason} - {await async_response.text()}"
                    raise LokiAPIError(error_message)
                response = await async_response.json()
                for result in response["data"]["result"]:
                    text_log = ""
                    pod_name = result["stream"]["pod"]
                    values = result["values"]
                    # The second element of each values element is a log line (first is a timestamp).
                    # The first three fields of the line are information about the log, we only care about the rest.
                    for value in values:
                        timestamp = int(value[0])
                        line = value[1]
                        # logger.debug(
                        #    f"New line from {pod_name}: {human_readable_time} {line}"
                        # )

                        # Avoid duplicate log lines by filtering out the one that where produced before the start timestamp
                        if timestamp <= start_timestamp:
                            logger.error(
                                "Found duplicate log line: (%s: %s)", timestamp, value
                            )
                        else:
                            if timestamp > next_timestamp:
                                next_timestamp = timestamp
                            if len(line) > self.max_log_line_length_in_bytes:
                                line = (
                                    line[: self.max_log_line_length_in_bytes]
                                    + "... --- WARNING: Log line truncated\n"
                                )
                            text_log += line + "\n"
                    await self.message_bus.handle(
                        [
                            DeploymentNewLogLinesEvent(
                                logs=text_log,
                                executor_id=pod_name,
                                deployment_id=deployment_id,
                            )
                        ]
                    )
        except ClientConnectorError as err:
            logger.warning("The log service Loki, is unavailable: %s", err)
        except CancelledError:
            logger.debug(
                "The log watch was cancelled for deployment: %s", deployment_id
            )
        except aiohttp.ServerDisconnectedError:
            raise
        except Exception:
            logger.exception("Unexpected error during log capture")
        # We offset the timestamp to avoid getting the same log twice. Since Loki timestamps are in nanoseconds,
        # it is safe to do this without risking offsetting into the future and missing logs.
        return next_timestamp + 1

    @retry_with_backoff(retries=-1)
    async def watch_logs(self, deployment_id: str) -> None:
        """
        Watch execution_log from the pod in this deployment.
        """
        if self.loki_url is None:
            logger.warning(
                "Log capture is disable because the Loki API URL is not set."
            )
            return
        try:
            async with k8s.client.ApiClient() as client:
                api = k8s.client.AppsV1Api(client)
                deployment_list: V1DeploymentList = (
                    await api.list_namespaced_deployment(
                        namespace=self.user_namespace,
                        label_selector=f"action_deployment_id={deployment_id}",
                    )
                )
                if len(deployment_list.items) != 1:
                    raise PodInitializingError()
                deployment: V1Deployment = deployment_list.items[0]
                creation_timestamp = deployment.metadata.creation_timestamp
            # FIXME: put the first timestamp 24H before to be sure that even if  not in teh same timezone we have the logs!
            start_timestamp = int(
                (creation_timestamp.timestamp() - (24 * 60 * 60)) * 10**9
            )
            async with aiohttp.client.ClientSession() as session:
                while not self.cancelled:
                    start_timestamp = await self._watch_pod_logs_within_range(
                        session, deployment_id, start_timestamp
                    )
                    await asyncio.sleep(self.log_query_interval_in_seconds)
        except (
            asyncio.TimeoutError,
            PodInitializingError,
            aiohttp.ServerDisconnectedError,
            aiohttp.ClientConnectorError,
        ) as err:
            logger.debug(
                f"Error while reading execution_log for deployment {deployment_id}, maybe not started yet? Restarting the watcher: {err}"
            )
            raise

    async def delete_deployment(self, deployment_id: str) -> None:
        async with k8s.client.ApiClient() as client:
            api = k8s.client.AppsV1Api(client)
            try:
                await api.delete_namespaced_deployment(
                    namespace=self.user_namespace, name=deployment_id
                )
            except k8s.client.ApiException as err:
                logger.warning(
                    f"Unable to delete the deployment {deployment_id} due to {err}. Ignoring..."
                )

    async def delete_service(self, deployment_id: str) -> None:
        async with k8s.client.ApiClient() as api:
            core_api = k8s.client.CoreV1Api(api)
            try:
                await core_api.delete_namespaced_service(
                    namespace=self.user_namespace, name=deployment_id
                )
            except k8s.client.ApiException as err:
                logger.warning(
                    f"Unable to delete the service {deployment_id} due to {err}. Ignoring..."
                )

    async def delete_ingress(self, deployment_id: str) -> None:
        async with k8s.client.ApiClient() as api:
            net_api = k8s.client.NetworkingV1Api(api)
            try:
                await net_api.delete_namespaced_ingress(
                    namespace=self.user_namespace, name=deployment_id
                )
            except k8s.client.ApiException as err:
                logger.debug(
                    f"Unable to delete the ingress {deployment_id} due to {err}. Ignoring..."
                )

    async def delete_middleware(self, deployment_id: str) -> None:
        async with k8s.client.ApiClient() as api:
            crd_api = k8s.client.CustomObjectsApi(api)
            try:
                await crd_api.delete_namespaced_custom_object(
                    namespace=self.user_namespace,
                    name=deployment_id,
                    group="traefik.containo.us",
                    version="v1alpha1",
                    plural="middlewares",
                )
            except k8s.client.ApiException as err:
                logger.debug(
                    f"Unable to delete the middleware {deployment_id} due to {err}. Ignoring..."
                )

    async def _get_service(self, service_id: str) -> V1Service | None:
        async with k8s.client.ApiClient() as api:
            core_api = k8s.client.CoreV1Api(api)
            try:
                return await core_api.read_namespaced_service(
                    namespace=self.user_namespace, name=service_id
                )
            except k8s.client.ApiException as err:
                logger.warning(
                    f"Unable to read the service {service_id} due to {err}. Ignoring..."
                )
        return None

    async def get_all_deployments(self) -> list[V1Deployment]:
        async with k8s.client.ApiClient() as api:
            apps_v1 = k8s.client.AppsV1Api(api)
            list_deployments = await apps_v1.list_namespaced_deployment(
                self.user_namespace,
            )
            return list_deployments.items

    async def get_all_nodes_with_selectors(
        self, label_selectors: dict[str, str]
    ) -> list[V1Node]:
        async with k8s.client.ApiClient() as api:
            core_v1 = k8s.client.CoreV1Api(api)
            try:
                # Convert the dictionary to a label selector string
                label_selector_str = ",".join(
                    f"{key}={value}" for key, value in label_selectors.items()
                )

                nodes = await core_v1.list_node(label_selector=label_selector_str)
                return nodes.items  # Return a list of V1Node objects
            except k8s.client.ApiException as e:
                raise RuntimeError(
                    f"Failed to list nodes with labels {label_selectors}: {e}"
                )

    async def get_all_pods_by_node_name(self, node_name: str) -> list[V1Pod]:
        async with k8s.client.ApiClient() as api:
            core_v1 = k8s.client.CoreV1Api(api)
            v1pods = await core_v1.list_pod_for_all_namespaces(
                field_selector=f"spec.nodeName={node_name}"
            )
            return v1pods.items
