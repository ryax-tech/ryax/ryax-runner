# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import json
from dataclasses import dataclass
from logging import getLogger

import aiohttp

from ryax.worker.domain.deployment.deployment_values import (
    DeploymentState,
    ExecutionType,
)
from ryax.worker.domain.deployment.deployment_exceptions import (
    DeploymentError,
    UndeploymentError,
)
from ryax.worker.application.tasks_service import TasksService
from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.worker.domain.deployment.deployment_service import IDeploymentService
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import (
    K8sApiException,
    K8sEngine,
)
from ryax.worker.infrastructure.deployment.k8s.k8s_grpcv1_external_access import (
    K8sGRPCV1ExternalAccess,
)
from ryax.common.infrastructure.kubernetes.k8s_utils import safe_resource_name
from ryax.worker.infrastructure.deployment.k8s.k8s_recommendation_service import (
    RecommendationService,
)

logger = getLogger(__name__)


class UnsupportedActionKindDeploymentError(Exception):
    pass


def _labels(deployment: Deployment) -> dict[str, str]:
    """
    Apply name constraints defined by Kubernetes.

    More details here:
    https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set
    """
    return {
        "ryax": "action",
        "kind": deployment.kind.name,
        "action_deployment_id": deployment.id,
    }


@dataclass
class K8SActionDeploymentService(IDeploymentService):
    engine: K8sEngine
    tasks_service: TasksService
    default_grpc_port_inside_actions: int
    site_name: str
    recommendation_service: RecommendationService
    resource_recommendation_enabled: bool
    action_pull_secret_name: str

    async def apply(self, deployment: Deployment) -> None:
        """Apply the action deployment in Kubernetes"""
        logger.debug(
            "Deploying action '%s:%s'. Deployment id: %s",
            deployment.name,
            deployment.version,
            deployment.id,
        )

        if self.resource_recommendation_enabled:
            logger.debug("Recommendation service is ON, analyzing recommendation")
            recommendation_resource = self.recommendation_service.get_recommendation(
                target_image=deployment.container_image
            )
            if recommendation_resource is not None:
                logger.info(
                    f"FOUND RECOMMENDATION for deployment {deployment.id}... updating resources "
                )
                if recommendation_resource.cpu is not None:
                    deployment.resources.cpu = recommendation_resource.cpu
                if recommendation_resource.memory is not None:
                    deployment.resources.memory = recommendation_resource.memory
        else:
            logger.debug(
                "Recommendation service is OFF to activate it define RYAX_ACTIVATE_RECOMMENDATION=1"
            )

        k8s_deploy_def = self._generate(deployment, site_name=self.site_name)

        k8s_external_access = K8sGRPCV1ExternalAccess.create_from_deployment(
            deployment,
            self.engine.user_namespace,
            grpc_connection_port=self.default_grpc_port_inside_actions,
        )

        k8s_service_def = k8s_external_access.generate()
        # WARN Side effect here: Update action deployment gRPC endpoint
        if deployment.execution_type == ExecutionType.GRPC_V1:
            deployment.internal_endpoint = k8s_external_access.grpc_endpoint
        else:
            raise Exception(
                f"Unsupported Action execution type: {deployment.execution_type}"
            )

        try:
            await self.engine.apply_service_definition(k8s_service_def)
            await self.engine.apply_deployment_definition(k8s_deploy_def)
        except K8sApiException as err:
            logger.exception(
                f"Fail to deploy {deployment.id} remove trailing resource."
            )
            try:
                await self.delete(deployment)
            except UndeploymentError:
                pass
            raise DeploymentError(f"{err.status}: {err.reason}: {err.body}") from err
        except Exception as err:
            logger.exception(
                f"Fail to deploy {deployment.id} remove trailing resource."
            )
            try:
                await self.delete(deployment)
            except UndeploymentError:
                pass
            raise DeploymentError(str(err)) from err

    async def get_current_state(self, deployment: Deployment) -> DeploymentState:
        """Initialize deployment state"""
        try:
            return await self.engine.get_deployment_state(deployment.id)
        except aiohttp.ClientConnectorError as err:
            raise DeploymentError(
                "Unable to get state of the deployment %s", deployment.id
            ) from err

    async def watch(self, deployment: Deployment) -> None:
        """Launch a watcher process in background"""

        self.tasks_service.register_task(
            asyncio.create_task(
                self.engine.watch_deployment(deployment.id),
                name=deployment.id + "-deploy",
            )
        )
        self.tasks_service.register_task(
            asyncio.create_task(
                self.engine.watch_logs(deployment.id),
                name=deployment.id + "-logs",
            )
        )

    async def unwatch(self, deployment: Deployment) -> None:
        try:
            task = self.tasks_service.pop_task(deployment.id + "-deploy")
            task.cancel()

            logger.debug(
                "Wait until the logs are captured before stopping the watch of deployment: %s",
                deployment.id,
            )
            await asyncio.sleep(self.engine.log_query_interval_in_seconds * 1.5)
            task = self.tasks_service.pop_task(deployment.id + "-logs")
            task.cancel()
        except KeyError:
            logger.exception(
                f"Unable to find the task to cancel for stopping deployment: {deployment.id}"
            )

    async def delete(self, deployment: Deployment) -> None:
        try:
            resource_name = safe_resource_name(
                deployment.id, deployment.name, deployment.version
            )
            await self.engine.delete_deployment(resource_name)
            await self.engine.delete_service(resource_name)
        except Exception as err:
            raise UndeploymentError(str(err)) from err

    def _generate(self, deployment: Deployment, site_name: str) -> dict:
        resource_name = safe_resource_name(
            deployment.id, deployment.name, deployment.version
        )
        labels = _labels(deployment)
        extra_envs = [
            {
                "name": "RYAX_EXECUTOR_ID",
                "valueFrom": {"fieldRef": {"fieldPath": "metadata.name"}},
            },
            {
                "name": "RYAX_LOG_LEVEL",
                "value": deployment.log_level,
            },
            {
                "name": "RYAX_USER_DEFINED_CURRENT_IMAGE_URL",
                "value": deployment.container_image,
            },
        ]
        if deployment.addons is not None:
            for addon_name in deployment.addons:
                for addon_param in deployment.addons[addon_name]:
                    env_var_name = (
                        "RYAX_USER_DEFINED_"
                        + addon_name.upper()
                        + "_"
                        + addon_param.upper()
                    )
                    env_var_value = ""
                    if isinstance(deployment.addons[addon_name][addon_param], dict):
                        env_var_value = json.dumps(
                            json.dumps(deployment.addons[addon_name][addon_param])
                        )
                    elif deployment.addons[addon_name][addon_param] is not None:
                        env_var_value = str(deployment.addons[addon_name][addon_param])
                    extra_envs.append(
                        {
                            "name": env_var_name,
                            "value": env_var_value,
                        }
                    )
        containers = [
            {
                "name": resource_name,
                "image": deployment.container_image,
                # Be sur that the image is always pull
                "imagePullPolicy": "Always",
                "env": [
                    *extra_envs,
                ],
                "volumeMounts": [
                    {"mountPath": "/tmp", "name": "tmp-dir"},
                    {"mountPath": "/home/ryax", "name": "home-dir"},
                    {"mountPath": "/static_data", "name": "inputs-dir"},
                ],
                # the default user called "ryax" has 1200:1200 UID:GID
                "securityContext": {
                    "allowPrivilegeEscalation": False,
                    "readOnlyRootFilesystem": True,
                },
            }
        ]

        deployment_annotations = {}
        if self.resource_recommendation_enabled:
            deployment_annotations = {
                "ryax.tech/recommender_input": json.dumps(
                    {
                        "image": deployment.container_image,
                        # Here in this key we can add more metadata about the workflow and the inputs, in future
                    }
                )
            }

        if (
            deployment.resources is not None
            and deployment.resources.has_deployment_spec()
        ):
            resource_deployment_spec = deployment.resources.generate_deployment_spec()
            containers[0].update(resource_deployment_spec)
            # Add default resource requested by user as a deployment annotation

            # For recommender
            user_resources_request = {}
            for k, v in resource_deployment_spec["resources"]["requests"].items():
                if v is not None:
                    user_resources_request[k] = v
            deployment_annotations["ryax.tech/user_resources_request"] = json.dumps(
                user_resources_request
            )

        template_spec = {
            # the default user called "ryax" has 1200:1200 UID:GID
            "securityContext": {
                "runAsUser": 1200,
                "runAsGroup": 1200,
                "fsGroup": 1200,
            },
            "automountServiceAccountToken": False,
            "terminationGracePeriodSeconds": 2,  # We expect the launcher to take less than 2s to exit.
            "containers": containers,
            "volumes": [
                {"emptyDir": {}, "name": "tmp-dir"},
                {"emptyDir": {}, "name": "home-dir"},
                {"emptyDir": {}, "name": "inputs-dir"},
            ],
            "nodeSelector": deployment.node_pool.selector,  # get the labels that correspond to the node pool
        }
        if self.action_pull_secret_name:
            template_spec.update(
                {"imagePullSecrets": [{"name": self.action_pull_secret_name}]}
            )

        pod_labels = _labels(deployment)
        pod_annotations = {}
        kube_params = deployment.addons.get("kubernetes")
        if kube_params is not None:
            if kube_params.get("labels") is not None:
                pod_labels.update(kube_params["labels"])

            if kube_params.get("annotations") is not None:
                pod_annotations = kube_params["annotations"]

            if kube_params.get("node_selector") is not None:
                template_spec["nodeSelector"] = kube_params["node_selector"]

            if kube_params.get("service_account_name") is not None:
                template_spec["serviceAccountName"] = kube_params[
                    "service_account_name"
                ]
                template_spec["automountServiceAccountToken"] = True
            if kube_params.get("runtime_class") is not None:
                template_spec["runtimeClassName"] = kube_params["runtime_class"]

        toleration = []
        if (
            deployment.resources is not None
            and deployment.resources.gpu is not None
            and deployment.resources.gpu > 0
        ):
            toleration.append(
                {
                    "key": "sku",
                    "operator": "Equal",
                    "value": "gpu",
                    "effect": "NoSchedule",
                },  # For azure deployment on gpu
            )

        toleration.append(
            {  # {"key": "ryax.tech/ryaxns-execs", "value": "only", "effect": "NoSchedule"}
                # scaleway add: taint=noprefix=ryax.tech/ryaxns-execs=only:NoSchedule
                "key": "ryax.tech/ryaxns-execs",
                "operator": "Equal",
                "value": "only",
                "effect": "NoSchedule",
            },  # make nodes dedicated to run ryaxns-execs actions by using the taint
        )

        template_spec.update({"tolerations": toleration})

        return {
            "apiVersion": "apps/v1",
            "kind": "Deployment",
            "metadata": {
                "name": resource_name,
                "labels": labels,
                "annotations": deployment_annotations,
            },
            "spec": {
                "strategy": {  # Avoid having two pod for the same deployment
                    "type": "Recreate"
                },
                "replicas": 1,
                "selector": {
                    "matchLabels": {
                        "action_deployment_id": deployment.id,
                    }
                },
                # This options keeps old versions of a deployment to rollback
                # if needed (up to 10 by default). We don't want this, because
                # it results in dangling pods when a deployment gets deleted or
                # modified.  Versioning is managed by the user, not by
                # Kubernetes.
                "revisionHistoryLimit": 0,
                "template": {
                    "metadata": {
                        "labels": pod_labels,
                        "annotations": pod_annotations,
                    },
                    "spec": template_spec,
                },
            },
        }
