class RegistrationNotFoundError(Exception):
    pass


class RegistrationFailureError(Exception):
    pass
