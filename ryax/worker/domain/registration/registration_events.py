from dataclasses import dataclass

from ryax.common.domain.internal_messaging.base_messages import BaseCommand, BaseEvent
from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
    ObjectiveScores,
    GPU_MODE_FULL,
)


@dataclass
class RegisterCommand(BaseCommand):
    @dataclass
    class RegistrationNodePool:
        name: str
        cpu: float
        memory: int
        gpu: int
        architecture: NodeArchitecture
        selector: dict[str, str]
        instance_type: str | None = None
        maximum_time_in_sec: int | None = None
        # A score for each objective
        objective_scores: ObjectiveScores | None = None
        gpu_mode: str = GPU_MODE_FULL
        gpu_num_total: int = 0
        filter_no_gpu_action: bool = True

    id: str | None
    name: str
    type: SiteType
    node_pools: list[RegistrationNodePool]


class UpdateRegistrationEvent(BaseEvent):
    pass
