from dataclasses import dataclass

REGISTRATION_UNIQUE_ID = "registration_unique_id"


@dataclass
class Registration:
    site_id: str
    id: str = REGISTRATION_UNIQUE_ID
