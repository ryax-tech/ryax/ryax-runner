import abc

from ryax.worker.domain.registration.registration_entities import Registration


class IRegistrationRepository:
    @abc.abstractmethod
    def get(self) -> Registration | None:
        ...

    @abc.abstractmethod
    def set(self, registration: Registration) -> None:
        ...
