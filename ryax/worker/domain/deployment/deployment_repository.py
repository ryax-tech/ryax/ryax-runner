import abc

from ryax.worker.domain.deployment.deployment_entities import Deployment


class IDeploymentRepository:
    @abc.abstractmethod
    def get(self, deployment_id: str) -> Deployment:
        ...

    @abc.abstractmethod
    def get_eager(self, deployment_id: str) -> Deployment:
        ...

    @abc.abstractmethod
    def add(self, deployment: Deployment) -> None:
        ...

    @abc.abstractmethod
    def list(self) -> list[Deployment]:
        ...

    @abc.abstractmethod
    def delete(self, deployment_id: str) -> None:
        ...
