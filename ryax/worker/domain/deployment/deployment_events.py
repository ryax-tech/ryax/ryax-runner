from dataclasses import dataclass
from datetime import datetime

from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.worker.domain.deployment.deployment_values import DeploymentState
from ryax.common.domain.internal_messaging.base_messages import BaseEvent, BaseCommand


@dataclass
class CreateDeploymentCommand(BaseCommand):
    deployment: Deployment


@dataclass
class ApplyDeploymentCommand(BaseCommand):
    deployment_id: str


@dataclass
class UndeployCommand(BaseCommand):
    deployment_id: str


@dataclass
class DeploymentGetCurrentStateCommand(BaseCommand):
    deployment_id: str


@dataclass
class RecommendationsVPAUpdateCommand(BaseCommand):
    @dataclass
    class RecommendationVPA:
        container_image: str
        cpu: float
        memory: int
        gpu_mig_instance: str
        timestamp: datetime
        is_oom_bumpup: bool
        is_gpuoom_bumpup: bool

    recommendations: list[RecommendationVPA]


@dataclass
class DeploymentUpdatedEvent(BaseEvent):
    deployment_id: str
    state: DeploymentState
    logs: str


@dataclass
class DeploymentNewLogLinesEvent(BaseEvent):
    deployment_id: str
    executor_id: str
    logs: str
