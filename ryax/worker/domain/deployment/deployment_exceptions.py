class DeploymentNotFoundError(Exception):
    pass


class DeploymentError(Exception):
    pass


class UndeploymentError(Exception):
    pass


class RecommendationsVPAUpdateFailureError(Exception):
    pass
