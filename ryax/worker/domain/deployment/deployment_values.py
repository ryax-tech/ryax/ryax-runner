from enum import Enum


class DeploymentState(Enum):
    REGISTERED = "Registered"
    DEPLOYING = "Deploying"
    READY = "Ready"
    RUNNING = "Running"
    UNDEPLOYING = "Undeploying"
    UNDEPLOYED = "Undeployed"
    ERROR = "Error"
    NO_RESOURCES_AVAILABLE = "Not Enough Resources Available"

    def is_stopped(self) -> bool:
        return self in [
            DeploymentState.ERROR,
            DeploymentState.UNDEPLOYED,
            DeploymentState.UNDEPLOYING,
        ]


class ExecutionType(Enum):
    GRPC_V1 = "GRPC_V1"
    SLURM_SSH_V1 = "SLURM_SSH_V1"
