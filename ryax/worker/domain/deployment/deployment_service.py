# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc

from ryax.worker.domain.deployment.deployment_entities import (
    Deployment,
)
from ryax.worker.domain.deployment.deployment_values import (
    DeploymentState,
)
from ryax.worker.domain.deployment.deployment_exceptions import DeploymentError


class IDeploymentService(abc.ABC):
    """Interface for deployment service.

    This service is in charge of applying the deployment to the target infrastructure from the Deployment object.
    It also has to watch the state and the execution_log of the deployment and delete it when requested.
    """

    async def deploy(self, action: Deployment) -> None:
        try:
            await self.apply(action)
        except Exception as err:
            raise DeploymentError(str(err)) from err

    @abc.abstractmethod
    async def apply(self, action: Deployment) -> None:
        """Apply an action deployment on the target infrastructure"""
        raise NotImplementedError()

    @abc.abstractmethod
    async def get_current_state(self, deployment: Deployment) -> DeploymentState:
        """Returns the current state of the deployment"""
        raise NotImplementedError()

    @abc.abstractmethod
    async def watch(self, action: Deployment) -> None:
        """Trigger the watch of the deployment state and the execution logs

        Should send DeploymentUpdatedEvent when the action change state.
        Should also send DeploymentNewLogLinesEvent when new log lines are captured
        """
        pass

    @abc.abstractmethod
    async def unwatch(self, action_deployment: Deployment) -> None:
        """Stop the deployment and execution_log watch"""
        pass

    @abc.abstractmethod
    async def delete(self, action_deployment: Deployment) -> None:
        """Delete the deployment from the infrastructure"""
        pass
