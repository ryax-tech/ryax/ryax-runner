from dataclasses import dataclass
from datetime import datetime
from enum import Enum

from ryax.worker.domain.deployment.deployment_values import ExecutionType
from ryax.worker.domain.node_pool.node_pool_entities import NodePool


class Kind(Enum):
    ACTION = 0
    TRIGGER = 1


@dataclass
class Resources:
    cpu: float | None = None
    memory: int | None = None
    time: float | None = None
    gpu: int | None = None
    gpu_mig_instance: str | None = None

    def has_deployment_spec(self) -> bool:
        return (
            self.cpu is not None
            or self.memory is not None
            or self.gpu is not None
            or self.gpu_mig_instance is not None
        )

    def generate_deployment_spec(
        self, memory_coeff: float = 1.0, cpu_coeff: float = 2
    ) -> dict:
        spec: dict = {"resources": {"requests": {}, "limits": {}}}
        if self.cpu:
            spec["resources"]["requests"]["cpu"] = f"{int(self.cpu * 1000)}m"
            spec["resources"]["limits"]["cpu"] = f"{int(self.cpu * 1000 * cpu_coeff)}m"
        if self.memory:
            spec["resources"]["requests"]["memory"] = str(int(self.memory))
            spec["resources"]["limits"]["memory"] = str(int(self.memory * memory_coeff))
        if self.gpu:
            spec["resources"]["limits"]["nvidia.com/gpu"] = str(self.gpu)
        return spec


@dataclass
class ResourceRecommendation:
    # Current key is only image name. To add more as key in future
    image: str
    resources: Resources
    timestamp: datetime

    def match(self, image: str) -> bool:
        return self.image == image


@dataclass
class Deployment:
    id: str
    name: str
    version: str
    container_image: str
    kind: Kind
    resources: Resources
    execution_type: ExecutionType
    deployment_type: str
    addons: dict
    log_level: str
    node_pool: NodePool
    internal_endpoint: str | None = None

    def do_run_forever(self) -> bool:
        """Trigger and deployment of kind sshslurm should never be killed by a timeout"""
        return self.kind != Kind.ACTION or self.deployment_type == "sshslurm"
