class ExecutionNotFoundError(Exception):
    pass


class ExecutionIODefinitionNotFoundError(Exception):
    pass


class TaskInitializationError(Exception):
    pass
