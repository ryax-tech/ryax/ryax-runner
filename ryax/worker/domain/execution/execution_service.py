# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
import asyncio
from logging import getLogger

from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.worker.domain.execution.execution_entities import (
    Execution,
)
from ryax.worker.domain.execution.execution_events import (
    ExecutionEndedEvent,
)
from ryax.worker.application.tasks_service import TasksService
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.storage.storage_service import IStorageService
from ryax.common.domain.utils import format_exception
from ryax.worker.domain.execution.execution_values import ExecutionState

logger = getLogger(__name__)


class ActionExecutionTimeout(Exception):
    pass


class ActionExecutionRequestFailError(Exception):
    pass


class IExecutionService(abc.ABC):
    """Interface for action Execution"""

    storage_service: IStorageService
    message_bus: IMessageBus
    tasks_service: TasksService

    @abc.abstractmethod
    async def run(
        self,
        execution: Execution,
        deployment: Deployment,
    ) -> None:
        """
        Running a action execution_connection on the target.

        This method only returns when an error occurs or when the ExecutionConnection finishes.

        WARNING: Never call this directly, use self.start instead

        The message bus is used for:
        1. inform the deployment service after initialization using the message bus
           Example:
           ```py
           await self.message_bus.handle_command(
                InitializeExecutionResultCommand(
                    execution.id, executor_id, log_delimiter
                )
            )
           ```
        2. Get file inputs from the stored_file service
        3. send every created executions through the message bus
           Example:
           ```py
           await self.message_bus.handle_command(
               CreateExecutionResultCommand(
                   execution_connection_id=execution.id,
                   outputs=response.outputs,
                   status=ExecutionResultState.SUCCESS,
                   start_time=response.started_at.ToDatetime(),
                   end_time=response.ended_at.ToDatetime(),
                   workflow_run_id="wfr-123486734",
                   next_log_delimiter=response.next_log_delimiter,
               )
           )
        4. returns when the run is done.
           ```
        """
        raise NotImplementedError()

    async def _run_wrapper(self, execution: Execution, deployment: Deployment) -> None:
        try:
            if deployment.do_run_forever():
                await self.run(execution, deployment)
            else:
                await asyncio.wait_for(
                    self.run(execution, deployment),
                    timeout=float(execution.time_allotment),
                )
        except asyncio.TimeoutError:
            await self.message_bus.handle_event(
                ExecutionEndedEvent(
                    execution.id,
                    status=ExecutionState.CANCELED,
                    error_message=f"Execution has taken more than the allotted time ({execution.time_allotment}s). Execution has been cancelled.",
                )
            )
        except asyncio.CancelledError:
            await self.message_bus.handle_event(
                ExecutionEndedEvent(
                    execution.id,
                    status=ExecutionState.CANCELED,
                    error_message="Execution was cancel by the user.",
                )
            )
        except Exception as err:
            logger.exception(
                "Unexpected error while running execution %s with error: %s",
                execution.id,
                err,
            )
            await self.message_bus.handle_event(
                ExecutionEndedEvent(
                    execution.id,
                    status=ExecutionState.ERROR,
                    error_message=f"Internal Error while running execution: {format_exception(err)}",
                )
            )
        else:
            await self.message_bus.handle_event(
                ExecutionEndedEvent(
                    execution.id,
                    status=ExecutionState.COMPLETED,
                )
            )
        finally:
            del execution

    async def start(self, execution: Execution, deployment: Deployment) -> None:
        """
        Create the tasks for the given execution
        :param deployment: the deployment to run the execution on
        :param execution: the execution to start
        :param addons_extra_params: a dict containing the values of extra parameters for addons
        """
        self.tasks_service.register_task(
            asyncio.create_task(
                self._run_wrapper(execution, deployment),
                name=f"IExecutionService-{execution.id}",
            )
        )

    async def stop(self, execution: Execution) -> None:
        """
        Cancel the tasks create for the given execution
        :param execution: the execution to cancel
        """
        try:
            task = self.tasks_service.pop_task(f"IExecutionService-{execution.id}")
            task.cancel()
        except KeyError:
            logger.warning(
                f"No task is registered for the execution {execution.id}. Unable to stop, ignoring..."
            )
        else:
            try:
                await asyncio.wait_for(task, timeout=5)
            except asyncio.exceptions.TimeoutError:
                logger.warning("Unable to stop Execution Connection %s", execution.id)
