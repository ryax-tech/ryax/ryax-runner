from dataclasses import dataclass, field
from datetime import datetime
from typing import Any

from ryax.worker.domain.execution.execution_entities import Execution
from ryax.worker.domain.execution.execution_values import ExecutionState, TaskState
from ryax.common.domain.internal_messaging.base_messages import BaseEvent, BaseCommand
from ryax.common.domain.utils import get_date


@dataclass
class CreateExecutionCommand(BaseCommand):
    execution: Execution


@dataclass
class GetExecutionStateCommand(BaseCommand):
    execution_id: str


@dataclass
class StartExecutionCommand(BaseCommand):
    execution_id: str


@dataclass
class StopExecutionCommand(BaseCommand):
    execution_id: str


@dataclass
class ExecutionEndedEvent(BaseEvent):
    execution_id: str
    status: ExecutionState
    error_message: str = ""


@dataclass
class InitTaskCommand(BaseCommand):
    execution_id: str
    executor_id: str
    log_delimiter: str | None = None


@dataclass
class TaskCompletedEvent(BaseEvent):
    @dataclass
    class OutputFile:
        output_name: str
        file_path: str
        mimetype: str | None
        encoding: str | None
        size_in_bytes: int

    id: str
    outputs: dict[str, Any]
    output_files: list[OutputFile]
    status: TaskState
    end_time: datetime = field(default_factory=get_date)
    error_message: str | None = None
    http_status_code: int | None = None


@dataclass
class TaskNewLogLinesEvent(BaseEvent):
    log_lines: str
    task_id: str
