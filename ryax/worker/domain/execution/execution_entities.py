from dataclasses import dataclass
from enum import Enum

from ryax.common.domain.utils import get_random_id
from ryax.worker.domain.execution.execution_exceptions import (
    ExecutionIODefinitionNotFoundError,
)
from ryax.worker.domain.execution.execution_values import ExecutionState


class IOType(Enum):
    BOOLEAN = 0
    STRING = 1
    INTEGER = 2
    FLOAT = 3
    FILE = 6
    DIRECTORY = 7
    BYTES = 8

    def is_file_type(self) -> bool:
        return self in [IOType.FILE, IOType.DIRECTORY]


@dataclass
class IODefinition:
    id: str
    name: str
    type: IOType
    optional: bool

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"


class InputDefinition(IODefinition):
    pass


class OutputDefinition(IODefinition):
    pass


@dataclass
class Execution:
    id: str
    deployment_id: str
    inputs_definitions: list[InputDefinition]
    outputs_definitions: list[OutputDefinition]
    inputs: dict
    time_allotment: int
    state: ExecutionState = ExecutionState.CREATED

    def get_input_definition(self, input_name: str) -> IODefinition:
        for action_input in self.inputs_definitions:
            if action_input.name == input_name:
                return action_input
        raise ExecutionIODefinitionNotFoundError(
            f"Could not find input matching name: '{input_name}' list of inputs: {self.inputs}"
        )

    def get_output_definition(self, output_name: str) -> IODefinition:
        for action_output in self.outputs_definitions:
            if action_output.name == output_name:
                return action_output
        raise ExecutionIODefinitionNotFoundError(
            f"Could not find output matching name: '{output_name}' list of outputs: {self.outputs_definitions}"
        )

    def get_outputs(self, filter_type: IOType | None = None) -> list[OutputDefinition]:
        """
        Returns all outputs from this action definition filtered by type.
        """
        if filter_type is not None:
            return [
                action
                for action in self.outputs_definitions
                if action.type == filter_type
            ]
        return self.outputs_definitions

    def set_running(self) -> None:
        self.state = ExecutionState.RUNNING
