from enum import Enum


class ExecutionState(Enum):
    UNKNOWN = None
    CREATED = "Created"
    RUNNING = "Running"
    COMPLETED = "Completed"
    CANCELED = "Canceled"
    ERROR = "Error"


class TaskState(Enum):
    SUCCESS = "Success"
    ERROR = "Error"
