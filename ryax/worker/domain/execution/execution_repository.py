import abc

from ryax.worker.domain.execution.execution_entities import Execution


class IExecutionRepository:
    @abc.abstractmethod
    def get(self, execution_id: str) -> Execution:
        ...

    @abc.abstractmethod
    def add(self, execution: Execution) -> None:
        ...

    @abc.abstractmethod
    def list_running(self) -> list[Execution]:
        ...

    @abc.abstractmethod
    def delete(self, execution_id: str) -> None:
        ...
