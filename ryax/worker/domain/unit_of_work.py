# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from types import TracebackType
from typing import Type

from ryax.worker.domain.deployment.deployment_repository import (
    IDeploymentRepository,
)
from ryax.worker.domain.execution.execution_repository import IExecutionRepository
from ryax.worker.domain.node_pool.node_pool_repository import INodePoolRepository
from ryax.worker.domain.registration.registration_repository import (
    IRegistrationRepository,
)


class IUnitOfWork(abc.ABC):
    deployment_repository: IDeploymentRepository
    execution_repository: IExecutionRepository
    registration_repository: IRegistrationRepository
    node_pool_repository: INodePoolRepository

    def __enter__(self) -> "IUnitOfWork":
        return self

    def __exit__(
        self,
        exc_type: Type[Exception] | None = None,
        exc_val: Exception | None = None,
        exc_tb: TracebackType | None = None,
    ) -> None:
        pass

    @abc.abstractmethod
    def commit(self) -> None:
        pass

    @abc.abstractmethod
    def rollback(self) -> None:
        pass
