from dataclasses import dataclass

from ryax.common.domain.utils import get_random_id


@dataclass
class NodePool:
    id: str
    name: str
    selector: dict

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"
