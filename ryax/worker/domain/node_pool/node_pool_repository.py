import abc

from ryax.worker.domain.node_pool.node_pool_entities import NodePool


class NodePoolNotFoundError(Exception):
    pass


class INodePoolRepository:
    @abc.abstractmethod
    def get_by_name(self, node_pool_name: str) -> NodePool:
        ...

    @abc.abstractmethod
    def add(self, node_pool: NodePool) -> None:
        ...
