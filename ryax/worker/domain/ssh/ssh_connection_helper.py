# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
import logging
import os
from dataclasses import dataclass
from pathlib import Path
from typing import NamedTuple, Optional

logger = logging.getLogger(__name__)


class ISshExecutionOutput(NamedTuple):
    stdout: str
    stderr: str
    return_code: int


@dataclass
class SSHCredentials:
    server: str
    private_key: str
    password: str
    username: str
    port: int
    config: str | None

    @classmethod
    def from_dict(cls, data: dict) -> "SSHCredentials":
        # Get private key content
        private_key_file_path = data.get(
            "privateKeyFile", Path(os.environ["HOME"]) / ".ssh" / "id_rsa"
        )
        if private_key_file_path.is_file():
            with open(private_key_file_path) as private_key_file:
                private_key = private_key_file.read()
        else:
            private_key = ""
            logger.warning(
                "Unable to find SSH private key on the given location: %s",
                private_key_file_path,
            )

        config = data.get("configFile", Path(os.environ["HOME"]) / ".ssh" / "config")
        return cls(
            private_key=private_key,
            server=data["server"],
            password=data.get("password", None),
            username=data["username"],
            port=int(data.get("port", 22)),
            config=config if config.is_file() else None,
        )


class ISshConnectionHelper(abc.ABC):
    """Interface to help anything that need an ssh connection.

    This service can login using private_key or password on an ssh remote server to execute commands
    send/receive files.
    """

    hostname: str
    port: int = 22
    username: str
    password: Optional[str]
    private_key: Optional[str]

    @abc.abstractmethod
    def __init__(
        self, hostname: str, port: int, username: str, password: str, private_key: str
    ):
        ...

    @abc.abstractmethod
    async def exec_ssh_cmd(self, cmd: str) -> ISshExecutionOutput:
        """
        StartExecution ssh command and return stdout, stderr, and the exit code.
        """
        ...

    @abc.abstractmethod
    async def remote_file_exists(self, filepath: Path) -> bool:
        """
        Verify that the filepath exists on remote, if yes returns True
        False otherwise. Raise error if ssh connection problem is detected.
        """
        ...

    @abc.abstractmethod
    async def put_ssh_file(self, local_file: Path, remote_file: Path) -> None:
        """
        Copy local_file in to remote_file on the remote peer.
        @param local_file path to the local file to copy from the current machine to the server.
        @param remote_file destination path on the remote host.
        """
        ...

    @abc.abstractmethod
    async def get_ssh_file(self, remote_file: Path, local_file: Path) -> None:
        """
        Download a file from the ssh remote peer.
        @param remote_file path of the file on the remote peer.
        @param local_file path to the file locally after download.
        """
        ...

    @abc.abstractmethod
    async def close(self) -> None:
        """
        Close all open connection properly.
        """
        ...
