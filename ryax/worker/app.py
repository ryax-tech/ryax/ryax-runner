# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.


import concurrent.futures
import asyncio
import gc
import logging
import platform
import tracemalloc
from pathlib import Path

import multiprocessing
from typing import AsyncGenerator

from aiohttp import web
from dependency_injector.wiring import inject
from opentelemetry.instrumentation.logging import LoggingInstrumentor

from ryax.worker.application.setup import setup as application_setup
from ryax.worker.container import ApplicationContainer
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupCommand,
    ApplicationStartupEvent,
    StopApplicationCommand,
)
from ryax.worker.infrastructure.api.setup import setup as api_setup
from ryax.worker.infrastructure.messaging.setup import setup as messaging_setup
from .domain.registration.registration_events import UpdateRegistrationEvent

from .version import __version__

logger = logging.getLogger(__name__)
mem_logger = logging.getLogger("ryax.performance.memory")


def _env_var_to_bool(value: str) -> bool:
    if isinstance(value, bool):
        return value
    elif isinstance(value, str):
        if (
            value.upper() == "FALSE"
            or value == "0"
            or value == ""
            or value.upper() == "NULL"
            or value.upper() == "DISABLED"
        ):
            return False
    return True


def configure() -> ApplicationContainer:
    # Init application container
    container = ApplicationContainer()

    # First get config file path
    container.configuration.config_file.from_env("RYAX_CONFIG", Path("config.yaml"))
    # Load config file
    container.configuration.from_yaml(container.configuration.config_file())

    # Override with env variables
    # Site configuration
    if not container.configuration.site.name():
        container.configuration.site.name.from_env(
            "RYAX_SITE_NAME", default=platform.node()
        )
    if not container.configuration.site.type():
        container.configuration.site.type.from_env(
            "RYAX_SITE_TYPE", default="KUBERNETES"
        )
    if not container.configuration.site.spec():
        container.configuration.site.spec.from_env("RYAX_SITE_SPEC", default={})

    if not container.configuration.api.port():
        container.configuration.api.port.from_env(
            "RYAX_API_PORT", default=8880, as_=int
        )
    if not container.configuration.log_level():
        container.configuration.log_level.from_env("RYAX_LOG_LEVEL", "INFO")
    if not container.configuration.messaging.url():
        container.configuration.messaging.url.from_env("RYAX_BROKER", required=True)
    if not container.configuration.database.url():
        container.configuration.database.url.from_env(
            "RYAX_WORKER_DATABASE_URL", required=True
        )
    if not container.configuration.resource_recommendation_enabled():
        container.configuration.resource_recommendation_enabled.from_env(
            "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=True, as_=_env_var_to_bool
        )

    # Deployment configuration
    if container.configuration.site.type() == "KUBERNETES":
        if not container.configuration.site.spec.namespace():
            container.configuration.site.spec.namespace.from_env(
                "RYAX_USER_NAMESPACE", required=True
            )

    if not container.configuration.actions.web_access_port():
        container.configuration.actions.web_access_port.from_env(
            "RYAX_ACTIONS_WEB_ACCESS_PORT", default=8080, as_=int
        )
    if not container.configuration.actions.grpc_port():
        container.configuration.actions.grpc_port.from_env(
            "RYAX_ACTIONS_GRPC_PORT", default=8081, as_=int
        )
    if not container.configuration.actions.pull_secret():
        container.configuration.actions.pull_secret.from_env(
            "RYAX_ACTIONS_PULL_SECRET", default="ryax-registry-pull-secret"
        )

    if container.configuration.site.type() == "SLURM_SSH":
        if not container.configuration.site.spec.cache_dir():
            container.configuration.site.spec.cache_dir.from_env(
                "RYAX_HPC_CACHE_DIR", default=Path(".ryax_cache")
            )
        if not container.configuration.deployment.internal_registry_override():
            container.configuration.deployment.internal_registry_override.from_env(
                "RYAX_INTERNAL_REGISTRY_OVERRIDE"
            )

    # stored_file config
    if not container.configuration.storage.local.type():
        container.configuration.storage.local.type.from_env(
            "RYAX_LOCAL_STORAGE_TYPE", "s3"
        )
    if not container.configuration.storage.local.s3.server():
        container.configuration.storage.local.s3.server.from_env(
            "RYAX_FILESTORE", required=True
        )
    if not container.configuration.storage.local.s3.bucket():
        container.configuration.storage.local.s3.bucket.from_env(
            "RYAX_FILESTORE_BUCKET", "ryax-local-data"
        )
    if not container.configuration.storage.local.s3.access_key():
        container.configuration.storage.local.s3.access_key.from_env(
            "RYAX_FILESTORE_ACCESS_KEY", required=True
        )
    if not container.configuration.storage.local.s3.secret_key():
        container.configuration.storage.local.s3.secret_key.from_env(
            "RYAX_FILESTORE_SECRET_KEY", required=True
        )
    if not container.configuration.storage.local.posix.root_path():
        container.configuration.storage.local.posix.root_path.from_env(
            "RYAX_DATA_DIR", "./ryax_data"
        )

    # Metrics server
    container.configuration.observabiliy.metrics_server_port.from_env(
        "RYAX_METRICS_SERVER_PORT", default=8090, as_=int
    )
    container.configuration.observabiliy.otlp_endpoint.from_env("RYAX_OTLP_ENDPOINT")

    # Loki url
    container.configuration.deployment.logs.loki.api_url.from_env(
        "RYAX_LOKI_API_URL", required=True
    )
    container.configuration.deployment.logs.query_interval_in_seconds.from_env(
        "RYAX_LOKI_QUERY_INTERVAL_IN_SECONDS", default=1, as_=float
    )
    container.configuration.deployment.logs.logs_max_size_in_bytes.from_env(
        "RYAX_ACTION_LOGS_MAX_SIZE_IN_BYTES", default=1024 * 1024, as_=int
    )
    container.configuration.deployment.logs.log_line_max_size_in_bytes.from_env(
        "RYAX_ACTION_LOGS_LINE_MAX_SIZE_IN_BYTES", default=1024, as_=int
    )

    # Grpc server
    container.configuration.grpc_server_port.from_env(
        "RYAX_GRPC_SERVER_PORT", default=8326, as_=int
    )

    # Logging

    str_level = container.configuration.log_level()
    numeric_level = getattr(logging, str_level.strip().upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_level)
    LoggingInstrumentor().instrument(set_logging_format=True, log_level=numeric_level)

    logging.getLogger("ryax").setLevel(numeric_level)
    logger = logging.getLogger(__name__)
    logger.info("Logging level is set to %s" % str_level.upper())

    if (
        container.configuration.site.type() == "SLURM_SSH"
        and container.configuration.resource_recommendation_enabled()
    ):
        logger.error(
            "Recommendation is enabled but this is a SLURM_SSH worker (recommendation is unsupported)."
            "Disabling recommendation!"
        )
        container.configuration.resource_recommendation_enabled.from_value(False)

    container.configuration.enable_memory_profiling.from_env(
        "RYAX_PERFORMANCE_MEMORY_PROFILE_ENABLED", default=False, as_=_env_var_to_bool
    )
    # Only see performance logs (SQL timing of each query, and more) when it crosses a threshold.
    container.configuration.logs.performance.from_env(
        "RYAX_PERFORMANCE_LOGS_LEVEL", default="WARNING"
    )
    logging.getLogger("ryax.performance").setLevel(
        getattr(logging, container.configuration.logs.performance().upper())
    )
    # Disable internal lib debug logs by default
    logging.getLogger("asyncio").setLevel(logging.INFO)
    logging.getLogger("kubernetes_asyncio").setLevel(logging.INFO)
    logging.getLogger("aiormq").setLevel(logging.INFO)

    # WARNING: Only uncomment this in dev.
    # Do not show the configuration because it contains secrets
    # logger.debug(f"CONFIG:\n{yaml.dump(container.configuration())}")

    return container


def init(container: ApplicationContainer) -> web.Application:
    """Init application"""
    application_setup(container)
    messaging_setup(container)

    app: web.Application = web.Application()
    api_setup(app, container)
    app["container"] = container

    return app


@inject
async def on_startup(app: web.Application) -> None:
    """Hooks for application startup"""
    container: ApplicationContainer = app["container"]

    container.opentelemetry().init()
    container.metrics_service().serve()

    container.grpc_server().start()

    await container.messaging_engine().connect()
    container.database_engine().connect()
    await container.messaging_consumer().start(
        "WorkerQ." + container.configuration.site.name()
    )

    if container.configuration.site.type() == "KUBERNETES":
        k8s_engine = container.k8s_engine()
        await k8s_engine.init()

    if container.configuration.storage.local.type() == "s3":
        s3_engine = container.local_s3_engine()
        s3_engine.connect()
        await s3_engine.create_bucket()

    if container.configuration.enable_memory_profiling():
        app["memory_profiler"] = asyncio.create_task(collect_stats())

    await container.message_bus().handle(
        [ApplicationStartupCommand(), ApplicationStartupEvent()]
    )


async def background_tasks(app: web.Application) -> AsyncGenerator:
    container: ApplicationContainer = app["container"]

    async def periodic() -> None:
        while True:
            await asyncio.sleep(30)
            await container.message_bus().handle_event(UpdateRegistrationEvent())

    periodic_task = asyncio.create_task(periodic())

    yield
    periodic_task.cancel()
    await periodic_task


async def on_shutdown(app: web.Application) -> None:
    """Define hook when application stop"""
    container: ApplicationContainer = app["container"]
    await container.message_bus().handle_command(StopApplicationCommand())
    await container.addons().run_hook_on_cleanup()


async def on_cleanup(app: web.Application) -> None:
    """Define hook when application stop"""
    container: ApplicationContainer = app["container"]
    await container.message_bus().stop()
    container.database_engine().disconnect()
    await container.messaging_engine().disconnect()
    await container.metrics_service().stop()
    if container.configuration.site.spec.type() == "KUBERNETES":
        k8s_engine = container.k8s_engine()
        k8s_engine.stop()


async def collect_stats(sample_time_in_sec: int = 300, nb_frame: int = 5) -> None:
    snapshots = []
    mem_logger.warning("Starts memory profiling: DO NOT USE THIS IN PRODUCTION")
    try:
        tracemalloc.start(nb_frame)
        while True:
            try:
                gc.collect()
                snapshots.append(tracemalloc.take_snapshot())
                mem_logger.info(
                    "New snapshot taken for memory profiling: %s",
                    snapshots[-1].statistics("filename", cumulative=True),
                )
                # filters: list[
                #    tracemalloc.Filter
                # ] = []  # [tracemalloc.Filter(inclusive=True, filename_pattern="*requests*")]
                if len(snapshots) > 1:
                    stats = (
                        snapshots[-1]
                        # .filter_traces(filters)
                        .compare_to(snapshots[-2], "filename")
                    )
                    mem_logger.warning(
                        f"Memory profiling for {nb_frame} frame (sampling {sample_time_in_sec}s):"
                    )
                    for stat in stats[:nb_frame]:
                        mem_logger.warning(
                            f"* Diff allocations size: {stat.size_diff / 1024} (Total: {stat.size / 1024}) KiB\n"
                            f"  Diff nb memory blocks: {stat.count_diff}  (Total: {stat.count})"
                        )
                        for line in stat.traceback.format():
                            mem_logger.warning(line)
                mem_logger.info(
                    f"Waiting for {sample_time_in_sec}s before the next snapshot"
                )
                snapshots.pop(0)
                await asyncio.sleep(sample_time_in_sec)
            except Exception:
                mem_logger.exception("Error while tracing memory")
    finally:
        tracemalloc.stop()


def start() -> None:
    """Start application"""
    # Needed to share some object across processes
    multiprocessing.set_start_method("spawn")

    # Do database migration on startup to initialize alembic revision
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.submit(db_migration)

    container = configure()
    logger.info(f"Ryax Worker version: {__version__}")

    app: web.Application = init(container)
    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)
    app.on_cleanup.append(on_cleanup)
    app.cleanup_ctx.append(background_tasks)  # type: ignore
    web.run_app(
        app,
        shutdown_timeout=5,
        port=container.configuration.api.port(),
    )


def db_migration() -> None:
    import alembic.config

    alembic_args = ["--config", "./migrations/worker/alembic.ini", "upgrade", "head"]
    alembic.config.main(argv=alembic_args)
