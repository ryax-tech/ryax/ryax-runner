# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from typing import Callable

from dependency_injector import containers, providers

from ryax.addons.addon_entities import RyaxAddOns
from ryax.common.application.message_bus import MessageBus
from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.common.infrastructure.messaging.querier import RabbitMQMessageQuerier
from ryax.common.infrastructure.monitoring.opentelemetry_tracer import (
    OpenTelemetryTracer,
)
from ryax.worker.application.tasks_service import TasksService
from ryax.worker.domain.ssh.ssh_connection_helper import SSHCredentials
from ryax.worker.infrastructure.deployment.k8s.k8s_recommendation_service import (
    RecommendationService,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.storage.storage_service import IStorageService
from ryax.worker.infrastructure.database.deployment_repository import (
    DatabaseDeploymentRepository,
)
from ryax.worker.infrastructure.database.execution_repository import (
    DatabaseExecutionRepository,
)
from ryax.worker.infrastructure.database.mapper import map_orm
from ryax.worker.infrastructure.database.node_pool_repository import (
    DatabaseNodePoolRepository,
)
from ryax.worker.infrastructure.database.registration_repository import (
    DatabaseRegistrationRepository,
)
from ryax.worker.infrastructure.database.unit_of_work import DatabaseUnitOfWork

# k8s deployment service with grpc
from ryax.worker.infrastructure.deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import K8sEngine

from ryax.worker.infrastructure.grpc.grpc_server_service import GrpcServer

# slurm deployment service with ssh
from ryax.worker.infrastructure.deployment.sshslurm.ssh_slurm_action_deployment_service import (
    SshSlurmActionDeploymentService,
)
from ryax.worker.infrastructure.execution.grpcv1.grpc_v1_execution_service import (
    GRPCV1ExecutionService,
)
from ryax.worker.infrastructure.execution.ssh_slurmv1.ssh_slurm_v1_execution_service import (
    SSHV1ExecutionService,
)
from ryax.common.infrastructure.messaging.consumer import RabbitMQMessageConsumer
from ryax.common.infrastructure.messaging.engine import MessagingEngine
from ryax.common.infrastructure.messaging.publisher import RabbitMQMessagePublisher
from ryax.common.infrastructure.monitoring.prometheus_server import (
    PrometheusMetricsServer,
)
from ryax.common.infrastructure.storage.posix.posix_storage_service import (
    PosixStorageService,
)
from ryax.common.infrastructure.storage.s3.s3_engine import S3Engine
from ryax.common.infrastructure.storage.s3.s3_service import S3Storage

logger = getLogger(__name__)


class ApplicationContainer(containers.DeclarativeContainer):
    """Application container for dependency injection"""

    # Define configuration provider
    configuration = providers.Configuration()

    # Application
    # Internal bus
    message_bus: providers.Singleton[IMessageBus] = providers.Singleton(
        MessageBus, keep_event_history=configuration.keep_event_history
    )

    # Infrastructure
    # Messaging
    messaging_engine = providers.Singleton(
        MessagingEngine, connection_url=configuration.messaging.url
    )
    messaging_consumer: providers.Singleton[
        RabbitMQMessageConsumer
    ] = providers.Singleton(RabbitMQMessageConsumer, engine=messaging_engine)
    messaging_publisher = providers.Singleton(
        RabbitMQMessagePublisher, engine=messaging_engine, base_routing_key="Worker"
    )
    message_querier = providers.Singleton(
        RabbitMQMessageQuerier, engine=messaging_engine, base_routing_key="Worker"
    )

    # Storage
    local_posix_storage = providers.Singleton(
        PosixStorageService,
        root_path=configuration.storage.local.posix.root_path,
    )
    local_s3_engine = providers.Singleton(
        S3Engine,
        connection_url=configuration.storage.local.s3.server,
        bucket=configuration.storage.local.s3.bucket,
        access_key=configuration.storage.local.s3.access_key,
        secret_key=configuration.storage.local.s3.secret_key,
    )
    local_s3_storage = providers.Factory(
        S3Storage,
        engine=local_s3_engine,
    )
    # Storage Services
    local_storage_service: Callable[[], IStorageService] = providers.Selector(
        configuration.storage.local.type,
        posix=local_posix_storage,
        s3=local_s3_storage,
    )
    # Kubernetes
    k8s_engine = providers.Singleton(
        K8sEngine,
        user_namespace=configuration.site.spec.namespace,
        message_bus=message_bus,
        loki_url=configuration.deployment.logs.loki.api_url,
        log_query_interval_in_seconds=configuration.deployment.logs.query_interval_in_seconds,
    )

    # Action execution
    tasks_service = providers.Singleton(
        TasksService,
    )

    recommendation_service = providers.Singleton(
        RecommendationService,
        k8s_engine=k8s_engine,
        site_name=configuration.site.name,
    )

    grpc_v1_execution_service = providers.Factory(
        GRPCV1ExecutionService,
        message_bus=message_bus,
        storage_service=local_storage_service,
        tasks_service=tasks_service,
    )
    ssh_v1_execution_service = providers.Factory(
        SSHV1ExecutionService,
        message_bus=message_bus,
        storage_service=local_storage_service,
        tasks_service=tasks_service,
        ryax_cache_dir=configuration.site.spec.cache_dir,
        default_credentials=configuration.site.spec.credentials.as_(
            SSHCredentials.from_dict
        ),
    )
    # Be careful, all of these services are factories: everytime you get them from the FactoryAggregate you get a new
    # instance. Which means that you can't store state between 2 calls of a functions, i.e. consider all methods to be
    # static  (except for the parameters given below, at initialization).
    execution_service_factory = providers.FactoryAggregate(  # type: ignore
        GRPC_V1=grpc_v1_execution_service,
        SLURM_SSH_V1=ssh_v1_execution_service,
    )

    # Deployment
    ssh_slurm_deployment_service = providers.Factory(
        SshSlurmActionDeploymentService,
        tasks_service=tasks_service,
        message_bus=message_bus,
        ryax_cache_dir=configuration.site.spec.cache_dir,
        credentials=configuration.site.spec.credentials.as_(SSHCredentials.from_dict),
        build_with_fakeroot=configuration.site.spec.build_with_fakeroot,
        internal_registry_override=configuration.deployment.internal_registry_override,
    )
    # Be careful, all of these services are factories: everytime you get them from the FactoryAggregate you get a new
    # instance. Which means that you can't store state between 2 calls of a functions, i.e. consider all methods to be
    # static (except for the parameters given below, at initialization).
    action_deployment_factory = providers.FactoryAggregate(  # type: ignore
        k8s=providers.Factory(
            K8SActionDeploymentService,
            engine=k8s_engine,
            tasks_service=tasks_service,
            default_grpc_port_inside_actions=configuration.actions.grpc_port,
            site_name=configuration.site.name,
            recommendation_service=recommendation_service,
            resource_recommendation_enabled=configuration.resource_recommendation_enabled,
            action_pull_secret_name=configuration.actions.pull_secret,
        ),
        sshslurm=ssh_slurm_deployment_service,
    )

    # Database
    deployment_repository = providers.Factory(
        DatabaseDeploymentRepository,
    )
    execution_repository = providers.Factory(
        DatabaseExecutionRepository,
    )
    registration_repository = providers.Factory(DatabaseRegistrationRepository)
    node_pool_repository = providers.Factory(DatabaseNodePoolRepository)
    database_engine = providers.Singleton(
        SqlalchemyDatabaseEngine,
        connection_url=configuration.database.url,
        create_mapping=map_orm,
    )
    unit_of_work: providers.Factory[DatabaseUnitOfWork] = providers.Factory(
        DatabaseUnitOfWork,
        engine=database_engine,
        deployment_repository_factory=deployment_repository.provider,
        execution_repository_factory=execution_repository.provider,
        registration_repository_factory=registration_repository.provider,
        node_pool_repository_factory=node_pool_repository.provider,
    )
    # addons
    addons: providers.Singleton[RyaxAddOns] = providers.Singleton(
        RyaxAddOns,
    )

    # Monitoring
    metrics_service = providers.Singleton(
        PrometheusMetricsServer,
        metrics_port=configuration.observabiliy.metrics_server_port,
    )
    opentelemetry = providers.Singleton(
        OpenTelemetryTracer,
        open_telemetry_collector_endpoint=configuration.observabiliy.otlp_endpoint,
        service_name=configuration.site.name,
    )

    # GRPC Server
    grpc_server = providers.Singleton(
        GrpcServer,
        message_bus=message_bus,
        server_port=configuration.grpc_server_port,
    )
