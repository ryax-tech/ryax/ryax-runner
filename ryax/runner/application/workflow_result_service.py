import json
from dataclasses import dataclass
from json import JSONDecodeError
from typing import Any, Dict, List, Union

from ryax.runner.application.workflow_run_service import WorkflowRunService
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_result.execution_result_execptions import (
    ExecutionResultNotFoundError,
)
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.stored_file.stored_file_service import IStoredFileView
from ryax.runner.domain.workflow_result.workflow_result import WorkflowResultView


@dataclass
class WorkflowResultService:
    uow: IUnitOfWork
    workflow_run_service: WorkflowRunService
    stored_file_service: IStoredFileView

    @staticmethod
    def _convert_to_json(workflow_results: List[WorkflowResultView]) -> Dict[str, Any]:
        workflow_results_dict = {
            result.key: result.value for result in workflow_results
        }
        for result in workflow_results:
            try:
                workflow_results_dict[result.key] = json.loads(result.value)
            except (JSONDecodeError, TypeError):
                pass
        return workflow_results_dict

    async def get(
        self, workflow_run_id: str, project_id: str
    ) -> Union[dict[str, Any], StoredFile]:
        """Returns a json of results, or a StoredFile file results"""

        self.workflow_run_service.check_workflow_run_result_state(
            workflow_run_id, project_id
        )
        with self.uow as uow:
            try:
                results = (
                    uow.execution_result_repository.get_results_by_workflow_run_id(
                        workflow_run_id
                    )
                )
            except ExecutionResultNotFoundError:
                return {}
            if len(results) == 0:
                return {}
            if any(not result.type.is_file_type() for result in results):
                # FIXME: This is a hack to convert strings into JSON in the results. Remove when Object/JSON Type is supported
                return self._convert_to_json(results)
            else:
                if len(results) > 1:
                    zipped_stored_file = await self.stored_file_service.zip(
                        [
                            self.stored_file_service.get_by_file_path(
                                result.value, project_id
                            )
                            for result in results
                        ]
                    )
                    return zipped_stored_file
                else:
                    return self.stored_file_service.get_by_file_path(
                        results[0].value, project_id
                    )
