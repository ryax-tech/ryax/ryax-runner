import logging
from dataclasses import dataclass

from ryax.addons.addon_entities import RyaxAddOns
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_exceptions import (
    UnresolvedReferenceInputs,
)

logger = logging.getLogger(__name__)


@dataclass
class ReferenceSolverService:
    uow: IUnitOfWork
    addons: RyaxAddOns

    def filter_inputs(
        self, workflow_action: WorkflowAction, static_inputs: dict
    ) -> None:
        """
        Filter addons inputs that should not be injected from the static inputs. Modify in place
        """
        not_injectable_inputs = self.addons.get_addons_not_injectable_inputs(
            workflow_action
        )
        to_delete = []
        for input_name in static_inputs.keys():
            if input_name in not_injectable_inputs:
                to_delete.append(input_name)
        for input_to_del in to_delete:
            del static_inputs[input_to_del]

    def resolve_reference_inputs(
        self,
        parent_execution_result: ExecutionResult | None,
        workflow_action: WorkflowAction,
    ) -> dict[str, str | int | float | bool | bytes]:
        """
        Resolve the workflow action inputs using parent executions outputs and static values
        :param parent_execution_result:
        :param workflow_action:
        :return:
        """
        static_inputs = {
            input.name: input.value for input in workflow_action.static_inputs
        }
        previous_results = (
            parent_execution_result.get_all_previous_outputs()
            if parent_execution_result
            else []
        )

        for ref_input in workflow_action.reference_inputs:
            for parent_output in previous_results:
                if (
                    ref_input.output_name == parent_output.name
                    and ref_input.action_name == parent_output.workflow_action_name
                ):
                    static_inputs[ref_input.name] = parent_output.value
                    break
            else:
                # reference was not found, either it's optional or there's a problem
                input_definition = workflow_action.definition.get_input_type(
                    ref_input.name
                )
                if input_definition.optional is False:
                    raise UnresolvedReferenceInputs(
                        "Error while resolving reference input for run '{}', not optional and no reference found for {}".format(
                            parent_execution_result.workflow_run_id
                            if parent_execution_result
                            else "NOT DEFINED YET",
                            ref_input,
                        )
                    )
        logger.debug(
            "Resolved inputs for workflow action '%s': %s",
            workflow_action.id,
            static_inputs,
        )
        return static_inputs

    def resolve_addons(
        self,
        workflow_action_id: str | None = None,
        execution_connection_id: str | None = None,
    ) -> dict[str, dict[str, str | int | float | bool | bytes]]:
        """Resolve the addons"""
        with self.uow as uow:
            if execution_connection_id is not None:
                # This is happening during the workflow execution uses to reference value
                execution_connection = uow.execution_connection_repository.get(
                    execution_connection_id, eager_load=True
                )
                parent_execution = execution_connection.parent_execution_result
                workflow_action = execution_connection.workflow_action
                resolved_inputs_values = self.resolve_reference_inputs(
                    parent_execution, workflow_action
                )
            else:
                # This is happening during the workflow creation and only static values are used
                assert workflow_action_id is not None
                workflow_action = uow.workflow_repository.get_workflow_action_by_id(
                    workflow_action_id
                )
                resolved_inputs_values = {
                    input.name: input.value for input in workflow_action.static_inputs
                }

            resolved_addons: dict[str, dict[str, str | int | float | bool | bytes]] = {}
            for addon_def in workflow_action.definition.addons_inputs:
                assert addon_def.addon_name is not None
                if addon_def.name in resolved_inputs_values:
                    if addon_def.addon_name not in resolved_addons:
                        resolved_addons[addon_def.addon_name] = {}
                    resolved_addons[addon_def.addon_name][
                        addon_def.name
                    ] = resolved_inputs_values[addon_def.name]
            logger.debug(
                "Resolved addons for execution connection %s: %s",
                execution_connection_id,
                resolved_addons,
            )
            return resolved_addons
