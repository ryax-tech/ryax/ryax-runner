import logging
from dataclasses import dataclass

from ryax.runner.domain.accounting.accounting_entities import AccountingEntry
from ryax.runner.domain.common.unit_of_work import IUnitOfWork

logger = logging.getLogger(__name__)


@dataclass
class AccountingService:
    uow: IUnitOfWork

    def list_accounting(
        self,
        execution_ids: list[str],
        project_ids: list[str],
        action_ids: list[str],
        workflow_ids: list[str],
        run_ids: list[str],
    ) -> list[AccountingEntry]:
        with self.uow:
            accounting = self.uow.accounting_repository.list_accounting(
                execution_ids=execution_ids,
                action_ids=action_ids,
                workflow_ids=workflow_ids,
                project_ids=project_ids,
                run_ids=run_ids,
            )
        return accounting

    async def update_entry(self, execution_id: str) -> None:
        with self.uow:
            # build/get accounting entry
            execution_connection = self.uow.execution_connection_repository.get(
                execution_id, eager_load=True
            )
            accounting = self.uow.accounting_repository.get(execution_id)
            if not accounting:
                # first entry
                accounting = AccountingEntry(
                    execution_connection_id=execution_id,
                    project_id=execution_connection.project_id,
                    workflow_definition_id=execution_connection.workflow_action.workflow_definition_id,
                    action_definition_id=execution_connection.workflow_action.definition.id,
                    workflow_run_id=execution_connection.workflow_run_id,
                    submitted_at=execution_connection.submitted_at,
                    started_at=execution_connection.started_at,
                    ended_at=execution_connection.ended_at,
                )
                if execution_connection.action_deployment:
                    accounting.instance_type = (
                        execution_connection.action_deployment.node_pool.instance_type
                    )
                if execution_connection.workflow_action.definition.resources:
                    accounting.requested_cpu = (
                        execution_connection.workflow_action.definition.resources.cpu
                    )

                    accounting.requested_memory = (
                        execution_connection.workflow_action.definition.resources.memory
                    )

                    accounting.requested_time = (
                        execution_connection.workflow_action.definition.resources.time
                    )

                    accounting.requested_gpu = (
                        execution_connection.workflow_action.definition.resources.gpu
                    )

                logger.info("Create accounting of execution %s", execution_id)
                self.uow.accounting_repository.add(accounting)
            else:
                # update entry (end of run)
                logger.info("Update accounting of execution %s", execution_id)
                accounting.ended_at = execution_connection.ended_at
            self.uow.commit()
