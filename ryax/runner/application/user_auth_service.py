from datetime import datetime, timezone
from typing import List

from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.encryption.encryption_service import IEncryptionService
from ryax.runner.domain.user_auth.api_key.api_key_entitites import UserApiKeyStatus
from ryax.runner.domain.user_auth.api_key.user_api_key_exceptions import (
    UserApiKeyNotPresentInHeaderException,
)
from ryax.runner.domain.user_auth.user_auth_entities import (
    ProjectSecurityRequirement,
    UserAuthorizationMethod,
)
from ryax.runner.domain.user_auth.user_auth_exceptions import (
    UserAuthUnauthorizedException,
)


class UserAuthService:
    def __init__(self, uow: IUnitOfWork, encryption_service: IEncryptionService):
        self.uow = uow
        self.encryption_service = encryption_service

    def authenticate(self, project_id: str, headers: dict) -> None:
        project_authorization_methods = self.get_project_authorization_methods(
            project_id
        )
        for auth_method in project_authorization_methods:
            if (
                auth_method.authorization_method == UserAuthorizationMethod.APIKEY
                and auth_method.enabled
            ):
                try:
                    api_key = headers["X-API-KEY"]
                    self.check_api_key(api_key, project_id)
                except KeyError:
                    raise UserApiKeyNotPresentInHeaderException()

    def get_project_authorization_methods(
        self, project_id: str
    ) -> List[ProjectSecurityRequirement]:
        with self.uow as uow:
            return uow.user_api_key_repository.list_auth_methods_for_project(project_id)

    def check_api_key(self, api_key: str, project_id: str) -> None:
        with self.uow as uow:
            api_keys = uow.user_api_key_repository.get_all_keys(project_id)
            for user_api_key in api_keys:
                if user_api_key.status == UserApiKeyStatus.VALID.value:
                    decrypted_key = self.encryption_service.decrypt(
                        user_api_key.api_key
                    )
                    if api_key == decrypted_key:
                        user_api_key.last_used_date = datetime.now(timezone.utc)
                        uow.commit()
                        return
            else:
                raise UserAuthUnauthorizedException()

    def get_all_auth_methods(self, project_id: str) -> dict:
        """
        This method is highly specialized for the moment and
        we are doing some data conversion here instead of directly
        from controller to schema
        This is because we expect the spec to change so better not to create too much extra code
        """
        authorization_methods = {}
        with self.uow as uow:
            auth_requirements = (
                uow.user_api_key_repository.list_auth_methods_for_project(project_id)
            )

            # For now there is only API key. If not api key, return the blank template
            for requirement in auth_requirements:
                if requirement.authorization_method == UserAuthorizationMethod.APIKEY:
                    api_key_requirement = requirement
                    break
            else:
                return {"api_key": {"enabled": False, "keys": []}}

        api_keys = uow.user_api_key_repository.get_all_api_keys_for_project(project_id)
        keys: list = []
        for api_key in api_keys:
            keys.append(
                {
                    "id": api_key.id,
                    "name": api_key.name,
                    "creation_date": api_key.creation_date.isoformat(),
                    "last_use_date": (
                        api_key.last_used_date.isoformat()
                        if api_key.last_used_date is not None
                        else None
                    ),
                    "expiration_date": api_key.expiration_date.isoformat(),
                    "status": api_key.status,
                }
            )
        authorization_methods["api_key"] = {
            "enabled": api_key_requirement.enabled,
            "keys": sorted(keys, key=lambda x: x["creation_date"], reverse=True),
        }

        return authorization_methods
