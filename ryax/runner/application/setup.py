# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

from ryax.runner.application.handlers import (
    action_definition_handlers,
    action_deployment_handlers,
    execution_connection_handlers,
    execution_result_handlers,
    file_storage_handlers,
    scheduler_handler,
    workflow_deployment_handlers,
    workflow_run_handlers,
    site_handlers,
    recommendation_vpa_handlers,
    accounting_handlers,
)
from ryax.common.application.message_bus import HandlerType
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_commands import (
    CreateActionDefinitionCommand,
    DeleteActionDefinitionCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    CreateActionDeploymentCommand,
    UndeployActionDeploymentCommand,
    WatchActionDeploymentStateCommand,
    UpdateActionDeploymentCommand,
    ErrorActionDeploymentCommand,
    SetRunningActionDeploymentCommand,
    DeallocateActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentAppliedEvent,
    ActionDeploymentAvailableEvent,
    ActionDeploymentIsReadyEvent,
    ActionDeploymentNewLogLinesEvent,
    ActionDeploymentUndeployedEvent,
    ActionDeploymentUndeployingEvent,
    ActionDeploymentResourcesAvailableEvent,
    ActionDeploymentNoMoreResourcesAvailableEvent,
    ActionDeploymentCreatedEvent,
    ActionDeploymentIsDeployingEvent,
    ActionDeploymentNewLogLinesAddedEvent,
    ActionDeploymentErroredEvent,
    ActionDeploymentIsRunningEvent,
)
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    AllocateExecutionConnectionCommand,
    CreateExecutionConnectionCommand,
    InitializeExecutionResultCommand,
    StartExecutionConnectionCommand,
    StopExecutionConnectionCommand,
    DeallocateExecutionConnectionCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionEndedEvent,
    ExecutionConnectionPendingEvent,
    ExecutionConnectionUpdatedEvent,
    ExecutionConnectionErroredEvent,
    ExecutionConnectionRetryIfNeededEvent,
)
from ryax.runner.domain.execution_result.execution_result_commands import (
    TriggerNextExecutionCommand,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultNewLogLinesToBeAddedEvent,
    ExecutionResultCompletedEvent,
    ExecutionResultFinishedEvent,
    ExecutionResultInitializedEvent,
    ExecutionResultNewLogLinesAddedEvent,
)
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupEvent,
    StopApplicationCommand,
)
from ryax.runner.domain.site.site_events import RegisterSiteCommand
from ryax.runner.domain.stored_file.stored_file_commands import CreateStoredFileCommand
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
    DeleteWorkflowDeploymentCommand,
    StartWorkflowDeploymentCommand,
    StopWorkflowDeploymentCommand,
    StopWorkflowDeploymentFromDefinitionIdCommand,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_events import (
    WorkflowDeployedSuccessfully,
    WorkflowStoppedEvent,
)
from ryax.runner.domain.workflow_run.workflow_run_commands import (
    CreateWorkflowRunCommand,
    DeleteWorkflowRunsCommand,
)
from ryax.runner.domain.workflow_run.workflow_run_events import (
    WorkflowRunIsCompletedEvent,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_events import (
    RecommendationsVPAUpdateCommand,
)

logger = logging.getLogger(__name__)


def setup(container: ApplicationContainer) -> None:
    """Method to setup addons and messaging mapper (event type/messages mapping)"""

    # Load addons
    # Taken from the official documentation:
    # https://packaging.python.org/en/latest/guides/creating-and-discovering-plugins/#using-namespace-packages
    # def iter_namespace(ns_pkg: ModuleType) -> Iterator[pkgutil.ModuleInfo]:
    #    # Specifying the second argument (prefix) to iter_modules makes the
    #    # returned name an absolute name instead of a relative one. This allows
    #    # import_module to work without having to do additional modification to
    #    # the name.
    #    return pkgutil.iter_modules(ns_pkg.__path__, ns_pkg.__name__ + ".")

    # discovered_addons = {
    #    name: importlib.import_module(name)
    #    for finder, name, ispkg in iter_namespace(ryax.addons)
    # }

    # Allow dependencies injection into addons
    # container.wire(modules=discovered_addons.values())

    container.wire(
        modules=[
            action_definition_handlers,
            action_deployment_handlers,
            execution_connection_handlers,
            execution_result_handlers,
            workflow_deployment_handlers,
            file_storage_handlers,
            workflow_run_handlers,
            scheduler_handler,
            site_handlers,
            recommendation_vpa_handlers,
            accounting_handlers,
        ]
    )
    message_bus = container.message_bus()

    # Action Definition
    # Commands
    message_bus.register_command_handler(
        CreateActionDefinitionCommand, action_definition_handlers.create
    )
    message_bus.register_command_handler(
        DeleteActionDefinitionCommand, action_definition_handlers.delete
    )
    # Action deployments
    # Register command handlers
    message_bus.register_command_handler(
        CreateActionDeploymentCommand, action_deployment_handlers.create
    )
    message_bus.register_command_handler(
        WatchActionDeploymentStateCommand, action_deployment_handlers.watch
    )
    message_bus.register_command_handler(
        UpdateActionDeploymentCommand, action_deployment_handlers.update
    )
    message_bus.register_command_handler(
        SetRunningActionDeploymentCommand, action_deployment_handlers.set_running
    )
    message_bus.register_command_handler(
        DeallocateActionDeploymentCommand, action_deployment_handlers.deallocate
    )
    message_bus.register_command_handler(
        UndeployActionDeploymentCommand, action_deployment_handlers.delete
    )
    message_bus.register_command_handler(
        StopApplicationCommand, action_deployment_handlers.unwatch_all
    )

    # Register event handlers
    message_bus.register_event_handler(
        ApplicationStartupEvent, action_deployment_handlers.on_startup
    )
    message_bus.register_event_handler(
        ActionDeploymentCreatedEvent, action_deployment_handlers.apply
    )
    message_bus.register_event_handler(
        ActionDeploymentIsDeployingEvent, action_deployment_handlers.do_apply
    )
    message_bus.register_event_handler(
        ActionDeploymentAppliedEvent, action_deployment_handlers.on_applied
    )
    message_bus.register_event_handler(
        ActionDeploymentNoMoreResourcesAvailableEvent,
        action_deployment_handlers.no_more_resource_timeout,
    )
    message_bus.register_event_handler(
        ActionDeploymentIsReadyEvent, action_deployment_handlers.on_is_ready
    )
    message_bus.register_event_handler(
        ActionDeploymentUndeployingEvent, action_deployment_handlers.on_undeploying
    )
    message_bus.register_event_handler(
        ActionDeploymentUndeployedEvent, action_deployment_handlers.is_stopped
    )

    # Action executions
    # Commands
    message_bus.register_command_handler(
        CreateExecutionConnectionCommand, execution_connection_handlers.create
    )
    message_bus.register_command_handler(
        ErrorActionDeploymentCommand, action_deployment_handlers.on_error
    )
    message_bus.register_command_handler(
        StopExecutionConnectionCommand, execution_connection_handlers.stop
    )
    message_bus.register_command_handler(
        InitializeExecutionResultCommand, execution_connection_handlers.initialize
    )
    message_bus.register_command_handler(
        StopApplicationCommand, execution_connection_handlers.stop_all
    )
    # Execution Results command
    message_bus.register_event_handler(
        ExecutionResultFinishedEvent,
        execution_result_handlers.finish,
    )
    message_bus.register_command_handler(
        AllocateExecutionConnectionCommand, execution_connection_handlers.allocate
    )
    message_bus.register_command_handler(
        DeallocateExecutionConnectionCommand, execution_connection_handlers.deallocate
    )
    message_bus.register_command_handler(
        StartExecutionConnectionCommand, execution_connection_handlers.start
    )
    # Events
    message_bus.register_event_handler(
        ApplicationStartupEvent, execution_connection_handlers.on_startup
    )
    message_bus.register_event_handler(
        ExecutionConnectionEndedEvent, execution_connection_handlers.on_ended
    )
    message_bus.register_event_handler(
        ExecutionConnectionErroredEvent, execution_connection_handlers.on_error
    )
    message_bus.register_event_handler(
        ActionDeploymentNewLogLinesEvent,
        execution_connection_handlers.on_new_execution_log_line,
    )
    message_bus.register_event_handler(
        ExecutionResultNewLogLinesToBeAddedEvent,
        execution_result_handlers.on_new_immediate_execution_log_line,
    )
    message_bus.register_event_handler(
        ExecutionConnectionRetryIfNeededEvent,
        execution_connection_handlers.on_execution_connection_retry_if_needed,
    )
    # Workflow deployments
    # Commands
    message_bus.register_command_handler(
        CreateWorkflowDeploymentCommand,
        workflow_deployment_handlers.create,
    )
    message_bus.register_command_handler(
        StartWorkflowDeploymentCommand,
        workflow_deployment_handlers.start,
    )
    message_bus.register_command_handler(
        TriggerNextExecutionCommand,
        workflow_deployment_handlers.trigger_next_execution,
    )
    message_bus.register_command_handler(
        StopWorkflowDeploymentCommand,
        workflow_deployment_handlers.stop,
    )
    message_bus.register_command_handler(
        StopWorkflowDeploymentFromDefinitionIdCommand,
        workflow_deployment_handlers.stop,
    )
    message_bus.register_command_handler(
        DeleteWorkflowDeploymentCommand,
        workflow_deployment_handlers.delete,
    )
    message_bus.register_command_handler(
        CreateStoredFileCommand,
        file_storage_handlers.create,
    )
    message_bus.register_event_handler(
        ApplicationStartupEvent, workflow_deployment_handlers.on_startup
    )
    message_bus.register_command_handler(
        StopApplicationCommand, workflow_deployment_handlers.pause_all
    )

    # Events
    message_bus.register_event_handler(
        WorkflowDeployedSuccessfully, workflow_deployment_handlers.is_started
    )
    message_bus.register_event_handler(
        WorkflowStoppedEvent,
        workflow_deployment_handlers.on_stopped,
    )

    # scheduler
    message_bus.register_event_handler(
        ExecutionConnectionPendingEvent, scheduler_handler.on_connection_is_pending
    )
    message_bus.register_event_handler(
        ActionDeploymentAvailableEvent,
        scheduler_handler.on_action_deployment_is_available,
    )
    message_bus.register_event_handler(
        ActionDeploymentIsRunningEvent,
        scheduler_handler.on_action_deployment_is_running,
    )
    message_bus.register_event_handler(
        ActionDeploymentUndeployedEvent,
        scheduler_handler.on_action_deployment_undeployed,
    )
    message_bus.register_event_handler(
        WorkflowStoppedEvent, scheduler_handler.on_workflow_stopped
    )
    message_bus.register_event_handler(
        ActionDeploymentResourcesAvailableEvent,
        scheduler_handler.on_resources_available,
    )
    message_bus.register_event_handler(
        ActionDeploymentNoMoreResourcesAvailableEvent,
        scheduler_handler.on_no_more_resources_available,
    )
    message_bus.register_event_handler(
        ActionDeploymentErroredEvent,
        scheduler_handler.deployment_error,
    )

    # workflow run
    message_bus.register_command_handler(
        CreateWorkflowRunCommand, workflow_run_handlers.create
    )
    message_bus.register_command_handler(
        DeleteWorkflowRunsCommand, workflow_run_handlers.delete
    )
    message_bus.register_event_handler(
        ExecutionResultInitializedEvent,
        workflow_run_handlers.on_execution_result_created,
    )
    message_bus.register_event_handler(
        ExecutionConnectionPendingEvent,
        workflow_run_handlers.on_execution_connection_created,
    )
    message_bus.register_event_handler(
        ExecutionConnectionUpdatedEvent,
        workflow_run_handlers.on_execution_connection_updated,
    )
    message_bus.register_event_handler(
        ExecutionResultCompletedEvent,
        workflow_run_handlers.on_execution_result_completed,
    )
    message_bus.register_event_handler(
        WorkflowStoppedEvent, workflow_run_handlers.on_stop
    )
    message_bus.register_event_handler(
        ExecutionConnectionUpdatedEvent,
        accounting_handlers.on_execution_updated,
    )

    # Site
    message_bus.register_command_handler(
        RegisterSiteCommand, site_handlers.on_register_or_update
    )

    # VPA Recommendation
    message_bus.register_command_handler(
        RecommendationsVPAUpdateCommand,
        recommendation_vpa_handlers.on_recommendations_vpa_update,
    )

    # FIXME make this setup addons work!
    # Change this to load from the registry
    # discovered_addons = {
    #     name: importlib.import_module("ryax.addons." + name)
    #     for finder, name, ispkg
    #     in pkgutil.iter_modules([str(Path(ryax.addons.__file__).parent)])
    #     if name.startswith('ryax_')
    # }
    # for addon_name, addon_module in discovered_addons.items():
    #     logger.info(f"Registering addon: {addon_name} with module {addon_module}")
    #     addon_module.init_addon(container.addons())

    from ryax.addons import ryax_http_service

    ryax_http_service.HttpServiceAddOn(container.addons())

    from ryax.addons import ryax_http_api

    ryax_http_api.HttpAPIAddOn(container.addons())

    from ryax.addons import ryax_hpc

    ryax_hpc.HpcServiceAddOn(container.addons())

    from ryax.addons import ryax_kubernetes

    ryax_kubernetes.KubernetesAddOn(container.addons())

    # Add an empty handler to avoid Warnings when no listener is attached
    async def empty(event: HandlerType) -> None:
        pass

    message_bus.register_event_handler(ExecutionResultNewLogLinesAddedEvent, empty)
    message_bus.register_event_handler(WorkflowRunIsCompletedEvent, empty)
    message_bus.register_event_handler(ActionDeploymentNewLogLinesAddedEvent, empty)
