import secrets
from datetime import datetime, timezone
from typing import Optional

from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.encryption.encryption_service import IEncryptionService
from ryax.runner.domain.user_auth.api_key.api_key_entitites import (
    DecryptedUserApiKey,
    UserApiKey,
)
from ryax.runner.domain.user_auth.api_key.user_api_key_exceptions import (
    UserApiKeyDateInvalidException,
)
from ryax.runner.domain.user_auth.user_auth_entities import (
    ProjectSecurityRequirement,
    UserAuthorizationMethod,
)


class UserApiKeyService:
    def __init__(self, uow: IUnitOfWork, encryption_service: IEncryptionService):
        self.encryption_service = encryption_service
        self.uow = uow

    def add(
        self, name: str, project_id: str, expiration_date: Optional[datetime] = None
    ) -> DecryptedUserApiKey:
        if (
            expiration_date is not None
            and datetime.now(tz=timezone.utc) > expiration_date
        ):
            raise UserApiKeyDateInvalidException()
        with self.uow as uow:
            uow.user_api_key_repository.ensure_name_does_not_exist(name, project_id)
            new_api_key = secrets.token_urlsafe()
            encrypted_api_key = self.encryption_service.encrypt(new_api_key)
            new_user_api_key = UserApiKey.create_from_name_project_key(
                name=name,
                project_id=project_id,
                api_key=encrypted_api_key,
                expiration_date=expiration_date,
            )
            uow.user_api_key_repository.add_api_key(new_user_api_key)
            uow.commit()
            return DecryptedUserApiKey(
                id=new_user_api_key.id,
                name=new_user_api_key.name,
                api_key=new_api_key,
                expiration_date=new_user_api_key.expiration_date,
            )

    def delete(self, user_api_key_id: str, current_project: str) -> None:
        with self.uow as uow:
            user_api_key = uow.user_api_key_repository.get(
                user_api_key_id, current_project
            )
            uow.user_api_key_repository.delete(user_api_key)
            uow.commit()

    def revoke(self, user_api_key_id: str, project_id: str) -> None:
        with self.uow as uow:
            user_api_key = uow.user_api_key_repository.get(user_api_key_id, project_id)
            user_api_key.revoke()
            uow.commit()

    def toggle(self, project_id: str) -> None:
        with self.uow as uow:
            auth_requirements = (
                uow.user_api_key_repository.list_auth_methods_for_project(project_id)
            )
            for requirement in auth_requirements:
                if requirement.authorization_method == UserAuthorizationMethod.APIKEY:
                    api_key_requirement = requirement
                    api_key_requirement.enabled = not api_key_requirement.enabled
                    break
            else:
                new_api_key_requirement = (
                    ProjectSecurityRequirement.from_project_auth_method(
                        project_id=project_id,
                        authorization_method=UserAuthorizationMethod.APIKEY,
                    )
                )
                uow.user_api_key_repository.add_project_security_requirement(
                    new_api_key_requirement
                )
            uow.commit()
