from dataclasses import dataclass

from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.site.site_entities import Site


@dataclass
class SiteService:
    uow: IUnitOfWork

    def list(self) -> list[Site]:
        with self.uow as uow:
            return uow.site_repository.list()
