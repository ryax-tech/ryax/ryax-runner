import logging
from dataclasses import dataclass

from ryax.runner.domain.scheduler.scheduler_entities import (
    PendingExecutionConnection,
)
from ryax.runner.domain.scheduler.scheduler_exceptions import (
    NoResourcesFoundWithConstraints,
    NotEnoughResourcesException,
)
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.common.domain.registration.registration_values import (
    NodeArchitecture,
    SiteType,
    GPU_MODE_FULL,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_entities import (
    RecommendationVPA,
)

logger = logging.getLogger(__name__)


@dataclass
class SitePlacementService:
    policy: str = "first_fit"

    @staticmethod
    def _resolve_placement_constraints(
        list_constraints: list[Site] | None,
        type_constraints: list[SiteType] | None,
        sites: list[Site],
    ) -> list[Site]:
        """
        Return a list of sites that fit the constraints

        All constraints are applied with a logical OR
        """
        site_that_fit: list[Site] = []

        if not list_constraints:
            if not type_constraints:
                return sites
            else:
                for site in sites:
                    if site.type in type_constraints:
                        site_that_fit.append(site)
                        logger.debug(
                            f"Found valid site type constraint for site {site.id}"
                        )
        else:
            for constraint_site in list_constraints:
                for site in sites:
                    if constraint_site.id == site.id and (
                        not type_constraints or site.type in type_constraints
                    ):
                        site_that_fit.append(site)
                        logger.debug(f"Found valid site constraint for site {site.id}")

        if len(site_that_fit) == 0:
            raise NoResourcesFoundWithConstraints(
                f"No site fits the constraints found for site name '{list_constraints}' and site type: {type_constraints}"
            )

        return site_that_fit

    @staticmethod
    def _resolve_architecture_constraints(
        architecture_constraints: list[NodeArchitecture] | None,
        node_pools: list[NodePool],
    ) -> list[NodePool]:
        if not architecture_constraints:
            return node_pools

        node_pool_that_fit: list[NodePool] = []
        for node_pool in node_pools:
            if node_pool.has_architecture(architecture_constraints):
                node_pool_that_fit.append(node_pool)
        return node_pool_that_fit

    @staticmethod
    def _score_on_objectives(
        to_schedule: PendingExecutionConnection,
        valid_node_pools: list[NodePool],
    ) -> dict[str, float]:
        objectives = to_schedule.objectives
        node_pool_performance_score = to_schedule.node_pool_objective_score

        scores: dict[str, float] = {node_pool.id: 0 for node_pool in valid_node_pools}
        if objectives is None:
            return scores

        node_pool_performance_score_filtered = (
            {
                key: value
                for key, value in node_pool_performance_score.items()
                if key in [node_pool.id for node_pool in valid_node_pools]
            }
            if node_pool_performance_score
            else {}
        )
        if len(node_pool_performance_score_filtered) != len(valid_node_pools):
            logger.info("Do not use Performance Score if not set for all node pools")
            node_pool_performance_score_filtered = {}

        for objective, level in objectives.items():
            for node_pool in valid_node_pools:
                if node_pool_performance_score_filtered.get(node_pool.id):
                    logger.debug(
                        "Use the node pool performance score computed for this action id: %s",
                        to_schedule.id,
                    )
                    scores[node_pool.id] = (
                        scores[node_pool.id]
                        + node_pool_performance_score_filtered[node_pool.id]
                    )
                else:
                    logger.debug(
                        "Use the generic node pool objective score for action id: %s",
                        to_schedule.id,
                    )
                    if node_pool.objective_scores is None:
                        scores[node_pool.id] = 0
                    else:
                        scores[node_pool.id] = scores[node_pool.id] + (
                            node_pool.objective_scores[objective] * level
                        )
        return scores

    @staticmethod
    def _first_fit(
        pending_execution: PendingExecutionConnection, valid_node_pool: list[NodePool]
    ) -> NodePool:
        errors = []
        for node_pool in valid_node_pool:
            logger.debug(
                "Compare resource request %s with node pool resources %s",
                pending_execution.resources,
                node_pool,
            )
            enough_resources, error_message = node_pool.has_enough_resources(
                pending_execution.resources
            )
            if enough_resources:
                return node_pool
            errors.append(f"{node_pool.name}: {error_message}")

        formatted_errors = "\n".join(errors)
        raise NotEnoughResourcesException(
            "Not enough resources to schedule execution:\n" + formatted_errors
        )

    @staticmethod
    def _get_gpumemory_gb_from_gpu_mode(mig_str: str) -> int:
        """
        Extract the GPU memory size in GB from a string representing a GPU mode in MIG mode.

        Args:
            mig_str: A string representing the GPU MIG mode. It can be like:
                "mig-1g.10gb" in nodepool format.
                "1g.10gb" in VPA recommendation format.

        Returns:
            The GPU memory size in GB as an integer.

        """
        return int(mig_str.split(".")[1][:-2])

    @staticmethod
    def _get_node_pool_largest_mig(node_pools: list[NodePool]) -> NodePool:
        """
        Return the node pool with the largest GPU size.

        The order of GPU size (max to min):
        1. Full GPU
        2. MIG mode GPUs, ordered by the GPU memory size of the MIG slice (GI)

        If there is no available GPU (full or MIG) at all, raise NotEnoughResourcesException.
        """
        max_pool = None

        for pool in node_pools:
            if pool.gpu_mode == GPU_MODE_FULL and pool.gpu >= 1:
                return pool  # If there is pool with full GPU, assign to it.
            # This pool contains GPU is in MIG mode
            if pool.gpu_mode.startswith("mig") and pool.gpu >= 1:
                if max_pool is None or (
                    SitePlacementService._get_gpumemory_gb_from_gpu_mode(pool.gpu_mode)
                    > SitePlacementService._get_gpumemory_gb_from_gpu_mode(
                        max_pool.gpu_mode
                    )
                ):
                    max_pool = pool

        # No available GPU with MIG mode
        if max_pool is None:
            raise NotEnoughResourcesException(
                "Not enough GPU resources to schedule execution"
            )
        else:
            return max_pool

    @staticmethod
    def _get_smallest_suitable_mig_pool(
        node_pools: list[NodePool], workload_mig_size: str
    ) -> NodePool:
        """
        Return the node pool who has free GPU (or MIG slice) with smaller GPU size, and the size is not smaller than the workload's GPU requirement.
        If there are GPU pools (full or MIG) satisfying the size requirement, but they are all occupied (no free ones):
            in this case we choose the smallest pool (although it's not free), to let K8s provision another node.
        If there is no GPU pool (full or MIG) satisfying the size requirement at all, raise NotEnoughResourcesException.

        The order of GPU size is the same as function above.
        """
        workload_gpumem_size = SitePlacementService._get_gpumemory_gb_from_gpu_mode(
            workload_mig_size
        )
        smallest_pool_with_free_mig_space = None
        for pool in node_pools:
            if (
                pool.gpu_mode.startswith("mig")
                and pool.gpu >= 1
                and pool.gpu_free_total >= 1
            ):
                cur_pool_gpumem_size = (
                    SitePlacementService._get_gpumemory_gb_from_gpu_mode(pool.gpu_mode)
                )
                if cur_pool_gpumem_size >= workload_gpumem_size:
                    if smallest_pool_with_free_mig_space is None or (
                        cur_pool_gpumem_size
                        < SitePlacementService._get_gpumemory_gb_from_gpu_mode(
                            smallest_pool_with_free_mig_space.gpu_mode
                        )
                    ):
                        smallest_pool_with_free_mig_space = pool

        if smallest_pool_with_free_mig_space is not None:
            return smallest_pool_with_free_mig_space

        # The case that there is no available MIG slice anywhere... Should provision another MIG node with smallest suitable size
        smallest_pool = None
        for pool in node_pools:
            if (
                pool.gpu_mode.startswith("mig") and pool.gpu >= 1
            ):  # Whatever there is free space
                cur_pool_gpumem_size = (
                    SitePlacementService._get_gpumemory_gb_from_gpu_mode(pool.gpu_mode)
                )
                if cur_pool_gpumem_size >= workload_gpumem_size:
                    if smallest_pool is None or (
                        cur_pool_gpumem_size
                        < SitePlacementService._get_gpumemory_gb_from_gpu_mode(
                            smallest_pool.gpu_mode
                        )
                    ):
                        smallest_pool = pool

        # No MIG Pool everywhere...
        if smallest_pool is not None:
            return smallest_pool

        for pool in node_pools:
            if pool.gpu_mode == GPU_MODE_FULL and pool.gpu >= 1:
                return pool  # If there is pool with full GPU, assign to it.

        raise NotEnoughResourcesException(
            "Not enough GPU resources to schedule execution (with recommendation)"
        )

    def schedule_one(
        self,
        sites: list[Site],
        container_image_to_recommendations: dict[str, RecommendationVPA],
        to_schedule: PendingExecutionConnection,
    ) -> NodePool:
        logger.debug(
            "Start scheduling node pool for PendingExecutionConnection: %s", to_schedule
        )
        # 1. Filter site that does not fit the constraints
        valid_sites = self._resolve_placement_constraints(
            to_schedule.site_list,
            to_schedule.site_type_list,
            sites,
        )

        all_node_pools = []
        for site in valid_sites:
            all_node_pools.extend(site.node_pools)
        logger.debug("All node pools: %s", all_node_pools)

        # 2. filter by nodepool list
        if to_schedule.node_pool_list:
            valid_node_pools = to_schedule.node_pool_list
        else:
            valid_node_pools = all_node_pools

        # 3. Filter on architecture constraint
        valid_node_pools = self._resolve_architecture_constraints(
            to_schedule.arch_list, valid_node_pools
        )
        logger.debug("Valid node pools: %s", valid_node_pools)

        # 4. Remove GPU node if not ask
        valid_node_pools = [
            valid_node_pool
            for valid_node_pool in valid_node_pools
            if valid_node_pool.gpu == 0
            or (to_schedule.resources is not None and to_schedule.resources.gpu)
            or not valid_node_pool.filter_no_gpu_action
        ]

        # 5. TODO Apply affinity constraints

        # GPU MIG Autoscaling algorithm: first fit
        # If this connection requires 1 GPU, we trigger a special first fit on available MIG instances.
        # If successfully triggered, the original first fit and scoring system will be ignored (GPU priority is higher)
        if to_schedule.resources is not None and to_schedule.resources.gpu == 1:
            rec = container_image_to_recommendations.get(
                to_schedule.action_container_image
            )
            # If MIG recommendation exists
            if (
                rec is not None
                and rec.gpu_mig_instance is not None
                and rec.gpu_mig_instance != ""
            ):
                logger.debug(
                    "Schedule smallest MIG node pool among: %s, for recommendation %s",
                    valid_node_pools,
                    rec.gpu_mig_instance,
                )
                return self._get_smallest_suitable_mig_pool(
                    valid_node_pools, rec.gpu_mig_instance
                )
            else:  # If no MIG recommendation exists, use the largest GPU (or MIG slice)
                logger.debug(
                    "Schedule largest MIG node pool among: %s", valid_node_pools
                )
                return self._get_node_pool_largest_mig(valid_node_pools)

        # Apply scoring functions
        # 1. Score on objectives
        scores = self._score_on_objectives(
            to_schedule,
            valid_node_pools,
        )

        # Sort with the highest score first
        valid_node_pools.sort(key=lambda node_pool: scores[node_pool.id], reverse=True)
        for node_pool in valid_node_pools:
            logger.info(
                "Score: execution_id=%s score=%s node_pool=%s",
                to_schedule.id,
                scores[node_pool.id],
                node_pool.name,
            )

        # Apply scheduling policy
        if self.policy == "first_fit":
            placement = self._first_fit(to_schedule, valid_node_pools)
        else:
            raise Exception(f"Unimplemented policy {self.policy}")
        # Here we always schedule the resource_id to the first site with enough available Resources
        return placement
