import logging
from dataclasses import dataclass, field
from typing import Optional, Callable

from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentNewLogLinesAddedEvent,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultNewLogLinesAddedEvent,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.action_logs.action_logs_entities import (
    LogsNotFoundError,
    ActionLogs,
)

# WARNING: This should be in sync with the action wrapper
END_LOG_DELIMITER_PREFIX = "RYAX_EXECUTION_END---"
START_LOG_DELIMITER_PREFIX = "RYAX_EXECUTION_START---"

logger = logging.getLogger(__name__)


def get_delimiter(
    log_line: str, log_delimiters: list[str], prefix: str
) -> Optional[str]:
    for delimiter in log_delimiters:
        if log_line.startswith(prefix) and delimiter in log_line:
            return delimiter
    return None


@dataclass
class ActionLogsService:
    uow_factory: Callable[..., IUnitOfWork]
    message_bus: IMessageBus
    log_query_interval_in_seconds: float
    max_log_size_per_action_in_bytes: int = 1024 * 1024  # 1MB
    max_one_line_log_size_in_bytes: int = 1024  # 1KB
    # In memory store of the list of action deployment id watched with the current execution connection id if any
    currently_watching_actions: dict[str, Optional[str]] = field(default_factory=dict)

    def watch_action_deployment(self, action_deployment_id: str) -> None:
        """Register a new watch"""
        self.currently_watching_actions[action_deployment_id] = None

    def unwatch_action_deployment(self, action_deployment_id: str) -> None:
        """Remove a watch. Ignore if never created"""
        try:
            del self.currently_watching_actions[action_deployment_id]
        except KeyError:
            pass

    async def new_action_deployment_log_lines(
        self, log_lines: str, action_deployment_id: str
    ) -> None:
        """Register new log line in the execution"""
        if not log_lines:
            return
        executions_new_logs_lines: dict[str, str] = {}
        action_deployments_new_logs_lines: dict[str, str] = {}
        with self.uow_factory() as uow:
            action_deployment = uow.action_deployment_repository.get_by_id(
                action_deployment_id
            )
            project_id = action_deployment.project_id
            if action_deployment.action_definition.is_a_trigger():
                # Just catch the logs and store them in the action_deployment
                await self.append(action_deployment.id, project_id, log_lines, uow)
                await self.message_bus.handle_event(
                    ActionDeploymentNewLogLinesAddedEvent(
                        action_deployment_id=action_deployment_id, log_lines=log_lines
                    )
                )
                return

            end_of_log_delimiters: list = uow.execution_connection_repository.list_log_delimiters_by_action_deployment(
                action_deployment
            )
            # Iterate over the log lines.
            # If a delimiter is found, move the cached logs to their execution result. That result log is done.
            # Else, continue to put logs into the cache.
            for log_line in log_lines.splitlines(keepends=True):
                end_delimiter_in_line = get_delimiter(
                    log_line, end_of_log_delimiters, END_LOG_DELIMITER_PREFIX
                )
                start_delimiter_in_line = get_delimiter(
                    log_line, end_of_log_delimiters, START_LOG_DELIMITER_PREFIX
                )
                # Associate a result to this current logs
                if start_delimiter_in_line:
                    (
                        started_execution_result_id,
                        _,
                    ) = uow.execution_result_repository.get_ids_by_end_of_log_delimiter(
                        start_delimiter_in_line
                    )
                    self.currently_watching_actions[
                        action_deployment_id
                    ] = started_execution_result_id
                # An execution result is associated to this deployment
                elif (
                    end_delimiter_in_line is not None
                    and action_deployment_id in self.currently_watching_actions
                    and self.currently_watching_actions[action_deployment_id]
                    is not None
                ):
                    self.currently_watching_actions[action_deployment_id] = None
                elif (
                    end_delimiter_in_line is None
                    and action_deployment_id in self.currently_watching_actions
                    and self.currently_watching_actions[action_deployment_id]
                    is not None
                ):
                    execution_result_id = self.currently_watching_actions[
                        action_deployment_id
                    ]
                    assert execution_result_id is not None
                    if execution_result_id not in executions_new_logs_lines:
                        executions_new_logs_lines[execution_result_id] = ""
                    executions_new_logs_lines[execution_result_id] += log_line

                # Only happen with old wrapper without start delimiter OR
                # when the logs start happen when the runner was down
                elif end_delimiter_in_line is not None and (
                    action_deployment_id not in self.currently_watching_actions
                    or self.currently_watching_actions[action_deployment_id] is None
                ):
                    assert isinstance(end_delimiter_in_line, str)
                    (
                        execution_result_id,
                        execution_connection_id,
                    ) = uow.execution_result_repository.get_ids_by_end_of_log_delimiter(
                        end_delimiter_in_line
                    )
                    try:
                        action_deployment_logs = (
                            uow.action_logs_repository.get_logs_as_string(
                                action_deployment_id
                            )
                            + action_deployments_new_logs_lines.get(
                                action_deployment_id, ""
                            )
                        )
                        if execution_result_id not in executions_new_logs_lines:
                            executions_new_logs_lines[execution_result_id] = ""
                        executions_new_logs_lines[
                            execution_result_id
                        ] += action_deployment_logs
                    except (LogsNotFoundError, KeyError):
                        logger.warning(
                            "No logs found for this action deployment: %s",
                            action_deployment_id,
                        )
                    execution_connection = uow.execution_connection_repository.get(
                        execution_connection_id
                    )
                    execution_connection.remove_end_of_log_delimiter_from_queue(
                        end_delimiter_in_line
                    )
                    # Reset the logs for this deployment
                    try:
                        uow.action_logs_repository.reset(action_deployment_id)
                        action_deployments_new_logs_lines[action_deployment_id] = ""
                    except LogsNotFoundError:
                        pass
                else:
                    if action_deployment_id not in action_deployments_new_logs_lines:
                        action_deployments_new_logs_lines[action_deployment_id] = ""
                    action_deployments_new_logs_lines[action_deployment_id] += log_line

            for execution_result_id, log_lines in executions_new_logs_lines.items():
                await self.append(execution_result_id, project_id, log_lines, uow)
                await self.message_bus.handle_event(
                    ExecutionResultNewLogLinesAddedEvent(
                        execution_result_id=execution_result_id,
                        log_lines=log_lines,
                    )
                )
            for (
                action_deployment_id,
                log_lines,
            ) in action_deployments_new_logs_lines.items():
                await self.append(action_deployment_id, project_id, log_lines, uow)
            uow.commit()

    async def append(
        self, associated_id: str, project_id: str, log_lines: str, uow: IUnitOfWork
    ) -> None:
        try:
            logs = uow.action_logs_repository.get(associated_id)
        except LogsNotFoundError:
            logs = ActionLogs.create(associated_id, project_id)
            uow.action_logs_repository.add(logs)

        for line in log_lines.splitlines(keepends=True):
            size = len(line.encode())
            # Avoid adding very large lines
            if size < self.max_one_line_log_size_in_bytes:
                logs.append(line)
            else:
                logs.append(
                    f"{line[:self.max_one_line_log_size_in_bytes]} ... WARNING: Truncated log line because it exceeded the maximum size of {self.max_one_line_log_size_in_bytes / 1024}KB"
                )
            if logs.logs_size_in_bytes > self.max_log_size_per_action_in_bytes:
                # the logs are too big
                logs.append(
                    f"...\nWARNING Ryax Action Logging: Truncated log lines. Only the fist {self.max_log_size_per_action_in_bytes / 1024} KB are kept."
                )
                break

    async def get_logs(self, associated_id: str, project_id: str) -> str:
        with self.uow_factory() as uow:
            # Check projects is correct
            try:
                logs = uow.action_logs_repository.get(associated_id)
            except LogsNotFoundError:
                return ""

            if not logs.project_id == project_id:
                raise LogsNotFoundError
            return uow.action_logs_repository.get_logs_as_string(associated_id)
