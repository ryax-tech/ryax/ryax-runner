# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from ryax.runner.domain.common.unit_of_work import IUnitOfWork


@dataclass
class PortalView:
    uow: IUnitOfWork

    def get_portal(self, workflow_definition_id: str) -> dict:
        with self.uow as uow:
            workflow = uow.workflow_repository.get_eventually_running_workflow_by_workflow_definition_id(
                workflow_definition_id
            )

            outputs = []
            for out in workflow.get_root_action().get_writable_outputs():
                outputs.append(
                    {
                        "id": out.name,
                        "type": out.type.name,
                        "display_name": out.display_name,
                        "help": out.help,
                        "enum_values": out.enum_values,
                        "optional": out.optional,
                    }
                )
        return {
            "workflow_definition_id": workflow_definition_id,
            "workflow_deployment_id": workflow.id,
            "name": workflow.name,
            "outputs": outputs,
        }
