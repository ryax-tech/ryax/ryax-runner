from dataclasses import dataclass

from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.exceptions import WorkflowNotFoundError
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction


@dataclass
class WorkflowView:
    uow: IUnitOfWork

    def get_trigger_action(self, workflow_definition_id: str) -> WorkflowAction:
        with self.uow as uow:
            workflow = uow.workflow_repository.get_eventually_running_workflow_by_workflow_definition_id(
                workflow_definition_id, eager_load=True
            )
            root_action = workflow.get_root_action()
            if root_action is None:
                raise WorkflowNotFoundError()

        return root_action
