# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import shutil
import tempfile
from pathlib import Path
from typing import BinaryIO

from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.common.domain.storage.storage_service import IStorageService
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.stored_file.stored_file_service import IStoredFileView


class StoredFileView(IStoredFileView):
    def __init__(self, local_storage_service: IStorageService, uow: IUnitOfWork):
        self.local_storage_service = local_storage_service
        self.uow = uow

    async def download_file(self, file_path: str) -> BinaryIO:
        return await self.local_storage_service.read(file_path)

    def get(self, stored_file_id: str, project_id: str) -> StoredFile:
        with self.uow as uow:
            return uow.stored_file_repository.get(stored_file_id, project_id)

    def get_by_file_path(self, stored_file_path: str, project_id: str) -> StoredFile:
        with self.uow as uow:
            return uow.stored_file_repository.get_by_path(stored_file_path, project_id)

    async def zip(self, stored_files: list[StoredFile]) -> StoredFile:
        with self.uow as uow:
            with tempfile.TemporaryDirectory() as tmp_dir:
                for stored_file in stored_files:
                    local_file_loc = Path(tmp_dir) / Path(stored_file.file_path).name
                    with open(local_file_loc, "wb") as f:
                        for chunk in await self.download_file(stored_file.file_path):
                            f.write(chunk)
                        f.flush()

                with tempfile.TemporaryDirectory() as tmp_zip_dir:
                    base_filename = str(Path(tmp_zip_dir) / "tmp")
                    filename = base_filename + ".zip"
                    shutil.make_archive(base_filename, "zip", tmp_dir)

                    zip_size = Path(filename).stat().st_size

                    zipfile_path = self.local_storage_service.generate_file_path(
                        "result"
                    )
                    with open(filename, "rb") as zip_content:
                        await self.local_storage_service.write(
                            zipfile_path, zip_content
                        )

            new_stored_file = StoredFile.create_from_info(
                io_name="result",
                file_path=zipfile_path,
                project_id=stored_files[0].project_id,
                mimetype="application/zip",
                encoding=None,
                size_in_bytes=zip_size,
            )
            uow.stored_file_repository.add(new_stored_file)
            uow.commit()
            return new_stored_file
