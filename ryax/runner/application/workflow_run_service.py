import logging
from dataclasses import dataclass
from typing import Dict, List, Optional

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionIO,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.exceptions import WorkflowRunNotFoundError
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    StopExecutionConnectionCommand,
)
from ryax.common.domain.internal_messaging.base_messages import BaseMessage
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.storage.storage_service import IStorageService
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.workflow_result.workflow_result import (
    WorkflowResultsUnavailableError,
    WorkflowResultsUserDefinedError,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import ActionRun, WorkflowRun
from ryax.runner.domain.workflow_run.workflow_run_values import WorkflowRunState

logger = logging.getLogger(__name__)


@dataclass
class WorkflowRunService:
    uow: IUnitOfWork
    message_bus: IMessageBus
    base_url: str
    storage_service: IStorageService

    async def delete(self, workflow_run_id: str, project_id: str) -> None:
        with self.uow as uow:
            workflow_run = uow.workflow_run_repository.get(workflow_run_id)
            if workflow_run.project_id != project_id:
                raise WorkflowRunNotFoundError()

            stored_files_to_be_deleted = (
                uow.workflow_run_repository.get_stored_files_to_delete(
                    [workflow_run_id]
                )
            )
            for stored_file in stored_files_to_be_deleted:
                try:
                    await self.storage_service.remove_file(stored_file.file_path)
                except FileNotFoundError:
                    logger.warning(
                        f"Trying to delete already deleted file {stored_file}, ignoring..."
                    )
            uow.workflow_run_repository.delete(workflow_run_id)
            uow.commit()

    def get(self, workflow_run_id: str, project_id: str) -> WorkflowRun:
        with self.uow as uow:
            workflow_run = uow.workflow_run_repository.get(workflow_run_id)
            if workflow_run.project_id != project_id:
                raise WorkflowRunNotFoundError()
            return workflow_run

    def check_workflow_run_result_state(
        self, workflow_run_id: str, project_id: str
    ) -> None:
        """
        Used when recovering results from a workflow run.
        If the workflow run is in an erroneous state, it could be due to reuslt unavailability,
        or the fact that a user raised a RyaxException.
        Otherwise, the state is good and no exception is raised.
        """
        with self.uow as uow:
            workflow_run = uow.workflow_run_repository.get(workflow_run_id)
            if workflow_run.project_id != project_id:
                raise WorkflowRunNotFoundError()
            if workflow_run.state in [
                WorkflowRunState.CANCELED,
                WorkflowRunState.ERROR,
            ]:
                execution_with_user_defined_exit = (
                    workflow_run.get_last_execution_with_user_defined_exit()
                )
                if execution_with_user_defined_exit is not None:
                    assert execution_with_user_defined_exit.http_status_code is not None
                    raise WorkflowResultsUserDefinedError(
                        code=execution_with_user_defined_exit.http_status_code,
                        message=execution_with_user_defined_exit.error_message,
                    )
                raise WorkflowResultsUnavailableError()

    def get_view(self, workflow_run_id: str, project_id: str) -> Dict:
        with self.uow as uow:
            workflow_run = uow.workflow_run_repository.get(workflow_run_id)
            if workflow_run.project_id != project_id:
                raise WorkflowRunNotFoundError()
            runs: List[dict] = []
            if workflow_run.runs is not None:
                action_run: ActionRun
                for action_run in workflow_run.runs:
                    converted_execution_result: dict = _convert(
                        action_run, self.base_url
                    )
                    runs.append(converted_execution_result)

            return {
                "id": workflow_run.id,
                "state": workflow_run.state.value,
                "started_at": workflow_run.started_at,
                "last_result_at": workflow_run.last_result_at,
                "total_steps": workflow_run.number_of_actions,
                "completed_steps": workflow_run.number_of_completed_actions,
                "workflow_definition_id": workflow_run.workflow_definition_id,
                "runs": runs,
            }

    def list_view(
        self,
        project_id: str,
        state: Optional[WorkflowRunState] = None,
        workflow_definition_id: Optional[str] = None,
        _range: Optional[str] = None,
    ) -> tuple[list[dict], int, int, int]:
        with self.uow as uow:
            (
                domain_workflow_runs,
                offset,
                limit,
                count,
            ) = uow.workflow_run_repository.list_with_filter(
                workflow_definition_id=workflow_definition_id,
                state=state,
                _range=_range,
            )
            if any(
                workflow_run.project_id != project_id
                for workflow_run in domain_workflow_runs
            ):
                raise WorkflowRunNotFoundError()

        workflow_runs: list[dict] = []
        for domain_workflow_run in domain_workflow_runs:
            workflow_runs.append(
                {
                    "id": domain_workflow_run.id,
                    "state": domain_workflow_run.state.value,
                    "started_at": domain_workflow_run.started_at,
                    "last_result_at": domain_workflow_run.last_result_at,
                    "total_steps": domain_workflow_run.number_of_actions,
                    "completed_steps": domain_workflow_run.number_of_completed_actions,
                    "workflow_definition_id": domain_workflow_run.workflow_definition_id,
                }
            )
        return (
            workflow_runs,
            offset,
            limit,
            count,
        )

    async def cancel(self, workflow_run_id: str, project_id: str) -> None:
        with self.uow as uow:
            workflow_run = uow.workflow_run_repository.get(workflow_run_id)
            if workflow_run.project_id != project_id:
                raise WorkflowRunNotFoundError()

            messages: list[BaseMessage] = []
            for (
                execution_connection_id
            ) in workflow_run.get_running_execution_connections():
                messages.append(StopExecutionConnectionCommand(execution_connection_id))

            await self.message_bus.handle(messages)
            workflow_run.stop()

            uow.commit()


def _action_convert(
    domain_action: ActionDefinition,
    display_name: Optional[str],
    inputs: Optional[dict],
    outputs: Optional[dict],
    stored_files: List[StoredFile],
    base_url: str,
) -> dict:
    return {
        "id": domain_action.id,
        "technical_name": domain_action.technical_name,
        "name": display_name if display_name else domain_action.human_name,
        "description": domain_action.description,
        "version": domain_action.version,
        "kind": domain_action.kind.value,
        "inputs": [
            {
                "id": io.id,
                "type": io.type.value,
                "display_name": io.display_name,
                "help": io.help,
                "enum_values": io.enum_values,
                **_convert_io(io, inputs, stored_files, base_url),
            }
            for io in domain_action.inputs
        ],
        "outputs": [
            {
                "id": io.id,
                "type": io.type.value,
                "display_name": io.display_name,
                "help": io.help,
                "enum_values": io.enum_values,
                **_convert_io(io, outputs, stored_files, base_url),
            }
            for io in domain_action.outputs
        ],
        "container_image": domain_action.container_image,
    }


def _convert(
    action_run: ActionRun,
    base_url: str,
) -> Dict:
    inputs = {}
    outputs = {}
    stored_files = []
    last_execution = action_run.last_execution_result()
    if last_execution is not None:
        inputs = last_execution.inputs
        outputs = last_execution.outputs
        stored_files = last_execution.stored_files

    return {
        "id": action_run.id,
        "state": action_run.state.value,
        "error_message": action_run.error_message,
        "action": _action_convert(
            action_run.workflow_action.definition,
            action_run.workflow_action.display_name,
            inputs,
            outputs,
            stored_files,
            base_url,
        ),
        "results": [
            {
                "id": execution_result.id,
                "submitted_at_date": execution_result.submitted_at,
                "started_at_date": execution_result.started_at,
                "ended_at_date": execution_result.ended_at,
                "error_message": (
                    execution_result.error_message
                    if execution_result.error_message is not None
                    else ""
                ),
                "state": execution_result.state,
            }
            for execution_result in action_run.execution_results
        ],
    }


def _convert_io(
    io: ActionIO, values: Optional[dict], stored_files: list[StoredFile], base_url: str
) -> dict:
    try:
        if io.type.is_file_type():
            for stored_file in stored_files:
                if stored_file.io_name == io.name:
                    url, headers = stored_file.get_download_url()
                    return {"value": base_url + url, "headers": headers}
        elif values is not None:
            return {"value": values[io.name], "headers": {}}
        return {"value": None, "headers": {}}
    except Exception:
        return {"value": None, "headers": {}}
