# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Optional

from ryax.runner.domain.authorization_api_service import IAuthorizationApiService


class ProjectAuthorizationService:
    def __init__(self, authorization_api_service: IAuthorizationApiService):
        self.authorization_api_service = authorization_api_service

    async def get_current_project(
        self, user_id: str, authorization_token: str
    ) -> Optional[str]:
        return (
            await self.authorization_api_service.get_current_project(
                user_id, authorization_token
            )
            if user_id
            else None
        )
