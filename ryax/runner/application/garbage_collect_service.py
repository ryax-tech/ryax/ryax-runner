import asyncio
import functools
import logging
import multiprocessing
from asyncio import FIRST_COMPLETED
from dataclasses import dataclass
from queue import Empty
from typing import Sequence

from ryax.common.application.utils import retry_with_backoff

from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.common.domain.storage.storage_service import IStorageService
from ryax.common.infrastructure.storage.s3.s3_engine import S3Engine
from ryax.common.infrastructure.storage.s3.s3_service import S3Storage
from ryax.runner.infrastructure.database.mapper import map_orm
from ryax.runner.infrastructure.database.repositories.workflow_run_repository import (
    DatabaseWorkflowRunRepository,
)
from opentelemetry.instrumentation.logging import LoggingInstrumentor
import concurrent.futures

LoggingInstrumentor().instrument(set_logging_format=True, log_level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Disable internal lib debug logs by default
logging.getLogger("asyncio").setLevel(logging.INFO)
logging.getLogger("kubernetes_asyncio").setLevel(logging.INFO)
logging.getLogger("aiormq").setLevel(logging.INFO)
logging.getLogger("ryax.performance").setLevel(logging.INFO)


@dataclass
class GarbageCollectorService:
    queue: multiprocessing.Queue
    database_connection_url: str
    storage_config: dict
    max_number_of_runs_per_project: int = 5000
    delay_between_calls_in_seconds: int = 900  # 15min
    database_engine: SqlalchemyDatabaseEngine | None = None
    local_storage_service: IStorageService | None = None

    @retry_with_backoff(
        retries=-1, backoff_in_ms=5000, max_backoff_in_ms=10 * 60 * 1000
    )
    async def main(self) -> None:
        """
        This garbage collection service is standalone and should be run in a separate process.
        The collection is triggerd by an external process pushing on the queue
        """
        try:
            logger.info("Connecting to the database...")
            if self.database_engine is None:
                self.database_engine = SqlalchemyDatabaseEngine(
                    self.database_connection_url, map_orm
                )
                self.database_engine.connect()
            else:
                self.database_engine.connect(do_mapping=False)

            if self.local_storage_service is None:
                engine = S3Engine(
                    connection_url=self.storage_config["server"],
                    bucket=self.storage_config["bucket"],
                    access_key=self.storage_config["access_key"],
                    secret_key=self.storage_config["secret_key"],
                )
                self.local_storage_service = S3Storage(engine)
                engine.connect()

            logger.info("Connecting to the file storage server...")
            logger.info("Garbage collector successfully started!")
            while True:
                await self.run()
        except Exception:
            logger.exception("Garbage collection failed unexpectedly")
            raise
        finally:
            if self.database_engine is not None:
                self.database_engine.disconnect()

    async def run(self) -> None:
        logger.info(
            "Next garbage collection in %s", self.delay_between_calls_in_seconds
        )
        done, pending = await asyncio.wait(
            [
                asyncio.create_task(asyncio.sleep(self.delay_between_calls_in_seconds)),
                asyncio.create_task(self.async_queue_get()),
            ],
            return_when=FIRST_COMPLETED,
        )
        for task in pending:
            task.cancel()
        for task in done:
            workflow_run_ids = task.result()
            if workflow_run_ids is not None:
                await self.delete_runs(workflow_run_ids)
                return
        else:
            await self.remove_old_runs()

    async def remove_old_runs(self) -> None:
        """Run a deletion of all the workflow run given in the workflow_runs ID list"""
        assert self.database_engine is not None
        with self.database_engine.get_session() as session:
            DatabaseWorkflowRunRepository(session).delete_oldest(
                self.max_number_of_runs_per_project
            )
            session.commit()

    async def delete_runs(self, workflow_run_ids: Sequence[str]) -> None:
        """Run a deletion of all the workflow run given in the workflow_runs ID list"""
        assert self.database_engine is not None
        with self.database_engine.get_session() as session:
            repo = DatabaseWorkflowRunRepository(session)
            stored_files_to_be_deleted = repo.get_stored_files_to_delete(
                workflow_run_ids
            )
            for stored_file in stored_files_to_be_deleted:
                try:
                    assert self.local_storage_service
                    await self.local_storage_service.remove_file(stored_file.file_path)
                except FileNotFoundError:
                    logger.warning(
                        f"Trying to delete already deleted file in {stored_file}, ignoring..."
                    )

            repo.delete_all(workflow_run_ids)
            session.commit()

    async def async_queue_get(self) -> Sequence[str] | None:
        try:
            loop = asyncio.get_event_loop()
            with concurrent.futures.ThreadPoolExecutor() as pool:
                get_from_queue = functools.partial(self.queue.get, timeout=1)
                result = None
                while result is None:
                    task = loop.run_in_executor(pool, get_from_queue)
                    try:
                        result = await task
                    except Empty:
                        pass
        except asyncio.CancelledError:
            task.cancel()

        logger.info("Stop watching queue")
        return result
