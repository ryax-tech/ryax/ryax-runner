# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Optional

from ryax.runner.domain.auth_token import AuthToken
from ryax.runner.domain.security_service import ISecurityService


class AuthenticationService:
    def __init__(self, security_service: ISecurityService):
        self.security_service = security_service

    def check_access(self, token: str) -> Optional[AuthToken]:
        return self.security_service.get_auth_token(token) if token else None
