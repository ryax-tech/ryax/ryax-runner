# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import datetime
from logging import getLogger
from typing import Callable

from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionErroredEvent,
)
from ryax.runner.domain.scheduler.scheduler_exceptions import (
    NoResourcesFoundWithConstraints,
    NotEnoughResourcesException,
)
from ryax.runner.domain.site.site_entities import NodePool
from ryax.runner.application.site_placement_service import SitePlacementService
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    CreateActionDeploymentCommand,
    UndeployActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.addons.addon_entities import RyaxAddOns
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.exceptions import ActionDeploymentError
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    AllocateExecutionConnectionCommand,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.scheduler.scheduler_entities import (
    PendingExecutionConnection,
    PlatformStateSnapshot,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_exceptions import (
    RecommendationVPANotFoundError,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_entities import (
    RecommendationVPA,
)

logger = getLogger(__name__)


class MultiObjectiveScheduler:
    """
    The scheduler is responsible to:
    - select the best Site's NodePool to deploy an action using objectives and constraints
    - decide when to deploy or undeploy a ActionDeployment on the selected sites
    - allocate pending ExecutionConnections to ActionDeployments

    The scheduler is not responsible to decide which ExecutionConnection to create once a
    result is created. In other word, the scheduler knows nothing about ExecutionResults.

    The basic cinematic of a scheduler in Ryax is:
    1) Take a look at the pending ExecutionConnections
    2) Take a look at the ActionDeployments
    3) Decide where the pending ExecutionConnections will be scheduled:
        - on ready ActionDeployments, allocate them
        - on busy ActionDeployments, wait
        - on new ActionDeployments, deploy them.
    And don't forget that Triggers should always run.

    A new scheduling should be triggered on every event that change the add new pending connections, or modify the state of a deployment.

    A Scheduler can send the following messages to the message_bus:
    - CreateActionDeploymentCommand
    - UndeployActionDeploymentCommand
    - AllocateExecutionConnectionCommand
    """

    def __init__(
        self,
        message_bus: IMessageBus,
        uow_factory: Callable[..., IUnitOfWork],
        addons: RyaxAddOns,
        site_placement_service: SitePlacementService,
        undeploy_idle_actions_after_seconds: int = 0,
        schedule_retry_delay_seconds: int = 10,
        max_action_deployments: int = 50,
        max_concurrent_deploying_actions: int = 3,
    ):
        self.max_concurrent_deploying_actions = max_concurrent_deploying_actions
        self.max_action_deployments = max_action_deployments

        self.message_bus = message_bus
        self.uow_factory = uow_factory
        self.addons = addons
        self.site_placement_service = site_placement_service

        self.undeploy_idle_actions_after_seconds = undeploy_idle_actions_after_seconds
        self.scheduler_retry_delay_seconds = schedule_retry_delay_seconds

        self._schedule_lock = asyncio.Lock()
        if self.undeploy_idle_actions_after_seconds > 0:
            # If this variable is 0 or -1, we leave actions deployed.
            asyncio.get_event_loop().call_later(
                self.undeploy_idle_actions_after_seconds,
                lambda: asyncio.create_task(
                    self._timer_unschedule_idle_actions(),
                    name=f"{self.__class__.__name__}-{get_random_id()}",
                ),
            )
        self._platform_state: PlatformStateSnapshot | None = None

    async def _timer_unschedule_idle_actions(self) -> None:
        async with self._schedule_lock:
            logger.debug("Running garbage collect on idle actions")
            with self.uow_factory() as uow:
                idle_actions = uow.action_deployment_repository.list_eventually_ready()
                for action in idle_actions:
                    idle_date_threshold = datetime.datetime.now(
                        datetime.timezone.utc
                    ) - datetime.timedelta(
                        seconds=self.undeploy_idle_actions_after_seconds
                    )

                    if (
                        action.last_allocation_date <= idle_date_threshold
                        and not action.action_definition.is_a_trigger()
                        and not action.allocated_connection_id
                    ):
                        logger.debug(
                            "Found an actions idle since %s, : %s",
                            str(idle_date_threshold),
                            action,
                        )
                        await self.message_bus.handle_command(
                            UndeployActionDeploymentCommand(
                                action_deployment_id=action.id
                            )
                        )

                # Invalidate platform state
                self._platform_state = None
        asyncio.get_event_loop().call_later(
            self.undeploy_idle_actions_after_seconds,
            lambda: asyncio.create_task(
                self._timer_unschedule_idle_actions(),
                name=f"{self.__class__.__name__}-{get_random_id()}",
            ),
        )

    async def create_and_allocate(
        self, connection: PendingExecutionConnection, node_pool: NodePool
    ) -> None:
        """
        Will attempt to create an ActionDeployment.
        If at this stage, an error occurs, we will retry later.
        Otherwise, it gets created, and we allocate it if the allocate parameter is set
        """
        try:
            new_action_id = await self.message_bus.handle_command(
                CreateActionDeploymentCommand(
                    project_id=connection.project_id,
                    action_definition_id=connection.action_definition_id,
                    node_pool_id=node_pool.id,
                    execution_connection_id=connection.id,
                )
            )
            await self.message_bus.handle_command(
                AllocateExecutionConnectionCommand(
                    execution_connection_id=connection.id,
                    action_deployment_id=new_action_id,
                )
            )
        except ActionDeploymentError:
            logger.warning(
                f"Create action deployment with action definition id '{connection.action_definition_id}' failed. Retrying schedule in {self.scheduler_retry_delay_seconds} seconds"
            )

            loop = asyncio.get_event_loop()
            loop.call_later(
                self.scheduler_retry_delay_seconds,
                lambda: asyncio.create_task(
                    self.schedule(), name=f"{self.__class__.__name__}-{get_random_id()}"
                ),
            )

    async def _schedule_trigger(
        self, connection: PendingExecutionConnection, node_pool: NodePool
    ) -> None:
        with self.uow_factory() as uow:
            ready_actions = uow.action_deployment_repository.list_unallocated_and_ready_from_action_definition_id(
                connection.action_definition_id, connection.project_id, node_pool.id
            )
            num_ready_actions = len(ready_actions)
            if num_ready_actions > 0:
                first_action_id = ready_actions[0].id
            else:
                first_action_id = None

        if num_ready_actions == 0:
            await self.create_and_allocate(connection, node_pool)
        else:
            assert first_action_id is not None
            await self.message_bus.handle_command(
                AllocateExecutionConnectionCommand(
                    execution_connection_id=connection.id,
                    action_deployment_id=first_action_id,
                )
            )

    def update_action_deployments_state(
        self,
        connection: PendingExecutionConnection,
        refresh_cache: bool,
        node_pool: NodePool,
    ) -> None:
        """
        Returns a list of eventually ready ActionDeployment
        """
        if self._platform_state is None:
            self._platform_state = PlatformStateSnapshot()
        if refresh_cache is True:
            with self.uow_factory() as uow:
                logger.debug("Updating Platform State")
                self._platform_state.action_deployments_by_definition_id[
                    node_pool.id, connection.action_definition_id
                ] = uow.action_deployment_repository.list_eventually_ready_from_action_definition_id(
                    connection.action_definition_id, connection.project_id, node_pool.id
                )
                self._platform_state.nb_actions_eventually_deployed = (
                    uow.action_deployment_repository.count_eventually_ready(
                        node_pool.id
                    )
                )

    async def _schedule_action(
        self,
        connection: PendingExecutionConnection,
        refresh_cache: bool,
        node_pool: NodePool,
    ) -> bool:
        self.update_action_deployments_state(connection, refresh_cache, node_pool)
        assert self._platform_state is not None
        nb_deploying_actions = 0
        action_deployments = (
            self._platform_state.action_deployments_by_definition_id.get(
                (node_pool.id, connection.action_definition_id), []
            )
        )
        nb_current_deployments = self._platform_state.nb_actions_eventually_deployed

        logger.debug(
            "Found Action Deployments for action definition id '%s': %s",
            connection.action_definition_id,
            action_deployments,
        )

        for action_deployment in action_deployments:
            if (
                action_deployment.state == ActionDeploymentState.READY
                and action_deployment.allocated_connection_id is None
            ):
                # We have an unallocated action deployment. It is ready, allocate it!
                await self.message_bus.handle_command(
                    AllocateExecutionConnectionCommand(
                        execution_connection_id=connection.id,
                        action_deployment_id=action_deployment.id,
                    )
                )
                return True
            elif action_deployment.state in [
                ActionDeploymentState.DEPLOYING,
                ActionDeploymentState.CREATED,
                ActionDeploymentState.NO_RESOURCES_AVAILABLE,
            ]:
                # Count pending action deployment for this connection
                nb_deploying_actions += 1

        if (
            nb_current_deployments < self.max_action_deployments
            and nb_deploying_actions < self.max_concurrent_deploying_actions
        ):
            # There is enough resources to trigger the scaling 1-N
            logger.debug(
                "Connection %s for action definition %s, triggered a new deployment (Deploying: %d [max: %d], Total Deployments: %d [max: %d])",
                connection.id,
                connection.action_definition_id,
                nb_deploying_actions,
                self.max_concurrent_deploying_actions,
                nb_current_deployments,
                self.max_action_deployments,
            )
            await self.create_and_allocate(connection, node_pool)
            return True
        else:
            # In this case all action deployments are allocated, and we can't re-allocate until they're done
            logger.debug(
                "Unable to allocate connection %s for action definition %s, waiting for an available deployment (Deploying: %d [max: %d], Total Deployments: %d [max: %d])",
                connection.id,
                connection.action_definition_id,
                nb_deploying_actions,
                self.max_concurrent_deploying_actions,
                nb_current_deployments,
                self.max_action_deployments,
            )
            return False

    async def _undeploy_useless_actions(self) -> None:
        logger.info("Scheduler is called for undeploy useless actions")
        unallocated_actions = []
        with self.uow_factory() as uow:
            for action in uow.action_deployment_repository.list_unallocated():
                unallocated_actions.append(
                    (
                        action.id,
                        action.action_definition.id,
                        action.action_definition.is_a_trigger(),
                        action.get_deployment_type() == "sshslurm",
                    )
                )
            for (
                action_id,
                action_definition_id,
                is_a_trigger,
                is_hpc,
            ) in unallocated_actions:
                # TODO ideally is_hpc and is_a_trigger should become a parameter always_undeploy in the action_definition
                # this parameter can be set upon the creation of the action, if it is a trigger, or eventually on
                # a specific addon hook.
                if is_a_trigger or is_hpc:
                    logger.debug(
                        "Undeploy Action %s for action definition %s because it is not a reusable",
                        action_id,
                        action_definition_id,
                    )
                    await self.message_bus.handle_command(
                        UndeployActionDeploymentCommand(action_deployment_id=action_id)
                    )
                else:
                    workflow_list = uow.workflow_repository.list_active_workflows_for_this_action_definition(
                        action_definition_id
                    )
                    logger.debug(
                        "Found workflow containing action with definition %s: %s",
                        action_definition_id,
                        workflow_list,
                    )
                    num_active_workflows = len(workflow_list)

                    if num_active_workflows == 0:
                        logger.debug(
                            "Undeploy Action %s for action definition %s because it was not a found in a deployed workflow",
                            action_id,
                            action_definition_id,
                        )
                        await self.message_bus.handle_command(
                            UndeployActionDeploymentCommand(
                                action_deployment_id=action_id
                            )
                        )

    def _get_container_image_recommendations(
        self, uow: IUnitOfWork, pending_connections: list[PendingExecutionConnection]
    ) -> dict[str, RecommendationVPA]:
        container_image_to_recommendations: dict[str, RecommendationVPA] = {}
        for connection in pending_connections:
            if connection.action_container_image in container_image_to_recommendations:
                logger.debug(
                    "Recommendation for container image %s is already registered. Skip.",
                    connection.action_container_image,
                )
                continue
            try:
                # Find recommendation by container image
                recommendation_vpa = uow.recommendation_vpa_repository.get(
                    connection.action_container_image
                )
            except RecommendationVPANotFoundError:
                logger.debug(
                    "Recommendation for container image %s not found",
                    connection.action_container_image,
                )
            else:
                container_image_to_recommendations[
                    connection.action_container_image
                ] = recommendation_vpa
                logger.debug(
                    "Found recommendation for execution connection %s: %s",
                    connection.id,
                    recommendation_vpa,
                )

        return container_image_to_recommendations

    # Update gpu_free_total in each NodePool by calculating:
    # gpu_free_total = gpu_num_total - Number of Allocated or Running ExecutionConnections on the NodePool
    # TODO The logic can be implemented more in DB repository. To reduce the amount of data loaded...
    def _update_nodepools_gpu_free_total(self) -> None:
        with self.uow_factory() as uow:
            allocated_or_running_execution_connections = (
                uow.execution_connection_repository.list_allocated_running()
            )
            nodepool_id_to_num_execution_connections = {}
            for execution_connection in allocated_or_running_execution_connections:
                # Only count for GPU Pools
                if (
                    execution_connection.action_deployment is not None
                    and execution_connection.action_deployment.node_pool is not None
                    and execution_connection.action_deployment.node_pool.gpu > 0
                ):
                    nodepool_id = execution_connection.action_deployment.node_pool.id
                    if nodepool_id not in nodepool_id_to_num_execution_connections:
                        nodepool_id_to_num_execution_connections[nodepool_id] = 0
                    nodepool_id_to_num_execution_connections[nodepool_id] += 1

            for site in uow.site_repository.list():
                for node_pool in site.node_pools:
                    # Only update for GPU Pools.
                    if node_pool.gpu > 0:
                        node_pool.gpu_free_total = (
                            node_pool.gpu_num_total
                            - nodepool_id_to_num_execution_connections.get(
                                node_pool.id, 0
                            )
                        )  # If there's no execution in the nodepool, the default value is 0

            uow.commit()

    async def schedule(self) -> None:
        """
        The scheduler looks at pending executions and trigger action deployment if necessary.
        It also undeploy unused actions when no deployed workflow reference them or after a delay defined in `undeploy_idle_actions_after_seconds`.

        Here is an overview of a scheduling round. For each pending execution ordered by submission time (oldest first):
        - if it's a trigger, always deploy and allocate an ActionDeployment
        - if a deployment is available allocate it
        - for other actions deploy an ActionDeployment only if:
            - the action has no ActionDeployment available
            - no action currently deploying
            - the maximum number of action deployed is not reached
        - Finally, undeploy all ActionDeployment hat are not in use in a running workflow.

        Acquire self._schedule_lock before running this function to avoid concurrent scheduling.
        """
        # Only do one schedule at a time to avoid double allocations
        async with self._schedule_lock:
            self._update_nodepools_gpu_free_total()
            with self.uow_factory() as uow:
                pending_connections = uow.execution_connection_repository.list_pending()
                sites = uow.site_repository.list()
                container_image_to_recommendations = (
                    self._get_container_image_recommendations(uow, pending_connections)
                )
            logger.debug(
                "Run scheduler on pending connections: %s", pending_connections
            )
            logger.debug(
                "VPA Recommendations by container image: %s",
                container_image_to_recommendations,
            )
            refresh_deployment_state: bool = True
            for connection in pending_connections:
                # Define site to allocate the connection
                try:
                    node_pool = self.site_placement_service.schedule_one(
                        sites, container_image_to_recommendations, connection
                    )
                except (
                    NoResourcesFoundWithConstraints,
                    NotEnoughResourcesException,
                ) as err:
                    await self.message_bus.handle(
                        [
                            ExecutionConnectionErroredEvent(
                                connection.id,
                                f"Unable to schedule the Execution {connection.id}: {err.__class__.__name__}: {err}",
                            )
                        ]
                    )
                    refresh_deployment_state = True
                    continue
                if connection.is_a_trigger:
                    await self._schedule_trigger(connection, node_pool)
                else:
                    refresh_deployment_state = await self._schedule_action(
                        connection, refresh_deployment_state, node_pool
                    )

            # Now we can undeploy useless ActionDeployments
            await self._undeploy_useless_actions()
