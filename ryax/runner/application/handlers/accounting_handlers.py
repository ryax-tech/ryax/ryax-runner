from dependency_injector.wiring import Provide

from ryax.runner.application.accounting_service import AccountingService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionUpdatedEvent,
)


async def on_execution_updated(
    event: ExecutionConnectionUpdatedEvent,
    accounting_service: AccountingService = Provide[
        ApplicationContainer.accounting_service
    ],
) -> None:
    await accounting_service.update_entry(event.execution_connection_id)
