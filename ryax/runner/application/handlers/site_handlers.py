import logging

from dependency_injector.wiring import Provide

from ryax.common.domain.registration.registration_values import ObjectiveScores
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.site.site_entities import NodePool, Site
from ryax.runner.domain.site.site_execeptions import (
    SiteNotFoundError,
    DuplicatedSiteNameError,
)
from ryax.runner.domain.site.site_events import RegisterSiteCommand

logger = logging.getLogger(__name__)


async def on_register_or_update(
    command: RegisterSiteCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> str:
    """
    Register new site or update node pool list
    """
    with uow:
        try:
            current_site = uow.site_repository.get(command.site.id)
        except SiteNotFoundError:
            for site in uow.site_repository.list():
                if site.name == command.site.name:
                    raise DuplicatedSiteNameError(
                        f"A site with the same name {site.name} already exists"
                    )
            site = Site.from_command(command.site)
            uow.site_repository.add(site)
            uow.commit()
            logger.info(
                "New site '%s' with id '%s' is registered",
                site.name,
                site.id,
            )
            logger.debug("Details of new site registered: %s", site)
        else:
            logger.info(
                "Site '%s' with id '%s' is already registered",
                command.site.name,
                command.site.id,
            )
            # Remove old node pools
            to_remove = []
            for old_node_pool in current_site.node_pools:
                if old_node_pool.name not in [
                    elem.name for elem in command.site.node_pools
                ]:
                    to_remove.append(old_node_pool)
            for node_pool in to_remove:
                logger.info(
                    "Old node pool '%s' detected on site '%s', removing it",
                    node_pool.name,
                    command.site.name,
                )
                current_site.node_pools.remove(node_pool)
            # Add new node pool
            for new_node_pool in command.site.node_pools:
                if new_node_pool.name not in [
                    elem.name for elem in current_site.node_pools
                ]:
                    logger.info(
                        "New node pool '%s' detected on site '%s', adding it",
                        new_node_pool.name,
                        command.site.name,
                    )
                    current_site.node_pools.append(NodePool.from_command(new_node_pool))
            # Update node pools
            for updated_node_pool in command.site.node_pools:
                for node_pool_to_update in current_site.node_pools:
                    if node_pool_to_update.name == updated_node_pool.name:
                        logger.debug(
                            "Updating nodepool '%s' of site '%s'",
                            node_pool_to_update.id,
                            current_site.id,
                        )
                        node_pool_to_update.cpu = updated_node_pool.cpu
                        node_pool_to_update.memory = updated_node_pool.memory
                        node_pool_to_update.gpu = updated_node_pool.gpu
                        node_pool_to_update.maximum_time_in_sec = (
                            updated_node_pool.maximum_time_in_sec
                        )
                        node_pool_to_update.instance_type = (
                            updated_node_pool.instance_type
                        )
                        if updated_node_pool.objective_scores:
                            if not node_pool_to_update.objective_scores:
                                node_pool_to_update.objective_scores = ObjectiveScores(
                                    id=ObjectiveScores.new_id()
                                )
                            node_pool_to_update.objective_scores.cost = (
                                updated_node_pool.objective_scores.cost
                            )
                            node_pool_to_update.objective_scores.performance = (
                                updated_node_pool.objective_scores.performance
                            )
                            node_pool_to_update.objective_scores.energy = (
                                updated_node_pool.objective_scores.energy
                            )
                            node_pool_to_update.architecture = (
                                updated_node_pool.architecture
                            )
                        node_pool_to_update.gpu_mode = updated_node_pool.gpu_mode
                        node_pool_to_update.gpu_num_total = (
                            updated_node_pool.gpu_num_total
                        )
            uow.commit()

            logger.debug("Details of updated site registered: %s", current_site)
    return command.site.id
