import logging

from dependency_injector.wiring import Provide

from ryax.runner.domain.recommendation_vpa.recommendation_vpa_entities import (
    RecommendationVPA,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_events import (
    RecommendationsVPAUpdateCommand,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_exceptions import (
    RecommendationVPANotFoundError,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.container import ApplicationContainer

logger = logging.getLogger(__name__)


async def on_recommendations_vpa_update(
    command: RecommendationsVPAUpdateCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    """
    Update newest recommendations
    """
    with uow:
        for incoming_rec in command.recommendations:
            try:
                existing_rec_db = uow.recommendation_vpa_repository.get(
                    incoming_rec.container_image
                )
            except RecommendationVPANotFoundError:  # Key not exist, add a row in DB
                uow.recommendation_vpa_repository.add(
                    RecommendationVPA.from_command(incoming_rec)
                )
                uow.commit()
            else:  # Key exist
                if (
                    incoming_rec.timestamp > existing_rec_db.timestamp
                ):  # If incoming recommendation is newer, update row in DB
                    existing_rec_db.memory = incoming_rec.memory
                    existing_rec_db.gpu_mig_instance = incoming_rec.gpu_mig_instance
                    existing_rec_db.timestamp = incoming_rec.timestamp
                    # Update last OOM timestamp if OOM happens, else don't care
                    if incoming_rec.is_oom_bumpup:
                        existing_rec_db.last_oom_timestamp = incoming_rec.timestamp
                    if incoming_rec.is_gpuoom_bumpup:
                        existing_rec_db.last_gpuoom_timestamp = incoming_rec.timestamp
                    uow.commit()
