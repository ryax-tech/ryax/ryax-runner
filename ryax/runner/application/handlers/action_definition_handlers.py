# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
from logging import getLogger

from dependency_injector.wiring import Provide

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_commands import (
    CreateActionDefinitionCommand,
    DeleteActionDefinitionCommand,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_errors import (
    ActionDefinitionNotFoundError,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork

logger = getLogger(__name__)

creation_lock = asyncio.Lock()


async def create(
    cmd: CreateActionDefinitionCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> str:
    """
    Create an action definition from the given command.
    :return: the action definition id
    """
    with uow:
        # Avoid parallel creation of the same definition
        async with creation_lock:
            try:
                action_definition = uow.action_definition_repository.get(cmd.id)
                return action_definition.id
            except ActionDefinitionNotFoundError:
                action_definition = ActionDefinition(
                    id=cmd.id,
                    project_id=cmd.project_id,
                    human_name=cmd.human_name,
                    description=cmd.description,
                    technical_name=cmd.technical_name,
                    version=cmd.version,
                    kind=cmd.kind,
                    container_image=cmd.container_image,
                    inputs=cmd.inputs,
                    outputs=cmd.outputs,
                    addons_inputs=cmd.addons_inputs,
                    has_dynamic_outputs=cmd.has_dynamic_outputs,
                    resources=cmd.resources,
                )
                uow.action_definition_repository.add(action_definition)
                uow.commit()
                return action_definition.id


async def delete(
    cmd: DeleteActionDefinitionCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        action_definition = (
            uow.action_definition_repository.get_by_technical_name_and_version(
                technical_name=cmd.technical_name,
                version=cmd.version,
                project_id=cmd.project_id,
            )
        )
        uow.action_definition_repository.delete(action_definition)
        uow.commit()
