# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dependency_injector.wiring import Provide

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.stored_file.stored_file_commands import CreateStoredFileCommand


async def create(
    cmd: CreateStoredFileCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> str:
    """Handler command to create a new StoredFile"""
    with uow:
        stored_file = StoredFile.create_from_info(
            io_name=cmd.io_name,
            file_path=cmd.file_path,
            project_id=cmd.project_id,
            mimetype=cmd.mimetype,
            size_in_bytes=cmd.size_in_bytes,
            encoding=cmd.encoding,
        )
        uow.stored_file_repository.add(stored_file)
        uow.commit()
        stored_file_id = stored_file.id
    return stored_file_id
