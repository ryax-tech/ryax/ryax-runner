# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from typing import Tuple, Callable, Coroutine
from datetime import datetime, timezone, timedelta
import asyncio

from dependency_injector.wiring import Provide

from ryax.common.domain.internal_messaging.querier import IQuerierService
from ryax.runner.application.action_logs_service import ActionLogsService
from ryax.common.application.message_bus import MessageBus
from ryax.runner.application.reference_solver_service import ReferenceSolverService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    DeallocateActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeploymentStateMachine,
)
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentNewLogLinesEvent,
    ActionDeploymentIsRunningEvent,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    ActionDeploymentNotFoundError,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.addons.addon_entities import RyaxAddOns
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    AllocateExecutionConnectionCommand,
    CreateExecutionConnectionCommand,
    InitializeExecutionResultCommand,
    StartExecutionConnectionCommand,
    StopExecutionConnectionCommand,
    DeallocateExecutionConnectionCommand,
    GetExecutionConnectionStateCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
    ExecutionConnectionStateMachine,
)
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionEndedEvent,
    ExecutionConnectionPendingEvent,
    ExecutionConnectionUpdatedEvent,
    ExecutionConnectionErroredEvent,
    ExecutionConnectionRetryIfNeededEvent,
)
from ryax.runner.domain.site.site_entities import Site
from ryax.runner.domain.execution_connection.execution_connection_exceptions import (
    ExecutionConnectionNotFoundError,
)
from ryax.runner.domain.execution_connection.execution_connection_service import (
    IExecutionService,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultInitializedEvent,
)
from ryax.runner.domain.execution_result.execution_result_execptions import (
    ExecutionResultNotFoundError,
)
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupEvent,
    BaseMessage,
    StopApplicationCommand,
    BaseCommand,
)
from ryax.runner.domain.utils import get_date
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    StopWorkflowDeploymentCommand,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_events import (
    WorkflowDeployedSuccessfully,
)
from ryax.runner.domain.workflow_run.workflow_run_commands import (
    CreateWorkflowRunCommand,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_exceptions import (
    RecommendationVPANotFoundError,
)
from ryax.common.domain.registration.registration_values import GPU_MODE_FULL

logger = getLogger(__name__)


class StartExecutionException(Exception):
    pass


Handler = Callable[..., Coroutine]


@ExecutionConnectionStateMachine.state_machine(
    from_states=[ExecutionConnectionState.NONE],
    to_states=[ExecutionConnectionState.PENDING],
)
async def create(
    cmd: CreateExecutionConnectionCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
    default_execution_time_allotment_in_seconds: float = Provide[
        ApplicationContainer.configuration.default_time_allotment_in_seconds
    ],
    reference_solver: ReferenceSolverService = Provide[
        ApplicationContainer.reference_solver
    ],
) -> str:
    """
    Get input file from run execution command (in base64 or URI) and put it to the local stored_file.
    Then, creates a pending execution.

    :return: The created execution id
    """
    with uow:
        parent_execution_result = None
        if cmd.parent_execution_result_id is not None:
            parent_execution_result = uow.execution_result_repository.get(
                cmd.parent_execution_result_id
            )

        workflow_action = uow.workflow_repository.get_workflow_action_by_id(
            cmd.workflow_action_id
        )
        inputs = reference_solver.resolve_reference_inputs(
            parent_execution_result, workflow_action
        )

        # Inject dynamic inputs in the execution inputs
        try:
            inputs = inputs | await addons.run_hook_on_create_execution_on_runner(
                workflow_action, project_id=cmd.project_id, inputs=inputs
            )
        except Exception as err:
            logger.exception(f"Addon failed with exception {err}")
        reference_solver.filter_inputs(workflow_action, inputs)

        execution_time_allotment = (
            workflow_action.get_resource_time_request()
            if workflow_action.get_resource_time_request() is not None
            else default_execution_time_allotment_in_seconds
        )

        execution_connection = ExecutionConnection.create_from_run_execution_command(
            cmd=cmd,
            inputs=inputs,
            parent_execution_result=parent_execution_result,
            action=workflow_action,
            time_allotment=(
                execution_time_allotment
                if execution_time_allotment is not None
                else default_execution_time_allotment_in_seconds
            ),
        )
        execution_connection_id = execution_connection.id

        execution_connection.set_pending()
        uow.execution_connection_repository.add(execution_connection)

        logger.debug("Created execution connection: %s", execution_connection)
        uow.commit()
    await message_bus.handle([ExecutionConnectionPendingEvent(execution_connection_id)])
    return execution_connection_id


async def on_startup(
    _: ApplicationStartupEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    querier: IQuerierService = Provide[ApplicationContainer.message_querier],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    """
    Ask the worker for an updated state of the executions
    """
    messages: list[BaseMessage] = []
    with uow:
        for (
            execution_connection
        ) in uow.execution_connection_repository.list_is_allocated():
            if execution_connection.action_deployment:
                site_name = execution_connection.action_deployment.node_pool.site.name
                execution_state = await querier.query(
                    GetExecutionConnectionStateCommand(execution_connection.id),
                    site_name=site_name,
                )
                logger.debug(
                    "Execution %s has state %s on the worker %s",
                    execution_connection.id,
                    execution_state,
                    site_name,
                )
                if execution_state != execution_connection.state:
                    messages.append(
                        ExecutionConnectionErroredEvent(
                            execution_connection.id,
                            f"Execution state mismatch between the Runner and the Worker: expected {execution_connection.state}, found {execution_state} ",
                        )
                    )
    await message_bus.handle(messages)


@ExecutionConnectionStateMachine.state_machine(
    from_states=[
        ExecutionConnectionState.PENDING,
        ExecutionConnectionState.ALLOCATED,
        ExecutionConnectionState.STARTED,
        ExecutionConnectionState.RUNNING,
        ExecutionConnectionState.ERROR,
        ExecutionConnectionState.STOPPING,
    ],
    to_states=[ExecutionConnectionState.ERROR],
)
async def on_error(
    event: ExecutionConnectionErroredEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        execution = uow.execution_connection_repository.get(
            event.execution_connection_id
        )
        execution.set_error(event.error_message)
        uow.commit()
    await message_bus.handle([ExecutionConnectionUpdatedEvent(execution.id)])


@ExecutionConnectionStateMachine.state_machine(
    from_states=[ExecutionConnectionState.PENDING],
    to_states=[ExecutionConnectionState.ALLOCATED],
)
@ActionDeploymentStateMachine.state_machine(
    from_states=[*ActionDeploymentState.eventually_ready_states()],
    to_states=[*ActionDeploymentState.eventually_ready_states()],
)
async def allocate(
    cmd: AllocateExecutionConnectionCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    """
    Allocate the ExecutionConnection to the ActionDeployment.
    If everything is ready, run the StartExecutionConnectionCommand!
    """
    start_command: BaseCommand | None = None
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            cmd.action_deployment_id
        )
        # This might happen because the scheduler snapshot view is outdated
        if (
            action_deployment.allocated_connection_id is not None
            and action_deployment.allocated_connection_id != cmd.execution_connection_id
        ):
            logger.warning(
                "Double allocation detected for action_deployment %s, already allocated to %s. Execution connection %s stays in pending",
                action_deployment.id,
                action_deployment.allocated_connection_id,
                cmd.execution_connection_id,
            )
            return
        if (
            action_deployment.state
            not in ActionDeploymentState.eventually_ready_states()
        ):
            logger.warning(
                "Wrong allocation detected for action_deployment %s because it is in state %s. Execution connection %s stays in pending",
                action_deployment.id,
                action_deployment.state,
                cmd.execution_connection_id,
            )
            return

        execution = uow.execution_connection_repository.get(cmd.execution_connection_id)

        # Keep cross-references for allocated deployment
        execution.action_deployment = action_deployment
        action_deployment.allocated_connection_id = execution.id

        execution.set_allocated()

        logger.info(
            "Execution %s is allocated to deployment %s with state %s",
            execution.id,
            action_deployment.id,
            action_deployment.state,
        )
        # If the deployment is already READY start the execution directly, else wait for it's deployment
        if action_deployment.state == ActionDeploymentState.READY:
            start_command = StartExecutionConnectionCommand(
                execution_connection_id=execution.id,
            )
            # Be sure that the Action deployment is set to RUNNING to avoid multiple allocation
            # Need to be applied here to avoid race condition
            action_deployment.set_running()
            # await message_bus.handle_command(
            #    SetRunningActionDeploymentCommand(action_deployment.id),
            # )
        uow.commit()
    # Not blocking to avoid deadlock due to chain of command changing the same object state
    if start_command:
        await message_bus.handle_command(
            start_command,
            blocking=False,
        )


async def deallocate(
    cmd: DeallocateExecutionConnectionCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    """
    Deallocate the ExecutionConnection from the ActionDeployment. The execution goes to Error state
    """
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            cmd.action_deployment_id
        )
        execution = uow.execution_connection_repository.get(cmd.execution_connection_id)
        if execution.state != ExecutionConnectionState.ALLOCATED:
            logger.error(
                "Deallocate should not be called from another state then ALLOCATED, current state is %s",
                execution.state,
            )
        await message_bus.handle_command(
            DeallocateActionDeploymentCommand(cmd.action_deployment_id)
        )
        logger.warning(
            "Execution %s is deallocated from deployment %s with state %s",
            execution.id,
            action_deployment.id,
            action_deployment.state,
        )
        # Run it synchronously to be sure that the error state is commited
        await message_bus.handle_event(
            ExecutionConnectionErroredEvent(
                execution.id, action_deployment.error_message
            )
        )


@ExecutionConnectionStateMachine.state_machine(
    from_states=[ExecutionConnectionState.ALLOCATED],
    to_states=[ExecutionConnectionState.STARTED],
)
@ActionDeploymentStateMachine.state_machine(
    from_states=[ActionDeploymentState.READY],
    to_states=[ExecutionConnectionState.RUNNING],
)
async def start(
    cmd: StartExecutionConnectionCommand,
    container: ApplicationContainer = Provide[ApplicationContainer],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
) -> None:
    """
    Start the ExecutionConnection using the right IExecutionService
    """
    with uow:
        execution = uow.execution_connection_repository.get(
            cmd.execution_connection_id, eager_load=True
        )
        if execution.state != ExecutionConnectionState.ALLOCATED:
            # Necessary when double start du too multiple ready event from the worker
            logger.warning(
                "Trying to start execution connection %s from another state then ALLLOCATED (%s). Ignoring...",
                execution.id,
                execution.state,
            )
            return
        try:
            if execution.action_deployment is None:
                raise ActionDeploymentNotFoundError()

            # Need to be applied here to avoid race condition
            execution.action_deployment.set_running()

            if execution.workflow_action.is_root_action:
                execution.workflow_action.connection_execution_id = execution.id
                event = WorkflowDeployedSuccessfully(
                    workflow_definition_id=execution.workflow_action.workflow_definition_id,
                    workflow_deployment_id=execution.workflow_action.workflow_deployment_id,
                )
                await message_bus.handle([event])

            # Set Started before actually starts to avoid bad state transition triggered by the execution start
            execution.set_started()
            uow.commit()

            execution_type = execution.action_deployment.execution_type.value
            service: IExecutionService = container.execution_service_factory(
                execution_type,
            )
            try:
                addons_extra_params = (
                    await addons.run_hook_execution_on_start_on_runner(execution)
                )
            except Exception as err:
                logger.exception(f"Addon failed with exception {err}")
            await service.start(execution, addons_extra_params)

        except Exception as err:
            await message_bus.handle(
                [
                    ExecutionConnectionErroredEvent(
                        execution.id,
                        f"Something went wrong while starting the execution: {err}",
                    )
                ]
            )
            raise StartExecutionException from err
    await message_bus.handle(
        [
            ExecutionConnectionUpdatedEvent(cmd.execution_connection_id),
            ActionDeploymentIsRunningEvent(execution.action_deployment.id),
        ]
    )


@ExecutionConnectionStateMachine.state_machine(
    # Also work from RUNNING to RUNNING on Triggers
    from_states=[ExecutionConnectionState.STARTED, ExecutionConnectionState.RUNNING],
    to_states=[ExecutionConnectionState.RUNNING],
)
async def initialize(
    cmd: InitializeExecutionResultCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
) -> Tuple[str, str]:
    """
    register in the execution the delimiter and the executor id
    """
    with uow:
        execution_connection = uow.execution_connection_repository.get(
            cmd.execution_connection_id
        )
        execution_connection.executor_id = cmd.executor_id
        execution_connection.set_running()
        # Commit here to avoid nested session errors
        uow.commit()

    try:
        with uow:
            execution_connection = uow.execution_connection_repository.get(
                cmd.execution_connection_id
            )
            if execution_connection.parent_execution_result is None:
                assert execution_connection.started_at is not None
                workflow_run_id = await message_bus.handle_command(
                    CreateWorkflowRunCommand(
                        project_id=execution_connection.project_id,
                        workflow_deployment_id=execution_connection.workflow_action.workflow_deployment_id,
                    )
                )
            else:
                workflow_run_id = (
                    execution_connection.parent_execution_result.workflow_run_id
                )
            execution_connection.workflow_run_id = workflow_run_id

            if cmd.log_delimiter is not None:
                execution_connection.end_of_log_delimiter_queue.append(
                    cmd.log_delimiter
                )

            execution_result = ExecutionResult.create_from_execution_connection(
                project_id=execution_connection.project_id,
                execution_connection_id=execution_connection.id,
                workflow_run_id=workflow_run_id,
                workflow_action=execution_connection.workflow_action,
                end_of_log_delimiter=cmd.log_delimiter,
                submitted_at=execution_connection.submitted_at,
                started_at=get_date(),
                inputs=execution_connection.inputs,
            )
            uow.execution_result_repository.add(execution_result)
            uow.commit()
    except Exception as err:
        await message_bus.handle(
            [
                ExecutionConnectionErroredEvent(
                    execution_connection.id,
                    f"Something went wrong while initializing the execution :{err}",
                )
            ]
        )
        raise
    await message_bus.handle(
        [
            ExecutionResultInitializedEvent(
                execution_result.id,
            )
        ]
    )
    return execution_result.id, workflow_run_id


async def on_new_execution_log_line(
    event: ActionDeploymentNewLogLinesEvent,
    action_logs_service: ActionLogsService = Provide[
        ApplicationContainer.action_logs_service
    ],
) -> None:
    """Register new log line in the execution"""
    await action_logs_service.new_action_deployment_log_lines(
        event.log_lines, event.action_deployment_id
    )


@ExecutionConnectionStateMachine.state_machine(
    from_states=[*ExecutionConnectionState.eventually_running_states()],
    to_states=[ExecutionConnectionState.STOPPING, ExecutionConnectionState.CANCELED],
)
async def stop(
    command: StopExecutionConnectionCommand,
    container: ApplicationContainer = Provide[ApplicationContainer],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    with uow:
        try:
            execution = uow.execution_connection_repository.get(
                command.execution_connection_id
            )
            if execution.action_deployment is None:
                logger.debug(
                    "Trying to stop a non-running ExecutionConnection %s. Cancel it!",
                    execution.id,
                )
                execution.set_canceled()
                uow.commit()
                return
            if execution.state == ExecutionConnectionState.ALLOCATED:
                logger.debug(
                    "The execution connection %s is allocated but not running, cancel it",
                    execution.id,
                )
                await message_bus.handle(
                    [
                        ExecutionConnectionEndedEvent(
                            execution.id,
                            status=ExecutionConnectionState.CANCELED,
                        )
                    ]
                )
                return
            execution_type = execution.action_deployment.execution_type.value
        except ExecutionConnectionNotFoundError:
            logger.warning(
                f"Trying to stop an non-existing ExecutionConnection {command.execution_connection_id}."
            )
        service: IExecutionService = container.execution_service_factory(
            execution_type,
        )
        execution.set_stopping()
        uow.commit()
        await service.stop(execution)


async def stop_all(
    _: StopApplicationCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    messages: list[BaseMessage] = []
    with uow:
        for execution in uow.execution_connection_repository.list_eventually_running():
            messages.append(StopExecutionConnectionCommand(execution.id))
    await message_bus.handle(messages)


@ExecutionConnectionStateMachine.state_machine(
    from_states=[
        *ExecutionConnectionState.eventually_running_states(),
        ExecutionConnectionState.STOPPING,
        ExecutionConnectionState.ERROR,
    ],
    to_states=[ExecutionConnectionState.COMPLETED, ExecutionConnectionState.CANCELED],
)
async def on_ended(
    event: ExecutionConnectionEndedEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    messages: list[BaseMessage] = []
    with uow:
        execution = uow.execution_connection_repository.get(
            event.execution_connection_id
        )
        execution.workflow_action.connection_execution_id = None
        if event.status == ExecutionConnectionState.COMPLETED:
            # This happens when the Execution is already in error
            if execution.state != ExecutionConnectionState.ERROR:
                execution.set_completed()
            workflow = uow.workflow_repository.get(
                execution.workflow_action.workflow_deployment_id
            )
            if (
                execution.workflow_action.definition.is_a_trigger()
                and not workflow.is_stopped()
            ):
                messages.append(
                    StopWorkflowDeploymentCommand(
                        execution.workflow_action.workflow_deployment_id, graceful=True
                    )
                )
        elif event.status == ExecutionConnectionState.CANCELED:
            execution.set_canceled(event.error_message)
            workflow = uow.workflow_repository.get(
                execution.workflow_action.workflow_deployment_id
            )
            if (
                execution.workflow_action.definition.is_a_trigger()
                and not workflow.is_stopped()
                and not workflow.is_paused()
            ):
                messages.append(
                    StopWorkflowDeploymentCommand(
                        execution.workflow_action.workflow_deployment_id
                    )
                )
            try:
                # Cancel also running result if it exists
                result: ExecutionResult = (
                    uow.execution_result_repository.get_running_from_connection_id(
                        event.execution_connection_id,
                    )
                )
                result.set_canceled(error_message=event.error_message)
            except ExecutionResultNotFoundError:
                # No result attached to this connection, nothing to cancel!
                pass

        elif event.status == ExecutionConnectionState.ERROR:
            execution.set_error(msg=event.error_message)
        else:
            raise Exception(
                f"ExecutionConnectionEndedEvent sent with an impossible state: {event.status}"
            )

        if execution.action_deployment is not None:
            messages.append(
                DeallocateActionDeploymentCommand(execution.action_deployment.id)
            )

        uow.commit()
    messages.append(ExecutionConnectionUpdatedEvent(event.execution_connection_id))
    await message_bus.handle(messages)


def _is_gpugi_value_smaller_than_by_memory(a: str, b: str) -> bool:
    """
    Compares 2 MIG GPU GI size strings(the compute and GPU memory size of the slice "xg.xgb", or empty string "" represents no GPU)
    determines if the first is smaller than the second.
    According to its GPU Memory size ("xgb" part). See test case for usage.

    Input:
        a: MIG GPU GI size string "xg.xgb", or in site format "mig-xg.xgb"
        b: MIG GPU GI size string "xg.xgb", or in site format "mig-xg.xgb"

    Returns:
        bool: True if the GPU Memory part of 'a' is smaller than that of 'b', False otherwise.
    """
    if a == "":
        if b == "":
            return False
        return True

    parts_a = a.split(".")
    value_a = int(parts_a[1][:-2])

    parts_b = b.split(".")
    value_b = int(parts_b[1][:-2])

    return value_a < value_b


def _get_max_retry_memory_bytes_and_gpu_gi_from_sites(
    sites: list[Site],
) -> tuple[int, str]:
    max_retry_memory_bytes = 0
    max_retry_gpu_gi = ""
    for site in sites:
        for node_pool in site.node_pools:
            if node_pool.memory > max_retry_memory_bytes:
                max_retry_memory_bytes = node_pool.memory
            # TODO Currently we don't consider MIG GPU and Full GPU at the same time.
            # Currently MIG should be enabled together with VPA recommendation for GPU at the same time.
            # See https://gitlab.com/ryax-tech/ryax/ryax-runner/-/issues/17
            if (
                node_pool.gpu_mode
                and node_pool.gpu_mode != GPU_MODE_FULL
                and _is_gpugi_value_smaller_than_by_memory(
                    max_retry_gpu_gi, node_pool.gpu_mode
                )
            ):
                max_retry_gpu_gi = node_pool.gpu_mode

    return max_retry_memory_bytes, max_retry_gpu_gi


async def on_execution_connection_retry_if_needed(
    event: ExecutionConnectionRetryIfNeededEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    collect_oom_range_length_seconds: int = Provide[
        ApplicationContainer.configuration.action_retry.collect_oom_range_length_seconds
    ],
    collect_oom_end_waiting_seconds: int = Provide[
        ApplicationContainer.configuration.action_retry.collect_oom_end_waiting_seconds
    ],  # Following the assumption above, we wait for this amount seconds after execution connection ends. Allow OOM bump-up to arrive.
    max_retry_error: int = Provide[
        ApplicationContainer.configuration.action_retry.max_error
    ],
) -> None:
    """
    [we assume] If the retry is because of OOM, the bump-up from VPA arrives not too late or too early, during:
    [execution connection ends time + collect_oom_end_waiting_seconds - collect_oom_range_length_seconds,
      execution connection ends time + collect_oom_end_waiting_seconds]
    If this assumption does not hold:
      1) the retry will be treated as because of internal error not OOM Kill (that it should be),
      2) in this case if the OOM bump-up arrives too late (after collect_oom_end_waiting_seconds),
         and second continuous retry occurs because of internal error not OOM:
         The second retry will be wrongly treated as OOM Kill.

    TODO This function is too long, to refactor...
    """
    with uow:
        execution_connection = uow.execution_connection_repository.get(
            event.execution_connection_id
        )

        if (
            execution_connection.workflow_action is None
            or execution_connection.workflow_run_id is None
        ):
            logger.debug(
                "Not consider to retry execution connection %s, because it is not complete (no workflow action or workflow run).",
                execution_connection,
            )
            return

        # Get Action Run and container image
        workflow_run = uow.workflow_run_repository.get(
            execution_connection.workflow_run_id
        )
        action_run = workflow_run.get_run_from_execution_connection(
            execution_connection
        )
        # No retry if not in error state
        if not action_run.is_error():
            logger.debug(
                "No retry because the action is not in error state: %s",
                action_run.state,
            )
            return

        # Get Recommendation
        # Wait for a while to be sure that we have collected the OOM events
        await asyncio.sleep(collect_oom_end_waiting_seconds)
        time_now = datetime.now(timezone.utc)

        container_image = (
            execution_connection.workflow_action.definition.container_image
        )

        recommendation = None
        try:
            recommendation = uow.recommendation_vpa_repository.get(container_image)
        except RecommendationVPANotFoundError:
            logger.debug(
                "No recommendation for container image %s: %s",
                execution_connection,
                container_image,
            )

        has_oom = False
        # Judge if retry is because of OOM
        if recommendation is not None:
            logger.debug(
                "Retry info: last OOM %s, last GPU %s, now %s. DeltaOOM %f, DeltaGPU %f",
                recommendation.last_oom_timestamp,
                recommendation.last_gpuoom_timestamp,
                time_now,
                (time_now - recommendation.last_oom_timestamp).total_seconds(),
                (time_now - recommendation.last_gpuoom_timestamp).total_seconds(),
            )

            sites = uow.site_repository.list()
            (
                max_retry_memory_bytes,
                max_retry_gpu_gi,
            ) = _get_max_retry_memory_bytes_and_gpu_gi_from_sites(sites)
            # If the last memory OOM is less than 30 seconds old and more than 10 seconds old, the retry is because of memory OOM
            if (
                recommendation.last_oom_timestamp
                > time_now - timedelta(seconds=collect_oom_range_length_seconds)
                and recommendation.memory is not None
            ):
                has_oom = True
                if recommendation.memory >= max_retry_memory_bytes:
                    logger.debug("OOM recommendation reached the upper bound.")
                    action_run.exceed_retry_memory_bytes = True
                else:
                    logger.debug(
                        "Retry action run ##### Because of Memory OOM ##### %s, execution connection %s",
                        action_run,
                        execution_connection.id,
                    )
                    action_run.used_retry_memory_times += 1

            # If the last GPU OOM is less than 30 seconds old and more than 10 seconds old. the retry is because of GPU OOM
            if (
                recommendation.last_gpuoom_timestamp
                > time_now - timedelta(seconds=collect_oom_range_length_seconds)
                and recommendation.gpu_mig_instance is not None
            ):
                has_oom = True
                if not _is_gpugi_value_smaller_than_by_memory(
                    recommendation.gpu_mig_instance, max_retry_gpu_gi
                ):
                    logger.debug("GPU OOM recommendation reached the upper bound.")
                    action_run.exceed_retry_gpu_gi = True
                else:
                    logger.debug(
                        "Retry action run ##### Because of GPU OOM ##### %s, execution connection %s",
                        action_run,
                        execution_connection.id,
                    )
                    action_run.used_retry_gpu_gi_times += 1

        # There is no OOM in the context of the retry (as the assumption). So the retry is because of internal error.
        if not has_oom:
            if action_run.used_retry_error >= max_retry_error:
                logger.debug("Used all internal error retry times.")
                action_run.exceed_retry_error = True
            else:
                logger.debug(
                    "Retry action run ##### Because of Error ##### %s, execution connection %s",
                    action_run,
                    execution_connection.id,
                )
                action_run.used_retry_error += 1

        # Because we modified action run above, here we should synchronize the state of the whole workflow run.
        workflow_run.refresh_state(is_retry_enabled=True)

        # No "return" statement before this line since start modifying action deployment.
        # Only set bool labels as indicators of operations.
        # The retry operation will be performed after this line.
        uow.commit()

        if not action_run.need_retry():
            logger.debug(
                "No retry operation because the action run does not need retry: %s",
                action_run,
            )
            return

        await message_bus.handle(
            [
                CreateExecutionConnectionCommand(
                    workflow_action_id=execution_connection.workflow_action.id,
                    action_definition_id=execution_connection.workflow_action.definition.id,
                    project_id=execution_connection.project_id,
                    parent_execution_result_id=None
                    if execution_connection.parent_execution_result is None
                    else execution_connection.parent_execution_result.id,
                )
            ]
        )
