# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from typing import List, Union

from dependency_injector.wiring import Provide

from ryax.common.application.message_bus import MessageBus
from ryax.runner.application.reference_solver_service import ReferenceSolverService
from ryax.runner.container import ApplicationContainer
from ryax.addons.addon_entities import RyaxAddOns
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.exceptions import WorkflowNotFoundError
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    CreateExecutionConnectionCommand,
    StopExecutionConnectionCommand,
)
from ryax.runner.domain.execution_result.execution_result_commands import (
    TriggerNextExecutionCommand,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupEvent,
    BaseMessage,
    StopApplicationCommand,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.site.site_execeptions import (
    SiteNotFoundError,
    NodePoolNotFoundError,
)
from ryax.runner.domain.workflow_action.workflow_action import SchedulingConstraints
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
    DeleteWorkflowDeploymentCommand,
    StartWorkflowDeploymentCommand,
    StopWorkflowDeploymentCommand,
    StopWorkflowDeploymentFromDefinitionIdCommand,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_events import (
    WorkflowDeployedSuccessfully,
    WorkflowStoppedEvent,
    WorkflowUndeployedSuccessfully,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.domain.workflow_run.workflow_run_events import (
    WorkflowRunIsCompletedEvent,
)
from ryax.common.domain.internal_messaging.event_publisher import IPublisherService

logger = getLogger(__name__)


async def create(
    cmd: CreateWorkflowDeploymentCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> str:
    """
    Create a workflow_deployment from the definition
    Check that the actions exists and are deployed
    Creates a pending execution for the root source action.

    :return: The created workflow_deployment id
    """
    try:
        with uow:
            try:
                workflow = uow.workflow_repository.get_eventually_running_workflow_by_workflow_definition_id(
                    cmd.workflow_definition_id
                )
                logger.warning(
                    f"Trying to create a workflow that is already running with workflow_definition_id '{cmd.workflow_definition_id}'. Ignoring"
                )
                return workflow.id
            except WorkflowNotFoundError:
                pass
            action_definitions = {}
            constraints = {}
            for action in cmd.actions:
                action_definitions[
                    action.action_definition_id
                ] = uow.action_definition_repository.get(action.action_definition_id)

                site_list = []
                for site_id in action.constraints.site_list:
                    try:
                        site_list.append(uow.site_repository.get(site_id))
                    except SiteNotFoundError:
                        logger.warning(
                            "Site %s from site constraints list not found. This might come from a deleted site, ignoring...",
                            site_id,
                        )

                node_pool_list = []
                for node_pool_id in action.constraints.node_pool_list:
                    try:
                        node_pool_list.append(
                            uow.site_repository.get_node_pool(node_pool_id)
                        )
                    except NodePoolNotFoundError:
                        logger.warning(
                            "Node Pool %s from node pool constraints list not found. This might come from a deleted site, ignoring..."
                        )

                constraints[action.id_in_studio] = SchedulingConstraints(
                    site_list=site_list,
                    site_type_list=action.constraints.site_type_list,
                    node_pool_list=node_pool_list,
                    arch_list=action.constraints.arch_list,
                )
            logger.debug(f"Found constraints for actions: {constraints}")
            workflow = WorkflowDeployment.create_from_create_command(
                cmd, action_definitions, constraints
            )
            # Set root action
            workflow.get_root_action().is_root_action = True

            workflow.set_created()
            uow.workflow_repository.add(workflow)
            uow.commit()

        await message_bus.handle([StartWorkflowDeploymentCommand(workflow.id)])
    except Exception as err:
        await message_bus.handle_command(
            StopWorkflowDeploymentFromDefinitionIdCommand(
                workflow_definition_id=cmd.workflow_definition_id,
                error=f"Unable to create the workflow: {err.__class__.__name__}: {err}",
            )
        )
        raise

    return workflow.id


async def start(
    cmd: StartWorkflowDeploymentCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    reference_solver: ReferenceSolverService = Provide[
        ApplicationContainer.reference_solver
    ],
) -> None:
    message = None
    try:
        with uow:
            workflow = uow.workflow_repository.get(cmd.workflow_id)
            root_action = workflow.get_root_action()

            # Resolve all static reference in the workflow actions
            for workflow_action in workflow.actions:
                workflow_action.addons = reference_solver.resolve_addons(
                    workflow_action_id=workflow_action.id
                )

            workflow.set_started()
            uow.commit()
            message = CreateExecutionConnectionCommand(
                project_id=workflow.project_id,
                action_definition_id=root_action.definition.id,
                workflow_action_id=root_action.id,
            )
    except Exception as err:
        logger.exception(
            "An unexpected error occurred during the deployment of the workflow %s, stopping it.",
            workflow.id,
        )
        await message_bus.handle(
            [StopWorkflowDeploymentCommand(workflow.id, error=str(err))]
        )
    if message:
        await message_bus.handle([message])


async def is_started(
    event: WorkflowDeployedSuccessfully,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
    messaging_publisher: IPublisherService = Provide[
        ApplicationContainer.message_publisher
    ],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    try:
        with uow:
            workflow = uow.workflow_repository.get(event.workflow_deployment_id)
            try:
                await addons.run_hook_workflow_deployment_is_started(workflow)
            except Exception as err:
                logger.exception(f"Addon failed with exception {err}")
            workflow.set_running()
            await messaging_publisher.publish([event])
            uow.commit()
    except Exception as err:
        logger.exception(
            "An unexpected error occurred during the deployment of the workflow %s, stopping it.",
            workflow.id,
        )
        await message_bus.handle(
            [StopWorkflowDeploymentCommand(workflow.id, error=str(err))]
        )


async def trigger_next_execution(
    event: TriggerNextExecutionCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    """Create a new executions if next executions are present"""

    new_execution_commands: List[BaseMessage] = []
    with uow:
        execution_result = uow.execution_result_repository.get(
            event.execution_result_id
        )
        # Stop the execution propagation if the result state is error
        if execution_result.state == ExecutionResultState.ERROR:
            logger.debug(
                "Execution %s propagation canceled because the execution is in Error",
                execution_result.id,
            )
            await message_bus.handle(
                [WorkflowRunIsCompletedEvent(execution_result.workflow_run_id)]
            )
            return

        if execution_result.workflow_action is None:
            await message_bus.handle(
                [WorkflowRunIsCompletedEvent(execution_result.workflow_run_id)]
            )
            raise Exception(
                f"The execution result workflow action is empty! Execution result: {execution_result}"
            )

        workflow = uow.workflow_repository.get(
            execution_result.workflow_action.workflow_deployment_id
        )
        # Stop event propagation if the workflow is stopped (continue if graceful stopped)
        if workflow.state == WorkflowDeploymentState.STOPPED:
            logger.debug(
                "Execution %s propagation canceled because the workflow %s is stopped",
                execution_result.id,
                workflow.id,
            )
            await message_bus.handle(
                [WorkflowRunIsCompletedEvent(execution_result.workflow_run_id)]
            )
            return
        if len(execution_result.workflow_action.next_actions) == 0:
            await message_bus.handle(
                [WorkflowRunIsCompletedEvent(execution_result.workflow_run_id)]
            )
        else:
            for next_action in execution_result.workflow_action.next_actions:
                new_execution_commands.append(
                    CreateExecutionConnectionCommand(
                        action_definition_id=next_action.definition.id,
                        workflow_action_id=next_action.id,
                        parent_execution_result_id=execution_result.id,
                        project_id=execution_result.project_id,
                    )
                )
            uow.commit()
        await message_bus.handle(new_execution_commands)


async def stop(
    cmd: Union[
        StopWorkflowDeploymentCommand, StopWorkflowDeploymentFromDefinitionIdCommand
    ],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    publisher: IPublisherService = Provide[ApplicationContainer.message_publisher],
) -> bool:
    """
    Stop the execution and undeploy the root action of the workflow.
    """
    messages: list[BaseMessage] = []
    with uow:
        if isinstance(cmd, StopWorkflowDeploymentFromDefinitionIdCommand):
            try:
                workflow = uow.workflow_repository.get_eventually_running_workflow_by_workflow_definition_id(
                    cmd.workflow_definition_id
                )
            except WorkflowNotFoundError:
                logger.warning(
                    f"Trying to undeploy non-existent workflow, respond anyway: {cmd}"
                )
                await publisher.publish(
                    [WorkflowUndeployedSuccessfully(cmd.workflow_definition_id)]
                )
                return False

        else:
            try:
                workflow = uow.workflow_repository.get(cmd.workflow_id)
            except WorkflowNotFoundError:
                logger.warning(f"Trying to undeploy non-existent workflow, {cmd}")
                return False

        if cmd.graceful is True:
            workflow.set_stopped_gracefully()
            root_action = workflow.get_root_action()
            connections = (
                uow.execution_connection_repository.list_active_from_workflow_action_id(
                    root_action.id
                )
            )
            if connections:
                assert len(connections) == 1
                messages.append(StopExecutionConnectionCommand(connections[0].id))
            del root_action
        else:
            workflow.set_stopped()
            for workflow_action in workflow.actions:
                connections = uow.execution_connection_repository.list_active_from_workflow_action_id(
                    workflow_action.id
                )
                for connection in connections:
                    messages.append(StopExecutionConnectionCommand(connection.id))
        messages.append(
            WorkflowStoppedEvent(
                workflow.workflow_definition_id,
                workflow.id,
                error=cmd.error,
                graceful=cmd.graceful,
            )
        )

        uow.commit()

    await message_bus.handle(messages)
    return True


async def on_stopped(
    event: WorkflowStoppedEvent,
    publisher: IPublisherService = Provide[ApplicationContainer.message_publisher],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
) -> None:
    with uow:
        workflow = uow.workflow_repository.get(event.workflow_deployment_id)
        try:
            await addons.run_hook_workflow_deployment_is_stopped(workflow)
        except Exception as err:
            logger.exception(f"Addon failed with exception {err}")
        await publisher.publish(
            [
                WorkflowUndeployedSuccessfully(
                    workflow.workflow_definition_id, error=event.error
                )
            ]
        )


async def delete(
    cmd: DeleteWorkflowDeploymentCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        workflow = uow.workflow_repository.get(cmd.workflow_id)
        if not workflow.is_stopped():
            raise Exception("Impossible to delete a workflow which is not stopped")
        uow.workflow_repository.delete(workflow.id)
        uow.commit()


async def on_startup(
    _: ApplicationStartupEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    messages: List[BaseMessage] = []
    with uow:
        workflows = uow.workflow_repository.get_all_paused_or_running()
        for workflow in workflows:
            workflow.workflow_gauge_wih_labels().inc()
            logger.debug(
                "workflow %s needs to be restarted, state at startup %s",
                workflow.id,
                workflow.state.value,
            )
            root_workflow_action = workflow.get_root_action()
            if root_workflow_action.connection_execution_id is None:
                messages.append(StartWorkflowDeploymentCommand(workflow_id=workflow.id))
            else:
                root_execution = uow.execution_connection_repository.get(
                    root_workflow_action.connection_execution_id
                )
                if root_execution.is_running():
                    workflow.set_running()
                    uow.commit()
                else:
                    messages.append(
                        StartWorkflowDeploymentCommand(workflow_id=workflow.id)
                    )
    await message_bus.handle(messages)


async def pause_all(
    _: StopApplicationCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        workflows = uow.workflow_repository.get_all_running()
        for workflow in workflows:
            workflow.set_paused()
        uow.commit()
