# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import multiprocessing
from logging import getLogger

from dependency_injector.wiring import Provide

from ryax.common.application.message_bus import MessageBus
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionUpdatedEvent,
    ExecutionConnectionPendingEvent,
    ExecutionConnectionRetryIfNeededEvent,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultCompletedEvent,
    ExecutionResultInitializedEvent,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_events import (
    WorkflowStoppedEvent,
)
from ryax.runner.domain.workflow_run.workflow_run_commands import (
    CreateWorkflowRunCommand,
    DeleteWorkflowRunsCommand,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import WorkflowRun
from ryax.runner.domain.workflow_run.workflow_run_values import WorkflowRunState

logger = getLogger(__name__)


async def create(
    cmd: CreateWorkflowRunCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> str:
    """Create a workflow run and return its id"""
    with uow:
        workflow = uow.workflow_repository.get(cmd.workflow_deployment_id)
        workflow_run = WorkflowRun.create_from_workflow_deployment(workflow)
        uow.workflow_run_repository.add(workflow_run)
        uow.commit()
    return workflow_run.id


async def on_execution_result_created(
    event: ExecutionResultInitializedEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    enable_retry_config: bool = Provide[
        ApplicationContainer.configuration.action_retry.enabled
    ],
) -> None:
    with uow:
        execution_result = uow.execution_result_repository.get(
            event.execution_result_id
        )
        workflow_run = uow.workflow_run_repository.get(execution_result.workflow_run_id)
        execution_connection = uow.execution_connection_repository.get(
            execution_result.execution_connection_id
        )
        is_retry_enabled = enable_retry_config and execution_connection.retry_enabled()
        workflow_run.execution_result_created(execution_result, is_retry_enabled)
        uow.commit()


async def on_execution_result_completed(
    event: ExecutionResultCompletedEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    enable_retry_config: bool = Provide[
        ApplicationContainer.configuration.action_retry.enabled
    ],
) -> None:
    with uow:
        execution_result = uow.execution_result_repository.get(
            event.execution_result_id
        )
        workflow_run = uow.workflow_run_repository.get(execution_result.workflow_run_id)
        execution_connection = uow.execution_connection_repository.get(
            execution_result.execution_connection_id
        )
        is_retry_enabled = enable_retry_config and execution_connection.retry_enabled()

        workflow_run.execution_result_finished(execution_result, is_retry_enabled)
        uow.commit()


async def on_execution_connection_created(
    event: ExecutionConnectionPendingEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    enable_retry_config: bool = Provide[
        ApplicationContainer.configuration.action_retry.enabled
    ],
) -> None:
    with uow:
        execution_connection = uow.execution_connection_repository.get(
            event.execution_connection_id
        )
        # No workflow run id means that this connection comes from a trigger, ignore it
        if execution_connection.workflow_run_id is not None:
            workflow_run = uow.workflow_run_repository.get(
                execution_connection.workflow_run_id
            )
            is_retry_enabled = (
                enable_retry_config and execution_connection.retry_enabled()
            )
            workflow_run.connection_created(execution_connection, is_retry_enabled)
            uow.commit()


async def on_execution_connection_updated(
    event: ExecutionConnectionUpdatedEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    enable_retry_config: bool = Provide[
        ApplicationContainer.configuration.action_retry.enabled
    ],
) -> None:
    with uow:
        execution_connection = uow.execution_connection_repository.get(
            event.execution_connection_id
        )
        # No workflow run id means that this connection comes from a trigger, ignore it
        if execution_connection.workflow_run_id is not None:
            workflow_run = uow.workflow_run_repository.get(
                execution_connection.workflow_run_id
            )
            if (
                execution_connection.state
                not in ExecutionConnectionState.eventually_running_states()
            ):
                is_retry_enabled = (
                    enable_retry_config and execution_connection.retry_enabled()
                )
                # If not enabled retry, the workflow run state should be updated regardless it need retry or not.
                # If enabled retry, the whole workflow run state will be finally calculated by the retry handler. The single action run state is updated here.
                workflow_run.connection_has_ended(
                    execution_connection, is_retry_enabled
                )
                uow.commit()

                if is_retry_enabled:
                    await message_bus.handle(
                        [ExecutionConnectionRetryIfNeededEvent(execution_connection.id)]
                    )


async def on_stop(
    event: WorkflowStoppedEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    if not event.graceful:
        with uow:
            workflow_runs = uow.workflow_run_repository.list_all(
                workflow_definition_id=event.workflow_definition_id,
                state=WorkflowRunState.RUNNING,
            )
            for workflow_run in workflow_runs:
                workflow_run.stop()
            uow.commit()


async def delete(
    cmd: DeleteWorkflowRunsCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    garbage_collector_service_queue: multiprocessing.Queue = Provide[
        ApplicationContainer.garbage_collector_queue
    ],
) -> None:
    with uow:
        workflow_runs = uow.workflow_run_repository.list_all(
            workflow_definition_id=cmd.workflow_definition_id
        )
        garbage_collector_service_queue.put(
            [run.id for run in workflow_runs], block=False
        )
