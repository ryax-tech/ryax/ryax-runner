# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger

from dependency_injector.wiring import Provide

from ryax.runner.application.action_logs_service import ActionLogsService
from ryax.common.application.message_bus import MessageBus
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_result.execution_result_commands import (
    TriggerNextExecutionCommand,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultNewLogLinesToBeAddedEvent,
    ExecutionResultCompletedEvent,
    ExecutionResultFinishedEvent,
)
from ryax.runner.domain.stored_file.stored_file_commands import CreateStoredFileCommand

logger = getLogger(__name__)


async def finish(
    event: ExecutionResultFinishedEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        execution_result = uow.execution_result_repository.get(event.id)
        execution_result_id = execution_result.id
        execution_connection = uow.execution_connection_repository.get(
            execution_result.execution_connection_id
        )
        execution_result.update_on_execution_result_finished(
            event,
            parent_execution_result=execution_connection.parent_execution_result,
        )
        uow.commit()
        stored_file_ids = []
        for output_file in event.output_files:
            stored_file_ids.append(
                await message_bus.handle_command(
                    CreateStoredFileCommand(
                        io_name=output_file.output_name,
                        file_path=output_file.file_path,
                        project_id=execution_connection.project_id,
                        size_in_bytes=output_file.size_in_bytes,
                        mimetype=output_file.mimetype,
                        encoding=output_file.encoding,
                    )
                )
            )

        execution_result.stored_files = [
            uow.stored_file_repository.get(
                stored_file_id, execution_connection.project_id
            )
            for stored_file_id in stored_file_ids
        ]
        uow.commit()
        del execution_result
    await message_bus.handle(
        [
            ExecutionResultCompletedEvent(execution_result_id),
            TriggerNextExecutionCommand(execution_result_id),
        ]
    )


async def on_new_immediate_execution_log_line(
    event: ExecutionResultNewLogLinesToBeAddedEvent,
    action_log_service: ActionLogsService = Provide[
        ApplicationContainer.action_logs_service
    ],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    """Register new log line immediately in the execution"""
    if not event.log_lines:
        return
    with uow:
        execution_result = uow.execution_result_repository.get(
            event.execution_result_id
        )
        await action_log_service.append(
            associated_id=event.execution_result_id,
            project_id=execution_result.project_id,
            log_lines=event.log_lines,
            uow=uow,
        )
        uow.commit()
