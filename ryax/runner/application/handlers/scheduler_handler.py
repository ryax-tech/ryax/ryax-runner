from dependency_injector.wiring import Provide

from ryax.runner.application.scheduler import MultiObjectiveScheduler
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentUndeployedEvent,
    ActionDeploymentAvailableEvent,
    ActionDeploymentResourcesAvailableEvent,
    ActionDeploymentNoMoreResourcesAvailableEvent,
    ActionDeploymentErroredEvent,
    ActionDeploymentIsRunningEvent,
)
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionPendingEvent,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_events import (
    WorkflowStoppedEvent,
)


async def on_workflow_stopped(
    event: WorkflowStoppedEvent,
    scheduler: MultiObjectiveScheduler = Provide[ApplicationContainer.scheduler],
) -> None:
    await scheduler.schedule()


async def on_connection_is_pending(
    pending_connection: ExecutionConnectionPendingEvent,  # noqa
    scheduler: MultiObjectiveScheduler = Provide[ApplicationContainer.scheduler],
) -> None:
    """
    Called whenever an ExecutionConnection is changing its state to PENDING.
    """
    await scheduler.schedule()


async def on_action_deployment_is_available(
    event: ActionDeploymentAvailableEvent,
    scheduler: MultiObjectiveScheduler = Provide[ApplicationContainer.scheduler],
) -> None:
    """
    Called whenever an ActionDeployment is changing its state to READY.
    """
    await scheduler.schedule()


async def on_action_deployment_is_running(
    event: ActionDeploymentIsRunningEvent,
    scheduler: MultiObjectiveScheduler = Provide[ApplicationContainer.scheduler],
) -> None:
    """
    Called whenever an ActionDeployment is changing its state to RUNNING.
    """
    await scheduler.schedule()


async def on_action_deployment_undeployed(
    event: ActionDeploymentUndeployedEvent,
    scheduler: MultiObjectiveScheduler = Provide[ApplicationContainer.scheduler],
) -> None:
    """
    Called whenever an ActionDeployment is undeployed.
    """
    await scheduler.schedule()


async def on_no_more_resources_available(
    event: ActionDeploymentNoMoreResourcesAvailableEvent,
    scheduler: MultiObjectiveScheduler = Provide[ApplicationContainer.scheduler],
) -> None:
    """
    Called whenever an ActionDeployment is changing its state to READY.
    """
    await scheduler.schedule()


async def on_resources_available(
    event: ActionDeploymentResourcesAvailableEvent,
    scheduler: MultiObjectiveScheduler = Provide[ApplicationContainer.scheduler],
) -> None:
    """
    Called whenever an ActionDeployment is changing its state to READY.
    """
    await scheduler.schedule()


async def deployment_error(
    event: ActionDeploymentErroredEvent,
    scheduler: MultiObjectiveScheduler = Provide[ApplicationContainer.scheduler],
) -> None:
    """
    Called whenever an ActionDeployment is in Error.
    """
    await scheduler.schedule()
