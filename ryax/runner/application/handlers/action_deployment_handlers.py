# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
from logging import getLogger
from typing import List

from dependency_injector.wiring import Provide

from ryax.common.application.message_bus import MessageBus
from ryax.common.application.utils import retry_with_backoff
from ryax.runner.application.reference_solver_service import ReferenceSolverService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    CreateActionDeploymentCommand,
    UndeployActionDeploymentCommand,
    WatchActionDeploymentStateCommand,
    UpdateActionDeploymentCommand,
    ErrorActionDeploymentCommand,
    SetRunningActionDeploymentCommand,
    DeallocateActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
    ActionDeploymentStateMachine,
)
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentAppliedEvent,
    ActionDeploymentAvailableEvent,
    ActionDeploymentIsReadyEvent,
    ActionDeploymentUndeployedEvent,
    ActionDeploymentUndeployingEvent,
    ActionDeploymentNoMoreResourcesAvailableEvent,
    ActionDeploymentResourcesAvailableEvent,
    ActionDeploymentCreatedEvent,
    ActionDeploymentIsDeployingEvent,
    ActionDeploymentErroredEvent,
)
from ryax.runner.domain.action_deployment.action_deployment_service import (
    IActionDeploymentService,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.addons.addon_entities import RyaxAddOns
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.exceptions import ActionDeploymentError, ActionUndeploymentError
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    StartExecutionConnectionCommand,
    DeallocateExecutionConnectionCommand,
)
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupEvent,
    BaseMessage,
    StopApplicationCommand,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    StopWorkflowDeploymentCommand,
)

logger = getLogger(__name__)


def get_deploy_service(
    action_deployment: ActionDeployment,
    # WARNING: Inject the container and not the factory aggregate, or it fails to resolve dependencies
    container: ApplicationContainer = Provide[ApplicationContainer],
) -> IActionDeploymentService:
    type_ = action_deployment.get_deployment_type()
    deploy_service = container.action_deployment_factory(type_)
    return deploy_service


@ActionDeploymentStateMachine.state_machine(
    from_states=[ActionDeploymentState.NONE],
    to_states=[ActionDeploymentState.CREATED],
)
async def create(
    cmd: CreateActionDeploymentCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    reference_solver: ReferenceSolverService = Provide[
        ApplicationContainer.reference_solver
    ],
) -> str:
    """
    Create an action deployment from the given command.
    :return: the action deployment id
    """
    with uow:
        action_definition = uow.action_definition_repository.get(
            cmd.action_definition_id
        )
        node_pool = uow.site_repository.get_node_pool(cmd.node_pool_id)
        action_deployment = ActionDeployment.create_from_apply_command(
            cmd, action_definition, node_pool
        )
        uow.action_deployment_repository.add(action_deployment)
        action_deployment.set_created()

        # Solve reference and static values in the addons
        action_deployment.addons = reference_solver.resolve_addons(
            execution_connection_id=cmd.execution_connection_id
        )
        uow.commit()
        action_deployment_id = action_deployment.id
    await message_bus.handle([ActionDeploymentCreatedEvent(action_deployment_id)])
    return action_deployment_id


@ActionDeploymentStateMachine.state_machine(
    from_states=[ActionDeploymentState.CREATED],
    to_states=[ActionDeploymentState.DEPLOYING],
)
async def apply(
    event: ActionDeploymentCreatedEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            event.action_deployment_id
        )
        action_deployment.set_deploying()
        uow.commit()
    await message_bus.handle(
        [ActionDeploymentIsDeployingEvent(event.action_deployment_id)]
    )


@ActionDeploymentStateMachine.state_machine(
    from_states=[ActionDeploymentState.DEPLOYING],
    to_states=[ActionDeploymentState.DEPLOYING, ActionDeploymentState.ERROR],
)
async def do_apply(
    event: ActionDeploymentIsDeployingEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
) -> None:
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            event.action_deployment_id, eager_load=True
        )
        logger.debug("Deploying action: %s", action_deployment)
        deploy_service = get_deploy_service(action_deployment)
        try:
            try:
                addons_extra_params = (
                    await addons.run_hook_action_before_deploy_on_runner(
                        action_deployment
                    )
                )
            except Exception as err:
                logger.exception(f"Addon failed with exception {err}")
            logger.debug(
                "Calling apply of action %s with addons_extra_params %s",
                action_deployment.id,
                addons_extra_params,
            )
            await deploy_service.deploy(action_deployment, addons_extra_params)

            try:
                await addons.run_hook_action_after_deploy_on_runner(
                    action_deployment, deploy_service
                )
            except Exception as err:
                logger.exception(f"Addon failed with exception {err}")
        except Exception as err:
            await message_bus.handle_command(
                ErrorActionDeploymentCommand(
                    action_deployment_id=action_deployment.id,
                    error_message=f"Unexpected error while deploying Action: {err.__class__.__name__}: {err}",
                ),
                blocking=False,
            )
            raise

        uow.commit()
        action_id = action_deployment.id
    await message_bus.handle([ActionDeploymentAppliedEvent(action_id)])


@ActionDeploymentStateMachine.state_machine(
    from_states=[
        *ActionDeploymentState.eventually_ready_states(),
    ],
    to_states=[
        ActionDeploymentState.ERROR,
        ActionDeploymentState.UNDEPLOYED,
        ActionDeploymentState.NO_RESOURCES_AVAILABLE,
        ActionDeploymentState.DEPLOYING,
        ActionDeploymentState.READY,
        ActionDeploymentState.RUNNING,
    ],
)
async def on_startup(
    _: ApplicationStartupEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    messages: List[BaseMessage] = []
    with uow:
        action_deployments = uow.action_deployment_repository.list_all_in_progress()
        for action_deployment in action_deployments:
            action_deployment.action_gauge_with_labels().inc()
            if action_deployment.state in [
                ActionDeploymentState.UNDEPLOYED,
                ActionDeploymentState.ERROR,
                ActionDeploymentState.NONE,
            ]:
                logger.warning(
                    f"Unsupported state {action_deployment.state} on startup for action deployment"
                    f" {action_deployment.id}"
                )
            elif action_deployment.state == ActionDeploymentState.UNDEPLOYING:
                # If action are undeploying restart the undeploy process
                messages.append(UndeployActionDeploymentCommand(action_deployment.id))
                continue

            deploy_service = get_deploy_service(action_deployment)
            try:
                real_state = await deploy_service.get_current_state(action_deployment)
            except ActionDeploymentError:
                logger.exception(
                    f"Error while getting action deployment state on startup watch it to restore its: {action_deployment}"
                )
                continue
            logger.debug(
                "Real state of action %s is %s", action_deployment.id, real_state
            )
            if real_state == ActionDeploymentState.ERROR:
                action_deployment.set_error()
            elif real_state == ActionDeploymentState.UNDEPLOYED:
                action_deployment.set_undeployed()
            elif real_state == ActionDeploymentState.NO_RESOURCES_AVAILABLE:
                action_deployment.set_no_resources_available()
                messages.append(
                    ActionDeploymentNoMoreResourcesAvailableEvent(action_deployment.id)
                )
                messages.append(WatchActionDeploymentStateCommand(action_deployment.id))
            elif real_state == ActionDeploymentState.RUNNING:
                # Its already running, just watch the deployment
                action_deployment.set_running()
                messages.append(WatchActionDeploymentStateCommand(action_deployment.id))
            elif real_state in [
                ActionDeploymentState.DEPLOYING,
                ActionDeploymentState.READY,
            ]:
                action_deployment.allocated_connection_id = None
                action_deployment.set_state(real_state)
                messages.append(WatchActionDeploymentStateCommand(action_deployment.id))
            uow.commit()
    await message_bus.handle(messages)


async def on_applied(
    event: ActionDeploymentAppliedEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    cmd = WatchActionDeploymentStateCommand(event.action_deployment_id)
    await message_bus.handle([cmd])


async def watch(
    cmd: WatchActionDeploymentStateCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            cmd.action_deployment_id
        )
        deploy_service = get_deploy_service(action_deployment)
        await deploy_service.watch(action_deployment)
        uow.commit()


@ActionDeploymentStateMachine.state_machine(
    from_states=[
        ActionDeploymentState.RUNNING,
        ActionDeploymentState.DEPLOYING,
        ActionDeploymentState.READY,
        ActionDeploymentState.NO_RESOURCES_AVAILABLE,
        # Happen sometimes if the action deployment was loss (network issue) and comes back
        ActionDeploymentState.UNDEPLOYED,
        ActionDeploymentState.ERROR,
    ],
    to_states=[ActionDeploymentState.READY, ActionDeploymentState.RUNNING],
)
async def on_is_ready(
    cmd: ActionDeploymentIsReadyEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    messages: List[BaseMessage] = []
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            cmd.action_deployment_id
        )
        if action_deployment.allocated_connection_id is None:
            action_deployment.set_ready()
            messages.append(ActionDeploymentAvailableEvent(action_deployment.id))
        else:
            assert action_deployment.allocated_connection_id is not None
            if action_deployment.state == ActionDeploymentState.RUNNING:
                logger.debug(
                    "Nothing to do, %s is already RUNNING and allocated to %s",
                    action_deployment.id,
                    action_deployment.allocated_connection_id,
                )
                return
            action_deployment.set_running()
            messages.append(
                StartExecutionConnectionCommand(
                    execution_connection_id=action_deployment.allocated_connection_id,
                )
            )
        uow.commit()
    await message_bus.handle(messages)


@ActionDeploymentStateMachine.state_machine(
    from_states=[ActionDeploymentState.READY, ActionDeploymentState.RUNNING],
    to_states=[ActionDeploymentState.RUNNING],
)
async def set_running(
    cmd: SetRunningActionDeploymentCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            cmd.action_deployment_id
        )
        action_deployment.set_running()
        uow.commit()


async def deallocate(
    cmd: DeallocateActionDeploymentCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            cmd.action_deployment_id
        )
        action_deployment.deallocate()

        # If the action deployment was running mark it as ready else keep actual state
        if action_deployment.state == ActionDeploymentState.RUNNING:
            await message_bus.handle(
                [ActionDeploymentIsReadyEvent(action_deployment.id)]
            )
        uow.commit()


@ActionDeploymentStateMachine.state_machine(
    from_states=[
        ActionDeploymentState.DEPLOYING,
        ActionDeploymentState.READY,
        ActionDeploymentState.NO_RESOURCES_AVAILABLE,
    ],
    to_states=[
        ActionDeploymentState.READY,
        ActionDeploymentState.RUNNING,
        ActionDeploymentState.DEPLOYING,
        ActionDeploymentState.UNDEPLOYED,
        ActionDeploymentState.NO_RESOURCES_AVAILABLE,
    ],
)
async def update(
    command: UpdateActionDeploymentCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    logger.debug(
        f"Updating Action deployment {command.action_deployment_id} with state '{command.state}'. Logs: {command.logs}"
    )
    messages: list[BaseMessage] = []
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            command.action_deployment_id, eager_load=False
        )
        # If the previous state was no more resources available and the current is not, inform the scheduler
        logger.debug("Action deployment previous state: %s", action_deployment.state)
        if (
            action_deployment.state == ActionDeploymentState.NO_RESOURCES_AVAILABLE
            and command.state != ActionDeploymentState.NO_RESOURCES_AVAILABLE
        ):
            messages.append(
                ActionDeploymentResourcesAvailableEvent(
                    action_deployment_id=action_deployment.id
                )
            )
        if command.state == ActionDeploymentState.DEPLOYING:
            action_deployment.error_message = command.logs
            action_deployment.set_deploying()
        elif command.state == ActionDeploymentState.ERROR:
            messages.append(
                ErrorActionDeploymentCommand(
                    action_deployment_id=action_deployment.id,
                    error_message=command.logs,
                )
            )
        elif command.state == ActionDeploymentState.UNDEPLOYED:
            action_deployment.set_undeployed()
        elif command.state == ActionDeploymentState.READY:
            messages.append(ActionDeploymentIsReadyEvent(action_deployment.id))
        elif command.state == ActionDeploymentState.NO_RESOURCES_AVAILABLE:
            action_deployment.error_message = command.logs
            action_deployment.set_no_resources_available()
            messages.append(
                ActionDeploymentNoMoreResourcesAvailableEvent(
                    action_deployment_id=action_deployment.id
                )
            )
        elif command.state in [
            ActionDeploymentState.CREATED,
            ActionDeploymentState.RUNNING,
            ActionDeploymentState.UNDEPLOYING,
            ActionDeploymentState.NONE,
        ]:
            raise Exception(
                f"Trying to update the state of a ActionDeployment to a non-handled"
                f" state: {command}"
            )
        uow.commit()
    if messages:
        await message_bus.handle(messages, blocking_commands=False)


async def no_more_resource_timeout(
    event: ActionDeploymentNoMoreResourcesAvailableEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    deployment_timeout_in_seconds: int = Provide[
        ApplicationContainer.configuration.deployment.k8s.timeout
    ],
) -> None:
    """If a deployment has not enough resources wait for either more resource or timeout and set in error"""
    new_resources_task = message_bus.wait_for_event(
        ActionDeploymentResourcesAvailableEvent,
        {"action_deployment_id": event.action_deployment_id},
    )
    try:
        await asyncio.wait_for(
            new_resources_task, timeout=deployment_timeout_in_seconds
        )
    except asyncio.TimeoutError:
        await message_bus.handle_command(
            ErrorActionDeploymentCommand(
                event.action_deployment_id,
                error_message=f"Timeout waiting for the Action Deployment ({deployment_timeout_in_seconds}s)",
            ),
            blocking=False,
        )


@ActionDeploymentStateMachine.state_machine(
    from_states=[
        ActionDeploymentState.DEPLOYING,
        ActionDeploymentState.READY,
        ActionDeploymentState.NO_RESOURCES_AVAILABLE,
    ],
    to_states=[ActionDeploymentState.ERROR],
)
async def on_error(
    command: ErrorActionDeploymentCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    messages: list[BaseMessage] = []
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            command.action_deployment_id,
        )

        if action_deployment.allocated_connection_id is not None:
            messages.append(
                DeallocateExecutionConnectionCommand(
                    execution_connection_id=action_deployment.allocated_connection_id,
                    action_deployment_id=action_deployment.id,
                )
            )
            # Also stop the workflow associated if it is a trigger deployment that fails
            if action_deployment.action_definition.is_a_trigger():
                execution = uow.execution_connection_repository.get(
                    action_deployment.allocated_connection_id
                )
                messages.append(
                    StopWorkflowDeploymentCommand(
                        execution.workflow_action.workflow_deployment_id,
                        error=command.error_message,
                    )
                )
        action_deployment.error_message = command.error_message
        action_deployment.set_error()
        uow.commit()

    await message_bus.handle(
        messages
        + [
            ActionDeploymentErroredEvent(command.action_deployment_id),
            UndeployActionDeploymentCommand(command.action_deployment_id),
        ],
        blocking_commands=False,
    )


@ActionDeploymentStateMachine.state_machine(
    from_states=[
        *ActionDeploymentState.eventually_ready_states(),
        ActionDeploymentState.ERROR,
    ],
    to_states=[ActionDeploymentState.UNDEPLOYING],
)
async def delete(
    cmd: UndeployActionDeploymentCommand,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    addons: RyaxAddOns = Provide[ApplicationContainer.addons],
) -> None:
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            cmd.action_deployment_id
        )

        deploy_service = get_deploy_service(action_deployment)
        try:
            await addons.run_hook_action_deployment_delete_on_runner(
                action_deployment, deploy_service
            )
        except Exception as err:
            logger.exception(f"Addon failed with exception {err}")
        action_deployment.set_undeploying()
        uow.commit()

    await message_bus.handle([ActionDeploymentUndeployingEvent(action_deployment.id)])


@ActionDeploymentStateMachine.state_machine(
    from_states=[ActionDeploymentState.UNDEPLOYING],
    to_states=[ActionDeploymentState.UNDEPLOYING, ActionDeploymentState.ERROR],
)
@retry_with_backoff(retries=10, backoff_in_ms=2_000, max_backoff_in_ms=10_000)
async def on_undeploying(
    event: ActionDeploymentUndeployingEvent,
    message_bus: MessageBus = Provide[ApplicationContainer.message_bus],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    retry_delay_seconds: int = 10,
) -> None:
    try:
        with uow:
            action_deployment = uow.action_deployment_repository.get_by_id(
                event.action_deployment_id
            )
            deploy_service = get_deploy_service(action_deployment)
            await deploy_service.unwatch(action_deployment)
            await deploy_service.delete(action_deployment)
        await message_bus.handle(
            [
                ActionDeploymentUndeployedEvent(
                    action_deployment_id=event.action_deployment_id
                )
            ]
        )
    except (ActionUndeploymentError, Exception) as err:
        raise ActionUndeploymentError(
            f"Action deployment '{action_deployment.id}' with action definition id '{action_deployment.action_definition.id}' undeployment failed. Retrying in {retry_delay_seconds} seconds"
        ) from err


@ActionDeploymentStateMachine.state_machine(
    from_states=[ActionDeploymentState.UNDEPLOYING],
    to_states=[ActionDeploymentState.UNDEPLOYED],
)
async def is_stopped(
    cmd: ActionDeploymentUndeployedEvent,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        action_deployment = uow.action_deployment_repository.get_by_id(
            cmd.action_deployment_id
        )
        action_deployment.set_undeployed()
        uow.commit()


async def unwatch_all(
    cmd: StopApplicationCommand,
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
) -> None:
    with uow:
        action_deployments = uow.action_deployment_repository.list_all_in_progress()
        for action_deployment in action_deployments:
            deploy_service = get_deploy_service(action_deployment)
            await deploy_service.unwatch(action_deployment)
