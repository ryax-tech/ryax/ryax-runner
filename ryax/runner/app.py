# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.


import asyncio
import gc
import logging
import os
import tracemalloc

# FIXME: Remove this when this issue is fixed
from ryax.common.infrastructure.messaging.patch import (
    patch_spanbuilder_set_channel,
)  # noqa

patch_spanbuilder_set_channel()

import concurrent.futures
import aiomultiprocess
import multiprocessing
from aiohttp import web
from dependency_injector.wiring import inject
from opentelemetry.instrumentation.logging import LoggingInstrumentor

from ryax.runner.application.setup import setup as application_setup
from ryax.runner.container import ApplicationContainer
from ryax.common.domain.internal_messaging.base_messages import (
    ApplicationStartupEvent,
    StopApplicationCommand,
)
from ryax.runner.infrastructure.api.setup import setup as api_setup
from ryax.runner.infrastructure.messaging.setup import setup as messaging_setup
from ryax.common.infrastructure.monitoring.opentelemetry_tracer import (
    OpenTelemetryTracer,
)

from .version import __version__

logger = logging.getLogger(__name__)
mem_logger = logging.getLogger("ryax.performance.memory")


def configure() -> ApplicationContainer:
    # Init application container
    container = ApplicationContainer()

    # First get config file path
    container.configuration.config_file.from_env("RYAX_CONFIG", "./config.yaml")
    # Load config file
    container.configuration.from_yaml(container.configuration.config_file())

    # Override with env variables
    if not container.configuration.log_level():
        container.configuration.log_level.from_env("RYAX_LOG_LEVEL", "INFO")
    if not container.configuration.database.url():
        container.configuration.database.url.from_env("RYAX_DATASTORE", required=True)
    if not container.configuration.messaging.url():
        container.configuration.messaging.url.from_env("RYAX_BROKER", required=True)
    if not container.configuration.undeploy_idle_actions_after_seconds():
        container.configuration.undeploy_idle_actions_after_seconds.from_env(
            "RYAX_UNDEPLOY_IDLE_ACTIONS_AFTER_SECONDS", default=30, as_=int
        )
    if not container.configuration.scheduler_retry_delay_seconds():
        container.configuration.scheduler_retry_delay_seconds.from_env(
            "RYAX_SCHEDULER_RETRY_DELAY_SECONDS", default=10, as_=int
        )
    if not container.configuration.scheduler.max_action_deployments():
        container.configuration.scheduler.max_action_deployments.from_env(
            "RYAX_SCHEDULER_MAX_ACTION_DEPLOYMENTS", default=50, as_=int
        )
    if not container.configuration.scheduler.max_concurrent_deploying_actions():
        container.configuration.scheduler.max_concurrent_deploying_actions.from_env(
            "RYAX_SCHEDULER_MAX_CONCURRENT_DEPLOYING_ACTIONS", default=3, as_=int
        )

    if not container.configuration.default_time_allotment_in_seconds():
        container.configuration.default_time_allotment_in_seconds.from_env(
            "RYAX_DEFAULT_TIME_ALLOTMENT_IN_SECONDS", default=900.0, as_=float
        )
    # Action retry configurations. If not present in RYAX_CONFIG, will use env vars
    if not container.configuration.action_retry.collect_oom_range_length_seconds():
        container.configuration.action_retry.collect_oom_range_length_seconds.from_env(
            "RYAX_ACTION_RETRY_COLLECT_OOM_RANGE_LENGTH_SECONDS", default=15, as_=int
        )
    if not container.configuration.action_retry.collect_oom_end_waiting_seconds():
        container.configuration.action_retry.collect_oom_end_waiting_seconds.from_env(
            "RYAX_ACTION_RETRY_COLLECT_OOM_END_WAITING_SECONDS", default=10, as_=int
        )
    if not container.configuration.action_retry.max_error():
        container.configuration.action_retry.max_error.from_env(
            "RYAX_ACTION_RETRY_MAX_ERROR", default=5, as_=int
        )
    if not container.configuration.action_retry.enabled():
        container.configuration.action_retry.enabled.from_env(
            "RYAX_ACTION_RETRY_ENABLED", default=True, as_=bool
        )

    # TODO: Add these to helm chart values
    container.configuration.max_number_of_runs_per_project.from_env(
        "RYAX_MAX_RUNS_PER_PROJECTS", default=5000, as_=int
    )
    container.configuration.delay_between_calls_in_seconds.from_env(
        "RYAX_GARBAGE_COLLECTION_FREQUENCY_IN_SEC", default=900, as_=int
    )

    # Deployment configuration
    if not container.configuration.deployment.type():
        container.configuration.deployment.type.from_env("RYAX_DEPLOYMENT_MODE", "k8s")
    if container.configuration.deployment.type() == "k8s":
        if not container.configuration.deployment.k8s.user_namespace():
            container.configuration.deployment.k8s.user_namespace.from_env(
                "RYAX_USER_NAMESPACE", required=True
            )
        if not container.configuration.ryax.registry():
            container.configuration.ryax.registry.from_env(
                "RYAX_INTERNAL_REGISTRY", required=True
            )
        if not container.configuration.deployment.k8s.timeout():
            container.configuration.deployment.k8s.timeout.from_env(
                "RYAX_DEPLOYMENT_K8S_TIMEOUT_SEC", default=900, as_=int
            )

    if not container.configuration.actions.web_access_port():
        container.configuration.actions.web_access_port.from_env(
            "RYAX_DEFAULT_ACTION_WEB_ACCESS_PORT", default=8080, as_=int
        )
    if not container.configuration.actions.grpc_port():
        container.configuration.actions.grpc_port.from_env(
            "RYAX_DEFAULT_ACTION_GRPC_PORT", default=8081, as_=int
        )
    if not container.configuration.actions.log_level():
        container.configuration.actions.log_level.from_env(
            "RYAX_DEFAULT_ACTION_LOG_LEVEL", default="INFO"
        )

    if not container.configuration.deployment.internal.registry():
        container.configuration.ryax.ssh_internal_registry.from_env(
            "RYAX_SSH_INTERNAL_REGISTRY", default="ryax-registry:5000"
        )

    if not container.configuration.authorization.base_url():
        container.configuration.authorization.base_url.from_env(
            "RYAX_AUTHORIZATION_API_BASE_URL", required=True
        )

    # stored_file config
    if not container.configuration.storage.local.type():
        container.configuration.storage.local.type.from_env(
            "RYAX_LOCAL_STORAGE_TYPE", "s3"
        )
    if container.configuration.storage.local.type() == "s3":
        if not container.configuration.storage.local.s3.server():
            container.configuration.storage.local.s3.server.from_env(
                "RYAX_FILESTORE", required=True
            )
        if not container.configuration.storage.local.s3.bucket():
            container.configuration.storage.local.s3.bucket.from_env(
                "RYAX_FILESTORE_BUCKET", "ryax-local-data"
            )
        if not container.configuration.storage.local.s3.access_key():
            container.configuration.storage.local.s3.access_key.from_env(
                "RYAX_FILESTORE_ACCESS_KEY", required=True
            )
        if not container.configuration.storage.local.s3.secret_key():
            container.configuration.storage.local.s3.secret_key.from_env(
                "RYAX_FILESTORE_SECRET_KEY", required=True
            )
    elif container.configuration.storage.local.type() == "posix":
        if not container.configuration.storage.local.posix.root_path():
            container.configuration.storage.local.posix.root_path.from_env(
                "RYAX_DATA_DIR", "./ryax_data"
            )

    container.configuration.user_api.prefix.from_env(
        "RYAX_USER_API_PREFIX", "/user-api"
    )

    # Get secrets from the env variables
    container.configuration.jwt_secret_key.from_env(
        "RYAX_JWT_SECRET_KEY", required=True
    )

    container.configuration.api.base_url.from_env("RYAX_RUNNER_API_BASE_URL", "")

    # Metrics server
    container.configuration.observabiliy.metrics_server_port.from_env(
        "RYAX_METRICS_SERVER_PORT", default=8090, as_=int
    )

    container.configuration.deployment.logs.logs_max_size_in_bytes.from_env(
        "RYAX_ACTION_LOGS_MAX_SIZE_IN_BYTES", default=1024 * 1024, as_=int
    )
    container.configuration.deployment.logs.log_line_max_size_in_bytes.from_env(
        "RYAX_ACTION_LOGS_LINE_MAX_SIZE_IN_BYTES", default=1024, as_=int
    )

    # Encryption Key
    container.configuration.encryption_key.from_env(
        "RYAX_ENCRYPTION_KEY", required=True
    )
    container.configuration.encryption_key.override(
        container.configuration.encryption_key().encode()
    )

    # Setup logging
    # logging.basicConfig(
    #    level=logging.INFO,
    #    format="%(asctime)s.%(msecs)03d %(levelname)-7s %(name)-22s %(message)s",
    #    datefmt="%Y-%m-%d,%H:%M:%S",
    # )
    str_level = container.configuration.log_level()
    numeric_level = getattr(logging, str_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_level)
    LoggingInstrumentor().instrument(set_logging_format=True, log_level=numeric_level)

    logging.getLogger("ryax").setLevel(numeric_level)
    logger = logging.getLogger(__name__)
    logger.info("Logging level is set to %s" % str_level.upper())

    # Change level of  logs for external libs
    container.configuration.logs.sqlalchemy.from_env(
        "RYAX_LOGS_SQLALCHEMY", default="ERROR"
    )
    logging.getLogger("sqlalchemy").setLevel(
        getattr(logging, container.configuration.logs.sqlalchemy().upper())
    )
    # Only see performance logs (SQL timing of each query, and more) when it crosses a threshold.
    container.configuration.logs.performance.from_env(
        "RYAX_LOGS_PERFORMANCE", default="WARNING"
    )
    logging.getLogger("ryax.performance").setLevel(
        getattr(logging, container.configuration.logs.performance().upper())
    )

    # Disable internal lib debug logs by default
    container.configuration.logs.asyncio.from_env(
        "RYAX_LOGS_ASYNCIO", default="WARNING"
    )
    logging.getLogger("asyncio").setLevel(
        getattr(logging, container.configuration.logs.asyncio().upper())
    )
    container.configuration.logs.kubernetes.from_env(
        "RYAX_LOGS_KUBERNETES", default="WARNING"
    )
    logging.getLogger("kubernetes_asyncio").setLevel(
        getattr(logging, container.configuration.logs.kubernetes().upper())
    )
    container.configuration.logs.aiormq.from_env("RYAX_LOGS_AIORMQ", default="WARNING")
    logging.getLogger("aiormq").setLevel(
        getattr(logging, container.configuration.logs.aiormq().upper())
    )
    container.configuration.logs.aio_pika.from_env(
        "RYAX_LOGS_AIO_PIKA", default="WARNING"
    )
    logging.getLogger("aio_pika").setLevel(
        getattr(logging, container.configuration.logs.aio_pika().upper())
    )

    # Do not show the configuration because it contains secrets
    # logger.debug(f"CONFIG: {container.configuration()}")
    container.configuration.enable_memory_profiling.from_env(
        "RYAX_PERFORMANCE_MEMORY_PROFILE_ENABLED", default=False, as_=bool
    )

    return container


def init(container: ApplicationContainer) -> web.Application:
    """Init application"""
    application_setup(container)
    messaging_setup(container)

    app: web.Application = web.Application()
    api_setup(app, container)
    app["container"] = container

    return app


@inject
async def on_startup(app: web.Application) -> None:
    """Hooks for application startup"""
    container: ApplicationContainer = app["container"]

    container.metrics_service().serve()

    await container.addons().run_hook_on_startup(
        container.configuration(),
        container.message_bus(),
        container.workflow_result_service(),
        container.user_auth_service(),
        container.stored_file_view(),
    )
    # Add extra parallel worker in process
    app["workers"] = {}
    garbage_collector = container.garbage_collector_service()
    worker = aiomultiprocess.Worker(target=garbage_collector.main)
    worker.start()
    app["workers"]["garbage_collector"] = worker

    container.database_engine().connect()

    await container.messaging_engine().connect()
    await container.message_consumer().start("RunnerQ")

    if container.configuration.deployment.type() == "k8s":
        k8s_engine = container.k8s_engine()
        await k8s_engine.init()

    if container.configuration.storage.local.type() == "s3":
        s3_engine = container.local_s3_engine()
        s3_engine.connect()
        await s3_engine.create_bucket()

    if container.configuration.enable_memory_profiling():
        app["memory_profiler"] = asyncio.create_task(collect_stats())

    await container.message_bus().handle([ApplicationStartupEvent()])


@inject
async def on_cleanup(app: web.Application) -> None:
    """Define hook when application stop"""
    for name, worker in app["workers"].items():
        if worker.is_alive():
            logger.info("Terminating worker for %s", name)
            worker.kill()

    container: ApplicationContainer = app["container"]
    await container.message_bus().handle_command(StopApplicationCommand())
    await container.message_bus().stop()

    await container.addons().run_hook_on_cleanup()
    container.database_engine().disconnect()
    await container.messaging_engine().disconnect()
    await container.metrics_service().stop()


async def collect_stats(sample_time_in_sec: int = 300, nb_frame: int = 5) -> None:
    snapshots = []
    mem_logger.warning("Starts memory profiling: DO NOT USE THIS IN PRODUCTION")
    try:
        tracemalloc.start(nb_frame)
        while True:
            try:
                gc.collect()
                snapshots.append(tracemalloc.take_snapshot())
                mem_logger.info(
                    "New snapshot taken for memory profiling: %s",
                    snapshots[-1].statistics("filename", cumulative=True),
                )
                # filters: list[
                #    tracemalloc.Filter
                # ] = []  # [tracemalloc.Filter(inclusive=True, filename_pattern="*requests*")]
                if len(snapshots) > 1:
                    stats = (
                        snapshots[-1]
                        # .filter_traces(filters)
                        .compare_to(snapshots[-2], "filename")
                    )
                    mem_logger.warning(
                        f"Memory profiling for {nb_frame} frame (sampling {sample_time_in_sec}s):"
                    )
                    for stat in stats[:nb_frame]:
                        mem_logger.warning(
                            f"* Diff allocations size: {stat.size_diff / 1024} (Total: {stat.size / 1024}) KiB\n"
                            f"  Diff nb memory blocks: {stat.count_diff}  (Total: {stat.count})"
                        )
                        for line in stat.traceback.format():
                            mem_logger.warning(line)
                mem_logger.info(
                    f"Waiting for {sample_time_in_sec}s before the next snapshot"
                )
                snapshots.pop(0)
                await asyncio.sleep(sample_time_in_sec)
            except Exception:
                mem_logger.exception("Error while tracing memory")
    finally:
        tracemalloc.stop()


def start() -> None:
    """Start application"""
    # Needed to share some object across processes
    multiprocessing.set_start_method("spawn")

    # Do database migration on startup to initialize alembic revision
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.submit(db_migration)

    opentelemetry = OpenTelemetryTracer(
        os.environ.get("RYAX_OTLP_ENDPOINT"), "ryax-runner"
    )
    opentelemetry.init()

    container = configure()
    logger.info(f"Ryax Runner version: {__version__}")

    app: web.Application = init(container)
    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)
    web.run_app(app, shutdown_timeout=5)


def db_migration() -> None:
    import alembic.config

    alembic_args = ["--config", "./migrations/runner/alembic.ini", "upgrade", "head"]
    alembic.config.main(argv=alembic_args)
