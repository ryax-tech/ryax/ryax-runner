# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from multiprocessing import Queue
from typing import Callable

from dependency_injector import containers, providers

from ryax.common.infrastructure.messaging.querier import RabbitMQMessageQuerier
from ryax.runner.application.accounting_service import AccountingService
from ryax.runner.application.action_logs_service import ActionLogsService
from ryax.runner.application.authentication_service import AuthenticationService
from ryax.runner.application.garbage_collect_service import GarbageCollectorService
from ryax.runner.application.health_service import HealthService
from ryax.common.application.message_bus import MessageBus
from ryax.runner.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.runner.application.reference_solver_service import ReferenceSolverService
from ryax.runner.application.scheduler import MultiObjectiveScheduler
from ryax.runner.application.site_placement_service import SitePlacementService
from ryax.runner.application.site_service import SiteService
from ryax.runner.application.user_api_key_service import UserApiKeyService
from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.application.views.portal_view import PortalView
from ryax.runner.application.views.stored_file_view import StoredFileView
from ryax.runner.application.views.workflow_deployment_view import WorkflowView
from ryax.runner.application.workflow_result_service import WorkflowResultService
from ryax.runner.application.workflow_run_service import WorkflowRunService
from ryax.addons.addon_entities import RyaxAddOns
from ryax.common.domain.storage.storage_service import IStorageService
from ryax.runner.infrastructure.action_deployment.dummy.dummy_action_deployment_service import (
    DummyActionDeploymentService,
)

# k8s deployment service with grpc
from ryax.runner.infrastructure.action_deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)
from ryax.runner.infrastructure.action_deployment.k8s.k8s_engine import K8sEngine

# slurm deployment service with ssh
from ryax.runner.infrastructure.action_deployment.sshslurm.ssh_slurm_action_deployment_service import (
    SshSlurmActionDeploymentService,
)
from ryax.runner.infrastructure.action_execution.dummy.dummy_execution_service import (
    DUMMYExecutionService,
)
from ryax.runner.infrastructure.action_execution.worker.worker_execution_service import (
    WorkerExecutionService,
)
from ryax.runner.infrastructure.api.controllers.workflows_websocket_controller import (
    WorkflowWebSocketController,
)
from ryax.runner.infrastructure.api.controllers.runs_websocket_controller import (
    RunsWebSocketController,
)
from ryax.runner.infrastructure.api.jwt_service import JwtService
from ryax.runner.infrastructure.authorization.authorization_api_service import (
    AuthorizationApiService,
)
from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.runner.infrastructure.database.mapper import map_orm
from ryax.runner.infrastructure.database.repositories.accounting_repository import (
    DatabaseAccountingRepository,
)
from ryax.runner.infrastructure.database.repositories.action_definition_repository import (
    DatabaseActionDefinitionRepository,
)
from ryax.runner.infrastructure.database.repositories.action_deployment_repository import (
    DatabaseActionDeploymentRepository,
)
from ryax.runner.infrastructure.database.repositories.action_logs_repository import (
    DatabaseActionLogsRepository,
)
from ryax.runner.infrastructure.database.repositories.api_key_repository import (
    DatabaseUserApiKeyRepository,
)
from ryax.runner.infrastructure.database.repositories.execution_connection_repository import (
    DatabaseExecutionConnectionRepository,
)
from ryax.runner.infrastructure.database.repositories.execution_result_repository import (
    DatabaseExecutionResultRepository,
)
from ryax.runner.infrastructure.database.repositories.site_repository import (
    DatabaseSiteRepository,
)
from ryax.runner.infrastructure.database.repositories.recommendation_vpa_repository import (
    DatabaseRecommendationVPARepository,
)
from ryax.runner.infrastructure.database.repositories.stored_file_repository import (
    DatabaseStoredFileRepository,
)
from ryax.runner.infrastructure.database.repositories.workflow_repository import (
    DatabaseWorkflowDeploymentRepository,
)
from ryax.runner.infrastructure.database.repositories.workflow_run_repository import (
    DatabaseWorkflowRunRepository,
)
from ryax.runner.infrastructure.database.unit_of_work import DatabaseUnitOfWork
from ryax.runner.infrastructure.encryption.encryption_service import EncryptionService
from ryax.common.infrastructure.messaging.consumer import RabbitMQMessageConsumer
from ryax.common.infrastructure.messaging.engine import MessagingEngine
from ryax.common.infrastructure.messaging.publisher import RabbitMQMessagePublisher
from ryax.common.infrastructure.monitoring.prometheus_server import (
    PrometheusMetricsServer,
)
from ryax.common.infrastructure.storage.posix.posix_storage_service import (
    PosixStorageService,
)
from ryax.common.infrastructure.storage.s3.s3_engine import S3Engine
from ryax.common.infrastructure.storage.s3.s3_service import S3Storage

logger = getLogger(__name__)


class ApplicationContainer(containers.DeclarativeContainer):
    """Application container for dependency injection"""

    # Define configuration provider
    configuration = providers.Configuration()

    # Application
    # Internal bus
    message_bus = providers.Singleton(
        MessageBus, keep_event_history=configuration.keep_event_history
    )

    # Infrastructure
    # Messaging
    messaging_engine = providers.Singleton(
        MessagingEngine, connection_url=configuration.messaging.url
    )
    message_consumer: providers.Singleton[
        RabbitMQMessageConsumer
    ] = providers.Singleton(RabbitMQMessageConsumer, engine=messaging_engine)
    message_publisher = providers.Singleton(
        RabbitMQMessagePublisher, engine=messaging_engine, base_routing_key="Runner"
    )
    message_querier = providers.Singleton(
        RabbitMQMessageQuerier, engine=messaging_engine, base_routing_key="Runner"
    )

    # Storage
    local_posix_storage = providers.Singleton(
        PosixStorageService,
        root_path=configuration.storage.local.posix.root_path,
    )
    local_s3_engine = providers.Singleton(
        S3Engine,
        connection_url=configuration.storage.local.s3.server,
        bucket=configuration.storage.local.s3.bucket,
        access_key=configuration.storage.local.s3.access_key,
        secret_key=configuration.storage.local.s3.secret_key,
    )
    local_s3_storage = providers.Factory(
        S3Storage,
        engine=local_s3_engine,
    )
    # Storage Services
    local_storage_service: Callable[[], IStorageService] = providers.Selector(
        configuration.storage.local.type,
        posix=local_posix_storage,
        s3=local_s3_storage,
    )
    # Kubernetes
    k8s_engine = providers.Singleton(
        K8sEngine,
        user_namespace=configuration.deployment.k8s.user_namespace,
        message_bus=message_bus,
    )

    # Action execution
    worker_execution_service = providers.Factory(
        WorkerExecutionService,
        publisher=message_publisher,
    )
    # Dummy Action execution
    dummy_execution_service = providers.Factory(
        DUMMYExecutionService,
        message_bus=message_bus,
    )
    # Be careful, all of these services are factories: everytime you get them from the FactoryAggregate you get a new
    # instance. Which means that you can't store state between 2 calls of a functions, i.e. consider all methods to be
    # static  (except for the parameters given below, at initialization).
    execution_service_factory = providers.FactoryAggregate(  # type: ignore
        GRPC_V1=worker_execution_service,
        SLURM_SSH_V1=worker_execution_service,
        DUMMY=dummy_execution_service,
    )

    # Database
    database_engine = providers.Singleton(
        SqlalchemyDatabaseEngine,
        connection_url=configuration.database.url,
        create_mapping=map_orm,
    )

    action_deployment_repository = providers.Factory(
        DatabaseActionDeploymentRepository,
    )

    action_definition_repository = providers.Factory(
        DatabaseActionDefinitionRepository,
    )

    user_api_key_repository = providers.Factory(DatabaseUserApiKeyRepository)

    execution_connection_repository = providers.Factory(
        DatabaseExecutionConnectionRepository,
    )

    workflow_repository = providers.Factory(
        DatabaseWorkflowDeploymentRepository,
    )
    stored_file_repository = providers.Factory(DatabaseStoredFileRepository)

    workflow_run_repository = providers.Factory(
        DatabaseWorkflowRunRepository,
    )

    execution_result_repository = providers.Factory(
        DatabaseExecutionResultRepository,
    )

    action_logs_repository = providers.Factory(
        DatabaseActionLogsRepository,
    )

    site_repository = providers.Factory(
        DatabaseSiteRepository,
    )

    recommendation_vpa_repository = providers.Factory(
        DatabaseRecommendationVPARepository,
    )

    accounting_repository = providers.Factory(
        DatabaseAccountingRepository,
    )

    unit_of_work: providers.Factory[DatabaseUnitOfWork] = providers.Factory(
        DatabaseUnitOfWork,
        engine=database_engine,
        action_deployment_repository_factory=action_deployment_repository.provider,
        action_definition_repository_factory=action_definition_repository.provider,
        execution_connection_repository_factory=execution_connection_repository.provider,
        workflow_deployment_repository_factory=workflow_repository.provider,
        execution_result_repository_factory=execution_result_repository.provider,
        stored_file_repository_factory=stored_file_repository.provider,
        user_api_key_repository_factory=user_api_key_repository.provider,
        workflow_run_repository_factory=workflow_run_repository.provider,
        action_logs_repository_factory=action_logs_repository.provider,
        site_repository_factory=site_repository.provider,
        recommendation_vpa_repository_factory=recommendation_vpa_repository.provider,
        accounting_repository_factory=accounting_repository.provider,
    )
    # addons
    addons: providers.Singleton[RyaxAddOns] = providers.Singleton(
        RyaxAddOns,
    )

    # Action deployment
    action_logs_service = providers.Singleton(
        ActionLogsService,
        uow_factory=unit_of_work.provider,
        message_bus=message_bus,
        log_query_interval_in_seconds=configuration.deployment.logs.query_interval_in_seconds,
        max_log_size_per_action_in_bytes=configuration.deployment.logs.logs_max_size_in_bytes,
        max_one_line_log_size_in_bytes=configuration.deployment.logs.log_line_max_size_in_bytes,
    )

    runs_websocket_controller = providers.Singleton(
        RunsWebSocketController,
        uow_factory=unit_of_work.provider,
        message_bus=message_bus,
    )
    workflow_websocket_controller = providers.Singleton(
        WorkflowWebSocketController,
        uow_factory=unit_of_work.provider,
        message_bus=message_bus,
    )

    # Be careful, all of these services are factories: everytime you get them from the FactoryAggregate you get a new
    # instance. Which means that you can't store state between 2 calls of a functions, i.e. consider all methods to be
    # static (except for the parameters given below, at initialization).
    action_deployment_factory = providers.FactoryAggregate(  # type: ignore
        k8s=providers.Factory(
            K8SActionDeploymentService,
            engine=k8s_engine,
            ryax_registry=configuration.ryax.registry,
            publisher=message_publisher,
            querier=message_querier,
            action_logs_service=action_logs_service,
            user_action_log_level=configuration.actions.log_level,
        ),
        sshslurm=providers.Factory(
            SshSlurmActionDeploymentService,
            ryax_registry=configuration.ryax.ssh_internal_registry,
            publisher=message_publisher,
            querier=message_querier,
            user_action_log_level=configuration.actions.log_level,
        ),
        dummy=providers.Factory(DummyActionDeploymentService, message_bus=message_bus),
    )

    # Extra service
    reference_solver: providers.Factory[ReferenceSolverService] = providers.Factory(
        ReferenceSolverService,
        uow=unit_of_work,
        addons=addons,
    )

    # Scheduler
    site_placement_service = providers.Singleton(
        SitePlacementService,
        # policy=configuration.scheduler.placement_policy
    )
    scheduler = providers.Singleton(
        MultiObjectiveScheduler,
        message_bus=message_bus,
        uow_factory=unit_of_work.provider,
        addons=addons,
        site_placement_service=site_placement_service,
        undeploy_idle_actions_after_seconds=configuration.undeploy_idle_actions_after_seconds,
        schedule_retry_delay_seconds=configuration.scheduler_retry_delay_seconds,
        max_action_deployments=configuration.scheduler.max_action_deployments,
        max_concurrent_deploying_actions=configuration.scheduler.max_concurrent_deploying_actions,
    )

    # Views
    portal_view = providers.Factory(
        PortalView,
        uow=unit_of_work,
    )
    workflow_view = providers.Factory(
        WorkflowView,
        uow=unit_of_work,
    )
    stored_file_view = providers.Factory(
        StoredFileView,
        local_storage_service=local_storage_service,
        uow=unit_of_work,
    )

    # API auth
    jwt_service = providers.Singleton(
        JwtService, secret_key=configuration.jwt_secret_key
    )
    authentication_service = providers.Singleton(
        AuthenticationService, security_service=jwt_service
    )
    authorization_api_service = providers.Singleton(
        AuthorizationApiService, configuration.authorization.base_url
    )
    project_authorization_service = providers.Singleton(
        ProjectAuthorizationService, authorization_api_service=authorization_api_service
    )
    # Other services
    health_service = providers.Singleton(HealthService)

    workflow_run_service = providers.Factory(
        WorkflowRunService,
        uow=unit_of_work,
        message_bus=message_bus,
        base_url=configuration.api.base_url,
        storage_service=local_storage_service,
    )

    workflow_result_service = providers.Factory(
        WorkflowResultService,
        uow=unit_of_work,
        workflow_run_service=workflow_run_service,
        stored_file_service=stored_file_view,
    )

    site_service = providers.Factory(
        SiteService,
        uow=unit_of_work,
    )

    encryption_service = providers.Singleton(
        EncryptionService, encryption_key=configuration.encryption_key
    )

    user_api_key_service = providers.Factory(
        UserApiKeyService, uow=unit_of_work, encryption_service=encryption_service
    )

    user_auth_service = providers.Factory(
        UserAuthService, uow=unit_of_work, encryption_service=encryption_service
    )

    metrics_service = providers.Singleton(
        PrometheusMetricsServer,
        metrics_port=configuration.observabiliy.metrics_server_port,
    )

    garbage_collector_queue: providers.Singleton[Queue] = providers.Singleton(Queue)

    garbage_collector_service = providers.Singleton(
        GarbageCollectorService,
        queue=garbage_collector_queue,
        database_connection_url=configuration.database.url,
        storage_config=configuration.storage.local.s3,
        max_number_of_runs_per_project=configuration.max_number_of_runs_per_project,
        delay_between_calls_in_seconds=configuration.delay_between_calls_in_seconds,
    )

    accounting_service = providers.Factory(AccountingService, uow=unit_of_work)
