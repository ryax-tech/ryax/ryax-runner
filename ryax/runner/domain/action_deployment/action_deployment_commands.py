# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionResources,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.common.domain.internal_messaging.base_messages import BaseCommand


@dataclass
class ActionDeploymentBaseCommand(BaseCommand):
    action_deployment_id: str


@dataclass
class CreateActionDeploymentCommand(BaseCommand):
    project_id: str
    action_definition_id: str
    node_pool_id: str
    execution_connection_id: str


@dataclass
class DeployActionDeploymentCommand(ActionDeploymentBaseCommand):
    name: str
    version: str
    container_image: str
    kind: ActionKind
    log_level: int
    resources: ActionResources | None
    execution_type: ActionExecutionType
    deployment_type: str
    parameters: dict
    node_pool_name: str


@dataclass
class UpdateActionDeploymentCommand(ActionDeploymentBaseCommand):
    state: ActionDeploymentState
    logs: str = field(default="")


@dataclass
class SetRunningActionDeploymentCommand(ActionDeploymentBaseCommand):
    pass


@dataclass
class DeallocateActionDeploymentCommand(ActionDeploymentBaseCommand):
    pass


@dataclass
class GetActionDeploymentStateCommand(ActionDeploymentBaseCommand):
    pass


@dataclass
class ErrorActionDeploymentCommand(ActionDeploymentBaseCommand):
    error_message: str


@dataclass
class UndeployActionDeploymentCommand(ActionDeploymentBaseCommand):
    pass


@dataclass
class DeleteActionDeploymentCommand(ActionDeploymentBaseCommand):
    pass


@dataclass
class WatchActionDeploymentStateCommand(ActionDeploymentBaseCommand):
    pass
