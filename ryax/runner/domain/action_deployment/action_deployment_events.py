# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from ryax.common.domain.internal_messaging.base_messages import BaseEvent


@dataclass
class ActionDeploymentBaseEvent(BaseEvent):
    action_deployment_id: str


@dataclass
class ActionDeploymentAppliedEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentIsReadyEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentAvailableEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentUndeployingEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentUndeployedEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentNewLogLinesEvent(ActionDeploymentBaseEvent):
    log_lines: str
    executor_id: str


@dataclass
class ActionDeploymentNewLogLinesAddedEvent(ActionDeploymentBaseEvent):
    log_lines: str


@dataclass
class ActionDeploymentErroredEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentResourcesAvailableEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentNoMoreResourcesAvailableEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentCreatedEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentIsDeployingEvent(ActionDeploymentBaseEvent):
    pass


@dataclass
class ActionDeploymentIsRunningEvent(ActionDeploymentBaseEvent):
    pass
