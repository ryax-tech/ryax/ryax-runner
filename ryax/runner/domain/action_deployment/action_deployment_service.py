# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc

from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.runner.domain.exceptions import ActionDeploymentError


class IActionDeploymentService(abc.ABC):
    """Interface for actions deployment service.

    This service is in charge of applying the deployment to the target infrastructure from the ActionDeployment object.
    It also has to watch the state and the execution_log of the deployment and delete it when requested.

    Use the message bus to update ActionDeployment with the command:
      UpdateActionDeploymentCommand(action_deployment_id, action_deployment_state)
    """

    async def deploy(self, action: ActionDeployment, addons_extra_params: dict) -> None:
        try:
            await self.apply(action, addons_extra_params)
        except Exception as err:
            raise ActionDeploymentError from err

    @abc.abstractmethod
    async def apply(self, action: ActionDeployment, addons_extra_params: dict) -> None:
        """Apply an action deployment on the target infrastructure"""
        raise NotImplementedError()

    @abc.abstractmethod
    async def get_current_state(
        self, action_deployment: ActionDeployment
    ) -> ActionDeploymentState:
        """Returns the current state of the deployment"""
        raise NotImplementedError()

    @abc.abstractmethod
    async def watch(self, action: ActionDeployment) -> None:
        """Trigger the watch of the deployment state and the execution_log

        Should at least send the message ActionDeploymentIsReadyEvent(action.id) when the action is ready to run.
        """
        pass

    @abc.abstractmethod
    async def unwatch(self, action_deployment: ActionDeployment) -> None:
        """Stop the deployment and execution_log watch"""
        pass

    @abc.abstractmethod
    async def delete(self, action_deployment: ActionDeployment) -> None:
        """Delete the deployment from the infrastructure"""
        pass
