# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
import logging
from dataclasses import dataclass, field
from typing import Optional, TypeAlias

from prometheus_client import Gauge

from ryax.common.domain.registration.registration_values import SiteType
from ryax.common.domain.state_machine_observability import StateMachine
from ryax.runner.domain.site.site_entities import NodePool
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    CreateActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.common.domain.utils import get_random_id

logger = logging.getLogger(__name__)

# Instrumentation
action_gauge = Gauge(
    "ryax_action_deployments",
    "Actions deployments with metadata",
    labelnames=[
        "name",
        "version",
        "state",
        "execution_type",
        "project_id",
        "node_pool_name",
        "site_name",
    ],
)


class ActionDeploymentStateMachine(StateMachine):
    stateType: TypeAlias = ActionDeploymentState
    stateMachine = {}
    id_attribute = "action_deployment_id"


@dataclass
class ActionDeployment:
    id: str
    project_id: str
    action_definition: ActionDefinition
    execution_type: ActionExecutionType
    node_pool: NodePool
    state: ActionDeploymentState = ActionDeploymentState.NONE
    # Endpoint for internal access to the action for execution
    action_internal_endpoint: Optional[str] = None
    # Addons parameters resolved with final values
    addons: dict[str, dict] = field(default_factory=dict)
    # if this deployment is running or will run a connection, it is this connection
    allocated_connection_id: Optional[str] = None
    error_message: str = ""
    last_allocation_date: datetime.datetime = datetime.datetime.now(
        datetime.timezone.utc
    )

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_apply_command(
        cls,
        command: CreateActionDeploymentCommand,
        action_definition: ActionDefinition,
        node_pool: NodePool,
    ) -> "ActionDeployment":
        # FIXME: This is a hack, use a builtin type instead of action name
        if (
            "postgw" == action_definition.technical_name
            or "httpapijson" == action_definition.technical_name
        ):
            exec_type = ActionExecutionType.DUMMY
        elif node_pool.site.type == SiteType.SLURM_SSH:
            exec_type = ActionExecutionType.SLURM_SSH_V1
        else:
            exec_type = ActionExecutionType.GRPC_V1

        return ActionDeployment(
            id=cls.new_id(),
            project_id=command.project_id,
            action_definition=action_definition,
            execution_type=exec_type,
            node_pool=node_pool,
        )

    def action_gauge_with_labels(self) -> Gauge:
        return action_gauge.labels(
            name=self.action_definition.technical_name,
            version=self.action_definition.version,
            state=self.state.value,
            execution_type=self.execution_type,
            project_id=self.project_id,
            node_pool_name=self.node_pool.name,
            site_name=self.node_pool.site.name,
        )

    def set_state(self, state: ActionDeploymentState) -> None:
        """Track state changes"""
        logger.debug(
            "Setting state of %s(%s) from %s to %s",
            self.__class__,
            self.id,
            self.state,
            state,
        )
        ActionDeploymentStateMachine.check_transition(self.state, state)
        self.action_gauge_with_labels().dec()
        self.state = state
        self.action_gauge_with_labels().inc()

    def set_ready(self) -> None:
        self.set_state(ActionDeploymentState.READY)
        self.last_allocation_date = datetime.datetime.now(datetime.timezone.utc)

    def set_deploying(self) -> None:
        self.set_state(ActionDeploymentState.DEPLOYING)

    def set_running(self) -> None:
        self.set_state(ActionDeploymentState.RUNNING)
        self.last_allocation_date = datetime.datetime.now(datetime.timezone.utc)

    def set_undeploying(self) -> None:
        self.set_state(ActionDeploymentState.UNDEPLOYING)
        self.last_allocation_date = datetime.datetime.now(datetime.timezone.utc)

    def set_undeployed(self) -> None:
        self.set_state(ActionDeploymentState.UNDEPLOYED)
        self.allocated_connection_id = None
        self.last_allocation_date = datetime.datetime.now(datetime.timezone.utc)

    def set_created(self) -> None:
        self.state = ActionDeploymentState.CREATED
        self.action_gauge_with_labels().inc()

    def set_error(self) -> None:
        self.set_state(ActionDeploymentState.ERROR)
        self.allocated_connection_id = None
        logger.error(
            "Action deployment '%s' is on error: %s",
            self.id,
            self.error_message,
        )

    def set_no_resources_available(self) -> None:
        self.set_state(ActionDeploymentState.NO_RESOURCES_AVAILABLE)

    def get_deployment_type(self) -> str:
        # FIXME: Use a built-in type of action for those
        if (
            "postgw" in self.action_definition.technical_name
            or "httpapijson" in self.action_definition.technical_name
        ):
            return "dummy"
        if self.node_pool.site.type == SiteType.SLURM_SSH:
            return "sshslurm"
        elif self.node_pool.site.type == SiteType.KUBERNETES:
            return "k8s"
        else:
            raise NotImplementedError(
                f"Unable to deploy Action of type {self.node_pool.site.type}"
            )

    def deallocate(self) -> None:
        self.allocated_connection_id = None
