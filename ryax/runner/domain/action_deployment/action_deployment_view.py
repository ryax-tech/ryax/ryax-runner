from dataclasses import dataclass

from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)


@dataclass
class ActionDeploymentSchedulerView:
    id: str
    state: ActionDeploymentState
    allocated_connection_id: str
