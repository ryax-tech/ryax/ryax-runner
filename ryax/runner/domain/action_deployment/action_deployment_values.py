# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from enum import Enum


class ActionDeploymentState(Enum):
    NONE = "None"
    CREATED = "Created"
    DEPLOYING = "Deploying"
    READY = "Ready"
    RUNNING = "Running"
    UNDEPLOYING = "Undeploying"
    UNDEPLOYED = "Undeployed"
    ERROR = "Error"
    NO_RESOURCES_AVAILABLE = "Not Enough Resources Available"

    @staticmethod
    def eventually_ready_states() -> list["ActionDeploymentState"]:
        return [
            ActionDeploymentState.CREATED,
            ActionDeploymentState.DEPLOYING,
            ActionDeploymentState.READY,
            ActionDeploymentState.RUNNING,
            ActionDeploymentState.NO_RESOURCES_AVAILABLE,
        ]

    @staticmethod
    def all_but_none() -> list["ActionDeploymentState"]:
        all_states = list(ActionDeploymentState)
        all_states.remove(ActionDeploymentState.NONE)
        return all_states


class ActionExecutionType(Enum):
    NONE = "None"
    GRPC_V1 = "GRPC_V1"
    SLURM_SSH_V1 = "SLURM_SSH_V1"
    DUMMY = "DUMMY"
