# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List

from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_view import (
    ActionDeploymentSchedulerView,
)


class ActionDeploymentNotFoundError(Exception):
    pass


class IActionDeploymentRepository(abc.ABC):
    @abc.abstractmethod
    def get_by_id(self, action_id: str, eager_load: bool = False) -> ActionDeployment:
        ...

    @abc.abstractmethod
    def get_by_workflow_definition_id(
        self, workflow_definition_id: str
    ) -> ActionDeployment:
        ...

    @abc.abstractmethod
    def list_unallocated_and_ready_from_action_definition_id(
        self, action_definition_id: str, project_id: str, node_pool_id: str
    ) -> List[ActionDeployment]:
        """
        returns all actions where
        action.definition == action_definition_id and
        action.state == ActionDeploymentState.READY and
        action.allocated_connection is None
        """
        ...

    @abc.abstractmethod
    def list_unallocated(self) -> list[ActionDeployment]:
        """
        Action that are already in deployment or deployed and not allocated
        """
        ...

    @abc.abstractmethod
    def list_eventually_ready(self) -> list[ActionDeployment]:
        ...

    @abc.abstractmethod
    def list_eventually_ready_from_action_definition_id(
        self, action_definition_id: str, project_id: str, node_pool_id: str
    ) -> list[ActionDeploymentSchedulerView]:
        """
        returns all actions where action definition id match and is in a state that will eventually lead to READY
        """
        ...

    @abc.abstractmethod
    def count_eventually_ready(self, node_pool_id: str) -> int:
        ...

    @abc.abstractmethod
    def list_all_in_progress(self) -> list[ActionDeployment]:
        """
        :return:  All ActionDeployment that are in one of the following state:
        CREATED, DEPLOYING, UNDEPLOYING, READY, RUNNING
        """
        ...

    @abc.abstractmethod
    def add(self, action_deployment: ActionDeployment) -> None:
        ...

    @abc.abstractmethod
    def delete(self, action_deployment_id: str) -> None:
        ...
