# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from types import TracebackType
from typing import Type

from ryax.runner.domain.accounting.accounting_repository import IAccountingRepository
from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    IActionDeploymentRepository,
)
from ryax.runner.domain.execution_connection.execution_connection_repository import (
    IExecutionConnectionRepository,
)
from ryax.runner.domain.execution_result.execution_result_repository import (
    IExecutionResultRepository,
)
from ryax.runner.domain.action_logs.action_logs_repository import IActionLogsRepository
from ryax.runner.domain.site.site_repository import ISiteRepository
from ryax.runner.domain.stored_file.stored_file_reporitory import IStoredFileRepository
from ryax.runner.domain.user_auth.api_key.api_key_repository import (
    IUserApiKeyRepository,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_repository import (
    IWorkflowDeploymentRepository,
)
from ryax.runner.domain.workflow_run.workflow_run_repository import (
    IWorkflowRunRepository,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_repository import (
    IRecommendationVPARepository,
)


class IUnitOfWork(abc.ABC):
    action_deployment_repository: IActionDeploymentRepository
    action_definition_repository: IActionDefinitionRepository
    execution_connection_repository: IExecutionConnectionRepository
    workflow_repository: IWorkflowDeploymentRepository
    execution_result_repository: IExecutionResultRepository
    stored_file_repository: IStoredFileRepository
    user_api_key_repository: IUserApiKeyRepository
    workflow_run_repository: IWorkflowRunRepository
    action_logs_repository: IActionLogsRepository
    site_repository: ISiteRepository
    recommendation_vpa_repository: IRecommendationVPARepository
    accounting_repository: IAccountingRepository

    def __enter__(self) -> "IUnitOfWork":
        return self

    def __exit__(
        self,
        exc_type: Type[Exception] | None = None,
        exc_val: Exception | None = None,
        exc_tb: TracebackType | None = None,
    ) -> None:
        pass

    @abc.abstractmethod
    def commit(self) -> None:
        pass

    @abc.abstractmethod
    def rollback(self) -> None:
        pass
