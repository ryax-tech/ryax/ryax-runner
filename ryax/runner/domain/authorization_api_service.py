# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc


class IAuthorizationApiService(abc.ABC):
    @abc.abstractmethod
    async def get_current_project(self, user_id: str, authorization_token: str) -> str:
        """Method to get user's current project from authorization api"""
        pass
