# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc


class IEncryptionService(abc.ABC):
    @abc.abstractmethod
    def encrypt(self, plaintext: str) -> bytes:
        pass

    @abc.abstractmethod
    def decrypt(self, ciphertext: bytes) -> str:
        pass
