# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
class NoCurrentProjectException(Exception):
    message: str = "User has no project assigned"


class WorkflowNotFoundError(Exception):
    pass


class WorkflowRunNotFoundError(Exception):
    message: str = "Workflow Run not found"


class ActionDeploymentError(Exception):
    pass


class ActionUndeploymentError(Exception):
    pass
