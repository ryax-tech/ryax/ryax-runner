from asyncio import Task
from dataclasses import dataclass, field


@dataclass
class TasksService:
    """Keep track of process in memory to avoid background pending_execution garbage collection"""

    tasks_process: dict[str, Task] = field(default_factory=dict)

    def register_task(self, task: Task) -> None:
        self.tasks_process[task.get_name()] = task

    def pop_task(self, task_name: str) -> Task:
        return self.tasks_process.pop(task_name)
