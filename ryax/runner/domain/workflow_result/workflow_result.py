# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import Any, Optional

from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
)


class WorkflowResultsNotReadyError(Exception):
    message = "Not all workflow results have been computed for this workflow run yet."


class WorkflowResultsUnavailableError(Exception):
    message = "Workflow run has failed or was canceled."


@dataclass
class WorkflowResultsUserDefinedError(Exception):
    code: int
    message: Optional[str]


@dataclass
class WorkflowResult:
    """
    A WorkflowResult is a user-defined output from any action in the workflow (input or output).
    """

    id: str
    key: str
    technical_name: str  # it is the technical_name field of the workflow_action_io
    workflow_action_io_id: str
    action_id: str  # it is the WorkflowAction.action_definition_id
    workflow_deployment_id: str

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_create_command(
        cls,
        result: CreateWorkflowDeploymentCommand.Result,
        workflow_deployment_id: str,
    ) -> "WorkflowResult":
        return WorkflowResult(
            id=cls.new_id(),
            key=result.key,
            action_id=result.action_id,
            technical_name=result.technical_name,
            workflow_action_io_id=result.workflow_action_io_id,
            workflow_deployment_id=workflow_deployment_id,
        )


@dataclass
class WorkflowResultView:
    """Workflow result view contains the value of the result obtained after an execution"""

    key: str
    value: Any
    type: ActionIOType
