# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from datetime import datetime
from typing import List, Optional, Tuple

from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.workflow_result.workflow_result import WorkflowResultView


class IExecutionResultRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, execution_id: str) -> ExecutionResult:
        ...

    @abc.abstractmethod
    def list(
        self,
        project: str,
        workflow_definition_id: Optional[str] = None,
        workflow_action_id: Optional[str] = None,
        state: Optional[ExecutionResultState] = None,
        submitted_before: Optional[datetime] = None,
        submitted_after: Optional[datetime] = None,
        _range: Optional[str] = None,
    ) -> Tuple[List[ExecutionResult], int, int, int]:
        ...

    @abc.abstractmethod
    def add(self, execution: ExecutionResult) -> None:
        ...

    @abc.abstractmethod
    def get_ids_by_end_of_log_delimiter(
        self, end_of_log_delimiter: str
    ) -> Tuple[str, str]:
        ...

    @abc.abstractmethod
    def get_running_from_connection_id(
        self, execution_connection_id: str
    ) -> ExecutionResult:
        ...

    @abc.abstractmethod
    def get_results_by_workflow_run_id(
        self, workflow_run_id: str
    ) -> List[WorkflowResultView]:
        ...
