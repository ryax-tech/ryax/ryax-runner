# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from datetime import datetime
from typing import Any, Optional

from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.common.domain.internal_messaging.base_messages import BaseEvent
from ryax.runner.domain.utils import get_date


@dataclass
class ExecutionResultCompletedEvent(BaseEvent):
    execution_result_id: str


@dataclass
class ExecutionResultInitializedEvent(BaseEvent):
    execution_result_id: str


@dataclass
class ExecutionResultFinishedEvent(BaseEvent):
    @dataclass
    class OutputFile:
        output_name: str
        file_path: str
        mimetype: Optional[str]
        encoding: Optional[str]
        size_in_bytes: int

    id: str
    outputs: dict[str, Any]
    output_files: list[OutputFile]
    status: ExecutionResultState
    end_time: datetime = field(default_factory=get_date)
    error_message: Optional[str] = None
    http_status_code: Optional[int] = None


@dataclass
class ExecutionResultNewLogLinesToBeAddedEvent(BaseEvent):
    log_lines: str
    execution_result_id: str


@dataclass
class ExecutionResultNewLogLinesAddedEvent(BaseEvent):
    log_lines: str
    execution_result_id: str
