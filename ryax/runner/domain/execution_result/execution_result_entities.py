# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from textwrap import dedent
from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional, Union

from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultFinishedEvent,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.utils import get_date
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction


@dataclass
class OutputReference:
    name: str
    value: Union[str, int, float, bool, bytes]
    workflow_action_name: str | None
    type: ActionIOType | None


@dataclass
class ExecutionResult:
    id: str
    project_id: str
    execution_connection_id: str
    workflow_action: WorkflowAction
    submitted_at: datetime
    started_at: datetime
    workflow_run_id: str
    end_of_log_delimiter: str | None = None
    state: ExecutionResultState = field(default=ExecutionResultState.NONE)
    ended_at: datetime | None = None
    error_message: str | None = None
    http_status_code: int | None = None

    outputs: dict = field(default_factory=dict)
    inputs: dict = field(default_factory=dict)
    stored_files: list[StoredFile] = field(default_factory=list)

    parents: list["ExecutionResult"] = field(default_factory=list)

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_execution_connection(
        cls,
        project_id: str,
        execution_connection_id: str,
        workflow_action: WorkflowAction,
        workflow_run_id: str,
        end_of_log_delimiter: str | None,
        submitted_at: datetime,
        started_at: datetime,
        inputs: dict,
    ) -> "ExecutionResult":
        return cls(
            id=cls.new_id(),
            project_id=project_id,
            execution_connection_id=execution_connection_id,
            workflow_run_id=workflow_run_id,
            workflow_action=workflow_action,
            end_of_log_delimiter=end_of_log_delimiter,
            submitted_at=submitted_at,
            started_at=started_at,
            inputs=inputs,
            state=ExecutionResultState.CREATED,
        )

    def update_on_execution_result_finished(
        self,
        new_result: ExecutionResultFinishedEvent,
        parent_execution_result: Optional["ExecutionResult"],
    ) -> None:
        self.state = new_result.status
        self.ended_at = new_result.end_time
        self.outputs = new_result.outputs
        self.parents = [parent_execution_result] if parent_execution_result else []
        self.error_message = new_result.error_message
        self.http_status_code = new_result.http_status_code

    def get_all_previous_outputs(self) -> list[OutputReference]:
        all_outputs = [
            OutputReference(
                name=output_id,
                value=output_value,
                workflow_action_name=(
                    self.workflow_action.id_in_studio if self.workflow_action else None
                ),
                type=(
                    self.workflow_action.get_output_type_with_info(output_id)
                    if self.workflow_action
                    else None
                ),
            )
            for output_id, output_value in self.outputs.items()
        ]
        for parent in self.parents:
            all_outputs.extend(parent.get_all_previous_outputs())
        return all_outputs

    def set_canceled(self, error_message: str | None = None) -> None:
        self.state = ExecutionResultState.CANCELED
        self.ended_at = get_date()
        self.error_message = error_message

    def __str__(self) -> str:
        return dedent(
            f"""EXECUTION_RESULT:
        --> Execution Connection ID : {self.execution_connection_id}
        --> State : {self.state}
        --> Submitted At : {self.submitted_at}
        --> Started At : {self.started_at}
        --> Ended At : {self.ended_at}
        --> Error Message Length : {-1 if self.error_message is None else len(self.error_message)}
        --> Inputs Keys : {self.inputs.keys()}
        --> Outputs Keys : {self.outputs.keys()}
        """
        )
