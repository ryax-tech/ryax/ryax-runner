# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from datetime import datetime, timezone


def format_exception(exception: Exception) -> str:
    return f"{type(exception).__name__}: {exception}"


def get_date() -> datetime:
    return datetime.now(timezone.utc)
