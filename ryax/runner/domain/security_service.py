# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import Optional

from ryax.runner.domain.auth_token import AuthToken


class ISecurityService(abc.ABC):
    @abc.abstractmethod
    def get_auth_token(self, token: str) -> Optional[AuthToken]:
        """Method to check authorization token"""
        pass
