import abc

from ryax.runner.domain.accounting.accounting_entities import AccountingEntry


class IAccountingRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, execution_id: str) -> AccountingEntry | None:
        ...

    @abc.abstractmethod
    def list_accounting(
        self,
        execution_ids: list[str],
        project_ids: list[str],
        action_ids: list[str],
        workflow_ids: list[str],
        run_ids: list[str],
    ) -> list[AccountingEntry]:
        ...

    @abc.abstractmethod
    def add(self, accounting: AccountingEntry) -> None:
        ...
