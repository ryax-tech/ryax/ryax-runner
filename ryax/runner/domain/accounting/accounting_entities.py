# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
from dataclasses import dataclass


@dataclass
class AccountingEntry:
    project_id: str
    workflow_definition_id: str
    action_definition_id: str
    workflow_run_id: str | None
    execution_connection_id: str
    submitted_at: datetime.datetime
    started_at: datetime.datetime | None
    requested_cpu: float | None = None
    requested_memory: int | None = None
    requested_time: float | None = None
    requested_gpu: int | None = None
    instance_type: str | None = None
    ended_at: datetime.datetime | None = None
    # TODO subscription_id
    # TODO execution status
    # TODO add real, non optional, values of used resources
