import uuid
from dataclasses import dataclass
from enum import Enum


class UserAuthorizationMethod(Enum):
    APIKEY = "ApiKey"


@dataclass
class ProjectSecurityRequirement:
    id: str
    project_id: str
    authorization_method: UserAuthorizationMethod
    enabled: bool = True

    @staticmethod
    def from_project_auth_method(
        project_id: str, authorization_method: UserAuthorizationMethod
    ) -> "ProjectSecurityRequirement":
        return ProjectSecurityRequirement(
            id=str(uuid.uuid4()),
            project_id=project_id,
            authorization_method=authorization_method,
            enabled=True,
        )
