class UserApiKeyNotFoundException(Exception):
    message: str = "The user api key requested does not exist."


class UserApiKeyNameAlreadyExistsException(Exception):
    message: str = "An api key with this name already exists in the project"


class UserApiKeyDateInvalidException(Exception):
    message: str = "The expiration date set is in the past and thus invalid"


class UserApiKeyNotPresentInHeaderException(Exception):
    message: str = "The key 'X-API-KEY' should be present in the request headers when using api key auth."
