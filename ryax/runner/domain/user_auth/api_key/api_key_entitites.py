import uuid
from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from enum import Enum
from typing import Optional


class UserApiKeyStatus(Enum):
    VALID = "Valid"
    EXPIRED = "Expired"
    REVOKED = "Revoked"


@dataclass
class UserApiKey:
    id: str
    name: str
    api_key: bytes
    project_id: str
    creation_date: datetime
    expiration_date: datetime
    last_used_date: Optional[datetime] = None
    revoked: bool = False

    @property
    def status(self) -> str:
        if self.revoked:
            return UserApiKeyStatus.REVOKED.value
        else:
            return (
                UserApiKeyStatus.VALID.value
                if datetime.now(tz=timezone.utc) < self.expiration_date
                else UserApiKeyStatus.EXPIRED.value
            )

    @staticmethod
    def create_from_name_project_key(
        name: str,
        project_id: str,
        api_key: bytes,
        expiration_date: Optional[datetime] = None,
    ) -> "UserApiKey":
        return UserApiKey(
            id=str(uuid.uuid4()),
            name=name,
            project_id=project_id,
            api_key=api_key,
            creation_date=datetime.now(timezone.utc),
            revoked=False,
            expiration_date=(
                expiration_date
                if expiration_date is not None
                else datetime.now(timezone.utc) + timedelta(days=365)
            ),
        )

    def revoke(self) -> None:
        self.revoked = True
        self.expiration_date = datetime.now(timezone.utc)


@dataclass
class DecryptedUserApiKey:
    id: str
    name: str
    api_key: str
    expiration_date: datetime
