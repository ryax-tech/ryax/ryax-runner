# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from abc import ABC, abstractmethod
from typing import List

from ryax.runner.domain.user_auth.api_key.api_key_entitites import UserApiKey
from ryax.runner.domain.user_auth.user_auth_entities import ProjectSecurityRequirement


class IUserApiKeyRepository(ABC):
    @abstractmethod
    def add_api_key(self, new_user_api_key: UserApiKey) -> None:
        ...

    @abstractmethod
    def add_project_security_requirement(
        self, project_security_requirement: ProjectSecurityRequirement
    ) -> None:
        ...

    @abstractmethod
    def delete(self, user_api_key: UserApiKey) -> None:
        ...

    @abstractmethod
    def get(self, user_api_key_id: str, project_id: str) -> UserApiKey:
        ...

    @abstractmethod
    def get_all_keys(self, project_id: str) -> list[UserApiKey]:
        ...

    @abstractmethod
    def ensure_name_does_not_exist(self, name: str, project_id: str) -> None:
        ...

    @abstractmethod
    def list_auth_methods_for_project(
        self, project_id: str
    ) -> List[ProjectSecurityRequirement]:
        ...

    @abstractmethod
    def get_all_api_keys_for_project(self, project_id: str) -> List[UserApiKey]:
        ...
