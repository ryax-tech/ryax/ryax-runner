class UserAuthUnauthorizedException(Exception):
    message: str = "Unauthorized"


class ProjectAuthorizationRequirementNotFoundException(Exception):
    message: str = "This project does not have any authorization requirements."
