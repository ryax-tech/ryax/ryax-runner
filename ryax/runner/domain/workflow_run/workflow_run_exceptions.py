class ActionRunNotFoundError(Exception):
    pass


class UnknownExecutionStateError(Exception):
    pass


class WorkflowRunStateError(Exception):
    pass
