from dataclasses import dataclass

from ryax.common.domain.internal_messaging.base_messages import BaseCommand


@dataclass
class CreateWorkflowRunCommand(BaseCommand):
    project_id: str
    workflow_deployment_id: str


@dataclass
class DeleteWorkflowRunsCommand(BaseCommand):
    workflow_definition_id: str
    project_id: str
