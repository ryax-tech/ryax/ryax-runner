# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List, Tuple

from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.workflow_run.workflow_run_entities import WorkflowRun
from ryax.runner.domain.workflow_run.workflow_run_values import WorkflowRunState


class IWorkflowRunRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, workflow_run: WorkflowRun) -> None:
        ...

    @abc.abstractmethod
    def list_with_filter(
        self,
        workflow_definition_id: str | None = None,
        state: WorkflowRunState | None = None,
        _range: str | None = None,
    ) -> Tuple[list[WorkflowRun], int, int, int]:
        ...

    @abc.abstractmethod
    def list_all(
        self,
        workflow_definition_id: str | None = None,
        state: WorkflowRunState | None = None,
        eager_load: bool = False,
    ) -> list[WorkflowRun]:
        ...

    @abc.abstractmethod
    def get(self, workflow_run_id: str) -> WorkflowRun:
        ...

    @abc.abstractmethod
    def get_stored_files_to_delete(
        self, workflow_run_ids: list[str]
    ) -> list[StoredFile]:
        ...

    @abc.abstractmethod
    def delete(self, workflow_run_id: str) -> None:
        pass

    @abc.abstractmethod
    def delete_all(self, workflow_run_ids: List[str]) -> None:
        pass

    @abc.abstractmethod
    def delete_oldest(self, max_runs_per_project: int) -> None:
        pass
