from dataclasses import dataclass

from ryax.common.domain.internal_messaging.base_messages import BaseEvent


@dataclass
class WorkflowRunIsCompletedEvent(BaseEvent):
    workflow_run_id: str
