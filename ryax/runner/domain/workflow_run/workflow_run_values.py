from enum import Enum


class WorkflowRunState(Enum):
    CANCELED = "Canceled"
    ERROR = "Error"
    RUNNING = "Running"
    COMPLETED = "Completed"

    def is_completed(self) -> bool:
        return self in [
            WorkflowRunState.COMPLETED,
            WorkflowRunState.CANCELED,
            WorkflowRunState.ERROR,
        ]


class ActionRunState(Enum):
    SUCCESS = "Success"
    ERROR = "Error"
    RUNNING = "Running"
    PENDING = "Pending"
    WAITING = "Waiting"
    CANCELED = "Canceled"

    def is_completed(self) -> bool:
        return self in [
            ActionRunState.SUCCESS,
            ActionRunState.CANCELED,
            ActionRunState.ERROR,
        ]

    def will_run_eventually(self) -> bool:
        return self in [
            ActionRunState.RUNNING,
            ActionRunState.WAITING,
            ActionRunState.PENDING,
        ]

    def is_running_or_pending(self) -> bool:
        return self in [
            ActionRunState.RUNNING,
            ActionRunState.PENDING,
        ]
