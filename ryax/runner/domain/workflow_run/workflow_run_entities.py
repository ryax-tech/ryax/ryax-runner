# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from textwrap import dedent
from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional

from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_execptions import (
    ExecutionResultNotFoundError,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.utils import get_date
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_run.workflow_run_exceptions import (
    ActionRunNotFoundError,
    UnknownExecutionStateError,
    WorkflowRunStateError,
)
from ryax.runner.domain.workflow_run.workflow_run_values import (
    ActionRunState,
    WorkflowRunState,
)

logger = logging.getLogger(__name__)


@dataclass
class ActionRun:
    id: str
    state: ActionRunState
    execution_results: list[ExecutionResult]
    workflow_action: WorkflowAction
    # Attribute use by the database to keep action list ordered
    position: int
    error_message: str = ""

    exceed_retry_error: bool = False
    used_retry_error: int = 0

    exceed_retry_memory_bytes: bool = False
    used_retry_memory_times: int = 0

    exceed_retry_gpu_gi: bool = False
    used_retry_gpu_gi_times: int = 0

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @staticmethod
    def get_state_from_execution_result_state(
        execution_result_state: ExecutionResultState,
    ) -> ActionRunState:
        if execution_result_state == ExecutionResultState.CREATED:
            state = ActionRunState.RUNNING
        elif execution_result_state == ExecutionResultState.SUCCESS:
            state = ActionRunState.SUCCESS
        elif execution_result_state == ExecutionResultState.CANCELED:
            state = ActionRunState.CANCELED
        elif execution_result_state == ExecutionResultState.ERROR:
            state = ActionRunState.ERROR
        else:
            raise UnknownExecutionStateError(
                "Unable to get Action run state from the execution result state"
            )
        return state

    @staticmethod
    def get_state_from_connection_state(
        connection_state: ExecutionConnectionState,
    ) -> ActionRunState:
        if connection_state in [
            ExecutionConnectionState.NONE,
            ExecutionConnectionState.ERROR,
        ]:
            state = ActionRunState.ERROR
        elif connection_state in [
            ExecutionConnectionState.PENDING,
            ExecutionConnectionState.ALLOCATED,
        ]:
            state = ActionRunState.PENDING
        elif connection_state in [
            ExecutionConnectionState.STARTED,
            ExecutionConnectionState.RUNNING,
        ]:
            state = ActionRunState.RUNNING
        elif connection_state == ExecutionConnectionState.CANCELED:
            state = ActionRunState.CANCELED
        elif connection_state == ExecutionConnectionState.COMPLETED:
            state = ActionRunState.SUCCESS
        else:
            raise UnknownExecutionStateError(
                "Unable to get Action run state from the execution connection"
            )
        return state

    def last_execution_result(self) -> ExecutionResult | None:
        if len(self.execution_results) > 0:
            return self.execution_results[-1]
        else:
            return None

    def will_run_eventually(self) -> bool:
        return self.state.will_run_eventually()

    def is_running_or_pending(self) -> bool:
        return self.state.is_running_or_pending()

    def set_canceled(self) -> None:
        self.state = ActionRunState.CANCELED

    def is_completed(self) -> bool:
        return self.state.is_completed()

    def is_error(self) -> bool:
        return self.state == ActionRunState.ERROR

    def add_error_message(self, error_message: str | None) -> None:
        if error_message is None:
            return

        if self.error_message:
            self.error_message = "\n" + error_message
        else:
            self.error_message = error_message

    def need_retry(self) -> bool:
        # If not in error state, no retry
        if self.state != ActionRunState.ERROR:
            return False

        # If retry reached its max value limit (because of OOM or error), no retry
        if (
            self.exceed_retry_gpu_gi
            or self.exceed_retry_memory_bytes
            or self.exceed_retry_error
        ):
            return False

        return True

    def __str__(self) -> str:
        results = "\n".join(str(r) for r in self.execution_results)
        return dedent(
            f"""ACTION_RUN:
        --> State : {self.state}
        --> Error Message Length : {-1 if self.error_message is None else len(self.error_message)}
        --> Exceed Retry Error: {self.exceed_retry_error}
        --> Used Retry Error: {self.used_retry_error}
        --> Exceed Retry Mem Bytes: {self.exceed_retry_memory_bytes}
        --> Used Retry Mem times: {self.used_retry_memory_times}
        --> Exceed Retry GPU GI: {self.exceed_retry_gpu_gi}
        --> Used Retry GPU GI times: {self.used_retry_gpu_gi_times}
        --> Execution results :
            
            {results}
            
        """
        )


@dataclass
class WorkflowRun:
    """
    A workflow run is an object representing the execution of a workflow,
    from the first result of a trigger to the last children of this execution.
    """

    id: str
    project_id: str
    state: WorkflowRunState
    number_of_completed_actions: int
    number_of_actions: int
    workflow_definition_id: str
    started_at: datetime
    last_result_at: Optional[datetime] = None

    runs: list[ActionRun] = field(default_factory=list)

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_workflow_deployment(
        cls, workflow_deployment: WorkflowDeployment
    ) -> "WorkflowRun":
        runs = []
        for action in workflow_deployment.actions:
            assert (
                action is not None and action.position is not None
            ), f"Malformed workflow_deployment: {workflow_deployment}"
            runs.append(
                ActionRun(
                    id=ActionRun.new_id(),
                    workflow_action=action,
                    execution_results=[],
                    state=ActionRunState.WAITING,
                    position=action.position,
                )
            )
        return WorkflowRun(
            id=cls.new_id(),
            project_id=workflow_deployment.project_id,
            workflow_definition_id=workflow_deployment.workflow_definition_id,
            state=WorkflowRunState.RUNNING,
            started_at=get_date(),
            number_of_actions=len(workflow_deployment.actions),
            number_of_completed_actions=0,
            runs=runs,
        )

    def refresh_state(self, is_retry_enabled: bool = True) -> None:
        """
        Refresh the state of the WorkflowRun regarding its ActionRun states.
        We need to know if the retry is disabled, to update the state properly
        """
        # Update number of completed actions
        self.number_of_completed_actions = len(
            [action for action in self.runs if action.is_completed()]
        )
        # Set the workflow run to error only if an action run is in error and cannot be retried
        if any(
            [
                (
                    action_run.state == ActionRunState.ERROR
                    and not (is_retry_enabled and action_run.need_retry())
                )
                for action_run in self.runs
            ]
        ):
            self.state = WorkflowRunState.ERROR
        # Set the workflow run to running if an action run is error and can be retried
        elif any(
            [
                (
                    action_run.state == ActionRunState.ERROR
                    and (is_retry_enabled and action_run.need_retry())
                )
                for action_run in self.runs
            ]
        ):
            self.state = WorkflowRunState.RUNNING
        elif any(
            [action_run.state == ActionRunState.CANCELED for action_run in self.runs]
        ):
            self.state = WorkflowRunState.CANCELED
        elif (
            any([action_run.will_run_eventually() for action_run in self.runs])
            or self.number_of_completed_actions < self.number_of_actions
            and all(
                [action_run.state == ActionRunState.SUCCESS for action_run in self.runs]
            )
        ):
            self.state = WorkflowRunState.RUNNING
        elif self.number_of_actions >= self.number_of_completed_actions and all(
            [action_run.state == ActionRunState.SUCCESS for action_run in self.runs]
        ):
            self.state = WorkflowRunState.COMPLETED
        else:
            actions_state = ",".join([run.state.name for run in self.runs])
            raise WorkflowRunStateError(
                f"Unable to compute state of the workflow run {self.id}: Current state {self.state} {self.number_of_completed_actions}/{self.number_of_actions}, with actions: {actions_state}"
            )

    def get_run_from_workflow_action(
        self, workflow_action: WorkflowAction
    ) -> ActionRun:
        for run in self.runs:
            if run.workflow_action.id == workflow_action.id:
                return run
        raise ActionRunNotFoundError(
            f"The action run for workflow action {workflow_action.id} not found in workflow run: {self}"
        )

    def get_run_from_execution_connection(
        self, execution_connection: ExecutionConnection
    ) -> ActionRun:
        for run in self.runs:
            if execution_connection.workflow_action.id == run.workflow_action.id:
                return run
        raise ActionRunNotFoundError(
            f"Action run not found for execution connection {execution_connection.id} in workflow run {self.id} with executions {self.runs}"
        )

    def pending_runs(self) -> list[ActionRun]:
        """
        Find the last completed run and returns its children
        FIXME: Does not work if there is multiple branches running in parallel. We have to walk the workflow DAG for this
        """
        last_completed_run = None
        # Find the last not pending run in the flow
        for run in self.runs:
            if not run.will_run_eventually():
                last_completed_run = run

        if last_completed_run is not None:
            return [
                self.get_run_from_workflow_action(workflow_action)
                for workflow_action in last_completed_run.workflow_action.next_actions
            ]
        return []

    def is_completed(self) -> bool:
        return self.state.is_completed()

    def _set_last_result(self, execution_result: ExecutionResult) -> None:
        if (
            execution_result.ended_at is not None
            and self.last_result_at is not None
            and execution_result.ended_at > self.last_result_at
        ) or self.last_result_at is None:
            self.last_result_at = execution_result.ended_at

    def connection_created(
        self, execution_connection: ExecutionConnection, is_retry_enabled: bool
    ) -> None:
        action_run = self.get_run_from_execution_connection(execution_connection)
        action_run.state = ActionRun.get_state_from_connection_state(
            execution_connection.state
        )
        self.refresh_state(is_retry_enabled)

    def execution_result_created(
        self, execution_result: ExecutionResult, is_retry_enabled: bool
    ) -> None:
        """
        Attach execution result to the Action run matching with the workflow action
        """
        for action_run in self.runs:
            if action_run.workflow_action.id == execution_result.workflow_action.id:
                action_run.execution_results.append(execution_result)
                action_run.state = ActionRun.get_state_from_execution_result_state(
                    execution_result.state
                )
                self.refresh_state(is_retry_enabled)
                return
        raise ActionRunNotFoundError(
            f"Action run not found for execution result {execution_result.id} in workflow run {self.id} with executions {self.runs}"
        )

    def connection_has_ended(
        self, execution_connection: ExecutionConnection, is_retry_enabled: bool
    ) -> None:
        # A completed workflow keep its state even if the connection stops
        if self.is_completed():
            return

        action_run = self.get_run_from_execution_connection(execution_connection)
        # A completed action should not be updated from a stopped connection: ignore it
        if not action_run.is_completed():
            action_run.state = ActionRun.get_state_from_connection_state(
                execution_connection.state
            )
            action_run.add_error_message(execution_connection.error_message)
            # Set the last result ended at date if present
            try:
                execution_result = action_run.execution_results[-1]
                execution_result.ended_at = execution_connection.ended_at
                self._set_last_result(execution_result)
            except IndexError:
                pass

            self.refresh_state(is_retry_enabled)

    def execution_result_finished(
        self, execution_result: ExecutionResult, is_retry_enabled: bool
    ) -> None:
        for action_run in self.runs:
            for existing_execution_result in action_run.execution_results:
                if existing_execution_result.id == execution_result.id:
                    action_run.state = ActionRun.get_state_from_execution_result_state(
                        execution_result.state
                    )
                    action_run.add_error_message(execution_result.error_message)
                    self._set_last_result(execution_result)
                    self.refresh_state(is_retry_enabled)
                    return
        raise ActionRunNotFoundError(
            f"Action run not found for execution result {execution_result.id} in workflow run {self.id}"
        )

    def previous_execution_result(self, action_run: ActionRun) -> ExecutionResult:
        for run in self.runs:
            if action_run.workflow_action in run.workflow_action.next_actions:
                if run.execution_results:
                    return run.execution_results[-1]
        raise ExecutionResultNotFoundError(
            f"Parent execution result not found for action {action_run}"
        )

    def get_last_execution_with_user_defined_exit(self) -> Optional[ExecutionResult]:
        for run in reversed(self.runs):
            last_results = run.last_execution_result()
            if last_results and last_results.http_status_code is not None:
                return last_results
        return None

    def stop(self) -> None:
        """
        Set the Workflow to a stopped state if not completed.
        """
        if not self.is_completed():
            for run in self.runs:
                if run.is_running_or_pending():
                    run.set_canceled()
            self.state = WorkflowRunState.CANCELED

    def get_running_execution_connections(self) -> list[str]:
        """
        :return: The execution connection ids linked to this Run that are running
        """
        execution_connections_to_stop = []
        if not self.is_completed():
            for run in self.runs:
                if run.will_run_eventually():
                    for execution_result in run.execution_results:
                        if execution_result.execution_connection_id is not None:
                            execution_connections_to_stop.append(
                                execution_result.execution_connection_id
                            )
        return execution_connections_to_stop

    def __str__(self) -> str:
        runs = "\n".join(str(r) for r in self.runs)
        return dedent(
            f"""WORKFLOW_RUN:
        --> State : {self.state}
        --> Number of actions : {self.number_of_actions}
        --> Number of action results : {self.number_of_completed_actions}
        --> Started at : {self.started_at}
        --> Last result at : {self.last_result_at}
        --> ActionRuns :
        
            {runs}
            
        """
        )
