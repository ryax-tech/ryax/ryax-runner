from dataclasses import dataclass, field
from datetime import datetime
from typing import Tuple

from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
    Objectives,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionResources,
)
from ryax.runner.domain.action_deployment.action_deployment_view import (
    ActionDeploymentSchedulerView,
)
from ryax.runner.domain.site.site_entities import Site, NodePool


@dataclass
class PendingExecutionConnection:
    """
    This object is used for pending ExecutionConnections with the only necessary element for the scheduler.
    """

    id: str
    action_definition_id: str
    project_id: str
    is_a_trigger: bool
    submitted_at: datetime
    action_container_image: str
    addons: dict[str, dict] | None = None
    resources: ActionResources | None = None
    site_list: list[Site] | None = None
    node_pool_list: list[NodePool] | None = None
    site_type_list: list[SiteType] | None = None
    arch_list: list[NodeArchitecture] | None = None
    objectives: dict[Objectives, float] | None = None
    node_pool_objective_score: dict[str, float] | None = None


@dataclass
class PlatformStateSnapshot:
    """A Snapshot of the platform current state"""

    """ A dict that contains the list of action deployment for each tuple (node pool, action definition)
    """
    action_deployments_by_definition_id: dict[
        Tuple[str, str], list[ActionDeploymentSchedulerView]
    ] = field(default_factory=dict)

    nb_actions_eventually_deployed: int = 0
