# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from abc import ABC, abstractmethod

from ryax.runner.domain.stored_file.stored_file import StoredFile


class IStoredFileRepository(ABC):
    @abstractmethod
    def add(self, stored_file: StoredFile) -> None:
        pass

    @abstractmethod
    def get(self, stored_file_id: str, project_id: str) -> StoredFile:
        pass

    @abstractmethod
    def get_by_path(self, stored_file_path: str, project_id: str) -> StoredFile:
        pass
