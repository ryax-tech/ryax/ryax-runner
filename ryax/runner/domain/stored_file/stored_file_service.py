# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from abc import ABC, abstractmethod
from typing import BinaryIO

from ryax.runner.domain.stored_file.stored_file import StoredFile


class IStoredFileView(ABC):
    @abstractmethod
    async def download_file(self, file_path: str) -> BinaryIO:
        ...

    @abstractmethod
    def get(self, stored_file_id: str, project_id: str) -> StoredFile:
        ...

    @abstractmethod
    def get_by_file_path(self, stored_file_path: str, project_id: str) -> StoredFile:
        ...

    @abstractmethod
    async def zip(self, stored_files: list[StoredFile]) -> StoredFile:
        ...
