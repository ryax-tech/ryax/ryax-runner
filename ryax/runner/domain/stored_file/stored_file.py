# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Tuple

from ryax.common.domain.utils import get_random_id


@dataclass
class StoredFile:
    id: str
    io_name: str
    file_path: str
    project_id: str
    mimetype: str | None
    encoding: str | None
    size_in_bytes: int

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_info(
        cls,
        io_name: str,
        file_path: str,
        project_id: str,
        mimetype: Optional[str],
        encoding: Optional[str],
        size_in_bytes: int,
    ) -> "StoredFile":
        return cls(
            id=cls.new_id(),
            io_name=io_name,
            file_path=file_path,
            project_id=project_id,
            mimetype=mimetype,
            size_in_bytes=size_in_bytes,
            encoding=encoding,
        )

    def get_headers(self) -> dict:
        return {
            "Content-Type": f"{self.mimetype}"
            + (f"; charset={self.encoding}" if self.encoding else ""),
            "Content-Length": str(self.size_in_bytes),
            "Content-Disposition": f'attachment; filename="{Path(self.file_path).name}"',
        }

    def get_download_url(self) -> Tuple[str, dict]:
        # WARNING should be consistent with the stored file API
        return f"/filestore/{self.id}/{Path(self.file_path).name}", self.get_headers()
