import uuid
from dataclasses import dataclass, field


@dataclass
class LogLine:
    id: str
    line: str
    index: int
    size_in_bytes: int

    @classmethod
    def create(cls, line: str, index: int, size: int | None = None) -> "LogLine":
        return cls(
            id=str(uuid.uuid4()),
            line=line,
            index=index,
            size_in_bytes=size if size else len(line.encode()),
        )


@dataclass
class ActionLogs:
    """Store log lines. Store the associated id of the logs' producer. Might be an ExecutionResult or an ActionDeployment"""

    associated_id: str
    project_id: str
    logs_size_in_bytes: int = 0
    log_lines: list[LogLine] = field(default_factory=list)
    index: int = 0

    @classmethod
    def create(cls, associated_id: str, project_id: str) -> "ActionLogs":
        return cls(associated_id=associated_id, project_id=project_id)

    def __str__(self) -> str:
        return "".join([log.line for log in self.log_lines])

    def append(self, line: str) -> None:
        new_line = LogLine.create(line, index=self.index)
        self.index = self.index + 1
        self.log_lines.append(new_line)
        self.logs_size_in_bytes += new_line.size_in_bytes


class LogsNotFoundError(Exception):
    message: str = "Logs of this action was not captured"
