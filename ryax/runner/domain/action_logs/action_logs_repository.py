import abc

from ryax.runner.domain.action_logs.action_logs_entities import ActionLogs


class IActionLogsRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, logs: ActionLogs) -> None:
        ...

    @abc.abstractmethod
    def get(self, associated_id: str) -> ActionLogs:
        ...

    @abc.abstractmethod
    def reset(self, associated_id: str) -> None:
        ...

    @abc.abstractmethod
    def get_logs_as_string(self, associated_id: str) -> str:
        ...
