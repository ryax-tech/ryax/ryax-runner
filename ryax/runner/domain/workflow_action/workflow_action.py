# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import Union, cast

from ryax.common.domain.registration.registration_values import (
    NodeArchitecture,
    SiteType,
    ObjectiveScores,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionDynamicOutput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
)


class UnsupportedInputTypeError(Exception):
    pass


class WorkflowActionIONotFoundError(Exception):
    pass


@dataclass
class ReferenceInput:
    id: str
    name: str
    action_name: str
    # Must be equal to input/output key in action deployment inputs/outputs dict
    output_name: str

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_name_value(
        cls, name: str, action_name: str, output_name: str
    ) -> "ReferenceInput":
        return ReferenceInput(
            id=cls.new_id(),
            name=name,
            action_name=action_name,
            output_name=output_name,
        )


@dataclass
class StaticInput:
    id: str
    name: str
    value: str | int | float | bool | bytes

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_name_value(
        cls, name: str, value: str | int | float | bool | bytes
    ) -> "StaticInput":
        return StaticInput(id=cls.new_id(), name=name, value=value)


@dataclass
class SchedulingConstraints:
    site_list: list[Site]
    site_type_list: list[SiteType]
    node_pool_list: list[NodePool]
    # FIXME arch constraints are not usable right now because Ryax is only able to build actions for one arch
    arch_list: list[NodeArchitecture]


@dataclass
class SchedulingObjectives:
    energy: float
    cost: float
    performance: float
    # TODO already in the scheduler but never used: provide objective score per node pool for each Action (requires some extra benchmarking step)
    # Performance Score for this action for each node pool
    node_pool_objective_score: ObjectiveScores | None = None


@dataclass
class WorkflowAction:
    """
    A WorkflowAction is an action within a workflow.
    This means that it defines its input values, and is linked to a ActionDefinition.
    Multiple WorkflowActions can share the same ActionDefinition.
    A ActionDeployment is a deployed ActionDefinition.
    Ryax uses a WorkflowAction when triggering new executions.
    """

    id: str
    id_in_studio: str
    workflow_deployment_id: str
    workflow_definition_id: str
    definition: ActionDefinition
    static_inputs: list[StaticInput]
    reference_inputs: list[ReferenceInput]
    constraints: SchedulingConstraints | None = None
    objectives: SchedulingObjectives | None = None
    display_name: str | None = None
    dynamic_outputs: list[ActionDynamicOutput] = field(default_factory=list)
    next_actions: list["WorkflowAction"] = field(default_factory=list)
    is_root_action: bool = False
    # This field is only set for a Trigger that is running
    connection_execution_id: str | None = None
    # Attribute use by the database to keep action list ordered
    position: int | None = None
    # Addon name key represents the activated addons with value being a dict of default values
    addons: dict[str, dict] = field(default_factory=dict)

    def get_resource_time_request(self) -> float | None:
        if self.definition.resources is not None:
            return self.definition.resources.time
        return None

    def get_outputs(self) -> list[ActionOutput]:
        if self.definition.has_dynamic_outputs:
            outputs = cast(list[ActionOutput], self.dynamic_outputs.copy())
            outputs.extend(self.definition.outputs.copy())
            return outputs
        else:
            return self.definition.outputs.copy()

    def get_writable_outputs(self) -> list[ActionOutput]:
        outputs = self.get_outputs()
        # FIXME: This is a hack we should have special type of outputs for read_only/internal outputs
        return [output for output in outputs if not output.name.startswith("ryax_")]

    def get_output_type(self, output_name: str) -> ActionIOType:
        for output_value in self.get_outputs():
            if output_value.name == output_name:
                return output_value.type
        raise WorkflowActionIONotFoundError(
            f"Could not find output with name: '{output_name}' in the list of outputs: {self.get_outputs()}"
        )

    def get_output_type_by_name(self, output_name: str) -> ActionIOType:
        for output_value in self.get_outputs():
            if output_value.name == output_name:
                return output_value.type
        raise WorkflowActionIONotFoundError(
            f"Could not find output with id: '{output_name}' in the list of outputs: {self.get_outputs()}"
        )

    def get_output_type_with_info(self, output_info: str) -> ActionIOType:
        for output_value in self.get_outputs():
            if output_info in (output_value.id, output_value.name):
                return output_value.type
        raise WorkflowActionIONotFoundError(
            f"Could not find output with id or name: '{output_info}' in the list of outputs: {self.get_outputs()}"
        )

    def get_input_static_value(
        self, input_name: str
    ) -> Union[str, int, float, bool, bytes, dict]:
        """Return the static value or an input, or an empty string if not defined"""
        for input in self.static_inputs:
            if input.name == input_name:
                return input.value
        return ""

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_create_command(
        cls,
        action: CreateWorkflowDeploymentCommand.Action,
        action_definition: ActionDefinition,
        workflow_deployment_id: str,
        workflow_definition_id: str,
        constraints: SchedulingConstraints,
    ) -> "WorkflowAction":
        # Convert inputs
        reference_inputs: list[ReferenceInput] = []
        static_inputs: list[StaticInput] = []
        for input_name, static_input_value in action.static_inputs.items():
            static_inputs.append(
                StaticInput.create_from_name_value(
                    name=input_name, value=static_input_value
                )
            )

        for input_name, ref_input_value in action.reference_inputs.items():
            # This is a reference
            # TODO check that this action ID exists
            ref_action_name: str = ref_input_value["action"]
            # TODO check that this output ID exists in this action
            ref_output_name: str = ref_input_value["output"]
            reference_inputs.append(
                ReferenceInput.create_from_name_value(
                    name=input_name,
                    action_name=ref_action_name,
                    output_name=ref_output_name,
                )
            )

        return WorkflowAction(
            id=cls.new_id(),
            id_in_studio=action.id_in_studio,
            display_name=action.display_name,
            definition=action_definition,
            next_actions=[],
            reference_inputs=reference_inputs,
            static_inputs=static_inputs,
            dynamic_outputs=action.dynamic_outputs,
            workflow_definition_id=workflow_definition_id,
            workflow_deployment_id=workflow_deployment_id,
            addons=action.addons,
            constraints=constraints,
            objectives=SchedulingObjectives(
                cost=action.objectives.cost,
                energy=action.objectives.energy,
                performance=action.objectives.performance,
            ),
        )
