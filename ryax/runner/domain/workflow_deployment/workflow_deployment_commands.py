# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import Dict, List, Optional

from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDynamicOutput,
)
from ryax.common.domain.internal_messaging.base_messages import BaseCommand


@dataclass
class CreateWorkflowDeploymentCommand(BaseCommand):
    @dataclass
    class Action:
        @dataclass
        class ActionResources:
            cpu: Optional[float] = None
            memory: Optional[int] = None
            time: Optional[float] = None
            gpu: Optional[int] = None

        @dataclass
        class ActionConstraints:
            site_list: list[str]
            node_pool_list: list[str]
            site_type_list: list[SiteType]
            arch_list: list[NodeArchitecture]

        @dataclass
        class ActionObjectives:
            energy: float
            cost: float
            performance: float

        id_in_studio: str
        action_definition_id: str
        constraints: ActionConstraints
        objectives: ActionObjectives
        resources: Optional[ActionResources] = None
        display_name: Optional[str] = ""
        endpoint: Optional[str] = None
        static_inputs: Dict[str, str] = field(default_factory=dict)
        # Inside dict MUST contain two entry: action and output
        reference_inputs: Dict[str, Dict[str, str]] = field(default_factory=dict)
        next_actions: List[str] = field(default_factory=list)
        dynamic_outputs: List[ActionDynamicOutput] = field(default_factory=list)
        addons: Dict[str, dict] = field(default_factory=dict)

    @dataclass
    class Result:
        key: str
        technical_name: str
        workflow_action_io_id: str
        action_id: str

    project_id: str
    workflow_name: str
    actions: List[Action]
    workflow_definition_id: str
    results: List[Result]


@dataclass
class StartWorkflowDeploymentCommand(BaseCommand):
    workflow_id: str


@dataclass
class StopWorkflowDeploymentCommand(BaseCommand):
    workflow_id: str
    graceful: bool = False
    error: Optional[str] = None


@dataclass
class StopWorkflowDeploymentFromDefinitionIdCommand(BaseCommand):
    workflow_definition_id: str
    graceful: bool = False
    error: Optional[str] = None


@dataclass
class DeleteWorkflowDeploymentCommand(BaseCommand):
    workflow_id: str
