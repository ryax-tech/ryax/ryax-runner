# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from enum import Enum


class WorkflowDeploymentState(Enum):
    NONE = "None"
    CREATED = "Created"
    STARTED = "Started"
    RUNNING = "Running"
    STOPPED_GRACEFULLY = "Stopped gracefully"
    STOPPED = "Stopped"
    PAUSED = "Paused"

    @staticmethod
    def eventually_running_state() -> list["WorkflowDeploymentState"]:
        return [
            WorkflowDeploymentState.CREATED,
            WorkflowDeploymentState.STARTED,
            WorkflowDeploymentState.RUNNING,
        ]
