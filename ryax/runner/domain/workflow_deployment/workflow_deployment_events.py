# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import Optional

from ryax.common.domain.internal_messaging.base_messages import BaseEvent


@dataclass
class WorkflowStoppedEvent(BaseEvent):
    workflow_definition_id: str
    workflow_deployment_id: str
    error: Optional[str] = None
    graceful: Optional[bool] = None


@dataclass
class WorkflowUndeployedSuccessfully(BaseEvent):
    workflow_definition_id: str
    error: Optional[str] = None


@dataclass
class WorkflowDeployedSuccessfully(BaseEvent):
    workflow_definition_id: str
    workflow_deployment_id: str
