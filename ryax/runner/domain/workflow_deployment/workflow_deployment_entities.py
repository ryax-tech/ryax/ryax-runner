# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import Dict, List, Optional

from prometheus_client import Gauge

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.utils import get_date
from ryax.runner.domain.workflow_action.workflow_action import (
    WorkflowAction,
    SchedulingConstraints,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.domain.workflow_result.workflow_result import WorkflowResult

workflow_gauge = Gauge(
    "ryax_workflow_deployments",
    "Number of workflows deployed by state",
    labelnames=["name", "state", "project_id", "definition_id"],
)


class InvalidWorkflowError(Exception):
    pass


class WorkflowDeploymentNotFoundError(Exception):
    message = "Workflow deployment not found."


class MultipleRunningWorkflowDeploymentsError(Exception):
    pass


@dataclass
class WorkflowDeployment:
    id: str
    name: str
    project_id: str
    creation_time: str
    workflow_definition_id: str
    stopped_time: str | None = None
    state: WorkflowDeploymentState = WorkflowDeploymentState.NONE
    actions: list[WorkflowAction] = field(default_factory=list)
    results: list[WorkflowResult] = field(default_factory=list)

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_create_command(
        cls,
        cmd: CreateWorkflowDeploymentCommand,
        action_definitions: dict[str, ActionDefinition],
        constraints: dict[str, SchedulingConstraints],
    ) -> "WorkflowDeployment":
        # Check action names are unique
        action_names = [action.id_in_studio for action in cmd.actions]
        for name in action_names:
            if action_names.count(name) > 1:
                raise InvalidWorkflowError(
                    f"Action names must be unique in a workflow, found duplicate name: {name}"
                )

        results: List[WorkflowResult] = []
        actions: List[WorkflowAction] = []
        workflow_deployment_id = cls.new_id()

        for action in cmd.actions:
            actions.append(
                WorkflowAction.create_from_create_command(
                    action,
                    action_definitions[action.action_definition_id],
                    workflow_deployment_id,
                    cmd.workflow_definition_id,
                    constraints[action.id_in_studio],
                )
            )
        for result in cmd.results:
            results.append(
                WorkflowResult.create_from_create_command(
                    result,
                    workflow_deployment_id=workflow_deployment_id,
                )
            )

        # Determine next actions relationship
        for cmd_action, domain_action in zip(cmd.actions, actions):
            for next_action_name in cmd_action.next_actions:
                for next_domain_action in actions:
                    if next_domain_action.id_in_studio == next_action_name:
                        domain_action.next_actions.append(next_domain_action)
                        break
                else:
                    raise InvalidWorkflowError(
                        f"The next action of '{cmd_action.id_in_studio}' called '{next_action_name}' is not present in the workflow."
                    )

        workflow_deployment = WorkflowDeployment(
            id=workflow_deployment_id,
            name=cmd.workflow_name,
            project_id=cmd.project_id,
            creation_time=get_date().isoformat(),
            workflow_definition_id=cmd.workflow_definition_id,
            actions=actions,
            results=results,
        )

        return workflow_deployment

    def get_action(self, action_name: str) -> Optional[WorkflowAction]:
        for action in self.actions:
            if action.id_in_studio == action_name:
                return action
        return None

    def get_root_action(self) -> WorkflowAction:
        """
        :return:
        """
        if len(self.actions) == 0:
            raise InvalidWorkflowError(
                "A workflow must have at least one Action, found 0."
            )
        for action in self.actions:
            if action.is_root_action:
                return action

        # root action mark not found, find it using the tree
        parents_dict: Dict[str, List[WorkflowAction]] = {}

        for action in self.actions:
            for child_action in action.next_actions:
                if parents_dict.get(child_action.id) is None:
                    parents_dict[child_action.id] = []
                parents_dict[child_action.id].append(action)

        for action in self.actions:
            if not parents_dict.get(action.id):
                return action
        raise InvalidWorkflowError(f"A loop is detected for workflow {self.id}.")

    def workflow_gauge_wih_labels(self) -> Gauge:
        return workflow_gauge.labels(
            name=self.name,
            state=self.state.value,
            definition_id=self.workflow_definition_id,
            project_id=self.project_id,
        )

    def set_state(self, state: WorkflowDeploymentState) -> None:
        self.workflow_gauge_wih_labels().dec()
        self.state = state
        self.workflow_gauge_wih_labels().inc()

    def set_created(self) -> None:
        self.state = WorkflowDeploymentState.CREATED
        self.workflow_gauge_wih_labels().inc()

    def set_started(self) -> None:
        self.set_state(WorkflowDeploymentState.STARTED)

    def set_running(self) -> None:
        self.set_state(WorkflowDeploymentState.RUNNING)

    def set_paused(self) -> None:
        self.set_state(WorkflowDeploymentState.PAUSED)

    def set_stopped_gracefully(self) -> None:
        self.set_state(WorkflowDeploymentState.STOPPED_GRACEFULLY)

    def set_stopped(self) -> None:
        self.set_state(WorkflowDeploymentState.STOPPED)
        self.stopped_time = get_date().isoformat()

    def is_stopped(self) -> bool:
        return self.state in [
            WorkflowDeploymentState.STOPPED_GRACEFULLY,
            WorkflowDeploymentState.STOPPED,
        ]

    def is_paused(self) -> bool:
        return self.state == WorkflowDeploymentState.PAUSED
