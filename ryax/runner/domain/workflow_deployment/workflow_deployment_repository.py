# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List

from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)


class IWorkflowDeploymentRepository(abc.ABC):
    @abc.abstractmethod
    def get(self, workflow_deployment_id: str) -> WorkflowDeployment:
        ...

    @abc.abstractmethod
    def get_eventually_running_workflow_by_workflow_definition_id(
        self, workflow_definition_id: str, eager_load: bool = False
    ) -> WorkflowDeployment:
        ...

    @abc.abstractmethod
    def get_all_running(self) -> List[WorkflowDeployment]:
        ...

    @abc.abstractmethod
    def get_all_paused_or_running(self) -> List[WorkflowDeployment]:
        ...

    @abc.abstractmethod
    def add(self, workflow: WorkflowDeployment) -> None:
        ...

    @abc.abstractmethod
    def delete(self, workflow_deployment_id: str) -> None:
        ...

    @abc.abstractmethod
    def get_workflow_action_by_id(self, workflow_action_id: str) -> WorkflowAction:
        ...

    @abc.abstractmethod
    def list_active_workflows_for_this_action_definition(
        self, action_definition_id: str
    ) -> List[WorkflowDeployment]:
        ...
