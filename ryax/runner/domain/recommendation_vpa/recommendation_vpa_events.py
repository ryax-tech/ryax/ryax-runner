from dataclasses import dataclass
from datetime import datetime

from ryax.common.domain.internal_messaging.base_messages import BaseCommand


@dataclass
class RecommendationsVPAUpdateCommand(BaseCommand):
    @dataclass
    class RecommendationVPA:
        container_image: str
        # cpu: float
        memory: int
        gpu_mig_instance: str
        timestamp: datetime
        is_oom_bumpup: bool
        is_gpuoom_bumpup: bool

    recommendations: list[RecommendationVPA]
