import abc

from ryax.runner.domain.recommendation_vpa.recommendation_vpa_entities import (
    RecommendationVPA,
)


class IRecommendationVPARepository(abc.ABC):
    @abc.abstractmethod
    def add(self, recommendation: RecommendationVPA) -> None:
        ...

    @abc.abstractmethod
    def get(self, key: str) -> RecommendationVPA:
        ...

    @abc.abstractmethod
    def list(self) -> list[RecommendationVPA]:
        ...
