from dataclasses import dataclass
from datetime import datetime, timezone
from typing import Optional

from ryax.runner.domain.recommendation_vpa.recommendation_vpa_events import (
    RecommendationsVPAUpdateCommand,
)


@dataclass
class RecommendationVPA:
    key: str
    timestamp: datetime
    last_oom_timestamp: datetime
    last_gpuoom_timestamp: datetime
    memory: Optional[int] = None
    gpu_mig_instance: Optional[str] = None

    @classmethod
    def from_command(
        cls, rec: RecommendationsVPAUpdateCommand.RecommendationVPA
    ) -> "RecommendationVPA":
        return cls(
            key=rec.container_image,
            timestamp=rec.timestamp,
            memory=rec.memory,
            gpu_mig_instance=rec.gpu_mig_instance,
            # Set a very ancient timestamp if no OOM happens at all...
            last_oom_timestamp=rec.timestamp
            if rec.is_oom_bumpup
            else datetime(1, 1, 1, 0, 0, 0, 0, tzinfo=timezone.utc),
            last_gpuoom_timestamp=rec.timestamp
            if rec.is_gpuoom_bumpup
            else datetime(1, 1, 1, 0, 0, 0, 0, tzinfo=timezone.utc),
        )
