# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from enum import Enum


class ExecutionConnectionState(Enum):
    NONE = "None"
    PENDING = "Pending"
    ALLOCATED = "Allocated"  # an ActionDeployed has been chosen by the scheduler,
    # but the code is not running yet
    STARTED = "Started"
    RUNNING = "Running"
    STOPPING = "Stopping"
    COMPLETED = "Completed"
    CANCELED = "Canceled"
    ERROR = "Error"

    @staticmethod
    def eventually_running_states() -> list["ExecutionConnectionState"]:
        return [
            ExecutionConnectionState.PENDING,
            ExecutionConnectionState.ALLOCATED,
            ExecutionConnectionState.STARTED,
            ExecutionConnectionState.RUNNING,
        ]

    @staticmethod
    def is_allocated_states() -> list["ExecutionConnectionState"]:
        return [
            ExecutionConnectionState.STARTED,
            ExecutionConnectionState.RUNNING,
            ExecutionConnectionState.STOPPING,
        ]
