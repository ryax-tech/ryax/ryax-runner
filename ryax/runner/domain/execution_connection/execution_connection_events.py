# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import Optional

from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.common.domain.internal_messaging.base_messages import BaseEvent


@dataclass
class BaseExecutionConnectionEvent(BaseEvent):
    execution_connection_id: str


@dataclass
class ExecutionConnectionPendingEvent(BaseExecutionConnectionEvent):
    ...


@dataclass
class ExecutionConnectionEndedEvent(BaseExecutionConnectionEvent):
    status: ExecutionConnectionState
    error_message: Optional[str] = None


@dataclass
class ExecutionConnectionUpdatedEvent(BaseExecutionConnectionEvent):
    ...


@dataclass
class ExecutionConnectionErroredEvent(BaseExecutionConnectionEvent):
    error_message: str


@dataclass
class ExecutionConnectionRetryIfNeededEvent(BaseExecutionConnectionEvent):
    ...
