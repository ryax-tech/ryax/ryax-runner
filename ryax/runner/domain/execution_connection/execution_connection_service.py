# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from logging import getLogger

from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)

logger = getLogger(__name__)


class ActionExecutionTimeout(Exception):
    pass


class ActionExecutionRequestFailError(Exception):
    pass


class IExecutionService(abc.ABC):
    """Interface for action Execution"""

    @abc.abstractmethod
    async def start(
        self, execution: ExecutionConnection, addons_extra_params: dict
    ) -> None:
        """
        Create the tasks for the given execution
        :param execution: the execution to start
        :param addons_extra_params: a dict containing the values of extra parameters for addons
        """
        ...

    @abc.abstractmethod
    async def stop(self, execution: ExecutionConnection) -> None:
        """
        Cancel the tasks create for the given execution
        :param execution: the execution to cancel
        """
        ...
