# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from datetime import datetime
from typing import Optional

from ryax.common.domain.internal_messaging.base_messages import BaseCommand
from ryax.runner.domain.utils import get_date


@dataclass
class CreateExecutionConnectionCommand(BaseCommand):
    project_id: str
    action_definition_id: str
    workflow_action_id: str
    submitted_at: datetime = field(default_factory=get_date)
    parent_execution_result_id: Optional[str] = None


@dataclass
class ExecutionConnectionBaseCommand(BaseCommand):
    execution_connection_id: str


@dataclass
class GetExecutionConnectionStateCommand(ExecutionConnectionBaseCommand):
    pass


@dataclass
class AllocateExecutionConnectionCommand(ExecutionConnectionBaseCommand):
    action_deployment_id: str


@dataclass
class DeallocateExecutionConnectionCommand(ExecutionConnectionBaseCommand):
    action_deployment_id: str


@dataclass
class StartExecutionConnectionCommand(ExecutionConnectionBaseCommand):
    pass


@dataclass
class StopExecutionConnectionCommand(ExecutionConnectionBaseCommand):
    pass


@dataclass
class InitializeExecutionResultCommand(ExecutionConnectionBaseCommand):
    executor_id: str
    log_delimiter: Optional[str] = None
