# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc

from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.scheduler.scheduler_entities import PendingExecutionConnection


class IExecutionConnectionRepository(abc.ABC):
    @abc.abstractmethod
    def get(
        self, execution_connection_id: str, eager_load: bool = False
    ) -> ExecutionConnection:
        ...

    @abc.abstractmethod
    def list_eventually_running(
        self,
    ) -> list[ExecutionConnection]:
        """
        :return:  All ExecutionConnection that will eventually run.
        """
        ...

    @abc.abstractmethod
    def list_is_allocated(
        self,
    ) -> list[ExecutionConnection]:
        """
        :return: List all Execution connections that are allocated to a deployed action
        """
        ...

    @abc.abstractmethod
    def list_pending(
        self,
    ) -> list[PendingExecutionConnection]:
        """
        :return:  Get all ExecutionConnection in pending state sorted with the oldest submitted first
        """
        ...

    @abc.abstractmethod
    def list_active_from_workflow_action_id(
        self, workflow_action_id: str
    ) -> list[ExecutionConnection]:
        ...

    @abc.abstractmethod
    def add(self, execution: ExecutionConnection) -> None:
        ...

    @abc.abstractmethod
    def delete(self, execution_id: str) -> None:
        ...

    @abc.abstractmethod
    def list_log_delimiters_by_action_deployment(
        self, action_deployment: ActionDeployment
    ) -> list[str]:
        ...

    @abc.abstractmethod
    def list_allocated_running(
        self,
    ) -> list[ExecutionConnection]:
        """
        :return:  All ExecutionConnection that are in one of the following state:
        ALLOCATED, RUNNING
        """
        ...
