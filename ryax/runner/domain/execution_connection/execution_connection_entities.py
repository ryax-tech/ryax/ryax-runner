# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from textwrap import dedent
from dataclasses import dataclass, field
from datetime import datetime
from typing import Dict, Optional, TypeAlias

from prometheus_client import Gauge

from ryax.common.domain.state_machine_observability import StateMachine
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionIO,
    ActionOutput,
    ActionInput,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    CreateExecutionConnectionCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.utils import get_date
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction

logger = logging.getLogger(__name__)

execution_gauge = Gauge(
    "ryax_execution_connections",
    "Number of execution connection",
    labelnames=["state", "project_id"],
)


class ExecutionConnectionStateMachine(StateMachine):
    stateType: TypeAlias = ExecutionConnectionState
    # Automatically grab from handlers annotations
    stateMachine = {}
    id_attribute = "execution_connection_id"


@dataclass
class ExecutionConnection:
    id: str
    project_id: str
    workflow_action: WorkflowAction
    # ID of the workflow run, equal to none if it is the ExecutionConnection of a source,
    # as a connection of a source generates workflow runs.
    workflow_run_id: Optional[str]
    submitted_at: datetime
    # Default timeout for action executions in seconds
    time_allotment: float
    # The associated action deployment: is empty before being scheduled
    action_deployment: Optional[ActionDeployment] = None
    # Raw types inputs
    inputs: Dict = field(default_factory=dict)
    # Execution state
    state: ExecutionConnectionState = field(default=ExecutionConnectionState.NONE)
    error_message: str = ""
    executor_id: Optional[str] = None
    started_at: Optional[datetime] = None
    ended_at: Optional[datetime] = None
    end_of_log_delimiter_queue: list = field(default_factory=list)
    # Parent execution result that trigger this execution
    parent_execution_result: Optional[ExecutionResult] = None

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_run_execution_command(
        cls,
        cmd: CreateExecutionConnectionCommand,
        inputs: dict,
        parent_execution_result: Optional[ExecutionResult],
        action: WorkflowAction,
        time_allotment: float,
    ) -> "ExecutionConnection":
        workflow_run_id = (
            parent_execution_result.workflow_run_id if parent_execution_result else None
        )
        return cls(
            id=cls.new_id(),
            workflow_run_id=workflow_run_id,
            inputs=inputs,
            workflow_action=action,
            parent_execution_result=parent_execution_result,
            project_id=cmd.project_id,
            submitted_at=cmd.submitted_at,
            time_allotment=time_allotment,
        )

    def execution_gauge_wih_labels(self) -> Gauge:
        return execution_gauge.labels(
            state=self.state.value,
            project_id=self.project_id,
        )

    def set_state(self, state: ExecutionConnectionState) -> None:
        logger.debug(
            "Setting state of %s(%s) from %s to %s",
            self.__class__,
            self.id,
            self.state,
            state,
        )
        ExecutionConnectionStateMachine.check_transition(self.state, state)
        self.execution_gauge_wih_labels().dec()
        self.state = state
        self.execution_gauge_wih_labels().inc()

    def set_started(self) -> None:
        self.set_state(ExecutionConnectionState.STARTED)
        self.started_at = get_date()

    def set_running(self) -> None:
        self.set_state(ExecutionConnectionState.RUNNING)

    def set_completed(self) -> None:
        self.set_state(ExecutionConnectionState.COMPLETED)
        self.ended_at = get_date()

    def set_error(self, msg: Optional[str]) -> None:
        if msg is not None:
            self.error_message += msg + "\n"
        self.set_state(ExecutionConnectionState.ERROR)
        self.ended_at = get_date()

    def set_pending(self) -> None:
        self.set_state(ExecutionConnectionState.PENDING)

    def set_allocated(self) -> None:
        self.set_state(ExecutionConnectionState.ALLOCATED)

    def set_canceled(self, error_message: str | None = None) -> None:
        self.set_state(ExecutionConnectionState.CANCELED)
        if error_message is not None:
            self.error_message = error_message
        self.ended_at = get_date()

    def set_stopping(self) -> None:
        self.set_state(ExecutionConnectionState.STOPPING)

    def remove_end_of_log_delimiter_from_queue(self, end_of_log_delimiter: str) -> None:
        first_end_of_log_delimiter = self.end_of_log_delimiter_queue.pop(0)
        assert first_end_of_log_delimiter == end_of_log_delimiter

    def get_action_input(self, input_name: str) -> ActionIO:
        """
        Returns input types of inputs from name from the action definition. Also return type for injected inputs.
        """
        assert (
            self.action_deployment is not None
        ), "Action deployment should be set at this point"
        return self.action_deployment.action_definition.get_input_type(input_name)

    def get_action_outputs(
        self, filter_type: ActionIOType | None = None
    ) -> list[ActionOutput]:
        """
        Returns all outputs from this action definition filtered by type.
        """
        assert (
            self.action_deployment is not None
        ), "Action deployment should be set at this point"
        if filter_type is not None:
            return [
                action
                for action in self.action_deployment.action_definition.outputs
                if action.type == filter_type
            ]
        return self.action_deployment.action_definition.outputs

    def get_action_inputs(
        self,
    ) -> list[ActionInput]:
        """
        Returns all inputs from this action definition
        """
        assert (
            self.action_deployment is not None
        ), "Action deployment should be set at this point"
        return self.action_deployment.action_definition.get_action_inputs()

    def is_running(self) -> bool:
        return self.state == ExecutionConnectionState.RUNNING

    def __str__(self) -> str:
        return dedent(
            f"""EXECUTION CONNECTION:
        --> ID : {self.id}
        --> Workflow Run ID : {self.workflow_run_id}
        --> Submitted At : {self.submitted_at}
        --> Time Allotment : {self.time_allotment}
        --> State : {self.state}
        --> Error Message Length : {-1 if self.error_message is None else len(self.error_message)}
        --> Started At : {self.started_at}
        --> Ended At : {self.ended_at}
            
        """
        )

    def retry_enabled(self) -> bool:
        """
        Only enable retry for action running on kubernetes and that are not root action (Trigger)
        """
        return (
            self.action_deployment is not None
            and self.action_deployment.get_deployment_type() == "k8s"
            and not self.workflow_action.is_root_action
        )
