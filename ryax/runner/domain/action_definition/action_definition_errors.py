# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
class ActionDefinitionNotFoundError(Exception):
    error: str = "Action definition not found."


class ActionDefinitionAlreadyExists(Exception):
    pass


class ActionDefinitionIONotFoundError(Exception):
    pass
