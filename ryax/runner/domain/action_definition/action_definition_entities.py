# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import enum
from dataclasses import dataclass, field
from logging import getLogger
from typing import List, Optional, Type, Union

from ryax.runner.domain.action_definition.action_definition_errors import (
    ActionDefinitionIONotFoundError,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
    DynamicOutputOrigin,
)
from ryax.common.domain.utils import get_random_id

logger = getLogger(__name__)


class UnsupportedIOType(Exception):
    pass


@dataclass
class ActionResources:
    id: str
    cpu: Optional[float] = None
    memory: Optional[int] = None
    time: Optional[float] = None
    gpu: Optional[int] = None

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_data(
        cls,
        cpu: Optional[float],
        memory: Optional[int],
        time: Optional[float],
        gpu: Optional[int],
    ) -> "ActionResources":
        return ActionResources(
            id=cls.new_id(), cpu=cpu, memory=memory, time=time, gpu=gpu
        )

    def has_deployment_spec(self) -> bool:
        return self.cpu is not None or self.memory is not None or self.gpu is not None

    def generate_deployment_spec(self, memory_coeff: float = 1.0) -> dict:
        spec: dict = {"resources": {"requests": {}, "limits": {}}}
        if self.cpu is not None:
            spec["resources"]["requests"]["cpu"] = self.cpu

        if self.memory is not None:
            spec["resources"]["requests"]["memory"] = self.memory
            spec["resources"]["limits"]["memory"] = self.memory * memory_coeff
        if self.gpu is not None:
            spec["resources"]["limits"]["nvidia.com/gpu"] = self.gpu
        return spec


@dataclass
class ActionIO:
    id: str
    name: str
    type: ActionIOType
    display_name: str
    help: str
    optional: bool = False
    enum_values: List[str] = field(default_factory=list)

    def to_python_type(self) -> Union[Type, enum.Enum]:
        my_type = self.type
        return_type: Union[Type, enum.Enum]
        if my_type in [
            ActionIOType.STRING,
            ActionIOType.PASSWORD,
            ActionIOType.LONGSTRING,
        ]:
            return_type = str
        elif my_type == ActionIOType.ENUM:
            return_type = enum.Enum(  # type: ignore
                self.display_name.capitalize(),
                {elem: elem for elem in self.enum_values},
                type=str,
            )
        elif my_type == ActionIOType.INTEGER:
            return_type = int
        elif my_type == ActionIOType.FLOAT:
            return_type = float
        elif my_type in [ActionIOType.FILE, ActionIOType.DIRECTORY]:
            return_type = bytes
        elif my_type == ActionIOType.BOOLEAN:
            return_type = bool
        else:
            raise UnsupportedIOType()
        if self.optional:
            return Optional[return_type]  # type: ignore
        return return_type


@dataclass
class ActionInput(ActionIO):
    addon_name: str | None = None

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_data(
        cls,
        name: str,
        type: ActionIOType,
        display_name: str,
        help: str,
        enum_values: list[str],
        optional: bool,
        addon_name: str | None = None,
    ) -> "ActionInput":
        return cls(
            id=cls.new_id(),
            name=name,
            type=type,
            display_name=display_name,
            help=help,
            enum_values=enum_values,
            optional=optional,
            addon_name=addon_name,
        )


class ActionOutput(ActionIO):
    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_data(
        cls,
        name: str,
        type: ActionIOType,
        display_name: str,
        help: str,
        enum_values: list[str],
        optional: bool,
    ) -> "ActionOutput":
        return ActionOutput(
            id=cls.new_id(),
            name=name,
            type=type,
            display_name=display_name,
            help=help,
            enum_values=enum_values,
            optional=optional,
        )


@dataclass
class ActionDynamicOutput(ActionOutput):
    origin: DynamicOutputOrigin = DynamicOutputOrigin.NONE

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def create_from_data_with_origin(
        cls,
        name: str,
        type: ActionIOType,
        display_name: str,
        help: str,
        origin: DynamicOutputOrigin,
        enum_values: list[str],
        optional: bool,
    ) -> "ActionDynamicOutput":
        return ActionDynamicOutput(
            id=cls.new_id(),
            name=name,
            type=type,
            display_name=display_name,
            help=help,
            origin=origin,
            enum_values=enum_values,
            optional=optional,
        )


@dataclass
class ActionDefinition:
    """
    An ActionDefinition records all metadata around the definition of an action.
    It is not instantiated (i.e. it does not have any values for its inputs).
    ActionDefinition can be deployed, the deployed object is a ActionDeployment.
    Multiple WorkflowActions can share the same ActionDefinition.
    """

    id: str
    project_id: str
    technical_name: str
    version: str
    kind: ActionKind
    container_image: str
    human_name: str
    description: str
    inputs: List[ActionInput]
    outputs: List[ActionOutput]
    addons_inputs: List[ActionInput] = field(default_factory=list)
    resources: Optional[ActionResources] = None
    has_dynamic_outputs: bool = False

    def get_input_type(self, input_name: str) -> ActionIO:
        for action_input in self.inputs + self.addons_inputs:
            if action_input.name == input_name:
                return action_input
        raise ActionDefinitionIONotFoundError(
            f"Could not find input matching name: '{input_name}' list of inputs: {self.inputs}"
        )

    def is_a_trigger(self) -> bool:
        return self.kind.is_a_trigger()

    def get_action_inputs(self) -> list[ActionInput]:
        return self.inputs + self.addons_inputs
