# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import List, Optional

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionInput,
    ActionOutput,
    ActionResources,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.common.domain.internal_messaging.base_messages import BaseCommand


@dataclass
class CreateActionDefinitionCommand(BaseCommand):
    id: str
    project_id: str
    technical_name: str
    human_name: str
    description: str
    version: str
    kind: ActionKind
    container_image: str
    inputs: List[ActionInput]
    outputs: List[ActionOutput]
    addons_inputs: List[ActionInput] = field(default_factory=list)
    resources: Optional[ActionResources] = None
    has_dynamic_outputs: bool = False


@dataclass
class DeleteActionDefinitionCommand(BaseCommand):
    project_id: str
    technical_name: str
    version: str
