# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from enum import Enum


class ActionKind(Enum):
    SOURCE = "SOURCE"  # DEPRECATED: Do not use anymore. Here for retro-compatibility.
    TRIGGER = "TRIGGER"
    PROCESSOR = "PROCESSOR"
    PUBLISHER = "PUBLISHER"

    def is_a_trigger(self) -> bool:
        return self in [ActionKind.TRIGGER, ActionKind.SOURCE]


class ActionIOType(Enum):
    STRING = "string"
    INTEGER = "integer"
    FLOAT = "float"
    BOOLEAN = "boolean"
    FILE = "file"
    DIRECTORY = "directory"
    LONGSTRING = "longstring"
    PASSWORD = "password"
    ENUM = "enum"
    BYTES = "bytes"

    def is_file_type(self) -> bool:
        return self in [
            ActionIOType.FILE,
            ActionIOType.DIRECTORY,
        ]


class DynamicOutputOrigin(Enum):
    NONE = "none"
    PATH = "path"
    HEADER = "header"
    QUERY = "query"
    COOKIE = "cookie"
    BODY = "body"
