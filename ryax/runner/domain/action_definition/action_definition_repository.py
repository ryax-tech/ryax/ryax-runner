# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from abc import ABC, abstractmethod

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)


class IActionDefinitionRepository(ABC):
    @abstractmethod
    def get(self, action_definition_id: str) -> ActionDefinition:
        ...

    @abstractmethod
    def get_by_technical_name_and_version(
        self, technical_name: str, version: str, project_id: str
    ) -> ActionDefinition:
        ...

    @abstractmethod
    def add(self, action_deployment: ActionDefinition) -> None:
        ...

    @abstractmethod
    def delete(self, action_definition: ActionDefinition) -> None:
        ...
