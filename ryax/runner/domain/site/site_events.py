from dataclasses import dataclass, field

from ryax.common.domain.internal_messaging.base_messages import BaseCommand
from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
    ObjectiveScores,
    GPU_MODE_FULL,
)


@dataclass
class RegisterSiteCommand(BaseCommand):
    @dataclass
    class NodePool:
        name: str
        cpu: float
        memory: int
        gpu: int = 0
        instance_type: str | None = None
        architecture: NodeArchitecture = NodeArchitecture.X86_64
        maximum_time_in_sec: float | None = None
        # A score for each objective
        objective_scores: ObjectiveScores | None = None
        # Dynamic availability for MIG auto-scaling
        gpu_mode: str = GPU_MODE_FULL
        gpu_num_total: int = 0
        filter_no_gpu_action: bool = True

    @dataclass
    class Site:
        id: str
        name: str
        type: SiteType
        node_pools: "list[RegisterSiteCommand.NodePool]" = field(default_factory=list)

    site: "RegisterSiteCommand.Site"
