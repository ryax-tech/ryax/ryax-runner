from dataclasses import dataclass, field
from typing import Tuple

from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
    ObjectiveScores,
    GPU_MODE_FULL,
)
from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionResources,
)
from ryax.runner.domain.site.site_events import RegisterSiteCommand


@dataclass
class NodePool:
    id: str
    name: str
    cpu: float
    memory: int
    #  Only None when received from the worker before creation
    site: "Site" = field(repr=False, hash=False, compare=False)
    gpu: int = 0
    instance_type: str | None = None
    architecture: NodeArchitecture = NodeArchitecture.X86_64
    maximum_time_in_sec: float | None = None
    # A score for each objective
    objective_scores: ObjectiveScores | None = None
    # Do not allocate actions without GPU resource request on this node pool
    filter_no_gpu_action: bool = True
    # Dynamic MIG GPU Scheduling
    gpu_mode: str = GPU_MODE_FULL  # Init as full mode
    gpu_num_total: int = 0  # The number of gpu (or MIG slices) inside the whole node pool (no matter occupied or not)
    gpu_free_total: int = 0  # Dynamic maintain the number of gpu that is not allocated by an execution connection. This value is updated before scheduler works.

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    def has_architecture(
        self, architecture_constraints: list[NodeArchitecture]
    ) -> bool:
        return self.architecture in architecture_constraints

    # Static check the node pool capacity
    def has_enough_resources(
        self, resource_request: ActionResources | None
    ) -> Tuple[bool, str]:
        """
        Check if the NodePool can fit the resource request
        :param resource_request:
        :return: True if it has enough resource or if the resource request is empty
        """
        if resource_request is None:
            return True, ""

        error_messages = []

        enough_cpu = resource_request.cpu is None or self.cpu >= resource_request.cpu
        if not enough_cpu:
            error_messages.append(
                f"Not enough CPU (requested: {resource_request.cpu} / allocatable: {self.cpu})"
            )

        enough_memory = (
            resource_request.memory is None or self.memory >= resource_request.memory
        )
        if not enough_memory:
            error_messages.append(
                f"Not enough Memory (requested: {resource_request.memory} / allocatable: {self.memory})"
            )

        # Judge enough GPU with node capacity (static)
        enough_gpu = False
        if resource_request.gpu is None:  # No GPU Request
            enough_gpu = True
        elif (
            self.gpu_mode == GPU_MODE_FULL
        ):  # Has GPU request, and the GPU on this site is in Full mode
            enough_gpu = self.gpu >= resource_request.gpu
        else:  # Has GPU request, and the GPU on this site is in any MIG mode
            enough_gpu = (
                resource_request.gpu <= 1
            )  # Only support the request of 1 GPU to enable MIG VPA
        if not enough_gpu:
            error_messages.append(
                f"Not enough GPU (requested: {resource_request.gpu} / allocatable: {self.gpu})"
            )

        enough_time = resource_request.time is None or (
            self.maximum_time_in_sec is None
            or self.maximum_time_in_sec >= resource_request.time
        )
        if not enough_time:
            error_messages.append(
                f"Not enough Time (requested: {resource_request.time} / allocatable: {self.maximum_time_in_sec})"
            )
        return (enough_cpu and enough_memory and enough_gpu and enough_time), ", ".join(
            error_messages
        )

    @classmethod
    def from_command(cls, node_pool: RegisterSiteCommand.NodePool) -> "NodePool":
        return cls(
            cpu=node_pool.cpu,
            gpu=node_pool.gpu,
            memory=node_pool.memory,
            architecture=node_pool.architecture,
            objective_scores=node_pool.objective_scores,
            gpu_mode=node_pool.gpu_mode,
            gpu_num_total=node_pool.gpu_num_total,
            name=node_pool.name,
            instance_type=node_pool.instance_type,
            id=cls.new_id(),
            site=None,  # type: ignore
            filter_no_gpu_action=node_pool.filter_no_gpu_action,
        )


@dataclass
class Site:
    id: str
    name: str
    type: SiteType
    node_pools: list[NodePool] = field(default_factory=list)

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    @classmethod
    def from_command(cls, site: RegisterSiteCommand.Site) -> "Site":
        return cls(
            site.id,
            site.name,
            site.type,
            node_pools=[
                NodePool.from_command(nodepool) for nodepool in site.node_pools
            ],
        )
