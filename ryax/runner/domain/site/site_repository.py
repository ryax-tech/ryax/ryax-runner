import abc

from ryax.runner.domain.site.site_entities import Site, NodePool


class ISiteRepository(abc.ABC):
    @abc.abstractmethod
    def add(self, site: Site) -> None:
        ...

    @abc.abstractmethod
    def get(self, site_id: str) -> Site:
        ...

    @abc.abstractmethod
    def list(self) -> list[Site]:
        ...

    @abc.abstractmethod
    def get_node_pool(self, node_pool_id: str) -> NodePool:
        ...
