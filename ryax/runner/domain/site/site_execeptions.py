class SiteNotFoundError(Exception):
    pass


class NodePoolNotFoundError(Exception):
    pass


class DuplicatedSiteNameError(Exception):
    pass


class UnknownArchitectureError(Exception):
    pass
