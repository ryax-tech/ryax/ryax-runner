# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from typing import Dict

import kubernetes_asyncio as k8s
from opentelemetry.instrumentation.aiohttp_client import AioHttpClientInstrumentor

from ryax.common.domain.internal_messaging.message_bus import IMessageBus

logger = getLogger(__name__)

# Enable instrumentation
AioHttpClientInstrumentor().instrument()

K8sApiException = k8s.client.ApiException


class K8sEngine:
    def __init__(
        self,
        user_namespace: str,
        message_bus: IMessageBus,
    ) -> None:
        self.user_namespace = user_namespace
        self.message_bus: IMessageBus = message_bus

    async def init(self) -> None:
        try:
            await k8s.config.load_kube_config()
            logger.info("Kubernetes config loaded from KUBECONFIG")
        except k8s.config.config_exception.ConfigException as err:
            logger.debug(
                "Unable to load Kubernetes configuration from KUBECONFIG: %s",
                err,
            )
            k8s.config.load_incluster_config()
            logger.info("Kubernetes config loaded from In-cluster config")

    async def create_user_namespace(self) -> None:
        try:
            async with k8s.client.ApiClient() as api:
                core_api = k8s.client.CoreV1Api(api)
                await core_api.create_namespace(
                    body={
                        "apiVersion": "v1",
                        "kind": "Namespace",
                        "metadata": {"name": self.user_namespace},
                    }
                )
        except k8s.client.ApiException as err:
            if err.status != 409:
                raise

    async def apply_ingress_definition(self, k8s_definition: Dict) -> None:
        async with k8s.client.ApiClient() as api:
            net_api = k8s.client.NetworkingV1Api(api)
            await net_api.create_namespaced_ingress(
                namespace=self.user_namespace, body=k8s_definition
            )

    async def apply_middleware_definition(self, k8s_middleware_def: Dict) -> None:
        async with k8s.client.ApiClient() as api:
            crd_api = k8s.client.CustomObjectsApi(api)
            await crd_api.create_namespaced_custom_object(
                group="traefik.containo.us",
                version="v1alpha1",
                namespace=self.user_namespace,
                plural="middlewares",
                body=k8s_middleware_def,
            )

    async def apply_service_definition(self, k8s_definition: Dict) -> None:
        async with k8s.client.ApiClient() as api:
            net_api = k8s.client.CoreV1Api(api)
            await net_api.create_namespaced_service(
                namespace=self.user_namespace, body=k8s_definition
            )

    async def delete_service(self, deployment_id: str) -> None:
        async with k8s.client.ApiClient() as api:
            core_api = k8s.client.CoreV1Api(api)
            try:
                await core_api.delete_namespaced_service(
                    namespace=self.user_namespace, name=deployment_id
                )
            except k8s.client.ApiException as err:
                logger.warning(
                    f"Unable to delete the service {deployment_id} due to {err}. Ignoring..."
                )

    async def delete_ingress(self, deployment_id: str) -> None:
        async with k8s.client.ApiClient() as api:
            net_api = k8s.client.NetworkingV1Api(api)
            try:
                await net_api.delete_namespaced_ingress(
                    namespace=self.user_namespace, name=deployment_id
                )
            except k8s.client.ApiException as err:
                logger.debug(
                    f"Unable to delete the ingress {deployment_id} due to {err}. Ignoring..."
                )

    async def delete_middleware(self, deployment_id: str) -> None:
        async with k8s.client.ApiClient() as api:
            crd_api = k8s.client.CustomObjectsApi(api)
            try:
                await crd_api.delete_namespaced_custom_object(
                    namespace=self.user_namespace,
                    name=deployment_id,
                    group="traefik.containo.us",
                    version="v1alpha1",
                    plural="middlewares",
                )
            except k8s.client.ApiException as err:
                logger.debug(
                    f"Unable to delete the middleware {deployment_id} due to {err}. Ignoring..."
                )
