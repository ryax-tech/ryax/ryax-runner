# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from dataclasses import dataclass
from logging import getLogger

from ryax.runner.application.action_logs_service import ActionLogsService
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    DeployActionDeploymentCommand,
    GetActionDeploymentStateCommand,
    DeleteActionDeploymentCommand,
)
from ryax.common.domain.internal_messaging.event_publisher import IPublisherService
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_service import (
    IActionDeploymentService,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.runner.domain.exceptions import ActionDeploymentError, ActionUndeploymentError
from ryax.common.domain.internal_messaging.querier import IQuerierService
from ryax.runner.infrastructure.action_deployment.k8s.k8s_engine import K8sEngine

logger = getLogger(__name__)


class UnsupportedActionKindDeploymentError(Exception):
    pass


@dataclass
class K8SActionDeploymentService(IActionDeploymentService):
    engine: K8sEngine
    ryax_registry: str
    publisher: IPublisherService
    querier: IQuerierService
    action_logs_service: ActionLogsService
    user_action_log_level: str = "Info"

    async def apply(
        self,
        action_deployment: ActionDeployment,
        addons_extra_params: dict,
    ) -> None:
        """Apply the action deployment in Kubernetes"""
        logger.debug(
            "Deploying action '%s:%s'. Deployment id: %s",
            action_deployment.action_definition.technical_name,
            action_deployment.action_definition.version,
            action_deployment.id,
        )

        try:
            numeric_log_level: int | None = getattr(
                logging, self.user_action_log_level.upper(), None
            )
            assert numeric_log_level is not None, "Log level must not be None"
            await self.publisher.publish(
                [
                    DeployActionDeploymentCommand(
                        action_deployment_id=action_deployment.id,
                        name=action_deployment.action_definition.technical_name,
                        log_level=numeric_log_level,
                        version=action_deployment.action_definition.version,
                        execution_type=action_deployment.execution_type,
                        deployment_type=action_deployment.get_deployment_type(),
                        kind=action_deployment.action_definition.kind,
                        resources=action_deployment.action_definition.resources,
                        container_image=f"{self.ryax_registry}/{action_deployment.action_definition.id}:{action_deployment.action_definition.version}",
                        parameters=action_deployment.addons | addons_extra_params,
                        node_pool_name=action_deployment.node_pool.name,
                    )
                ],
                site_name=action_deployment.node_pool.site.name,
            )
        except Exception as err:
            raise ActionDeploymentError(str(err)) from err

    async def get_current_state(
        self, action_deployment: ActionDeployment
    ) -> ActionDeploymentState:
        """Initialize deployment state"""
        try:
            return await self.querier.query(
                GetActionDeploymentStateCommand(
                    action_deployment_id=action_deployment.id
                ),
                site_name=action_deployment.node_pool.site.name,
            )
        except Exception as err:
            raise ActionDeploymentError(str(err)) from err

    async def watch(self, action_deployment: ActionDeployment) -> None:
        self.action_logs_service.watch_action_deployment(action_deployment.id)

    async def unwatch(self, action_deployment: ActionDeployment) -> None:
        self.action_logs_service.unwatch_action_deployment(action_deployment.id)

    async def delete(self, action_deployment: ActionDeployment) -> None:
        try:
            await self.publisher.publish(
                [
                    DeleteActionDeploymentCommand(
                        action_deployment_id=action_deployment.id
                    )
                ],
                site_name=action_deployment.node_pool.site.name,
            )
        except Exception as err:
            raise ActionUndeploymentError(str(err)) from err
