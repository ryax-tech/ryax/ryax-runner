# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from ryax.runner.domain.action_deployment.action_deployment_commands import (
    UpdateActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_service import (
    IActionDeploymentService,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus


@dataclass
class DummyActionDeploymentService(IActionDeploymentService):
    message_bus: IMessageBus

    async def apply(
        self,
        action: ActionDeployment,
        addons_extra_params: dict,
        site_name: str | None = None,
    ) -> None:
        pass

    async def get_current_state(
        self, action_deployment: ActionDeployment
    ) -> ActionDeploymentState:
        if action_deployment.state in [
            ActionDeploymentState.DEPLOYING,
            ActionDeploymentState.READY,
            ActionDeploymentState.RUNNING,
        ]:
            return ActionDeploymentState.READY
        else:
            return ActionDeploymentState.UNDEPLOYED

    async def watch(self, action: ActionDeployment) -> None:
        """
        Watch only returns one event updating the state of the action and stop.
        """
        await self.message_bus.handle(
            [UpdateActionDeploymentCommand(action.id, ActionDeploymentState.READY)]
        )

    async def unwatch(self, action_deployment: ActionDeployment) -> None:
        pass

    async def delete(self, action_deployment: ActionDeployment) -> None:
        action_deployment.set_undeployed()
