# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.runner.domain.action_definition.action_definition_values import ActionIOType


class ModuleIOSchema(Schema):
    id = fields.String(
        metadata={
            "description": "ID of the workflow_deployment action input",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        required=True,
    )

    type = fields.String(
        metadata={
            "description": f"Action input type: {list(item.value.upper() for item in ActionIOType)}",
            "example": "string",
            "enum": list(item.value.upper() for item in ActionIOType),
        },
        validate=lambda x: x.upper() in [item.name for item in ActionIOType],
        required=True,
    )

    display_name = fields.String(
        metadata={
            "description": "This string is used to display the IO to the user",
            "example": "My IO",
        },
        required=True,
    )

    help = fields.String(
        metadata={
            "description": "This string helps the user understand the IO",
            "example": "This IO does this and that.",
        },
        dump_default="",
    )

    enum_values = fields.List(
        fields.String(),
        metadata={
            "description": "The list of values that this IO can take if it is an enum.",
            "example": ["a first value", "a second value"],
            "many": True,
        },
        dump_default=[],
    )
    value = fields.String(
        metadata={
            "description": "Action outputs value",
            "example": ["foobarr"],
        },
        allow_none=True,
    )
    optionals = fields.Bool()
    headers = fields.Dict(
        metadata={
            "description": "Headers to use for the file download",
            "example": {"Content-Type": "application/gzip", "Content-Size": 123},
        },
        dump_default={},
    )
