from marshmallow import Schema, fields

from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
)


class ObjectiveScoresSchema(Schema):
    energy = fields.Integer()
    performance = fields.Integer()
    cost = fields.Integer()


class NodePoolSchema(Schema):
    id = fields.String()
    name = fields.String()
    cpu = fields.Float()
    memory = fields.Integer()
    gpu = fields.Integer()
    architecture = fields.Enum(NodeArchitecture)
    maximum_time_in_sec = fields.Float(allow_none=True)
    # A score for each objective
    objective_scores = fields.Nested(ObjectiveScoresSchema)
    gpu_mode = fields.String()
    gpu_num_total = fields.Integer()
    gpu_free_total = fields.Integer()


class SiteSchema(Schema):
    id = fields.String()
    name = fields.String(
        metadata={
            "description": "Site name",
            "example": "local",
        },
    )
    type = fields.Enum(SiteType)
    node_pools = fields.List(fields.Nested(NodePoolSchema))


class SiteListSchema(Schema):
    sites = fields.List(fields.Nested(SiteSchema))
