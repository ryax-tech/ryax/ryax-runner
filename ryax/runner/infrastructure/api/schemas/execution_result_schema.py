# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate

from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import ActionRunState
from ryax.runner.infrastructure.api.schemas.module_definition_schema import ActionSchema


class ModuleRunIOSchema(Schema):
    class Meta:
        ordered = True

    name = fields.String(metadata={"description": "IO name", "example": "my-string1"})
    value = fields.String(
        metadata={"description": "IO value", "example": "This is the string value"}
    )
    type = fields.String(
        metadata={
            "description": f"IO type: {list(item.value for item in ActionIOType)}",
            "example": "string",
            "enum": list(item.value for item in ActionIOType),
        },
        attribute="type.value",
        validate=validate.OneOf(list(item.value for item in ActionIOType)),
    )


class ExecutionResultSchema(Schema):
    id = fields.String(
        metadata={
            "description": "execution result id",
            "example": "ExecutionResult-123458421-axfd",
        },
        allow_none=True,
    )
    submitted_at_date = fields.DateTime(
        metadata={
            "description": "execution submission datetime",
            "example": "2021-11-09T09:49:26.871330",
        },
        allow_none=True,
    )
    started_at_date = fields.DateTime(
        metadata={
            "description": "execution submission datetime",
            "example": "2021-11-09T09:49:26.871330",
        },
        allow_none=True,
    )
    ended_at_date = fields.DateTime(
        metadata={
            "description": "execution submission datetime",
            "example": "2021-11-09T09:49:26.871330",
        },
        allow_none=True,
    )
    error_message = fields.String(
        metadata={"description": "Error messages about why the action failed"},
        allow_none=True,
    )
    state = fields.Enum(ExecutionResultState, by_value=True, allow_none=True)


class ActionRunSchema(Schema):
    class Meta:
        ordered = True

    id = fields.String(
        metadata={
            "description": "ID of the action execution",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        allow_none=True,
    )
    state = fields.String(
        metadata={
            "description": f"Action Execution state: {list(item.value for item in ActionRunState)}",
            "example": "COMPLETED",
            "enum": list(item.value for item in ActionRunState),
        },
        validate=validate.OneOf(list(item.value for item in ActionRunState)),
    )
    error_message = fields.String()
    action = fields.Nested(ActionSchema)
    results = fields.Nested(ExecutionResultSchema, many=True)


class DeletedCountExecutionSchema(Schema):
    deleted_execution_count = fields.Integer(
        metadata={
            "description": "Number of deleted executions",
            "example": 42,
        },
    )
