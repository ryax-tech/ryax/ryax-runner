# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.runner.infrastructure.api.schemas.module_io_schema import ModuleIOSchema


class PortalSchema(Schema):
    class Meta:
        ordered = True

    workflow_deployment_id = fields.String(
        metadata={
            "description": "ID of the workflow",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        required=True,
    )
    workflow_definition_id = fields.String(
        metadata={
            "description": "ID of the workflow",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        required=True,
    )
    name = fields.String(
        metadata={
            "description": "Name of the workflow",
            "example": "Get all data and send it to me",
        },
        required=True,
    )
    outputs = fields.Nested(
        ModuleIOSchema,
        metadata={
            "description": "Action outputs",
        },
        many=True,
    )
