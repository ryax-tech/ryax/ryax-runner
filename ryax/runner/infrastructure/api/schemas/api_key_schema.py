# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class AddUserApiKeySchema(Schema):
    name = fields.String(
        metadata={
            "description": "Human name of user api key.",
            "example": "Api key for Andry Razafinjatovo",
        },
        required=True,
    )
    expiration_date = fields.DateTime(
        metadata={
            "description": "Expiration date of the api key",
            "example": "2022-12-20 15:52:11.757539+00:00",
        },
        required=False,
        allow_none=True,
    )


class DecryptedUserApiKeySchema(Schema):
    id = fields.String(required=True)

    name = fields.String(
        metadata={
            "description": "Human name of user api key.",
            "example": "Api key for Andry Razafinjatovo",
        },
        required=True,
    )

    api_key = fields.String(
        metadata={
            "description": "The api key",
            "example": "213jh098dvca980",
        },
        required=True,
    )
    expiration_date = fields.DateTime(
        metadata={
            "description": "Expiration date of the api key",
            "example": "2022-12-20 15:52:11.757539+00:00",
        },
        required=True,
    )
