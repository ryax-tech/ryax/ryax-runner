# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.infrastructure.api.schemas.module_io_schema import ModuleIOSchema


class ActionSchema(Schema):
    id = fields.String(required=False)

    technical_name = fields.String(
        metadata={"description": "Technical name of the action", "example": "mqttgw"},
        required=True,
    )

    name = fields.String(
        metadata={
            "description": "Human name of the action.",
            "example": "The cool mqttgw",
        },
        required=False,
    )

    description = fields.String(
        metadata={
            "description": "Description of the action.",
            "example": "This action adds two numbers",
        },
        required=False,
    )

    version = fields.String(
        metadata={
            "description": "action version",
            "example": "1.0",
        },
        required=True,
    )
    kind = fields.String(
        metadata={
            "description": f"Action kind: {list(item.value.capitalize() for item in ActionKind)}",
            "example": "Source",
            "enum": list(item.value.capitalize() for item in ActionKind),
        },
        validate=lambda x: x.upper() in [item.value for item in ActionKind],
        required=True,
    )

    container_image = fields.String(
        metadata={
            "description": "container image full name",
            "example": "registry.ryax.org/action-dev/return42:2",
        },
        required=True,
    )

    inputs = fields.Nested(
        ModuleIOSchema,
        metadata={
            "description": "Actions inputs",
        },
        many=True,
    )

    outputs = fields.Nested(
        ModuleIOSchema,
        metadata={
            "description": "Action outputs",
        },
        many=True,
    )


class DeleteModuleSchema(Schema):
    technical_name = fields.String(
        metadata={"description": "Technical name of the action", "example": "mqttgw"},
        required=True,
    )

    version = fields.String(
        metadata={
            "description": "Action version",
            "example": "1.0",
        },
        required=True,
    )
