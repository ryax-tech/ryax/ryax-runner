# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class AccountingSchema(Schema):
    class Meta:
        ordered = True

    project_id = fields.String(
        metadata={
            "description": "ID of the workflow",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        required=True,
    )
    workflow_definition_id = fields.String(
        metadata={
            "description": "ID of the workflow",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        required=True,
    )
    action_definition_id = fields.String(
        metadata={
            "description": "ID of the workflow",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        required=True,
    )
    workflow_run_id = fields.String(
        metadata={
            "description": "ID of the workflow",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        required=True,
    )
    execution_connection_id = fields.String(
        metadata={
            "description": "ID of the workflow",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
        required=True,
    )
    submitted_at = fields.DateTime(
        metadata={
            "description": "execution submission datetime",
            "example": "2021-11-09T09:49:26.871330",
        },
        allow_none=True,
    )
    started_at = fields.DateTime(
        metadata={
            "description": "execution submission datetime",
            "example": "2021-11-09T09:49:26.871330",
        },
        allow_none=True,
    )
    ended_at = fields.DateTime(
        metadata={
            "description": "execution submission datetime",
            "example": "2021-11-09T09:49:26.871330",
        },
        allow_none=True,
    )
    requested_cpu = fields.Float()
    requested_memory = fields.Integer()
    requested_time = fields.Float(
        metadata={
            "description": "Maximum time allowed",
        },
        allow_none=True,
    )
    requested_gpu = fields.Integer()
    instance_type = fields.String(
        metadata={
            "description": "Node pool instance type",
            "example": "c6g.16xlarge",
        },
        required=False,
    )
