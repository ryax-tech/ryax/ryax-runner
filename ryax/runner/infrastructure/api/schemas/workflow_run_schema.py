# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate

from ryax.runner.domain.workflow_run.workflow_run_entities import WorkflowRunState
from ryax.runner.infrastructure.api.schemas.execution_result_schema import (
    ActionRunSchema,
)


class WorkflowRunSchemaMinimal(Schema):
    class Meta:
        ordered = True

    id = fields.String(metadata={"description": "ID of the workflow run."})
    state = fields.String(
        metadata={
            "description": f"Workflow run state: {list(item.value for item in WorkflowRunState)}",
            "example": "ERROR",
            "enum": list(item.value for item in WorkflowRunState),
        },
        validate=validate.OneOf(list(item.value for item in WorkflowRunState)),
    )

    started_at = fields.DateTime(
        metadata={
            "description": "Workflow run start datetime",
            "example": "2021-11-09T09:49:26.871330",
        },
        allow_none=True,
    )

    last_result_at = fields.DateTime(
        metadata={
            "description": "Last recorded action execution ended at this time.",
            "example": "2021-11-09T09:49:26.871330",
        },
        allow_none=True,
    )

    total_steps = fields.Integer(
        metadata={
            "description": "Total number of steps.",
            "example": 42,
        },
    )

    completed_steps = fields.Integer(
        metadata={
            "description": "Number of completed steps.",
            "example": 7,
        },
    )

    workflow_definition_id = fields.String(
        metadata={
            "description": "ID of the workflow this run comes from",
            "example": "2ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )


class WorkflowRunSchema(WorkflowRunSchemaMinimal):
    runs = fields.Nested(
        ActionRunSchema,
        many=True,
        metadata={
            "description": "The workflow action executions",
        },
    )
