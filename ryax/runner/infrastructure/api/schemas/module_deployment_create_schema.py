# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields
from marshmallow.validate import OneOf

from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionExecutionType,
)


class ModuleDeploymentCreateSchema(Schema):
    class Meta:
        ordered = True

    project_id = fields.String()

    technical_name = fields.String(
        metadata={"description": "Technical name of the action", "example": "mqttgw"},
        required=True,
    )

    version = fields.String(
        metadata={
            "description": "Action version",
            "example": "1.0",
        },
        required=True,
    )

    execution_type = fields.String(
        metadata={
            "description": "The execution type of this action",
            "example": "GRPC_V1",
        },
        validate=OneOf([item.value for item in ActionExecutionType]),
        load_default="GRPC_V1",
    )
