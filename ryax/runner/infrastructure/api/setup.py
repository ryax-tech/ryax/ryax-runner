# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware

from ryax.runner.container import ApplicationContainer
from ryax.runner.infrastructure.api.controllers import (
    file_storage_controller,
    monitoring_controller,
    portal_controller,
    workflow_run_controller,
    site_controller,
    accounting_controller,
)
from ryax.runner.infrastructure.api.controllers.authorization import (
    api_key_controller,
    user_auth_controller,
)
from ryax.runner.infrastructure.api.middlewares import (
    authentication_middleware,
    current_project_id_middleware,
)
from ryax.runner.version import __version__


def setup(app: web.Application, container: ApplicationContainer) -> None:
    """Method to setup api"""
    # Configure application container for wiring
    container.wire(
        modules=[
            portal_controller,
            current_project_id_middleware,
            authentication_middleware,
            monitoring_controller,
            file_storage_controller,
            workflow_run_controller,
            user_auth_controller,
            api_key_controller,
            site_controller,
            accounting_controller,
        ]
    )

    # Setup server middlewares
    app.middlewares.extend(
        [
            authentication_middleware.check_token(
                public_paths=["/docs", "/static", "/healthz", "/results", "/ws"]
            ),
            current_project_id_middleware.handle(
                public_paths=["/docs", "/static", "/healthz", "/results", "/ws"]
            ),
            validation_middleware,
        ]
    )

    workflows_websocket_controller = container.workflow_websocket_controller()
    runs_websocket_controller = container.runs_websocket_controller()

    # Configure api routing
    app.add_routes(
        [
            web.get("/healthz", monitoring_controller.health_check, allow_head=False),
            web.get(
                "/portals/{workflow_definition_id}",
                portal_controller.get_portal,
                allow_head=False,
            ),
            web.post(
                "/portals/{workflow_definition_id}",
                portal_controller.trigger_workflow,
            ),
            web.get(
                "/workflow_runs",
                workflow_run_controller.list_workflow_runs,
                allow_head=False,
            ),
            web.get(
                "/workflow_runs/{workflow_run_id}",
                workflow_run_controller.get_workflow_run,
                allow_head=False,
            ),
            web.post(
                "/workflow_runs/{workflow_run_id}/cancel",
                workflow_run_controller.cancel_workflow_run,
            ),
            web.get(
                "/workflow_runs/{execution_result_id}/logs",
                workflow_run_controller.get_execution_result_logs,
                allow_head=False,
            ),
            web.delete(
                "/workflow_runs/{workflow_run_id}",
                workflow_run_controller.delete_workflow_run,
            ),
            web.get("/sites", site_controller.list),
            web.get(
                "/filestore/{stored_file_id}/{filename}",
                file_storage_controller.download_file,
            ),
            web.get(
                "/results/{workflow_run_id}",
                workflow_run_controller.get_workflow_results_with_user_api_auth,
            ),
            web.get(
                "/run-results/{workflow_run_id}",
                workflow_run_controller.get_workflow_results,
            ),
            web.post("/user-auth/api-key", api_key_controller.add_user_api_key),
            web.delete(
                "/user-auth/api-key/{user_api_key_id}",
                api_key_controller.delete_user_api_key,
            ),
            web.post(
                "/user-auth/api-key/{user_api_key_id}/revoke",
                api_key_controller.revoke_user_api_key,
            ),
            web.put(
                "/user-auth/api-key/toggle", api_key_controller.toggle_api_key_auth
            ),
            web.get("/user-auth", user_auth_controller.list_project_user_authorization),
            # WebSocket endpoints (prefixed by ws)
            web.get(
                "/ws/workflows/{workflow_definition_id}",
                workflows_websocket_controller.stream,
            ),
            web.get(
                "/ws/runs/{execution_result_id}",
                runs_websocket_controller.stream_logs,
            ),
            web.get(
                "/accounting",
                accounting_controller.list_accounting,
            ),
        ]
    )

    # Configure api documentation
    prefix = container.configuration.api.base_url()
    if prefix is None:
        prefix = ""
    setup_aiohttp_apispec(
        app,
        title="Ryax Runner API",
        version=__version__,
        url="/docs/swagger.json",
        swagger_path="/docs",
        static_path="/static/swagger",
        prefix=prefix,
        securityDefinitions={
            "bearer": {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header",
                "description": "Ryax token",
            }
        },
        security=[{"bearer": []}],
    )
