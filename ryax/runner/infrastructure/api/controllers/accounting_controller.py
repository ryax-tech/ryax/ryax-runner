from dependency_injector.wiring import Provide
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs

from ryax.runner.application.accounting_service import AccountingService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.accounting.accounting_exceptions import AccountingNotFoundError
from ryax.runner.infrastructure.api.schemas.accouting_schema import AccountingSchema
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema


@docs(
    tags=["Accounting"],
    summary="Get accounting",
    description="Get resources accounting details for defined executions, list separated by coma.",
    parameters=[
        {"in": "query", "name": "execution_ids", "type": "string"},
        {"in": "query", "name": "project_ids", "type": "string"},
        {"in": "query", "name": "action_ids", "type": "string"},
        {"in": "query", "name": "workflow_ids", "type": "string"},
        {"in": "query", "name": "run_ids", "type": "string"},
    ],
    responses={
        200: {
            "description": "Accounting fetched successfully",
            "schema": AccountingSchema(many=True),
        },
        404: {
            "description": "No execution found",
            "schema": ErrorSchema(),
        },
    },
)
async def list_accounting(
    request: Request,
    service: AccountingService = Provide[ApplicationContainer.accounting_service],
) -> Response:
    execution_ids = (
        request.query["execution_ids"].split(",")
        if request.query.get("execution_ids") is not None
        else []
    )
    project_ids = (
        request.query["project_ids"].split(",")
        if request.query.get("project_ids") is not None
        else []
    )
    workflow_ids = (
        request.query["workflow_ids"].split(",")
        if request.query.get("workflow_ids") is not None
        else []
    )
    run_ids = (
        request.query["run_ids"].split(",")
        if request.query.get("run_ids") is not None
        else []
    )
    action_ids = (
        request.query["action_ids"].split(",")
        if request.query.get("action_ids") is not None
        else []
    )
    try:
        accounting = service.list_accounting(
            execution_ids=execution_ids,
            action_ids=action_ids,
            workflow_ids=workflow_ids,
            project_ids=project_ids,
            run_ids=run_ids,
        )
    except AccountingNotFoundError:
        return json_response(
            ErrorSchema().dump({"error": "Execution not found"}), status=404
        )
    result = AccountingSchema(many=True).dump(accounting)
    return json_response(result, status=200)
