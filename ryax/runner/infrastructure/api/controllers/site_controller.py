from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs
from dependency_injector.wiring import Provide

from ryax.runner.application.site_service import SiteService
from ryax.runner.container import ApplicationContainer
from ryax.runner.infrastructure.api.schemas.site_schema import (
    SiteSchema,
    SiteListSchema,
)


@docs(
    tags=["Sites"],
    summary="List of available Sites",
    description="Returns the list of all Sites available on this Ryax instance to deploy Actions",
    responses={
        200: {
            "schema": SiteSchema(),
        },
    },
)
async def list(
    request: Request,
    sites_service: SiteService = Provide[ApplicationContainer.site_service],
) -> Response:
    sites = sites_service.list()

    result = SiteListSchema().dump({"sites": sites})
    return json_response(result, status=200)
