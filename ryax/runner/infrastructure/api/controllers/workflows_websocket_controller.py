import asyncio
import json
from asyncio import Task
from dataclasses import dataclass, field
from logging import getLogger
from typing import cast, Callable

import aiohttp
from aiohttp.web_ws import WebSocketResponse
from aiohttp_apispec import docs
from aiohttp.web_request import Request

from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentNewLogLinesAddedEvent,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    ActionDeploymentNotFoundError,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.action_logs.action_logs_entities import LogsNotFoundError
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema

logger = getLogger(__name__)


@dataclass
class WorkflowWebSocketController:
    uow_factory: Callable[..., IUnitOfWork]
    message_bus: IMessageBus
    open_websockets: dict[int, Task] = field(default_factory=dict)

    @docs(
        tags=["Logs", "Trigger", "Service"],
        summary="Get Service or Trigger deployment log in stream",
        description="Get the logs for an  with its ID.",
        responses={
            101: {
                "description": "Create a WebSocket",
            },
            404: {
                "description": "Deployment not found",
                "schema": ErrorSchema(),
            },
        },
    )
    async def stream(
        self,
        request: Request,
    ) -> WebSocketResponse:
        workflow_definition_id: str = request.match_info["workflow_definition_id"]

        ws = WebSocketResponse()
        await ws.prepare(request)

        with self.uow_factory() as uow:
            try:
                action_deployment = (
                    uow.action_deployment_repository.get_by_workflow_definition_id(
                        workflow_definition_id
                    )
                )
                action_deployment_id = action_deployment.id
                last_logs = uow.action_logs_repository.get_logs_as_string(
                    action_deployment_id
                )
            except (ActionDeploymentNotFoundError, LogsNotFoundError):
                result = ErrorSchema().dump({"error": "not found"})
                await ws.close(message=result)
                return ws
            # FIXME: Get the project from a token
            # if action_deployment.project_id != current_project:
            #    result = ErrorSchema().dump({"error": "not found"})
            #    await ws.close(message=result)
            #    return ws

        logger.debug(f"Starting websocket for ActionDeployment: {action_deployment_id}")
        async for msg in ws:
            logger.debug(f"Websocket message received: {msg}")
            if msg.type == aiohttp.WSMsgType.TEXT:
                try:
                    message = json.loads(msg.data)
                except Exception as err:
                    await ws.send_str(
                        json.dumps({"error": f"Error while parsing message: {err}"})
                    )
                    continue

                if msg.data == "close":
                    await ws.close()
                else:
                    try:
                        if "logs" in message:
                            await ws.send_str(json.dumps({"logs": last_logs}))
                            await self._register_client(action_deployment_id, ws)
                        else:
                            await ws.send_str(
                                json.dumps({"error": f"Error in query: {message}"})
                            )
                    except Exception as err:
                        await ws.send_str(
                            json.dumps(
                                {"error": f"Error while handling request: {err}"}
                            )
                        )
            elif msg.type == aiohttp.WSMsgType.ERROR:
                logger.warning(
                    "ws connection closed with exception %s" % ws.exception()
                )
        await self._unregister_client(ws)
        logger.debug(f"Websocket connection {id(ws)} closed")
        return ws

    async def _register_client(
        self, action_deployment_id: str, websocket: WebSocketResponse
    ) -> None:
        async def push_logs() -> None:
            waiter = None
            try:
                while True:
                    waiter = self.message_bus.wait_for_event(
                        ActionDeploymentNewLogLinesAddedEvent,
                        {"action_deployment_id": action_deployment_id},
                    )
                    new_logs_event = await asyncio.wait_for(waiter, timeout=None)
                    new_logs_event = cast(
                        ActionDeploymentNewLogLinesAddedEvent, new_logs_event
                    )
                    if new_logs_event.log_lines:
                        await websocket.send_str(
                            json.dumps({"logs": new_logs_event.log_lines})
                        )
            except asyncio.CancelledError:
                if waiter is not None:
                    waiter.cancel()

        self.open_websockets[id(websocket)] = asyncio.create_task(push_logs())

    async def _unregister_client(self, websocket: WebSocketResponse) -> None:
        callback = self.open_websockets.get(id(websocket))
        if callback is not None:
            callback.cancel()
