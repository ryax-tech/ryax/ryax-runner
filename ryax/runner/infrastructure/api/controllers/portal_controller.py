# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import html
import json
import mimetypes
import os
import tempfile
from logging import getLogger
from typing import IO, Dict, Optional, cast

import aiohttp
from aiohttp import MultipartReader
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs
from dependency_injector.wiring import Provide
from marshmallow import Schema, fields

from ryax.runner.application.views.portal_view import PortalView
from ryax.runner.application.views.workflow_deployment_view import WorkflowView
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import ActionOutput
from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.runner.domain.exceptions import WorkflowNotFoundError
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    InitializeExecutionResultCommand,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultFinishedEvent,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.storage.storage_service import IStorageService
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.runner.infrastructure.api.schemas.portal_schema import PortalSchema

logger = getLogger(__name__)

MAX_UPLOAD_SIZE = 1_073_741_824  # 1G


@docs(
    tags=["Workflows", "Portals"],
    summary="Get a portal",
    description="Get a portal from a workflow definition id (the id used in the studio).",
    responses={
        200: {
            "description": "Portal fetched successfully",
            "schema": PortalSchema(),
        },
        404: {
            "description": "Workflow not found",
            "schema": ErrorSchema(),
        },
    },
)
async def get_portal(
    request: Request,
    view: PortalView = Provide[ApplicationContainer.portal_view],
) -> Response:
    workflow_definition_id = request.match_info["workflow_definition_id"]
    try:
        portal = view.get_portal(
            workflow_definition_id,
        )
    except WorkflowNotFoundError:
        return json_response(
            ErrorSchema().dump({"error": "Workflow not found"}), status=404
        )
    result = PortalSchema().dump(portal)
    return json_response(result, status=200)


async def copy_uploaded_file(
    to_read: aiohttp.BodyPartReader,
    to_write: IO[bytes],
    max_upload_size: int = MAX_UPLOAD_SIZE,
) -> int:
    total_size = 0
    while True:
        chunk = await to_read.read_chunk()
        if not chunk:
            break
        to_write.write(chunk)
        total_size += len(chunk)
        if total_size > max_upload_size:
            raise Exception(
                f"The uploaded file is too big: {total_size}. Maximum upload size is {max_upload_size}"
            )
    to_write.seek(0)
    return total_size


class WorkflowRunTriggerResponseSchema(Schema):
    workflow_run_id = fields.String(
        metadata={
            "description": "Workflow run id",
            "summary": "ID of the created workflow run. If you request very fast, the system might respond not found,"
            "wait a bit.",
            "example": "wfrid-123-abcd",
        },
    )


@docs(
    tags=["Executions", "Portals"],
    summary="Trigger a new workflow run",
    description="Await a multipart, where each part is named after an input and contains the data.",
    responses={
        201: {
            "description": "Workflow run created successfully",
            "schema": WorkflowRunTriggerResponseSchema(),
        },
        404: {"description": "Action not found", "schema": ErrorSchema},
        400: {"description": "Malformed inputs", "schema": ErrorSchema},
    },
    consumes=["multipart/form-data"],
)
async def trigger_workflow(
    request: Request,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
    workflow_view: WorkflowView = Provide[ApplicationContainer.workflow_view],
    storage_service: IStorageService = Provide[
        ApplicationContainer.local_storage_service
    ],
) -> Response:
    workflow_definition_id = request.match_info["workflow_definition_id"]

    try:
        source_action = workflow_view.get_trigger_action(workflow_definition_id)
        # The following condition happens when the source_action is not fully deployed yet.
        if source_action.connection_execution_id is None:
            raise WorkflowNotFoundError
    except WorkflowNotFoundError:
        return json_response(
            ErrorSchema().dump({"error": "Workflow not found"}), status=404
        )

    if not request.content_type.startswith("multipart/"):
        return json_response(
            ErrorSchema().dump({"error": "Only multipart requests are supported"}),
            status=400,
        )
    reader = await request.multipart()
    data: Dict[str, Optional[str]] = {}
    output_files: list[ExecutionResultFinishedEvent.OutputFile] = []
    outputs_dict: Dict[str, ActionOutput] = {}

    for dynamic_output in source_action.dynamic_outputs:
        data[dynamic_output.name] = None
        outputs_dict[dynamic_output.name] = cast(ActionOutput, dynamic_output)

    execution_result_id, workflow_run_id = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=source_action.connection_execution_id,
            executor_id="ryax-portal-executor",
            log_delimiter=None,
        )
    )

    with tempfile.TemporaryDirectory() as output_dir:
        while True:
            field = await reader.next()
            if field is None:
                break
            elif isinstance(field, MultipartReader):
                break
            if field.name not in data:
                return json_response(
                    ErrorSchema().dump(
                        {
                            "error": f"Output field name {field.name} does not exist in the dynamic output data: {data}"
                        }
                    ),
                    status=400,
                )

            if outputs_dict[field.name].type in (
                ActionIOType.FILE,
                ActionIOType.DIRECTORY,  # wait for a zip file directly
            ):
                if field.filename is None:
                    return json_response(
                        ErrorSchema().dump(
                            {"error": "All fields must have a filename."}
                        ),
                        status=400,
                    )
                filename: str = field.filename
                tmp_file_path = os.path.join(
                    output_dir, field.name, os.path.basename(filename)
                )
                os.mkdir(os.path.dirname(tmp_file_path))
                with open(tmp_file_path, "wb") as f:
                    size = await copy_uploaded_file(field, f)

                mimetype, encoding = mimetypes.guess_type(tmp_file_path)

                file_path = storage_service.generate_file_path(filename)
                output_files.append(
                    ExecutionResultFinishedEvent.OutputFile(
                        output_name=field.name,
                        file_path=file_path,
                        mimetype=mimetype,
                        encoding=encoding,
                        size_in_bytes=size,
                    )
                )
                with open(tmp_file_path, "rb") as f:
                    await storage_service.write(file_path, f)

                data[field.name] = file_path

            else:
                new_data = await field.text()
                data[field.name] = new_data

        for field_name, value in data.items():
            if value is None:
                return json_response(
                    ErrorSchema().dump(
                        {"error": f"Missing value for '{html.escape(field_name)}'"}
                    ),
                    status=400,
                )
        # Add headers in the outputs
        data["ryax_headers"] = json.dumps(
            {key: value for key, value in request.headers.items()}
        )

        await message_bus.handle_event(
            ExecutionResultFinishedEvent(
                id=execution_result_id,
                outputs=data,
                status=ExecutionResultState.SUCCESS,
                output_files=output_files,
            )
        )

    result = WorkflowRunTriggerResponseSchema().dump(
        {"workflow_run_id": workflow_run_id}
    )
    return json_response(result, status=201)
