# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger

from aiohttp.web import StreamResponse
from aiohttp.web_request import Request
from aiohttp.web_response import json_response
from aiohttp_apispec import docs
from dependency_injector.wiring import Provide

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.stored_file.storage_exceptions import StorageFileNotFoundError
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.stored_file.stored_file_service import IStoredFileView
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema

logger = getLogger(__name__)


@docs(
    tags=["Filestore"],
    summary="Download file",
    description="Download a file from the filestore in use",
    responses={
        200: {"description": "File downloaded successfully"},
        404: {"description": "Error downloading file", "schema": ErrorSchema},
    },
)
async def download_file(
    request: Request,
    view: IStoredFileView = Provide[ApplicationContainer.stored_file_view],
) -> StreamResponse:
    file_id = request.match_info["stored_file_id"]
    current_project = request["current_project_id"]
    try:
        stored_file: StoredFile = view.get(file_id, current_project)
        stream_response = StreamResponse(status=200, headers=stored_file.get_headers())
        await stream_response.prepare(request)

        for chunk in await view.download_file(stored_file.file_path):
            await stream_response.write(chunk)

        await stream_response.write_eof()
        return stream_response

    except StorageFileNotFoundError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
