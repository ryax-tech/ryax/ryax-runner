# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web
from aiohttp.web_response import Response
from aiohttp_apispec import docs
from dependency_injector.wiring import Provide

from ryax.runner.application.health_service import HealthService
from ryax.runner.container import ApplicationContainer


@docs(
    tags=["Monitoring"],
    summary="Check service status",
    description="Help to know service status",
    responses={
        200: {"description": "Service healthy"},
        400: {"description": "Service unhealthy"},
    },
    security=[],
)
async def health_check(
    _request: web.Request,
    service: HealthService = Provide[ApplicationContainer.health_service],
) -> Response:
    if service.is_healthy():
        return web.json_response("Service healthy", status=200)
    else:
        return web.json_response("Service unhealthy", status=400)
