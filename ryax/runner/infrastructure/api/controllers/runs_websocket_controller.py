import asyncio
from asyncio import Task
from dataclasses import dataclass, field
from logging import getLogger
from typing import cast, Callable

import aiohttp
from aiohttp.web_ws import WebSocketResponse
from aiohttp_apispec import docs
from aiohttp.web_request import Request

from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultNewLogLinesAddedEvent,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema

logger = getLogger(__name__)


@dataclass
class RunsWebSocketController:
    uow_factory: Callable[..., IUnitOfWork]
    message_bus: IMessageBus
    open_websockets: dict[int, Task] = field(default_factory=dict)

    @docs(
        tags=["Execution Logs"],
        summary="Get execution log in stream",
        description="Get the logs for an execution result with its ID.",
        responses={
            101: {
                "description": "Create Execution Log WebSocket",
            },
            404: {
                "description": "Execution Log not found",
                "schema": ErrorSchema(),
            },
        },
    )
    async def stream_logs(
        self,
        request: Request,
    ) -> WebSocketResponse:
        execution_result_id: str = request.match_info["execution_result_id"]

        ws = WebSocketResponse()
        await ws.prepare(request)

        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                if msg.data == "close":
                    await ws.close()
                else:
                    # new_message: str = str(msg.data)
                    # TODO check access rights
                    try:
                        with self.uow_factory() as uow:
                            execution_result_logs = uow.action_logs_repository.get(
                                execution_result_id
                            )
                            await ws.send_str(str(execution_result_logs))
                            await self._register_client(execution_result_id, ws)
                    except Exception as err:
                        await ws.send_str(f"Error while getting logs: {err}")
            elif msg.type == aiohttp.WSMsgType.ERROR:
                logger.warning(
                    "ws connection closed with exception %s" % ws.exception()
                )
        await self._unregister_client(ws)
        logger.debug("Websocket connection for logs closed")
        return ws

    async def _register_client(
        self, execution_result_id: str, websocket: WebSocketResponse
    ) -> None:
        async def push_logs() -> None:
            while True:
                new_logs_event = await self.message_bus.wait_for_event(
                    ExecutionResultNewLogLinesAddedEvent,
                    {"execution_result_id": execution_result_id},
                )
                new_logs_event = cast(
                    ExecutionResultNewLogLinesAddedEvent, new_logs_event
                )
                if new_logs_event.log_lines:
                    await websocket.send_str(new_logs_event.log_lines)

        self.open_websockets[id(websocket)] = asyncio.create_task(push_logs())

    async def _unregister_client(self, websocket: WebSocketResponse) -> None:
        callback = self.open_websockets.get(id(websocket))
        if callback is not None:
            callback.cancel()
