# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from typing import Union, cast

from aiohttp.web_request import Request
from aiohttp.web_response import Response, StreamResponse, json_response
from aiohttp_apispec import docs
from dependency_injector.wiring import Provide

from ryax.runner.application.action_logs_service import ActionLogsService
from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.application.workflow_result_service import WorkflowResultService
from ryax.runner.application.workflow_run_service import WorkflowRunService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_logs.action_logs_entities import LogsNotFoundError
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.exceptions import WorkflowRunNotFoundError
from ryax.runner.domain.execution_result.execution_result_execptions import (
    ExecutionResultNotFoundError,
)
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.stored_file.stored_file_service import IStoredFileView
from ryax.runner.domain.user_auth.api_key.user_api_key_exceptions import (
    UserApiKeyNotPresentInHeaderException,
)
from ryax.runner.domain.user_auth.user_auth_exceptions import (
    UserAuthUnauthorizedException,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeploymentNotFoundError,
)
from ryax.runner.domain.workflow_result.workflow_result import (
    WorkflowResultsNotReadyError,
    WorkflowResultsUnavailableError,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import WorkflowRun
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.runner.infrastructure.api.schemas.execution_log_schema import (
    ExecutionLogSchema,
)
from ryax.runner.infrastructure.api.schemas.workflow_run_schema import (
    WorkflowRunSchema,
    WorkflowRunSchemaMinimal,
)

logger = getLogger(__name__)


@docs(
    tags=["Runs"],
    summary="List workflow runs",
    description="List workflow runs.",
    parameters=[
        {"in": "query", "name": "workflow_id", "type": "string"},
        {"in": "query", "name": "range", "type": "string"},
    ],
    responses={
        200: {
            "description": "Workflow runs fetched successfully",
            "schema": WorkflowRunSchemaMinimal(many=True),
        },
        400: {
            "description": "Too many execution found",
            "schema": ErrorSchema(),
        },
    },
)
async def list_workflow_runs(
    request: Request,
    workflow_run_service: WorkflowRunService = Provide[
        ApplicationContainer.workflow_run_service
    ],
) -> Response:
    current_project = request["current_project_id"]
    workflow_definition_id = request.rel_url.query.get("workflow_id")
    _range = request.rel_url.query.get("range")

    try:
        (workflow_runs, offset, limit, count) = workflow_run_service.list_view(
            current_project,
            workflow_definition_id=workflow_definition_id,
            _range=_range,
        )

        result = WorkflowRunSchemaMinimal().dump(workflow_runs, many=True)
        headers = {
            "Content-Range": f"{offset}-{limit}/{count}",
            "Accept-Range": f"execution {50}",
        }
        return json_response(result, status=200, headers=headers)
    except ValueError as err:
        return json_response(ErrorSchema().dump({"error": str(err)}), status=400)


@docs(
    tags=["Runs"],
    summary="Get a workflow run",
    description="Get one complete workflow run from its id.",
    responses={
        200: {
            "schema": WorkflowRunSchema(),
        },
        404: {
            "description": "Workflow not found",
            "schema": ErrorSchema(),
        },
    },
)
async def get_workflow_run(
    request: Request,
    workflow_runs: WorkflowRunService = Provide[
        ApplicationContainer.workflow_run_service
    ],
) -> Response:
    try:
        workflow_run_id = request.match_info["workflow_run_id"]
        current_project = request["current_project_id"]
        workflow_run = workflow_runs.get_view(workflow_run_id, current_project)

        result = WorkflowRunSchema().dump(workflow_run)
        return json_response(result, status=200)
    except WorkflowRunNotFoundError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Runs"],
    summary="Cancel a workflow run",
    description="Cancel a workflow run by its id. All running actions will stop immediately for this run",
    responses={
        200: {
            "description": "Workflow run canceled successfully",
        },
        404: {
            "description": "Workflow not found",
            "schema": ErrorSchema(),
        },
    },
)
async def cancel_workflow_run(
    request: Request,
    workflow_runs: WorkflowRunService = Provide[
        ApplicationContainer.workflow_run_service
    ],
) -> Response:
    try:
        current_project = request["current_project_id"]
        workflow_run_id: str = request.match_info["workflow_run_id"]
        await workflow_runs.cancel(workflow_run_id, current_project)
        return json_response({}, status=200)
    except WorkflowRunNotFoundError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Runs"],
    summary="Delete a workflow run",
    description="Delete a workflow run by its id. Deletes all execution results and connections belonging to that workflow run.",
    responses={
        200: {
            "description": "Workflow run deleted successfully",
        },
        404: {
            "description": "Workflow not found",
            "schema": ErrorSchema(),
        },
    },
)
async def delete_workflow_run(
    request: Request,
    workflow_runs: WorkflowRunService = Provide[
        ApplicationContainer.workflow_run_service
    ],
) -> Response:
    try:
        current_project = request["current_project_id"]
        workflow_run_id: str = request.match_info["workflow_run_id"]
        await workflow_runs.delete(workflow_run_id, current_project)
        return json_response({}, status=200)
    except WorkflowRunNotFoundError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Execution Logs"],
    summary="Get an execution log",
    description="Get the logs for an execution result with its ID.",
    responses={
        200: {
            "description": "Execution Log fetched successfully",
            "schema": ExecutionLogSchema(),
        },
        404: {
            "description": "Execution Log not found",
            "schema": ErrorSchema(),
        },
    },
)
async def get_execution_result_logs(
    request: Request,
    action_logs_service: ActionLogsService = Provide[
        ApplicationContainer.action_logs_service
    ],
) -> Response:
    try:
        execution_result_id: str = request.match_info["execution_result_id"]
        current_project = request["current_project_id"]
        logs = await action_logs_service.get_logs(execution_result_id, current_project)
        result = ExecutionLogSchema().dump(
            {
                "id": execution_result_id,
                "log": logs,
            }
        )
        return json_response(result)
    except LogsNotFoundError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Results"],
    summary="Get the results for a workflow with user auth",
    description="Get all the results for a workflow by the workflow run ID",
    responses={
        200: {
            "description": "A dictionary of keys/values.",
        },
        404: {
            "description": "Workflow results or workflow run not found",
            "schema": ErrorSchema(),
        },
        503: {"description": "Workflow results not ready yet", "schema": ErrorSchema()},
        422: {
            "description": WorkflowResultsUnavailableError.message,
            "schema": ErrorSchema(),
            401: {
                "description": UserAuthUnauthorizedException.message,
                "schema": ErrorSchema(),
            },
        },
    },
)
async def get_workflow_results_with_user_api_auth(
    request: Request,
    workflow_result_service: WorkflowResultService = Provide[
        ApplicationContainer.workflow_result_service
    ],
    user_auth_service: UserAuthService = Provide[
        ApplicationContainer.user_auth_service
    ],
    uow: IUnitOfWork = Provide[ApplicationContainer.unit_of_work],
    view: IStoredFileView = Provide[ApplicationContainer.stored_file_view],
) -> Union[Response, StreamResponse]:
    """Bypass the service to access directly to the workflow run. Use API user auth instead of classical user auth with project included"""
    try:
        with uow:
            workflow_run_id: str = request.match_info["workflow_run_id"]
            workflow_run: WorkflowRun = uow.workflow_run_repository.get(workflow_run_id)

        project_id: str = workflow_run.project_id

        user_auth_service.authenticate(project_id, cast(dict, request.headers))

        workflow_results = await workflow_result_service.get(
            workflow_run_id=workflow_run_id, project_id=project_id
        )
        if isinstance(workflow_results, StoredFile):
            stream_response = StreamResponse(
                status=200, headers=workflow_results.get_headers()
            )
            await stream_response.prepare(request)
            for chunk in await view.download_file(workflow_results.file_path):
                await stream_response.write(chunk)
            await stream_response.write_eof()
            return stream_response
        else:
            return json_response(workflow_results, status=200)

    except (
        ExecutionResultNotFoundError,
        WorkflowRunNotFoundError,
        WorkflowDeploymentNotFoundError,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)

    except WorkflowResultsNotReadyError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=503)

    except WorkflowResultsUnavailableError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=422)

    except (
        UserAuthUnauthorizedException,
        UserApiKeyNotPresentInHeaderException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=401)


@docs(
    tags=["Workflow Results"],
    summary="Get the results for a workflow",
    description="Get all the results for a workflow by the workflow run ID",
    responses={
        200: {
            "description": "A dictionary of keys/values.",
        },
        404: {
            "description": "Workflow results or workflow run not found",
            "schema": ErrorSchema(),
        },
        503: {"description": "Workflow results not ready yet", "schema": ErrorSchema()},
        422: {
            "description": WorkflowResultsUnavailableError.message,
            "schema": ErrorSchema(),
        },
    },
)
async def get_workflow_results(
    request: Request,
    workflow_result_service: WorkflowResultService = Provide[
        ApplicationContainer.workflow_result_service
    ],
    view: IStoredFileView = Provide[ApplicationContainer.stored_file_view],
) -> Union[Response, StreamResponse]:
    try:
        workflow_run_id: str = request.match_info["workflow_run_id"]
        current_project = request["current_project_id"]
        workflow_results = await workflow_result_service.get(
            workflow_run_id=workflow_run_id, project_id=current_project
        )
        if isinstance(workflow_results, StoredFile):
            stream_response = StreamResponse(
                status=200, headers=workflow_results.get_headers()
            )
            await stream_response.prepare(request)
            for chunk in await view.download_file(workflow_results.file_path):
                await stream_response.write(chunk)
            await stream_response.write_eof()
            return stream_response
        else:
            return json_response(workflow_results, status=200)

    except (
        ExecutionResultNotFoundError,
        WorkflowRunNotFoundError,
        WorkflowDeploymentNotFoundError,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)

    except WorkflowResultsNotReadyError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=503)

    except WorkflowResultsUnavailableError as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=422)
