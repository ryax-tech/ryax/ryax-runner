from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, request_schema
from dependency_injector.wiring import Provide

from ryax.runner.application.user_api_key_service import UserApiKeyService
from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.user_auth.api_key.user_api_key_exceptions import (
    UserApiKeyDateInvalidException,
    UserApiKeyNameAlreadyExistsException,
    UserApiKeyNotFoundException,
)
from ryax.runner.infrastructure.api.schemas.api_key_schema import (
    AddUserApiKeySchema,
    DecryptedUserApiKeySchema,
)
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema


@docs(
    tags=["User Api Keys"],
    summary="Add user api key",
    description="Add new user api key",
    responses={
        201: {
            "description": "User api key created successfully",
            "schema": DecryptedUserApiKeySchema,
        },
        400: {
            "description": "User api key with this name already exists",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(AddUserApiKeySchema)
async def add_user_api_key(
    request: Request,
    user_api_key_service: UserApiKeyService = Provide[
        ApplicationContainer.user_api_key_service
    ],
) -> Response:
    project_id = request["current_project_id"]
    name = request["data"]["name"]
    expiration_date = request["data"].get("expiration_date")
    try:
        decrypted_key = user_api_key_service.add(
            name=name, project_id=project_id, expiration_date=expiration_date
        )
        result = DecryptedUserApiKeySchema().dump(decrypted_key)
        return json_response(result, status=201)
    except (
        UserApiKeyNameAlreadyExistsException,
        UserApiKeyDateInvalidException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["User Api Keys"],
    summary="Delete user api key",
    description="Delete a user api key",
    responses={
        200: {"description": "API key deleted successfully"},
        404: {"description": "Api key not found", "schema": ErrorSchema},
    },
)
async def delete_user_api_key(
    request: Request,
    user_api_key_service: UserApiKeyService = Provide[
        ApplicationContainer.user_api_key_service
    ],
    user_auth_service: UserAuthService = Provide[
        ApplicationContainer.user_auth_service
    ],
) -> Response:
    try:
        current_project = request["current_project_id"]
        api_key_id = request.match_info["user_api_key_id"]
        user_api_key_service.delete(api_key_id, current_project)
        auth_methods: dict = user_auth_service.get_all_auth_methods(current_project)
        return json_response(auth_methods, status=200)
    except UserApiKeyNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["User Api Keys"],
    summary="Revoke user api key",
    description="Revoke user api key",
    responses={
        200: {
            "description": "User api key revoked",
        },
        400: {"description": "User apu key not found", "schema": ErrorSchema},
    },
)
async def revoke_user_api_key(
    request: Request,
    user_api_key_service: UserApiKeyService = Provide[
        ApplicationContainer.user_api_key_service
    ],
    user_auth_service: UserAuthService = Provide[
        ApplicationContainer.user_auth_service
    ],
) -> Response:
    project_id = request["current_project_id"]
    api_key_id = request.match_info["user_api_key_id"]
    try:
        user_api_key_service.revoke(api_key_id, project_id=project_id)
        auth_methods: dict = user_auth_service.get_all_auth_methods(
            project_id=project_id
        )
        return json_response(auth_methods, status=200)
    except (UserApiKeyNotFoundException, UserApiKeyNameAlreadyExistsException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["User Api Keys"],
    summary="Enable or disable user api key",
    description="Toggle user api key auth",
    responses={
        200: {
            "description": "User api key auth toggled successfully",
        },
    },
)
async def toggle_api_key_auth(
    request: Request,
    user_api_key_service: UserApiKeyService = Provide[
        ApplicationContainer.user_api_key_service
    ],
    user_auth_service: UserAuthService = Provide[
        ApplicationContainer.user_auth_service
    ],
) -> Response:
    project_id = request["current_project_id"]
    user_api_key_service.toggle(project_id=project_id)
    auth_methods: dict = user_auth_service.get_all_auth_methods(project_id=project_id)
    return json_response(auth_methods, status=200)
