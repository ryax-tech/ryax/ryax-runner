from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs
from dependency_injector.wiring import Provide

from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.container import ApplicationContainer


@docs(
    tags=["User Auth"],
    summary="Get project user auth auth",
    description="Get the info on the project auth",
    responses={
        201: {
            "description": "User auth fetched successfully",
        }
    },
)
async def list_project_user_authorization(
    request: Request,
    user_auth_service: UserAuthService = Provide[
        ApplicationContainer.user_auth_service
    ],
) -> Response:
    project_id = request["current_project_id"]
    auth_methods: dict = user_auth_service.get_all_auth_methods(project_id)
    return json_response(auth_methods, status=200)
