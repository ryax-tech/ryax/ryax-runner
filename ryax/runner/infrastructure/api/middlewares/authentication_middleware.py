# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Any, Callable, List, Union

from aiohttp import web
from aiohttp.web_response import Response
from dependency_injector.wiring import Provide

from ryax.runner.application.authentication_service import AuthenticationService
from ryax.runner.container import ApplicationContainer

# Design decorator to indicate that auth will be skipped
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema


def skip() -> Callable:
    def wrapper(handler: Any) -> Callable:
        handler.auth_skip = True
        return handler

    return wrapper


def check_token(
    public_paths: List[str],
    service: AuthenticationService = Provide[
        ApplicationContainer.authentication_service
    ],
) -> Callable:
    @web.middleware
    async def middleware_handler(
        request: web.Request, handler: Callable
    ) -> Union[Response, Callable]:
        is_public = any(request.path.startswith(item) for item in public_paths)
        if is_public:
            return await handler(request)
        elif getattr(handler, "auth_skip", False):
            return await handler(request)
        else:
            token = request.headers.get("Authorization", "")
            token_payload = service.check_access(token)
            if not token_payload:
                result = ErrorSchema().dump({"name": "Access denied"})
                return web.json_response(result, status=401)
            else:
                request["user_id"] = token_payload.user_id
                return await handler(request)

    return middleware_handler
