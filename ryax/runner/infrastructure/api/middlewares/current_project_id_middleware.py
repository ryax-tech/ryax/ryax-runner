# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Callable, List, Union

from aiohttp import web
from aiohttp.web_response import Response
from dependency_injector.wiring import Provide

from ryax.runner.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.exceptions import NoCurrentProjectException
from ryax.runner.infrastructure.api.schemas.error_schema import ErrorSchema


def handle(
    public_paths: List[str],
    service: ProjectAuthorizationService = Provide[
        ApplicationContainer.project_authorization_service
    ],
) -> Callable:
    """Get current project id from authorization with a http request then Inject it in the request"""

    @web.middleware
    async def middleware_handler(
        request: web.Request, handler: Callable
    ) -> Union[Callable, Response]:
        is_public: bool = any(request.path.startswith(item) for item in public_paths)
        authorization_token: str = request.headers.get("Authorization", "")
        if is_public or getattr(handler, "auth_skip", False):
            return await handler(request)
        else:
            user_project = await service.get_current_project(
                request["user_id"], authorization_token
            )
            if not user_project:
                error_result = ErrorSchema().dump(
                    {"name": NoCurrentProjectException.message}
                )
                return web.json_response(error_result, status=401)
            request["current_project_id"] = user_project
            return await handler(request)

    return middleware_handler
