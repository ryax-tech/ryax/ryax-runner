# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionEndedEvent,
)
from ryax.runner.domain.execution_connection.execution_connection_service import (
    IExecutionService,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus


@dataclass
class DUMMYExecutionService(IExecutionService):
    message_bus: IMessageBus

    async def start(
        self, execution: ExecutionConnection, addons_extra_params: dict
    ) -> None:
        pass

    async def stop(self, execution: ExecutionConnection) -> None:
        await self.message_bus.handle(
            [
                ExecutionConnectionEndedEvent(
                    execution.id,
                    ExecutionConnectionState.COMPLETED,
                    execution.workflow_run_id,
                )
            ]
        )
