# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from logging import getLogger

from opentelemetry.instrumentation.grpc import GrpcAioInstrumentorClient

from ryax.runner.domain.execution_connection.execution_connection_commands import (
    StopExecutionConnectionCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_service import (
    IExecutionService,
)
from ryax.common.domain.internal_messaging.event_publisher import IPublisherService

logger = getLogger(__name__)

GrpcAioInstrumentorClient().instrument()


class UnsupportedIOType(Exception):
    pass


class ExecutionInitializationError(Exception):
    pass


@dataclass
class WorkerExecutionService(IExecutionService):
    publisher: IPublisherService

    async def start(
        self, execution: ExecutionConnection, addons_extra_params: dict
    ) -> None:
        assert execution.action_deployment is not None
        site_name = execution.action_deployment.node_pool.site.name

        logger.info("Starting Execution %s client on %s", execution.id, site_name)
        await self.publisher.publish([execution], site_name=site_name)

    async def stop(self, execution: ExecutionConnection) -> None:
        assert execution.action_deployment is not None
        site_name = execution.action_deployment.node_pool.site.name

        logger.info("Stopping Execution %s client on %s", execution.id, site_name)
        await self.publisher.publish(
            [StopExecutionConnectionCommand(execution.id)], site_name=site_name
        )
