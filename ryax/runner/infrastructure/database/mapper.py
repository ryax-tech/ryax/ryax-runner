# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from sqlalchemy import and_
from sqlalchemy.engine import Engine
from sqlalchemy.ext.orderinglist import ordering_list
from sqlalchemy.orm import registry, relationship

from ryax.common.domain.registration.registration_values import (
    ObjectiveScores,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionDynamicOutput,
    ActionInput,
    ActionOutput,
    ActionResources,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.accounting.accounting_entities import AccountingEntry
from ryax.runner.domain.action_logs.action_logs_entities import (
    ActionLogs,
    LogLine,
)
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.user_auth.api_key.api_key_entitites import UserApiKey
from ryax.runner.domain.user_auth.user_auth_entities import ProjectSecurityRequirement
from ryax.runner.domain.workflow_action.workflow_action import (
    ReferenceInput,
    StaticInput,
    SchedulingConstraints,
    SchedulingObjectives,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowAction,
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_result.workflow_result import WorkflowResult
from ryax.runner.domain.workflow_run.workflow_run_entities import ActionRun, WorkflowRun
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_entities import (
    RecommendationVPA,
)
from ryax.runner.infrastructure.database.metadata import (
    action_definition_table,
    action_deployment_table,
    action_input_types_table,
    action_output_types_table,
    action_resources_table,
    action_run_table,
    execution_connection_table,
    execution_result_association_table,
    execution_result_table,
    project_security_requirements_table,
    stored_file_association,
    stored_file_table,
    user_api_key_table,
    workflow_action_association_table,
    workflow_action_dynamic_outputs_table,
    workflow_action_reference_inputs_table,
    workflow_action_static_inputs_table,
    workflow_action_table,
    workflow_deployment_table,
    workflow_result_table,
    workflow_run_table,
    action_logs_table,
    action_log_lines_table,
    site_table,
    node_pool_table,
    objective_scores_table,
    metadata,
    scheduling_constraints_table,
    scheduling_objectives_table,
    scheduling_constraints_sites_association_table,
    scheduling_constraints_node_pools_association_table,
    recommendation_vpa_table,
    accounting_resources_table,
)

# Migrate to SqlAlchemy 2.x
mapper_reg = registry()
mapper = mapper_reg.map_imperatively


def start_mapping() -> None:
    mapper(
        ActionInput,
        action_input_types_table,
    )

    mapper(
        ActionOutput,
        action_output_types_table,
    )

    mapper(ActionDynamicOutput, workflow_action_dynamic_outputs_table)

    mapper(WorkflowResult, workflow_result_table)

    mapper(ActionResources, action_resources_table)

    mapper(
        ActionDefinition,
        action_definition_table,
        properties={
            "inputs": relationship(
                ActionInput,
                cascade="all, delete",
                primaryjoin=and_(
                    action_input_types_table.columns.action_definition_id
                    == action_definition_table.columns.id,
                    ActionInput.addon_name == None,  # noqa
                ),
            ),
            "addons_inputs": relationship(
                ActionInput,
                cascade="all, delete",
                primaryjoin=and_(
                    action_input_types_table.columns.action_definition_id
                    == action_definition_table.columns.id,
                    ActionInput.addon_name != None,  # noqa
                ),
                overlaps="inputs",
            ),
            "outputs": relationship(
                ActionOutput,
                cascade="all, delete",
            ),
            "resources": relationship(
                ActionResources,
                uselist=False,
                cascade="all, delete",
            ),
        },
    )
    mapper(ObjectiveScores, objective_scores_table)
    mapper(
        NodePool,
        node_pool_table,
        properties={
            "objective_scores": relationship(
                ObjectiveScores,
                single_parent=True,
                uselist=False,
                lazy=False,
                cascade="all, delete-orphan",
            ),
            "site": relationship(
                Site,
                back_populates="node_pools",
                uselist=False,
                lazy=False,
            ),
        },
    )
    mapper(
        Site,
        site_table,
        properties={
            "node_pools": relationship(
                NodePool,
                cascade="all, delete-orphan",
                passive_deletes=True,
                back_populates="site",
            ),
        },
    )
    mapper(
        ActionDeployment,
        action_deployment_table,
        properties={
            "action_definition": relationship(ActionDefinition, uselist=False),
            "node_pool": relationship(NodePool, uselist=False),
        },
    )

    mapper(
        StaticInput,
        workflow_action_static_inputs_table,
    )

    mapper(ReferenceInput, workflow_action_reference_inputs_table)

    mapper(
        SchedulingConstraints,
        scheduling_constraints_table,
        properties={
            "site_list": relationship(
                Site,
                secondary=scheduling_constraints_sites_association_table,
            ),
            "node_pool_list": relationship(
                NodePool, secondary=scheduling_constraints_node_pools_association_table
            ),
        },
    )
    mapper(
        SchedulingObjectives,
        scheduling_objectives_table,
        properties={
            "node_pool_objective_score": relationship(ObjectiveScores, uselist=False)
        },
    )

    mapper(
        WorkflowAction,
        workflow_action_table,
        properties={
            "definition": relationship(ActionDefinition, uselist=False),
            "static_inputs": relationship(StaticInput, cascade="all, delete"),
            "dynamic_outputs": relationship(ActionDynamicOutput, cascade="all, delete"),
            "reference_inputs": relationship(ReferenceInput, cascade="all, delete"),
            "next_actions": relationship(
                WorkflowAction,
                secondary=workflow_action_association_table,
                primaryjoin=workflow_action_table.c.id
                == workflow_action_association_table.c.parents,
                secondaryjoin=workflow_action_table.c.id
                == workflow_action_association_table.c.children,
                backref="previous_actions",
                passive_deletes=True,
            ),
            "objectives": relationship(
                SchedulingObjectives, cascade="all, delete", uselist=False
            ),
            "constraints": relationship(
                SchedulingConstraints, cascade="all, delete", uselist=False
            ),
        },
    )

    mapper(
        WorkflowDeployment,
        workflow_deployment_table,
        properties={
            "actions": relationship(
                WorkflowAction,
                order_by=workflow_action_table.columns.position,
                collection_class=ordering_list("position"),
            ),
            "results": relationship(
                WorkflowResult,
            ),
        },
    )
    mapper(AccountingEntry, accounting_resources_table)
    mapper(
        ExecutionResult,
        execution_result_table,
        properties={
            "children": relationship(
                ExecutionResult,
                secondary=execution_result_association_table,
                primaryjoin=execution_result_table.c.id
                == execution_result_association_table.c.parents,
                secondaryjoin=execution_result_table.c.id
                == execution_result_association_table.c.children,
                backref="parents",
                cascade="all, delete",
            ),
            "workflow_action": relationship(WorkflowAction, uselist=False),
            "stored_files": relationship(
                StoredFile,
                secondary=stored_file_association,
                back_populates="execution_results",
                cascade="all, delete",
            ),
        },
    )

    mapper(
        ExecutionConnection,
        execution_connection_table,
        properties={
            "action_deployment": relationship(
                ActionDeployment,
                uselist=False,
            ),
            "workflow_action": relationship(WorkflowAction, uselist=False),
            "parent_execution_result": relationship(
                ExecutionResult, uselist=False, cascade="all, delete"
            ),
        },
    )

    mapper(
        StoredFile,
        stored_file_table,
        properties={
            "execution_results": relationship(
                ExecutionResult,
                secondary=stored_file_association,
                back_populates="stored_files",
            )
        },
    )

    mapper(UserApiKey, user_api_key_table)

    mapper(ProjectSecurityRequirement, project_security_requirements_table)

    mapper(
        ActionRun,
        action_run_table,
        properties={
            "execution_results": relationship(
                ExecutionResult,
                cascade="all, delete",
                order_by=ExecutionResult.submitted_at,
                collection_class=ordering_list("submitted_at"),
            ),
            "workflow_action": relationship(
                WorkflowAction,
            ),
        },
    )
    mapper(
        WorkflowRun,
        workflow_run_table,
        properties={
            "runs": relationship(
                ActionRun,
                cascade="all, delete",
                order_by=ActionRun.position,  # type: ignore
                passive_deletes=True,
            ),
        },
    )
    mapper(
        LogLine,
        action_log_lines_table,
    )
    mapper(
        ActionLogs,
        action_logs_table,
        properties={
            "log_lines": relationship(
                LogLine,
                cascade="all, delete",
                order_by=LogLine.index,  # type: ignore
                lazy="dynamic",
                passive_deletes=True,
            ),
        },
    )
    mapper(
        RecommendationVPA,
        recommendation_vpa_table,
    )


def map_orm(engine: Engine) -> None:
    metadata.create_all(bind=engine)
    start_mapping()
