# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import List

from sqlalchemy import or_, select
from sqlalchemy.exc import MultipleResultsFound, NoResultFound
from sqlalchemy.orm import selectinload

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.exceptions import WorkflowNotFoundError
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    MultipleRunningWorkflowDeploymentsError,
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_repository import (
    IWorkflowDeploymentRepository,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.common.infrastructure.database.engine import Session


@dataclass
class DatabaseWorkflowDeploymentRepository(IWorkflowDeploymentRepository):
    """Database persistence implementation of workflows"""

    session: Session

    def get_all_running(self) -> List[WorkflowDeployment]:
        return (
            self.session.execute(
                select(WorkflowDeployment).where(
                    WorkflowDeployment.state == WorkflowDeploymentState.RUNNING
                )
            )
            .scalars()
            .all()
        )

    def get_all_paused_or_running(self) -> List[WorkflowDeployment]:
        return (
            self.session.execute(
                select(WorkflowDeployment).where(
                    or_(
                        WorkflowDeployment.state == WorkflowDeploymentState.PAUSED,
                        WorkflowDeployment.state == WorkflowDeploymentState.RUNNING,
                    )
                )
            )
            .scalars()
            .all()
        )

    def get(self, workflow_deployment_id: str) -> WorkflowDeployment:
        workflow_deployment = self.session.get(
            WorkflowDeployment, workflow_deployment_id
        )
        if not workflow_deployment:
            raise WorkflowNotFoundError()
        return workflow_deployment

    def get_eventually_running_workflow_by_workflow_definition_id(
        self, workflow_definition_id: str, eager_load: bool = False
    ) -> WorkflowDeployment:
        query = (
            select(WorkflowDeployment)
            .where(WorkflowDeployment.workflow_definition_id == workflow_definition_id)
            .where(
                or_(
                    WorkflowDeployment.state == WorkflowDeploymentState.RUNNING,
                    WorkflowDeployment.state == WorkflowDeploymentState.STARTED,
                    WorkflowDeployment.state == WorkflowDeploymentState.CREATED,
                )
            )
        )
        if eager_load:
            query = query.options(
                selectinload(WorkflowDeployment.actions).selectinload(
                    WorkflowAction.dynamic_outputs
                )
            )
        try:
            workflow_deployment = self.session.execute(query).scalar_one()
        except NoResultFound:
            raise WorkflowNotFoundError()
        except MultipleResultsFound:
            raise MultipleRunningWorkflowDeploymentsError(
                f"Multiple workflows are running for the same workflow definition: {workflow_definition_id}"
            )

        return workflow_deployment

    def get_workflow_action_by_id(self, workflow_action_id: str) -> WorkflowAction:
        workflow_action = self.session.execute(
            select(WorkflowAction)
            .where(WorkflowAction.id == workflow_action_id)
            .options(
                selectinload(WorkflowAction.static_inputs),
                selectinload(WorkflowAction.reference_inputs),
            )
        ).scalar()
        if not workflow_action:
            raise WorkflowNotFoundError()
        return workflow_action

    def list_active_workflows_for_this_action_definition(
        self, action_definition_id: str
    ) -> List[WorkflowDeployment]:
        return (
            self.session.execute(
                select(WorkflowDeployment)
                .join(WorkflowAction)
                .join(ActionDefinition)
                .where(
                    WorkflowDeployment.state.in_(  # type: ignore
                        WorkflowDeploymentState.eventually_running_state()
                    ),
                )
                .where(ActionDefinition.id == action_definition_id)
            )
            .unique()
            .scalars()
            .all()
        )

    def add(self, workflow: WorkflowDeployment) -> None:
        self.session.add(workflow)

    def delete(self, workflow_deployment_id: str) -> None:
        try:
            workflow = self.get(workflow_deployment_id)
            self.session.delete(workflow)
        except NoResultFound:
            raise WorkflowNotFoundError()
