# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List

from sqlalchemy import select, func
from sqlalchemy.exc import NoResultFound

from ryax.runner.domain.user_auth.api_key.api_key_entitites import UserApiKey
from ryax.runner.domain.user_auth.api_key.api_key_repository import (
    IUserApiKeyRepository,
)
from ryax.runner.domain.user_auth.api_key.user_api_key_exceptions import (
    UserApiKeyNameAlreadyExistsException,
    UserApiKeyNotFoundException,
)
from ryax.runner.domain.user_auth.user_auth_entities import ProjectSecurityRequirement
from ryax.common.infrastructure.database.engine import Session


class DatabaseUserApiKeyRepository(IUserApiKeyRepository):
    def __init__(self, session: Session):
        self.session: Session = session

    def add_api_key(self, new_user_api_key: UserApiKey) -> None:
        self.session.add(new_user_api_key)

    def add_project_security_requirement(
        self, project_security_requirement: ProjectSecurityRequirement
    ) -> None:
        self.session.add(project_security_requirement)

    def delete(self, user_api_key: UserApiKey) -> None:
        self.session.delete(user_api_key)

    def get(self, user_api_key_id: str, project_id: str) -> UserApiKey:
        try:
            return self.session.execute(
                select(UserApiKey).where(
                    UserApiKey.id == user_api_key_id,
                    UserApiKey.project_id == project_id,
                )
            ).scalar_one()
        except NoResultFound:
            raise UserApiKeyNotFoundException()

    def get_all_keys(self, project_id: str) -> list[UserApiKey]:
        return (
            self.session.execute(
                select(UserApiKey).where(
                    UserApiKey.project_id == project_id,
                )
            )
            .scalars()
            .all()
        )

    def ensure_name_does_not_exist(self, name: str, project_id: str) -> None:
        if (
            self.session.scalar(
                select(func.count())
                .select_from(UserApiKey)
                .where(UserApiKey.name == name, UserApiKey.project_id == project_id)
            )
            > 0
        ):
            raise UserApiKeyNameAlreadyExistsException()

    def list_auth_methods_for_project(
        self, project_id: str
    ) -> List[ProjectSecurityRequirement]:
        return (
            self.session.execute(
                select(ProjectSecurityRequirement).where(
                    ProjectSecurityRequirement.project_id == project_id
                )
            )
            .scalars()
            .all()
        )

    def get_all_api_keys_for_project(self, project_id: str) -> List[UserApiKey]:
        return (
            self.session.execute(
                select(UserApiKey).where(UserApiKey.project_id == project_id)
            )
            .scalars()
            .all()
        )
