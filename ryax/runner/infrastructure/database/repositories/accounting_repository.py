from dataclasses import dataclass

from sqlalchemy import select, desc

from ryax.common.infrastructure.database.engine import Session
from ryax.runner.domain.accounting.accounting_entities import AccountingEntry
from ryax.runner.domain.accounting.accounting_repository import IAccountingRepository


@dataclass
class DatabaseAccountingRepository(IAccountingRepository):
    session: Session

    def get(self, execution_id: str) -> AccountingEntry | None:
        accounting = self.session.get(AccountingEntry, execution_id)
        return accounting

    def list_accounting(
        self,
        execution_ids: list[str],
        project_ids: list[str],
        action_ids: list[str],
        workflow_ids: list[str],
        run_ids: list[str],
    ) -> list[AccountingEntry]:
        query = select(AccountingEntry)
        if len(execution_ids) != 0:
            query = query.where(
                AccountingEntry.execution_connection_id.in_(execution_ids)  # type: ignore
            )
        if len(project_ids) != 0:
            query = query.where(AccountingEntry.project_id.in_(project_ids))  # type: ignore
        if len(action_ids) != 0:
            query = query.where(AccountingEntry.action_definition_id.in_(action_ids))  # type: ignore
        if len(workflow_ids) != 0:
            query = query.where(AccountingEntry.workflow_definition_id.in_(workflow_ids))  # type: ignore
        if len(run_ids) != 0:
            query = query.where(AccountingEntry.workflow_run_id.in_(run_ids))  # type: ignore

        return (
            self.session.execute(query.order_by(desc(AccountingEntry.submitted_at)))
            .scalars()
            .all()
        )

    def add(self, accounting: AccountingEntry) -> None:
        self.session.add(accounting)
