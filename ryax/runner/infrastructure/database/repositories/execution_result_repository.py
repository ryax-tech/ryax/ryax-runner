# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from datetime import datetime
from logging import getLogger
from typing import List, Optional, Tuple

from sqlalchemy import asc, select, func
from sqlalchemy.exc import NoResultFound

from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_execptions import (
    ExecutionResultNotFoundError,
)
from ryax.runner.domain.execution_result.execution_result_repository import (
    IExecutionResultRepository,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.workflow_result.workflow_result import (
    WorkflowResult,
    WorkflowResultsNotReadyError,
    WorkflowResultView,
)
from ryax.common.infrastructure.database.engine import Session

logger = getLogger(__name__)


@dataclass
class DatabaseExecutionResultRepository(IExecutionResultRepository):
    session: Session

    def add(self, execution: ExecutionResult) -> None:
        self.session.add(execution)

    def get(self, execution_id: str) -> ExecutionResult:
        try:
            return self.session.execute(
                select(ExecutionResult).filter(
                    ExecutionResult.id == execution_id,
                )
            ).scalar_one()
        except NoResultFound:
            raise ExecutionResultNotFoundError()

    def list(
        self,
        project: str,
        workflow_definition_id: Optional[str] = None,
        workflow_action_id: Optional[str] = None,
        state: Optional[ExecutionResultState] = None,
        submitted_before: Optional[datetime] = None,
        submitted_after: Optional[datetime] = None,
        _range: Optional[str] = None,
    ) -> Tuple[List[ExecutionResult], int, int, int]:
        # Filtering
        all_filters: List[bool] = list()
        all_filters.append(ExecutionResult.project_id == project)
        if workflow_definition_id:
            try:
                all_filters.append(
                    ExecutionResult.workflow_action is not None
                    and ExecutionResult.workflow_action.workflow_definition_id
                    == workflow_definition_id
                )
            except AttributeError:
                # The workflow action might not be defined
                pass
        if workflow_action_id:
            try:
                all_filters.append(
                    ExecutionResult.workflow_action.id == workflow_action_id
                )
            except AttributeError:
                # The workflow action might not be defined
                pass
        if state:
            all_filters.append(ExecutionResult.state == state)
        if submitted_before:
            all_filters.append(ExecutionResult.submitted_at < submitted_before)
        if submitted_after:
            all_filters.append(ExecutionResult.submitted_at > submitted_after)

        # Ordering
        order = asc(ExecutionResult.submitted_at)

        # Pagination
        count: int = self.session.scalar(
            select(func.count()).select_from(ExecutionResult).where(*all_filters)
        )
        offset, limit = 0, count
        (_from, _to) = (
            tuple(map(int, _range.split("-"))) if _range is not None else (None, None)
        )
        if _from and _from >= count:
            raise ValueError
        elif _from and _from < count:
            offset = _from
        if _to and _to < count:
            limit = _to - offset
        elif _to and _to >= count:
            limit = count - offset
            _to = count
        if _from is None:
            _from = 0
        if _to is None:
            _to = _from + 50
        if limit > 50:
            raise ValueError(
                "The number of execution to display is too big: > 50. Please use a filter or a range (e.g. range=0-50)."
            )
        executions: List[ExecutionResult] = (
            self.session.execute(
                select(ExecutionResult)
                .where(*all_filters)
                .order_by(order)
                .offset(offset)
                .limit(limit)
            )
            .scalars()
            .all()
        )
        return executions, _from, _to, count

    def get_ids_by_end_of_log_delimiter(
        self, end_of_log_delimiter: str
    ) -> Tuple[str, str]:
        try:
            row = self.session.execute(
                select(
                    ExecutionResult.id, ExecutionResult.execution_connection_id
                ).where(ExecutionResult.end_of_log_delimiter == end_of_log_delimiter)
            ).one()
            return row.id, row.execution_connection_id
        except NoResultFound:
            raise ExecutionResultNotFoundError()

    def get_running_from_connection_id(
        self, execution_connection_id: str
    ) -> ExecutionResult:
        try:
            return self.session.execute(
                select(ExecutionResult).where(
                    ExecutionResult.execution_connection_id == execution_connection_id,
                    ExecutionResult.state == ExecutionResultState.CREATED,
                )
            ).scalar_one()
        except NoResultFound:
            raise ExecutionResultNotFoundError()

    def get_results_by_workflow_run_id(
        self, workflow_run_id: str
    ) -> List[WorkflowResultView]:
        # Ensure there are results for this workflow run.
        execution_results_in_workflow_run: List[ExecutionResult] = self.session.scalars(
            select(ExecutionResult).where(
                ExecutionResult.workflow_run_id == workflow_run_id
            )
        ).all()
        if not execution_results_in_workflow_run:
            raise ExecutionResultNotFoundError()

        # Find workflow deployment id from the execution result.
        workflow_deployment_id = execution_results_in_workflow_run[
            0
        ].workflow_action.workflow_deployment_id

        # Get all workflow results by their workflow deployment id
        workflow_results: List[WorkflowResult] = self.session.scalars(
            select(WorkflowResult).where(
                WorkflowResult.workflow_deployment_id == workflow_deployment_id
            )
        ).all()
        if not workflow_results:
            return []

        # Find the inputs/outputs related to these workflow results and return them.
        workflow_result_views = []
        for workflow_result in workflow_results:
            result_value = None
            result_type = None
            for execution_result in execution_results_in_workflow_run:
                if (
                    workflow_result.action_id
                    == execution_result.workflow_action.id_in_studio
                ):
                    if (
                        workflow_result.technical_name
                        in execution_result.outputs.keys()
                    ):
                        result_value = execution_result.outputs[
                            workflow_result.technical_name
                        ]
                        for (
                            action_output
                        ) in execution_result.workflow_action.get_outputs():
                            if action_output.name == workflow_result.technical_name:
                                result_type = action_output.type
                                break

            if result_value is None or result_type is None:
                raise WorkflowResultsNotReadyError()
            workflow_result_views.append(
                WorkflowResultView(
                    key=workflow_result.key, value=result_value, type=result_type
                )
            )
        return workflow_result_views
