# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from sqlalchemy import select, func
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import selectinload

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    ActionDeploymentNotFoundError,
    IActionDeploymentRepository,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.runner.domain.action_deployment.action_deployment_view import (
    ActionDeploymentSchedulerView,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.common.infrastructure.database.engine import Session


@dataclass
class DatabaseActionDeploymentRepository(IActionDeploymentRepository):
    """Database repository for action deployment"""

    session: Session

    def get_by_id(
        self, action_deployment_id: str, eager_load: bool = False
    ) -> ActionDeployment:
        query = select(ActionDeployment).where(
            ActionDeployment.id == action_deployment_id
        )
        if eager_load:
            query = query.options(selectinload(ActionDeployment.action_definition))
        try:
            action_deployment = self.session.execute(query).scalar_one()
        except NoResultFound:
            raise ActionDeploymentNotFoundError()
        return action_deployment

    def get_by_workflow_definition_id(
        self, workflow_definition_id: str
    ) -> ActionDeployment:
        query = (
            select(ActionDeployment)
            .join(
                ExecutionConnection,
                ActionDeployment.allocated_connection_id == ExecutionConnection.id,
            )
            .join(WorkflowAction)
            .where(WorkflowAction.workflow_definition_id == workflow_definition_id)
            .where(WorkflowAction.is_root_action == True)  # noqa
        )
        try:
            return self.session.execute(query).unique().scalar_one()
        except NoResultFound:
            raise ActionDeploymentNotFoundError()

    def list_from_action_definition_id(
        self, action_definition_id: str, project_id: str
    ) -> list[ActionDeployment]:
        return (
            self.session.execute(
                select(ActionDeployment)
                .where(ActionDeployment.project_id == project_id)
                .join(ActionDeployment.action_definition)
                .where(ActionDefinition.id == action_definition_id)
            )
            .unique()
            .scalars()
            .all()
        )

    def list_unallocated_and_ready_from_action_definition_id(
        self, action_definition_id: str, project_id: str, node_pool_id: str
    ) -> list[ActionDeployment]:
        """
        returns all actions that match the definition the project and the site that are unallocated and ready
        """
        return (
            self.session.execute(
                select(ActionDeployment)
                .where(ActionDeployment.project_id == project_id)
                .where(ActionDeployment.action_definition_id == action_definition_id)  # type: ignore
                .where(ActionDeployment.state == ActionDeploymentState.READY)
                .where(ActionDeployment.allocated_connection_id == None)  # noqa: E711
                .where(ActionDeployment.node_pool_id == node_pool_id)  # type: ignore
            )
            .scalars()
            .all()
        )

    def list_eventually_ready_from_action_definition_id(
        self,
        action_definition_id: str,
        project_id: str,
        node_pool_id: str,
    ) -> list[ActionDeploymentSchedulerView]:
        """
        returns all actions where action definition id match and is in a state that will eventually lead to READY
        """
        return [
            ActionDeploymentSchedulerView(
                id=row.id,
                state=row.state,
                allocated_connection_id=row.allocated_connection_id,
            )
            for row in self.session.execute(
                select(
                    ActionDeployment.id,
                    ActionDeployment.allocated_connection_id,
                    ActionDeployment.state,
                )
                .where(ActionDeployment.project_id == project_id)
                .where(ActionDeployment.action_definition_id == action_definition_id)  # type: ignore
                .where(
                    ActionDeployment.state.in_(  # type: ignore
                        ActionDeploymentState.eventually_ready_states()
                    )
                )
                .where(ActionDeployment.node_pool_id == node_pool_id)  # type: ignore
            )
        ]

    def list_unallocated(self) -> list[ActionDeployment]:
        """
        returns all actions where unallocated and ready
        """
        return (
            self.session.execute(
                select(ActionDeployment)
                .where(
                    ActionDeployment.state.in_(  # type: ignore
                        [
                            ActionDeploymentState.READY,
                            ActionDeploymentState.NO_RESOURCES_AVAILABLE,
                            ActionDeploymentState.DEPLOYING,
                        ]
                    )
                )
                .where(ActionDeployment.allocated_connection_id == None)  # noqa: E711
            )
            .scalars()
            .all()
        )

    def list_eventually_ready(self) -> list[ActionDeployment]:
        return (
            self.session.execute(
                select(ActionDeployment).where(
                    ActionDeployment.state.in_(  # type: ignore
                        ActionDeploymentState.eventually_ready_states()
                    )
                )
            )
            .scalars()
            .all()
        )

    def count_eventually_ready(self, node_pool_id: str) -> int:
        return self.session.scalar(
            select(func.count())
            .select_from(ActionDeployment)
            .where(
                ActionDeployment.state.in_(  # type: ignore
                    ActionDeploymentState.eventually_ready_states()
                )
            )
            .where(ActionDeployment.node_pool_id == node_pool_id)  # type: ignore
        )

    def list_all_in_progress(self) -> list[ActionDeployment]:
        return (
            self.session.execute(
                select(ActionDeployment).where(
                    ActionDeployment.state.in_(  # type: ignore
                        ActionDeploymentState.eventually_ready_states()
                        + [ActionDeploymentState.UNDEPLOYING]
                    )
                )
            )
            .scalars()
            .all()
        )

    def add(self, action_deployment: ActionDeployment) -> None:
        self.session.add(action_deployment)

    def delete(self, action_deployment_id: str) -> None:
        try:
            action = self.get_by_id(action_deployment_id)
            self.session.delete(action)
        except NoResultFound:
            raise ActionDeploymentNotFoundError
