# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from logging import getLogger
from typing import List, Optional, Tuple, Sequence

from sqlalchemy import and_, delete, desc, func, select
from sqlalchemy.orm import Session, selectinload
from sqlalchemy.sql.functions import count

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.runner.domain.action_logs.action_logs_entities import ActionLogs
from ryax.runner.domain.exceptions import WorkflowRunNotFoundError
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.workflow_action.workflow_action import (
    WorkflowAction,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import WorkflowRun, ActionRun
from ryax.runner.domain.workflow_run.workflow_run_repository import (
    IWorkflowRunRepository,
)
from ryax.runner.domain.workflow_run.workflow_run_values import WorkflowRunState
from ryax.runner.infrastructure.database.metadata import stored_file_association

logger = getLogger(__name__)


@dataclass
class DatabaseWorkflowRunRepository(IWorkflowRunRepository):
    session: Session

    def add(self, workflow_run: WorkflowRun) -> None:
        self.session.add(workflow_run)

    def list_with_filter(
        self,
        workflow_definition_id: Optional[str] = None,
        state: Optional[WorkflowRunState] = None,
        _range: Optional[str] = None,
    ) -> Tuple[List[WorkflowRun], int, int, int]:
        """
        Always return the WorkflowRun without any runs.
        """
        # Filtering
        all_filters: List[bool] = list()
        if workflow_definition_id is not None:
            all_filters.append(
                WorkflowRun.workflow_definition_id == workflow_definition_id
            )

        if state is not None:
            all_filters.append(WorkflowRun.state == state)

        # Pagination
        count: int = self.session.execute(
            select(func.count()).select_from(WorkflowRun).where(*all_filters)
        ).scalar_one()
        offset, limit = 0, count
        _from, _to = (
            tuple(map(int, _range.split("-"))) if _range is not None else (None, None)
        )
        if _from and _from >= count:
            raise ValueError
        elif _from and _from < count:
            offset = _from
        if _to and _to < count:
            limit = _to - offset
        elif _to and _to >= count:
            limit = count - offset
            _to = count
        if _from is None:
            _from = 0
        if _to is None:
            _to = _from + 50
        if limit > 50:
            raise ValueError(
                "The number of runs to display is too big: > 50. Please use a filter or a range"
                "(e.g. range=0-50)."
            )
        workflow_runs = (
            self.session.execute(
                select(WorkflowRun)
                .filter(*all_filters)
                .order_by(desc(WorkflowRun.started_at))
                .offset(offset)
                .limit(limit)
            )
            .scalars()
            .all()
        )
        return workflow_runs, _from, _to, count

    def list_all(
        self,
        workflow_definition_id: Optional[str] = None,
        state: Optional[WorkflowRunState] = None,
        eager_load: bool = False,
    ) -> List[WorkflowRun]:
        """
        Always return the WorkflowRun without any runs.
        """
        # Filtering
        all_filters: List[bool] = []
        if workflow_definition_id is not None:
            all_filters.append(
                WorkflowRun.workflow_definition_id == workflow_definition_id
            )

        if state is not None:
            all_filters.append(WorkflowRun.state == state)

        query = (
            select(WorkflowRun)
            .filter(*all_filters)
            .order_by(desc(WorkflowRun.started_at))
        )

        if eager_load:
            query = query.options(
                selectinload(WorkflowRun.runs).selectinload(ActionRun.execution_results)
            )

        workflow_runs = self.session.execute(query).scalars().all()
        return workflow_runs

    def get(self, workflow_run_id: str) -> WorkflowRun:
        result = self.session.get(WorkflowRun, workflow_run_id)
        if result is None:
            raise WorkflowRunNotFoundError()
        return result

    def delete(self, workflow_run_id: str) -> None:
        """Delete all ExecutionResults and ExecutionConnections for this workflow run."""
        self.delete_all([workflow_run_id])

    def get_stored_files_to_delete(
        self, workflow_run_ids: Sequence[str]
    ) -> list[StoredFile]:
        return (
            self.session.execute(
                select(StoredFile)
                .join(stored_file_association)
                .join(ExecutionResult)
                .where(
                    ExecutionResult.workflow_run_id.in_(workflow_run_ids)  # type: ignore
                )
            )
            .unique()
            .scalars()
            .all()
        )

    def delete_all(self, workflow_run_ids: Sequence[str]) -> None:
        self.session.execute(
            delete(ExecutionConnection).filter(
                ExecutionConnection.workflow_run_id.in_(workflow_run_ids)  # type: ignore
            )
        )
        execution_results_ids = (
            self.session.execute(
                select(ExecutionResult.id).filter(
                    ExecutionResult.workflow_run_id.in_(workflow_run_ids)  # type: ignore
                )
            )
            .scalars()
            .all()
        )
        self.session.execute(
            delete(ExecutionResult).where(ExecutionResult.id.in_(execution_results_ids))  # type: ignore
        )
        self.session.execute(
            delete(ActionLogs).where(
                ActionLogs.associated_id.in_(execution_results_ids)  # type: ignore
            )
        )
        workflow_runs = (
            self.session.execute(
                select(WorkflowRun).where(WorkflowRun.id.in_(workflow_run_ids))  # type: ignore
            )
            .scalars()
            .all()
        )
        for workflow_run in workflow_runs:
            self.session.delete(workflow_run)

        self.session.commit()
        self.garbage_collect()

    def delete_oldest(self, max_runs_per_project: int) -> None:
        nb_runs = self.session.execute(
            select(WorkflowRun.project_id, count(WorkflowRun.id)).group_by(
                WorkflowRun.project_id
            )
        )
        for project_id, nb_run in nb_runs:
            if nb_run > max_runs_per_project:
                workflow_run_ids = (
                    self.session.execute(
                        select(WorkflowRun.id)
                        .order_by(WorkflowRun.last_result_at)
                        .limit(nb_run - max_runs_per_project)
                        .where(WorkflowRun.project_id == project_id)
                    )
                    .scalars()
                    .all()
                )
                self.delete_all(workflow_run_ids)

    def garbage_collect(self) -> None:
        """Deletion of linked object because Runs can keep reference on old object not in used elsewhere"""
        self._delete_unused_action_deployments()
        self._delete_unused_workflow_actions()
        self._delete_unused_workflow_deployments()
        self._delete_unused_action_definitions()

    def _delete_unused_action_deployments(self) -> None:
        """Delete ActionDeployment to which no ExecutionConnection refer"""
        action_deployments = (
            (
                self.session.execute(
                    select(ActionDeployment).filter(
                        and_(
                            ActionDeployment.id.notin_(  # type: ignore
                                select(ActionDeployment.id)  # type: ignore
                                .join(ExecutionConnection)
                                .where(ExecutionConnection.action_deployment != None)  # type: ignore # noqa
                            ),
                            ActionDeployment.state == ActionDeploymentState.UNDEPLOYED,  # type: ignore
                        )
                    )
                )
            )
            .scalars()
            .all()
        )
        for action_deployment in action_deployments:
            self.session.delete(action_deployment)

    def _delete_unused_workflow_actions(self) -> None:
        """Delete WorkflowAction to which no ExecutionConnection, ExecutionResult, or Action Run refer to"""
        workflow_actions = (
            (
                self.session.execute(
                    select(WorkflowAction).filter(
                        WorkflowAction.id.notin_(  # type: ignore
                            select(WorkflowAction.id)  # type: ignore
                            .join(ExecutionConnection)
                            .where(ExecutionConnection.workflow_action != None)  # noqa
                        ),
                        WorkflowAction.id.notin_(  # type: ignore
                            select(WorkflowAction.id)  # type: ignore
                            .join(ExecutionResult)
                            .where(ExecutionResult.workflow_action != None),  # noqa
                        ),
                        WorkflowAction.id.notin_(  # type: ignore
                            select(WorkflowAction.id)  # type: ignore
                            .join(ActionRun)
                            .where(ActionRun.workflow_action != None),  # noqa
                        ),
                    )
                )
            )
            .scalars()
            .all()
        )
        with self.session.no_autoflush:
            for workflow_action in workflow_actions:
                self.session.delete(workflow_action)

    def _delete_unused_workflow_deployments(self) -> None:
        """Delete WorkflowDeployment that is undeployed"""
        workflow_deployments = (
            self.session.execute(
                select(WorkflowDeployment).filter(
                    WorkflowDeployment.id.notin_(  # type: ignore
                        select(WorkflowAction.workflow_deployment_id).where(  # type: ignore
                            WorkflowAction.workflow_deployment_id != None  # noqa
                        )
                    )
                )
            )
            .scalars()
            .all()
        )
        for workflow_deployment in workflow_deployments:
            self.session.delete(workflow_deployment)

    def _delete_unused_action_definitions(self) -> None:
        """Delete ActionDeployment to which no WorkflowAction refer"""
        action_definitions = (
            (
                self.session.execute(
                    select(ActionDefinition).filter(
                        ActionDefinition.id.notin_(  # type: ignore
                            select(ActionDefinition.id)  # type: ignore
                            .join(WorkflowAction)  # type: ignore
                            .where(
                                WorkflowAction.definition != None  # noqa # type: ignore
                            )
                        ),
                        ActionDefinition.id.notin_(  # type: ignore
                            select(ActionDefinition.id)  # type: ignore
                            .join(ActionDeployment)
                            .where(  # type: ignore
                                ActionDeployment.action_definition  # type: ignore
                                != None  # noqa
                            )
                        ),
                    )
                )
            )
            .scalars()
            .all()
        )
        for action_definition in action_definitions:
            self.session.delete(action_definition)
