# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import selectinload

from ryax.common.domain.registration.registration_values import (
    Objectives,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_exceptions import (
    ExecutionConnectionNotFoundError,
)
from ryax.runner.domain.execution_connection.execution_connection_repository import (
    IExecutionConnectionRepository,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.scheduler.scheduler_entities import PendingExecutionConnection
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.common.infrastructure.database.engine import Session


@dataclass
class DatabaseExecutionConnectionRepository(IExecutionConnectionRepository):
    session: Session

    def add(self, execution: ExecutionConnection) -> None:
        self.session.add(execution)

    def get(
        self, execution_connection_id: str, eager_load: bool = False
    ) -> ExecutionConnection:
        query = select(ExecutionConnection).where(
            ExecutionConnection.id == execution_connection_id
        )

        if eager_load:
            query = query.options(
                selectinload(ExecutionConnection.workflow_action).selectinload(
                    WorkflowAction.dynamic_outputs
                ),
                selectinload(ExecutionConnection.workflow_action).selectinload(
                    WorkflowAction.static_inputs
                ),
                selectinload(ExecutionConnection.workflow_action).selectinload(
                    WorkflowAction.reference_inputs
                ),
                selectinload(ExecutionConnection.workflow_action)
                .selectinload(WorkflowAction.definition)
                .selectinload(ActionDefinition.inputs),
                selectinload(ExecutionConnection.workflow_action)
                .selectinload(WorkflowAction.definition)
                .selectinload(ActionDefinition.addons_inputs),
                selectinload(ExecutionConnection.workflow_action)
                .selectinload(WorkflowAction.definition)
                .selectinload(ActionDefinition.outputs),
            )
        try:
            return self.session.execute(query).scalar_one()
        except NoResultFound:
            raise ExecutionConnectionNotFoundError()

    def list_eventually_running(
        self,
    ) -> list[ExecutionConnection]:
        return self.session.scalars(
            select(ExecutionConnection).where(
                ExecutionConnection.state.in_(  # type: ignore
                    ExecutionConnectionState.eventually_running_states()
                ),
            )
        ).all()

    def list_is_allocated(
        self,
    ) -> list[ExecutionConnection]:
        return self.session.scalars(
            select(ExecutionConnection).where(
                ExecutionConnection.state.in_(  # type: ignore
                    ExecutionConnectionState.is_allocated_states()
                ),
            )
        ).all()

    def list_from_action_deployment_id(
        self, action_deployment_id: str
    ) -> list[ExecutionConnection]:
        return (
            self.session.scalars(
                select(ExecutionConnection)
                .join(ExecutionConnection.action_deployment)
                .where(ActionDeployment.id == action_deployment_id)
            )
            .unique()
            .all()
        )

    def list_active_from_workflow_action_id(
        self, workflow_action_id: str
    ) -> list[ExecutionConnection]:
        return (
            self.session.scalars(
                select(ExecutionConnection)
                .join(ExecutionConnection.workflow_action)
                .where(WorkflowAction.id == workflow_action_id)
                .where(
                    ExecutionConnection.state.in_(  # type: ignore
                        ExecutionConnectionState.eventually_running_states()
                    ),
                )
            )
            .unique()
            .all()
        )

    def list_allocated_running(
        self,
    ) -> list[ExecutionConnection]:
        return self.session.scalars(
            select(ExecutionConnection).where(
                ExecutionConnection.state.in_(  # type: ignore
                    [
                        ExecutionConnectionState.ALLOCATED,
                        ExecutionConnectionState.RUNNING,
                    ]
                )
            )
        ).all()

    def delete(self, execution_id: str) -> None:
        try:
            execution = self.session.get(ExecutionConnection, execution_id)
            self.session.delete(execution)
        except NoResultFound:
            raise ExecutionConnectionNotFoundError()

    def list_pending(self) -> list[PendingExecutionConnection]:
        pending_list = []
        try:
            results = self.session.scalars(
                select(ExecutionConnection)
                .join(WorkflowAction)
                .options(
                    selectinload(ExecutionConnection.workflow_action)
                    .selectinload(WorkflowAction.definition)
                    .load_only(
                        ActionDefinition.id,
                        ActionDefinition.kind,
                        ActionDefinition.container_image,
                    )
                )
                .options(
                    selectinload(ExecutionConnection.workflow_action).load_only(
                        WorkflowAction.id,
                    )
                )
                .options(
                    selectinload(ExecutionConnection.workflow_action).selectinload(
                        WorkflowAction.constraints
                    )
                )
                .options(
                    selectinload(ExecutionConnection.workflow_action).selectinload(
                        WorkflowAction.objectives
                    )
                )
                .options(
                    selectinload(ExecutionConnection.workflow_action).selectinload(
                        WorkflowAction.static_inputs
                    )
                )
                .options(
                    selectinload(ExecutionConnection.workflow_action).selectinload(
                        WorkflowAction.reference_inputs
                    )
                )
                .filter(ExecutionConnection.state == ExecutionConnectionState.PENDING)
                .order_by(ExecutionConnection.submitted_at)
            ).unique()
            for execution_connection in results.all():
                pending_list.append(
                    PendingExecutionConnection(
                        id=execution_connection.id,
                        project_id=execution_connection.project_id,
                        action_definition_id=execution_connection.workflow_action.definition.id,
                        is_a_trigger=execution_connection.workflow_action.definition.is_a_trigger(),
                        submitted_at=execution_connection.submitted_at,
                        action_container_image=execution_connection.workflow_action.definition.container_image,
                        site_list=(
                            execution_connection.workflow_action.constraints.site_list
                            if execution_connection.workflow_action.constraints
                            else []
                        ),
                        site_type_list=(
                            execution_connection.workflow_action.constraints.site_type_list
                            if execution_connection.workflow_action.constraints
                            else []
                        ),
                        arch_list=(
                            execution_connection.workflow_action.constraints.arch_list
                            if execution_connection.workflow_action.constraints
                            else []
                        ),
                        node_pool_list=(
                            execution_connection.workflow_action.constraints.node_pool_list
                            if execution_connection.workflow_action.constraints
                            else []
                        ),
                        resources=execution_connection.workflow_action.definition.resources,
                        objectives={
                            Objectives.COST: execution_connection.workflow_action.objectives.cost,
                            Objectives.ENERGY: execution_connection.workflow_action.objectives.energy,
                            Objectives.PERFORMANCE: execution_connection.workflow_action.objectives.performance,
                        }
                        if execution_connection.workflow_action.objectives is not None
                        else {
                            Objectives.COST: 0,
                            Objectives.ENERGY: 0,
                            Objectives.PERFORMANCE: 0,
                        },
                        node_pool_objective_score=execution_connection.workflow_action.objectives.node_pool_objective_score
                        if execution_connection.workflow_action.objectives is not None
                        else None,
                    )
                )
        except NoResultFound:
            raise ExecutionConnectionNotFoundError()
        return pending_list

    def list_log_delimiters_by_action_deployment(
        self, action_deployment: ActionDeployment
    ) -> list[str]:
        end_of_log_delimiters: list = []
        for end_of_log_delimiter_queue in self.session.scalars(
            select(ExecutionConnection.end_of_log_delimiter_queue).where(
                ExecutionConnection.action_deployment == action_deployment
            )
        ):
            end_of_log_delimiters.extend(end_of_log_delimiter_queue)
        return end_of_log_delimiters
