# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from sqlalchemy.exc import NoResultFound

from ryax.runner.domain.stored_file.storage_exceptions import StorageFileNotFoundError
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.stored_file.stored_file_reporitory import IStoredFileRepository
from ryax.common.infrastructure.database.engine import Session


@dataclass
class DatabaseStoredFileRepository(IStoredFileRepository):
    session: Session

    def add(self, stored_file: StoredFile) -> None:
        self.session.add(stored_file)

    def get(self, stored_file_id: str, project_id: str) -> StoredFile:
        try:
            stored_file = (
                self.session.query(StoredFile).filter(
                    StoredFile.id == stored_file_id, StoredFile.project_id == project_id
                )
            ).one()
            return stored_file
        except NoResultFound:
            raise StorageFileNotFoundError()

    def get_by_path(self, stored_file_path: str, project_id: str) -> StoredFile:
        try:
            stored_file = (
                self.session.query(StoredFile).filter(
                    StoredFile.file_path == stored_file_path,
                    StoredFile.project_id == project_id,
                )
            ).one()
            return stored_file
        except NoResultFound:
            raise StorageFileNotFoundError()
