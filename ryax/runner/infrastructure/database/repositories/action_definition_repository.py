# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from sqlalchemy import select, and_
from sqlalchemy.exc import NoResultFound

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_errors import (
    ActionDefinitionNotFoundError,
)
from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)
from ryax.common.infrastructure.database.engine import Session


@dataclass
class DatabaseActionDefinitionRepository(IActionDefinitionRepository):
    """Database repository for action deployment"""

    session: Session

    def get(self, action_definition_id: str) -> ActionDefinition:
        action_definition = self.session.get(ActionDefinition, action_definition_id)
        if not action_definition:
            raise ActionDefinitionNotFoundError
        return action_definition

    def get_by_technical_name_and_version(
        self, technical_name: str, version: str, project_id: str
    ) -> ActionDefinition:
        try:
            return self.session.execute(
                select(ActionDefinition).where(
                    and_(
                        ActionDefinition.technical_name == technical_name,
                        ActionDefinition.project_id == project_id,
                        ActionDefinition.version == version,
                    )
                )
            ).scalar_one()
        except NoResultFound:
            raise ActionDefinitionNotFoundError()

    def add(self, action_definition: ActionDefinition) -> None:
        self.session.add(action_definition)

    def delete(self, action_definition: ActionDefinition) -> None:
        self.session.delete(action_definition)
