from dataclasses import dataclass

from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import joinedload, selectinload

from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.runner.domain.site.site_repository import ISiteRepository
from ryax.runner.domain.site.site_execeptions import (
    SiteNotFoundError,
    NodePoolNotFoundError,
)
from ryax.common.infrastructure.database.engine import Session


@dataclass
class DatabaseSiteRepository(ISiteRepository):
    session: Session

    def add(self, site: Site) -> None:
        self.session.add(site)

    def get(self, site_id: str) -> Site:
        try:
            return (
                self.session.execute(
                    select(Site).filter(
                        Site.id == site_id,
                    )
                )
                .unique()
                .scalar_one()
            )
        except NoResultFound:
            raise SiteNotFoundError()

    def list(self) -> list[Site]:
        return (
            self.session.execute(select(Site).options(joinedload(Site.node_pools)))
            .unique()
            .scalars()
            .all()
        )

    def get_node_pool(self, node_pool_id: str) -> NodePool:
        try:
            return (
                self.session.execute(
                    select(NodePool)
                    .where(NodePool.id == node_pool_id)
                    .options(selectinload(NodePool.site))
                )
                .unique()
                .scalar_one()
            )
        except NoResultFound:
            raise NodePoolNotFoundError()
