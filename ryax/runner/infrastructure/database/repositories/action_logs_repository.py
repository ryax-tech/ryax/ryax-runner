from dataclasses import dataclass

from sqlalchemy import select, delete
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session

from ryax.runner.domain.action_logs.action_logs_entities import (
    ActionLogs,
    LogsNotFoundError,
    LogLine,
)
from ryax.runner.domain.action_logs.action_logs_repository import IActionLogsRepository


@dataclass
class DatabaseActionLogsRepository(IActionLogsRepository):
    session: Session

    def add(self, logs: ActionLogs) -> None:
        self.session.add(logs)

    def get(self, associated_id: str) -> ActionLogs:
        try:
            return (
                (
                    self.session.execute(
                        select(ActionLogs).where(
                            ActionLogs.associated_id == associated_id,  # type: ignore
                        )
                    )
                )
                .scalars()
                .one()
            )
        except NoResultFound:
            raise LogsNotFoundError()

    def reset(self, associated_id: str) -> None:
        self.session.execute(
            delete(LogLine).where(
                LogLine.logs_id == associated_id,  # type: ignore
            )
        )

    def get_logs_as_string(self, associated_id: str) -> str:
        log_lines = (
            self.session.execute(
                select(LogLine).where(
                    LogLine.logs_id == associated_id,  # type: ignore
                )
            )
            .scalars()
            .all()
        )
        return "".join([log.line for log in log_lines])
