from dataclasses import dataclass

from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from ryax.runner.domain.recommendation_vpa.recommendation_vpa_entities import (
    RecommendationVPA,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_repository import (
    IRecommendationVPARepository,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_exceptions import (
    RecommendationVPANotFoundError,
)
from ryax.common.infrastructure.database.engine import Session


@dataclass
class DatabaseRecommendationVPARepository(IRecommendationVPARepository):
    session: Session

    def add(self, recommendation: RecommendationVPA) -> None:
        self.session.add(recommendation)

    def get(self, key: str) -> RecommendationVPA:
        try:
            return (
                self.session.execute(
                    select(RecommendationVPA).filter(
                        RecommendationVPA.key == key,
                    )
                )
                .unique()
                .scalar_one()
            )
        except NoResultFound:
            raise RecommendationVPANotFoundError()

    def list(self) -> list[RecommendationVPA]:
        return self.session.execute(select(RecommendationVPA)).unique().scalars().all()
