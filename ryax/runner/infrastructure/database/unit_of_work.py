# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import traceback
from dataclasses import dataclass
from types import TracebackType
from typing import Callable, Type

from ryax.runner.domain.accounting.accounting_repository import IAccountingRepository
from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    IActionDeploymentRepository,
)
from ryax.runner.domain.action_logs.action_logs_repository import IActionLogsRepository
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_connection.execution_connection_repository import (
    IExecutionConnectionRepository,
)
from ryax.runner.domain.execution_result.execution_result_repository import (
    IExecutionResultRepository,
)
from ryax.runner.domain.site.site_repository import ISiteRepository
from ryax.runner.domain.stored_file.stored_file_reporitory import IStoredFileRepository
from ryax.runner.domain.user_auth.api_key.api_key_repository import (
    IUserApiKeyRepository,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_repository import (
    IWorkflowDeploymentRepository,
)
from ryax.runner.domain.workflow_run.workflow_run_repository import (
    IWorkflowRunRepository,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_repository import (
    IRecommendationVPARepository,
)
from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine

logger = logging.getLogger(__name__)


@dataclass
class DatabaseUnitOfWork(IUnitOfWork):
    engine: SqlalchemyDatabaseEngine
    action_deployment_repository_factory: Callable[..., IActionDeploymentRepository]
    action_definition_repository_factory: Callable[..., IActionDefinitionRepository]
    execution_connection_repository_factory: Callable[
        ..., IExecutionConnectionRepository
    ]
    workflow_deployment_repository_factory: Callable[..., IWorkflowDeploymentRepository]
    execution_result_repository_factory: Callable[..., IExecutionResultRepository]
    stored_file_repository_factory: Callable[..., IStoredFileRepository]
    user_api_key_repository_factory: Callable[..., IUserApiKeyRepository]
    workflow_run_repository_factory: Callable[..., IWorkflowRunRepository]
    action_logs_repository_factory: Callable[..., IActionLogsRepository]
    site_repository_factory: Callable[..., ISiteRepository]
    recommendation_vpa_repository_factory: Callable[..., IRecommendationVPARepository]
    accounting_repository_factory: Callable[..., IAccountingRepository]

    def __enter__(self) -> IUnitOfWork:
        self.session = self.engine.get_session()
        self.action_deployment_repository = self.action_deployment_repository_factory(
            session=self.session
        )
        self.action_definition_repository = self.action_definition_repository_factory(
            session=self.session
        )
        self.workflow_repository = self.workflow_deployment_repository_factory(
            session=self.session
        )
        self.execution_connection_repository = (
            self.execution_connection_repository_factory(session=self.session)
        )
        self.execution_result_repository = self.execution_result_repository_factory(
            session=self.session
        )
        self.stored_file_repository = self.stored_file_repository_factory(
            session=self.session
        )
        self.user_api_key_repository = self.user_api_key_repository_factory(
            session=self.session
        )
        self.workflow_run_repository = self.workflow_run_repository_factory(
            session=self.session
        )
        self.action_logs_repository = self.action_logs_repository_factory(
            session=self.session
        )
        self.site_repository = self.site_repository_factory(session=self.session)
        self.recommendation_vpa_repository = self.recommendation_vpa_repository_factory(
            session=self.session
        )
        self.accounting_repository = self.accounting_repository_factory(
            session=self.session
        )

        return self

    def __exit__(
        self,
        exc_type: Type[Exception] | None = None,
        exc_val: Exception | None = None,
        exc_tb: TracebackType | None = None,
    ) -> None:
        super().__exit__(exc_type, exc_val, exc_tb)
        if exc_type is not None:
            logger.error(
                "Error during the Unit of Work context: %s",
                "".join(traceback.format_exception(exc_type, value=exc_val, tb=exc_tb)),
            )
        self.session.close()

    def commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()
