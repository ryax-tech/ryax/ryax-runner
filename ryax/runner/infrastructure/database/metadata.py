# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import datetime
from typing import Any, Optional

from sqlalchemy import (
    JSON,
    BigInteger,
    Boolean,
    Column,
    DateTime,
    Enum,
    Float,
    ForeignKey,
    Integer,
    LargeBinary,
    MetaData,
    PickleType,
    String,
    Table,
    TypeDecorator,
    Text,
)
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy_utils import ScalarListType

from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
)
from ryax.common.infrastructure.database.scalarEnumListType import ScalarEnumListType
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
    DynamicOutputOrigin,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.user_auth.user_auth_entities import UserAuthorizationMethod
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import (
    ActionRunState,
    WorkflowRunState,
)


class TZDateTime(TypeDecorator):
    """
    A DateTime type which can only store and returns tz-aware DateTimes.
    """

    cache_ok = True

    impl = DateTime(timezone=True)

    def process_bind_param(self, value: Any, _: Any) -> Any:
        if isinstance(value, datetime.datetime) and value.tzinfo is None:
            raise ValueError("{!r} must be TZ-aware".format(value))
        return value

    def process_result_value(
        self, value: Optional[datetime.datetime], _: Any
    ) -> Optional[datetime.datetime]:
        if value is None:
            return value
        return value.replace(tzinfo=datetime.timezone.utc)

    def __repr__(self) -> str:
        return "TZDateTime()"


metadata = MetaData()

# Action Deployments Tables
action_kind_enum = Enum(ActionKind, name="action_kind_enum")

action_io_type = Enum(ActionIOType, name="action_io_type")

action_deployment_state = Enum(
    ActionDeploymentState,
    name="action_deployment_state",
)

action_execution_type = Enum(
    ActionExecutionType,
    name="action_execution_type",
)

dynamic_output_origin = Enum(
    DynamicOutputOrigin,
    name="dynamic_output_origin",
)

action_definition_table: Table = Table(
    "action_definition",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("project_id", String, nullable=False),
    Column("technical_name", String, nullable=False),
    Column("version", String, nullable=False),
    Column("kind", action_kind_enum, nullable=False),
    Column("container_image", String, nullable=False),
    Column("human_name", String, nullable=False),
    Column("description", String, nullable=False),
    Column("has_dynamic_outputs", Boolean, nullable=False),
    Column(
        "resources_id",
        ForeignKey("action_resources.id"),
        nullable=True,
    ),
)

action_deployment_table: Table = Table(
    "action_deployment",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("project_id", String, nullable=False),
    Column("action_definition_id", String, ForeignKey("action_definition.id")),
    Column("node_pool_id", String, ForeignKey("node_pools.id")),
    Column("execution_type", action_execution_type),
    Column("state", action_deployment_state, nullable=False),
    Column("action_internal_endpoint", String),
    Column("addons", JSON),
    Column("allocated_connection_id", String),
    Column("error_message", String),
    Column("last_allocation_date", TZDateTime(), nullable=False),
)

action_input_types_table: Table = Table(
    "action_input_types",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column(
        "action_definition_id",
        String,
        ForeignKey("action_definition.id"),
        nullable=False,
    ),
    Column("type", action_io_type, nullable=False),
    Column("display_name", String, nullable=False),
    Column("help", String, nullable=False),
    Column("enum_values", ScalarListType(str), nullable=False),
    Column("optional", Boolean, nullable=False),
    Column("addon_name", String, nullable=True),
)

action_output_types_table: Table = Table(
    "action_output_types",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column(
        "action_definition_id",
        String,
        ForeignKey("action_definition.id"),
        nullable=False,
    ),
    Column("type", action_io_type, nullable=False),
    Column("display_name", String, nullable=False),
    Column("help", String, nullable=False),
    Column("enum_values", ScalarListType(str), nullable=False),
    Column("optional", Boolean, nullable=False),
)

# Workflow tables
workflow_deployment_state_type = Enum(
    WorkflowDeploymentState,
    name="workflow_deployment_state_type",
)

workflow_deployment_table: Table = Table(
    "workflow_deployment",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("project_id", String, nullable=False),
    Column("creation_time", String, nullable=False),
    Column("workflow_definition_id", String),
    Column("state", workflow_deployment_state_type, nullable=False),
    Column("stopped_time", String),
)

workflow_action_reference_inputs_table: Table = Table(
    "workflow_action_reference_inputs",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("action_name", String, nullable=True),
    Column("output_name", String, nullable=True),
    Column(
        "parent_action_id",
        String,
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    ),
)

workflow_action_static_inputs_table: Table = Table(
    "workflow_action_static_inputs",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("value", PickleType, nullable=True),
    Column(
        "workflow_action_id",
        String,
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=False,
    ),
)

workflow_action_dynamic_outputs_table: Table = Table(
    "workflow_action_dynamic_outputs",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column(
        "workflow_action_id",
        String,
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column("type", action_io_type, nullable=False),
    Column("display_name", String, nullable=False),
    Column("help", String, nullable=False),
    Column("enum_values", ScalarListType(str), nullable=False),
    Column("origin", dynamic_output_origin, nullable=False),
    Column("optional", Boolean, nullable=False),
)

workflow_action_association_table: Table = Table(
    "workflow_action_association",
    metadata,
    Column("parents", String, ForeignKey("workflow_action.id", ondelete="CASCADE")),
    Column("children", String, ForeignKey("workflow_action.id", ondelete="CASCADE")),
)

SiteTypeEnum = Enum(SiteType, name="site_type")
NodeArchitectureEnum = Enum(NodeArchitecture, name="node_architecture")

scheduling_constraints_table = Table(
    "scheduling_constraints",
    metadata,
    Column(
        "workflow_action_id", String, ForeignKey("workflow_action.id"), primary_key=True
    ),
    Column("site_type_list", ScalarEnumListType(SiteType), nullable=False),
    Column("arch_list", ScalarEnumListType(NodeArchitecture), nullable=False),
)
scheduling_constraints_sites_association_table: Table = Table(
    "site_constraints_association",
    metadata,
    Column("site", String, ForeignKey("sites.id")),
    Column(
        "constraint", String, ForeignKey("scheduling_constraints.workflow_action_id")
    ),
)
scheduling_constraints_node_pools_association_table: Table = Table(
    "nodepool_constraints_association",
    metadata,
    Column("node_pool", String, ForeignKey("node_pools.id")),
    Column(
        "constraint", String, ForeignKey("scheduling_constraints.workflow_action_id")
    ),
)

scheduling_objectives_table = Table(
    "scheduling_objectives",
    metadata,
    Column("energy", Float),
    Column("performance", Float),
    Column("cost", Float),
    Column(
        "workflow_action_id", String, ForeignKey("workflow_action.id"), primary_key=True
    ),
    Column("objective_score_id", String, ForeignKey("objective_score.id")),
)

workflow_action_table: Table = Table(
    "workflow_action",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("id_in_studio", String, nullable=False),
    Column("display_name", String),
    Column(
        "action_definition_id",
        String,
        ForeignKey("action_definition.id"),
        nullable=False,
        index=True,
    ),
    Column("workflow_definition_id", String),
    Column("connection_execution_id", String),
    Column("addons", JSON),
    Column("is_root_action", Boolean),
    Column("position", Integer),
    Column(
        "workflow_deployment_id",
        String,
        ForeignKey("workflow_deployment.id"),
        nullable=True,
        index=True,
    ),
)

workflow_result_table: Table = Table(
    "workflow_result",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("key", String, nullable=False),
    Column("workflow_action_io_id", String, nullable=False),
    Column(
        "workflow_deployment_id",
        String,
        ForeignKey("workflow_deployment.id"),
        nullable=False,
    ),
    Column("technical_name", String, nullable=False),
    Column("action_id", String, nullable=False),
)

execution_result_state = Enum(
    ExecutionResultState,
    name="execution_result_state",
)

execution_result_association_table: Table = Table(
    "execution_result_association",
    metadata,
    Column(
        "parents",
        String,
        ForeignKey("execution_result.id", ondelete="CASCADE"),
        index=True,
    ),
    Column(
        "children",
        String,
        ForeignKey("execution_result.id", ondelete="CASCADE"),
        index=True,
    ),
)

execution_result_table: Table = Table(
    "execution_result",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("project_id", String, nullable=False),
    Column("workflow_run_id", String),
    Column("execution_connection_id", String),
    Column(
        "workflow_action_id",
        String,
        ForeignKey("workflow_action.id"),
    ),
    Column("action_run_id", ForeignKey("action_run.id"), index=True),
    Column("error_message", String),
    Column("state", execution_result_state, nullable=False),
    Column("submitted_at", TZDateTime(), nullable=True),
    Column("started_at", TZDateTime(), nullable=True),
    Column("ended_at", TZDateTime(), nullable=True),
    Column("outputs", JSON),
    Column("inputs", JSON),
    Column(
        "end_of_log_delimiter",
        String,
        nullable=True,
        index=True,
    ),
    Column("http_status_code", Integer, nullable=True),
)

accounting_resources_table: Table = Table(
    "accounting_resources",
    metadata,
    Column("requested_cpu", Float, nullable=True),
    Column("requested_memory", BigInteger, nullable=True),
    Column("requested_time", Integer, nullable=True),
    Column("requested_gpu", Integer, nullable=True),
    Column("instance_type", String),
    Column("project_id", String),
    Column("workflow_definition_id", String),
    Column("action_definition_id", String),
    Column("workflow_run_id", String),
    Column("execution_connection_id", String, primary_key=True),
    Column("submitted_at", TZDateTime(), nullable=True),
    Column("started_at", TZDateTime(), nullable=True),
    Column("ended_at", TZDateTime(), nullable=True),
)

execution_connection_state = Enum(
    ExecutionConnectionState,
    name="execution_connection_state",
)

execution_connection_table: Table = Table(
    "execution_connection",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("project_id", String, nullable=False),
    Column(
        "action_deployment_id",
        String,
        ForeignKey("action_deployment.id"),
        nullable=True,
        index=True,
    ),
    Column(
        "workflow_action_id",
        String,
        ForeignKey("workflow_action.id"),
        nullable=True,
        index=True,
    ),
    Column("inputs", JSON),
    Column("workflow_run_id", String),
    Column("state", execution_connection_state, nullable=False),
    Column("error_message", String, nullable=False),
    Column("time_allotment", Float, nullable=False),
    Column("executor_id", String),
    Column("submitted_at", TZDateTime(), nullable=True),
    Column("started_at", TZDateTime(), nullable=True),
    Column("ended_at", TZDateTime(), nullable=True),
    Column(
        "parent_execution_result_id",
        String,
        ForeignKey("execution_result.id"),
        nullable=True,
        index=True,
    ),
    Column("end_of_log_delimiter_queue", MutableList.as_mutable(ScalarListType(str))),
)

stored_file_table: Table = Table(
    "stored_file",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("io_name", String, nullable=False),
    Column("file_path", String, nullable=False),
    Column("project_id", String, nullable=False),
    Column("mimetype", String),
    Column("encoding", String),
    Column("size_in_bytes", BigInteger),
)

stored_file_association = Table(
    "stored_file_association",
    metadata,
    Column("stored_file_id", String, ForeignKey("stored_file.id", ondelete="CASCADE")),
    Column(
        "execution_result_id",
        String,
        ForeignKey("execution_result.id", ondelete="CASCADE"),
    ),
)

user_api_key_table = Table(
    "user_api_key_table",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("api_key", LargeBinary, nullable=False),
    Column("project_id", String, nullable=False),
    Column("revoked", Boolean, nullable=False),
    Column("creation_date", TZDateTime(), nullable=False),
    Column("last_used_date", TZDateTime(), nullable=True),
    Column("expiration_date", TZDateTime(), nullable=False),
)

user_authorization_method = Enum(
    UserAuthorizationMethod, name="user_authorization_method"
)

project_security_requirements_table = Table(
    "project_security_requirements_table",
    metadata,
    Column("project_id", String, nullable=False, primary_key=True),
    Column(
        "authorization_method",
        user_authorization_method,
        nullable=False,
        primary_key=True,
    ),
    Column("enabled", Boolean, nullable=False),
)

action_resources_table = Table(
    "action_resources",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("cpu", Float, nullable=True),
    Column("memory", BigInteger, nullable=True),
    Column("time", Float, nullable=True),
    Column("gpu", Integer, nullable=True),
)

action_run_state = Enum(
    ActionRunState,
    name="action_run_state",
)
action_run_table = Table(
    "action_run",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("state", action_run_state, nullable=False),
    Column("position", Integer, nullable=False),
    Column("error_message", String, nullable=False),
    Column(
        "workflow_run_id", ForeignKey("workflow_run.id", ondelete="CASCADE"), index=True
    ),
    Column("workflow_action_id", ForeignKey("workflow_action.id")),
    Column("exceed_retry_error", Boolean, nullable=False),
    Column("used_retry_error", Integer, nullable=False),
    Column("exceed_retry_memory_bytes", Boolean, nullable=False),
    Column("used_retry_memory_times", Integer, nullable=False),
    Column("exceed_retry_gpu_gi", Boolean, nullable=False),
    Column("used_retry_gpu_gi_times", Integer, nullable=False),
)

workflow_run_state = Enum(
    WorkflowRunState,
    name="workflow_run_state",
)

workflow_run_table = Table(
    "workflow_run",
    metadata,
    Column("id", String, primary_key=True),
    Column("project_id", String),
    Column("workflow_definition_id", String),
    Column("state", workflow_run_state),
    Column("number_of_action_results", Integer, key="number_of_completed_actions"),
    Column("number_of_actions", Integer),
    Column("started_at", TZDateTime()),
    Column("last_result_at", TZDateTime(), nullable=True),
)

action_logs_table = Table(
    "logs",
    metadata,
    # Not set as a foreign key because it can be set to either ExecutionResult or ActionDeployment.
    # Use it as primary key instead
    Column("associated_id", String, primary_key=True),
    Column("project_id", String, nullable=False),
    Column("logs_size_in_bytes", Integer),
    Column("index", Integer),
)
action_log_lines_table = Table(
    "log_lines",
    metadata,
    Column("id", String, primary_key=True),
    Column("index", Integer),
    Column("line", Text),
    Column("size_in_bytes", Integer),
    Column(
        "logs_id",
        String,
        ForeignKey("logs.associated_id", ondelete="CASCADE"),
        index=True,
    ),
)
objective_scores_table = Table(
    "objective_score",
    metadata,
    Column("id", String, primary_key=True),
    Column("energy", Integer),
    Column("performance", Integer),
    Column("cost", Integer),
)
node_pool_table = Table(
    "node_pools",
    metadata,
    Column("id", String, primary_key=True),
    Column("name", String, nullable=False),
    Column("cpu", Float, nullable=False),
    Column("memory", BigInteger, nullable=False),
    Column("maximum_time_in_sec", Float),
    Column("gpu", Integer),
    Column("instance_type", String),
    Column("architecture", NodeArchitectureEnum, nullable=False),
    Column("site_id", String, ForeignKey("sites.id")),
    Column("objective_score_id", String, ForeignKey("objective_score.id")),
    Column("gpu_mode", String, nullable=False),
    Column("gpu_num_total", Integer, nullable=False),
    Column("gpu_free_total", Integer, nullable=False),
    Column("filter_no_gpu_action", Boolean),
)
site_table = Table(
    "sites",
    metadata,
    Column("id", String, primary_key=True),
    Column("name", String, nullable=False, unique=True),
    Column("type", SiteTypeEnum),
)
recommendation_vpa_table = Table(
    "recommendation_vpa",
    metadata,
    Column("key", String, primary_key=True),
    Column("timestamp", TZDateTime(), nullable=False),
    # Column("cpu", Float, nullable=True),
    Column("memory", BigInteger, nullable=True),
    Column("gpu_mig_instance", String, nullable=True),
    Column("last_oom_timestamp", TZDateTime(), nullable=False),
    Column("last_gpuoom_timestamp", TZDateTime(), nullable=False),
)
