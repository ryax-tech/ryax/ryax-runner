# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from cryptography.fernet import Fernet

from ryax.runner.domain.encryption.encryption_service import IEncryptionService


class EncryptionService(IEncryptionService):
    def __init__(self, encryption_key: bytes):
        self.symmetric_encryption = Fernet(encryption_key)

    def encrypt(self, plaintext: str) -> bytes:
        if plaintext == "":
            return b""
        return self.symmetric_encryption.encrypt(plaintext.encode())

    def decrypt(self, ciphertext: bytes) -> str:
        decrypted_password: bytes = self.symmetric_encryption.decrypt(ciphertext)
        return decrypted_password.decode()
