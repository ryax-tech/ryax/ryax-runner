# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from ryax.common.infrastructure.messaging.messages.worker_messages_pb2 import (
    ActionDeploymentUpdated,
    TaskNewLogLines,
    ActionDeploymentNewLogLines,
    InitTaskRequest,
    ExecutionEnded,
    ActionDeploymentCurrentState,
    TaskCompleted,
    Register,
    RecommendationsVPA,
    GetExecutionStateReply,
)
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    DeployActionDeploymentCommand,
    GetActionDeploymentStateCommand,
    DeleteActionDeploymentCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    StopExecutionConnectionCommand,
    GetExecutionConnectionStateCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_events import (
    WorkflowDeployedSuccessfully,
    WorkflowUndeployedSuccessfully,
)
from ryax.common.infrastructure.messaging.messages.studio_messages_pb2 import (
    WorkflowDeleted,
    WorkflowUndeploy,
    WorkflowDeploy,
)
from ryax.runner.infrastructure.messaging.handlers import (
    workflow_rmq_handler,
    action_deployment_rmq_handler,
    execution_rmq_handler,
    site_rmq_handler,
)


def setup(
    container: ApplicationContainer,
) -> None:
    """Method to setup messaging mapper (event type/messages mapping)"""
    container.wire(
        modules=[
            workflow_rmq_handler,
            action_deployment_rmq_handler,
            execution_rmq_handler,
            site_rmq_handler,
        ]
    )

    # Consumer
    messaging_consumer = container.message_consumer()
    # Workflow
    messaging_consumer.register_handler(
        WorkflowDeploy, workflow_rmq_handler.on_deploy_workflow, "Studio"
    )
    messaging_consumer.register_handler(
        WorkflowUndeploy, workflow_rmq_handler.on_undeploy_workflow, "Studio"
    )
    messaging_consumer.register_handler(
        WorkflowDeleted, workflow_rmq_handler.on_workflow_deleted, "Studio"
    )
    # Action Deployment
    messaging_consumer.register_handler(
        ActionDeploymentUpdated,
        action_deployment_rmq_handler.on_action_deployment_updated,
        "Worker",
    )
    messaging_consumer.register_handler(
        ActionDeploymentNewLogLines,
        action_deployment_rmq_handler.on_action_new_log_lines,
        "Worker",
    )
    # Execution Connections
    messaging_consumer.register_handler(
        InitTaskRequest, execution_rmq_handler.on_init_execution_connection, "Worker"
    )
    messaging_consumer.register_handler(
        TaskNewLogLines,
        execution_rmq_handler.on_execution_connection_new_log_lines,
        "Worker",
    )
    messaging_consumer.register_handler(
        TaskCompleted,
        execution_rmq_handler.on_execution_result_completed,
        "Worker",
    )
    messaging_consumer.register_handler(
        ExecutionEnded, execution_rmq_handler.on_execution_connection_ended, "Worker"
    )
    # Site
    messaging_consumer.register_handler(
        Register, site_rmq_handler.on_worker_register, "Worker"
    )
    # Recommendation
    messaging_consumer.register_handler(
        RecommendationsVPA,
        action_deployment_rmq_handler.on_worker_recommendation_vpa_update,
        "Worker",
    )

    # Publisher
    messaging_publisher = container.message_publisher()
    # Workflow
    messaging_publisher.register_handler(
        WorkflowUndeployedSuccessfully,
        workflow_rmq_handler.on_workflow_undeployed_successfully,
    )
    messaging_publisher.register_handler(
        WorkflowDeployedSuccessfully,
        workflow_rmq_handler.on_workflow_deployed_successfully,
    )
    # Deployment
    messaging_publisher.register_handler(
        DeployActionDeploymentCommand, action_deployment_rmq_handler.on_deploy_action
    )
    messaging_publisher.register_handler(
        DeleteActionDeploymentCommand, action_deployment_rmq_handler.on_undeploy_action
    )
    # Execution
    messaging_publisher.register_handler(
        ExecutionConnection, execution_rmq_handler.on_start_execution
    )
    messaging_publisher.register_handler(
        StopExecutionConnectionCommand, execution_rmq_handler.on_stop_execution
    )

    # Querier
    message_querier = container.message_querier()

    message_querier.register_request_handler(
        GetActionDeploymentStateCommand,
        action_deployment_rmq_handler.on_get_action_deployment_state,
        "Runner",
    )
    message_querier.register_reply_handler(
        ActionDeploymentCurrentState,
        action_deployment_rmq_handler.on_current_action_deployment_state,
    )

    message_querier.register_request_handler(
        GetExecutionConnectionStateCommand,
        execution_rmq_handler.on_get_execution_state,
        "Runner",
    )
    message_querier.register_reply_handler(
        GetExecutionStateReply,
        execution_rmq_handler.on_received_execution_state,
    )
