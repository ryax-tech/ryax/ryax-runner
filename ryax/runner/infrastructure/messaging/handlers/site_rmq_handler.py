from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide

from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.domain.registration.registration_values import (
    SiteType,
    ObjectiveScores,
    NodeArchitecture,
)
from ryax.common.infrastructure.messaging.message_handler import ProtobufMessage
from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    RegisterReply,
)
from ryax.common.infrastructure.messaging.messages.worker_messages_pb2 import Register
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.site.site_entities import Site
from ryax.runner.domain.site.site_events import RegisterSiteCommand
from ryax.runner.domain.site.site_execeptions import DuplicatedSiteNameError


async def on_worker_register(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> ProtobufMessage:
    message_content = Register()
    message_content.ParseFromString(message.body)

    site_id = message_content.site_id
    if not site_id:
        site_id = Site.new_id()
    site = RegisterSiteCommand.Site(
        id=site_id,
        name=message_content.site_name,
        type=SiteType[Register.SiteType.Name(message_content.type)],
        node_pools=[],
    )
    node_pools = [
        RegisterSiteCommand.NodePool(
            name=node_pool.name,
            memory=node_pool.memory,
            cpu=node_pool.cpu,
            gpu=node_pool.gpu,
            architecture=NodeArchitecture[node_pool.architecture],
            instance_type=node_pool.instance_type,
            maximum_time_in_sec=(
                node_pool.maximum_time_in_sec if node_pool.maximum_time_in_sec else None
            ),
            objective_scores=(
                ObjectiveScores(
                    id=ObjectiveScores.new_id(),
                    performance=node_pool.objective_scores.performance,
                    energy=node_pool.objective_scores.energy,
                    cost=node_pool.objective_scores.cost,
                )
                if node_pool.objective_scores
                else None
            ),
            gpu_mode=node_pool.gpu_mode,
            gpu_num_total=node_pool.gpu_num_total,
            filter_no_gpu_action=node_pool.filter_no_gpu_action,
        )
        for node_pool in message_content.node_pools
    ]
    site.node_pools = node_pools
    try:
        await message_bus.handle_command(RegisterSiteCommand(site))
        return RegisterReply(site_id=site_id)
    except DuplicatedSiteNameError as err:
        return RegisterReply(site_id=None, error_message=str(err))
