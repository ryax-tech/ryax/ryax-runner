# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
from typing import Coroutine, Iterable, List, cast

from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide
from google.protobuf.json_format import MessageToDict

from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
)
from ryax.common.infrastructure.messaging.publisher import ProtobufMessage
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_commands import (
    CreateActionDefinitionCommand,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDynamicOutput,
    ActionInput,
    ActionOutput,
    ActionResources,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
    DynamicOutputOrigin,
)
from ryax.common.domain.internal_messaging.base_messages import BaseEvent
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
    StopWorkflowDeploymentFromDefinitionIdCommand,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_events import (
    WorkflowDeployedSuccessfully,
    WorkflowUndeployedSuccessfully,
)
from ryax.runner.domain.workflow_run.workflow_run_commands import (
    DeleteWorkflowRunsCommand,
)
from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    WorkflowDeployed,
    WorkflowUndeployed,
)
from ryax.common.infrastructure.messaging.messages.studio_messages_pb2 import (
    WorkflowDeleted,
    WorkflowDeploy,
    WorkflowUndeploy,
)


class WorkflowDeploymentException(Exception):
    pass


def _create_action_outputs(
    outputs: Iterable[WorkflowDeploy.WorkflowActionIO],
) -> List[ActionOutput]:
    return [
        ActionOutput.create_from_data(
            name=output.technical_name,
            type=ActionIOType[
                WorkflowDeploy.WorkflowActionIO.IOType.Name(output.type).upper()
            ],
            display_name=output.display_name,
            help=output.help,
            enum_values=[e for e in output.enum_values],
            optional=output.optional,
        )
        for output in outputs
    ]


def _create_action_dynamic_outputs(
    outputs: Iterable[WorkflowDeploy.WorkflowActionIO],
) -> List[ActionDynamicOutput]:
    return [
        ActionDynamicOutput.create_from_data_with_origin(
            name=output.technical_name,
            type=ActionIOType[
                WorkflowDeploy.WorkflowActionIO.IOType.Name(output.type).upper()
            ],
            display_name=output.display_name,
            help=output.help,
            enum_values=[value for value in output.enum_values],
            origin=DynamicOutputOrigin[
                WorkflowDeploy.WorkflowActionIO.ORIGIN.Name(output.origin).upper()
            ],
            optional=output.optional,
        )
        for output in outputs
    ]


async def on_deploy_workflow(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
    registry_name: str = Provide[ApplicationContainer.configuration.ryax.registry],
) -> None:
    """Convert DeployWorkflow event to internal message"""
    message_content = WorkflowDeploy()
    message_content.ParseFromString(message.body)

    create_action_commands: List[CreateActionDefinitionCommand] = []
    deploy_action_commands: List[CreateWorkflowDeploymentCommand.Action] = []
    for action in message_content.actions:
        action_dict = MessageToDict(action, preserving_proto_field_name=True)

        create_action_commands.append(
            CreateActionDefinitionCommand(
                id=action.action_id,
                project_id=message_content.project_id,
                human_name=action.human_name,
                description=action.description,
                kind=ActionKind[WorkflowDeploy.WorkflowAction.Kind.Name(action.kind)],
                version=action.version,
                technical_name=action.technical_name,
                container_image=action.container_image
                if len(action.container_image) > 0
                else f"{registry_name}/{action.action_id}:{action.version}",
                inputs=[
                    ActionInput.create_from_data(
                        name=input.technical_name,
                        type=ActionIOType[
                            WorkflowDeploy.WorkflowActionIO.IOType.Name(
                                input.type
                            ).upper()
                        ],
                        display_name=input.display_name,
                        help=input.help,
                        enum_values=[value for value in input.enum_values],
                        optional=input.optional,
                    )
                    for input in action.inputs
                ],
                outputs=_create_action_outputs(action.outputs),
                has_dynamic_outputs=action.has_dynamic_outputs,
                addons_inputs=[
                    ActionInput.create_from_data(
                        name=input.technical_name,
                        type=ActionIOType[
                            WorkflowDeploy.WorkflowActionIO.IOType.Name(
                                input.type
                            ).upper()
                        ],
                        display_name=input.display_name,
                        help=input.help,
                        enum_values=[value for value in input.enum_values],
                        optional=input.optional,
                        addon_name=input.addon_name,
                    )
                    for input in action.addons_inputs
                ],
                resources=ActionResources.create_from_data(
                    cpu=(
                        action.resources.cpu
                        if action.resources.HasField("cpu")
                        else None
                    ),
                    time=(
                        action.resources.time
                        if action.resources.HasField("time")
                        else None
                    ),
                    memory=(
                        action.resources.memory
                        if action.resources.HasField("memory")
                        else None
                    ),
                    gpu=(
                        action.resources.gpu
                        if action.resources.HasField("gpu")
                        else None
                    ),
                ),
            )
        )
        deploy_action_commands.append(
            CreateWorkflowDeploymentCommand.Action(
                id_in_studio=action.workflow_action_id,
                display_name=action.human_name,
                action_definition_id=action.action_id,
                next_actions=list(action.next_actions),
                dynamic_outputs=_create_action_dynamic_outputs(action.dynamic_outputs),
                static_inputs=action_dict.get("static_values", {})
                | {
                    input_name: input_value
                    for input_name, input_value in action.file_values.items()
                },
                reference_inputs={
                    input_name: {
                        "action": input_value.workflow_action_id,
                        "output": input_value.output_technical_name,
                    }
                    for input_name, input_value in action.reference_values.items()
                },
                addons=action_dict.get("action_addons", {}),
                resources=CreateWorkflowDeploymentCommand.Action.ActionResources(
                    cpu=action.resources.cpu,
                    memory=action.resources.memory,
                    time=action.resources.time,
                    gpu=action.resources.gpu,
                ),
                objectives=CreateWorkflowDeploymentCommand.Action.ActionObjectives(
                    energy=action.objectives.energy,
                    cost=action.objectives.cost,
                    performance=action.objectives.performance,
                ),
                constraints=CreateWorkflowDeploymentCommand.Action.ActionConstraints(
                    site_list=[site_id for site_id in action.constraints.site_list],
                    node_pool_list=[
                        np_id for np_id in action.constraints.node_pool_list
                    ],
                    site_type_list=[
                        SiteType[site_type]
                        for site_type in action.constraints.site_type_list
                    ],
                    arch_list=[
                        NodeArchitecture[arch] for arch in action.constraints.arch_list
                    ],
                ),
            )
        )

    tasks: List[Coroutine] = []
    for create_command in create_action_commands:
        tasks.append(message_bus.handle_command(create_command))
    await asyncio.gather(*tasks)

    await message_bus.handle_command(
        CreateWorkflowDeploymentCommand(
            workflow_definition_id=message_content.id,
            project_id=message_content.project_id,
            workflow_name=message_content.name,
            results=[
                CreateWorkflowDeploymentCommand.Result(
                    key=result.key,
                    technical_name=result.technical_name,
                    workflow_action_io_id=result.workflow_action_io_id,
                    action_id=result.action_id,
                )
                for result in message_content.results
            ],
            actions=deploy_action_commands,
        )
    )


async def on_undeploy_workflow(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    """Convert DeployWorkflow event to internal message"""
    message_content = WorkflowUndeploy()
    message_content.ParseFromString(message.body)

    await message_bus.handle_command(
        StopWorkflowDeploymentFromDefinitionIdCommand(
            message_content.workflow_id, message_content.graceful
        )
    )


async def on_workflow_deleted(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    """
    When a workflow is deleted in studio, we delete all its workflow runs
    """
    message_content = WorkflowDeleted()
    message_content.ParseFromString(message.body)
    await message_bus.handle_command(
        DeleteWorkflowRunsCommand(
            workflow_definition_id=message_content.workflow_id,
            project_id=message_content.project_id,
        )
    )


def on_workflow_deployed_successfully(
    event: BaseEvent,
) -> ProtobufMessage:
    event = cast(WorkflowDeployedSuccessfully, event)
    return WorkflowDeployed(workflow_id=event.workflow_definition_id)


def on_workflow_undeployed_successfully(
    event: BaseEvent,
) -> ProtobufMessage:
    event = cast(WorkflowUndeployedSuccessfully, event)
    if event.error is not None:
        return WorkflowUndeployed(
            workflow_id=event.workflow_definition_id, error=event.error
        )
    return WorkflowUndeployed(workflow_id=event.workflow_definition_id)
