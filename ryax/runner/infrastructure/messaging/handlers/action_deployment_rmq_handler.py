from typing import cast
from datetime import datetime, timezone

from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide
from google.protobuf.struct_pb2 import Struct

from ryax.common.domain.internal_messaging.base_messages import BaseCommand
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.infrastructure.messaging.messages.worker_messages_pb2 import (
    ActionDeploymentUpdated,
    ActionDeploymentNewLogLines,
    ActionDeploymentCurrentState,
    DeploymentState,
    RecommendationsVPA,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    DeployActionDeploymentCommand,
    UpdateActionDeploymentCommand,
    GetActionDeploymentStateCommand,
    DeleteActionDeploymentCommand,
)
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentNewLogLinesEvent,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_events import (
    RecommendationsVPAUpdateCommand,
)
from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    Deploy,
    GetDeploymentState,
    Undeploy,
    RecommendationsVPAReply,
)
from ryax.common.infrastructure.messaging.publisher import ProtobufMessage

import logging

logger = logging.getLogger(__name__)


def on_deploy_action(
    event: DeployActionDeploymentCommand,
) -> ProtobufMessage:
    parameters = Struct()
    parameters.update(event.parameters)

    kind_map = {
        ActionKind.TRIGGER: Deploy.Kind.TRIGGER,
        ActionKind.SOURCE: Deploy.Kind.TRIGGER,
        ActionKind.PROCESSOR: Deploy.Kind.ACTION,
        ActionKind.PUBLISHER: Deploy.Kind.ACTION,
    }

    return Deploy(
        id=event.action_deployment_id,
        name=event.name,
        version=event.version,
        container_image=event.container_image,
        kind=kind_map[event.kind],
        log_level=Deploy.LogLevel.Value(Deploy.LogLevel.Name(event.log_level)),  # type: ignore
        resources=(
            Deploy.Resources(
                cpu=event.resources.cpu,
                memory=event.resources.memory,
                time=event.resources.time,
                gpu=event.resources.gpu,
            )
            if event.resources is not None
            else None
        ),
        execution_type=event.execution_type.name,
        deployment_type=event.deployment_type,
        parameters=parameters,
        node_pool_name=event.node_pool_name,
    )


# Message from the Workers
async def on_action_deployment_updated(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    message_content = ActionDeploymentUpdated()
    message_content.ParseFromString(message.body)
    await message_bus.handle_command(
        UpdateActionDeploymentCommand(
            action_deployment_id=message_content.deployment_id,
            state=ActionDeploymentState[
                DeploymentState.Name(message_content.deployment_state)
            ],
            logs=message_content.logs,
        )
    )


async def on_action_new_log_lines(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    message_content = ActionDeploymentNewLogLines()
    message_content.ParseFromString(message.body)
    await message_bus.handle_event(
        ActionDeploymentNewLogLinesEvent(
            action_deployment_id=message_content.deployment_id,
            log_lines=message_content.logs,
            executor_id=message_content.executor_id,
        )
    )


def on_get_action_deployment_state(
    command: BaseCommand,
) -> ProtobufMessage:
    command = cast(GetActionDeploymentStateCommand, command)
    return GetDeploymentState(deployment_id=command.action_deployment_id)


async def on_current_action_deployment_state(
    message: AbstractIncomingMessage,
) -> ActionDeploymentState:
    message_content = ActionDeploymentCurrentState()
    message_content.ParseFromString(message.body)
    return ActionDeploymentState[DeploymentState.Name(message_content.deployment_state)]


def on_undeploy_action(
    command: DeleteActionDeploymentCommand,
) -> ProtobufMessage:
    return Undeploy(id=command.action_deployment_id)


# VPA Recommendation
async def on_worker_recommendation_vpa_update(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> ProtobufMessage:
    message_content = RecommendationsVPA()
    message_content.ParseFromString(message.body)
    try:
        now = datetime.now(timezone.utc)
        # Re assign the timestamp here, using the runner local time
        await message_bus.handle_command(
            RecommendationsVPAUpdateCommand(
                recommendations=[
                    RecommendationsVPAUpdateCommand.RecommendationVPA(
                        container_image=r.container_image,
                        # Not pass cpu to the runner
                        memory=r.memory,
                        gpu_mig_instance=r.gpu_mig_instance,
                        timestamp=now,
                        is_oom_bumpup=r.is_oom_bumpup,
                        is_gpuoom_bumpup=r.is_gpuoom_bumpup,
                    )
                    for r in message_content.recommendations
                ]
            )
        )
        return RecommendationsVPAReply(error_message=None)
    except Exception as e:
        return RecommendationsVPAReply(error_message=str(e))
