import datetime
from typing import cast

from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide
from google.protobuf.struct_pb2 import Struct

from ryax.common.domain.internal_messaging.base_messages import BaseMessage, BaseCommand
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    StartExecution,
    InitTaskReply,
    StopExecution,
    GetExecutionState,
)
from ryax.common.infrastructure.messaging.messages.worker_messages_pb2 import (
    TaskNewLogLines,
    InitTaskRequest,
    ExecutionEnded,
    TaskCompleted,
    GetExecutionStateReply,
)
from ryax.common.infrastructure.messaging.publisher import ProtobufMessage
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    InitializeExecutionResultCommand,
    StopExecutionConnectionCommand,
    GetExecutionConnectionStateCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionEndedEvent,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultNewLogLinesToBeAddedEvent,
    ExecutionResultFinishedEvent,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)


async def on_execution_result_completed(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    message_content = TaskCompleted()
    message_content.ParseFromString(message.body)
    await message_bus.handle(
        [
            ExecutionResultFinishedEvent(
                id=message_content.task_id,
                error_message=message_content.error_message,
                status=ExecutionResultState[
                    TaskCompleted.Status.Name(message_content.status)
                ],
                # TODO Use a consistent datetime
                end_time=message_content.end_time.ToDatetime(tzinfo=datetime.UTC),
                outputs={key: value for key, value in message_content.outputs.items()},
                output_files=[
                    ExecutionResultFinishedEvent.OutputFile(
                        file_path=elem.file_path,
                        output_name=elem.output_name,
                        mimetype=elem.mimetype,
                        encoding=elem.encoding,
                        size_in_bytes=elem.size_in_bytes,
                    )
                    for elem in message_content.output_files
                ],
                http_status_code=message_content.http_status_code,
            )
        ]
    )


async def on_execution_connection_new_log_lines(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    message_content = TaskNewLogLines()
    message_content.ParseFromString(message.body)
    await message_bus.handle(
        [
            ExecutionResultNewLogLinesToBeAddedEvent(
                log_lines=message_content.logs,
                execution_result_id=message_content.task_id,
            )
        ]
    )


async def on_execution_connection_ended(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> None:
    message_content = ExecutionEnded()
    message_content.ParseFromString(message.body)
    await message_bus.handle(
        [
            ExecutionConnectionEndedEvent(
                execution_connection_id=message_content.execution_id,
                status=ExecutionConnectionState[
                    ExecutionEnded.ExecutionStatus.Name(message_content.status)
                ],
                error_message=message_content.error_message,
            )
        ]
    )


def on_start_execution(
    event: BaseMessage,
) -> ProtobufMessage:
    execution = cast(ExecutionConnection, event)
    assert execution.action_deployment is not None

    type_map = {
        ActionIOType.PASSWORD: StartExecution.IOType.STRING,
        ActionIOType.STRING: StartExecution.IOType.STRING,
        ActionIOType.LONGSTRING: StartExecution.IOType.STRING,
        ActionIOType.ENUM: StartExecution.IOType.STRING,
        ActionIOType.BOOLEAN: StartExecution.IOType.BOOLEAN,
        ActionIOType.FLOAT: StartExecution.IOType.FLOAT,
        ActionIOType.INTEGER: StartExecution.IOType.INTEGER,
        ActionIOType.DIRECTORY: StartExecution.IOType.DIRECTORY,
        ActionIOType.FILE: StartExecution.IOType.FILE,
    }

    inputs = Struct()
    inputs.update(execution.inputs)
    message = StartExecution(
        execution_id=execution.id,
        time_allotment=int(execution.time_allotment),
        deployment_id=execution.action_deployment.id,
        outputs_definitions=[
            StartExecution.IODefinition(
                name=output.name,
                type=type_map[output.type],
                optional=output.optional,
            )
            for output in execution.get_action_outputs()
        ],
        inputs_definitions=[
            StartExecution.IODefinition(
                name=input.name,
                type=type_map[input.type],
                optional=input.optional,
            )
            for input in execution.get_action_inputs()
        ],
        inputs=inputs,
    )
    return message


def on_stop_execution(
    event: StopExecutionConnectionCommand,
) -> ProtobufMessage:
    return StopExecution(execution_id=event.execution_connection_id)


def on_get_execution_state(
    command: BaseCommand,
) -> ProtobufMessage:
    command = cast(GetExecutionConnectionStateCommand, command)
    return GetExecutionState(execution_id=command.execution_connection_id)


async def on_received_execution_state(
    message: AbstractIncomingMessage,
) -> ExecutionConnectionState:
    message_content = GetExecutionStateReply()
    message_content.ParseFromString(message.body)
    try:
        return ExecutionConnectionState(
            GetExecutionStateReply.State.Name(message_content.execution_state)
        )
    except ValueError:
        return ExecutionConnectionState.NONE


# Query handler
async def on_init_execution_connection(
    message: AbstractIncomingMessage,
    message_bus: IMessageBus = Provide[ApplicationContainer.message_bus],
) -> ProtobufMessage:
    message_content = InitTaskRequest()
    message_content.ParseFromString(message.body)
    execution_result_id, workflow_run_id = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            executor_id=message_content.executor_id,
            log_delimiter=message_content.log_delimiter,
            execution_connection_id=message_content.execution_id,
        )
    )
    return InitTaskReply(task_id=execution_result_id, workflow_run_id=workflow_run_id)
