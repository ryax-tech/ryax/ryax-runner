# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import importlib.metadata
import platform

try:
    __version__ = importlib.metadata.version(__package__)
except Exception:
    __version__ = "dev"

python_version = platform.python_version()
