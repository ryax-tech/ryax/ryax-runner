import asyncio
import functools
import logging
from enum import Enum
from inspect import signature
from logging import getLogger
from typing import TypeAlias, Callable, Coroutine

from ryax.common.domain.internal_messaging.base_messages import BaseMessage

logger = getLogger(__name__)

Handler = Callable[..., Coroutine]


class SateMachineTransitionError(Exception):
    pass


class StateMachine:
    """
    A class to implement observability of the internal Object state machine.
    Usage:
    - Create a sublass that override the `stateType` with actual object state enum (must contain a "None" value)
    - Add MySubClasss.state_machine on each handler that change the object state. It will discover the Event/Command that is linked to this handler for you, but you have to provide the from_state and to_state list.

    Call the get get_mermaid() and/or get_transition_table() functions to get a visualization of the state transition.

    To see the current object state highlighted, call get_mermaid using the current_state option.

    Set raises_on_state_transition_error to ensure that a exception is raised when a state transition is not documented.
    """

    # Override this to adapt to the actual object
    stateType: TypeAlias = Enum
    stateMachine: dict[
        stateType, dict[stateType, list[tuple[type[BaseMessage], Handler]]]
    ] = {}
    raises_on_state_transition_error = True
    id_attribute = "id"
    locks: dict[str, asyncio.Lock] = {}

    @classmethod
    def get_mermaid(cls, current_state: stateType | None = None) -> str:
        txt = "graph TD\n"
        for state in cls.stateType:
            if current_state == state:
                txt += state.name + "{" + f'"{state.name}"' + "}\n"
            else:
                txt += f'{state.name}["{state.name}"]\n'
        for from_state, transitions in cls.stateMachine.items():
            for to_state, transition_list in transitions.items():
                for message_type, handler in transition_list:
                    handler_name = f" {handler.__name__}()"
                    if from_state is None:
                        txt += (
                            f'InitialState(( )) --"{handler_name}"--> {to_state.name}\n'
                        )
                    elif to_state is None:
                        txt += f'{from_state.name} --"{handler_name}"--> FinalState(("💀"))\n'
                    else:
                        txt += (
                            f'{from_state.name} --"{handler_name}"--> {to_state.name}\n'
                        )
        return txt

    @classmethod
    def get_transition_table(cls) -> str:
        """Show a state transition table only with Command to represents external interface of this State machine"""
        state_lines = []

        # Get all messages
        messages = []
        for transitions in cls.stateMachine.values():
            for transition_list in transitions.values():
                for message_type, _ in transition_list:
                    if message_type is not None:
                        if message_type.__name__ not in messages:
                            messages.append(message_type.__name__)

        states = [*cls.stateType]

        for state in states:
            state_line = [state.name] if state.name != "NONE" else [" ⃝ --> "]
            for _ in messages:
                # No Entry sign in UTF-8
                state_line.append("⛔")

            # find related transitions
            for from_state, transitions in cls.stateMachine.items():
                if from_state == state:
                    for to_state, transition_list in transitions.items():
                        for message, _ in transition_list:
                            message_name = message.__name__

                            if to_state is None:
                                state_line[messages.index(message_name) + 1] = "--> 💀"
                            else:
                                state_line[
                                    messages.index(message_name) + 1
                                ] = to_state.name

            state_lines.append(state_line)

        # Rendering o the table
        # Only works if headers are bigger than states !
        headers = ["↓ States / Messages →"] + messages
        headers_lines = "| " + " | ".join(headers) + " |\n"
        headers_lines += "".join(
            [f"| :{'-' * (len(header) - 2)}: " for header in headers]
        )
        state_table_lines = ""
        for state_line in state_lines:
            for header, state_entry in zip(headers, state_line):
                state_table_lines += (
                    f"| {' ' * (len(header) - len(state_entry))}{state_entry} "
                )
            state_table_lines += " |\n"

        transition_table = f"""
{headers_lines}
{state_table_lines}
            """
        return transition_table

    @classmethod
    def state_machine(
        cls,
        from_states: list[stateType],
        to_states: list[stateType],
    ) -> Callable:
        def decorator(func: Handler) -> Handler:
            message_type: type
            func_signature = signature(func)
            for param in func_signature.parameters.values():
                if issubclass(param.annotation, BaseMessage):
                    message_type = param.annotation
            assert (
                message_type is not None
            ), "The type of the message that triggers the handler was not found!"
            for from_state in from_states:
                transition = cls.stateMachine.get(from_state)
                if transition is None:
                    cls.stateMachine[from_state] = {}
                    transition = cls.stateMachine[from_state]
                for to_state in to_states:
                    existing_transition_list = transition.get(to_state)
                    if existing_transition_list is None:
                        transition.update({to_state: [(message_type, func)]})
                    else:
                        existing_transition_list.append((message_type, func))

            @functools.wraps(func)
            async def decorated(*args, **kwargs):  # type: ignore
                try:
                    lock = None
                    object_id = None
                    for arg in args:
                        if isinstance(arg, message_type):
                            if hasattr(arg, cls.id_attribute):
                                object_id = getattr(arg, cls.id_attribute)
                                lock = cls.locks.get(object_id)
                                if not lock:
                                    lock = asyncio.Lock()
                                    cls.locks[object_id] = lock
                                break
                    if lock:
                        if lock.locked():
                            logging.warning(
                                "Lock for object '%s' is in use for event %s",
                                object_id,
                                message_type,
                            )
                        async with lock:
                            return await func(*args, **kwargs)
                    else:
                        return await func(*args, **kwargs)
                except Exception as err:
                    msg = f"Unexpected error while running state transition with handler {func.__name__}"
                    logger.error(msg)
                    raise SateMachineTransitionError(msg) from err

            return decorated

        return decorator

    @classmethod
    def check_transition(cls, from_state: stateType, to_state: stateType) -> None:
        """Raise and/or log if a state transition is not supported"""
        if cls.stateMachine.get(from_state) is None:
            err_message = f"No state transition are registered from state '{from_state}' in {cls.stateType}"
            if cls.raises_on_state_transition_error:
                raise SateMachineTransitionError(err_message)
            logger.error(err_message)
        if to_state not in cls.stateMachine[from_state]:
            err_message = f"No state transition are registered from state '{from_state}' to state '{to_state}'"
            if cls.raises_on_state_transition_error:
                raise SateMachineTransitionError(err_message)
            logger.error(err_message)
        transitions = cls.stateMachine[from_state][to_state]
        logger.debug(
            "State transition from '%s' to state '%s' registered with transitions %s",
            from_state,
            to_state,
            transitions,
        )
