# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import random
import string
import time
from datetime import datetime, timezone


def format_exception(exception: Exception) -> str:
    return f"{type(exception).__name__}: {exception}"


def get_date() -> datetime:
    return datetime.now(timezone.utc)


def get_random_id(length: int = 8) -> str:
    letters = string.digits + string.ascii_lowercase
    new_id = "".join(random.choice(letters) for _ in range(length))
    now = str(int(time.time()))
    return now + "-" + new_id
