# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import BinaryIO


class IStorageService(abc.ABC):
    """
    Interface for Storage resource query
    """

    @abc.abstractmethod
    def generate_file_path(self, file_name: str) -> str:
        raise NotImplementedError()

    @abc.abstractmethod
    async def write_file(self, file_path: str, file: bytes, file_size: int) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    async def read_file(self, file_path: str) -> bytes:
        raise NotImplementedError()

    @abc.abstractmethod
    async def write(self, file_path: str, stream: BinaryIO) -> None:
        """Write into the file_path the content of the byte stream."""
        raise NotImplementedError()

    @abc.abstractmethod
    async def read(self, file_path: str) -> BinaryIO:
        """Return a byte stream to read from the file_path."""
        raise NotImplementedError()

    @abc.abstractmethod
    async def remove_file(self, file_path: str) -> None:
        raise NotImplementedError()
