from dataclasses import dataclass
from enum import Enum

from ryax.common.domain.utils import get_random_id
from ryax.runner.domain.site.site_execeptions import UnknownArchitectureError

GPU_MODE_FULL = "full"


class SiteType(Enum):
    SLURM_SSH = "SLURM_SSH"
    KUBERNETES = "KUBERNETES"


class NodeArchitecture(Enum):
    """
    Values are linux defined arch like returned by uname -a
    """

    X86_64 = "x86_64"
    ARM64 = "arm64"

    @classmethod
    def list(cls) -> list[str]:
        return list(map(lambda c: c.value, cls))  # type: ignore

    @staticmethod
    def get_architecture(architecture_raw: str) -> "NodeArchitecture":
        try:
            return (
                NodeArchitecture[architecture_raw.upper()]
                if architecture_raw
                else NodeArchitecture.X86_64
            )
        except (KeyError, ValueError):
            raise UnknownArchitectureError


class Objectives(Enum):
    ENERGY = "Energy"
    PERFORMANCE = "Performance"
    COST = "Cost"

    @classmethod
    def list(cls) -> list[str]:
        return list(map(lambda c: c.value, cls))  # type: ignore


@dataclass
class ObjectiveScores:
    id: str
    energy: int = 0
    performance: int = 0
    cost: int = 0

    @classmethod
    def new_id(cls) -> str:
        return f"{cls.__name__}-{get_random_id()}"

    def __getitem__(self, item: Objectives) -> int:
        return getattr(self, item.name.lower())
