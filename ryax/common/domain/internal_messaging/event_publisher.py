# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List, Any


class IPublisherService(abc.ABC):
    @abc.abstractmethod
    async def publish(self, events: List[Any], site_name: str | None = None) -> None:
        """Method used to publish domain event to an external service"""
        raise NotImplementedError
