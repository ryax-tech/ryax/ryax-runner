import abc
from typing import Any

from .base_messages import BaseCommand


class IQuerierService(abc.ABC):
    @abc.abstractmethod
    async def query(
        self, command: BaseCommand, timeout: int = 10, site_name: str | None = None
    ) -> Any:
        """Method used to publish domain event to an external service
        Raise a timeout exception if timeout is reach, if '-1' wait forever
        """
        raise NotImplementedError
