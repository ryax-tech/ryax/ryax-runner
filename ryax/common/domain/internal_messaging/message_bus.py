# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from abc import ABC, abstractmethod
from asyncio import Task
from typing import Any, Callable, List, Optional, Type

from .base_messages import (
    BaseCommand,
    BaseEvent,
    BaseMessage,
)

Messages = List[BaseMessage]


class IMessageBus(ABC):
    """Interface of the internal message bus.

    It distinguishes between Command and Event messages:
    - Commands are synchronous, targets only one handler and optionally returns value. Exception raised in a command are sent by the caller.
    - Events are asynchronous, targets one or more handlers without values or exception sent back to the caller.

    The message bus purpose is to:
    - manage synchronous and asynchronous calls between services the services
    - de-correlate services, thus avoid circular import and simplify services evolution
    """

    @abstractmethod
    def register_event_handler(
        self, event: Type[BaseEvent], event_handler: Callable
    ) -> None:
        raise NotImplementedError()

    @abstractmethod
    def register_command_handler(
        self, command: Type[BaseCommand], command_handler: Callable
    ) -> None:
        raise NotImplementedError()

    @abstractmethod
    async def handle(self, messages: Messages) -> None:
        """Asynchronous event and command handling: create a pending_execution for each event and command"""
        raise NotImplementedError()

    @abstractmethod
    async def handle_event(
        self, event: BaseEvent, task_name: str | None = None
    ) -> None:
        """synchronous handling for one event"""
        raise NotImplementedError()

    @abstractmethod
    async def handle_command(self, command: BaseCommand, blocking: bool = True) -> Any:
        """synchronous handling for one command unless blocking set to False"""
        pass

    @abstractmethod
    def wait_for_event(
        self, event_type: Type[BaseEvent], with_parameters: Optional[dict] = None
    ) -> Task[BaseEvent]:
        pass

    @abstractmethod
    async def stop(self) -> None:
        """Stop all remaining (Cancel) task with a gracefull time of 5 seconds"""
        ...
