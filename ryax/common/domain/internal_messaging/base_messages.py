# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass


@dataclass
class BaseMessage:
    pass


class BaseCommand(BaseMessage):
    # Class to define base command
    pass


class BaseEvent(BaseMessage):
    # Class to define base event
    pass


class ApplicationStartupEvent(BaseEvent):
    pass


class ApplicationStartupCommand(BaseCommand):
    pass


class StopApplicationCommand(BaseCommand):
    pass
