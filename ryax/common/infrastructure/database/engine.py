# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import time
from typing import Optional, Callable
from urllib.parse import urlsplit, urlunsplit

from sqlalchemy import event
from sqlalchemy.engine import Connection, Engine, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session as SessionType
from opentelemetry.instrumentation.sqlalchemy import SQLAlchemyInstrumentor


Session = sessionmaker(expire_on_commit=False)
logger = logging.getLogger(__name__)
logger_sql_time = logging.getLogger("ryax.performance.sql_query_time")


class SqlalchemyDatabaseEngine:
    def __init__(
        self,
        connection_url: str,
        create_mapping: Callable[[Engine], None],
    ) -> None:
        self.create_mapping = create_mapping
        if connection_url is None:
            raise ValueError("The connection URL is missing")
        self.connection_url: str = connection_url.strip()
        self._engine: Optional[Engine] = None

    def connect(self, do_mapping: bool = True) -> None:
        logger.info("Connecting to database")
        if not self.connection_url:
            raise Exception("The connection URL to the database is empty.")
        logger.debug(
            "Database connection url: %s",
            remove_password_from_URI(self.connection_url),
        )
        if self.connection_url.startswith("sqlite"):
            self._engine = create_engine(
                self.connection_url, connect_args={"timeout": 30}
            )
        else:
            self._engine = create_engine(
                self.connection_url, pool_size=10, max_overflow=90
            )
        # Add tracing instrumentation
        SQLAlchemyInstrumentor().instrument(
            engine=self._engine,
        )
        if do_mapping is True:
            self.create_mapping(self._engine)

    def disconnect(self) -> None:
        logger.info("Disconnecting from database")
        if self._engine is not None:
            self._engine.dispose()

    def get_session(self) -> SessionType:
        return Session(bind=self._engine)


@event.listens_for(Engine, "before_cursor_execute")
def before_cursor_execute(  # type: ignore
    conn: Connection, _cursor, statement: str, _parameters, _context, _executemany
) -> None:
    conn.info.setdefault("query_start_time", []).append(time.time())
    logger_sql_time.debug("Start Query: %s", statement)


@event.listens_for(Engine, "after_cursor_execute")
def after_cursor_execute(  # type: ignore
    conn: Connection, _cursor, statement: str, _parameters, _context, _executemany
) -> None:
    total = time.time() - conn.info["query_start_time"].pop(-1)
    if total > 0.1:
        logger_sql_time.warning(
            "This query took too much time: %s. Query: %s", total, statement
        )
    else:
        logger_sql_time.debug("Query Complete! Total Time: %f", total)


def remove_password_from_URI(uri: str) -> str:
    try:
        url = urlsplit(uri)
        # Remove password from the URL if present
        if url.password is None:
            return uri

        edited_url = url._replace(netloc=url.netloc.replace(url.password, "*****"))
        return urlunsplit(edited_url)
    except ValueError:
        # This is not a valid URL
        return uri
