from enum import Enum
from typing import Any

import sqlalchemy as sa
from sqlalchemy import types


class ScalarEnumListException(Exception):
    pass


class ScalarEnumListType(types.TypeDecorator):
    impl = sa.UnicodeText()

    def __init__(
        self, coerce_func: type = str, separator: str = ",", *args: str, **kwargs: str
    ) -> None:
        self.separator = separator
        self.coerce_func = coerce_func

    def serialize_item(self, value: Enum | str) -> str:
        if issubclass(self.coerce_func, Enum):
            assert isinstance(value, Enum)
            return str(value.value)
        assert isinstance(value, str)
        return value

    def process_bind_param(self, value: list[Enum] | None, dialect: Any) -> str | None:
        if value is None:
            return value

        to_serialize = [self.serialize_item(v) for v in value]

        if any(self.separator in item for item in to_serialize):
            raise ScalarEnumListException(
                "List values can't contain string '%s' (its being used as "
                "separator. If you wish for scalar list values to contain "
                "these strings, use a different separator string.)" % self.separator
            )

        return self.separator.join(to_serialize)

    def process_result_value(self, value: str, dialect: Any) -> list[Enum]:
        if not value:
            return []

        return list(map(self.coerce_func, value.split(self.separator)))
