import logging
from dataclasses import dataclass

from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.trace import TracerProvider as TraceProviderAbstract
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
)
from opentelemetry.trace import NoOpTracerProvider

logger = logging.getLogger(__name__)


@dataclass
class OpenTelemetryTracer:
    open_telemetry_collector_endpoint: str | None
    service_name: str
    processor: BatchSpanProcessor | None = None

    def init(self) -> None:
        provider: TraceProviderAbstract
        if not self.open_telemetry_collector_endpoint:
            provider = NoOpTracerProvider()
            logger.warning(
                "OpenTelemetry Tracer is disabled! Set the RYAX_OTLP_ENDPOINT variable to enable tracing"
            )
        else:
            resource = Resource(attributes={"service.name": self.service_name})
            provider = TracerProvider(resource=resource)

            try:
                otlp_exporter = OTLPSpanExporter(
                    endpoint=self.open_telemetry_collector_endpoint, insecure=True
                )
                self.processor = BatchSpanProcessor(otlp_exporter)
                provider.add_span_processor(self.processor)
            except Exception:
                logger.exception("Unable to start OpenTelemetry tracer exporter")

        # Sets the global default tracer provider
        trace.set_tracer_provider(provider)

    def stop(self) -> None:
        if self.processor is not None:
            self.processor.shutdown()
