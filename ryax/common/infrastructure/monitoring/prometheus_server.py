import asyncio
import logging
from dataclasses import dataclass
from typing import Optional

import uvicorn
from fastapi import FastAPI
from prometheus_client import make_asgi_app

logger = logging.getLogger(__name__)


@dataclass
class PrometheusMetricsServer:
    metrics_port: int = 8090
    server: Optional[uvicorn.Server] = None
    server_task: Optional[asyncio.Task] = None

    def serve(self) -> None:
        # Create app
        app = FastAPI(debug=False)

        # Add prometheus asgi middleware to route /metrics requests
        metrics_app = make_asgi_app()
        app.mount("/metrics", metrics_app)

        config = uvicorn.Config(
            app,
            host="0.0.0.0",
            port=self.metrics_port,
            log_level="info",
            loop="asyncio",
        )
        self.server = uvicorn.Server(config)
        # Disable Uvicorn signal catch to allow proper signal handling on shutdown
        self.server.install_signal_handlers = lambda: None  # type: ignore
        self.server_task = asyncio.create_task(self.server.serve())
        logger.info("Prometheus Metrics server started successfully")

    async def stop(self, wait_completion: bool = True, timeout: int = 1) -> None:
        logger.info("Stopping Prometheus Metrics server...")
        server = self.server
        assert server is not None
        server.should_exit = True
        server.force_exit = True
        await server.shutdown()
        if wait_completion:
            # TODO find a better way to do this. This is still an open issue:
            # https://github.com/encode/uvicorn/discussions/1103
            if self.server_task:
                await asyncio.wait_for(self.server_task, timeout=timeout)
        logger.info("Prometheus Metrics server stopped")
