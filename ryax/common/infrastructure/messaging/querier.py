# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import uuid
from logging import Logger, getLogger
from typing import Type, MutableMapping, Any

import aio_pika
from aio_pika.abc import AbstractIncomingMessage, AbstractChannel, AbstractExchange

from ryax.common.domain.internal_messaging.base_messages import BaseCommand
from ryax.common.domain.internal_messaging.querier import IQuerierService
from ryax.common.infrastructure.messaging.engine import MessagingEngine
from ryax.common.infrastructure.messaging.message_handler import (
    MessagePublishHandler,
    ProtobufMessage,
    PublishingHandler,
    ConsumingHandler,
)

logger: Logger = getLogger(__name__)


class RabbitMQMessageQuerier(IQuerierService):
    def __init__(self, engine: MessagingEngine, base_routing_key: str):
        self._request_handlers: dict[Type, MessagePublishHandler] = {}
        self._reply_handlers: dict[str, ConsumingHandler] = {}
        self.engine: MessagingEngine = engine
        self.base_routing_key = base_routing_key
        self._futures: MutableMapping[str, asyncio.Future] = {}
        self._channel: AbstractChannel | None = None
        self._exchange: AbstractExchange | None = None

    def register_request_handler(
        self,
        message_type: Type,
        message_handler: PublishingHandler,
        routing_prefix: str,
    ) -> None:
        """Method to register converter"""
        self._request_handlers[message_type] = MessagePublishHandler(
            handler_function=message_handler,
            message_type=message_type,
            routing_prefix=routing_prefix,
        )

    def register_reply_handler(
        self,
        message_type: Type,
        message_handler: ConsumingHandler,
    ) -> None:
        """Method to register converter"""
        self._reply_handlers[message_type.__name__] = message_handler

    def handle_request_message(self, message: BaseCommand) -> ProtobufMessage | None:
        """Method to convert received message from registered converter"""
        message_handler = self._request_handlers.get(message.__class__)
        if message_handler and message_handler.handler_function is not None:
            return message_handler.handler_function(message)

        raise Exception(
            f"Message handler not registered for type: {message.__class__.__name__}"
        )

    async def handle_reply_message(self, message: AbstractIncomingMessage) -> Any:
        """
        Method to handle received message.
        Reject the message if the handling raise an exception
        """
        async with message.process():
            assert message.type is not None
            message_handler = self._reply_handlers.get(message.type)
            if message_handler:
                response = await message_handler(message)
                logger.debug(
                    "Message of type %s was consumed successfully with handler: %s",
                    message.type,
                    message_handler,
                )
                return response
            else:
                logger.warning(
                    f"Message controller not registered for type: {message.type}"
                )

    async def _callback(self, message: AbstractIncomingMessage) -> None:
        if message.correlation_id is None:
            logger.debug(
                f"Received not correlated message, this should not happen {message!r}"
            )
            raise
        logger.info("Message from Query queue received")
        future: asyncio.Future = self._futures.pop(message.correlation_id)
        future.set_result(message)

    async def _get_channel(self) -> AbstractChannel:
        if not self._channel or self._channel.is_closed:
            self._channel = await self.engine.get_channel()
        return self._channel

    async def _get_exchange(self) -> AbstractExchange:
        """Cache the exchange and the channel to avoid creating it at every publish"""
        if not self._exchange:
            await self._get_channel()
            assert self._channel is not None
            self._exchange = await self._channel.declare_exchange(
                "domain_events", aio_pika.ExchangeType.TOPIC, durable=True
            )
        return self._exchange

    async def query(
        self, command: BaseCommand, timeout: int = 10, site_name: str | None = None
    ) -> Any:
        channel = await self._get_channel()
        exchange = await self._get_exchange()
        callback_queue = await channel.declare_queue(
            exclusive=True,
            auto_delete=True,
            arguments={"x-message-ttl": timeout * 1000},
        )
        await callback_queue.bind(exchange, callback_queue.name)
        await callback_queue.consume(self._callback)

        # Initialize message with correlation
        correlation_id = str(uuid.uuid4())
        loop = asyncio.get_running_loop()
        response_future = loop.create_future()
        self._futures[correlation_id] = response_future

        message_content = self.handle_request_message(command)
        assert message_content is not None

        event_type = message_content.__class__.__name__
        message = aio_pika.Message(
            type=event_type,
            body=message_content.SerializeToString(),
            expiration=(timeout * 1000)
            - 100,  # Avoid message processed on the receiver while nobody is waiting for it
            correlation_id=correlation_id,
            reply_to=callback_queue.name,
        )
        routing_prefix = (
            f"{self._request_handlers[command.__class__].routing_prefix}.{event_type}"
        )
        if site_name is not None:
            routing_prefix = f"{self._request_handlers[command.__class__].routing_prefix}.{site_name}.{event_type}"
        await exchange.publish(
            message,
            routing_key=routing_prefix,
        )
        reply_message = await asyncio.wait_for(response_future, timeout=timeout)
        answer = await self.handle_reply_message(reply_message)
        await callback_queue.delete(if_empty=False, if_unused=False)
        return answer
