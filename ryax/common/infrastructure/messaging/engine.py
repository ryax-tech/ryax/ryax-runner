# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from typing import Optional

from aio_pika import connect_robust
from aio_pika.abc import AbstractChannel, AbstractRobustConnection
from opentelemetry.instrumentation.aio_pika import AioPikaInstrumentor

from ryax.worker.infrastructure.utils import remove_password_from_URI

AioPikaInstrumentor().instrument()

logger = getLogger(__name__)


class MessagingEngine:
    """Class to handle connection to messaging system (RabbitMQ)"""

    def __init__(self, connection_url: str) -> None:
        self.connection_url: str = connection_url
        self.connection: Optional[AbstractRobustConnection] = None

    async def connect(self) -> None:
        """Method to connect to messaging system"""
        logger.info(
            f"Connecting to broker with url: {remove_password_from_URI(self.connection_url)}"
        )
        self.connection = await connect_robust(self.connection_url)

    async def disconnect(self) -> None:
        """Method to disconnect from the messaging system"""
        logger.info("Disconnecting from broker")
        if self.connection is not None:
            await self.connection.close()

    async def get_channel(self, channel_number: int | None = None) -> AbstractChannel:
        """Method to get a channel from active connection"""
        assert self.connection is not None
        return await self.connection.channel(channel_number=channel_number)
