# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import Logger, getLogger
from typing import Type

import aio_pika
from aio_pika import ExchangeType
from aio_pika.abc import AbstractChannel, AbstractIncomingMessage, AbstractExchange

from .engine import MessagingEngine
from .message_handler import (
    MessageConsumeHandler,
    ConsumingHandler,
)

logger: Logger = getLogger(__name__)


class RabbitMQMessageConsumer:
    """Class to handle global system messages (aka. Domain events)"""

    def __init__(self, engine: MessagingEngine) -> None:
        self._handlers: dict[str, MessageConsumeHandler] = {}
        self.engine: MessagingEngine = engine
        self.channel: AbstractChannel | None = None
        self.exchange: AbstractExchange | None = None

    def register_handler(
        self,
        message_type: Type,
        message_handler: ConsumingHandler,
        routing_prefix: str,
    ) -> None:
        """Method to register handler"""
        self._handlers[message_type.__name__] = MessageConsumeHandler(
            handler_function=message_handler,
            message_type=message_type,
            routing_prefix=routing_prefix,
        )

    async def handle_message(self, message: AbstractIncomingMessage) -> None:
        """
        Method to handle received message.
        Reject the message if the handling raise an exception
        """
        async with message.process():
            try:
                assert message.type is not None
                message_handler = self._handlers.get(message.type)
                if (
                    message_handler is not None
                    and message_handler.handler_function is not None
                ):
                    if message.reply_to is not None:
                        # This is a direct query
                        assert self.exchange is not None
                        response = await message_handler.handler_function(message)
                        await self.exchange.publish(
                            aio_pika.Message(
                                body=response.SerializeToString(),
                                correlation_id=message.correlation_id,
                                type=response.__class__.__name__,
                            ),
                            routing_key=message.reply_to,
                        )
                        logger.debug(
                            "Query of type %s was handled successfully with handler: %s",
                            message.type,
                            message_handler,
                        )
                    else:
                        await message_handler.handler_function(message)
                        logger.debug(
                            "Message of type %s was consumed successfully with handler: %s",
                            message.type,
                            message_handler,
                        )
                else:
                    logger.warning(
                        "Message handler not registered for message type: %s",
                        message.type,
                    )
            except Exception as err:
                logger.exception("Failed to consume message. error = %s", err)
                raise err

    async def start(self, queue_name: str = "") -> None:
        """Method to start message consuming"""
        logger.info("Start message consuming")
        self.channel = await self.engine.get_channel()
        # Do not bound message consumption
        await self.channel.set_qos(prefetch_count=0)
        self.exchange = await self.channel.declare_exchange(
            "domain_events", ExchangeType.TOPIC, durable=True
        )
        queue = await self.channel.declare_queue(queue_name, durable=True)
        for handler in self._handlers.values():
            await queue.bind(
                self.exchange,
                ".".join([handler.routing_prefix, handler.message_type.__name__]),
            )
        await queue.consume(self.handle_message)
