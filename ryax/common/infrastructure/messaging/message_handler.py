from dataclasses import dataclass
from typing import Type, Callable, Coroutine

from aio_pika.abc import AbstractIncomingMessage
from google.protobuf.message import Message as ProtobufMessage

from ryax.common.domain.internal_messaging.base_messages import BaseCommand

ProtobufMessage = ProtobufMessage
PublishingHandler = Callable[[BaseCommand], ProtobufMessage]
ConsumingHandler = Callable[[AbstractIncomingMessage], Coroutine]


@dataclass
class MessagePublishHandler:
    message_type: Type
    handler_function: PublishingHandler
    routing_prefix: str


@dataclass
class MessageConsumeHandler:
    message_type: Type
    handler_function: ConsumingHandler
    routing_prefix: str
