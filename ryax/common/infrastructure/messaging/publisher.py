# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import Logger, getLogger
from typing import Callable, Dict, List, Optional, Type, Any

import aio_pika
from aio_pika.abc import AbstractChannel, AbstractExchange

from ryax.common.application.utils import retry_with_backoff
from ryax.common.domain.internal_messaging.base_messages import BaseEvent
from ryax.common.domain.internal_messaging.event_publisher import IPublisherService
from ryax.common.infrastructure.messaging.engine import MessagingEngine
from ryax.common.infrastructure.messaging.message_handler import ProtobufMessage

MessageHandler = Callable[[Any], ProtobufMessage]

logger: Logger = getLogger(__name__)


class RabbitMQMessagePublisher(IPublisherService):
    def __init__(self, engine: MessagingEngine, base_routing_key: str):
        self._handlers: Dict[Type, MessageHandler] = {}
        self.engine: MessagingEngine = engine
        self.base_routing_key = base_routing_key
        self._channel: AbstractChannel | None = None
        self._exchange: AbstractExchange | None = None

    def register_handler(
        self,
        message_type: Type,
        message_handler: MessageHandler,
    ) -> None:
        """Method to register converter"""
        self._handlers[message_type] = message_handler

    def handle_message(self, message: BaseEvent) -> Optional[ProtobufMessage]:
        """Method to convert received message from registered converter"""
        message_handler = self._handlers.get(message.__class__)
        if message_handler:
            return message_handler(message)
        else:
            logger.error(
                f"Message handler not registered for type: {message.__class__.__name__}"
            )
        return None

    async def _get_exchange(self) -> AbstractExchange:
        """Cache the exchange and the channel to avoid creating it at every publish"""
        if not self._exchange:
            if not self._channel:
                self._channel = await self.engine.get_channel()
            self._exchange = await self._channel.declare_exchange(
                "domain_events", aio_pika.ExchangeType.TOPIC, durable=True
            )
        return self._exchange

    @retry_with_backoff(retries=10, backoff_in_ms=2_000, max_backoff_in_ms=10_000)
    async def publish(
        self, events: List[BaseEvent], site_name: str | None = None
    ) -> None:
        if len(events) == 0:
            return
        exchange = await self._get_exchange()
        for item in events:
            logger.info(f"Publishing event: {item}")
            message_content = self.handle_message(item)
            if message_content is not None:
                event_type = message_content.__class__.__name__
                message = aio_pika.Message(
                    type=event_type, body=message_content.SerializeToString()
                )
                routing_key = f"{self.base_routing_key}.{event_type}"
                # Only send this message the associated site
                if site_name:
                    routing_key = f"{self.base_routing_key}.{site_name}.{event_type}"
                await exchange.publish(message, routing_key=routing_key)
