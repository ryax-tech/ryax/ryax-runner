# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ryax/common/infrastructure/messaging/messages/studio_messages.proto
# Protobuf Python Version: 4.25.1
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.protobuf import struct_pb2 as google_dot_protobuf_dot_struct__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\nCryax/common/infrastructure/messaging/messages/studio_messages.proto\x12\x06studio\x1a\x1cgoogle/protobuf/struct.proto\"\xb9\x14\n\x0eWorkflowDeploy\x12\n\n\x02id\x18\x01 \x01(\t\x12\x0c\n\x04name\x18\x02 \x01(\t\x12\x36\n\x07\x61\x63tions\x18\x03 \x03(\x0b\x32%.studio.WorkflowDeploy.WorkflowAction\x12\x12\n\nproject_id\x18\x04 \x01(\t\x12\x36\n\x07results\x18\x05 \x03(\x0b\x32%.studio.WorkflowDeploy.WorkflowResult\x1a\x89\x01\n\x17WorkflowActionResources\x12\x10\n\x03\x63pu\x18\x01 \x01(\x02H\x00\x88\x01\x01\x12\x13\n\x06memory\x18\x02 \x01(\x03H\x01\x88\x01\x01\x12\x11\n\x04time\x18\x03 \x01(\x02H\x02\x88\x01\x01\x12\x10\n\x03gpu\x18\x04 \x01(\x03H\x03\x88\x01\x01\x42\x06\n\x04_cpuB\t\n\x07_memoryB\x07\n\x05_timeB\x06\n\x04_gpu\x1aq\n\x19WorkflowActionConstraints\x12\x11\n\tsite_list\x18\x01 \x03(\t\x12\x16\n\x0enode_pool_list\x18\x02 \x03(\t\x12\x16\n\x0esite_type_list\x18\x03 \x03(\t\x12\x11\n\tarch_list\x18\x04 \x03(\t\x1aM\n\x18WorkflowActionObjectives\x12\x0e\n\x06\x65nergy\x18\x01 \x01(\x02\x12\x0c\n\x04\x63ost\x18\x02 \x01(\x02\x12\x13\n\x0bperformance\x18\x03 \x01(\x02\x1a\x83\x01\n\x0eWorkflowResult\x12\x1a\n\x12workflow_result_id\x18\x01 \x01(\t\x12\x1d\n\x15workflow_action_io_id\x18\x02 \x01(\t\x12\x0b\n\x03key\x18\x03 \x01(\t\x12\x16\n\x0etechnical_name\x18\x04 \x01(\t\x12\x11\n\taction_id\x18\x05 \x01(\t\x1a\xb6\x04\n\x10WorkflowActionIO\x12\x16\n\x0etechnical_name\x18\x01 \x01(\t\x12\x14\n\x0c\x64isplay_name\x18\x03 \x01(\t\x12\x0c\n\x04help\x18\x04 \x01(\t\x12<\n\x04type\x18\x02 \x01(\x0e\x32..studio.WorkflowDeploy.WorkflowActionIO.IOType\x12\x13\n\x0b\x65num_values\x18\x05 \x03(\t\x12\x43\n\x06origin\x18\x06 \x01(\x0e\x32..studio.WorkflowDeploy.WorkflowActionIO.ORIGINH\x00\x88\x01\x01\x12\x17\n\naddon_name\x18\x07 \x01(\tH\x01\x88\x01\x01\x12\x10\n\x08optional\x18\x08 \x01(\x08\"\xc7\x01\n\x06IOType\x12\x08\n\x04NONE\x10\x00\x12\x0e\n\nLONGSTRING\x10\x01\x12\x0b\n\x07INTEGER\x10\x02\x12\t\n\x05\x46LOAT\x10\x03\x12\x0c\n\x08PASSWORD\x10\x04\x12\x08\n\x04\x45NUM\x10\x05\x12\x08\n\x04\x46ILE\x10\x06\x12\r\n\tDIRECTORY\x10\x07\x12\t\n\x05TABLE\x10\x08\x12\t\n\x05\x42YTES\x10\t\x12\x0b\n\x07\x42OOLEAN\x10\n\x12\x17\n\x13\x44IRECTORY_REFERENCE\x10\x0b\x12\x12\n\x0e\x46ILE_REFERENCE\x10\x0c\x12\n\n\x06STRING\x10\r\"?\n\x06ORIGIN\x12\x08\n\x04PATH\x10\x00\x12\n\n\x06HEADER\x10\x01\x12\t\n\x05QUERY\x10\x02\x12\n\n\x06\x43OOKIE\x10\x03\x12\x08\n\x04\x42ODY\x10\x04\x42\t\n\x07_originB\r\n\x0b_addon_name\x1a\xae\n\n\x0eWorkflowAction\x12\x1a\n\x12workflow_action_id\x18\x01 \x01(\t\x12\x11\n\taction_id\x18\x0e \x01(\t\x12\x12\n\nhuman_name\x18\x0f \x01(\t\x12\x13\n\x0b\x64\x65scription\x18\x10 \x01(\t\x12N\n\raction_addons\x18\x14 \x03(\x0b\x32\x37.studio.WorkflowDeploy.WorkflowAction.ActionAddonsEntry\x12\x16\n\x0etechnical_name\x18\x02 \x01(\t\x12\x0f\n\x07version\x18\x03 \x01(\t\x12\x38\n\x04kind\x18\x04 \x01(\x0e\x32*.studio.WorkflowDeploy.WorkflowAction.Kind\x12\x37\n\x06inputs\x18\x05 \x03(\x0b\x32\'.studio.WorkflowDeploy.WorkflowActionIO\x12>\n\raddons_inputs\x18\x15 \x03(\x0b\x32\'.studio.WorkflowDeploy.WorkflowActionIO\x12\x38\n\x07outputs\x18\x06 \x03(\x0b\x32\'.studio.WorkflowDeploy.WorkflowActionIO\x12@\n\x0f\x64ynamic_outputs\x18\x12 \x03(\x0b\x32\'.studio.WorkflowDeploy.WorkflowActionIO\x12\x1b\n\x13has_dynamic_outputs\x18\x11 \x01(\x08\x12\x12\n\nproject_id\x18\x07 \x01(\t\x12\x14\n\x0cnext_actions\x18\t \x03(\t\x12\x17\n\x0f\x63ontainer_image\x18\n \x01(\t\x12.\n\rstatic_values\x18\x0b \x01(\x0b\x32\x17.google.protobuf.Struct\x12T\n\x10reference_values\x18\x0c \x03(\x0b\x32:.studio.WorkflowDeploy.WorkflowAction.ReferenceValuesEntry\x12J\n\x0b\x66ile_values\x18\r \x03(\x0b\x32\x35.studio.WorkflowDeploy.WorkflowAction.FileValuesEntry\x12\x41\n\tresources\x18\x16 \x01(\x0b\x32..studio.WorkflowDeploy.WorkflowActionResources\x12\x45\n\x0b\x63onstraints\x18\x17 \x01(\x0b\x32\x30.studio.WorkflowDeploy.WorkflowActionConstraints\x12\x43\n\nobjectives\x18\x18 \x01(\x0b\x32/.studio.WorkflowDeploy.WorkflowActionObjectives\x1aL\n\x11\x41\x63tionAddonsEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12&\n\x05value\x18\x02 \x01(\x0b\x32\x17.google.protobuf.Struct:\x02\x38\x01\x1a]\n\x14ReferenceValuesEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\x34\n\x05value\x18\x02 \x01(\x0b\x32%.studio.WorkflowDeploy.ReferenceValue:\x02\x38\x01\x1a\x31\n\x0f\x46ileValuesEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t:\x02\x38\x01\";\n\x04Kind\x12\x08\n\x04NONE\x10\x00\x12\x0b\n\x07TRIGGER\x10\x01\x12\r\n\tPROCESSOR\x10\x02\x12\r\n\tPUBLISHER\x10\x03\x1aK\n\x0eReferenceValue\x12\x1a\n\x12workflow_action_id\x18\x01 \x01(\t\x12\x1d\n\x15output_technical_name\x18\x02 \x01(\t\"M\n\x10WorkflowUndeploy\x12\x13\n\x0bworkflow_id\x18\x01 \x01(\t\x12\x12\n\nproject_id\x18\x02 \x01(\t\x12\x10\n\x08graceful\x18\x03 \x01(\x08\":\n\x0fWorkflowDeleted\x12\x13\n\x0bworkflow_id\x18\x01 \x01(\t\x12\x12\n\nproject_id\x18\x02 \x01(\tb\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'ryax.common.infrastructure.messaging.messages.studio_messages_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:
  DESCRIPTOR._options = None
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_ACTIONADDONSENTRY']._options = None
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_ACTIONADDONSENTRY']._serialized_options = b'8\001'
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_REFERENCEVALUESENTRY']._options = None
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_REFERENCEVALUESENTRY']._serialized_options = b'8\001'
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_FILEVALUESENTRY']._options = None
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_FILEVALUESENTRY']._serialized_options = b'8\001'
  _globals['_WORKFLOWDEPLOY']._serialized_start=110
  _globals['_WORKFLOWDEPLOY']._serialized_end=2727
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONRESOURCES']._serialized_start=287
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONRESOURCES']._serialized_end=424
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONCONSTRAINTS']._serialized_start=426
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONCONSTRAINTS']._serialized_end=539
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONOBJECTIVES']._serialized_start=541
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONOBJECTIVES']._serialized_end=618
  _globals['_WORKFLOWDEPLOY_WORKFLOWRESULT']._serialized_start=621
  _globals['_WORKFLOWDEPLOY_WORKFLOWRESULT']._serialized_end=752
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONIO']._serialized_start=755
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONIO']._serialized_end=1321
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONIO_IOTYPE']._serialized_start=1031
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONIO_IOTYPE']._serialized_end=1230
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONIO_ORIGIN']._serialized_start=1232
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTIONIO_ORIGIN']._serialized_end=1295
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION']._serialized_start=1324
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION']._serialized_end=2650
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_ACTIONADDONSENTRY']._serialized_start=2367
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_ACTIONADDONSENTRY']._serialized_end=2443
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_REFERENCEVALUESENTRY']._serialized_start=2445
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_REFERENCEVALUESENTRY']._serialized_end=2538
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_FILEVALUESENTRY']._serialized_start=2540
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_FILEVALUESENTRY']._serialized_end=2589
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_KIND']._serialized_start=2591
  _globals['_WORKFLOWDEPLOY_WORKFLOWACTION_KIND']._serialized_end=2650
  _globals['_WORKFLOWDEPLOY_REFERENCEVALUE']._serialized_start=2652
  _globals['_WORKFLOWDEPLOY_REFERENCEVALUE']._serialized_end=2727
  _globals['_WORKFLOWUNDEPLOY']._serialized_start=2729
  _globals['_WORKFLOWUNDEPLOY']._serialized_end=2806
  _globals['_WORKFLOWDELETED']._serialized_start=2808
  _globals['_WORKFLOWDELETED']._serialized_end=2866
# @@protoc_insertion_point(module_scope)
