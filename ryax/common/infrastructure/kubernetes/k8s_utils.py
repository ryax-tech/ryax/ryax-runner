# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import re


def safe_resource_name(id: str, name: str, version: str, suffix: str = "") -> str:
    """
    Apply name constraints defined by Kubernetes.

    WARNING: We are using the 8 first characters of the action_deployment id to distinguish from other deployments. Be sur that it is unique enough...
    More details here:
    https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#dns-label-names
    """
    # contain at most 63 characters (44 + 13 + 4 + 2 dashes)
    name_length = 42 - len(suffix)
    unsafe_resource_name = f"{name[:name_length]}-{version[:13]}-{id[-4:]}"

    if suffix:
        unsafe_resource_name += "-" + suffix
    # contain only lowercase alphanumeric characters or '-'
    unsafe_resource_name = re.sub("[^0-9a-z]+", "-", unsafe_resource_name.lower())
    # start with an alphanumeric character
    unsafe_resource_name = unsafe_resource_name.lstrip("-")
    # end with an alphanumeric character
    unsafe_resource_name = unsafe_resource_name.rstrip("-")

    assert len(unsafe_resource_name) < 64

    return unsafe_resource_name.lower()
