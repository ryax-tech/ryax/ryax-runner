# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os
import uuid
from pathlib import Path
from typing import BinaryIO

from ryax.common.domain.storage.storage_service import IStorageService

BUFFER_SIZE = 512 * 1024  # 512KB


class PosixStorageService(IStorageService):
    """Interface for Local Storage"""

    def __init__(self, root_path: str):
        self.root_path = root_path
        super().__init__()

    def generate_file_path(self, file_path: str) -> str:
        return str(
            Path(self.root_path) / (str(uuid.uuid4()) + "-" + Path(file_path).name)
        )

    async def write_file(
        self, file_path: str, file_bytes: bytes, file_size: float
    ) -> None:
        with open(file_path, "wb") as to_write_file:
            to_write_file.write(file_bytes)

    async def read_file(self, file_path: str) -> bytes:
        with open(file_path, "rb") as to_read_file:
            file_content: bytes = to_read_file.read()
        return file_content

    async def read(self, file_path: str) -> BinaryIO:
        return open(file_path, "rb")

    async def write(self, file_path: str, stream: BinaryIO) -> None:
        with open(file_path, "wb") as to_write_file:
            with stream:
                content = stream.read(BUFFER_SIZE)
                while len(content) > 0:
                    to_write_file.write(content)
                    content = stream.read(BUFFER_SIZE)

    async def remove_file(self, file_path: str) -> None:
        os.remove(file_path)
