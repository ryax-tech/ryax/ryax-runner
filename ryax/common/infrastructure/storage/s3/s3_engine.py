# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

from minio import Minio

logger = logging.getLogger(__name__)


class S3Engine:
    def __init__(
        self, connection_url: str, access_key: str, secret_key: str, bucket: str
    ) -> None:
        self.connection_url: str = connection_url
        self.access_key: str = access_key
        self.secret_key: str = secret_key
        self.bucket: str = bucket
        self.connection: Minio | None = None

    def connect(self) -> None:
        logger.info("Connecting to filestore")
        minio_connection = Minio(
            self.connection_url,
            access_key=self.access_key,
            secret_key=self.secret_key,
            secure=False,
        )
        # Uncomment this to add debug minio tracing
        # minio_connection.trace_on(sys.stdout)
        self.connection = minio_connection

    async def create_bucket(self) -> None:
        if not self.connection:
            self.connect()
        assert self.connection is not None
        if not self.connection.bucket_exists(self.bucket):
            self.connection.make_bucket(self.bucket)
            logger.info("Filestore bucket has been created")
        else:
            logger.info("Filestore bucket already exists")
