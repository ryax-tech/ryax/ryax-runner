# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import concurrent.futures
import functools
import io
import logging
import uuid
from pathlib import Path
from typing import BinaryIO

from minio import Minio, error
from opentelemetry import trace

from ryax.common.domain.storage.storage_service import IStorageService

from .s3_engine import S3Engine

BUFFER_SIZE = 5 * 1024 * 1024  # 5MB
STORAGE_DIRECTORY_PREFIX = "executions-data"

logger = logging.getLogger(__name__)
tracer = trace.get_tracer(__name__)


class S3Storage(IStorageService):
    def __init__(self, engine: S3Engine):
        self.engine: S3Engine = engine
        self.bucket: str = engine.bucket
        self.exec_pool = concurrent.futures.ThreadPoolExecutor(
            thread_name_prefix=__name__
        )

    def generate_file_path(
        self, file_name: str, prefix: str = STORAGE_DIRECTORY_PREFIX
    ) -> str:
        return str(Path(prefix) / str(uuid.uuid4()) / file_name)

    @property
    def connection(self) -> Minio:
        if self.engine.connection is None:
            self.engine.connect()
        assert self.engine.connection is not None
        return self.engine.connection

    async def write(self, file_path: str, stream: BinaryIO) -> None:
        with tracer.start_as_current_span("Remote storage write"):
            logger.debug("Start Uploading stream to %s", file_path)
            loop = asyncio.get_running_loop()
            await loop.run_in_executor(
                self.exec_pool,
                functools.partial(
                    self.connection.put_object,
                    self.bucket,
                    file_path,
                    stream,
                    -1,
                    part_size=BUFFER_SIZE,
                ),
            )
            logger.debug("End Uploading stream to %s", file_path)

    async def write_file(self, file_path: str, file: bytes, file_size: int) -> None:
        with tracer.start_as_current_span("Remote storage write file"):
            logger.debug(f"Uploading {file_path}. Size: {file_size}")
            loop = asyncio.get_running_loop()
            await loop.run_in_executor(
                self.exec_pool,
                functools.partial(
                    self.connection.put_object,
                    self.bucket,
                    file_path,
                    io.BytesIO(file),
                    file_size,
                ),
            )
            logger.debug(f"Finished Uploading {file_path}. Size: {file_size}")

    async def remove_file(self, file_path: str) -> None:
        with tracer.start_as_current_span("Remote storage remove file"):
            logger.debug(f"Deleting file in path: {file_path}")
            try:
                loop = asyncio.get_running_loop()
                await loop.run_in_executor(
                    self.exec_pool,
                    functools.partial(
                        self.connection.stat_object, self.bucket, file_path
                    ),
                )
                await loop.run_in_executor(
                    self.exec_pool,
                    functools.partial(
                        self.connection.remove_object, self.bucket, file_path
                    ),
                )
            except error.S3Error as err:
                if err.code == "NoSuchKey":
                    raise FileNotFoundError
            logger.debug(f"Finish deletion of path: {file_path}")

    async def read(self, file_path: str) -> BinaryIO:
        with tracer.start_as_current_span("Remote storage read"):
            try:
                logger.debug("Reading stream to %s", file_path)
                # FIXME: validate path instead of striping characters
                loop = asyncio.get_running_loop()
                return await loop.run_in_executor(
                    self.exec_pool,
                    functools.partial(  # type: ignore
                        self.connection.get_object,
                        self.bucket,
                        file_path.lstrip(".").lstrip("/"),
                    ),
                )
            except error.S3Error as err:
                if err.code == "NoSuchKey":
                    raise FileNotFoundError from err
                else:
                    raise

    async def read_file(self, file_path: str) -> bytes:
        data = await self.read(file_path)
        return data.read()
