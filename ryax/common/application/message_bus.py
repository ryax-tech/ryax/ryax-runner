# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import json
import logging
import traceback
from asyncio import Task, create_task, CancelledError
from dataclasses import dataclass, asdict, field
from datetime import datetime
from enum import Enum
from typing import (
    Any,
    Callable,
    Coroutine,
    Dict,
    List,
    Optional,
    Type,
    TypeVar,
    Sequence,
)

from opentelemetry import trace
from opentelemetry.context import Context
from opentelemetry.trace import Status, StatusCode, Link
from prometheus_client import Histogram

from ryax.common.domain.utils import get_random_id
from ryax.common.domain.internal_messaging.base_messages import (
    BaseCommand,
    BaseEvent,
    BaseMessage,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus, Messages

logger = logging.getLogger(__name__)
tracer = trace.get_tracer(__name__)

perf_logger = logging.getLogger("ryax.performance.event_processing_time")
events_summary = Histogram(
    "ryax_events_processing_time_seconds",
    "Processing time of an internal event.",
    ["event_name", "handler_name", "event_type"],
)

EventType = TypeVar("EventType", bound=BaseEvent)
HandlerType = Callable[..., Coroutine]


@dataclass
class EventMetadata:
    event: BaseMessage
    handler: HandlerType = field(init=True)
    error: str
    start: datetime
    end: Optional[datetime] = None
    name: str = field(init=False)
    handler_name: str = field(init=False)

    def __post_init__(self) -> None:
        self.name = self.event.__class__.__name__
        self.handler_name = f"{self.handler.__module__}.{self.handler.__name__}"  # type: ignore

    def to_json(self) -> str:
        class DateTimeEncoder(json.JSONEncoder):
            def default(self, o: Any) -> str:
                try:
                    if isinstance(o, Callable):  # type: ignore
                        return f"{o.__module__}.{o.__name__}"
                    if isinstance(o, Enum):
                        return o.name
                    if isinstance(o, datetime):
                        return o.isoformat()
                    return json.JSONEncoder.default(self, o)
                except Exception:
                    logger.warning("Perf logger fail to parse event metadata")
                    return ""

        try:
            return "RYAX_PERFORMANCE_EVENT_METADATA: " + json.dumps(
                asdict(self), cls=DateTimeEncoder
            )
        except Exception:
            return "RYAX_PERFORMANCE_EVENT_METADATA: {}"

    def event_type(self) -> str:
        return "event" if isinstance(self.event, BaseEvent) else "command"

    def labels(self) -> list[str]:
        return [
            self.name,
            self.handler_name,
            self.event_type(),
        ]

    def labels_dict(
        self,
    ) -> dict[
        str,
        str
        | bool
        | int
        | float
        | Sequence[str]
        | Sequence[bool]
        | Sequence[int]
        | Sequence[float],
    ]:
        return {
            "name": self.name,
            "handler": self.handler_name,
            "type": self.event_type(),
        }


class MessageBus(IMessageBus):
    def __init__(self, keep_event_history: bool = False) -> None:
        self.event_handlers: Dict[Type[BaseEvent], List[HandlerType]] = dict()
        self.command_handlers: Dict[Type[BaseCommand], Callable] = dict()
        self.tasks: Dict[str, Task] = {}
        # Enable this to see the list of event in order during debugging
        self.keep_event_history: bool = keep_event_history
        self.events: List[EventMetadata] = []
        if self.keep_event_history is True:
            logger.warning(
                "Keeping event history is for debugging purposes only. DO NOT DO THIS IN PRODUCTION"
            )

    async def stop(self) -> None:
        canceling = []
        for task in self.tasks.values():
            task.cancel()
            canceling.append(task)
        if canceling:
            await asyncio.wait(canceling, timeout=5)

    def register_event_handler(self, event: Type, event_handler: HandlerType) -> None:
        if event in self.event_handlers:
            self.event_handlers[event].append(event_handler)
        else:
            self.event_handlers[event] = [event_handler]

    def register_command_handler(
        self, command: Type, command_handler: HandlerType
    ) -> None:
        self.command_handlers[command] = command_handler

    async def handle(self, messages: Messages, blocking_commands: bool = False) -> None:
        """Asynchronous event and command handling: create a pending_execution for each event, but run commands in order"""
        while messages:
            message = messages.pop(0)
            if isinstance(message, BaseEvent):
                task_name = f"{message.__class__.__name__}-{get_random_id()}"
                self.add_tasks(
                    task_name,
                    asyncio.create_task(
                        self.handle_event(message, task_name),
                        name=task_name,
                    ),
                )
            elif isinstance(message, BaseCommand):
                await self.handle_command(message, blocking=blocking_commands)
            else:
                raise Exception(f"{message} was not an Event or Command")

    async def handle_event(
        self, event: BaseEvent, task_name: str | None = None
    ) -> None:
        if type(event) not in self.event_handlers:
            logger.warning("No handler registered for event: %s", event)
            return
        for handler in self.event_handlers[type(event)]:
            event_metadata = EventMetadata(event, handler, "", start=datetime.now())
            logger.debug(
                "Start handling event %s with handler '%s'",
                event,
                event_metadata.handler_name,
            )
            with tracer.start_as_current_span(event_metadata.name) as span:
                span.set_attributes(event_metadata.labels_dict())
                if self.keep_event_history is True:
                    self.events.append(event_metadata)
                try:
                    await handler(event)
                    logger.debug(
                        "End   handling event %s with handler '%s'",
                        event,
                        event_metadata.handler_name,
                    )
                except Exception as err:
                    logger.exception("Exception handling event %s", event)
                    event_metadata.error = f"{err}: {traceback.format_exception(err)}"
                    span.set_status(Status(StatusCode.ERROR))
                    continue
                finally:
                    event_metadata.end = datetime.now()
                    duration = (
                        event_metadata.end - event_metadata.start
                    ).total_seconds()
                    if duration > 0.1:
                        perf_logger.warning(
                            f"Event processing for {event_metadata.name} took too long: %.3f seconds",
                            duration,
                        )
                        perf_logger.warning(event_metadata.to_json())
                    events_summary.labels(*event_metadata.labels()).observe(duration)
                    if task_name and task_name in self.tasks:
                        del self.tasks[task_name]

    async def handle_command(self, command: BaseCommand, blocking: bool = True) -> Any:
        try:
            handler = self.command_handlers[type(command)]
        except KeyError:
            raise Exception(
                f"Missing handler in the message bus for command: {command}"
            )

        event_metadata = EventMetadata(command, handler, "", start=datetime.now())
        logger.debug(
            "Start handling command %s with handler '%s'",
            command,
            event_metadata.handler_name,
        )
        # Reset the Trace context on commands to avoid very large traces that cannot be ingested by Tempo, but link it to the previous trace.
        span = tracer.start_span(
            event_metadata.name,
            context=Context(),
            links=[Link(trace.get_current_span().get_span_context())],
        )
        task_name = None
        with trace.use_span(span, end_on_exit=True) as span:
            span.set_attributes(event_metadata.labels_dict())
            if self.keep_event_history is True:
                self.events.append(event_metadata)
            try:
                if blocking:
                    result = await handler(command)
                    return result
                else:
                    task_name = f"{command.__class__.__name__}-{get_random_id()}"
                    self.add_tasks(
                        task_name,
                        asyncio.create_task(
                            handler(command),
                            name=task_name,
                        ),
                    )
            except Exception as err:
                logger.exception("Exception handling command %s", command)
                event_metadata.error = f"{err}: {traceback.format_exception(err)}"
                span.set_status(Status(StatusCode.ERROR))
                raise
            finally:
                logger.debug(
                    "End   handling command %s with handler '%s'",
                    command,
                    event_metadata.handler_name,
                )
                event_metadata.end = datetime.now()
                duration = (event_metadata.end - event_metadata.start).total_seconds()
                if duration > 0.1:
                    perf_logger.warning(
                        f"Event processing for {event_metadata.name} took too long: %.3f seconds",
                        duration,
                    )
                    perf_logger.warning(event_metadata.to_json())
                events_summary.labels(*event_metadata.labels()).observe(duration)
                del event_metadata
                if task_name and task_name in self.tasks:
                    del self.tasks[task_name]

    def wait_for_event(
        self, event_type: Type[EventType], with_parameters: dict | None = None
    ) -> Task[EventType]:
        """
        Register a waiter pending_execution for the wanted event. The returned pending_execution can then be awaited to wait for the event.
        It also optionally check for parameters value if defined.
        """
        event_received: asyncio.Queue = asyncio.Queue(maxsize=1)
        logger.debug("Waiting for event %s", event_type)
        with tracer.start_as_current_span(f"Wait for event {event_type}") as span:
            span.set_attribute("event", event_type.__class__.__name__)

        async def waiter(event: EventType) -> EventType:
            with tracer.start_as_current_span(
                f"Wait for event {event_type}"
            ) as waiter_span:
                waiter_span.set_attribute("event", event.__class__.__name__)
                if with_parameters:
                    match = []
                    for param_name, param_value in with_parameters.items():
                        if (
                            hasattr(event, param_name)
                            and getattr(event, param_name) == param_value
                        ):
                            match.append(event)
                    if len(match) == len(with_parameters):
                        await event_received.put(event)
                    else:
                        logger.debug(
                            f"Event {event} do not match with {with_parameters}"
                        )
                else:
                    await event_received.put(event)
                return event

        self.register_event_handler(event_type, waiter)

        async def waiter_task() -> EventType:
            result = None
            try:
                result = await event_received.get()
            except CancelledError:
                logger.warning("Waiter for %s was canceled", event_type)
                raise
            finally:
                self.event_handlers[event_type].remove(waiter)
            return result

        return create_task(waiter_task())

    def add_tasks(self, task_name: str, task: Task) -> None:
        self.tasks[task_name] = task
        if len(self.tasks) > 1:
            logger.debug("Tasks are running in parallel: %s", self.tasks.keys())
