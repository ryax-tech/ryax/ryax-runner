import asyncio
import functools
import logging
import random
from typing import Callable, Coroutine, Any


def retry_with_backoff(
    retries: int = 5, backoff_in_ms: int = 200, max_backoff_in_ms: int = 60_000
) -> Callable[
    [Callable[..., Coroutine[Any, Any, Any]]], Callable[..., Coroutine[Any, Any, Any]]
]:
    """
    Retry the function with a backoff. Use a retry of -1 for infinite retries.
    Backoff time is computed from this function where x is the current number of retry:
    >>> (min(backoff_in_ms * 2 ** x, max_backoff_in_ms) + random.uniform(0, 1))
    :param retries: number of retries
    :param backoff_in_ms: backoff in milliseconds
    :param max_backoff_in_ms: Maximum backoff time in milliseconds
    """

    def wrapper(
        f: Callable[..., Coroutine[Any, Any, Any]]
    ) -> Callable[..., Coroutine[Any, Any, Any]]:
        @functools.wraps(f)
        async def wrapped(*args: Any, **kwargs: Any) -> Coroutine[Any, Any, Any]:
            x = 0
            logger = logging.getLogger(f.__module__)
            while True:
                try:
                    return await f(*args, **kwargs)
                except Exception as err:
                    if x == retries:
                        logger.warning(
                            f"Failed with error, too many retries ({x}/{retries}): {err}"
                        )
                        raise
                    else:
                        sleep_ms = min(
                            backoff_in_ms * 2**x, max_backoff_in_ms
                        ) + random.uniform(0, 1)
                        await asyncio.sleep(sleep_ms / 1000)
                        x += 1
                        logger.warning(
                            f"Retrying ({x + 1}/{retries}) after {sleep_ms / 1000}s"
                        )

        return wrapped

    return wrapper
