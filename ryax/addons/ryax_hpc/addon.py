from logging import getLogger

from ryax.addons.addon_entities import RyaxAddOn, AddOnParameters, AddOnParameter
from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)


class AddonHpcConfigurationException(Exception):
    pass


logger = getLogger(__name__)


class HpcServiceAddOn(RyaxAddOn):
    name = "hpc"
    technical_name = "hpc"
    version = "0.1"
    parameters = AddOnParameters(
        AddOnParameter(
            id="slurm_args",
            name="slurm_args",
            display_name="Header of the slurm sbatch script",
            help="Head of sbatch script, ex.: \n#!/bin/bash\n#SBATCH --nodes=1\n#SBATCH --tasks=1\n",
            type=ActionIOType.STRING,
            value="#!/bin/bash\n#SBATCH --nodes=1\n#SBATCH --tasks=1\n",
            optional=True,
        ),
        AddOnParameter(
            id="custom_script",
            name="custom_script",
            display_name="Script to run in case of hpc offloading customizations",
            help="Bypass singularity run injected by ryax, run custom user script instead",
            type=ActionIOType.LONGSTRING,
            optional=True,
        ),
        AddOnParameter(
            id="username",
            name="username",
            display_name="Username on the HPC frontend",
            help="",
            type=ActionIOType.STRING,
            optional=True,
        ),
        AddOnParameter(
            id="frontend",
            name="frontend",
            display_name="",
            help="",
            type=ActionIOType.STRING,
            optional=True,
        ),
        AddOnParameter(
            id="password",
            name="password",
            display_name="",
            help="",
            type=ActionIOType.PASSWORD,
            optional=True,
        ),
        AddOnParameter(
            id="private_key",
            name="private_key",
            display_name="",
            help="",
            type=ActionIOType.PASSWORD,
            optional=True,
        ),
        AddOnParameter(
            id="port",
            name="port",
            display_name="",
            help="",
            type=ActionIOType.INTEGER,
            value=22,
        ),
    )

    async def hook_action_before_deploy_on_runner(
        self,
        action_deployment: ActionDeployment,
    ) -> dict:
        """
        Method to be called just before apply the execution on the scheduler
        addon parameters are feed into addon_extra_params
        :param deployment:
        :return: the HPC addon parameters
        """
        action_full_params = self.get_parameters_values(action_deployment)
        return action_full_params

    async def hook_on_start_execution_on_runner(
        self,
        deployment: ActionDeployment,
    ) -> dict:
        """
        Method to be called just before apply the execution on the worker
        addon parameters are feed into addon_extra_params
        :param deployment:
        :return: the HPC addon parameters
        """
        action_full_params = self.get_parameters_values(deployment)
        return action_full_params
