"""
Add-Ons allow to extend Ryax features.

The add-on defines hooks that run during the deployment and execution of the actions.
All parameters are accessible in the action context.
"""

from dataclasses import dataclass, field
from logging import getLogger
from typing import Any, Callable, Optional, Union

from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.application.workflow_result_service import WorkflowResultService
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionIO,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_service import (
    IActionDeploymentService,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.stored_file.stored_file_service import IStoredFileView
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.worker.domain.deployment.deployment_service import IDeploymentService

logger = getLogger(__name__)


@dataclass
class RyaxAddOnNotInstalledError(Exception):
    addon_name: str


@dataclass
class AddOnParameter(ActionIO):
    value: Optional[Any] = None
    is_user_defined: bool = True
    # Set this to true to enable the injection in Ryax action inputs at execution time
    injected_in_action_inputs: bool = False


class AddOnParameters(dict[str, AddOnParameter]):
    def __init__(self, *parameters: Any) -> None:
        for parameter in parameters:
            self[parameter.id] = parameter
        super().__init__()


@dataclass
class AddOnInjectedObject:
    # Name of the object in the user action
    name: str
    # method that should be call to create the object
    handler: Callable[[AddOnParameters], None]


class RyaxAddOn:
    # Unique id
    id: str
    # Unique name use for reference inside the code
    technical_name: str
    # Human-readable name
    name: str
    # version of this add-on (should be semver compliant)
    version: str
    # parameters that might be set at the Add-on level, in Action metadata file, can be overridden at workflow creation time.
    parameters: AddOnParameters = field(default_factory=AddOnParameters)

    def __init__(self, addons: Optional["RyaxAddOns"] = None):
        if addons is not None:
            self.init_addon(addons)

    def init_addon(self, addons: "RyaxAddOns") -> None:
        addons.register(self)

    def has_addon(
        self,
        action: Union[ActionDeployment, WorkflowAction, Deployment],
    ) -> bool:
        return action.addons is not None and self.technical_name in action.addons

    def get_parameters_values(
        self, action: ActionDeployment | WorkflowAction | Deployment
    ) -> dict:
        action_full_params: dict = {}
        action_params = action.addons.get(self.technical_name)
        if action_params is not None:
            for parameter_name, parameter in self.parameters.items():
                action_full_params[parameter_name] = action_params.get(
                    parameter_name, parameter.value
                )
        return action_full_params

    async def hook_on_startup(
        self,
        configuration: dict,
        message_bus: IMessageBus,
        workflow_result_service: WorkflowResultService,
        user_auth_service: UserAuthService,
        stored_files_service: IStoredFileView,
    ) -> None:
        pass

    async def hook_action_deployment_after_deploy_on_runner(
        self,
        action_deployment: ActionDeployment,
        deploy_service: IActionDeploymentService,
    ) -> None:
        pass

    async def hook_action_after_deploy_on_worker(
        self, deployment: Deployment, deploy_service: IDeploymentService
    ) -> None:
        pass

    async def hook_action_deployment_delete_on_runner(
        self,
        action_deployment: ActionDeployment,
        deploy_service: IActionDeploymentService,
    ) -> None:
        pass

    async def hook_action_deployment_delete_on_worker(
        self,
        action_deployment: Deployment,
        deploy_service: IDeploymentService,
    ) -> None:
        pass

    async def hook_workflow_deployment_is_started(
        self, workflow_deployment: WorkflowDeployment
    ) -> None:
        pass

    async def hook_workflow_deployment_is_stopped(
        self, workflow_deployment: WorkflowDeployment
    ) -> None:
        pass

    async def hook_on_cleanup(self) -> None:
        pass

    async def hook_action_before_deploy_on_worker(
        self, action_deployment: Deployment, deploy_service: IDeploymentService
    ) -> None:
        pass

    async def hook_action_before_deploy_on_runner(
        self, action_deployment: ActionDeployment
    ) -> dict:
        return {}

    async def hook_on_start_execution_on_worker(self, deployment: Deployment) -> None:
        pass

    async def hook_on_execution_start_on_runner(
        self, execution: ExecutionConnection
    ) -> dict:
        return {}

    async def hook_on_create_execution_on_runner(
        self, inputs: dict, project_id: str
    ) -> dict:
        return {}


@dataclass
class RyaxAddOns:
    add_ons: list[RyaxAddOn] = field(default_factory=list)

    def get_addon(self, addon_name: str) -> RyaxAddOn:
        for addon in self.add_ons:
            if addon.name == addon_name:
                return addon
        raise RyaxAddOnNotInstalledError(addon_name)

    def register(self, add_on: RyaxAddOn) -> None:
        self.add_ons.append(add_on)

    async def run_hook_on_startup(
        self,
        configuration: dict,
        message_bus: IMessageBus,
        workflow_result_service: WorkflowResultService,
        user_auth_service: UserAuthService,
        stored_files_service: IStoredFileView,
    ) -> None:
        for add_on in self.add_ons:
            await add_on.hook_on_startup(
                configuration,
                message_bus,
                workflow_result_service,
                user_auth_service,
                stored_files_service,
            )

    async def run_hook_on_cleanup(self) -> None:
        for add_on in self.add_ons:
            await add_on.hook_on_cleanup()

    def has_addon(
        self,
        addon_name: str,
        action: WorkflowAction,
    ) -> bool:
        return self.get_addon(addon_name).has_addon(action)

    def get_addons_not_injectable_inputs(
        self,
        action: WorkflowAction,
    ) -> list[str]:
        injectable_inputs = []
        for addon in self.add_ons:
            if self.has_addon(addon.name, action):
                for parameter_name, parameter_value in addon.parameters.items():
                    if parameter_value.injected_in_action_inputs is False:
                        injectable_inputs.append(parameter_name)
        return injectable_inputs

    async def run_hook_action_after_deploy_on_runner(
        self,
        action_deployment: ActionDeployment,
        deploy_service: IActionDeploymentService,
    ) -> None:
        for add_on in self.add_ons:
            if add_on.has_addon(action_deployment):
                await add_on.hook_action_deployment_after_deploy_on_runner(
                    action_deployment, deploy_service
                )

    async def run_hook_action_deployment_delete_on_runner(
        self,
        action_deployment: ActionDeployment,
        deploy_service: IActionDeploymentService,
    ) -> None:
        for add_on in self.add_ons:
            if add_on.has_addon(action_deployment):
                await add_on.hook_action_deployment_delete_on_runner(
                    action_deployment, deploy_service
                )

    async def run_hook_action_deployment_delete_on_worker(
        self,
        action_deployment: Deployment,
        deploy_service: IDeploymentService,
    ) -> None:
        for add_on in self.add_ons:
            if add_on.has_addon(action_deployment):
                await add_on.hook_action_deployment_delete_on_worker(
                    action_deployment, deploy_service
                )

    async def run_hook_workflow_deployment_is_started(
        self, workflow_deployment: WorkflowDeployment
    ) -> None:
        for add_on in self.add_ons:
            await add_on.hook_workflow_deployment_is_started(workflow_deployment)

    async def run_hook_workflow_deployment_is_stopped(
        self, workflow_deployment: WorkflowDeployment
    ) -> None:
        for add_on in self.add_ons:
            await add_on.hook_workflow_deployment_is_stopped(workflow_deployment)

    async def run_hook_action_before_deploy_on_runner(
        self, action_deployment: ActionDeployment
    ) -> dict:
        addon_extra_params = {}
        for add_on in self.add_ons:
            if add_on.has_addon(action_deployment):
                addon_extra_params[
                    add_on.technical_name
                ] = await add_on.hook_action_before_deploy_on_runner(action_deployment)
        return addon_extra_params

    async def run_hook_action_before_deploy_on_worker(
        self, deployment: Deployment, deploy_service: IDeploymentService
    ) -> None:
        logger.debug(f"Addons on deployment: {deployment.addons}")
        for add_on in self.add_ons:
            if add_on.has_addon(deployment):
                logger.debug(
                    f"Calling hook_action_before_deploy_on_worker for addon {add_on.technical_name}"
                )
                await add_on.hook_action_before_deploy_on_worker(
                    deployment, deploy_service
                )

    async def run_hook_action_after_deploy_on_worker(
        self, deployment: Deployment, deploy_service: IDeploymentService
    ) -> None:
        for add_on in self.add_ons:
            if add_on.has_addon(deployment):
                await add_on.hook_action_after_deploy_on_worker(
                    deployment, deploy_service
                )

    async def run_hook_execution_on_start_on_runner(
        self, execution: ExecutionConnection
    ) -> dict:
        addon_extra_params = {}
        for add_on in self.add_ons:
            if execution.action_deployment is not None and add_on.has_addon(
                execution.action_deployment
            ):
                addon_extra_params[
                    add_on.technical_name
                ] = await add_on.hook_on_execution_start_on_runner(execution)
        return addon_extra_params

    async def run_hook_execution_on_start_on_worker(
        self, deployment: Deployment
    ) -> None:
        for add_on in self.add_ons:
            if add_on.has_addon(deployment):
                await add_on.hook_on_start_execution_on_worker(deployment)

    async def run_hook_on_create_execution_on_runner(
        self, workflow_action: WorkflowAction, project_id: str, inputs: dict
    ) -> dict:
        """
        Allow to inject inputs in the execution dynamically at execution creation time.
        :param inputs: already resolved inputs for the execution
        :param project_id: the current project id
        :param workflow_action: the workflow action use to create the execution
        :return: the additional inputs
        """
        addon_extra_inputs: dict[str, str | float | bool | bytes] = {}
        for add_on in self.add_ons:
            if add_on.has_addon(workflow_action):
                addon_extra_inputs = (
                    addon_extra_inputs
                    | await add_on.hook_on_create_execution_on_runner(
                        inputs, project_id
                    )
                )
        return addon_extra_inputs
