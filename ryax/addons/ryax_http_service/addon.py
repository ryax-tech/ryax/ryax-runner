from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_service import (
    IActionDeploymentService,
)
from ryax.addons.addon_entities import (
    AddOnParameter,
    AddOnParameters,
    RyaxAddOn,
)
from ryax.runner.infrastructure.action_deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)
from ryax.common.infrastructure.kubernetes.k8s_utils import (
    safe_resource_name,
)


def generate_user_api_endpoint(
    user_api_prefix: str, project_id: str, user_defined_endpoint: str
) -> str:
    try:
        url_part = [user_api_prefix, project_id, user_defined_endpoint]
        return "/" + "/".join(s.strip("/") for s in url_part)
    except AttributeError:
        raise InvalidUserDefinedEndpointError(
            f"Invalid endpoint provided: {user_defined_endpoint}"
        )


class InvalidUserDefinedEndpointError(Exception):
    pass


class InvalidAddOnConfigurationError(Exception):
    pass


# FIXME: Use the K8S engine directly and not the deployment service
class HttpServiceAddOn(RyaxAddOn):
    name = "HttpService"
    technical_name = "http_service"
    version = "0.1"
    parameters = AddOnParameters(
        AddOnParameter(
            id="endpoint_prefix",
            name="endpoint_prefix",
            type=ActionIOType.STRING,
            display_name="Endpoint Prefix provided by the user",
            help="User defined prefix. The full prefix with Ryax instance prefix added will be exposed is user's action in the `ryax_endpoint_prefix` variable",
            optional=False,
        ),
        AddOnParameter(
            id="ryax_endpoint_prefix",
            name="ryax_endpoint_prefix",
            type=ActionIOType.STRING,
            display_name="Full Endpoint Prefix with Ryax instance included",
            help="Generated from user defined prefix and Ryax instance prefix. Expose is user's action",
            optional=False,
            injected_in_action_inputs=True,
        ),
        AddOnParameter(
            id="ryax_endpoint_port",
            name="ryax_endpoint_port",
            display_name="Service Port",
            help="Clone of port injected in Ryax actions.",
            type=ActionIOType.INTEGER,
            optional=False,
            injected_in_action_inputs=True,
        ),
        AddOnParameter(
            id="base_url",
            name="base_url",
            display_name="Base URL",
            help="The base URL your endpoint will be on",
            type=ActionIOType.STRING,
            is_user_defined=False,
            optional=False,
        ),
        AddOnParameter(
            id="strip_path_prefix",
            name="strip_path_prefix",
            display_name="Strip Prefix Path",
            help="If enabled, strip the prefix path of the request",
            type=ActionIOType.BOOLEAN,
            value=False,
            optional=False,
        ),
    )

    def generate_endpoint_prefix(
        self, user_defined_endpoint: str, project_id: str
    ) -> str:
        user_api_prefix = str(self.parameters["base_url"].value)
        if user_defined_endpoint is None:
            raise InvalidUserDefinedEndpointError()
        return generate_user_api_endpoint(
            user_api_prefix, project_id, user_defined_endpoint
        )

    def _get_resource_name(self, action_deployment: ActionDeployment) -> str:
        return safe_resource_name(
            action_deployment.id,
            suffix="-http-service",
            name=action_deployment.action_definition.technical_name,
            version=action_deployment.action_definition.version,
        )

    async def hook_action_deployment_after_deploy_on_runner(
        self,
        action_deployment: ActionDeployment,
        deploy_service: IActionDeploymentService,
    ) -> None:
        if not isinstance(deploy_service, K8SActionDeploymentService):
            raise NotImplementedError()
        resource_name = self._get_resource_name(action_deployment)
        action_deployment_id = action_deployment.id
        user_defined_endpoint = self.get_parameters_values(action_deployment)[
            "endpoint_prefix"
        ]
        prefix_path = self.generate_endpoint_prefix(
            user_defined_endpoint, action_deployment.project_id
        )
        user_namespace = deploy_service.engine.user_namespace
        parameters = self.get_parameters_values(action_deployment)
        port = int(parameters["ryax_endpoint_port"])
        strip_prefix = bool(parameters["strip_path_prefix"])

        if strip_prefix is True:
            middleware = {
                "apiVersion": "traefik.containo.us/v1alpha1",
                "kind": "Middleware",
                "metadata": {
                    "name": resource_name,
                    "labels": {
                        "ryax.tech/deployment": action_deployment_id,
                        "ryax.tech/addons": self.name,
                    },
                },
                "spec": {
                    "stripPrefix": {
                        "prefixes": [
                            prefix_path,
                        ]
                    }
                },
            }
            await deploy_service.engine.apply_middleware_definition(middleware)

        ingress = {
            "apiVersion": "networking.k8s.io/v1",
            "kind": "Ingress",
            "metadata": {
                "name": resource_name,
                "labels": {
                    "ryax.tech/deployment": action_deployment_id,
                    "ryax.tech/addons": self.name,
                },
                "annotations": (
                    {
                        "traefik.ingress.kubernetes.io/router.middlewares": f"{user_namespace}-{resource_name}@kubernetescrd"
                    }
                    if strip_prefix
                    else {}
                ),
            },
            "spec": {
                "rules": [
                    {
                        "http": {
                            "paths": [
                                {
                                    "path": prefix_path,
                                    "pathType": "Prefix",
                                    "backend": {
                                        "service": {
                                            "name": resource_name,
                                            "port": {
                                                "number": port,
                                            },
                                        }
                                    },
                                }
                            ]
                        }
                    }
                ]
            },
        }
        await deploy_service.engine.apply_ingress_definition(ingress)

        service = {
            "apiVersion": "v1",
            "kind": "Service",
            "metadata": {
                "name": resource_name,
                "labels": {
                    "ryax.tech/deployment": action_deployment_id,
                    "ryax.tech/addons": self.name,
                },
            },
            "spec": {
                "ports": [
                    {
                        "name": "http",
                        "port": port,
                    },
                ],
                "selector": {"action_deployment_id": action_deployment_id},
            },
        }
        await deploy_service.engine.apply_service_definition(service)

    async def hook_action_deployment_delete_on_runner(
        self,
        action_deployment: ActionDeployment,
        deploy_service: IActionDeploymentService,
    ) -> None:
        resource_name = self._get_resource_name(action_deployment)
        if isinstance(deploy_service, K8SActionDeploymentService):
            await deploy_service.engine.delete_ingress(resource_name)
            await deploy_service.engine.delete_middleware(resource_name)
            await deploy_service.engine.delete_service(resource_name)
        else:
            raise NotImplementedError()

    async def hook_on_create_execution_on_runner(
        self, inputs: dict, project_id: str
    ) -> dict:
        """Inject the ryax_endpoint_prefix
        :param inputs: already resolved inputs
        :param project_id: project id
        """
        user_defined_endpoint = inputs["endpoint_prefix"]
        return {
            "ryax_endpoint_prefix": self.generate_endpoint_prefix(
                user_defined_endpoint, project_id
            )
        }

    async def hook_on_startup(self, configuration: dict, *args) -> None:  # type: ignore # noqa
        try:
            base_url = configuration["user_api"]["prefix"]
            if base_url is None:
                raise
        except Exception:
            raise InvalidAddOnConfigurationError()
        self.parameters["base_url"].value = base_url
