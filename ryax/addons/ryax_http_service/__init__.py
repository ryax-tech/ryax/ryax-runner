from .addon import HttpServiceAddOn

__all__ = ["HttpServiceAddOn"]

__version__ = "0.0.1"
