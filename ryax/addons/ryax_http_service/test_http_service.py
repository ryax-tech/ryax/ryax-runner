from unittest import mock

import pytest

from ryax.addons.ryax_http_service import HttpServiceAddOn
from ryax.addons.ryax_http_service.addon import (
    InvalidUserDefinedEndpointError,
    generate_user_api_endpoint,
)
from ryax.common.domain.internal_messaging.event_publisher import IPublisherService
from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.application.workflow_result_service import WorkflowResultService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionExecutionType,
)
from ryax.addons.addon_entities import RyaxAddOns
from ryax.runner.domain.stored_file.stored_file_service import IStoredFileView
from ryax.runner.infrastructure.action_deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)
from ryax.runner.infrastructure.action_deployment.k8s.k8s_engine import K8sEngine


@pytest.mark.parametrize(
    "user_api_prefix,project_id,user_defined_endpoint,result",
    [
        pytest.param("/user-api", "123", None, None, id="No endpoint"),
        pytest.param("/user-api", "123", "/toto", "/user-api/123/toto", id="simple"),
        pytest.param(
            "/user-api/", "123", "/toto/", "/user-api/123/toto", id="trailing slash"
        ),
        pytest.param(
            "/user-api/v1",
            "123",
            "/toto",
            "/user-api/v1/123/toto",
            id="user-api with 2 part",
        ),
        pytest.param(
            "/user-api",
            "123",
            "/toto/tata",
            "/user-api/123/toto/tata",
            id="user endpoint with 2 part",
        ),
        pytest.param(
            "/user-api/v1",
            "123",
            "/toto/tata",
            "/user-api/v1/123/toto/tata",
            id="both endpoint and user-api with 2 part",
        ),
    ],
)
def test_user_web_access_endpoint(
    user_api_prefix: str, project_id: str, user_defined_endpoint: str, result: str
) -> None:
    if not result:
        with pytest.raises(InvalidUserDefinedEndpointError):
            generate_user_api_endpoint(
                user_api_prefix, project_id, user_defined_endpoint
            )
    else:
        assert (
            generate_user_api_endpoint(
                user_api_prefix, project_id, user_defined_endpoint
            )
            == result
        )


@pytest.fixture()
def app_container() -> object:
    container = ApplicationContainer()
    container.configuration.from_dict({"user_api": {"prefix": "toto"}})
    yield container
    container.unwire()


@pytest.fixture()
# Move addons tests to the addons itself
def addons(app_container: ApplicationContainer):
    addons = RyaxAddOns()
    addons.register(HttpServiceAddOn())
    app_container.addons.override(addons)
    return addons


@pytest.mark.parametrize(
    "addon_params",
    [
        pytest.param(
            {
                "http_service": {
                    "ryax_endpoint_port": 8080,
                    "endpoint_prefix": "/test",
                    "strip_path_prefix": True,
                }
            },
            id="http service with strip prefix",
        ),
        pytest.param(
            {
                "http_service": {
                    "ryax_endpoint_port": 8080,
                    "endpoint_prefix": "/test",
                }
            },
            id="http service without strip prefix",
        ),
        pytest.param({}, id="not http service"),
    ],
)
async def test_apply_action_deployment_hook(
    addon_params: dict, addons: RyaxAddOns, app_container: ApplicationContainer
):
    registry = "registry.ryaxns:5000"
    user_namespace = "ryaxns-execs"
    action_deployment = ActionDeployment(
        action_definition=mock.MagicMock(
            ActionDefinition,
            technical_name="a",
            version="1",
            id="123",
            kind=ActionKind.TRIGGER,
            container_image="",
        ),
        id="1",
        execution_type=ActionExecutionType.GRPC_V1,
        project_id="",
        addons=addon_params,
        node_pool=None,
    )
    engine: K8sEngine = mock.MagicMock(K8sEngine, user_namespace=user_namespace)
    service = K8SActionDeploymentService(
        publisher=mock.MagicMock(IPublisherService),
        querier=mock.MagicMock(),
        ryax_registry=registry,
        engine=engine,
        user_action_log_level="INFO",
        action_logs_service=None,
    )
    await addons.run_hook_on_startup(
        app_container.configuration(),
        app_container.message_bus(),
        mock.MagicMock(WorkflowResultService),
        mock.MagicMock(UserAuthService),
        mock.MagicMock(IStoredFileView),
    )
    await addons.run_hook_action_after_deploy_on_runner(action_deployment, service)
    if addon_params:
        engine.apply_ingress_definition.assert_called_once()
        if addon_params.get("strip_path_prefix"):
            engine.apply_middleware_definition.assert_called_once()
