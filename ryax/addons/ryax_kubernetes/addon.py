from logging import getLogger

from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.addons.addon_entities import (
    AddOnParameter,
    AddOnParameters,
    RyaxAddOn,
)
from ryax.worker.domain.deployment.deployment_entities import Deployment
from ryax.worker.domain.deployment.deployment_service import IDeploymentService
from ryax.worker.infrastructure.deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)

logger = getLogger(__name__)


class KubernetesAddOnError(Exception):
    pass


class KubernetesAddOn(RyaxAddOn):
    name = "KubernetesAddon"
    technical_name = "kubernetes"
    version = "0.0.4"
    parameters = AddOnParameters(
        AddOnParameter(
            id="labels",
            name="Labels",
            type=ActionIOType.STRING,
            display_name="Kubernetes deployment metadata labels",
            help="Add kubernetes labels on the deployment pod template, labels come in a single string respecting format label1=value1,label2=value2",
            optional=True,
        ),
        AddOnParameter(
            id="annotations",
            name="Annotations",
            type=ActionIOType.STRING,
            display_name="Kubernetes deployment metadata annotations",
            help="Add kubernetes annotations on the deployment pod template, annotations come in a single string respecting format label1=value1,label2=value2",
            optional=True,
        ),
        AddOnParameter(
            id="node_selector",
            name="Node Selector",
            type=ActionIOType.STRING,
            display_name="Kubernetes deployment node selector labels",
            help="Add kubernetes node selector on the deployment pod template, node selector labels come in a single string respecting format label1=value1,label2=value2",
            optional=True,
        ),
        AddOnParameter(
            id="service_account_name",
            name="service_account_name",
            type=ActionIOType.STRING,
            display_name="Kubernetes add service_account_name",
            help="Add kubernetes service_account_name on the deployment pod template, a simple string with the account name, account should be created by an admin user",
            optional=True,
        ),
        AddOnParameter(
            id="service_name",
            name="Service Name",
            type=ActionIOType.STRING,
            display_name="A service name to associate to the current pod",
            help="Add kubernetes service to the current action",
            optional=True,
        ),
        AddOnParameter(
            id="service_port",
            name="Service Port",
            type=ActionIOType.INTEGER,
            display_name="Kubernetes port to associate with the addon service, see Service Name",
            help="Open kubernetes service port",
            optional=True,
        ),
        AddOnParameter(
            id="runtime_class",
            name="Runtime Class",
            type=ActionIOType.STRING,
            display_name="Set the RuntimeClassName parameter of you pod to use alternate runtime like urunc of Kata",
            help="You can get the list of Runtime class using : kubectl get runtimeclasses",
            optional=True,
        ),
    )

    async def hook_action_deployment_delete_on_worker(
        self,
        deployment: Deployment,
        deploy_service: IDeploymentService,
    ) -> None:
        action_full_params = self.get_parameters_values(deployment)
        service_name = action_full_params["service_name"]
        if not isinstance(deploy_service, K8SActionDeploymentService):
            raise NotImplementedError()
        else:
            if service_name is not None:
                await deploy_service.engine.delete_service(service_name)

    async def hook_action_before_deploy_on_worker(
        self,
        action_deployment: Deployment,
        deploy_service: IDeploymentService,
    ) -> None:
        logger.debug("Entering kubernetes addon before deploy hook")

        if not isinstance(deploy_service, K8SActionDeploymentService):
            raise NotImplementedError(
                "Kubernetes addon only supports k8s action deployment"
            )

        action_full_params = self.get_parameters_values(action_deployment)

        logger.debug(
            f"Kubernetes Addon creating service {action_full_params['service_name']}:{action_full_params['service_port']}"
        )

        if action_full_params["service_name"] is not None:
            action_deployment_id = action_deployment.id

            # use default spark port
            if action_full_params["service_port"] is None:
                raise ValueError(
                    "Need to specify the service_port when service_name is set!"
                )

            service_port = int(action_full_params["service_port"])

            service = {
                "apiVersion": "v1",
                "kind": "Service",
                "metadata": {
                    "name": action_full_params["service_name"],
                    "deployment": action_deployment_id,
                },
                "spec": {
                    "clusterIP": "None",
                    "selector": {
                        "ryax.tech/kubernetes-service": action_full_params[
                            "service_name"
                        ],
                    },
                    "ports": [
                        {
                            "protocol": "TCP",
                            "port": service_port,
                        },
                    ],
                },
            }
            await deploy_service.engine.apply_service_definition(service)

    def _from_str_to_dict(self, input_str: str) -> dict:
        """
        Convert from format "key1=val1,key2=val2" to a python
        dict.
        """
        if input_str is None:
            return None
        parsed_dict = {}
        for item in input_str.strip().split(","):
            if "=" in item:
                name, value = item.split("=")
                parsed_dict[name.strip()] = value.strip()
            else:
                raise ValueError(
                    f"Error while parsing labels. Expected format is 'key1=val1,key2=val2'. Found: {input_str}"
                )
        return parsed_dict

    async def hook_action_before_deploy_on_runner(
        self,
        action_deployment: ActionDeployment,
    ) -> dict:
        action_full_params = self.get_parameters_values(action_deployment)

        action_deployment.addons["kubernetes"]["labels"] = self._from_str_to_dict(
            action_full_params["labels"]
        )

        if (
            "service_name" in action_full_params
            and action_full_params["service_name"] is not None
        ):
            # use default spark port
            if action_full_params["service_port"] is None:
                if (
                    "service_port" not in action_full_params
                    or action_full_params["service_port"] is None
                ):
                    raise ValueError(
                        "Need to specify the service_port when service_name is set!"
                    )

            if action_deployment.addons["kubernetes"]["labels"] is None:
                action_deployment.addons["kubernetes"]["labels"] = {}

            action_deployment.addons["kubernetes"]["labels"][  # type: ignore
                "ryax.tech/kubernetes-service"
            ] = action_full_params["service_name"]

        action_deployment.addons["kubernetes"]["annotations"] = self._from_str_to_dict(
            action_full_params["annotations"]
        )

        action_deployment.addons["kubernetes"][
            "node_selector"
        ] = self._from_str_to_dict(action_full_params["node_selector"])

        action_deployment.addons["kubernetes"][
            "service_account_name"
        ] = action_full_params["service_account_name"]

        action_deployment.addons["kubernetes"]["service_name"] = action_full_params[
            "service_name"
        ]

        if (
            "service_port" in action_full_params
            and action_full_params["service_port"] is not None
        ):
            action_deployment.addons["kubernetes"]["service_port"] = int(action_full_params["service_port"])  # type: ignore

        if (
            "runtime_class" in action_full_params
            and action_full_params["runtime_class"] is not None
        ):
            action_deployment.addons["kubernetes"][
                "runtime_class"
            ] = action_full_params["runtime_class"]

        return action_deployment.addons["kubernetes"]
