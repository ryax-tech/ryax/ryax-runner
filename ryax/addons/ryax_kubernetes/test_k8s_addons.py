from unittest import mock
import uuid
import pytest_asyncio

from ryax.addons.ryax_kubernetes import KubernetesAddOn
from unittest.mock import MagicMock

import kubernetes_asyncio as k8s
import os
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.worker.domain.deployment.deployment_entities import Deployment, Kind
from ryax.worker.domain.deployment.deployment_values import ExecutionType
from ryax.worker.domain.node_pool.node_pool_entities import NodePool
from ryax.worker.infrastructure.deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import K8sEngine
from datetime import datetime


def _todict(obj):
    if hasattr(obj, "attribute_map"):
        result = {}
        for k, v in getattr(obj, "attribute_map").items():
            val = getattr(obj, k)
            if val is not None:
                result[v] = _todict(val)
        return result
    elif isinstance(obj, list):
        return [_todict(x) for x in obj]
    elif isinstance(obj, datetime):
        return str(obj)
    else:
        return obj


@pytest_asyncio.fixture
async def k8s_engine() -> K8sEngine:
    namespace = f"test-{uuid.uuid4()}"
    engine = K8sEngine(
        user_namespace=namespace,
        message_bus=MagicMock(IMessageBus),
        loki_url=os.environ.get("RYAX_LOKI_API_URL"),
    )
    await engine.init()
    await engine.create_user_namespace()
    yield engine
    try:
        async with k8s.client.ApiClient() as api:
            core_api = k8s.client.CoreV1Api(api)
            await core_api.delete_namespace(name=engine.user_namespace)
    except Exception:
        pass


def new_deployment():
    return Deployment(
        name="a",
        version="1",
        id="123",
        kind=Kind.TRIGGER,
        container_image="myimage:latest",
        resources=None,
        execution_type=ExecutionType.GRPC_V1,
        deployment_type="",
        internal_endpoint="",
        log_level="",
        addons={},
        node_pool=NodePool(id="1", name="1", selector={"a": "b"}),
    )


async def test_all(k8s_engine):
    service = K8SActionDeploymentService(
        engine=k8s_engine,
        tasks_service=mock.MagicMock(),
        default_grpc_port_inside_actions=8081,
        site_name="mysite",
        recommendation_service=mock.MagicMock(),
        resource_recommendation_enabled=False,
        action_pull_secret_name="toto",
    )

    parameters = {
        "labels": "label1=value1,label2=value2",
        "annotations": "annotation1=value1,annotation2=value2",
        "service_account_name": "bonenfant",
        "service_name": "sparkpi",
        "service_port": 4041,
        "node_selector": "location=here,resource=big",
        "runtime_class": "urunc",
    }
    action_deployment = new_deployment()
    action_deployment.addons = {
        "kubernetes": parameters,
    }
    kube_addon = KubernetesAddOn()
    addon_extra_args = await kube_addon.hook_action_before_deploy_on_runner(
        action_deployment
    )
    assert "labels" in addon_extra_args
    assert "annotations" in addon_extra_args
    assert "service_account_name" in addon_extra_args
    assert "node_selector" in addon_extra_args
    assert "service_name" in addon_extra_args
    assert "service_port" in addon_extra_args
    assert "runtime_class" in addon_extra_args

    # respect the format on addon_entities.py::run_hook_action_before_deploy where each addon has a separated dict
    # for this reason we have {"kubernetes": addon_extra_args}
    await service.apply(
        action_deployment,
    )

    kube_deployment = await k8s_engine.get_deployment(action_deployment.id)

    # Check all addon parameters become environment variables
    for envvar in kube_deployment.spec.template.spec.containers[0].env:
        v = _todict(envvar)
        if "RYAX_USER_DEFINED_KUBERNETES_" in v["name"]:
            assert (
                v["name"].replace("RYAX_USER_DEFINED_KUBERNETES_", "").lower()
                in parameters
            )

    # import json
    # print(json.dumps(_todict(kube_deployment.spec.template.metadata), indent=2))

    # check all labels are there
    assert "label1" in kube_deployment.spec.template.metadata.labels
    assert kube_deployment.spec.template.metadata.labels["label1"] == "value1"
    assert "label2" in kube_deployment.spec.template.metadata.labels
    assert kube_deployment.spec.template.metadata.labels["label2"] == "value2"
    assert (
        "ryax.tech/kubernetes-service" in kube_deployment.spec.template.metadata.labels
    )
    assert (
        kube_deployment.spec.template.metadata.labels["ryax.tech/kubernetes-service"]
        == parameters["service_name"]
    )

    # check all annotations are there
    assert "annotation1" in kube_deployment.spec.template.metadata.annotations
    assert kube_deployment.spec.template.metadata.annotations["annotation1"] == "value1"
    assert "annotation2" in kube_deployment.spec.template.metadata.annotations
    assert kube_deployment.spec.template.metadata.annotations["annotation2"] == "value2"

    # check service_account_name
    assert (
        parameters["service_account_name"]
        == kube_deployment.spec.template.spec.service_account_name
    )

    # Check pull secret
    assert (
        kube_deployment.spec.template.spec.image_pull_secrets[0].name
        == service.action_pull_secret_name
    )

    # Check node selector
    assert kube_deployment.spec.template.spec.node_selector["location"] == "here"
    assert kube_deployment.spec.template.spec.node_selector["resource"] == "big"

    # Check service
    assert kube_deployment.spec.template.spec.node_selector["location"] == "here"
    assert kube_deployment.spec.template.spec.node_selector["resource"] == "big"

    # Check runtime class
    assert kube_deployment.spec.template.spec.runtime_class_name == "urunc"

    await kube_addon.hook_action_before_deploy_on_worker(
        action_deployment, deploy_service=service
    )

    kube_headless_service = await k8s_engine._get_service(
        service_id=parameters["service_name"]
    )

    # To debug the sent structure use the code below
    # import json
    # print(json.dumps(_todict(kube_headless_service), indent=2))

    assert kube_headless_service.spec.cluster_ip == "None"

    assert kube_headless_service.spec.ports[0].port == parameters["service_port"]
    assert (
        kube_headless_service.spec.selector["ryax.tech/kubernetes-service"]
        == parameters["service_name"]
    )

    await kube_addon.hook_action_deployment_delete_on_worker(action_deployment, service)
    assert await k8s_engine._get_service(service_id=parameters["service_name"]) is None
