from datetime import datetime
from unittest import mock

import pytest

import ryax.addons.ryax_http_api.api_gateway
from ryax.addons.ryax_http_api import HttpAPIAddOn
from ryax.addons.ryax_http_api.api_gateway import (
    APIGatewayMalformedEndpoint,
    APIGatewayUnsupportedMethodError,
)
from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.application.workflow_result_service import WorkflowResultService
from ryax.runner.container import ApplicationContainer
from ryax.runner.application.setup import setup as applicaiton_setup
from ryax.addons.addon_entities import RyaxAddOns
from ryax.runner.domain.stored_file.stored_file_service import IStoredFileView
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)


@pytest.fixture()
def app_container():
    container = ApplicationContainer()
    container.configuration.from_dict(
        {"user_api": {"prefix": "/toto"}, "api": {"base_url": "/base"}}
    )
    applicaiton_setup(container)
    yield container
    container.unwire()


@pytest.fixture()
# Move addons tests to the addons itself
def addons(app_container):
    addons = RyaxAddOns()
    ryax.addons.ryax_http_api.api_gateway._start_server = mock.MagicMock
    http_api_addon = HttpAPIAddOn()
    addons.register(http_api_addon)
    app_container.addons.override(addons)
    return addons


async def init(
    app_container, addons: RyaxAddOns, addon_params: dict
) -> WorkflowDeployment:
    workflow = WorkflowDeployment(
        name="wf name",
        id="wf",
        project_id="project_id",
        workflow_definition_id="workflow_definition_id",
        actions=[
            WorkflowAction(
                addons=addon_params,
                id="1",
                workflow_deployment_id="",
                workflow_definition_id="",
                definition=None,
                next_actions=[],
                id_in_studio="",
                reference_inputs=[],
                static_inputs=[],
                constraints=None,
                objectives=None,
            )
        ],
        creation_time=datetime.fromtimestamp(0),
    )

    await addons.run_hook_on_startup(
        app_container.configuration(),
        app_container.message_bus(),
        mock.MagicMock(WorkflowResultService),
        mock.MagicMock(UserAuthService),
        mock.MagicMock(IStoredFileView),
    )
    return workflow


@pytest.mark.parametrize(
    "addon_params",
    [
        pytest.param(
            {
                "http_api": {
                    "path": "/users",
                    "http_method": "GET",
                }
            },
            id="valid path without template",
        ),
        pytest.param(
            {
                "http_api": {
                    "path": "/users/{user_id}",
                    "http_method": "GET",
                }
            },
            id="valid path with template",
        ),
    ],
)
async def test_hook_workflow_deployment_is_started(
    addon_params: dict, addons: RyaxAddOns, app_container
):
    workflow = await init(app_container, addons, addon_params)
    await addons.run_hook_workflow_deployment_is_started(workflow)


async def test_hook_workflow_deployment_is_started_with_invalid_method(
    addons: RyaxAddOns, app_container
):
    addon_params = {
        "http_api": {
            "path": "/users",
            "http_method": "NOT EXISTS",
        }
    }
    workflow = await init(app_container, addons, addon_params)
    with pytest.raises(APIGatewayUnsupportedMethodError):
        await addons.run_hook_workflow_deployment_is_started(workflow)


@pytest.mark.parametrize(
    "addon_params",
    [
        pytest.param(
            {
                "http_api": {
                    "path": "/users/user_id}",
                    "http_method": "GET",
                }
            },
            id="invalid template malformed",
        ),
        pytest.param(
            {
                "http_api": {
                    "path": "/users/{user_id",
                    "http_method": "GET",
                }
            },
            id="invalid template malformed",
        ),
    ],
)
async def test_hook_workflow_deployment_is_started_with_invalid_endpoint(
    addon_params: dict, addons: RyaxAddOns, app_container
):
    workflow = await init(app_container, addons, addon_params)
    with pytest.raises(APIGatewayMalformedEndpoint):
        await addons.run_hook_workflow_deployment_is_started(workflow)
