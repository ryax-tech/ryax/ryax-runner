import asyncio
import tempfile
from unittest import mock

import pytest
from sqlalchemy.orm import clear_mappers
from starlette.testclient import TestClient

from ryax.addons.ryax_http_api.api_gateway import (
    FastAPIApiGateway,
    HTTPEndpoint,
    HTTPMethod,
    HTTPPath,
    HTTPSuccessCode,
)
from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDynamicOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    DynamicOutputOrigin,
)
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    InitializeExecutionResultCommand,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultFinishedEvent,
)
from ryax.runner.domain.execution_result.execution_result_repository import (
    IExecutionResultRepository,
)
from ryax.common.domain.internal_messaging.base_messages import BaseEvent
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_result.workflow_result import WorkflowResultView
from ryax.runner.domain.workflow_run.workflow_run_entities import (
    WorkflowRun,
    WorkflowRunState,
)
from ryax.runner.domain.workflow_run.workflow_run_events import (
    WorkflowRunIsCompletedEvent,
)
from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.runner.infrastructure.database.mapper import map_orm
from ryax.runner.infrastructure.database.metadata import metadata
from ryax.runner.infrastructure.database.repositories.workflow_run_repository import (
    DatabaseWorkflowRunRepository,
)
from ryax.runner.infrastructure.encryption.encryption_service import EncryptionService


@pytest.fixture()
def app_container():
    container = ApplicationContainer()

    container.configuration.set("storage.local.type", "posix")
    execution_result_repository_mock = mock.Mock(IExecutionResultRepository)
    execution_result_repository_mock.get_results_by_workflow_run_id.return_value = [
        WorkflowResultView(key="key", value="value", type=ActionIOType.STRING)
    ]
    container.execution_result_repository.override(execution_result_repository_mock)

    workflow_run_repository_mock = mock.Mock(DatabaseWorkflowRunRepository)
    wf_run_mock = mock.Mock(WorkflowRun)
    wf_run_mock.project_id = "project_id"
    wf_run_mock.runs = []
    wf_run_mock.state = WorkflowRunState.COMPLETED
    workflow_run_repository_mock.get.return_value = wf_run_mock
    container.workflow_run_repository.override(workflow_run_repository_mock)

    yield container
    container.unwire()


@pytest.fixture()
def database_engine(app_container: ApplicationContainer) -> SqlalchemyDatabaseEngine:
    with tempfile.TemporaryDirectory() as tmp_dir:
        database_url = f"sqlite:///{tmp_dir}/ryax.db"
        engine = SqlalchemyDatabaseEngine(database_url, map_orm)
        engine.connect()
        app_container.database_engine.override(engine)
        yield engine
        clear_mappers()
        metadata.drop_all(engine._engine)
        engine.disconnect()


@pytest.fixture()
def encryption_service_mock(app_container: ApplicationContainer):
    encryption_service_mock = mock.MagicMock(EncryptionService)
    app_container.encryption_service.override(encryption_service_mock)
    return encryption_service_mock


@pytest.fixture()
def user_auth_service_mock(app_container: ApplicationContainer):
    user_auth_service_mock = mock.MagicMock(UserAuthService)
    user_auth_service_mock.authenticate = mock.MagicMock()
    app_container.user_auth_service.override(user_auth_service_mock)
    return user_auth_service_mock


async def test_api_gateway_route(
    app_container, database_engine, user_auth_service_mock
):
    async def workflow_run_completed(_: BaseEvent):
        await app_container.message_bus().handle(
            [WorkflowRunIsCompletedEvent("wfr_id")]
        )

    app_container.message_bus().register_command_handler(
        InitializeExecutionResultCommand,
        mock.AsyncMock(return_value=("exec_id", "wfr_id")),
    )
    app_container.message_bus().register_event_handler(
        ExecutionResultFinishedEvent, workflow_run_completed
    )
    api_gateway = FastAPIApiGateway(
        base_url_prefix="",
        result_url_prefix="",
        server_port=8080,
        message_bus=app_container.message_bus(),
        workflow_result_service=app_container.workflow_result_service(),
        user_auth_service=user_auth_service_mock,
        stored_files=app_container.stored_file_view,
    )

    endpoint = HTTPEndpoint(
        method=HTTPMethod.GET,
        code=HTTPSuccessCode.CREATED_201,
        path=HTTPPath("/path/{hello}/toto/{foo}"),
        target_workflow_definition_id="wf_id_123",
        prefix_path="/123",
        async_reply_timeout=10,
        project_id="project_id",
    )
    trigger = WorkflowAction(
        id="1",
        workflow_deployment_id="",
        workflow_definition_id="",
        definition=None,
        next_actions=[],
        id_in_studio="",
        reference_inputs=[],
        static_inputs=[],
        dynamic_outputs=[
            ActionDynamicOutput(
                id="myid",
                name="myoutputname",
                type=ActionIOType.INTEGER,
                display_name="hello",
                help="help",
                origin=DynamicOutputOrigin.PATH,
            ),
            ActionDynamicOutput(
                id="myid1",
                name="myoutputname",
                type=ActionIOType.STRING,
                display_name="foo",
                help="help",
                origin=DynamicOutputOrigin.PATH,
            ),
            ActionDynamicOutput(
                id="test",
                name="test",
                type=ActionIOType.STRING,
                display_name="test",
                help="help",
                origin=DynamicOutputOrigin.BODY,
            ),
            ActionDynamicOutput(
                id="tata",
                name="tata",
                type=ActionIOType.STRING,
                display_name="tata",
                help="help",
                origin=DynamicOutputOrigin.BODY,
            ),
            ActionDynamicOutput(
                id="myid1",
                name="myname1",
                type=ActionIOType.ENUM,
                display_name="baz",
                help="help",
                origin=DynamicOutputOrigin.QUERY,
                optional=True,
                enum_values=["A", "B"],
            ),
            ActionDynamicOutput(
                id="myid1",
                name="myname1",
                type=ActionIOType.BOOLEAN,
                display_name="bool",
                help="help",
                origin=DynamicOutputOrigin.QUERY,
                optional=True,
            ),
        ],
        connection_execution_id="123",
        constraints=None,
        objectives=None,
    )
    try:
        await api_gateway.start()
        await api_gateway.add_endpoint(trigger, endpoint)

        api_client = TestClient(api_gateway.root_app)
        await asyncio.sleep(1)
        response = api_client.get("/123/docs")
        assert response.status_code == 200
        response = api_client.get("/123/openapi.json")
        openapi_spec = response.json()
        parameters = openapi_spec["paths"]["/path/{hello}/toto/{foo}"]["get"][
            "parameters"
        ]
        body_parameters = openapi_spec["paths"]["/path/{hello}/toto/{foo}"]["get"][
            "requestBody"
        ]["content"]["application/json"]["schema"]["properties"]
        assert len(parameters) + len(body_parameters) == len(trigger.dynamic_outputs)
        # Working request
        response = api_client.request(
            url="/123/path/123/toto/bar",
            method="GET",
            headers={"Content-Type": "application/json"},
            json={"test": "foo", "tata": "bar"},
        )
        assert response.status_code == 201
        assert response.json() == {"key": "value"}

        # Wrong body parameter missing
        response = api_client.request(
            url="/123/path/123/toto/bar",
            method="GET",
            headers={"Content-Type": "application/json"},
            json={"test": "tata is missing here"},
        )
        assert response.json() == {
            "errors": [
                "Invalid body. Parameters tata not available on the body {'test': 'tata is missing here'}. Error details: 'tata'"
            ]
        }
        assert response.status_code == 400

        # Wrong type request
        response = api_client.request(
            url="/123/path/abc/toto/bar",
            method="GET",
            headers={"Content-Type": "application/json"},
            json={"test": "foo", "tata": "bar"},
        )
        assert response.status_code == 422
        assert response.json() == {
            "detail": [
                {
                    "input": "abc",
                    "loc": ["path", "hello"],
                    "msg": "Input should be a valid integer, unable to parse string "
                    "as an integer",
                    "type": "int_parsing",
                    "url": "https://errors.pydantic.dev/2.10/v/int_parsing",
                }
            ]
        }

        # Unknown path request
        response = api_client.request(
            url="/123/notexists",
            method="GET",
            headers={"Content-Type": "application/json"},
            json={"test": "foo", "tata": "bar"},
        )
        assert response.status_code == 404

        # With optional enum query
        response = api_client.request(
            url="/123/path/123/toto/bar?baz=A",
            method="GET",
            headers={"Content-Type": "application/json"},
            json={"test": "foo", "tata": "bar"},
        )
        assert response.status_code == 201
        assert response.json() == {"key": "value"}

        # With optional enum query wrong value
        response = api_client.request(
            url="/123/path/123/toto/bar?baz=C",
            method="GET",
            headers={"Content-Type": "application/json"},
            json={"test": "foo", "tata": "bar"},
        )
        assert response.status_code == 422
        assert response.json() == {
            "detail": [
                {
                    "ctx": {"expected": "'A' or 'B'"},
                    "input": "C",
                    "loc": ["query", "baz"],
                    "msg": "Input should be 'A' or 'B'",
                    "type": "enum",
                    "url": "https://errors.pydantic.dev/2.10/v/enum",
                }
            ]
        }

        # With boolean value
        response = api_client.request(
            url="/123/path/123/toto/bar?bool=true",
            method="GET",
            headers={"Content-Type": "application/json"},
            json={"test": "foo", "tata": "bar"},
        )
        assert response.status_code == 201
        assert response.json() == {"key": "value"}

        # With boolean wrong value
        response = api_client.request(
            url="/123/path/123/toto/bar?bool=notabool",
            method="GET",
            headers={"Content-Type": "application/json"},
            json={"test": "foo", "tata": "bar"},
        )
        assert response.status_code == 422
        assert response.json() == {
            "detail": [
                {
                    "input": "notabool",
                    "loc": ["query", "bool"],
                    "msg": "Input should be a valid boolean, unable to interpret "
                    "input",
                    "type": "bool_parsing",
                    "url": "https://errors.pydantic.dev/2.10/v/bool_parsing",
                }
            ]
        }

        await api_gateway.remove_endpoint(endpoint.target_workflow_definition_id)
    finally:
        await api_gateway.stop()


async def test_default_success_status_code(
    app_container, database_engine, user_auth_service_mock
):
    async def workflow_run_completed(_: BaseEvent):
        # Wait more than the timeout
        await asyncio.sleep(0.2)
        await app_container.message_bus().handle(
            [WorkflowRunIsCompletedEvent("wfr_id")]
        )

    app_container.message_bus().register_command_handler(
        InitializeExecutionResultCommand,
        mock.AsyncMock(return_value=("exec_id", "wfr_id")),
    )
    app_container.message_bus().register_event_handler(
        ExecutionResultFinishedEvent, workflow_run_completed
    )
    api_gateway = FastAPIApiGateway(
        base_url_prefix="",
        message_bus=app_container.message_bus(),
        result_url_prefix="",
        server_port=8080,
        user_auth_service=user_auth_service_mock,
        workflow_result_service=app_container.workflow_result_service(),
        stored_files=app_container.stored_file_view,
    )

    endpoint = HTTPEndpoint(
        method=HTTPMethod.GET,
        code=HTTPSuccessCode.SUCCESS_200,
        path=HTTPPath("/test200"),
        target_workflow_definition_id="wf_id_123",
        prefix_path="",
        async_reply_timeout=10,
        project_id="project_id",
    )
    trigger = WorkflowAction(
        id="1",
        workflow_deployment_id="",
        workflow_definition_id="",
        definition=None,
        next_actions=[],
        id_in_studio="",
        reference_inputs=[],
        static_inputs=[],
        dynamic_outputs=[],
        connection_execution_id="123",
        constraints=None,
        objectives=None,
    )
    try:
        await api_gateway.start()
        await api_gateway.add_endpoint(trigger, endpoint)

        api_client = TestClient(api_gateway.root_app)
        await asyncio.sleep(1)
        response = api_client.get("/test200")
        assert response.status_code == 200
        await api_gateway.remove_endpoint(endpoint.target_workflow_definition_id)
    finally:
        await api_gateway.stop()


async def test_async_response_api_gateway(
    app_container, database_engine, user_auth_service_mock
):
    async def workflow_run_completed(_: BaseEvent):
        # Wait more than the timeout
        await asyncio.sleep(0.2)
        await app_container.message_bus().handle(
            [WorkflowRunIsCompletedEvent("wfr_id")]
        )

    app_container.message_bus().register_command_handler(
        InitializeExecutionResultCommand,
        mock.AsyncMock(return_value=("exec_id", "wfr_id")),
    )
    app_container.message_bus().register_event_handler(
        ExecutionResultFinishedEvent, workflow_run_completed
    )
    api_gateway = FastAPIApiGateway(
        base_url_prefix="",
        message_bus=app_container.message_bus(),
        result_url_prefix="",
        server_port=8080,
        user_auth_service=user_auth_service_mock,
        workflow_result_service=app_container.workflow_result_service(),
        stored_files=app_container.stored_file_view,
    )

    endpoint = HTTPEndpoint(
        method=HTTPMethod.GET,
        code=HTTPSuccessCode.SUCCESS_200,
        path=HTTPPath("/async"),
        target_workflow_definition_id="wf_id_123",
        prefix_path="",
        async_reply_timeout=0.1,
        project_id="project_id",
    )
    trigger = WorkflowAction(
        id="1",
        workflow_deployment_id="",
        workflow_definition_id="",
        definition=None,
        next_actions=[],
        id_in_studio="",
        reference_inputs=[],
        static_inputs=[],
        dynamic_outputs=[],
        connection_execution_id="123",
        constraints=None,
        objectives=None,
    )
    try:
        await api_gateway.start()
        await api_gateway.add_endpoint(trigger, endpoint)

        api_client = TestClient(api_gateway.root_app)
        await asyncio.sleep(1)
        response = api_client.get("/async")
        assert response.status_code == 202
        assert response.json() == {
            "result_link": "http://testserver/results/wfr_id",
            "workflow_run_id": "wfr_id",
        }

        await api_gateway.remove_endpoint(endpoint.target_workflow_definition_id)
    finally:
        await api_gateway.stop()
