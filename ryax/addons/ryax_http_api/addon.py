import logging
from typing import Optional

from ryax.addons.ryax_http_api.api_gateway import (
    FastAPIApiGateway,
    HTTPEndpoint,
    HTTPMethod,
    HTTPSuccessCode,
)
from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.application.workflow_result_service import WorkflowResultService
from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.addons.addon_entities import (
    AddOnParameter,
    AddOnParameters,
    RyaxAddOn,
    RyaxAddOns,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.stored_file.stored_file_service import IStoredFileView
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)

logger = logging.getLogger(__name__)


class HttpAPIAddOn(RyaxAddOn):
    name = "HttpAPI"
    technical_name = "http_api"
    version = "0.1"
    parameters = AddOnParameters(
        AddOnParameter(
            id="path",
            name="path",
            type=ActionIOType.STRING,
            display_name="Templated HTTP Endpoint Path",
            help="Use curly brackets to add parameters in your path. Example: /users/{user_id}/assets/{assets_id}",
            optional=False,
        ),
        AddOnParameter(
            id="http_method",
            name="http_method",
            type=ActionIOType.ENUM,
            display_name="HTTP method",
            help="HTTP method for your API endpoint.",
            enum_values=list(HTTPMethod),
            optional=False,
        ),
        AddOnParameter(
            id="http_success_status_code",
            name="http_success_status_code",
            type=ActionIOType.ENUM,
            display_name="HTTP success status code",
            help="A custom success status code 200 or 201, 202 is reserved by ryax in case of timeout. If timeout 202 is returned a link for pooling the result is given.",
            enum_values=list(HTTPSuccessCode),
            value=HTTPSuccessCode.SUCCESS_200,
            optional=False,
        ),
        AddOnParameter(
            id="async_reply_timeout",
            name="async_reply_timeout",
            type=ActionIOType.FLOAT,
            display_name="Asynchronous Reply Timeout",
            help="A timeout in seconds after which the reply will be asynchronous: respond a 201 with a link to the future result. If negative, wait forever",
            value=20,
            optional=False,
        ),
    )

    def __init__(self, addons: Optional[RyaxAddOns] = None):
        super(HttpAPIAddOn, self).__init__(addons)
        self.api_gateway: Optional[FastAPIApiGateway] = None

    async def hook_on_startup(
        self,
        configuration: dict,
        message_bus: IMessageBus,
        workflow_result_service: WorkflowResultService,
        user_auth_service: UserAuthService,
        stored_files_service: IStoredFileView,
    ) -> None:
        self.api_gateway = FastAPIApiGateway(
            base_url_prefix=configuration["user_api"]["prefix"],
            result_url_prefix=configuration["api"]["base_url"],
            message_bus=message_bus,
            workflow_result_service=workflow_result_service,
            user_auth_service=user_auth_service,
            stored_files=stored_files_service,
        )
        await self.api_gateway.start()
        logger.info("API Gateway is now running!")

    async def hook_workflow_deployment_is_started(
        self,
        workflow: WorkflowDeployment,
    ) -> None:
        trigger = workflow.get_root_action()
        params = self.get_parameters_values(trigger)
        if not params:
            return
        endpoint = HTTPEndpoint(
            path=params["path"].strip(),
            method=params["http_method"],
            code=params["http_success_status_code"],
            target_workflow_definition_id=workflow.workflow_definition_id,
            prefix_path="/" + workflow.project_id,
            async_reply_timeout=float(params["async_reply_timeout"]),
            project_id=workflow.project_id,
        )
        assert self.api_gateway is not None
        await self.api_gateway.add_endpoint(trigger, endpoint)

    async def hook_workflow_deployment_is_stopped(
        self,
        workflow: WorkflowDeployment,
    ) -> None:
        trigger = workflow.get_root_action()
        params = self.get_parameters_values(trigger)
        if not params:
            return
        assert self.api_gateway is not None
        await self.api_gateway.remove_endpoint(workflow.workflow_definition_id)

    async def hook_on_cleanup(self) -> None:
        if self.api_gateway:
            await self.api_gateway.stop()
        logger.info("API Gateway is now stopped!")
