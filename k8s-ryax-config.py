#!/usr/bin/env python3

import subprocess
import json
import base64
import os

# Directory to store secrets
SECRETS_DIR = "secrets"

# Ensure the directory exists
os.makedirs(SECRETS_DIR, exist_ok=True)

# Kubernetes secret configurations
secrets_to_extract = [
    {
        "name": "ryax-registry-creds-secret",
        "namespace": "ryaxns-execs",
        "file": "registry-execs.json",
    },
    {
        "name": "ryax-registry-creds-secret",
        "namespace": "ryaxns",
        "file": "registry.json",
    },
    {"name": "ryax-broker-secret", "namespace": "ryaxns", "file": "broker.json"},
    {"name": "ryax-minio-secret", "namespace": "ryaxns", "file": "filestore.json"},
]


# Function to run kubectl commands to extract secrets in JSON format
def extract_secrets():
    print("Extracting secrets using kubectl (JSON output)...")
    for secret in secrets_to_extract:
        file_path = os.path.join(SECRETS_DIR, secret["file"])
        try:
            with open(file_path, "w") as f:
                subprocess.run(
                    [
                        "kubectl",
                        "get",
                        "secret",
                        "-n",
                        secret["namespace"],
                        secret["name"],
                        "-o",
                        "json",
                    ],
                    stdout=f,
                )
            print(f"Extracted {secret['name']} to {file_path}")
        except subprocess.CalledProcessError as e:
            print(f"Failed to extract secret {secret['name']}: {e}")


# Function to decode, modify, and re-encode secret data in JSON format
def process_secret(file_path):
    print(f"Processing {file_path}...")

    # Read the JSON file
    with open(file_path, "r") as f:
        secret_json = json.load(f)

    # Remove unnecessary metadata fields
    secret_json["metadata"].pop("resourceVersion", None)
    secret_json["metadata"].pop("uid", None)
    secret_json["metadata"].pop("creationTimestamp", None)
    secret_json["metadata"].pop("labels", None)
    secret_json["metadata"].pop("annotations", None)

    # Process the data section: decode Base64, modify, and re-encode
    if "data" in secret_json:
        for key, encoded_value in secret_json["data"].items():
            decoded_data = base64.b64decode(encoded_value).decode("utf-8")

            # Modify service names: append '-ext' to 'ryax-broker' and 'minio'
            modified_data = decoded_data.replace(
                "ryax-broker", "ryax-broker-ext"
            ).replace("minio", "minio-ext")

            # Re-encode modified content back to Base64
            new_encoded_value = base64.b64encode(modified_data.encode("utf-8")).decode(
                "utf-8"
            )
            secret_json["data"][key] = new_encoded_value

    # Write the modified secret back to the JSON file
    with open(file_path, "w") as f:
        json.dump(secret_json, f, indent=2)

    print(f"Finished processing {file_path}.")


# Function to process all extracted secrets
def process_all_secrets():
    for secret in secrets_to_extract:
        process_secret(os.path.join(SECRETS_DIR, secret["file"]))


# Main function to orchestrate the secret extraction and processing
if __name__ == "__main__":
    # Step 1: Extract secrets using kubectl in JSON format
    extract_secrets()

    # Step 2: Process the secrets to modify ryax-broker and minio service names
    process_all_secrets()

    print("All secrets processed successfully.")
