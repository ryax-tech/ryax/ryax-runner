{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    flakeUtils.follows = "nix2container/flake-utils";
  };


  outputs = { self, nixpkgs, nix2container, flakeUtils }:

    # Change values here to support more arch
    flakeUtils.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ]
      (system:
        let
          pkgs = import nixpkgs { inherit system; };
          nix2containerPkgs = nix2container.packages.${system};
          python = pkgs.python311;
          lib = import ./nix/lib.nix { inherit pkgs python; };
        in
        {
          devShell = (pkgs.buildFHSUserEnv {
            name = "dev-shell";
            targetPkgs = pkgs: [ python pkgs.poetry pkgs.stdenv.cc.cc ];
          }).env;
          packages = {
            test = lib.test;
            lint = lib.lint;
            runner = let
              # Put the name of your service here
              myTool = "ryax-runner";

              buildDir = "/tmp/ryax/${myTool}";
              appDir = builtins.path { path = ./.; name = myTool; };
            in {
                install = lib.install appDir buildDir;
                image = pkgs.callPackage ./nix/runner_container.nix {
                  inherit myTool appDir python;
                  depsDir = (/. + buildDir);
                  nix2container = nix2containerPkgs.nix2container;
                };
            };
            worker = let
              # Put the name of your service here
              myTool = "ryax-worker";

              buildDir = "/tmp/ryax/${myTool}";
              appDir = builtins.path { path = ./.; name = myTool; };
            in {
                install = lib.install appDir buildDir;
                image = pkgs.callPackage ./nix/worker_container.nix {
                  inherit myTool appDir python;
                  depsDir = (/. + buildDir);
                  nix2container = nix2containerPkgs.nix2container;
                };
            };
          };
          # Enable autoformat
          formatter = pkgs.nixpkgs-fmt;
        }
      );
}
