# ExecutionConnectionStateMachine ryax.runner.domain.execution_connection.execution_connection_entities

## State Machine Graph

```mermaid
graph TD
NONE["NONE"]
PENDING["PENDING"]
ALLOCATED["ALLOCATED"]
STARTED["STARTED"]
RUNNING["RUNNING"]
STOPPING["STOPPING"]
COMPLETED["COMPLETED"]
CANCELED["CANCELED"]
ERROR["ERROR"]
NONE --" create()"--> PENDING
PENDING --" on_error()"--> ERROR
PENDING --" allocate()"--> ALLOCATED
PENDING --" stop()"--> STOPPING
PENDING --" stop()"--> CANCELED
PENDING --" on_ended()"--> CANCELED
PENDING --" on_ended()"--> COMPLETED
ALLOCATED --" on_error()"--> ERROR
ALLOCATED --" start()"--> STARTED
ALLOCATED --" stop()"--> STOPPING
ALLOCATED --" stop()"--> CANCELED
ALLOCATED --" on_ended()"--> CANCELED
ALLOCATED --" on_ended()"--> COMPLETED
STARTED --" on_error()"--> ERROR
STARTED --" initialize()"--> RUNNING
STARTED --" stop()"--> STOPPING
STARTED --" stop()"--> CANCELED
STARTED --" on_ended()"--> CANCELED
STARTED --" on_ended()"--> COMPLETED
RUNNING --" on_error()"--> ERROR
RUNNING --" initialize()"--> RUNNING
RUNNING --" stop()"--> STOPPING
RUNNING --" stop()"--> CANCELED
RUNNING --" on_ended()"--> CANCELED
RUNNING --" on_ended()"--> COMPLETED
ERROR --" on_error()"--> ERROR
ERROR --" on_ended()"--> COMPLETED
ERROR --" on_ended()"--> CANCELED
STOPPING --" on_error()"--> ERROR
STOPPING --" on_ended()"--> COMPLETED
STOPPING --" on_ended()"--> CANCELED

```

## State Machine Transition Table


| ↓ States / Messages → | CreateExecutionConnectionCommand | ExecutionConnectionErroredEvent | AllocateExecutionConnectionCommand | StopExecutionConnectionCommand | ExecutionConnectionEndedEvent | StartExecutionConnectionCommand | InitializeExecutionResultCommand |
| :-------------------: | :------------------------------: | :-----------------------------: | :--------------------------------: | :----------------------------: | :---------------------------: | :-----------------------------: | :------------------------------: 
|                ⃝ -->  |                          PENDING |                               ⛔ |                                  ⛔ |                              ⛔ |                             ⛔ |                               ⛔ |                                ⛔  |
|               PENDING |                                ⛔ |                           ERROR |                          ALLOCATED |                       CANCELED |                     COMPLETED |                               ⛔ |                                ⛔  |
|             ALLOCATED |                                ⛔ |                           ERROR |                                  ⛔ |                       CANCELED |                     COMPLETED |                         STARTED |                                ⛔  |
|               STARTED |                                ⛔ |                           ERROR |                                  ⛔ |                       CANCELED |                     COMPLETED |                               ⛔ |                          RUNNING  |
|               RUNNING |                                ⛔ |                           ERROR |                                  ⛔ |                       CANCELED |                     COMPLETED |                               ⛔ |                          RUNNING  |
|              STOPPING |                                ⛔ |                           ERROR |                                  ⛔ |                              ⛔ |                      CANCELED |                               ⛔ |                                ⛔  |
|             COMPLETED |                                ⛔ |                               ⛔ |                                  ⛔ |                              ⛔ |                             ⛔ |                               ⛔ |                                ⛔  |
|              CANCELED |                                ⛔ |                               ⛔ |                                  ⛔ |                              ⛔ |                             ⛔ |                               ⛔ |                                ⛔  |
|                 ERROR |                                ⛔ |                           ERROR |                                  ⛔ |                              ⛔ |                      CANCELED |                               ⛔ |                                ⛔  |

            

# ActionDeploymentStateMachine ryax.runner.domain.action_deployment.action_deployment_entities

## State Machine Graph

```mermaid
graph TD
NONE["NONE"]
CREATED["CREATED"]
DEPLOYING["DEPLOYING"]
READY["READY"]
RUNNING["RUNNING"]
UNDEPLOYING["UNDEPLOYING"]
UNDEPLOYED["UNDEPLOYED"]
ERROR["ERROR"]
NO_RESOURCES_AVAILABLE["NO_RESOURCES_AVAILABLE"]
CREATED --" allocate()"--> CREATED
CREATED --" allocate()"--> DEPLOYING
CREATED --" apply()"--> DEPLOYING
CREATED --" on_startup()"--> DEPLOYING
CREATED --" allocate()"--> READY
CREATED --" on_startup()"--> READY
CREATED --" allocate()"--> RUNNING
CREATED --" on_startup()"--> RUNNING
CREATED --" allocate()"--> NO_RESOURCES_AVAILABLE
CREATED --" on_startup()"--> NO_RESOURCES_AVAILABLE
CREATED --" on_startup()"--> ERROR
CREATED --" on_startup()"--> UNDEPLOYED
CREATED --" delete()"--> UNDEPLOYING
DEPLOYING --" allocate()"--> CREATED
DEPLOYING --" allocate()"--> DEPLOYING
DEPLOYING --" do_apply()"--> DEPLOYING
DEPLOYING --" on_startup()"--> DEPLOYING
DEPLOYING --" update()"--> DEPLOYING
DEPLOYING --" allocate()"--> READY
DEPLOYING --" on_startup()"--> READY
DEPLOYING --" on_is_ready()"--> READY
DEPLOYING --" update()"--> READY
DEPLOYING --" allocate()"--> RUNNING
DEPLOYING --" on_startup()"--> RUNNING
DEPLOYING --" on_is_ready()"--> RUNNING
DEPLOYING --" update()"--> RUNNING
DEPLOYING --" allocate()"--> NO_RESOURCES_AVAILABLE
DEPLOYING --" on_startup()"--> NO_RESOURCES_AVAILABLE
DEPLOYING --" update()"--> NO_RESOURCES_AVAILABLE
DEPLOYING --" do_apply()"--> ERROR
DEPLOYING --" on_startup()"--> ERROR
DEPLOYING --" on_error()"--> ERROR
DEPLOYING --" on_startup()"--> UNDEPLOYED
DEPLOYING --" update()"--> UNDEPLOYED
DEPLOYING --" delete()"--> UNDEPLOYING
READY --" allocate()"--> CREATED
READY --" allocate()"--> DEPLOYING
READY --" on_startup()"--> DEPLOYING
READY --" update()"--> DEPLOYING
READY --" allocate()"--> READY
READY --" on_startup()"--> READY
READY --" on_is_ready()"--> READY
READY --" update()"--> READY
READY --" allocate()"--> RUNNING
READY --" on_startup()"--> RUNNING
READY --" on_is_ready()"--> RUNNING
READY --" set_running()"--> RUNNING
READY --" update()"--> RUNNING
READY --" allocate()"--> NO_RESOURCES_AVAILABLE
READY --" on_startup()"--> NO_RESOURCES_AVAILABLE
READY --" update()"--> NO_RESOURCES_AVAILABLE
READY --" start()"--> RUNNING
READY --" on_startup()"--> ERROR
READY --" on_error()"--> ERROR
READY --" on_startup()"--> UNDEPLOYED
READY --" update()"--> UNDEPLOYED
READY --" delete()"--> UNDEPLOYING
RUNNING --" allocate()"--> CREATED
RUNNING --" allocate()"--> DEPLOYING
RUNNING --" on_startup()"--> DEPLOYING
RUNNING --" allocate()"--> READY
RUNNING --" on_startup()"--> READY
RUNNING --" on_is_ready()"--> READY
RUNNING --" allocate()"--> RUNNING
RUNNING --" on_startup()"--> RUNNING
RUNNING --" on_is_ready()"--> RUNNING
RUNNING --" set_running()"--> RUNNING
RUNNING --" allocate()"--> NO_RESOURCES_AVAILABLE
RUNNING --" on_startup()"--> NO_RESOURCES_AVAILABLE
RUNNING --" on_startup()"--> ERROR
RUNNING --" on_startup()"--> UNDEPLOYED
RUNNING --" delete()"--> UNDEPLOYING
NO_RESOURCES_AVAILABLE --" allocate()"--> CREATED
NO_RESOURCES_AVAILABLE --" allocate()"--> DEPLOYING
NO_RESOURCES_AVAILABLE --" on_startup()"--> DEPLOYING
NO_RESOURCES_AVAILABLE --" update()"--> DEPLOYING
NO_RESOURCES_AVAILABLE --" allocate()"--> READY
NO_RESOURCES_AVAILABLE --" on_startup()"--> READY
NO_RESOURCES_AVAILABLE --" on_is_ready()"--> READY
NO_RESOURCES_AVAILABLE --" update()"--> READY
NO_RESOURCES_AVAILABLE --" allocate()"--> RUNNING
NO_RESOURCES_AVAILABLE --" on_startup()"--> RUNNING
NO_RESOURCES_AVAILABLE --" on_is_ready()"--> RUNNING
NO_RESOURCES_AVAILABLE --" update()"--> RUNNING
NO_RESOURCES_AVAILABLE --" allocate()"--> NO_RESOURCES_AVAILABLE
NO_RESOURCES_AVAILABLE --" on_startup()"--> NO_RESOURCES_AVAILABLE
NO_RESOURCES_AVAILABLE --" update()"--> NO_RESOURCES_AVAILABLE
NO_RESOURCES_AVAILABLE --" on_startup()"--> ERROR
NO_RESOURCES_AVAILABLE --" on_error()"--> ERROR
NO_RESOURCES_AVAILABLE --" on_startup()"--> UNDEPLOYED
NO_RESOURCES_AVAILABLE --" update()"--> UNDEPLOYED
NO_RESOURCES_AVAILABLE --" delete()"--> UNDEPLOYING
NONE --" create()"--> CREATED
UNDEPLOYED --" on_is_ready()"--> READY
UNDEPLOYED --" on_is_ready()"--> RUNNING
ERROR --" on_is_ready()"--> READY
ERROR --" on_is_ready()"--> RUNNING
ERROR --" delete()"--> UNDEPLOYING
UNDEPLOYING --" on_undeploying()"--> UNDEPLOYING
UNDEPLOYING --" on_undeploying()"--> ERROR
UNDEPLOYING --" is_stopped()"--> UNDEPLOYED

```

## State Machine Transition Table


| ↓ States / Messages → | AllocateExecutionConnectionCommand | ActionDeploymentCreatedEvent | ApplicationStartupEvent | UndeployActionDeploymentCommand | ActionDeploymentIsDeployingEvent | UpdateActionDeploymentCommand | ActionDeploymentIsReadyEvent | ErrorActionDeploymentCommand | SetRunningActionDeploymentCommand | StartExecutionConnectionCommand | CreateActionDeploymentCommand | ActionDeploymentUndeployingEvent | ActionDeploymentUndeployedEvent |
| :-------------------: | :--------------------------------: | :--------------------------: | :---------------------: | :-----------------------------: | :------------------------------: | :---------------------------: | :--------------------------: | :--------------------------: | :-------------------------------: | :-----------------------------: | :---------------------------: | :------------------------------: | :-----------------------------: 
|                ⃝ -->  |                                  ⛔ |                            ⛔ |                       ⛔ |                               ⛔ |                                ⛔ |                             ⛔ |                            ⛔ |                            ⛔ |                                 ⛔ |                               ⛔ |                       CREATED |                                ⛔ |                               ⛔  |
|               CREATED |             NO_RESOURCES_AVAILABLE |                    DEPLOYING |              UNDEPLOYED |                     UNDEPLOYING |                                ⛔ |                             ⛔ |                            ⛔ |                            ⛔ |                                 ⛔ |                               ⛔ |                             ⛔ |                                ⛔ |                               ⛔  |
|             DEPLOYING |             NO_RESOURCES_AVAILABLE |                            ⛔ |              UNDEPLOYED |                     UNDEPLOYING |                            ERROR |                    UNDEPLOYED |                      RUNNING |                        ERROR |                                 ⛔ |                               ⛔ |                             ⛔ |                                ⛔ |                               ⛔  |
|                 READY |             NO_RESOURCES_AVAILABLE |                            ⛔ |              UNDEPLOYED |                     UNDEPLOYING |                                ⛔ |                    UNDEPLOYED |                      RUNNING |                        ERROR |                           RUNNING |                         RUNNING |                             ⛔ |                                ⛔ |                               ⛔  |
|               RUNNING |             NO_RESOURCES_AVAILABLE |                            ⛔ |              UNDEPLOYED |                     UNDEPLOYING |                                ⛔ |                             ⛔ |                      RUNNING |                            ⛔ |                           RUNNING |                               ⛔ |                             ⛔ |                                ⛔ |                               ⛔  |
|           UNDEPLOYING |                                  ⛔ |                            ⛔ |                       ⛔ |                               ⛔ |                                ⛔ |                             ⛔ |                            ⛔ |                            ⛔ |                                 ⛔ |                               ⛔ |                             ⛔ |                            ERROR |                      UNDEPLOYED  |
|            UNDEPLOYED |                                  ⛔ |                            ⛔ |                       ⛔ |                               ⛔ |                                ⛔ |                             ⛔ |                      RUNNING |                            ⛔ |                                 ⛔ |                               ⛔ |                             ⛔ |                                ⛔ |                               ⛔  |
|                 ERROR |                                  ⛔ |                            ⛔ |                       ⛔ |                     UNDEPLOYING |                                ⛔ |                             ⛔ |                      RUNNING |                            ⛔ |                                 ⛔ |                               ⛔ |                             ⛔ |                                ⛔ |                               ⛔  |
| NO_RESOURCES_AVAILABLE |             NO_RESOURCES_AVAILABLE |                            ⛔ |              UNDEPLOYED |                     UNDEPLOYING |                                ⛔ |                    UNDEPLOYED |                      RUNNING |                        ERROR |                                 ⛔ |                               ⛔ |                             ⛔ |                                ⛔ |                               ⛔  |

            

