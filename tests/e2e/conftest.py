import tempfile

import pytest
from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.orm import clear_mappers

from ryax.runner.container import ApplicationContainer
from ryax.common.infrastructure.database.engine import Session
from ryax.runner.infrastructure.database.mapper import start_mapping
from ryax.runner.infrastructure.database.metadata import metadata


@pytest.fixture()
def app_container(database_session: Session):
    container = ApplicationContainer()
    yield container
    container.unwire()


@pytest.fixture(scope="function")
def database_engine(app):
    with tempfile.TemporaryDirectory() as tmp_dir:
        database_url = f"sqlite:///{tmp_dir}/ryax.db"
        engine: Engine = create_engine(database_url)
        metadata.create_all(bind=engine)
        try:
            start_mapping()
            app.database_engine.override(engine)
            yield engine
        finally:
            clear_mappers()
            metadata.drop_all(engine)
            engine.dispose()
