import json
import os
import time
from multiprocessing import Process
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Tuple
from unittest import mock

import pytest
import requests
from aiohttp import web
from minio import Minio, S3Error
from sqlalchemy.orm import clear_mappers

from ryax.runner.app import configure, init, on_cleanup, on_startup
from ryax.runner.application.authentication_service import AuthenticationService
from ryax.runner.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.auth_token import AuthToken
from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.runner.infrastructure.database.mapper import map_orm
from ryax.runner.infrastructure.database.metadata import metadata


@pytest.fixture
def runner():
    db = SqlalchemyDatabaseEngine(
        connection_url=os.environ["RYAX_DATASTORE"], create_mapping=map_orm
    )
    db.connect()
    clear_mappers()
    metadata.drop_all(db._engine)
    db.disconnect()

    with TemporaryDirectory() as tmp_dir:

        def start():
            container = configure()
            container.configuration.deployment.logs.directory.override(tmp_dir)
            # {
            #    #"log_level": "DEBUG",
            #    #"database": {"url": os.environ["RYAX_DATASTORE"]},
            #    #"messaging": {"url": os.environ["RYAX_BROKER"]},
            #    "deployment": {
            #        "type": "k8s",
            #        "execution_log": {"directory": tmp_dir},
            #        "k8s": {"user_namespace": "test"},
            #    },
            #    #"stored_file": {
            #    #    "type": "posix"
            #    #}
            # }
            # Disable project id check and auth
            project_authorization_service_mock = mock.MagicMock(
                ProjectAuthorizationService
            )
            project_authorization_service_mock.get_current_project = mock.AsyncMock(
                return_value="project_id"
            )
            container.project_authorization_service.override(
                project_authorization_service_mock
            )
            authentication_service_mock = mock.MagicMock(AuthenticationService)
            authentication_service_mock.check_access = mock.MagicMock(
                return_value=AuthToken(user_id="user")
            )
            container.authentication_service.override(authentication_service_mock)

            app: web.Application = init(container)
            app.on_startup.append(on_startup)
            app.on_cleanup.append(on_cleanup)

            async def clean_the_database(app: web.Application) -> None:
                """Define hook when application stop"""
                container: ApplicationContainer = app["container"]
                clear_mappers()
                engine = container.database_engine()
                metadata.drop_all(engine._engine)
                engine.disconnect()

            app.on_cleanup.append(clean_the_database)
            web.run_app(app, shutdown_timeout=1)

        runner_app = Process(target=start)
        runner_app.start()
        url = "http://127.0.0.1:8080"
        counter = 0
        while counter < 20:
            try:
                requests.head(url)
                break
            except requests.exceptions.ConnectionError:
                print(".", end="")
                time.sleep(0.5)
                counter += 1

        yield url
        runner_app.terminate()
        counter = 0
        while runner_app.is_alive() and counter < 10:
            time.sleep(1)
            counter += 1
        runner_app.kill()


@pytest.fixture()
def minio_connection():
    minio = Minio(
        os.environ.get("RYAX_FILESTORE"),
        access_key=os.environ.get("RYAX_FILESTORE_ACCESS_KEY"),
        secret_key=os.environ.get("RYAX_FILESTORE_SECRET_KEY"),
        secure=False,
    )
    bucket = os.environ.get("RYAX_FILESTORE_BUCKET")
    try:
        objects_to_delete = minio.list_objects(bucket, recursive=True)
        for obj in objects_to_delete:
            minio.remove_object(bucket, obj.object_name)
        minio.remove_bucket(bucket)
    except S3Error:
        pass
    minio.make_bucket(bucket)
    yield minio, bucket


def test_get_documentation_from_API(runner):
    response = requests.get(runner + "/docs")
    assert response.status_code == 200


def test_health_check_from_API(runner):
    response = requests.get(runner + "/healthz")
    assert response.status_code == 200


@pytest.mark.skip(
    "Too long for the CI and only usable with telepresence launch with sudo rights"
)
def test_one_execution_trigger_from_API(runner, minio_connection: Tuple[Minio, str]):
    minio, bucket = minio_connection
    with open(Path(__file__).parent / "action-echo-example.json") as json_file:
        action1_definition = json.load(json_file)

    response = requests.post(runner + "/modules", json=action1_definition)
    assert response.status_code == 201

    with open(Path(__file__).parent / "execution-trigger-example-1.json") as json_file:
        execution_trigger = json.load(json_file)

    minio.fput_object(
        bucket,
        execution_trigger["inputs"]["file_1"],
        Path(__file__).parent / "execution-trigger-example-1.json",
    )

    response = requests.post(runner + "/execution_triggers", json=execution_trigger)
    assert response.status_code == 201
    trigger_id = response.json()["execution_trigger_id"]

    time.sleep(20)

    response = requests.get(runner + "/execution_triggers")
    assert response.status_code == 200
    trigger = response.json()[0]
    assert trigger["id"] == trigger_id
    assert trigger["state"] == "COMPLETED"


@pytest.mark.skip(
    "Too long for the CI and only usable with telepresence launch with sudo rights"
)
def test_workflow_deploy_from_API(runner, minio_connection: Tuple[Minio, str]):
    minio, bucket = minio_connection
    with open(Path(__file__).parent / "trigger-echo-example.json") as json_file:
        action1_definition = json.load(json_file)

    response = requests.post(runner + "/modules", json=action1_definition)
    assert response.status_code == 201

    with open(Path(__file__).parent / "action-echo-example.json") as json_file:
        action2_definition = json.load(json_file)
    response = requests.post(runner + "/modules", json=action2_definition)
    assert response.status_code == 201

    with open(Path(__file__).parent / "workflow_example.json") as json_file:
        workflow_definition = json.load(json_file)

    minio.fput_object(
        bucket,
        workflow_definition["modules"][0]["static_inputs"]["file_1"],
        Path(__file__).parent / "workflow_example.json",
    )

    response = requests.post(runner + "/workflows", json=workflow_definition)
    assert response.status_code == 201
    workflow = response.json()
    assert workflow["state"] == "STARTED"

    time.sleep(20)

    response = requests.get(
        runner + "/executions", params={"workflow_id": workflow["id"]}
    )
    assert response.status_code == 200
    for execution in response.json():
        if execution["module"]["kind"] != ActionKind.TRIGGER.value:
            assert execution["state"] == "Success"
