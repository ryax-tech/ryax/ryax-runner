from unittest.mock import AsyncMock, Mock

from ryax.common.domain.registration.registration_values import SiteType
from ryax.runner.container import ApplicationContainer as RunnerAppContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionInput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionExecutionType,
)
from ryax.runner.domain.site.site_entities import NodePool, Site
from ryax.worker.container import ApplicationContainer as WorkerAppContainer

from ryax.runner.infrastructure.messaging.setup import setup as runner_messaging_setup
from ryax.worker.domain.deployment.deployment_events import (
    CreateDeploymentCommand,
)
from ryax.worker.infrastructure.messaging.setup import setup as worker_messaging_setup


async def test_runner_worker_message():
    runner_app = RunnerAppContainer()
    runner_app.configuration.messaging.url.from_env("RYAX_BROKER", required=True)
    runner_app.configuration.actions.log_level.from_value("INFO")
    runner_messaging_setup(runner_app)

    site = Site(
        id=1,
        name="default",
        type=SiteType.KUBERNETES,
    )
    node_pool = NodePool(id="1", name="mynodepool", memory=123, cpu=2, site=site)
    site.node_pools = [node_pool]

    worker_app = WorkerAppContainer()
    uow_mock = AsyncMock()
    uow_mock.nodepool_repository = Mock()
    uow_mock.nodepool_repository.get_by_name = node_pool
    worker_app.unit_of_work.override(uow_mock)
    worker_app.configuration.messaging.url.from_env("RYAX_BROKER", required=True)
    worker_app.configuration.site.name.from_value(site.name)
    worker_messaging_setup(worker_app)

    await worker_app.messaging_engine().connect()
    await worker_app.messaging_consumer().start()
    mock_handler = AsyncMock()

    worker_app.message_bus().register_command_handler(
        CreateDeploymentCommand, mock_handler
    )

    runner_pub = runner_app.message_publisher()
    await runner_pub.engine.connect()
    k8s_deploy_service = runner_app.action_deployment_factory("k8s")
    action_deployment = ActionDeployment(
        id="new_action_id",
        project_id="test",
        action_definition=ActionDefinition(
            id="test",
            project_id="test",
            version="action_version",
            technical_name="action technical name",
            human_name="action human_name",
            description="descr",
            kind=ActionKind.PUBLISHER,
            container_image="test",
            inputs=[
                ActionInput(
                    id="input_id",
                    name="input_name",
                    type="input_type",
                    display_name="input_id",
                    help="help",
                    enum_values=[],
                )
            ],
            outputs=[
                ActionOutput(
                    id="output_id",
                    name="output_name",
                    type="output_type",
                    display_name="output_id",
                    help="help",
                    enum_values=[],
                )
            ],
        ),
        execution_type=ActionExecutionType.GRPC_V1,
        node_pool=node_pool,
    )
    await k8s_deploy_service.apply(action_deployment, {"extra_param": "test"})
    await runner_pub.engine.disconnect()
    await worker_app.messaging_engine().disconnect()
    mock_handler.assert_awaited_once()
