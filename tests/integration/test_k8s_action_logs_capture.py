import asyncio
import logging
import os
import uuid
from asyncio import CancelledError
from typing import Any, Callable, Type, cast

import kubernetes_asyncio as k8s
import pytest

from ryax.common.application.message_bus import Messages
from ryax.common.domain.internal_messaging.base_messages import BaseCommand, BaseEvent
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.deployment.deployment_events import DeploymentNewLogLinesEvent
from ryax.worker.domain.deployment.deployment_values import DeploymentState
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import K8sEngine

logger = logging.getLogger(__name__)


class MockMessageBus(IMessageBus):
    def __init__(self):
        self.saved_logs = ""

    def register_event_handler(
        self, event: Type[BaseEvent], event_handler: Callable
    ) -> None:
        raise NotImplementedError()

    def register_command_handler(
        self, command: Type[BaseCommand], command_handler: Callable
    ) -> None:
        pass

    async def handle_event(self, event: BaseEvent, **kwargs) -> None:
        pass

    async def handle_command(self, command: BaseCommand, blocking=True) -> Any:
        pass

    async def handle(self, messages: Messages) -> None:
        for message in messages:
            # messages are only of type ActionDeploymentNewLogLinesEvent in the tests bellow
            message = cast(DeploymentNewLogLinesEvent, message)
            self.saved_logs += message.logs

    async def wait_for_event(
        self, event_type: Type[BaseEvent], with_parameters: dict = None
    ) -> BaseEvent:
        pass

    async def stop(self) -> None:
        pass


@pytest.fixture()
async def k8s_engine(loop, app_container: ApplicationContainer) -> K8sEngine:
    namespace = f"test-{str(uuid.uuid4())[:6]}"
    logging.getLogger("kubernetes_asyncio").setLevel(logging.INFO)
    engine = K8sEngine(
        user_namespace=namespace,
        message_bus=MockMessageBus(),
        loki_url=os.environ.get("RYAX_LOKI_API_URL"),
    )
    await engine.init()
    await engine.create_user_namespace()
    yield engine
    try:
        async with k8s.client.ApiClient() as api:
            core_api = k8s.client.CoreV1Api(api)
            await core_api.delete_namespace(name=engine.user_namespace)
    except Exception:
        pass


async def _wait_deployment_state(
    k8s_engine: K8sEngine,
    deployment_id: str,
    state: DeploymentState,
    timeout: float,
) -> None:
    deployment_state = await k8s_engine.get_deployment_state(deployment_id)
    while deployment_state != state and timeout > 0:
        await asyncio.sleep(1)
        timeout -= 1
        deployment_state = await k8s_engine.get_deployment_state(deployment_id)
        logger.info("Deployment state: %s", deployment_state.value)
    if timeout <= 0:
        raise TimeoutError()


async def create_deployment_with_command(
    k8s_engine: K8sEngine,
    deployment_id: str,
    command: str,
    deployment_name: str,
    timeout: float,
):
    deployment = {
        "apiVersion": "apps/v1",
        "kind": "Deployment",
        "metadata": {
            "name": deployment_name,
            "labels": {"action_deployment_id": deployment_id},
        },
        "spec": {
            "selector": {"matchLabels": {"action_deployment_id": deployment_id}},
            "template": {
                "metadata": {
                    "labels": {
                        "app": "test",
                        "action_deployment_id": deployment_id,
                    },
                },
                "spec": {
                    "containers": [
                        {
                            "image": "busybox",
                            "name": deployment_name,
                            "command": ["/bin/sh"],
                            "args": ["-c", command],
                        }
                    ]
                },
            },
        },
    }

    await k8s_engine.apply_deployment_definition(deployment)
    if timeout > 0:
        await _wait_deployment_state(
            k8s_engine, deployment_id, DeploymentState.READY, timeout
        )


@pytest.mark.parametrize(
    ("command, expected_log"),
    [
        pytest.param('echo "toto"; sleep 100', "toto\n", id="single line"),
        pytest.param(
            'echo "toto\ntutu"; sleep 1; echo tata; sleep 100',
            "toto\ntutu\ntata\n",
            id="multi line",
        ),
    ],
)
async def test_action_log_running(
    k8s_engine: K8sEngine, command: str, expected_log: str
) -> None:
    deployment_id = "deploy_id"

    watch_logs_task = asyncio.create_task(k8s_engine.watch_logs(deployment_id))

    await create_deployment_with_command(
        k8s_engine, deployment_id, command, "test-deploy", 15
    )

    logs_timeout = 10.0
    step = 0.5
    while k8s_engine.message_bus.saved_logs != expected_log and logs_timeout > 0:
        await asyncio.sleep(step)
        logs_timeout -= step
    assert k8s_engine.message_bus.saved_logs == expected_log

    # Cleanup
    try:
        watch_logs_task.cancel()
        await asyncio.wait_for(watch_logs_task, timeout=1)
    except CancelledError:
        pass


async def test_action_log_not_running_anymore(
    k8s_engine: K8sEngine,
) -> None:
    command: str = 'echo "toto\ntutu\ntata"; sleep 100'
    expected_log: str = "toto\ntutu\ntata\n"
    deployment_id = "deploy-id"

    watch_logs_task = asyncio.create_task(k8s_engine.watch_logs(deployment_id))

    await create_deployment_with_command(
        k8s_engine, deployment_id, command, "test-deploy", 30
    )
    await k8s_engine.delete_deployment("test-deploy")
    await _wait_deployment_state(
        k8s_engine, deployment_id, DeploymentState.UNDEPLOYED, 5
    )

    step = 0.5
    logs_timeout = 10.0
    while k8s_engine.message_bus.saved_logs != expected_log and logs_timeout > 0:
        await asyncio.sleep(step)
        logs_timeout -= step
    assert k8s_engine.message_bus.saved_logs == expected_log

    # Cleanup
    try:
        watch_logs_task.cancel()
        await asyncio.wait_for(watch_logs_task, timeout=1)
    except CancelledError:
        pass


async def test_action_log_large_amount(k8s_engine: K8sEngine) -> None:
    seq = 1000
    command: str = f'for i in $(seq {seq}); do echo "$i,"; done; sleep 100'
    expected_log_list = []
    for i in range(seq):
        expected_log_list.append(f"{i+1},\n")
    expected_log = "".join(expected_log_list)
    deployment_id = str(uuid.uuid4())

    watch_logs_task = asyncio.create_task(k8s_engine.watch_logs(deployment_id))

    await create_deployment_with_command(
        k8s_engine, deployment_id, command, "test-deploy", 15
    )

    step = 0.5
    logs_timeout = 20.0
    sequence = []
    while len(sequence) < seq and logs_timeout > 0:
        await asyncio.sleep(step)
        logs_timeout -= step
        sequence = [
            int(elem) for elem in k8s_engine.message_bus.saved_logs.split(",")[:-1]
        ]
        assert sorted(sequence) == sequence
    assert k8s_engine.message_bus.saved_logs == expected_log

    # Cleanup
    try:
        watch_logs_task.cancel()
        await asyncio.wait_for(watch_logs_task, timeout=1)
    except CancelledError:
        pass
