import datetime

# import logging
from unittest import mock

from aiohttp.test_utils import TestClient
from sqlalchemy import select

from ryax.common.domain.internal_messaging.event_publisher import IPublisherService
from ryax.common.domain.registration.registration_values import (
    SiteType,
)
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionPendingEvent,
    ExecutionConnectionUpdatedEvent,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultInitializedEvent,
    ExecutionResultCompletedEvent,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.workflow_action.workflow_action import (
    ReferenceInput,
    StaticInput,
    WorkflowAction,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.domain.workflow_run.workflow_run_commands import (
    CreateWorkflowRunCommand,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import ActionRunState
from ryax.runner.domain.workflow_run.workflow_run_values import WorkflowRunState
from ryax.common.infrastructure.database.engine import Session, SqlalchemyDatabaseEngine
from tests.runner.domain.workflow_run import (
    generate_action_run,
    generate_workflow_run,
    generate_workflow_action,
)
from tests.helper import now


async def test_workflow_run_lifecycle(
    database_session: Session,
    project_authorization_service_mock: mock.MagicMock,
    authentication_service_mock: mock.MagicMock,
    api_client: TestClient,
    app_container: ApplicationContainer,
    message_publisher_mock: IPublisherService,
) -> None:
    """Create a workflow run and add some new results"""
    message_bus: IMessageBus = app_container.message_bus()
    workflow_action_1 = generate_workflow_action("action1")
    workflow_action_2 = generate_workflow_action("action2")
    workflow_deployment = WorkflowDeployment(
        id="workflow_deployment_id",
        name="A nice workflow_deployment_id1",
        project_id="project_id",
        creation_time=str(now),
        workflow_definition_id="workflow_definition_id1",
        actions=[workflow_action_1, workflow_action_2],
    )
    database_session.add(workflow_deployment)
    database_session.commit()

    # add site
    site = Site(id="1", name="testSite", type=SiteType.KUBERNETES)
    site.node_pools = [
        NodePool(id="1", name="noddPool1", site=site, cpu=1, memory=2000)
    ]
    database_session.add(site)

    await message_bus.handle_command(
        CreateWorkflowRunCommand(
            project_id=workflow_deployment.project_id,
            workflow_deployment_id=workflow_deployment.id,
        )
    )

    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    workflow_run_id = result[0]["id"]

    # Create a new workflow run
    exec_connection = ExecutionConnection(
        id="et1",
        project_id="project_id",
        workflow_action=None,
        workflow_run_id=workflow_run_id,
        submitted_at=datetime.datetime.fromtimestamp(1, datetime.timezone.utc),
        time_allotment=123,
    )
    database_session.add(exec_connection)
    exec_result = ExecutionResult(
        id="res1",
        workflow_run_id=workflow_run_id,
        project_id="project_id",
        submitted_at=now - datetime.timedelta(minutes=1),
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.CREATED,
        execution_connection_id="et1",
        workflow_action=workflow_action_1,
    )
    database_session.add(exec_result)
    database_session.commit()

    await message_bus.handle_event(
        ExecutionResultInitializedEvent(execution_result_id=exec_result.id)
    )
    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    assert result["state"] == WorkflowRunState.RUNNING.value
    assert len(result["runs"]) == 2
    assert result["runs"][0]["state"] == ActionRunState.RUNNING.value
    assert result["runs"][1]["state"] == ActionRunState.WAITING.value

    exec_result.state = ExecutionResultState.SUCCESS
    database_session.commit()
    await message_bus.handle_event(
        ExecutionResultCompletedEvent(execution_result_id=exec_result.id)
    )
    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    assert result["state"] == WorkflowRunState.RUNNING.value
    assert len(result["runs"]) == 2
    assert result["runs"][0]["state"] == ActionRunState.SUCCESS.value
    assert result["runs"][1]["state"] == ActionRunState.WAITING.value

    execution_connection = ExecutionConnection(
        id="conn1",
        workflow_run_id=workflow_run_id,
        project_id="project_id",
        submitted_at=now - datetime.timedelta(minutes=1),
        started_at=now,
        ended_at=now,
        state=ExecutionConnectionState.PENDING,
        workflow_action=workflow_action_2,
        time_allotment=12,
    )
    database_session.add(execution_connection)
    database_session.commit()
    # Workflow action next action is not set, so we trigger the next action manually
    await message_bus.handle_event(
        ExecutionConnectionPendingEvent(execution_connection_id=execution_connection.id)
    )

    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    assert result["state"] == WorkflowRunState.RUNNING.value
    assert len(result["runs"]) == 2
    assert result["runs"][0]["state"] == ActionRunState.SUCCESS.value
    assert result["runs"][1]["state"] == ActionRunState.PENDING.value

    execution_connection.state = ExecutionConnectionState.ERROR
    error_message = "ERRORORORORO"
    execution_connection.error_message = error_message
    database_session.commit()
    # Not really do the retry
    app_container.configuration.enable_action_retry.from_value(False)
    await message_bus.handle_event(
        ExecutionConnectionUpdatedEvent(execution_connection_id=execution_connection.id)
    )

    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    print("Result is: " + str(result))
    assert len(result["runs"]) == 2
    assert result["runs"][0]["state"] == ActionRunState.SUCCESS.value
    assert result["runs"][1]["state"] == ActionRunState.ERROR.value
    assert result["runs"][1]["error_message"] == error_message
    assert (
        result["state"] == WorkflowRunState.ERROR.value
    )  # If disable action retry, the state of whole workflow run will be error (no need to wait for retry)


async def test_delete_workflow_run(
    database_engine: SqlalchemyDatabaseEngine,
    project_authorization_service_mock: mock.MagicMock,
    authentication_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    # Debug SQL queries
    # logging.getLogger("sqlalchemy.engine").setLevel(logging.INFO)
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 0
    assert response.headers["Content-Range"] == "0-50/0"
    db = database_engine.get_session()
    action_def1 = ActionDefinition(
        id="mdf1",
        project_id="project_id",
        technical_name="tmdf1",
        human_name="action human_name",
        description="descr",
        version="1.0",
        kind=ActionKind.TRIGGER,
        container_image="ctn1",
        inputs=[],
        outputs=[
            ActionOutput(
                id="file",
                name="filename",
                type=ActionIOType.FILE,
                enum_values=[],
                display_name="",
                help="",
                optional=False,
            )
        ],
        addons_inputs=[],
    )
    static_input_1 = StaticInput(id="foo", name="baz", value="bar")
    reference_input_1 = ReferenceInput(
        id="baz",
        name="ref_input_name",
        action_name="Workflowaction1",
        output_name="action_output",
    )
    wfm1 = WorkflowAction(
        id="Workflowaction1",
        id_in_studio="WorkflowactionNAME1",
        workflow_deployment_id="workflow_deployment_id1",
        workflow_definition_id="workflow_definition_id1",
        definition=action_def1,
        static_inputs=[static_input_1],
        reference_inputs=[reference_input_1],
        constraints=None,
        objectives=None,
    )
    wf = WorkflowDeployment(
        id="workflow_deployment_id1",
        name="A nice workflow_deployment_id1",
        project_id="project_id",
        creation_time=str(now),
        workflow_definition_id="workflow_definition_id1",
        actions=[wfm1],
    )
    db.add(wf)

    action_deployment_1 = ActionDeployment(
        id="mod_depl_1",
        project_id="project_id",
        action_definition=action_def1,
        state=ActionDeploymentState.UNDEPLOYED,
        execution_type=ActionExecutionType.DUMMY,
        node_pool=None,
    )
    db.add(action_deployment_1)
    action_deployment_2 = ActionDeployment(
        id="mod_depl_2",
        project_id="project_id",
        action_definition=action_def1,
        state=ActionDeploymentState.UNDEPLOYED,
        execution_type=ActionExecutionType.DUMMY,
        node_pool=None,
    )
    db.add(action_deployment_2)

    action_def_to_be_deleted = ActionDefinition(
        id="mdfdelete",
        project_id="project_id",
        technical_name="tmdf1delete",
        human_name="action human_name delete me",
        description="descr i will be deleted",
        version="1.0",
        kind=ActionKind.TRIGGER,
        container_image="ctn1",
        inputs=[],
        outputs=[],
    )
    static_input_to_be_deleted = StaticInput(
        id="delete_me_static_in", name="name", value="1"
    )
    reference_input_to_be_deleted = ReferenceInput(
        id="delete_me_reference_input",
        name="name",
        action_name="theaction",
        output_name="theoutput",
    )
    workflow_action_to_be_deleted = WorkflowAction(
        id="wfmod_1",
        id_in_studio="wfmod",
        workflow_deployment_id="wfdeploy",
        workflow_definition_id="baz",
        definition=action_def_to_be_deleted,
        static_inputs=[static_input_to_be_deleted],
        reference_inputs=[reference_input_to_be_deleted],
        constraints=None,
        objectives=None,
    )
    workflow_deployment_to_be_deleted = WorkflowDeployment(
        id="wfdeploy",
        name="wfdeploy1",
        project_id="project",
        creation_time=str(now),
        workflow_definition_id="foobar",
        state=WorkflowDeploymentState.STOPPED,
        actions=[workflow_action_to_be_deleted],
    )
    db.add(workflow_deployment_to_be_deleted)

    action_deployment_not_undeployed = ActionDeployment(
        id="mod_depl_deploying",
        project_id="project_id",
        action_definition=action_def1,
        state=ActionDeploymentState.DEPLOYING,
        execution_type=ActionExecutionType.DUMMY,
        node_pool=None,
    )
    db.add(action_deployment_not_undeployed)
    db.add(
        ExecutionConnection(
            id="et1",
            project_id="project_id",
            workflow_action=wfm1,
            workflow_run_id="wfr1_SS",
            submitted_at=datetime.datetime.fromtimestamp(1, datetime.timezone.utc),
            time_allotment=123,
        )
    )
    db.add(
        ExecutionResult(
            id="res1",
            workflow_run_id="wfr1_SS",
            project_id="project_id",
            submitted_at=now - datetime.timedelta(minutes=1),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.SUCCESS,
            execution_connection_id="et1",
            workflow_action=wfm1,
            stored_files=[
                StoredFile(
                    id="sf-1",
                    io_name="foo",
                    file_path="test/sf1",
                    project_id="project_id",
                    mimetype="",
                    size_in_bytes=123,
                    encoding="",
                ),
                StoredFile(
                    id="sf-2",
                    io_name="foo",
                    file_path="test/sf2",
                    project_id="project_id",
                    mimetype="",
                    size_in_bytes=123,
                    encoding="",
                ),
            ],
        )
    )
    db.add(
        ExecutionResult(
            id="res2",
            workflow_run_id="wfr1_SS",
            project_id="project_id",
            submitted_at=now - datetime.timedelta(minutes=2),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.SUCCESS,
            execution_connection_id="et2",
            workflow_action=wfm1,
        )
    )
    db.add(
        ExecutionConnection(
            id="et3",
            project_id="project_id",
            workflow_action=wfm1,
            workflow_run_id="wfr2_SE",
            submitted_at=datetime.datetime.fromtimestamp(3, datetime.timezone.utc),
            time_allotment=123,
        )
    )
    db.add(
        ExecutionResult(
            id="res3",
            workflow_run_id="wfr2_SE",
            project_id="project_id",
            submitted_at=now - datetime.timedelta(minutes=1),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.SUCCESS,
            execution_connection_id="et3",
            workflow_action=wfm1,
        )
    )
    db.add(
        ExecutionConnection(
            id="et4",
            project_id="project_id",
            workflow_action=wfm1,
            workflow_run_id="wfr2_SE",
            submitted_at=datetime.datetime.fromtimestamp(4, datetime.timezone.utc),
            time_allotment=123,
        )
    )
    db.add(
        ExecutionResult(
            id="res4",
            workflow_run_id="wfr2_SE",
            project_id="project_id",
            submitted_at=now - datetime.timedelta(minutes=1),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.ERROR,
            execution_connection_id="et4",
            workflow_action=wfm1,
            outputs={"file": "path/to/myfile.txt"},
            stored_files=[
                StoredFile(
                    id="test",
                    io_name="file",
                    file_path="toto",
                    project_id="project_id",
                    size_in_bytes=23,
                    encoding="utf-8",
                    mimetype="app",
                )
            ],
        )
    )
    db.add(
        ExecutionConnection(
            id="et5",
            project_id="project_id",
            workflow_action=wfm1,
            workflow_run_id="wfr3_EE",
            submitted_at=datetime.datetime.fromtimestamp(5, datetime.timezone.utc),
            time_allotment=123,
        )
    )
    db.add(
        ExecutionResult(
            id="res5",
            workflow_run_id="wfr3_EE",
            project_id="project_id",
            submitted_at=now - datetime.timedelta(minutes=1),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.ERROR,
            execution_connection_id="et5",
            workflow_action=wfm1,
        )
    )
    db.add(
        ExecutionConnection(
            id="et6",
            project_id="project_id",
            workflow_action=wfm1,
            workflow_run_id="wfr3_EE",
            submitted_at=datetime.datetime.fromtimestamp(6, datetime.timezone.utc),
            time_allotment=123,
        )
    )
    db.add(
        ExecutionResult(
            id="res6",
            workflow_run_id="wfr3_EE",
            project_id="project_id",
            submitted_at=now - datetime.timedelta(minutes=1),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.ERROR,
            execution_connection_id="et6",
            workflow_action=wfm1,
        )
    )
    db.add(
        ExecutionConnection(
            id="et7",
            project_id="project_id",
            workflow_action=wfm1,
            workflow_run_id="wfr4_another_workflow",
            submitted_at=datetime.datetime.fromtimestamp(7, datetime.timezone.utc),
            time_allotment=123,
        )
    )
    db.add(
        ExecutionResult(
            id="res7",
            workflow_run_id="wfr4_another_workflow",
            workflow_action=WorkflowAction(
                id="Workflowaction2",
                id_in_studio="WorkflowactionNAME2",
                workflow_deployment_id="workflow_deployment_id1",
                workflow_definition_id="workflow_definition_id1",
                definition=ActionDefinition(
                    id="mdf2",
                    project_id="project_id",
                    technical_name="tmdf2",
                    human_name="action human_name",
                    description="descr",
                    version="1.0",
                    kind=ActionKind.TRIGGER,
                    container_image="ctn1",
                    inputs=[],
                    outputs=[],
                ),
                static_inputs=[],
                constraints=None,
                objectives=None,
                reference_inputs=[],
            ),
            project_id="project_id",
            submitted_at=now,
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.SUCCESS,
            execution_connection_id="et7",
        )
    )
    db.add(
        ExecutionConnection(
            id="et8_no_result",
            project_id="project_id",
            workflow_action=wfm1,
            workflow_run_id="wfr4_another_workflow",
            submitted_at=datetime.datetime.fromtimestamp(8, datetime.timezone.utc),
            time_allotment=123,
        )
    )
    workflow_run = generate_workflow_run()
    workflow_run.id = "wfr1_SS"
    workflow_run.project_id = "project_id"
    workflow_run.runs = []

    db.add(workflow_run)

    db.commit()
    workflow_run_to_delete = "wfr1_SS"
    connection_query = select(ExecutionConnection).filter(
        ExecutionConnection.workflow_run_id == workflow_run_to_delete
    )
    query = select(ExecutionResult).filter(
        ExecutionResult.workflow_run_id == workflow_run_to_delete
    )
    execution_connections = db.execute(connection_query).all()
    assert len(execution_connections) == 1

    execution_results = db.execute(query).all()
    assert len(execution_results) == 2

    db.close()

    # Delete wfr1_SS and ensure all referenced objects are deleted
    response = await api_client.delete(f"/workflow_runs/{workflow_run_to_delete}")
    assert response.status == 200
    result = await response.json()
    assert not result

    db = database_engine.get_session()
    # Ensure ExecutionConnection objects are deleted
    execution_connections = db.execute(connection_query).all()
    assert len(execution_connections) == 0

    # Ensure ExecutionResult objects are deleted
    execution_results = db.execute(query).all()
    assert len(execution_results) == 0

    # Ensure only the ActionDeployments that are undeployed and unreferenced get deleted
    # ('mod_depl_deploying' is deploying and must not be deleted yet)
    assert (
        db.get(ActionDeployment, action_deployment_not_undeployed.id)
        == action_deployment_not_undeployed
    )
    # Ensure associated StaticInputs are deleted ("delete_me_static_in")
    assert not db.get(StaticInput, static_input_to_be_deleted.id)

    # Ensure associated ReferenceInputs are deleted ('delete_me_reference_input')
    assert (
        not db.query(ReferenceInput)
        .filter(ReferenceInput.id == reference_input_to_be_deleted.id)
        .all()
    )

    # Ensure associated ActionDefinitions are deleted ('mdfdelete')
    assert not db.get(ActionDefinition, action_def_to_be_deleted.id)

    # Ensure associated WorkflowActions are deleted ('wfmod_1')
    assert not db.get(WorkflowAction, workflow_action_to_be_deleted.id)

    # Try to delete the WorkflowRun that has already been deleted, get 404
    response = await api_client.delete("/workflow_runs/wfr1_SS")
    assert response.status == 404


async def test_state_error(
    database_session: Session,
    project_authorization_service_mock: mock.MagicMock,
    authentication_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    with database_session as db:
        workflow_run_id_error = "wfr_error"
        workflow_run = generate_workflow_run("success")
        workflow_run.id = workflow_run_id_error
        workflow_run.project_id = "project_id"
        action_run1 = generate_action_run(
            "1", ExecutionResultState.ERROR, True, True, True
        )  # Really error
        action_run1.state = ActionRunState.ERROR
        action_run2 = generate_action_run("2")
        action_run2.state = ActionRunState.WAITING
        workflow_run.runs = [action_run1, action_run2]
        workflow_run.number_of_completed_actions = 1
        workflow_run.refresh_state()
        db.add(workflow_run)

        db.add(
            WorkflowDeployment(
                id=action_run1.workflow_action.workflow_deployment_id,
                name="A nice workflow_deployment_id1",
                project_id="project_id",
                creation_time=str(now),
                workflow_definition_id="workflow_definition_id1",
                actions=[action_run1.workflow_action, action_run2.workflow_action],
            )
        )
        db.commit()

        response = await api_client.get("/workflow_runs")
        assert response.status == 200
        result = await response.json()
        assert len(result) == 1
        assert response.headers["Content-Range"] == "0-50/1"

        response = await api_client.get(f"/workflow_runs/{workflow_run_id_error}")
        assert response.status == 200
        result = await response.json()
        assert result["state"] == "Error"
        assert result["total_steps"] == 2
        assert result["completed_steps"] == 1
        assert "1" in result["runs"][0]["id"]
        assert result["runs"][0]["state"] == "Error"
        assert "2" in result["runs"][1]["id"]
        assert result["runs"][1]["state"] == "Waiting"


async def test_state_completed(
    database_session: Session,
    project_authorization_service_mock: mock.MagicMock,
    authentication_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    with database_session as db:
        workflow_run_id_success = "wfr_success"
        workflow_run = generate_workflow_run("success")
        workflow_run.id = workflow_run_id_success
        workflow_run.project_id = "project_id"
        action_run1 = generate_action_run("1success")
        action_run1.state = ActionRunState.SUCCESS
        action_run2 = generate_action_run("2success")
        action_run2.state = ActionRunState.SUCCESS
        workflow_run.runs = [action_run1, action_run2]
        workflow_run.refresh_state()
        db.add(workflow_run)

        db.add(
            WorkflowDeployment(
                id=action_run1.workflow_action.workflow_deployment_id,
                name="A nice workflow_deployment_id1",
                project_id="project_id",
                creation_time=str(now),
                workflow_definition_id="workflow_definition_id1",
                actions=[action_run1.workflow_action, action_run2.workflow_action],
            )
        )
        db.commit()

        response = await api_client.get("/workflow_runs")
        assert response.status == 200
        result = await response.json()
        assert len(result) == 1
        assert response.headers["Content-Range"] == "0-50/1"

        response = await api_client.get(f"/workflow_runs/{workflow_run_id_success}")
        assert response.status == 200
        result = await response.json()
        assert result["state"] == "Completed"
        assert result["total_steps"] == 2
        assert result["completed_steps"] == 2
        assert "1" in result["runs"][0]["id"]
        assert result["runs"][0]["state"] == "Success"
        assert "2" in result["runs"][1]["id"]
        assert result["runs"][1]["state"] == "Success"


async def test_state_canceled_on_trigger(
    database_session: Session,
    project_authorization_service_mock: mock.MagicMock,
    authentication_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    with database_session as db:
        workflow_run_id_success = "wfr_success"
        workflow_run = generate_workflow_run("success")
        workflow_run.id = workflow_run_id_success
        workflow_run.project_id = "project_id"
        action_run1 = generate_action_run("1success")
        action_run1.state = ActionRunState.SUCCESS
        action_run2 = generate_action_run("2success")
        action_run2.state = ActionRunState.SUCCESS
        workflow_run.runs = [action_run1, action_run2]
        workflow_run.refresh_state()
        db.add(workflow_run)
        db.add(
            WorkflowDeployment(
                id=action_run1.workflow_action.workflow_deployment_id,
                name="A nice workflow_deployment_id1",
                project_id="project_id",
                creation_time=str(now),
                workflow_definition_id="workflow_definition_id1",
                actions=[action_run1.workflow_action, action_run2.workflow_action],
            )
        )

        workflow_run_id_canceled = "wfr_canceled"
        workflow_run = generate_workflow_run("1")
        workflow_run.id = workflow_run_id_canceled
        workflow_run.project_id = "project_id"
        action_run1 = generate_action_run("1")
        action_run1.state = ActionRunState.CANCELED
        action_run2 = generate_action_run("2")
        action_run2.state = ActionRunState.WAITING
        workflow_run.runs = [action_run1, action_run2]
        workflow_run.number_of_completed_actions = 1
        workflow_run.refresh_state()
        db.add(workflow_run)
        db.commit()

        response = await api_client.get("/workflow_runs")
        assert response.status == 200
        result = await response.json()
        assert len(result) == 2
        assert response.headers["Content-Range"] == "0-50/2"

        response = await api_client.get(f"/workflow_runs/{workflow_run_id_success}")
        assert response.status == 200
        result = await response.json()
        assert result["state"] == "Completed"
        assert result["total_steps"] == 2
        assert result["completed_steps"] == 2
        assert "1" in result["runs"][0]["id"]
        assert result["runs"][0]["state"] == "Success"
        assert "2" in result["runs"][1]["id"]
        assert result["runs"][1]["state"] == "Success"

        response = await api_client.get(f"/workflow_runs/{workflow_run_id_canceled}")
        assert response.status == 200
        result = await response.json()
        assert result["state"] == "Canceled"
        assert result["total_steps"] == 2
        assert result["completed_steps"] == 1
        assert "1" in result["runs"][0]["id"]
        assert result["runs"][0]["state"] == "Canceled"
        assert "2" in result["runs"][1]["id"]
        assert result["runs"][1]["state"] == "Waiting"


async def test_workflow_run_canceled_last_step(
    database_session: Session,
    project_authorization_service_mock: mock.MagicMock,
    authentication_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    with database_session as db:
        workflow_run_id_canceled = "wfr_canceled"
        workflow_run = generate_workflow_run("1")
        workflow_run.id = workflow_run_id_canceled
        workflow_run.project_id = "project_id"
        action_run1 = generate_action_run("1")
        action_run1.state = ActionRunState.SUCCESS
        action_run2 = generate_action_run("2")
        action_run2.state = ActionRunState.CANCELED
        workflow_run.runs = [action_run1, action_run2]
        workflow_run.refresh_state()
        db.add(workflow_run)
        db.add(
            WorkflowDeployment(
                id=action_run1.workflow_action.workflow_deployment_id,
                name="A nice workflow_deployment_id1",
                project_id="project_id",
                creation_time=str(now),
                workflow_definition_id="workflow_definition_id1",
                actions=[action_run1.workflow_action, action_run2.workflow_action],
            )
        )
        db.commit()

        response = await api_client.get("/workflow_runs")
        assert response.status == 200
        result = await response.json()
        assert len(result) == 1
        assert response.headers["Content-Range"] == "0-50/1"

        response = await api_client.get(f"/workflow_runs/{workflow_run_id_canceled}")
        assert response.status == 200
        result = await response.json()
        assert result["state"] == "Canceled"
        assert result["total_steps"] == 2
        assert result["completed_steps"] == 2
        assert result["runs"][0]["state"] == "Success"
        assert result["runs"][1]["state"] == "Canceled"
