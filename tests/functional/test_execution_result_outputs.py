import datetime
from unittest import mock

from aiohttp.test_utils import TestClient

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionDynamicOutput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
    OutputReference,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_result.workflow_result import WorkflowResult
from ryax.runner.domain.workflow_run.workflow_run_entities import WorkflowRunState
from ryax.common.infrastructure.database.engine import Session
from tests.runner.domain.workflow_run import generate_workflow_run
from tests.helper import now


def test_get_all_previous_outputs(
    database_session: Session, app_container: ApplicationContainer
) -> None:
    workflow_action_1 = WorkflowAction(
        id="1",
        id_in_studio="1",
        next_actions=[],
        is_root_action=True,
        workflow_definition_id="",
        workflow_deployment_id="workflow_deployment_id1",
        connection_execution_id="",
        static_inputs=[],
        reference_inputs=[],
        dynamic_outputs=[
            ActionDynamicOutput(
                name="dynamicoutput",
                type=ActionIOType.STRING,
                display_name="",
                id="dynamic_output_1",
                enum_values=[],
                help="",
            ),
        ],
        definition=ActionDefinition(
            id="2",
            has_dynamic_outputs=True,
            inputs=[],
            outputs=[
                ActionOutput(
                    id="action_output_1",
                    name="output2name",
                    type=ActionIOType.STRING,
                    display_name="",
                    help="",
                    enum_values=[],
                )
            ],
            technical_name="",
            project_id="project_id",
            human_name="",
            description="",
            version="",
            kind=ActionKind.PROCESSOR,
            container_image="",
        ),
        constraints=None,
        objectives=None,
    )
    workflow_action_2 = WorkflowAction(
        id="2",
        id_in_studio="2",
        next_actions=[],
        is_root_action=True,
        workflow_definition_id="workflow_deployment_id1",
        workflow_deployment_id="",
        connection_execution_id="",
        static_inputs=[],
        reference_inputs=[],
        dynamic_outputs=[
            ActionDynamicOutput(
                name="dynamicoutput",
                type=ActionIOType.STRING,
                display_name="",
                id="action_dynamic_output_2",
                enum_values=[],
                help="",
            ),
        ],
        definition=ActionDefinition(
            has_dynamic_outputs=True,
            inputs=[],
            outputs=[
                ActionOutput(
                    id="action_output_2",
                    name="output2name",
                    type=ActionIOType.STRING,
                    display_name="",
                    help="",
                    enum_values=[],
                )
            ],
            technical_name="",
            project_id="project_id",
            human_name="",
            description="",
            version="",
            kind=ActionKind.PROCESSOR,
            id="1",
            container_image="",
        ),
        constraints=None,
        objectives=None,
    )
    wf = WorkflowDeployment(
        id="workflow_deployment_id1",
        name="A nice workflow_deployment_id1",
        project_id="project_id",
        creation_time=str(now),
        workflow_definition_id="workflow_definition_id1",
        actions=[workflow_action_1, workflow_action_2],
    )
    execution_res1 = ExecutionResult(
        id="1",
        project_id="project_id",
        started_at=datetime.datetime.now(datetime.timezone.utc),
        submitted_at=datetime.datetime.now(datetime.timezone.utc),
        execution_connection_id="1234",
        workflow_run_id="",
        workflow_action=workflow_action_1,
        outputs={"action_output_1": "static_value", "dynamic_output_1": "dyn_value"},
    )

    execution_res2 = ExecutionResult(
        id="2",
        project_id="project_id",
        started_at=datetime.datetime.now(datetime.timezone.utc),
        submitted_at=datetime.datetime.now(datetime.timezone.utc),
        execution_connection_id="1234",
        workflow_run_id="",
        parents=[execution_res1],
        workflow_action=workflow_action_2,
        outputs={
            "action_output_2": "static_value_2",
            "action_dynamic_output_2": "dyn_value_2",
        },
    )
    database_session.add(wf)
    database_session.add(execution_res1)
    database_session.add(execution_res2)
    database_session.commit()

    previous_outputs = execution_res2.get_all_previous_outputs()
    assert previous_outputs == [
        OutputReference(
            name="action_output_2",
            value="static_value_2",
            workflow_action_name="2",
            type=ActionIOType.STRING,
        ),
        OutputReference(
            name="action_dynamic_output_2",
            value="dyn_value_2",
            workflow_action_name="2",
            type=ActionIOType.STRING,
        ),
        OutputReference(
            name="action_output_1",
            value="static_value",
            workflow_action_name="1",
            type=ActionIOType.STRING,
        ),
        OutputReference(
            name="dynamic_output_1",
            value="dyn_value",
            workflow_action_name="1",
            type=ActionIOType.STRING,
        ),
    ]


async def test_get_result(
    database_session: Session,
    project_authorization_service_mock: mock.MagicMock,
    authentication_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    workflow_deployment_id = "workflow_deployment_id1"

    workflow_action_1 = WorkflowAction(
        id="wf_mod1",
        id_in_studio="wf_mod1",
        next_actions=[],
        is_root_action=True,
        workflow_definition_id="workflow_definition_id1",
        workflow_deployment_id=workflow_deployment_id,
        connection_execution_id="connection_execution_id1",
        static_inputs=[],
        reference_inputs=[],
        dynamic_outputs=[
            ActionDynamicOutput(
                type=ActionIOType.STRING,
                name="dynamicoutputname",
                display_name="",
                id="action_dynamic_output_1",
                enum_values=[],
                help="",
            ),
        ],
        definition=ActionDefinition(
            id="mod_def1",
            has_dynamic_outputs=True,
            inputs=[],
            outputs=[
                ActionOutput(
                    id="action_output_1",
                    name="static",
                    type=ActionIOType.STRING,
                    display_name="",
                    help="",
                    enum_values=[],
                )
            ],
            technical_name="mod_def1_tname",
            project_id="project_id",
            human_name="",
            description="",
            version="",
            kind=ActionKind.PROCESSOR,
            container_image="",
        ),
        constraints=None,
        objectives=None,
    )
    workflow_action_2 = WorkflowAction(
        id="wf_mod2",
        id_in_studio="wf_mod2",
        next_actions=[],
        is_root_action=True,
        workflow_definition_id="workflow_definition_id1",
        workflow_deployment_id=workflow_deployment_id,
        connection_execution_id="",
        static_inputs=[],
        reference_inputs=[],
        dynamic_outputs=[
            ActionDynamicOutput(
                name="dynamicoutput",
                type=ActionIOType.STRING,
                display_name="",
                id="action_dynamic_output_2",
                enum_values=[],
                help="",
            ),
        ],
        definition=ActionDefinition(
            id="mod_def2",
            has_dynamic_outputs=True,
            inputs=[],
            outputs=[
                ActionOutput(
                    id="action_output_2",
                    name="output2name",
                    type=ActionIOType.STRING,
                    display_name="",
                    help="",
                    enum_values=[],
                )
            ],
            technical_name="mod_def2_tname",
            project_id="project_id",
            human_name="",
            description="",
            version="",
            kind=ActionKind.PROCESSOR,
            container_image="",
        ),
        constraints=None,
        objectives=None,
    )
    workflow_deployment = WorkflowDeployment(
        project_id="project_id",
        id=workflow_deployment_id,
        actions=[workflow_action_1, workflow_action_2],
        workflow_definition_id="",
        creation_time=str(now),
        name="TOTO",
    )
    database_session.add(workflow_deployment)
    execution_res1 = ExecutionResult(
        id="execres1",
        project_id="project_id",
        started_at=datetime.datetime.now(datetime.timezone.utc),
        submitted_at=datetime.datetime.now(datetime.timezone.utc),
        execution_connection_id="1234",
        workflow_run_id="workflow_run_id1",
        workflow_action=workflow_action_1,
        outputs={"static": "static_value", "dyn": "dyn_value"},
        inputs={"input1": "input1_value", "input2": "input2_value"},
    )
    database_session.add(execution_res1)
    execution_res2 = ExecutionResult(
        id="execres2",
        project_id="project_id",
        started_at=datetime.datetime.now(datetime.timezone.utc),
        submitted_at=datetime.datetime.now(datetime.timezone.utc),
        execution_connection_id="1234",
        workflow_run_id="workflow_run_id1",
        parents=[execution_res1],
        workflow_action=workflow_action_2,
        outputs={"static": "static_value_2", "dyn": "dyn_value_2"},
    )
    database_session.add(execution_res2)
    wfres2 = WorkflowResult(
        id="wfres2",
        key="key_wfres2",
        technical_name="static",
        workflow_action_io_id="wfres2_workflow_action_io_id",
        action_id="wf_mod1",
        workflow_deployment_id=workflow_deployment_id,
    )
    database_session.add(wfres2)
    workflow_run = generate_workflow_run()
    workflow_run.id = "workflow_run_id1"
    workflow_run.state = WorkflowRunState.COMPLETED
    database_session.add(workflow_run)

    database_session.commit()

    response = await api_client.get("/results/NON_EXISTING_workflow_run_id")
    assert response.status == 404

    response = await api_client.get("/results/workflow_run_id1")
    assert response.status == 200
    result = await response.json()
    assert result == {"key_wfres2": "static_value"}

    wfres1 = WorkflowResult(
        id="wfres1",
        key="key_wfres1",
        technical_name="inputX",
        workflow_action_io_id="wfres1_workflow_action_io_id",
        action_id="wf_mod3",
        workflow_deployment_id=workflow_deployment_id,
    )
    database_session.add(wfres1)
    database_session.commit()

    response = await api_client.get("/results/workflow_run_id1")
    assert response.status == 503
