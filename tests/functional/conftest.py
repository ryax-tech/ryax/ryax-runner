import tempfile
from unittest import mock

import pytest
import sqlalchemy
from aiohttp import test_utils, web
from sqlalchemy.orm import clear_mappers

from ryax.common.domain.internal_messaging.event_publisher import IPublisherService
from ryax.runner.application.authentication_service import AuthenticationService
from ryax.common.application.message_bus import MessageBus
from ryax.runner.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.runner.application.setup import setup as application_setup
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.auth_token import AuthToken
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.infrastructure.api.setup import setup as setup_api
from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine, Session
from ryax.runner.infrastructure.database.mapper import map_orm
from ryax.runner.infrastructure.database.metadata import metadata
from ryax.runner.infrastructure.database.repositories.execution_result_repository import (
    DatabaseExecutionResultRepository,
)


@pytest.fixture()
def app_container() -> ApplicationContainer:
    app_container = ApplicationContainer()
    app_container.configuration.storage.local.type.from_value("posix")
    app_container.configuration.default_time_allotment_in_seconds.from_value(900)
    app_container.configuration.encryption_key.from_value(
        b"Aj3AS7LHnEs850ZlpRBaQvQV5MisDU8-QTyz_vHSUrc="
    )
    app_container.configuration.actions.log_level.from_value("info")
    app_container.message_bus.override(MessageBus(keep_event_history=True))
    application_setup(app_container)
    app_container.configuration.undeploy_idle_actions_after_seconds.from_value(1)
    app_container.configuration.deployment.logs.logs_max_size_in_bytes.from_value(
        1024 * 1024
    )
    app_container.configuration.deployment.logs.log_line_max_size_in_bytes.from_value(
        1000
    )
    app_container.configuration.scheduler.max_action_deployments.from_value(50)
    app_container.configuration.scheduler.max_concurrent_deploying_actions.from_value(3)

    yield app_container
    app_container.unwire()


@pytest.fixture()
def api_client(
    loop, app_container: ApplicationContainer, aiohttp_client
) -> test_utils.TestClient:
    app = web.Application()
    setup_api(app, app_container)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture()
def authentication_service_mock(app_container: ApplicationContainer):
    authentication_service_mock = mock.MagicMock(AuthenticationService)
    authentication_service_mock.check_access = mock.MagicMock(
        return_value=AuthToken(user_id="user")
    )
    app_container.authentication_service.override(authentication_service_mock)
    return authentication_service_mock


@pytest.fixture()
def project_authorization_service_mock(app_container: ApplicationContainer):
    project_authorization_service_mock = mock.MagicMock(ProjectAuthorizationService)
    project_authorization_service_mock.get_current_project = mock.AsyncMock(
        return_value="project_id"
    )
    app_container.project_authorization_service.override(
        project_authorization_service_mock
    )
    return project_authorization_service_mock


@pytest.fixture()
def execution_result_repository_mock(
    app_container: ApplicationContainer,
):
    execution_result_repository_mock = mock.MagicMock(DatabaseExecutionResultRepository)
    exec_result_mock = mock.Mock(ExecutionResult)
    exec_result_mock.workflow_run_id = "123"
    execution_result_repository_mock.get = mock.Mock(return_value=exec_result_mock)
    app_container.execution_result_repository.override(execution_result_repository_mock)
    return execution_result_repository_mock


@pytest.fixture()
def database_engine(app_container: ApplicationContainer) -> SqlalchemyDatabaseEngine:
    with tempfile.TemporaryDirectory() as tmp_dir:
        database_url = f"sqlite:///{tmp_dir}/ryax.db"
        engine = SqlalchemyDatabaseEngine(database_url, map_orm)
        engine.connect()
        app_container.database_engine.override(engine)
        yield engine
        clear_mappers()
        metadata.drop_all(engine._engine)
        engine.disconnect()


@pytest.fixture(scope="function")
def database_session(database_engine: SqlalchemyDatabaseEngine) -> Session:
    session = database_engine.get_session()
    yield session
    sqlalchemy.orm.close_all_sessions()


@pytest.fixture()
def message_publisher_mock(app_container: ApplicationContainer) -> None:
    app_container.message_publisher.override(mock.MagicMock(IPublisherService))


@pytest.fixture()
def message_bus_mock(app_container: ApplicationContainer) -> None:
    app_container.message_bus.override(mock.AsyncMock(IMessageBus))
    return app_container.message_bus()
