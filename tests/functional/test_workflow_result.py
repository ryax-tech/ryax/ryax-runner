import datetime

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_result.workflow_result import WorkflowResult
from ryax.common.infrastructure.database.engine import Session
from tests.runner.domain.workflow_run import generate_workflow_run


async def test_get_workflow_results(
    database_session: Session,
    project_authorization_service_mock,
    authentication_service_mock,
    api_client,
) -> None:
    project_id = "project_id"
    workflow_run_id = "workflow_run"
    result_technical_name = "techname"
    workflow_action_id = "workflow_action"
    workflow_deployment_id = "workflow_deployment"
    action_definition = ActionDefinition(
        id="moddef",
        project_id=project_id,
        technical_name="name",
        version="2",
        kind=ActionKind.TRIGGER,
        container_image="cont",
        human_name="hname",
        description="desc",
        inputs=[],
        outputs=[
            ActionOutput(
                id="io1",
                name=result_technical_name,
                type=ActionIOType.STRING,
                display_name="io1",
                help="help",
                enum_values=[],
            ),
        ],
    )

    workflow_action = WorkflowAction(
        id=workflow_action_id,
        id_in_studio="workflow_action",
        workflow_deployment_id=workflow_deployment_id,
        workflow_definition_id="workflow_definition",
        definition=action_definition,
        static_inputs=[],
        reference_inputs=[],
        constraints=None,
        objectives=None,
    )
    execution_result = ExecutionResult(
        id="execresultid",
        project_id=project_id,
        execution_connection_id="conn",
        workflow_action=workflow_action,
        submitted_at=datetime.datetime.now(datetime.timezone.utc),
        started_at=datetime.datetime.now(datetime.timezone.utc),
        workflow_run_id=workflow_run_id,
        end_of_log_delimiter=None,
        outputs={result_technical_name: "RESULT VALUE"},
    )
    workflow_result = WorkflowResult(
        id="id",
        key="kk",
        technical_name=result_technical_name,
        workflow_action_io_id="io",
        action_id=workflow_action_id,
        workflow_deployment_id=workflow_deployment_id,
    )
    workflow_deployment = WorkflowDeployment(
        id=workflow_deployment_id,
        name="",
        project_id=project_id,
        creation_time=datetime.datetime.now(datetime.timezone.utc),
        workflow_definition_id="workflow_definition",
    )
    workflow_run = generate_workflow_run()
    workflow_run.id = workflow_run_id
    workflow_run.runs = []
    database_session.add(workflow_run)
    database_session.add(workflow_deployment)
    database_session.add(action_definition)
    database_session.add(workflow_action)
    database_session.add(execution_result)
    database_session.add(workflow_result)
    database_session.commit()

    response = await api_client.get(f"/results/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    assert (
        result[workflow_result.key] == execution_result.outputs[result_technical_name]
    )


async def test_get_workflow_results_when_503_error(
    database_session: Session,
    project_authorization_service_mock,
    authentication_service_mock,
    api_client,
) -> None:
    project_id = "project_id"
    workflow_run_id = "workflow_run"
    result_technical_name = "techname"
    workflow_action_id = "workflow_action"
    workflow_deployment_id = "workflow_deployment"
    workflow_definition_id = "workflow_definition"
    action_definition = ActionDefinition(
        id="moddef",
        project_id=project_id,
        technical_name="name",
        version="2",
        kind=ActionKind.TRIGGER,
        container_image="cont",
        human_name="hname",
        description="desc",
        inputs=[],
        outputs=[],
    )

    workflow_action = WorkflowAction(
        id=workflow_action_id,
        id_in_studio="workflow_action",
        workflow_deployment_id=workflow_deployment_id,
        workflow_definition_id=workflow_definition_id,
        definition=action_definition,
        static_inputs=[],
        reference_inputs=[],
        constraints=None,
        objectives=None,
    )
    execution_result = ExecutionResult(
        id="execresultid",
        project_id=project_id,
        execution_connection_id="conn",
        workflow_action=workflow_action,
        submitted_at=datetime.datetime.now(datetime.timezone.utc),
        started_at=datetime.datetime.now(datetime.timezone.utc),
        workflow_run_id=workflow_run_id,
        end_of_log_delimiter=None,
        outputs={result_technical_name: None},
    )
    workflow_result = WorkflowResult(
        id="id",
        key="kk",
        technical_name=result_technical_name,
        workflow_action_io_id="io",
        action_id=workflow_action_id,
        workflow_deployment_id=workflow_deployment_id,
    )
    workflow_deployment = WorkflowDeployment(
        id=workflow_deployment_id,
        name="",
        project_id=project_id,
        creation_time=datetime.datetime.now(datetime.timezone.utc),
        workflow_definition_id=workflow_definition_id,
    )
    workflow_run = generate_workflow_run()
    workflow_run.id = workflow_run_id
    workflow_run.runs = []
    database_session.add(workflow_run)

    database_session.add(action_definition)
    database_session.add(workflow_action)
    database_session.add(execution_result)
    database_session.add(workflow_result)
    database_session.add(workflow_deployment)
    database_session.commit()

    response = await api_client.get(f"/results/{workflow_run_id}")
    assert response.status == 503
