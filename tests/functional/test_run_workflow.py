import asyncio
import datetime
from asyncio import Queue
from dataclasses import dataclass
from typing import List, Tuple
from unittest.mock import Mock

from aiohttp.test_utils import TestClient
from dependency_injector import providers
from marshmallow import RAISE

from ryax.common.domain.registration.registration_values import SiteType
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_commands import (
    CreateActionDefinitionCommand,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionInput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentIsReadyEvent,
)
from ryax.runner.domain.action_deployment.action_deployment_service import (
    IActionDeploymentService,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
)
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    InitializeExecutionResultCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionEndedEvent,
)
from ryax.runner.domain.execution_connection.execution_connection_service import (
    IExecutionService,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultFinishedEvent,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.common.domain.internal_messaging.base_messages import BaseEvent
from ryax.common.domain.internal_messaging.event_publisher import IPublisherService
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.runner.domain.site.site_events import RegisterSiteCommand
from ryax.common.domain.storage.storage_service import IStorageService
from ryax.runner.domain.workflow_action.workflow_action import (
    SchedulingConstraints,
    SchedulingObjectives,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
    StopWorkflowDeploymentFromDefinitionIdCommand,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_events import (
    WorkflowDeployedSuccessfully,
    WorkflowUndeployedSuccessfully,
)
from ryax.runner.infrastructure.api.schemas.workflow_run_schema import (
    WorkflowRunSchema,
    WorkflowRunSchemaMinimal,
)
from ryax.common.infrastructure.database.engine import Session

test_timeout = 5


class DeployExecSimulator:
    """
    A class that helps writing some integration tests.

    It helps you simulate:
    - the IActionDeploymentService that manages deployment of actions
    - the IPublisherService that sends messages through RabbitMQ
    - the IExecutionService that manages the running of actions

    For the ActionDeploymentService, if we want one to be ready, we need to manually call:
    await message_bus.handle([ActionDeploymentIsReadyEvent(action.id)])

    For the ExecutionService, if we want an execution connection to finish, we need to manually
    call:
    await self.message_bus.handle_event(
        ExecutionConnectionEndedEvent(
            execution.id,
            status=ExecutionConnectionState.COMPLETED,
        )
    )

    """

    def __init__(self, app_container: ApplicationContainer) -> None:
        self.app_container = app_container
        self._override_action_deployment_factory()
        self.messaging_publisher = DeployExecSimulator.DebugMessagingPublisher()
        self.app_container.message_publisher.override(self.messaging_publisher)
        self._override_execution_service_factory()

    def _override_action_deployment_factory(
        self,
    ) -> None:
        # The only way that I (David) found to be able to wait for a method of a Factory
        # to be awaited is to have a queue declared as a global variable that receives
        # all calls of the method.
        self.action_deployment_apply_calls: Queue[ActionDeployment] = Queue()
        self.action_deployment_watch_calls: Queue[ActionDeployment] = Queue()
        self.action_deployment_delete_calls: Queue[
            Tuple[ActionDeployment, str]
        ] = Queue()
        parent_self = self

        @dataclass
        class DebugActionDeploymentService(IActionDeploymentService):
            async def apply(
                self,
                action: ActionDeployment,
                addons_extra_params: dict = lambda: dict(),
            ) -> None:
                await parent_self.action_deployment_apply_calls.put(action)

            async def get_current_state(
                self, action_deployment: ActionDeployment
            ) -> ActionDeploymentState:
                return ActionDeploymentState.READY

            async def watch(self, action: ActionDeployment) -> None:
                await parent_self.action_deployment_watch_calls.put(action)

            async def unwatch(self, action_deployment: ActionDeployment) -> None:
                pass

            async def delete(self, action_deployment: ActionDeployment) -> None:
                await parent_self.action_deployment_delete_calls.put(
                    (action_deployment, action_deployment.action_definition.id)
                )

        self.app_container.action_deployment_factory.factories["k8s"].override(
            providers.Factory(DebugActionDeploymentService)
        )

    class DebugMessagingPublisher(IPublisherService):
        def __init__(self) -> None:
            # The queue has to be created here, to be on the same asyncio loop that the test
            self.message_sent: Queue[BaseEvent] = Queue()

        async def publish(self, events: List[BaseEvent], **kargs) -> None:
            for e in events:
                await self.message_sent.put(e)

    def _override_execution_service_factory(self) -> None:
        self.execution_service_run_calls: Queue[ExecutionConnection] = Queue()
        parent_self = self

        @dataclass
        class DebugExecutionService(IExecutionService):
            storage_service: IStorageService
            message_bus: IMessageBus

            async def start(
                self,
                execution: ExecutionConnection,
                addons_extra_params: dict,
            ) -> None:
                await parent_self.execution_service_run_calls.put(execution)

            async def stop(self, execution: ExecutionConnection) -> None:
                await self.message_bus.handle(
                    [
                        ExecutionConnectionEndedEvent(
                            execution.id,
                            status=ExecutionConnectionState.COMPLETED,
                        )
                    ]
                )

        self.app_container.execution_service_factory.factories["GRPC_V1"].override(
            providers.Factory(
                DebugExecutionService,
                storage_service=parent_self.app_container.local_storage_service,
                message_bus=parent_self.app_container.message_bus,
            )
        )


async def init_site(message_bus: IMessageBus) -> None:
    site = Site(
        id="site1",
        name="site1",
        type=SiteType.KUBERNETES,
    )

    site.node_pools = [NodePool(site=site, id="np1", memory=1024, cpu=2, name="np1")]
    await message_bus.handle_command(RegisterSiteCommand(site))


async def init_with_workflow_2_actions(message_bus: IMessageBus) -> None:
    await init_site(message_bus)
    await message_bus.handle_command(
        CreateActionDefinitionCommand(
            id="action_def_source1",
            project_id="project_id",
            description="test_description",
            human_name="test_human_name",
            kind=ActionKind.TRIGGER,
            version="1.0",
            technical_name="technical_name1",
            container_image="container_image1",
            inputs=[
                ActionInput(
                    id="input1",
                    name="source_input1_name",
                    type=ActionIOType.STRING,
                    display_name="input1",
                    help="help",
                    enum_values=[],
                )
            ],
            outputs=[
                ActionOutput(
                    id="output1_id",
                    name="output1",
                    type=ActionIOType.STRING,
                    display_name="output1",
                    help="help",
                    enum_values=[],
                ),
                ActionOutput(
                    id="output2_id",
                    name="output2",
                    type=ActionIOType.FILE,
                    display_name="output2",
                    help="help",
                    enum_values=[],
                ),
            ],
            resources=None,
        )
    )
    await message_bus.handle_command(
        CreateActionDefinitionCommand(
            id="action_def_processor2",
            project_id="project_id",
            kind=ActionKind.PROCESSOR,
            description="test_description",
            human_name="test_human_name",
            version="1.0",
            technical_name="technical_name2",
            container_image="container_image2",
            inputs=[
                ActionInput(
                    id="input_def_1",
                    name="proc_input1_name",
                    type=ActionIOType.STRING,
                    display_name="input12",
                    help="help",
                    enum_values=[],
                ),
                ActionInput(
                    id="input_def_2",
                    name="proc_input2_name",
                    type=ActionIOType.STRING,
                    display_name="refinput22",
                    help="help",
                    enum_values=[],
                ),
            ],
            outputs=[],
        )
    )

    await message_bus.handle_command(
        CreateWorkflowDeploymentCommand(
            workflow_definition_id="workflow_definition_id1",
            project_id="project_id",
            workflow_name="workflow name1",
            actions=[
                CreateWorkflowDeploymentCommand.Action(
                    id_in_studio="source_name1",
                    action_definition_id="action_def_source1",
                    next_actions=["processor_name2"],
                    static_inputs={
                        "source_input1_name": "value1",
                    },
                    reference_inputs={},
                    constraints=SchedulingConstraints(
                        arch_list=[],
                        site_list=[],
                        site_type_list=[],
                        node_pool_list=[],
                    ),
                    objectives=SchedulingObjectives(
                        cost=0.1, energy=0.2, performance=0.3
                    ),
                ),
                CreateWorkflowDeploymentCommand.Action(
                    id_in_studio="processor_name2",
                    action_definition_id="action_def_processor2",
                    next_actions=[],
                    static_inputs={
                        "proc_input1_name": "value12",
                    },
                    reference_inputs={
                        "proc_input2_name": {
                            "action": "source_name1",
                            "output": "output1",
                        }
                    },
                    constraints=SchedulingConstraints(
                        arch_list=[],
                        site_list=[],
                        site_type_list=[],
                        node_pool_list=[],
                    ),
                    objectives=SchedulingObjectives(
                        cost=0.1, energy=0.2, performance=0.3
                    ),
                ),
            ],
            results=[
                CreateWorkflowDeploymentCommand.Result(
                    key="key",
                    technical_name="tname",
                    workflow_action_io_id="ioid",
                    action_id="action_id",
                )
            ],
        )
    )


async def test_execution_connection_complete_lifecycle(
    app_container: ApplicationContainer,
    project_authorization_service_mock: Mock,
    authentication_service_mock: Mock,
    api_client: TestClient,
    database_engine,
) -> None:
    app_container.configuration.set("keep_event_history", True)
    app_container.configuration.action_retry.collect_oom_end_waiting_seconds.from_value(
        0
    )
    app_container.configuration.action_retry.enabled.from_value(True)
    app_container.configuration.action_retry.max_error.from_value(5)
    message_bus = app_container.message_bus()
    sim = DeployExecSimulator(app_container)

    await init_with_workflow_2_actions(message_bus)

    # we start from an empty db
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 0
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)

    # wait for the source to be deployed
    action = await asyncio.wait_for(
        sim.action_deployment_apply_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_source1"

    # Once the deployment is watched, k8s tells the runner that the deployment is successful,
    # like the deployment_service would do
    action = await asyncio.wait_for(
        sim.action_deployment_watch_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_source1"
    # Simulate worker, tell runner the deployment is ready
    await message_bus.handle([ActionDeploymentIsReadyEvent(action.id)])

    # Scheduler works here to create execution. And it calls start in simulator here

    # the execution can start! Wait for the runner to ask the execution_service
    source_execution: ExecutionConnection = await asyncio.wait_for(
        sim.execution_service_run_calls.get(), timeout=test_timeout
    )
    assert source_execution.workflow_action.definition.id == "action_def_source1"
    # send the init command, like the execution_service would do
    execution_result_id, workflow_run_id = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=source_execution.id,
            executor_id="executor_id1",
            log_delimiter="log_delimiter1",
        )
    )
    # Workflow the source connection is initialized, we send the workflow deployed successfully event
    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )

    # Ensure the studio got success message (everything after InitializeExecutionResultCommand has succeed)
    assert type(msg) is WorkflowDeployedSuccessfully

    # Here, there is one workflow runs, as the workflow run id is created with the first ExecutionResult
    # (which is created upon Initialization of the ExecutionConnection)
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)

    # send the result to the runner, like the execution_service would do
    await message_bus.handle_event(
        ExecutionResultFinishedEvent(
            id=execution_result_id,
            outputs={"output1": "value_of_output1"},
            output_files=[
                ExecutionResultFinishedEvent.OutputFile(
                    "output2", "/tmp/toto", "mime", "utf8", 123
                )
            ],
            status=ExecutionResultState.SUCCESS,
            end_time=datetime.datetime(
                year=2020, month=1, day=2, tzinfo=datetime.timezone.utc
            ),
        )
    )

    # Here the scheduler is called again, what Ryax do here:
    # - find the next action
    # - create the new connection
    # - scheduler create new deployment and allocate new execution connection to it.

    # now, the source has its result, the scheduler requires the next action to be deployed
    action = await asyncio.wait_for(
        sim.action_deployment_apply_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_processor2"
    action = await asyncio.wait_for(
        sim.action_deployment_watch_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_processor2"
    await message_bus.handle([ActionDeploymentIsReadyEvent(action.id)])

    # The next connection has been created, thus, we now have a workflow_run!
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    workflow_run_id = result[0]["id"]
    assert result[0]["state"] == "Running"
    assert result[0]["total_steps"] == 2
    assert result[0]["completed_steps"] == 1
    assert result[0]["workflow_definition_id"] == "workflow_definition_id1"
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)
    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    assert result["id"] == workflow_run_id
    assert len(result["runs"]) == 2
    assert result["runs"][0]["id"] is not None
    assert result["runs"][0]["state"] == "Success"
    assert result["runs"][0]["results"][0]["state"] == "Success"
    assert result["runs"][1]["id"] is not None
    assert result["runs"][1]["state"] == "Pending"
    assert len(result["runs"][1]["results"]) == 0
    WorkflowRunSchema().load(result, unknown=RAISE, partial=False)

    # the execution can start! Wait for the runner to ask the execution_service
    proc_execution: ExecutionConnection = await asyncio.wait_for(
        sim.execution_service_run_calls.get(), timeout=test_timeout
    )
    assert proc_execution.workflow_action.definition.id == "action_def_processor2"
    assert proc_execution.workflow_run_id == workflow_run_id

    assert (
        proc_execution.state == ExecutionConnectionState.STARTED
    )  # We set this before, retry
    # send the init command, like the execution_service would do
    (
        second_execution_result_id,
        second_workflow_run_id,
    ) = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=proc_execution.id,
            executor_id="executor_id2",
            log_delimiter="log_delimiter2",
        )
    )
    assert second_workflow_run_id == workflow_run_id
    # once the execution is finished, send the result to the runner, like the execution_service would do
    await message_bus.handle_event(
        ExecutionConnectionEndedEvent(
            execution_connection_id=proc_execution.id,
            status=ExecutionConnectionState.ERROR,
            error_message="Error to be retry",
        )
    )

    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    assert result["id"] == workflow_run_id
    assert len(result["runs"]) == 2
    assert result["runs"][0]["id"] is not None
    # retry logic is here...

    # The action deployment above should be reused, No create a new one
    assert sim.action_deployment_apply_calls.empty()

    proc_execution: ExecutionConnection = await asyncio.wait_for(
        sim.execution_service_run_calls.get(), timeout=test_timeout
    )

    assert proc_execution.state == ExecutionConnectionState.STARTED  # retried
    assert proc_execution.workflow_action.definition.id == "action_def_processor2"
    assert proc_execution.workflow_run_id == workflow_run_id

    (
        retried_second_execution_result_id,
        retried_second_workflow_run_id,
    ) = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=proc_execution.id,
            executor_id="executor_id2",
            log_delimiter="log_delimiter2",
        )
    )
    assert retried_second_workflow_run_id == workflow_run_id

    await message_bus.handle_event(
        ExecutionResultFinishedEvent(
            id=retried_second_execution_result_id,
            outputs={},
            status=ExecutionResultState.SUCCESS,
            end_time=datetime.datetime(
                year=2020, month=1, day=4, tzinfo=datetime.timezone.utc
            ),
            output_files=[],
        )
    )

    await message_bus.handle_event(
        ExecutionConnectionEndedEvent(
            proc_execution.id,
            status=ExecutionConnectionState.COMPLETED,
        )
    )

    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    assert result[0]["id"] == workflow_run_id
    assert result[0]["state"] == "Completed"
    assert result[0]["total_steps"] == 2
    assert result[0]["completed_steps"] == 2
    assert result[0]["workflow_definition_id"] == "workflow_definition_id1"
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)
    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    assert result["id"] == workflow_run_id
    assert result["state"] == "Completed"
    assert len(result["runs"]) == 2
    assert result["runs"][0]["id"] is not None
    assert result["runs"][0]["state"] == "Success"
    assert result["runs"][1]["id"] is not None
    assert result["runs"][1]["state"] == "Success"

    WorkflowRunSchema().load(result, unknown=RAISE, partial=False)

    # Now let's stop the workflow
    assert (
        await message_bus.handle_command(
            StopWorkflowDeploymentFromDefinitionIdCommand(
                "THIS_WORKFLOW_DOES_NOT_EXIST!"
            )
        )
        is False
    )
    assert (
        await message_bus.handle_command(
            StopWorkflowDeploymentFromDefinitionIdCommand("workflow_definition_id1")
        )
        is True
    )

    # IExecutionService.stop(execution) is called, which kills the IExecutionService.run coroutine
    # the deployment is deleted
    action1, action_definition_id1 = await asyncio.wait_for(
        sim.action_deployment_delete_calls.get(), timeout=test_timeout
    )
    action2, action_definition_id2 = await asyncio.wait_for(
        sim.action_deployment_delete_calls.get(), timeout=test_timeout
    )
    assert "action_def_source1" in [action_definition_id1, action_definition_id2]
    assert "action_def_processor2" in [action_definition_id1, action_definition_id2]

    # The message is published even for a non-exiting workflow for consistency with studio
    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowUndeployedSuccessfully
    assert msg.workflow_definition_id == "THIS_WORKFLOW_DOES_NOT_EXIST!"

    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowUndeployedSuccessfully
    assert msg.workflow_definition_id == "workflow_definition_id1"
    await message_bus.stop()


async def test_workflow_auto_undeploy_when_trigger_stopped(
    database_session: Session,
    app_container: ApplicationContainer,
    project_authorization_service_mock: Mock,
    authentication_service_mock: Mock,
    api_client: TestClient,
    database_engine,
) -> None:
    app_container.database_engine.override(database_engine)
    app_container.configuration.set("keep_event_history", True)
    message_bus = app_container.message_bus()
    sim = DeployExecSimulator(app_container)

    await init_with_workflow_2_actions(message_bus)

    # we start from an empty db
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 0
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)

    # wait for the source to be deployed
    action = await asyncio.wait_for(
        sim.action_deployment_apply_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_source1"

    # Once the deployment is watched, k8s tells the runner that the deployment is successful,
    # like the deployment_service would do
    action = await asyncio.wait_for(
        sim.action_deployment_watch_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_source1"
    await asyncio.create_task(
        message_bus.handle([ActionDeploymentIsReadyEvent(action.id)])
    )

    # the execution can start! Wait for the runner to ask the execution_service
    source_execution: ExecutionConnection = await asyncio.wait_for(
        sim.execution_service_run_calls.get(), timeout=test_timeout
    )
    assert source_execution.workflow_action.definition.id == "action_def_source1"
    # send the init command, like the execution_service would do
    execution_result_id, workflow_run_id = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=source_execution.id,
            executor_id="executor_id1",
            log_delimiter="log_delimiter1",
        )
    )
    # Workflow the source connection is initialized, we send the workflow deployed successfully event
    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowDeployedSuccessfully

    # Here, there is one workflow runs, as the workflow run id is created with the first ExecutionResult
    # (which is created upon Initialization of the ExecutionConnection)
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)

    # send the result to the runner, like the execution_service would do
    await message_bus.handle_event(
        ExecutionResultFinishedEvent(
            id=execution_result_id,
            outputs={"output1": "value_of_output1"},
            output_files=[
                ExecutionResultFinishedEvent.OutputFile(
                    "output2", "/tmp/toto", "mime", "utf8", 123
                )
            ],
            status=ExecutionResultState.SUCCESS,
            end_time=datetime.datetime(
                year=2020, month=1, day=2, tzinfo=datetime.timezone.utc
            ),
        )
    )
    # End the ExecutionConnection that runs the source: The workflow will be undeployed gracefully
    await message_bus.handle_event(
        ExecutionConnectionEndedEvent(
            execution_connection_id=source_execution.id,
            status=ExecutionConnectionState.COMPLETED,
        )
    )

    # now, the source has its result, the scheduler requires the next action to be deployed
    action = await asyncio.wait_for(
        sim.action_deployment_apply_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_processor2"
    action = await asyncio.wait_for(
        sim.action_deployment_watch_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_processor2"
    await message_bus.handle([ActionDeploymentIsReadyEvent(action.id)])

    # The next connection has been created, thus, we now have a workflow_run!
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    workflow_run_id = result[0]["id"]
    assert result[0]["state"] == "Running"
    assert result[0]["total_steps"] == 2
    assert result[0]["completed_steps"] == 1
    assert result[0]["workflow_definition_id"] == "workflow_definition_id1"
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)
    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    assert response.status == 200
    result = await response.json()
    assert result["id"] == workflow_run_id
    assert len(result["runs"]) == 2
    assert result["runs"][0]["id"] is not None
    assert result["runs"][0]["state"] == "Success"
    assert result["runs"][1]["id"] is not None
    assert result["runs"][1]["state"] == "Pending"
    WorkflowRunSchema().load(result, unknown=RAISE, partial=False)

    # the execution can start! Wait for the runner to ask the execution_service
    proc_execution: ExecutionConnection = await asyncio.wait_for(
        sim.execution_service_run_calls.get(), timeout=test_timeout
    )
    assert proc_execution.workflow_action.definition.id == "action_def_processor2"
    assert proc_execution.workflow_run_id == workflow_run_id

    # send the init command, like the execution_service would do
    second_execution_result_id, workflow_run_id = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=proc_execution.id,
            executor_id="executor_id2",
            log_delimiter="log_delimiter2",
        )
    )
    # once the execution is finished, send the result to the runner, like the execution_service would do
    await message_bus.handle_event(
        ExecutionResultFinishedEvent(
            id=second_execution_result_id,
            outputs={},
            status=ExecutionResultState.SUCCESS,
            end_time=datetime.datetime(
                year=2020, month=1, day=4, tzinfo=datetime.timezone.utc
            ),
            output_files=[],
        )
    )
    # Now finish the execution
    await message_bus.handle_event(
        ExecutionConnectionEndedEvent(
            execution_connection_id=proc_execution.id,
            status=ExecutionConnectionState.COMPLETED,
        )
    )

    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    assert result[0]["id"] == workflow_run_id
    assert result[0]["state"] == "Completed"
    assert result[0]["total_steps"] == 2
    assert result[0]["completed_steps"] == 2
    assert result[0]["workflow_definition_id"] == "workflow_definition_id1"
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)
    response = await api_client.get(f"/workflow_runs/{workflow_run_id}")
    result = await response.json()
    assert response.status == 200
    result = await response.json()
    assert result["id"] == workflow_run_id
    assert result["state"] == "Completed"
    assert len(result["runs"]) == 2
    assert result["runs"][0]["id"] is not None
    assert result["runs"][0]["state"] == "Success"
    assert result["runs"][1]["id"] is not None
    assert result["runs"][1]["state"] == "Success"

    WorkflowRunSchema().load(result, unknown=RAISE, partial=False)

    # IExecutionService.stop(execution) is called, which kills the IExecutionService.run coroutine
    # the deployment is deleted
    action, action_definition_id = await asyncio.wait_for(
        sim.action_deployment_delete_calls.get(), timeout=test_timeout
    )
    assert action_definition_id == "action_def_source1"

    # This should be sent without any trigger by the user because of graceful stop when the trigger stopped
    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowUndeployedSuccessfully
    assert msg.workflow_definition_id == "workflow_definition_id1"
    await message_bus.stop()


async def test_stop_workflow_without_running_it(
    database_session: Session,
    app_container: ApplicationContainer,
    project_authorization_service_mock: Mock,
    authentication_service_mock: Mock,
    api_client: TestClient,
) -> None:
    message_bus = app_container.message_bus()
    sim = DeployExecSimulator(app_container)

    await init_with_workflow_2_actions(message_bus)

    # wait for the source to be deployed
    action = await asyncio.wait_for(
        sim.action_deployment_apply_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_source1"

    # Once the deployment is watched, k8s tells the runner that the deployment is successful,
    # like the deployment_service would do
    action = await asyncio.wait_for(
        sim.action_deployment_watch_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_source1"
    await asyncio.create_task(
        message_bus.handle([ActionDeploymentIsReadyEvent(action.id)])
    )

    # the execution can start! Wait for the runner to ask the execution_service
    source_execution: ExecutionConnection = await asyncio.wait_for(
        sim.execution_service_run_calls.get(), timeout=test_timeout
    )
    assert source_execution.workflow_action.definition.id == "action_def_source1"
    # send the init command, like the execution_service would do
    await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=source_execution.id,
            executor_id="executor_id1",
            log_delimiter="log_delimiter1",
        )
    )

    # Workflow the source connection is initialized, we send the workflow deployed successfully event
    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowDeployedSuccessfully

    # Now let's stop the workflow
    assert (
        await message_bus.handle_command(
            StopWorkflowDeploymentFromDefinitionIdCommand(
                "THIS_WORKFLOW_DOES_NOT_EXIST!"
            )
        )
        is False
    )
    assert (
        await message_bus.handle_command(
            StopWorkflowDeploymentFromDefinitionIdCommand("workflow_definition_id1")
        )
        is True
    )

    # IExecutionService.stop(execution) is called, which kills the IExecutionService.run coroutine
    # Then, the deployment is deleted
    action, action_definition_id = await asyncio.wait_for(
        sim.action_deployment_delete_calls.get(), timeout=test_timeout
    )
    assert action_definition_id == "action_def_source1"

    # Still send message when nothing to undeploy
    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowUndeployedSuccessfully
    assert msg.workflow_definition_id == "THIS_WORKFLOW_DOES_NOT_EXIST!"

    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowUndeployedSuccessfully
    assert msg.workflow_definition_id == "workflow_definition_id1"
    await message_bus.stop()


async def test_timer_unschedule_idle_actions(
    database_session: Session,
    app_container: ApplicationContainer,
    project_authorization_service_mock: Mock,
    authentication_service_mock: Mock,
    api_client: TestClient,
    database_engine,
) -> None:
    app_container.database_engine.override(database_engine)
    message_bus = app_container.message_bus()
    sim = DeployExecSimulator(app_container)

    await init_with_workflow_2_actions(message_bus)

    # we start from an empty db
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 0
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)

    # wait for the source to be deployed
    action = await asyncio.wait_for(
        sim.action_deployment_apply_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_source1"

    # Once the deployment is watched, k8s tells the runner that the deployment is successful,
    # like the deployment_service would do
    action = await asyncio.wait_for(
        sim.action_deployment_watch_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_source1"
    await asyncio.create_task(
        message_bus.handle([ActionDeploymentIsReadyEvent(action.id)])
    )

    # the execution can start! Wait for the runner to ask the execution_service
    source_execution: ExecutionConnection = await asyncio.wait_for(
        sim.execution_service_run_calls.get(), timeout=test_timeout
    )
    assert source_execution.workflow_action.definition.id == "action_def_source1"
    # send the init command, like the execution_service would do
    execution_result_id, workflow_run_id = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=source_execution.id,
            executor_id="executor_id1",
            log_delimiter="log_delimiter1",
        )
    )
    # Workflow the source connection is initialized, we send the workflow deployed successfully event
    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowDeployedSuccessfully

    # Here, there is one workflow runs, as the workflow run id is created with the first ExecutionResult
    # (which is created upon Initialization of the ExecutionConnection)
    response = await api_client.get("/workflow_runs")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    WorkflowRunSchemaMinimal(many=True).load(result, unknown=RAISE, partial=False)

    # send the result to the runner, like the execution_service would do
    await message_bus.handle_event(
        ExecutionResultFinishedEvent(
            id=execution_result_id,
            outputs={"output1": "value_of_output1"},
            output_files=[
                ExecutionResultFinishedEvent.OutputFile(
                    "output2", "/tmp/toto", "mime", "utf8", 123
                )
            ],
            status=ExecutionResultState.SUCCESS,
            end_time=datetime.datetime(
                year=2020, month=1, day=2, tzinfo=datetime.timezone.utc
            ),
        )
    )

    # We keep the source running

    # Test _timer_unschedule_idle_actions when a source is running
    await app_container.scheduler()._timer_unschedule_idle_actions()

    # now, the source has its result, the scheduler requires the next action to be deployed
    action = await asyncio.wait_for(
        sim.action_deployment_apply_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_processor2"
    action = await asyncio.wait_for(
        sim.action_deployment_watch_calls.get(), timeout=test_timeout
    )
    assert action.action_definition.id == "action_def_processor2"
    await message_bus.handle([ActionDeploymentIsReadyEvent(action.id)])

    # the execution can start! Wait for the runner to ask the execution_service
    proc_execution: ExecutionConnection = await asyncio.wait_for(
        sim.execution_service_run_calls.get(), timeout=test_timeout
    )
    assert proc_execution.workflow_action.definition.id == "action_def_processor2"
    # send the init command, like the execution_service would do
    second_execution_result_id, workflow_run_id = await message_bus.handle_command(
        InitializeExecutionResultCommand(
            execution_connection_id=proc_execution.id,
            executor_id="executor_id2",
            log_delimiter="log_delimiter2",
        )
    )

    # Test _timer_unschedule_idle_actions when a trigger and an action is running
    await app_container.scheduler()._timer_unschedule_idle_actions()

    # once the execution is finished, send the result to the runner, like the execution_service would do
    await message_bus.handle_event(
        ExecutionResultFinishedEvent(
            id=second_execution_result_id,
            outputs={},
            status=ExecutionResultState.SUCCESS,
            end_time=datetime.datetime(
                year=2020, month=1, day=4, tzinfo=datetime.timezone.utc
            ),
            output_files=[],
        )
    )
    # The Connection with the action ends, so the ExecutionService sends the following message:
    await message_bus.handle_event(
        ExecutionConnectionEndedEvent(
            execution_connection_id=proc_execution.id,
            status=ExecutionConnectionState.COMPLETED,
        )
    )

    # Test _timer_unschedule_idle_actions when an action has to be undeployed
    await app_container.scheduler()._timer_unschedule_idle_actions()

    action_undeployed, action_deployed_definition_id = await asyncio.wait_for(
        sim.action_deployment_delete_calls.get(), timeout=test_timeout
    )
    # The action that does nothing have been undeployed:
    assert action_undeployed.id == action.id

    # Now let's stop the workflow
    assert (
        await message_bus.handle_command(
            StopWorkflowDeploymentFromDefinitionIdCommand("workflow_definition_id1")
        )
        is True
    )

    # IExecutionService.stop(execution) is called, which kills the IExecutionService.run coroutine
    # the deployment is deleted
    action, action_definition_id = await asyncio.wait_for(
        sim.action_deployment_delete_calls.get(), timeout=test_timeout
    )
    assert action_definition_id == "action_def_source1"

    msg = await asyncio.wait_for(
        sim.messaging_publisher.message_sent.get(), timeout=test_timeout
    )
    assert type(msg) is WorkflowUndeployedSuccessfully
    assert msg.workflow_definition_id == "workflow_definition_id1"
    await message_bus.stop()
