import asyncio
import json

import aiohttp
import pytest

from ryax.runner.application.action_logs_service import (
    START_LOG_DELIMITER_PREFIX,
    END_LOG_DELIMITER_PREFIX,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentNewLogLinesEvent,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.common.infrastructure.database.engine import Session
from tests.helper import now


def initialize(
    database_session: Session, kind: ActionKind = ActionKind.PROCESSOR
) -> str:
    project_id = "project_id"
    delimiter = "DELIMITER"
    workflow_deployment_id = "wf_deployment_id"
    workflow_definition_id = "wf_def_id"
    execution_connection_id = "exec_id"
    action_definition = ActionDefinition(
        id="action_id",
        version="action_version",
        technical_name="other",
        human_name="action human_name",
        description="descr",
        kind=kind,
        container_image="test",
        project_id=project_id,
        has_dynamic_outputs=True,
        inputs=[],
        outputs=[],
    )
    action_deployment = ActionDeployment(
        id="deployment_id",
        project_id=project_id,
        action_definition=action_definition,
        state=ActionDeploymentState.RUNNING,
        execution_type=ActionExecutionType.GRPC_V1,
        allocated_connection_id=execution_connection_id,
        node_pool=None,
    )
    execution_connection = ExecutionConnection(
        id=execution_connection_id,
        action_deployment=action_deployment,
        workflow_run_id=None,
        project_id=project_id,
        submitted_at=now,
        workflow_action=None,
        state=ExecutionConnectionState.RUNNING,
        time_allotment=123,
        end_of_log_delimiter_queue=[delimiter],
    )
    workflow_action = WorkflowAction(
        id="wfmod",
        id_in_studio="name",
        workflow_deployment_id=workflow_deployment_id,
        workflow_definition_id=workflow_definition_id,
        definition=action_definition,
        static_inputs=[],
        reference_inputs=[],
        next_actions=[],
        is_root_action=kind == ActionKind.TRIGGER,
        connection_execution_id=execution_connection.id,
        position=None,
        constraints=None,
        objectives=None,
    )
    workflow_deployment = WorkflowDeployment(
        id=workflow_deployment_id,
        workflow_definition_id=workflow_definition_id,
        project_id=project_id,
        creation_time=now,
        name="test",
        actions=[workflow_action],
    )
    execution_result = ExecutionResult(
        id="execresid",
        project_id=project_id,
        execution_connection_id=execution_connection.id,
        end_of_log_delimiter=delimiter,
        workflow_run_id="wfrun",
        workflow_action=None,
        submitted_at=now,
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.SUCCESS,
        outputs={},
        inputs={},
        parents=[],
    )
    database_session.add(workflow_deployment)
    database_session.add(execution_connection)
    database_session.add(execution_result)
    database_session.commit()
    return execution_result.id, action_deployment.id, delimiter, workflow_definition_id


# TODO also test with ExecutionResultNewLogLinesToBeAddedEvent
async def test_get_execution_result_logs(
    database_session: Session,
    project_authorization_service_mock,
    authentication_service_mock,
    app_container,
    api_client,
) -> None:
    actual_logs = "LOGS MTrhFKA\nYeah!\n"

    message_bus = app_container.message_bus()

    # Initialize state
    execution_result_id, action_deployment_id, delimiter, _ = initialize(
        database_session
    )
    log_lines = START_LOG_DELIMITER_PREFIX + delimiter + "\n"
    log_lines += actual_logs
    log_lines += END_LOG_DELIMITER_PREFIX + delimiter + "\n"

    await message_bus.handle_event(
        ActionDeploymentNewLogLinesEvent(action_deployment_id, log_lines, "executor_id")
    )
    await asyncio.sleep(1)
    response = await api_client.get(f"/workflow_runs/{execution_result_id}/logs")
    assert response.status == 200
    result = await response.json()
    assert result["log"] == actual_logs


@pytest.mark.skip("To fix: Unable to get ActionDeployment from the query...")
async def test_get_trigger_logs(
    database_session: Session,
    project_authorization_service_mock,
    authentication_service_mock,
    app_container,
    api_client,
) -> None:
    initial_log_lines = "LOGS MTrhFKA\nYeah!\n"

    message_bus = app_container.message_bus()

    # Initialize state
    (
        execution_result_id,
        action_deployment_id,
        delimiter,
        workflow_definition_id,
    ) = initialize(database_session, ActionKind.TRIGGER)

    await message_bus.handle_event(
        ActionDeploymentNewLogLinesEvent(
            action_deployment_id, initial_log_lines, "executor_id"
        )
    )
    await asyncio.sleep(1)
    async with api_client.ws_connect(f"/ws/workflows/{workflow_definition_id}") as ws:
        asyncio.ensure_future(ws.send_str('{"logs": "-1"}'))
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = json.loads(msg.data)["logs"]
        assert message == initial_log_lines

        # Create new instant logs
        new_log_lines = "line three\nline four\n"
        await message_bus.handle_event(
            ActionDeploymentNewLogLinesEvent(
                log_lines=new_log_lines,
                executor_id="toto",
                action_deployment_id=action_deployment_id,
            )
        )
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = json.loads(msg.data)["logs"]
        assert message == new_log_lines

        # Create new instant logs again
        last_log_lines = "line five\nline six\n"
        await message_bus.handle_event(
            ActionDeploymentNewLogLinesEvent(
                log_lines=last_log_lines,
                executor_id="toto",
                action_deployment_id=action_deployment_id,
            )
        )
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = json.loads(msg.data)["logs"]
        assert message == last_log_lines

        await ws.close()
    assert message is not None, "message should not be empty, connection error"
