import datetime

from marshmallow import RAISE

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionDynamicOutput,
    ActionInput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.infrastructure.api.schemas.portal_schema import PortalSchema
from ryax.common.infrastructure.database.engine import Session


async def test_get_portal(
    database_session: Session,
    project_authorization_service_mock,
    authentication_service_mock,
    api_client,
) -> None:
    """
    Test if outputs that are internal are not shown
    """
    action_definition = ActionDefinition(
        id="action_id",
        version="action_version",
        technical_name="action technical name",
        human_name="action human_name",
        description="descr",
        kind=ActionKind.PUBLISHER,
        container_image="test",
        project_id="",
        has_dynamic_outputs=True,
        inputs=[
            ActionInput(
                id="io0",
                name="io0name",
                type=ActionIOType.FLOAT,
                display_name="io0",
                help="help",
                enum_values=[],
            )
        ],
        outputs=[
            ActionOutput(
                id="io0",
                name="io0name",
                type=ActionIOType.STRING,
                display_name="io0",
                help="help",
                enum_values=[],
            ),
            ActionOutput(
                id="ryax_io1",
                name="ryaxio0name",
                type=ActionIOType.FLOAT,
                display_name="io1",
                help="help",
                enum_values=[],
            ),
        ],
    )
    mod2 = WorkflowAction(
        id="action2",
        id_in_studio="action2name",
        workflow_deployment_id="workflow_deployment_id1",
        workflow_definition_id="workflow_def_id1",
        static_inputs=[],
        reference_inputs=[],
        is_root_action=False,
        definition=action_definition,
        constraints=None,
        objectives=None,
    )
    mod1 = WorkflowAction(
        id="action1",
        id_in_studio="action1name",
        workflow_deployment_id="workflow_deployment_id1",
        workflow_definition_id="workflow_def_id1",
        static_inputs=[],
        reference_inputs=[],
        is_root_action=True,
        next_actions=[mod2],
        definition=action_definition,
        dynamic_outputs=[
            ActionDynamicOutput(
                id="io_dyn",
                name="dynamicoutputname",
                type=ActionIOType.FLOAT,
                display_name="io_dyn",
                help="help",
                enum_values=[],
            )
        ],
        constraints=None,
        objectives=None,
    )
    database_session.add(
        WorkflowDeployment(
            id="workflow_id1",
            name="workflow_name1",
            project_id="project_id",
            creation_time=str(datetime.datetime.now()),
            workflow_definition_id="workflow_def_id1",
            state=WorkflowDeploymentState.RUNNING,
            actions=[mod1, mod2],
        )
    )
    database_session.commit()

    response = await api_client.get("/portals/workflow_def_id1")
    assert response.status == 200
    result = await response.json()
    PortalSchema().load(result, unknown=RAISE, partial=False)

    assert result["workflow_deployment_id"] == "workflow_id1"
    assert result["workflow_definition_id"] == "workflow_def_id1"
    assert result["name"] == "workflow_name1"
    assert len(result["outputs"]) == 3
    assert result["outputs"][0]["id"] == "dynamicoutputname"
    assert result["outputs"][0]["type"] == ActionIOType.FLOAT.name
    assert result["outputs"][1]["id"] == "io0name"
    assert result["outputs"][1]["type"] == ActionIOType.STRING.name
    assert result["outputs"][2]["id"] == "ryaxio0name"
    assert result["outputs"][2]["type"] == ActionIOType.FLOAT.name


async def test_get_portal_non_existing_workflow(
    database_session: Session,
    project_authorization_service_mock,
    authentication_service_mock,
    api_client,
):
    response = await api_client.get("/portals/workflow_def_id1")
    assert response.status == 404
