import asyncio
import datetime
from unittest.mock import AsyncMock

from dependency_injector import providers

from ryax.common.domain.internal_messaging.querier import IQuerierService
from ryax.common.domain.registration.registration_values import SiteType
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionDynamicOutput,
    ActionInput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.common.domain.internal_messaging.base_messages import ApplicationStartupEvent
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.runner.domain.workflow_action.workflow_action import (
    WorkflowAction,
    SchedulingConstraints,
    SchedulingObjectives,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.infrastructure.action_deployment.dummy.dummy_action_deployment_service import (
    DummyActionDeploymentService,
)
from ryax.runner.infrastructure.action_execution.dummy.dummy_execution_service import (
    DUMMYExecutionService,
)
from ryax.common.infrastructure.database.engine import Session


async def test_startup_events_finishes(
    database_session: Session,
    api_client,
    app_container,
    message_publisher_mock,
) -> None:
    """
    Test if pending runs are launched at startup
    """

    action_trigger_definition = ActionDefinition(
        id="trigger_id",
        version="action_version",
        technical_name="test",
        human_name="action human_name",
        description="descr",
        kind=ActionKind.TRIGGER,
        container_image="test",
        project_id="project_id",
        has_dynamic_outputs=True,
        inputs=[
            ActionInput(
                id="action_input_1",
                name="input1name",
                type=ActionIOType.INTEGER,
                display_name="io0",
                help="help",
                enum_values=[],
            )
        ],
        addons_inputs=[
            ActionInput(
                id="action_addon_input_1",
                name="addon1name",
                type=ActionIOType.STRING,
                display_name="ioAddons",
                help="help",
                enum_values=[],
            )
        ],
        outputs=[
            ActionOutput(
                id="action_output_1",
                name="output1name",
                type=ActionIOType.STRING,
                display_name="io0",
                help="help",
                enum_values=[],
            ),
            ActionOutput(
                id="action_output_2",
                name="output2name",
                type=ActionIOType.FLOAT,
                display_name="io1",
                help="help",
                enum_values=[],
            ),
        ],
    )
    action_definition = ActionDefinition(
        id="action_id",
        version="action_version",
        # To test DUMMY execution type
        # technical_name="postgw",
        technical_name="other",
        human_name="action human_name",
        description="descr",
        kind=ActionKind.PUBLISHER,
        container_image="test",
        project_id="project_id",
        has_dynamic_outputs=True,
        inputs=[
            ActionInput(
                id="action_input_2",
                name="input2name",
                type=ActionIOType.FLOAT,
                display_name="io0",
                help="help",
                enum_values=[],
            )
        ],
        outputs=[
            ActionOutput(
                id="action_output_3",
                name="output3name",
                type=ActionIOType.STRING,
                display_name="io0",
                help="help",
                enum_values=[],
            ),
            ActionOutput(
                id="action_output_4",
                name="output4name",
                type=ActionIOType.FLOAT,
                display_name="io1",
                help="help",
                enum_values=[],
            ),
        ],
    )
    mod2 = WorkflowAction(
        id="action2",
        id_in_studio="action2name",
        workflow_deployment_id="workflow_deployment_id1",
        workflow_definition_id="workflow_def_id1",
        static_inputs=[],
        reference_inputs=[],
        is_root_action=False,
        definition=action_definition,
        constraints=None,
        objectives=None,
    )
    mod1 = WorkflowAction(
        id="action1",
        id_in_studio="action1name",
        workflow_deployment_id="workflow_deployment_id1",
        workflow_definition_id="workflow_def_id1",
        static_inputs=[],
        reference_inputs=[],
        is_root_action=True,
        next_actions=[mod2],
        definition=action_trigger_definition,
        dynamic_outputs=[
            ActionDynamicOutput(
                id="dynamic_output_1",
                name="dynamicoutput1",
                type=ActionIOType.FLOAT,
                display_name="io_dyn",
                help="help",
                enum_values=[],
            )
        ],
        constraints=SchedulingConstraints(
            arch_list=[],
            site_list=[],
            site_type_list=[],
            node_pool_list=[],
        ),
        objectives=SchedulingObjectives(cost=0.1, energy=0.2, performance=0.3),
    )
    site = Site(
        id=1,
        name="MySite",
        type=SiteType.KUBERNETES,
    )
    node_pool = NodePool(id="1", name="1", memory=123, cpu=2, site=site)
    site.node_pools = [node_pool]
    database_session.add(site)
    action_deployment = ActionDeployment(
        id="action_deployment_id",
        project_id="project_id",
        action_definition=action_definition,
        state=ActionDeploymentState.RUNNING,
        execution_type=ActionExecutionType.GRPC_V1,
        node_pool=node_pool,
    )
    database_session.add(action_deployment)
    trigger_deployment = ActionDeployment(
        id="action_trigger_deployment_id",
        project_id="project_id",
        action_definition=action_trigger_definition,
        state=ActionDeploymentState.RUNNING,
        execution_type=ActionExecutionType.GRPC_V1,
        node_pool=node_pool,
    )
    database_session.add(trigger_deployment)
    execution_connection = ExecutionConnection(
        id="trigger_exec_id",
        action_deployment=trigger_deployment,
        workflow_run_id=None,
        project_id="project_id",
        submitted_at=datetime.datetime.now(datetime.timezone.utc),
        workflow_action=mod1,
        state=ExecutionConnectionState.PENDING,
        time_allotment=123,
    )
    database_session.add(execution_connection)
    database_session.add(
        WorkflowDeployment(
            id="workflow_id1",
            name="workflow_name1",
            project_id="project_id",
            creation_time=str(datetime.datetime.now(datetime.timezone.utc)),
            workflow_definition_id="workflow_def_id1",
            state=WorkflowDeploymentState.RUNNING,
            actions=[mod1, mod2],
        )
    )
    database_session.commit()
    app_container.execution_service_factory.factories["GRPC_V1"].override(
        providers.Factory(
            DUMMYExecutionService,
            message_bus=app_container.message_bus,
        )
    )
    app_container.action_deployment_factory.factories["k8s"].override(
        providers.Factory(
            DummyActionDeploymentService,
            message_bus=app_container.message_bus,
        )
    )
    mock_querier = AsyncMock(IQuerierService)
    mock_querier.query.return_value = ExecutionConnectionState.PENDING
    app_container.message_querier.override(mock_querier)
    message_bus = app_container.message_bus()

    await message_bus.handle_event(ApplicationStartupEvent())

    response = await api_client.get("/healthz")
    assert response.status == 200

    await asyncio.sleep(1)

    response = await api_client.get("/healthz")
    assert response.status == 200

    assert all([not event.error for event in message_bus.events])
    assert isinstance(message_bus.events[0].event, ApplicationStartupEvent)

    del message_bus
