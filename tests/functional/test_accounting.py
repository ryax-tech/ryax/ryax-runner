import datetime

from ryax.common.domain.registration.registration_values import SiteType
from ryax.common.infrastructure.database.engine import Session
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionOutput,
    ActionResources,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionKind,
    ActionIOType,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_events import (
    ExecutionConnectionUpdatedEvent,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.site.site_entities import NodePool, Site
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from tests.helper import now, yesterday


async def test_accounting(
    database_session: Session,
    project_authorization_service_mock,
    authentication_service_mock,
    api_client,
    app_container,
) -> None:
    message_bus = app_container.message_bus()
    project_id = "project_id"
    workflow_run_id = "workflow_run"
    result_technical_name = "techname"
    workflow_action_id = "workflow_action"
    workflow_deployment_id = "workflow_deployment"
    execution_connection_id1 = "execution_connection_id1"
    execution_connection_id2 = "execution_connection_id2"
    action_definition = ActionDefinition(
        id="moddef",
        project_id=project_id,
        technical_name="name",
        version="2",
        kind=ActionKind.TRIGGER,
        container_image="cont",
        human_name="hname",
        description="desc",
        inputs=[],
        outputs=[
            ActionOutput(
                id="io1",
                name=result_technical_name,
                type=ActionIOType.STRING,
                display_name="io1",
                help="help",
                enum_values=[],
            ),
        ],
        resources=ActionResources(id="res1", cpu=16, memory=256),
    )
    site = Site(id="1", name="testSite", type=SiteType.KUBERNETES)
    node_pool = NodePool(id="1", name="noddPool1", site=site, cpu=1, memory=2000)
    site.node_pools = [node_pool]
    action_deployment = ActionDeployment(
        id="deployment_id",
        project_id=project_id,
        action_definition=action_definition,
        state=ActionDeploymentState.RUNNING,
        execution_type=ActionExecutionType.GRPC_V1,
        allocated_connection_id=execution_connection_id1,
        node_pool=node_pool,
    )
    workflow_action = WorkflowAction(
        id=workflow_action_id,
        id_in_studio="workflow_action",
        workflow_deployment_id=workflow_deployment_id,
        workflow_definition_id="workflow_definition",
        definition=action_definition,
        static_inputs=[],
        reference_inputs=[],
        constraints=None,
        objectives=None,
    )
    execution_connection1 = ExecutionConnection(
        id=execution_connection_id1,
        action_deployment=action_deployment,
        workflow_run_id=workflow_run_id,
        project_id=project_id,
        submitted_at=now,
        workflow_action=workflow_action,
        state=ExecutionConnectionState.RUNNING,
        time_allotment=123,
    )
    execution_connection2 = ExecutionConnection(
        id=execution_connection_id2,
        action_deployment=action_deployment,
        workflow_run_id=workflow_run_id,
        project_id=project_id,
        submitted_at=yesterday,
        workflow_action=workflow_action,
        state=ExecutionConnectionState.RUNNING,
        time_allotment=123,
    )

    database_session.add(execution_connection1)
    database_session.add(execution_connection2)
    database_session.commit()

    response = await api_client.get(
        f"/accounting?execution_ids={execution_connection_id1}"
    )
    assert response.status == 200
    result = await response.json()
    assert len(result) == 0

    await message_bus.handle_event(
        ExecutionConnectionUpdatedEvent(execution_connection_id1)
    )
    await message_bus.handle_event(
        ExecutionConnectionUpdatedEvent(execution_connection_id2)
    )
    response = await api_client.get(
        f"/accounting?execution_ids={execution_connection_id1}"
    )
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    result = result[0]
    assert result["project_id"] == execution_connection1.project_id
    assert result["workflow_run_id"] == execution_connection1.workflow_run_id
    assert (
        result["requested_cpu"]
        == execution_connection1.action_deployment.action_definition.resources.cpu
    )
    assert (
        result["requested_gpu"]
        == execution_connection1.action_deployment.action_definition.resources.gpu
    )

    response = await api_client.get(
        f"/accounting?execution_ids={execution_connection_id1},{execution_connection_id2}"
    )
    assert response.status == 200
    result = await response.json()
    assert len(result) == 2

    response = await api_client.get("/accounting")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 2

    response = await api_client.get(f"/accounting?project_ids={project_id}")
    assert response.status == 200
    result = await response.json()
    assert len(result) == 2

    execution_connection2.ended_at = now
    database_session.commit()

    await message_bus.handle_event(
        ExecutionConnectionUpdatedEvent(execution_connection_id2)
    )
    response = await api_client.get(
        f"/accounting?execution_ids={execution_connection_id2}"
    )
    assert response.status == 200
    result = await response.json()
    assert len(result) == 1
    assert (
        datetime.datetime.fromisoformat(result[0]["ended_at"])
        == execution_connection2.ended_at
    )
