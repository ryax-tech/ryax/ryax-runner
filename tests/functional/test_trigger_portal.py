import datetime
from unittest import mock

import aiohttp
from aiohttp import test_utils
from marshmallow import RAISE

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionDynamicOutput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    InitializeExecutionResultCommand,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.infrastructure.api.controllers.portal_controller import (
    WorkflowRunTriggerResponseSchema,
)
from ryax.common.infrastructure.database.engine import Session


async def test_trigger_portal(
    message_bus_mock: mock.Mock,
    database_session: Session,
    project_authorization_service_mock: mock.Mock,
    authentication_service_mock: mock.Mock,
    execution_result_repository_mock: mock.Mock,
    api_client: test_utils.TestClient,
) -> None:
    """
    In this test, we will test the pipeline from receiving the data from a request,
    to the sending of the internal events triggering the execution.
    """

    action_definition = ActionDefinition(
        id="action_id",
        version="action_version",
        technical_name="action technical name",
        human_name="action human_name",
        description="descr",
        kind=ActionKind.TRIGGER,
        container_image="test",
        project_id="project_id",
        inputs=[],
        outputs=[
            ActionOutput(
                id="io1FLOAT",
                name="iofloatname",
                type=ActionIOType.FLOAT,
                display_name="io1FLOAT",
                help="help",
                enum_values=[],
            ),
            ActionOutput(
                id="io2STRING",
                name="iostringname",
                type=ActionIOType.STRING,
                display_name="io2STRING",
                help="help",
                enum_values=[],
            ),
        ],
    )

    mod2 = WorkflowAction(
        id="action2",
        id_in_studio="action2name",
        workflow_deployment_id="workflow_deployment_id1",
        workflow_definition_id="workflow_def_id1",
        static_inputs=[],
        reference_inputs=[],
        is_root_action=False,
        definition=action_definition,
        constraints=None,
        objectives=None,
    )
    mod1 = WorkflowAction(
        id="action1",
        id_in_studio="action1name",
        workflow_deployment_id="workflow_deployment_id1",
        workflow_definition_id="workflow_def_id1",
        static_inputs=[],
        dynamic_outputs=[
            ActionDynamicOutput(
                id="io1FLOAT",
                name="dynamic_float_name",
                type=ActionIOType.FLOAT,
                display_name="io1FLOAT",
                help="help",
                enum_values=[],
            ),
            ActionDynamicOutput(
                id="io2STRING",
                name="dynamic_string_name",
                type=ActionIOType.STRING,
                display_name="io2STRING",
                help="help",
                enum_values=[],
            ),
        ],
        reference_inputs=[],
        is_root_action=True,
        next_actions=[mod2],
        definition=action_definition,
        connection_execution_id="trig_exec_1",
        constraints=None,
        objectives=None,
    )
    database_session.add(
        WorkflowDeployment(
            id="workflow_id1",
            name="workflow_name1",
            project_id="project_id",
            creation_time=str(datetime.datetime.now()),
            workflow_definition_id="workflow_def_id1",
            state=WorkflowDeploymentState.RUNNING,
            actions=[mod1, mod2],
        )
    )
    database_session.commit()
    message_bus_mock.handle_command = mock.AsyncMock(return_value=("", ""))

    with aiohttp.MultipartWriter("mixed") as mpwriter:
        x = mpwriter.append("54.5")
        x.set_content_disposition("attachement", name="dynamic_float_name")
        x = mpwriter.append("Hello World!")
        x.set_content_disposition("attachement", name="dynamic_string_name")

        response = await api_client.post(
            "/portals/workflow_def_id1", data=mpwriter, headers={"TEST": "foo"}
        )
    assert response.status == 201
    WorkflowRunTriggerResponseSchema().load(
        await response.json(), unknown=RAISE, partial=False
    )

    assert message_bus_mock.handle_command.call_args.kwargs == {}

    assert len(message_bus_mock.handle_command.call_args.args) == 1
    cmd = message_bus_mock.handle_command.call_args.args[0]
    assert type(cmd) is InitializeExecutionResultCommand
    print(cmd)
    assert cmd.execution_connection_id == "trig_exec_1"


async def test_trigger_portal_non_existing_workflow(
    database_session: Session,
    project_authorization_service_mock: mock.Mock,
    authentication_service_mock: mock.Mock,
    api_client: test_utils.TestClient,
) -> None:
    with aiohttp.MultipartWriter("mixed") as mpwriter:
        x = mpwriter.append("54.5")
        x.set_content_disposition("attachement", name="dynamic_float_name")
        x = mpwriter.append("Hello World!")
        x.set_content_disposition("attachement", name="dynamic_string_name")

        response = await api_client.post(
            "/portals/workflow_def_NONEXISTING", data=mpwriter
        )
    assert response.status == 404
