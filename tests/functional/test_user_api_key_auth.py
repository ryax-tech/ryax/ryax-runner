from unittest import mock

from aiohttp.test_utils import TestClient

from ryax.common.infrastructure.database.engine import Session


class TestUserApiKeyAuthLifecycle:
    async def setup_class_variables(
        self,
        database_session: Session,
        project_authorization_service_mock: mock.MagicMock,
        authentication_service_mock: mock.MagicMock,
        api_client: TestClient,
    ):
        self.database_session = database_session
        self.project_authorization_service_mock = project_authorization_service_mock
        self.authentication_service_mock = authentication_service_mock
        self.api_client = api_client

    async def _create_api_key_with_name(self, name: str) -> None:
        response = await self.api_client.post("/user-auth/api-key", data={"name": name})
        assert response.status == 201
        result = await response.json()
        return result

    async def _revoke_api_key_by_id(self, api_key_id: str) -> None:
        response = await self.api_client.post(f"/user-auth/api-key/{api_key_id}/revoke")
        assert response.status == 200

    async def _delete_api_key_by_id(self, api_key_id: str) -> None:
        response = await self.api_client.delete(f"user-auth/api-key/{api_key_id}")
        assert response.status == 200

    async def _toggle_api_key_auth_requirement(self) -> None:
        response = await self.api_client.put("/user-auth/api-key/toggle")
        assert response.status == 200

    async def _get_user_auth(self) -> dict:
        response = await self.api_client.get("/user-auth")
        assert response.status == 200
        return await response.json()

    async def test_user_api_key_auth_lifecycle(
        self,
        database_session: Session,
        project_authorization_service_mock: mock.MagicMock,
        authentication_service_mock: mock.MagicMock,
        api_client: TestClient,
    ):
        await self.setup_class_variables(
            database_session,
            project_authorization_service_mock,
            authentication_service_mock,
            api_client,
        )

        # Activate api key auth, create a key, revoke it, delete it
        auth = await self._get_user_auth()
        assert not auth["api_key"]["enabled"]

        await self._toggle_api_key_auth_requirement()

        auth = await self._get_user_auth()
        assert auth["api_key"]["enabled"]

        new_api_key = await self._create_api_key_with_name(name="Ryax api key")
        api_key_id = new_api_key["id"]
        await self._revoke_api_key_by_id(api_key_id)
        await self._delete_api_key_by_id(api_key_id)

        await self._toggle_api_key_auth_requirement()
        auth = await self._get_user_auth()
        assert not auth["api_key"]["enabled"]
