from unittest import mock

from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    IActionDeploymentRepository,
)
from ryax.runner.domain.action_logs.action_logs_repository import IActionLogsRepository
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_connection.execution_connection_repository import (
    IExecutionConnectionRepository,
)
from ryax.runner.domain.execution_result.execution_result_repository import (
    IExecutionResultRepository,
)
from ryax.runner.domain.stored_file.stored_file_reporitory import IStoredFileRepository
from ryax.runner.domain.user_auth.api_key.api_key_repository import (
    IUserApiKeyRepository,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_repository import (
    IWorkflowDeploymentRepository,
)
from ryax.runner.domain.workflow_run.workflow_run_repository import (
    IWorkflowRunRepository,
)
from ryax.runner.domain.site.site_repository import ISiteRepository
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_repository import (
    IRecommendationVPARepository,
)


class UnitOfWorkStub(IUnitOfWork):
    def __init__(self):
        self.action_deployment_repository: IActionDeploymentRepository = mock.MagicMock(
            IActionDeploymentRepository
        )
        self.action_definition_repository: IActionDefinitionRepository = mock.MagicMock(
            IActionDefinitionRepository
        )
        self.execution_connection_repository: IExecutionConnectionRepository = (
            mock.MagicMock(IExecutionConnectionRepository)
        )
        self.workflow_repository: IWorkflowDeploymentRepository = mock.MagicMock(
            IWorkflowDeploymentRepository
        )
        self.execution_result_repository: IExecutionResultRepository = mock.MagicMock(
            IExecutionResultRepository
        )
        self.stored_file_repository: IStoredFileRepository = mock.MagicMock(
            IStoredFileRepository
        )
        self.user_api_key_repository: IUserApiKeyRepository = mock.MagicMock(
            IUserApiKeyRepository
        )
        self.workflow_run_repository: IWorkflowRunRepository = mock.MagicMock(
            IWorkflowRunRepository
        )
        self.action_logs_repository: IActionLogsRepository = mock.MagicMock(
            IActionLogsRepository
        )
        self.site_repository: IWorkflowDeploymentRepository = mock.MagicMock(
            ISiteRepository
        )
        self.recommendation_vpa_repository: IRecommendationVPARepository = (
            mock.MagicMock(IRecommendationVPARepository)
        )
        self.commit = mock.MagicMock()

    def rollback(self) -> None:
        pass

    commit = mock.MagicMock()
