from ryax.worker.container import ApplicationContainer
from ryax.worker.app import _env_var_to_bool
import os


def test_from_env_for_boolean_vars():
    container = ApplicationContainer()

    # when not defined the environment variable it returns the default value which is already bool
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is False

    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = "FALSE"
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is False

    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = "NULL"
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is False

    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = ""
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is False

    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = "DISABLED"
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is False

    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = "DisAblED"
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is False

    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = "0"
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is False

    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = "TRUE"
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is True

    # anything besides "", "FALSE", "NULL", or "DISABLE" returns true
    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = "eNaBlE"
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is True

    # anything besides "", "FALSE", "NULL", or "DISABLE" returns true
    os.environ["RYAX_RESOURCE_RECOMMENDATION_ENABLED"] = "1"
    container.configuration.resource_recommendation_enabled.from_env(
        "RYAX_RESOURCE_RECOMMENDATION_ENABLED", default=False, as_=_env_var_to_bool
    )
    assert container.configuration.resource_recommendation_enabled() is True
