import os
import shutil
import tempfile
import zipfile
from pathlib import Path
from typing import List, Type, Callable, Any, cast
from unittest import mock

import pytest
from minio import Minio

from ryax.addons.ryax_hpc import HpcServiceAddOn
from ryax.common.domain.internal_messaging.base_messages import BaseEvent, BaseCommand
from ryax.common.domain.internal_messaging.message_bus import IMessageBus, Messages
from ryax.runner.domain.site.site_entities import NodePool
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.deployment.deployment_entities import Deployment, Kind
from ryax.worker.domain.deployment.deployment_values import ExecutionType
from ryax.worker.domain.execution.execution_entities import (
    Execution,
    IODefinition,
    IOType,
)
from ryax.worker.domain.execution.execution_events import (
    TaskNewLogLinesEvent,
    InitTaskCommand,
)
from ryax.worker.infrastructure.execution.ssh_slurmv1.ssh_slurm_v1_execution_service import (
    SSHV1ExecutionService,
)
from ryax.worker.infrastructure.ssh.ssh_connection_helper import SshConnectionHelper
from ryax.common.infrastructure.storage.s3.s3_engine import S3Engine

ryax_cache_dir = ".ryax_cache"


@pytest.fixture(scope="function")
def ssh_helper():
    return SshConnectionHelper(
        hostname="127.0.0.1",
        port=2222,
        username="root",
        password="root",
        private_key="LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KYjNCbGJuTnphQzFyWlhrdGRqRUFBQUFBQkc1dmJtVUFBQUFFYm05dVpRQUFBQUFBQUFBQkFBQUJsd0FBQUFkemMyZ3RjbgpOaEFBQUFBd0VBQVFBQUFZRUExL0h0TGltNFZMdWpGaHAyek02dnBIUmpsVmtwOUpqM2c5RlhrTVorVUcxblpsQmxja1h2CkNLRFpUTlU2WHhSd05HWFoyRFUwK1hPQWsvcXNqNHBERmE5U0ZKUjlFYnFkdjRuaUZnUEpZMU1FVi9hTnlzTlJqZE1YUHAKa01hOXdBQUl3UEFzQjhSVjltNG9mOFduRU0vT1lVSUxsS0gwWGM1YVh5ZFgyYUFmZzhQMGFYZUdsSWtqVFJyZmpXSmlxMQpVYnpDWVJiRUpFSnZrOHlPai9GaitoajNvSFV0b3lEVk04RkF0SXhGaDBNK1g3T1hPbzFRemp3VnhQUDZoN3JMdFVod3VoClpZV1k4amx3cGpHQUQ1L2I5ZFlpd3A5MmRndHNMbndWUGJCODdSeDJGTnFmMnVMOXR5emZoTEovcGZmb0dNRkVPdkxmYUMKQVpodkN0cmJ2VE1CalpRak00aVVjZWh0aVI0a0RhQU9ScnVjMDJ5VlN4R0JSZE5McWFCUmhBelRteTg3ZElFTG9KSWszZQpKTDhndTlsS2RBelBxcDFjeUg0Wnc0TzduWGFXTVptcGRKS0RzSm5ycXNtRk0rK1U0dm9UbWZGUzVDZmRBdmlnL0ZNMmFlCnp1L3Z1WVhuMXgyS2NmUFg0TE9MVzcrVnRDamxKOFdvZXExK0U5WnJBQUFGZ0Fkd3k4RUhjTXZCQUFBQUIzTnphQzF5YzIKRUFBQUdCQU5meDdTNHB1RlM3b3hZYWRzek9yNlIwWTVWWktmU1k5NFBSVjVER2ZsQnRaMlpRWlhKRjd3aWcyVXpWT2w4VQpjRFJsMmRnMU5QbHpnSlA2ckkrS1F4V3ZVaFNVZlJHNm5iK0o0aFlEeVdOVEJGZjJqY3JEVVkzVEZ6NlpER3ZjQUFDTUR3CkxBZkVWZlp1S0gvRnB4RFB6bUZDQzVTaDlGM09XbDhuVjltZ0g0UEQ5R2wzaHBTSkkwMGEzNDFpWXF0Vkc4d21FV3hDUkMKYjVQTWpvL3hZL29ZOTZCMUxhTWcxVFBCUUxTTVJZZERQbCt6bHpxTlVNNDhGY1R6K29lNnk3VkljTG9XV0ZtUEk1Y0tZeApnQStmMi9YV0lzS2ZkbllMYkM1OEZUMndmTzBjZGhUYW45cmkvYmNzMzRTeWY2WDM2QmpCUkRyeTMyZ2dHWWJ3cmEyNzB6CkFZMlVJek9JbEhIb2JZa2VKQTJnRGthN25OTnNsVXNSZ1VYVFM2bWdVWVFNMDVzdk8zU0JDNkNTSk4zaVMvSUx2WlNuUU0KejZxZFhNaCtHY09EdTUxMmxqR1pxWFNTZzdDWjY2ckpoVFB2bE9MNkU1bnhVdVFuM1FMNG9QeFRObW5zN3Y3N21GNTljZAppbkh6MStDemkxdS9sYlFvNVNmRnFIcXRmaFBXYXdBQUFBTUJBQUVBQUFHQUVKMEtORXo5WTNkMEdwVTZvcE1jTERqUUdJCk5kbVBYVHhRNXNBRFZsTG1iY1Y2RER6VURZSFAvUzkvaEhuMTBCTE9iUkZiTXlPaWJLczVmTGd0ZU9SYkxSUzhnSXZ6NXUKT0hneExxUm43LzVKRFVOb3NUckpvWEdtRGlqVUxkQWxKK1dlbWdXWkNmVkhBeEtYTlpZUSs3dnZ2SHpFMlJjVTRZNGMwegpFd2tnbzVDRjB6OWcyVGlid0tqYW5oUDlwMVRLNksvZEUwa2QyU20vcFcxWGdxeXVqMDFDRnZ1MmNoUC9RVU9FKzZZYWVvCnV0QWtTRnJxNXBMMDBjdzdjQW5NMGhGWFpiV01lcmNRR2FkK21LblZYT25aQ1FsWjRGSHZsQ3JQbmFlYTA5aHhZMkV4SHkKc0p5RVhBQVgxT2p5KzFUeWo1OG4xNEVDQllTRmhYc21ZNzZGV1Z2NkxsUzc1K3hiOWFmZ2RNWG93dTUvaEJ0VSt6bFRMVwpoSFVKOS9hOXZCaUZtOXo5dng2dEg1L3RXOFMwN2hYSmIzQjc1ZmtTaXFzelNLR1hpaWhaQ3h1cWpNZ0hFR1Z0K1EvcGRnCkc0Y2hRWmpuMmxFVXpOZFd3bGRjWWdRWDlPbk9ydmR4V1REKzltMklscVIzNitFUkR1ZlNlK0M1eFU2NGlUcmZXQkFBQUEKd0U1cU13d2crNVA2dGFzL2JFN0ExdHArZ2ViZkNTZFVnK1RXTGtyTCtCdURQYXNPellQbVJ6R1dpMWFOTGYrQVk3WmtmKwprelJMUWtmNktHbXdWaVlNUHdZekNWMUJXak42L0xCM2RkdlhJWHVNc3dOaHFOdmZWTFNSUDMzdDJ6SEs3QU42OGVyZ21CCm4wcFVvNk11Tkd0Zm90TXVTTU0rYjNXbUphM2FQeThsMHJiUVZDRjJEUjNiajduREQyR0pTS1luSm5kZjQrdzFWVmZKVVMKU2F4RGpXTkROUzBmSkUrQ2FsYVREOVlaT2c5RXV5Z2hZYjlETVhKdE1GeCtwbUxnQUFBTUVBOThKQlhZMzNJRGYzUVFSbQo5RHNQR2pWNVViYlFqV2FJT0pPVDZZR3pkWklsS3hMeHU5c2hrMUd5RTFYS3ZNSVNsZzJkbUk1aTRLT3YwMXorTWZMWVZUClFlNFdDTEZ3dGt3M1hSYzJGZ1pBbml0UFNLYTd4QXdYdHJidC9qS2JqMUg3cmEyZU5yVWN2bjNIQkdHZElaK1dBZ2NQRFcKU3lFOXh1ZlQxV2VTbkVpYk1kNU00VGsyTGtiTXVzTGVwdzNYN0srY3NtMlhKY3YyZkxTelhGUk5KTHF3L2J6ZkI3aTZ4ZApRQVpNb1VTQzFLWGZ0RVQ1OTl1TFprcnh0ZlhJVGhBQUFBd1FEZklNUkhEdXVFZ2pBT2pndVJIZzhhYWdINXl6WXhwMzFNCmFEUncvazJUNWhmMGEyQkhTUHpCSG1kT0E2MzlCeUJpNTIrVG1mUXVCcjNXRHNJTUZ1bVZuYk5rTEFoSnlpSDExQzhvQnMKV1pnNTFsVjdEeEhLS3UxU2tSWE9UWUFyRStJQ0NLdkNUUjlNd2RwTGJmbEVXN21xNEpISUVoQnBXYW9way9jVU1JakplSgpCbVFib3JwTjZJcU1pcmRuTlcvSzVBejJXaDRnUEJPaVlsSHdhejdOemlVTUhxS0QyWFN1M1ZvZjFhUkY2akxJejJpK2Z1Ckk4ckZUQ3FRSU5lTXNBQUFBSmRtVnNhRzlBWm05NEFRST0KLS0tLS1FTkQgT1BFTlNTSCBQUklWQVRFIEtFWS0tLS0tCg==",
        config=None,
    )


@pytest.fixture()
def app():
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@pytest.fixture()
def filestore_bucket():
    filestore_bucket = os.getenv("RYAX_FILESTORE_BUCKET")
    return filestore_bucket


@pytest.fixture(scope="function")
def minio_client():
    filestore_url = os.environ["RYAX_FILESTORE"]
    filestore_access = os.environ["RYAX_FILESTORE_ACCESS_KEY"]
    filestore_secret = os.environ["RYAX_FILESTORE_SECRET_KEY"]
    filestore_bucket = os.environ["RYAX_FILESTORE_BUCKET"]
    minio_client: Minio = Minio(
        filestore_url,
        access_key=filestore_access,
        secret_key=filestore_secret,
        secure=False,
    )
    if not minio_client.bucket_exists(filestore_bucket):
        minio_client.make_bucket(filestore_bucket)
    yield minio_client
    for item in minio_client.list_objects(filestore_bucket, recursive=True):
        minio_client.remove_object(filestore_bucket, item.object_name)
    minio_client.remove_bucket(filestore_bucket)


@pytest.fixture(scope="function")
def storage_service(minio_client, filestore_bucket, app):
    s3_engine: S3Engine = mock.MagicMock(S3Engine)
    s3_engine.connection = minio_client
    s3_engine.bucket = filestore_bucket
    app.local_s3_engine.override(s3_engine)
    return app.local_s3_storage()


def deployment_simple() -> Deployment:
    action_id = "abcd-1234"
    action_name = "testaction"
    action_version = "1.0"
    mocked_deployment = Deployment(
        id=action_id,
        kind=Kind.ACTION,
        name=action_name,
        version=action_version,
        container_image=f"{action_id}:{action_version}",
        execution_type=ExecutionType.SLURM_SSH_V1,
        addons={
            "hpc": {
                "username": "root",
                "password": None,
                "private_key": "LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KYjNCbGJuTnphQzFyWlhrdGRqRUFBQUFBQkc1dmJtVUFBQUFFYm05dVpRQUFBQUFBQUFBQkFBQUJsd0FBQUFkemMyZ3RjbgpOaEFBQUFBd0VBQVFBQUFZRUExL0h0TGltNFZMdWpGaHAyek02dnBIUmpsVmtwOUpqM2c5RlhrTVorVUcxblpsQmxja1h2CkNLRFpUTlU2WHhSd05HWFoyRFUwK1hPQWsvcXNqNHBERmE5U0ZKUjlFYnFkdjRuaUZnUEpZMU1FVi9hTnlzTlJqZE1YUHAKa01hOXdBQUl3UEFzQjhSVjltNG9mOFduRU0vT1lVSUxsS0gwWGM1YVh5ZFgyYUFmZzhQMGFYZUdsSWtqVFJyZmpXSmlxMQpVYnpDWVJiRUpFSnZrOHlPai9GaitoajNvSFV0b3lEVk04RkF0SXhGaDBNK1g3T1hPbzFRemp3VnhQUDZoN3JMdFVod3VoClpZV1k4amx3cGpHQUQ1L2I5ZFlpd3A5MmRndHNMbndWUGJCODdSeDJGTnFmMnVMOXR5emZoTEovcGZmb0dNRkVPdkxmYUMKQVpodkN0cmJ2VE1CalpRak00aVVjZWh0aVI0a0RhQU9ScnVjMDJ5VlN4R0JSZE5McWFCUmhBelRteTg3ZElFTG9KSWszZQpKTDhndTlsS2RBelBxcDFjeUg0Wnc0TzduWGFXTVptcGRKS0RzSm5ycXNtRk0rK1U0dm9UbWZGUzVDZmRBdmlnL0ZNMmFlCnp1L3Z1WVhuMXgyS2NmUFg0TE9MVzcrVnRDamxKOFdvZXExK0U5WnJBQUFGZ0Fkd3k4RUhjTXZCQUFBQUIzTnphQzF5YzIKRUFBQUdCQU5meDdTNHB1RlM3b3hZYWRzek9yNlIwWTVWWktmU1k5NFBSVjVER2ZsQnRaMlpRWlhKRjd3aWcyVXpWT2w4VQpjRFJsMmRnMU5QbHpnSlA2ckkrS1F4V3ZVaFNVZlJHNm5iK0o0aFlEeVdOVEJGZjJqY3JEVVkzVEZ6NlpER3ZjQUFDTUR3CkxBZkVWZlp1S0gvRnB4RFB6bUZDQzVTaDlGM09XbDhuVjltZ0g0UEQ5R2wzaHBTSkkwMGEzNDFpWXF0Vkc4d21FV3hDUkMKYjVQTWpvL3hZL29ZOTZCMUxhTWcxVFBCUUxTTVJZZERQbCt6bHpxTlVNNDhGY1R6K29lNnk3VkljTG9XV0ZtUEk1Y0tZeApnQStmMi9YV0lzS2ZkbllMYkM1OEZUMndmTzBjZGhUYW45cmkvYmNzMzRTeWY2WDM2QmpCUkRyeTMyZ2dHWWJ3cmEyNzB6CkFZMlVJek9JbEhIb2JZa2VKQTJnRGthN25OTnNsVXNSZ1VYVFM2bWdVWVFNMDVzdk8zU0JDNkNTSk4zaVMvSUx2WlNuUU0KejZxZFhNaCtHY09EdTUxMmxqR1pxWFNTZzdDWjY2ckpoVFB2bE9MNkU1bnhVdVFuM1FMNG9QeFRObW5zN3Y3N21GNTljZAppbkh6MStDemkxdS9sYlFvNVNmRnFIcXRmaFBXYXdBQUFBTUJBQUVBQUFHQUVKMEtORXo5WTNkMEdwVTZvcE1jTERqUUdJCk5kbVBYVHhRNXNBRFZsTG1iY1Y2RER6VURZSFAvUzkvaEhuMTBCTE9iUkZiTXlPaWJLczVmTGd0ZU9SYkxSUzhnSXZ6NXUKT0hneExxUm43LzVKRFVOb3NUckpvWEdtRGlqVUxkQWxKK1dlbWdXWkNmVkhBeEtYTlpZUSs3dnZ2SHpFMlJjVTRZNGMwegpFd2tnbzVDRjB6OWcyVGlid0tqYW5oUDlwMVRLNksvZEUwa2QyU20vcFcxWGdxeXVqMDFDRnZ1MmNoUC9RVU9FKzZZYWVvCnV0QWtTRnJxNXBMMDBjdzdjQW5NMGhGWFpiV01lcmNRR2FkK21LblZYT25aQ1FsWjRGSHZsQ3JQbmFlYTA5aHhZMkV4SHkKc0p5RVhBQVgxT2p5KzFUeWo1OG4xNEVDQllTRmhYc21ZNzZGV1Z2NkxsUzc1K3hiOWFmZ2RNWG93dTUvaEJ0VSt6bFRMVwpoSFVKOS9hOXZCaUZtOXo5dng2dEg1L3RXOFMwN2hYSmIzQjc1ZmtTaXFzelNLR1hpaWhaQ3h1cWpNZ0hFR1Z0K1EvcGRnCkc0Y2hRWmpuMmxFVXpOZFd3bGRjWWdRWDlPbk9ydmR4V1REKzltMklscVIzNitFUkR1ZlNlK0M1eFU2NGlUcmZXQkFBQUEKd0U1cU13d2crNVA2dGFzL2JFN0ExdHArZ2ViZkNTZFVnK1RXTGtyTCtCdURQYXNPellQbVJ6R1dpMWFOTGYrQVk3WmtmKwprelJMUWtmNktHbXdWaVlNUHdZekNWMUJXak42L0xCM2RkdlhJWHVNc3dOaHFOdmZWTFNSUDMzdDJ6SEs3QU42OGVyZ21CCm4wcFVvNk11Tkd0Zm90TXVTTU0rYjNXbUphM2FQeThsMHJiUVZDRjJEUjNiajduREQyR0pTS1luSm5kZjQrdzFWVmZKVVMKU2F4RGpXTkROUzBmSkUrQ2FsYVREOVlaT2c5RXV5Z2hZYjlETVhKdE1GeCtwbUxnQUFBTUVBOThKQlhZMzNJRGYzUVFSbQo5RHNQR2pWNVViYlFqV2FJT0pPVDZZR3pkWklsS3hMeHU5c2hrMUd5RTFYS3ZNSVNsZzJkbUk1aTRLT3YwMXorTWZMWVZUClFlNFdDTEZ3dGt3M1hSYzJGZ1pBbml0UFNLYTd4QXdYdHJidC9qS2JqMUg3cmEyZU5yVWN2bjNIQkdHZElaK1dBZ2NQRFcKU3lFOXh1ZlQxV2VTbkVpYk1kNU00VGsyTGtiTXVzTGVwdzNYN0srY3NtMlhKY3YyZkxTelhGUk5KTHF3L2J6ZkI3aTZ4ZApRQVpNb1VTQzFLWGZ0RVQ1OTl1TFprcnh0ZlhJVGhBQUFBd1FEZklNUkhEdXVFZ2pBT2pndVJIZzhhYWdINXl6WXhwMzFNCmFEUncvazJUNWhmMGEyQkhTUHpCSG1kT0E2MzlCeUJpNTIrVG1mUXVCcjNXRHNJTUZ1bVZuYk5rTEFoSnlpSDExQzhvQnMKV1pnNTFsVjdEeEhLS3UxU2tSWE9UWUFyRStJQ0NLdkNUUjlNd2RwTGJmbEVXN21xNEpISUVoQnBXYW9way9jVU1JakplSgpCbVFib3JwTjZJcU1pcmRuTlcvSzVBejJXaDRnUEJPaVlsSHdhejdOemlVTUhxS0QyWFN1M1ZvZjFhUkY2akxJejJpK2Z1Ckk4ckZUQ3FRSU5lTXNBQUFBSmRtVnNhRzlBWm05NEFRST0KLS0tLS1FTkQgT1BFTlNTSCBQUklWQVRFIEtFWS0tLS0tCg==",
                "frontend": "localhost",
                "fakeroot": True,
                "port": 2222,
                "slurm_args": "#!/bin/bash\n#SBATCH --nodes=1\n#SBATCH --tasks=1",
            }
        },
        resources=None,
        deployment_type="file_v1",
        log_level=1,
        node_pool=NodePool(
            name="toto", id="1", cpu="500m", memory="64Mi", site="defautl"
        ),
    )
    return mocked_deployment


def input_list() -> List[IODefinition]:
    return [
        IODefinition(
            id="test_str",
            name="test_str",
            type=IOType.STRING,
            optional=False,
        ),
        IODefinition(
            id="test_dir",
            name="test_dir",
            type=IOType.DIRECTORY,
            optional=False,
        ),
        IODefinition(
            id="test_file",
            name="test_file",
            type=IOType.FILE,
            optional=False,
        ),
        IODefinition(
            id="test_int",
            name="test_int",
            type=IOType.INTEGER,
            optional=False,
        ),
        IODefinition(
            id="test_float",
            name="test_float",
            type=IOType.FLOAT,
            optional=False,
        ),
    ]


def output_list() -> List[IODefinition]:
    return [
        IODefinition(
            id="test_str",
            name="test_str",
            type=IOType.STRING,
            optional=False,
        ),
        IODefinition(
            id="test_dir",
            name="test_dir",
            type=IOType.DIRECTORY,
            optional=False,
        ),
        IODefinition(
            id="test_file",
            name="test_file",
            type=IOType.FILE,
            optional=False,
        ),
        IODefinition(
            id="test_int",
            name="test_int",
            type=IOType.INTEGER,
            optional=False,
        ),
        IODefinition(
            id="test_float",
            name="test_float",
            type=IOType.FLOAT,
            optional=False,
        ),
    ]


class MockMessageBus(IMessageBus):
    def __init__(self):
        self.saved_logs = ""

    def register_event_handler(
        self, event: Type[BaseEvent], event_handler: Callable
    ) -> None:
        raise NotImplementedError()

    def register_command_handler(
        self, command: Type[BaseCommand], command_handler: Callable
    ) -> None:
        pass

    async def handle_event(
        self, event: BaseEvent, task_name: str | None = None
    ) -> None:
        if isinstance(event, TaskNewLogLinesEvent):
            message = cast(TaskNewLogLinesEvent, event)
            self.saved_logs += message.log_lines

    async def handle_command(self, command: BaseCommand, blocking=True) -> Any:
        if isinstance(command, InitTaskCommand):
            return ("exec_res_id", "wf_run_id")

    async def handle(self, messages: Messages) -> None:
        pass

    async def wait_for_event(
        self, event_type: Type[BaseEvent], with_parameters: dict = None
    ) -> BaseEvent:
        pass

    async def stop(self) -> None:
        pass


@pytest.fixture()
def message_bus():
    return MockMessageBus()


async def test_slurm_ssh_execution_custom_script_no_singularity(
    app,
    minio_client,
    filestore_bucket,
    storage_service,
    message_bus,
    ssh_helper,
):
    action_deployment = deployment_simple()
    action_deployment.addons["hpc"][
        "custom_script"
    ] = """
set -e
set -u
echo INPUTS and OUTPUTS
echo $test_str
echo $test_dir
echo $test_file
echo $test_int
echo $test_float
touch test_tmp_file
"""
    await _slurm_ssh_execution(
        app,
        action_deployment,
        minio_client,
        filestore_bucket,
        storage_service,
        message_bus,
        ssh_helper,
    )


async def test_slurm_ssh_execution_custom_script(
    app,
    minio_client,
    filestore_bucket,
    storage_service,
    message_bus,
    ssh_helper,
):
    deployment = deployment_simple()
    deployment.addons["hpc"][
        "custom_script"
    ] = "singularity run --no-umask --bind $RYAX_CACHE_DIR:/home/ryax/.cache  --bind $RYAX_ENVFILE:/data/default.env --bind $RYAX_HOME:/home/ryax --bind $RYAX_PREFIX:/iodir --pwd=/data $RYAX_ACTION_IMG"
    await _slurm_ssh_execution(
        app,
        deployment,
        minio_client,
        filestore_bucket,
        storage_service,
        message_bus,
        ssh_helper,
    )


async def test_slurm_ssh_execution(
    app,
    minio_client,
    filestore_bucket,
    storage_service,
    message_bus,
    ssh_helper,
):
    deployment = deployment_simple()
    await _slurm_ssh_execution(
        app,
        deployment,
        minio_client,
        filestore_bucket,
        storage_service,
        message_bus,
        ssh_helper,
    )


async def _slurm_ssh_execution(
    app,
    action_deployment,
    minio_client,
    filestore_bucket,
    storage_service,
    message_bus,
    ssh_helper,
):
    # Imitate a deployment by copying the sif image to the cluster frontend
    action_id = action_deployment.id
    execution_id = "9876"
    action_ref = f"{action_id}_{execution_id}"

    # Ensure cache_dir exists
    await ssh_helper.exec_ssh_cmd(f"mkdir -p {ryax_cache_dir}")

    # Delete everything on the remote machine
    await ssh_helper.exec_ssh_cmd(f"rm -fr {ryax_cache_dir}/{action_ref}*")

    # Mock the deployment phase, copy the sif file directly to slurm frontale
    await ssh_helper.put_ssh_file(
        "/tmp/ryax_test/test-processor.sif",
        os.path.join(ryax_cache_dir, f"{action_id}_1.0.sif"),
    )

    # Set the input dictionary
    input_dict = {
        "test_str": "name",
        "test_dir": "test-dir",
        "test_file": "test-file.txt",
        "test_int": 23,
        "test_float": 3.0,
    }

    test_str = "Hello"
    ssh_executor = SSHV1ExecutionService(
        storage_service=storage_service,
        message_bus=message_bus,
        tasks_service=app.tasks_service(),
        ryax_cache_dir=Path(".ryax_cache"),
        default_credentials=None,
    )

    hpc_addon = HpcServiceAddOn()
    addon_extra_args = await hpc_addon.hook_on_start_execution_on_runner(
        action_deployment
    )

    try:
        with tempfile.TemporaryDirectory() as tmpdir:
            # Create test input file
            test_file = os.path.join(tmpdir, input_dict["test_file"])
            with open(test_file, "w") as text_file:
                text_file.write(test_str)
            # Upload to minio
            minio_client.fput_object(
                filestore_bucket, input_dict["test_file"], test_file
            )

            # Create test input directory with 2 text files
            test_dir = os.path.join(tmpdir, input_dict["test_dir"])
            os.makedirs(test_dir)
            for i in ["test_file1.txt", "test_file2.txt"]:
                with open(os.path.join(test_dir, i), "w") as text_file:
                    text_file.write(f"{test_str} {i}")
            with tempfile.NamedTemporaryFile(suffix=".zip") as fp:
                # Make an archive .zip with all directory content
                shutil.make_archive(fp.name[: -len(".zip")], "zip", root_dir=test_dir)
                # Upload to minio
                minio_client.fput_object(
                    filestore_bucket, os.path.basename(fp.name), fp.name
                )
                input_dict["test_dir"] = os.path.basename(fp.name)

            execution = Execution(
                id=execution_id,
                inputs=input_dict,
                time_allotment=2000,
                deployment_id=action_deployment.id,
                inputs_definitions=input_list(),
                outputs_definitions=output_list(),
            )

            ssh = await SshConnectionHelper.create_from_dict(addon_extra_args).ssh()
            ret = await ssh.run("echo CURRENT_DIRECTORY: $(pwd)")
            print(ret)

            await ssh_executor.run(
                execution, action_deployment, {"hpc": addon_extra_args}
            )

            assert ssh_executor.message_bus.saved_logs != ""
            # FIXME: The logs are not captured properly because the log files are not created by Slurm in the root home
            # assert "running command" in ssh_executor.message_bus.saved_logs
            print(ssh_executor.message_bus.saved_logs)
            print('Error "can\'t read slurm-X.out" is a test artifact, safe to ignore!')

            # Verify that the expected output objects exist on the filestore
            outputs = minio_client.list_objects(
                filestore_bucket, "executions-data/", recursive=True
            )
            out_dir = "tmpoutdir"
            for obj in outputs:
                out_file = os.path.basename(obj.object_name)
                minio_client.fget_object(filestore_bucket, obj.object_name, out_file)
                try:
                    with zipfile.ZipFile(out_file, "r") as zip_ref:
                        zip_ref.extractall(out_dir)
                        for every_file in os.listdir(out_dir):
                            with open(
                                os.path.join(out_dir, every_file), "r"
                            ) as each_file:
                                assert (
                                    each_file.read()
                                    == f"{test_str} {os.path.basename(every_file)}"
                                )
                except zipfile.BadZipfile:
                    with open(out_file, "r") as file_ref:
                        assert file_ref.read() == test_str
                os.remove(out_file)
            shutil.rmtree(out_dir, ignore_errors=True)
    finally:
        # comment this for debugging
        ssh = await SshConnectionHelper.create_from_dict(addon_extra_args).ssh()
        # await ssh.run("rm -rf /root/.ryax_cache")
        await ssh.run("scancel -u root")
