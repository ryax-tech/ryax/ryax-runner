import asyncio
from pathlib import Path
from tempfile import TemporaryFile
from unittest import mock

import pytest

from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.worker.application.tasks_service import TasksService
from ryax.worker.domain.deployment.deployment_entities import Deployment, Kind
from ryax.worker.domain.deployment.deployment_values import (
    DeploymentState,
    ExecutionType,
)
from ryax.worker.infrastructure.deployment.sshslurm.ssh_slurm_action_deployment_service import (
    SshSlurmActionDeploymentService,
)
from ryax.worker.infrastructure.ssh.ssh_connection_helper import SshConnectionHelper

ryax_cache_dir = ".ryax_cache"


@pytest.fixture(scope="function")
def ssh_slurm_deployment(ryax_registry):
    action_id = "abcd-1234"
    action_name = "testaction"
    action_version = "1.0"
    return Deployment(
        id="1",
        kind=Kind.ACTION,
        name=action_name,
        version=action_version,
        container_image=f"docker://{ryax_registry}/{action_id}-python3_file_v1:{action_version}",
        execution_type=ExecutionType.GRPC_V1,
        deployment_type="",
        node_pool="default",
        internal_endpoint="",
        log_level="",
        resources=None,
        addons={
            "hpc": {
                "username": "root",
                "password": None,
                "private_key": "LS0tLS1CRUdJTiBPUEVOU1NIIFBSSVZBVEUgS0VZLS0tLS0KYjNCbGJuTnphQzFyWlhrdGRqRUFBQUFBQkc1dmJtVUFBQUFFYm05dVpRQUFBQUFBQUFBQkFBQUJsd0FBQUFkemMyZ3RjbgpOaEFBQUFBd0VBQVFBQUFZRUExL0h0TGltNFZMdWpGaHAyek02dnBIUmpsVmtwOUpqM2c5RlhrTVorVUcxblpsQmxja1h2CkNLRFpUTlU2WHhSd05HWFoyRFUwK1hPQWsvcXNqNHBERmE5U0ZKUjlFYnFkdjRuaUZnUEpZMU1FVi9hTnlzTlJqZE1YUHAKa01hOXdBQUl3UEFzQjhSVjltNG9mOFduRU0vT1lVSUxsS0gwWGM1YVh5ZFgyYUFmZzhQMGFYZUdsSWtqVFJyZmpXSmlxMQpVYnpDWVJiRUpFSnZrOHlPai9GaitoajNvSFV0b3lEVk04RkF0SXhGaDBNK1g3T1hPbzFRemp3VnhQUDZoN3JMdFVod3VoClpZV1k4amx3cGpHQUQ1L2I5ZFlpd3A5MmRndHNMbndWUGJCODdSeDJGTnFmMnVMOXR5emZoTEovcGZmb0dNRkVPdkxmYUMKQVpodkN0cmJ2VE1CalpRak00aVVjZWh0aVI0a0RhQU9ScnVjMDJ5VlN4R0JSZE5McWFCUmhBelRteTg3ZElFTG9KSWszZQpKTDhndTlsS2RBelBxcDFjeUg0Wnc0TzduWGFXTVptcGRKS0RzSm5ycXNtRk0rK1U0dm9UbWZGUzVDZmRBdmlnL0ZNMmFlCnp1L3Z1WVhuMXgyS2NmUFg0TE9MVzcrVnRDamxKOFdvZXExK0U5WnJBQUFGZ0Fkd3k4RUhjTXZCQUFBQUIzTnphQzF5YzIKRUFBQUdCQU5meDdTNHB1RlM3b3hZYWRzek9yNlIwWTVWWktmU1k5NFBSVjVER2ZsQnRaMlpRWlhKRjd3aWcyVXpWT2w4VQpjRFJsMmRnMU5QbHpnSlA2ckkrS1F4V3ZVaFNVZlJHNm5iK0o0aFlEeVdOVEJGZjJqY3JEVVkzVEZ6NlpER3ZjQUFDTUR3CkxBZkVWZlp1S0gvRnB4RFB6bUZDQzVTaDlGM09XbDhuVjltZ0g0UEQ5R2wzaHBTSkkwMGEzNDFpWXF0Vkc4d21FV3hDUkMKYjVQTWpvL3hZL29ZOTZCMUxhTWcxVFBCUUxTTVJZZERQbCt6bHpxTlVNNDhGY1R6K29lNnk3VkljTG9XV0ZtUEk1Y0tZeApnQStmMi9YV0lzS2ZkbllMYkM1OEZUMndmTzBjZGhUYW45cmkvYmNzMzRTeWY2WDM2QmpCUkRyeTMyZ2dHWWJ3cmEyNzB6CkFZMlVJek9JbEhIb2JZa2VKQTJnRGthN25OTnNsVXNSZ1VYVFM2bWdVWVFNMDVzdk8zU0JDNkNTSk4zaVMvSUx2WlNuUU0KejZxZFhNaCtHY09EdTUxMmxqR1pxWFNTZzdDWjY2ckpoVFB2bE9MNkU1bnhVdVFuM1FMNG9QeFRObW5zN3Y3N21GNTljZAppbkh6MStDemkxdS9sYlFvNVNmRnFIcXRmaFBXYXdBQUFBTUJBQUVBQUFHQUVKMEtORXo5WTNkMEdwVTZvcE1jTERqUUdJCk5kbVBYVHhRNXNBRFZsTG1iY1Y2RER6VURZSFAvUzkvaEhuMTBCTE9iUkZiTXlPaWJLczVmTGd0ZU9SYkxSUzhnSXZ6NXUKT0hneExxUm43LzVKRFVOb3NUckpvWEdtRGlqVUxkQWxKK1dlbWdXWkNmVkhBeEtYTlpZUSs3dnZ2SHpFMlJjVTRZNGMwegpFd2tnbzVDRjB6OWcyVGlid0tqYW5oUDlwMVRLNksvZEUwa2QyU20vcFcxWGdxeXVqMDFDRnZ1MmNoUC9RVU9FKzZZYWVvCnV0QWtTRnJxNXBMMDBjdzdjQW5NMGhGWFpiV01lcmNRR2FkK21LblZYT25aQ1FsWjRGSHZsQ3JQbmFlYTA5aHhZMkV4SHkKc0p5RVhBQVgxT2p5KzFUeWo1OG4xNEVDQllTRmhYc21ZNzZGV1Z2NkxsUzc1K3hiOWFmZ2RNWG93dTUvaEJ0VSt6bFRMVwpoSFVKOS9hOXZCaUZtOXo5dng2dEg1L3RXOFMwN2hYSmIzQjc1ZmtTaXFzelNLR1hpaWhaQ3h1cWpNZ0hFR1Z0K1EvcGRnCkc0Y2hRWmpuMmxFVXpOZFd3bGRjWWdRWDlPbk9ydmR4V1REKzltMklscVIzNitFUkR1ZlNlK0M1eFU2NGlUcmZXQkFBQUEKd0U1cU13d2crNVA2dGFzL2JFN0ExdHArZ2ViZkNTZFVnK1RXTGtyTCtCdURQYXNPellQbVJ6R1dpMWFOTGYrQVk3WmtmKwprelJMUWtmNktHbXdWaVlNUHdZekNWMUJXak42L0xCM2RkdlhJWHVNc3dOaHFOdmZWTFNSUDMzdDJ6SEs3QU42OGVyZ21CCm4wcFVvNk11Tkd0Zm90TXVTTU0rYjNXbUphM2FQeThsMHJiUVZDRjJEUjNiajduREQyR0pTS1luSm5kZjQrdzFWVmZKVVMKU2F4RGpXTkROUzBmSkUrQ2FsYVREOVlaT2c5RXV5Z2hZYjlETVhKdE1GeCtwbUxnQUFBTUVBOThKQlhZMzNJRGYzUVFSbQo5RHNQR2pWNVViYlFqV2FJT0pPVDZZR3pkWklsS3hMeHU5c2hrMUd5RTFYS3ZNSVNsZzJkbUk1aTRLT3YwMXorTWZMWVZUClFlNFdDTEZ3dGt3M1hSYzJGZ1pBbml0UFNLYTd4QXdYdHJidC9qS2JqMUg3cmEyZU5yVWN2bjNIQkdHZElaK1dBZ2NQRFcKU3lFOXh1ZlQxV2VTbkVpYk1kNU00VGsyTGtiTXVzTGVwdzNYN0srY3NtMlhKY3YyZkxTelhGUk5KTHF3L2J6ZkI3aTZ4ZApRQVpNb1VTQzFLWGZ0RVQ1OTl1TFprcnh0ZlhJVGhBQUFBd1FEZklNUkhEdXVFZ2pBT2pndVJIZzhhYWdINXl6WXhwMzFNCmFEUncvazJUNWhmMGEyQkhTUHpCSG1kT0E2MzlCeUJpNTIrVG1mUXVCcjNXRHNJTUZ1bVZuYk5rTEFoSnlpSDExQzhvQnMKV1pnNTFsVjdEeEhLS3UxU2tSWE9UWUFyRStJQ0NLdkNUUjlNd2RwTGJmbEVXN21xNEpISUVoQnBXYW9way9jVU1JakplSgpCbVFib3JwTjZJcU1pcmRuTlcvSzVBejJXaDRnUEJPaVlsSHdhejdOemlVTUhxS0QyWFN1M1ZvZjFhUkY2akxJejJpK2Z1Ckk4ckZUQ3FRSU5lTXNBQUFBSmRtVnNhRzlBWm05NEFRST0KLS0tLS1FTkQgT1BFTlNTSCBQUklWQVRFIEtFWS0tLS0tCg==",
                "frontend": "localhost",
                "port": 2222,
                "slurm_args": "#!/bin/bash\n#SBATCH --nodes=1\n#SBATCH --tasks=1",
            }
        },
    )


@pytest.fixture(scope="function")
def ryax_registry():
    return "localhost:5000"


@pytest.fixture(scope="function")
def ssh_slurm_deployment_service():
    return SshSlurmActionDeploymentService(
        message_bus=mock.MagicMock(IMessageBus),
        tasks_service=mock.MagicMock(TasksService),
        ryax_cache_dir=Path(".ryax_cache"),
        hpc_offloading_enabled=True,
        credentials=None,
    )


@pytest.fixture(scope="function")
def ssh_helper(ssh_slurm_deployment) -> SshConnectionHelper:
    return SshConnectionHelper(
        hostname=ssh_slurm_deployment.addons["hpc"].get("frontend"),
        port=ssh_slurm_deployment.addons["hpc"].get("port"),
        username=ssh_slurm_deployment.addons["hpc"].get("username"),
        password=ssh_slurm_deployment.addons["hpc"].get("password"),
        private_key=ssh_slurm_deployment.addons["hpc"].get("private_key"),
        config=None,
    )


async def _execute_in_shell(cmd_name: str, cmd: str) -> str:
    with TemporaryFile(mode="w+", encoding="utf-8") as stdout, TemporaryFile(
        mode="w+", encoding="utf-8"
    ) as stderr:
        proc = await asyncio.create_subprocess_shell(
            cmd, shell=True, stdout=stdout, stderr=stderr
        )
        await proc.communicate()
        stdout.seek(0)
        out = stdout.read()
        stderr.seek(0)
        err = stderr.read()
        if proc.returncode == 0:
            status = "Done: "
            print(f"{status}: {cmd_name} (pid = {str(proc.pid)})" f"\nSTDOUT: {out}")
            return out
        else:
            status = "Failed: "
            print(
                f"{status}: {cmd_name} (pid = {str(proc.pid)})"
                f"\nSTDOUT: {out}"
                f"\nSTDERR: {err}"
            )
            print(f"Error: while running process {cmd_name}: {out}\n{err}")
            raise Exception(
                f"Error: while running process {cmd_name}: {out}\n{err}",
            )


async def test_do_deployment(
    ssh_slurm_deployment: Deployment,
    ssh_slurm_deployment_service,
    ssh_helper,
):
    action_id = ssh_slurm_deployment.id
    action_version = ssh_slurm_deployment.version
    try:
        # Assumes it is running from the root of the project and file test-processor.tar was downloaded correctly
        # test-processor.tar is downloaded by test.sh script from the ryax registry.
        image_archive = "docker-archive:/tmp/ryax_test/test-processor.img"
        f"""
        Example using local registry:
        skopeo copy -a --dest-tls-verify=False --insecure-policy \
            {image_archive} \
            {ssh_slurm_deployment.container_image}
        """
        await _execute_in_shell(
            "Copy local image to registry",
            f"skopeo copy -a --dest-tls-verify=False --insecure-policy {image_archive} {ssh_slurm_deployment.container_image}",
        )

        # Test deployment apply
        await ssh_slurm_deployment_service.apply(ssh_slurm_deployment)

        assert (
            await ssh_slurm_deployment_service.get_current_state(ssh_slurm_deployment)
            is DeploymentState.READY
        )

        await ssh_slurm_deployment_service.delete(ssh_slurm_deployment)

        assert ssh_slurm_deployment_service.ssh_connection is None

    finally:
        await ssh_helper.exec_ssh_cmd(
            f"rm -f .ryax_cache/{action_id}_{action_version}.sif"
        )
