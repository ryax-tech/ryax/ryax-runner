import asyncio
from datetime import datetime
from unittest.mock import AsyncMock

from ryax.worker.domain.deployment.deployment_entities import (
    Deployment,
    Kind,
    Resources,
)
from ryax.worker.domain.deployment.deployment_values import (
    DeploymentState,
    ExecutionType,
)
from ryax.worker.domain.execution.execution_entities import Execution
from ryax.worker.domain.node_pool.node_pool_entities import NodePool
from ryax.worker.infrastructure.deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import (
    K8sEngine,
)

from ryax.worker.infrastructure.deployment.k8s.k8s_recommendation_service import (
    RecommendationService,
)


async def _wait_deployment_state(
    k8s_engine: K8sEngine, deployment_id: str, state: DeploymentState, timeout: float
) -> None:
    counter = 1
    while (
        counter <= 60
        and await k8s_engine.get_deployment_state(deployment_id) != state
        and timeout > 0
    ):
        await asyncio.sleep(0.01 * counter)
        timeout -= 0.01 * counter
        counter += 1
    if timeout <= 0 or counter > 60:
        raise TimeoutError()


async def test_already_applied_recommendation(
    k8s_engine: K8sEngine,
    recommendation_service: RecommendationService,
    deployment_service: K8SActionDeploymentService,
) -> None:
    # create a Deployment
    deployment = Deployment(
        name="a",
        version="1",
        id="123",
        kind=Kind.ACTION,
        container_image="k8s.gcr.io/pause:3.1",
        resources=Resources(
            cpu=0.6,
            memory=64 * 1024 * 1024,  # 64 Mi
        ),
        execution_type=ExecutionType.GRPC_V1,
        deployment_type="",
        internal_endpoint="",
        log_level="",
        addons={},
        node_pool=NodePool(name="toto", id="1", selector=None),
    )

    k8s_deployment_dict = deployment_service._generate(deployment, site_name="mysite")
    await k8s_engine.apply_deployment_definition(k8s_definition=k8s_deployment_dict)
    await _wait_deployment_state(
        k8s_engine=k8s_engine,
        deployment_id=deployment.id,
        state=DeploymentState.READY,
        timeout=20.0,
    )

    deployment_k8s = await k8s_engine.get_deployment(deployment.id)
    k8s_engine.patch_deployment = AsyncMock()

    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["cpu"]
        == "600m"
    )
    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["memory"]
        == "67108864"
    )

    # create Execution
    execution_1 = Execution(
        id="exec456",
        deployment_id=deployment.id,
        inputs_definitions=[],
        outputs_definitions=[],
        inputs=[],
        time_allotment=1,
    )

    # mock recommender behavior, annotate the code with a recommendation equal to previous resources
    recommendation_service.update_recommendation(
        image="k8s.gcr.io/pause:3.1",
        new_recommended_resources=Resources(
            cpu=0.6,
            memory=64 * 1024 * 1024,  # 64 Mi
        ),
        timestamp=datetime.fromtimestamp(1725544575),
    )

    # apply recommendation with new execution
    await recommendation_service.apply_recommendation(execution=execution_1)

    # check resources after applied recommendation
    deployment_k8s = await k8s_engine.get_deployment(deployment.id)

    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["cpu"]
        == "600m"
    )
    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["memory"]
        == "67108864"
    )
    k8s_engine.patch_deployment.assert_not_awaited()


async def test_update_recommendation(
    k8s_engine: K8sEngine,
    recommendation_service: RecommendationService,
    deployment_service: K8SActionDeploymentService,
) -> None:
    # VERIFY THAT THE TIMESTAMP IS UPDATED FOR THE IMAGE
    recommendation_service.update_recommendation(
        image="k8s.gcr.io/pause:3.1",
        new_recommended_resources=Resources(
            cpu=1.0,
            memory=1 * 1024 * 1024,
        ),
        timestamp=datetime.fromtimestamp(1725544505),
    )
    recommendation_service.update_recommendation(
        image="k8s.gcr.io/pause:3.1",
        new_recommended_resources=Resources(
            cpu=2.0,
            memory=2 * 1024 * 1024,
        ),
        timestamp=datetime.fromtimestamp(1725544005),
    )

    recommended_values = recommendation_service.get_recommendation(
        target_image="k8s.gcr.io/pause:3.1",
    )
    assert recommended_values.memory == 1 * 1024 * 1024
    assert recommended_values.cpu == 1.0


async def test_apply_recommendation_existing_deployment(
    k8s_engine: K8sEngine,
    recommendation_service: RecommendationService,
    deployment_service: K8SActionDeploymentService,
) -> None:
    # create a Deployment
    deployment = Deployment(
        name="a",
        version="1",
        id="123",
        kind=Kind.ACTION,
        container_image="k8s.gcr.io/pause:3.1",
        resources=Resources(
            cpu=0.5,
            memory=64 * 1024 * 1024,  # 64 Mi
        ),
        execution_type=ExecutionType.GRPC_V1,
        deployment_type="",
        internal_endpoint="",
        log_level="",
        addons={},
        node_pool=NodePool(name="toto", id="1", selector=None),
    )

    k8s_deployment_dict = deployment_service._generate(deployment, site_name="mysite")
    await k8s_engine.apply_deployment_definition(k8s_definition=k8s_deployment_dict)
    await _wait_deployment_state(
        k8s_engine=k8s_engine,
        deployment_id=deployment.id,
        state=DeploymentState.READY,
        timeout=20.0,
    )

    # TODO check recommender annotations, convert them to json, check internals
    deployment_k8s = await k8s_engine.get_deployment(deployment.id)

    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["cpu"]
        == "500m"
    )
    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["memory"]
        == "67108864"
    )

    # create Execution
    execution_1 = Execution(
        id="exec456",
        deployment_id=deployment.id,
        inputs_definitions=[],
        outputs_definitions=[],
        inputs=[],
        time_allotment=1,
    )

    # apply recommendation with new execution
    await recommendation_service.apply_recommendation(execution=execution_1)

    # check resources after applied recommendation
    deployment_k8s = await k8s_engine.get_deployment(deployment.id)

    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["cpu"]
        == "500m"
    )
    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["memory"]
        == "67108864"
    )

    # mock recommender behavior, annotate the code with a recommendation
    # WARN annotations input are lost after add annotations, this can be safely ignored because it is a mock
    recommendation_service.update_recommendation(
        image="k8s.gcr.io/pause:3.1",
        new_recommended_resources=Resources(
            cpu=0.22,
            memory=123 * 1024,
        ),
        timestamp=datetime.fromtimestamp(1725544575),
    )

    # create Execution
    execution_2 = Execution(
        id="exec123",
        deployment_id=deployment.id,
        inputs_definitions=[],
        outputs_definitions=[],
        inputs=[],
        time_allotment=1,
    )

    # apply recommendation with new execution
    await recommendation_service.apply_recommendation(execution=execution_2)

    # check resources after applied recommendation
    deployment_k8s = await k8s_engine.get_deployment(deployment.id)

    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["cpu"]
        == "220m"
    )
    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["memory"]
        == "125952"
    )
