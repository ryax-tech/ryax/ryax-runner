import os
import uuid
from unittest import mock
from unittest.mock import MagicMock

import kubernetes_asyncio as k8s
import pytest
import pytest_asyncio

from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.worker.infrastructure.deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import K8sEngine
from ryax.worker.infrastructure.deployment.k8s.k8s_recommendation_service import (
    RecommendationService,
)


@pytest_asyncio.fixture
async def k8s_engine() -> K8sEngine:
    namespace = f"test-{uuid.uuid4()}"
    engine = K8sEngine(
        user_namespace=namespace,
        message_bus=MagicMock(IMessageBus),
        loki_url=os.environ.get("RYAX_LOKI_API_URL"),
    )
    await engine.init()
    await engine.create_user_namespace()
    yield engine
    try:
        async with k8s.client.ApiClient() as api:
            core_api = k8s.client.CoreV1Api(api)
            await core_api.delete_namespace(name=engine.user_namespace)
    except Exception:
        pass


@pytest.fixture
def recommendation_service(k8s_engine: K8sEngine) -> None:
    return RecommendationService(k8s_engine=k8s_engine, site_name="mysite")


@pytest.fixture
def deployment_service(k8s_engine: K8sEngine):
    rec = RecommendationService(
        k8s_engine=k8s_engine,
        site_name="default",
    )
    return K8SActionDeploymentService(
        engine=k8s_engine,
        tasks_service=mock.MagicMock(),
        default_grpc_port_inside_actions=8081,
        site_name="default",
        recommendation_service=rec,
        resource_recommendation_enabled=True,
        action_pull_secret_name="",
    )
