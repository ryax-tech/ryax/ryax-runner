from datetime import datetime
from unittest import mock


from ryax.worker.domain.deployment.deployment_entities import (
    Deployment,
    Kind,
    Resources,
)
from ryax.worker.domain.deployment.deployment_values import (
    ExecutionType,
    DeploymentState,
)
from ryax.worker.domain.node_pool.node_pool_entities import NodePool
from ryax.worker.infrastructure.deployment.k8s.k8s_action_deployment_service import (
    K8SActionDeploymentService,
)
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import K8sEngine


def new_deployment():
    return Deployment(
        name="a",
        version="1",
        id="123",
        kind=Kind.TRIGGER,
        container_image="k8s.gcr.io/pause:3.1",
        resources=Resources(
            cpu=0.5,
            memory=64 * 1024 * 1024,  # 64 Mi
        ),
        execution_type=ExecutionType.GRPC_V1,
        deployment_type="",
        internal_endpoint="",
        log_level="",
        addons={
            "kubernetes": {
                "parameters": {
                    "labels": "label1=value1,label2=value2",
                }
            }
        },
        node_pool=NodePool(name="toto", id="1", selector={"a": "b"}),
    )


async def test_apply_action_deployment():
    user_namespace = "ryaxns-execs"
    engine: K8sEngine = mock.MagicMock(K8sEngine, user_namespace=user_namespace)
    action_deployment = new_deployment()
    service = K8SActionDeploymentService(
        engine=engine,
        tasks_service=mock.MagicMock(),
        default_grpc_port_inside_actions=8081,
        site_name="my_site",
        recommendation_service=mock.MagicMock(),
        resource_recommendation_enabled=False,
        action_pull_secret_name="",
    )
    await service.apply(action_deployment)
    engine.apply_deployment_definition.assert_called_once()
    engine.apply_service_definition.assert_called_once()


async def test_apply_action_deployment_with_recommendation(
    deployment_service: K8SActionDeploymentService,
    k8s_engine: K8sEngine,
):
    action_deployment = new_deployment()
    action_deployment_2 = Deployment(
        name="b",
        version="2",
        id="333",
        kind=Kind.ACTION,
        container_image="k8s.gcr.io/pause:3.1",
        resources=Resources(
            cpu=0.5,
            memory=64 * 1024 * 1024,  # 64 Mi
        ),
        execution_type=ExecutionType.GRPC_V1,
        deployment_type="",
        internal_endpoint="",
        log_level="",
        addons={},
        node_pool=NodePool(name="toto", id="1", selector=None),
    )
    k8s_deployment_dict_2 = deployment_service._generate(
        action_deployment_2, site_name="mysite"
    )
    await k8s_engine.apply_deployment_definition(k8s_definition=k8s_deployment_dict_2)
    await deployment_service.recommendation_service._wait_deployment_state(
        deployment_id=action_deployment_2.id,
        state=DeploymentState.READY,
        timeout=20.0,
    )

    deployment_service.recommendation_service.update_recommendation(
        image="k8s.gcr.io/pause:3.1",
        new_recommended_resources=Resources(
            cpu=0.22,
            memory=123 * 1024,
        ),
        timestamp=datetime.fromtimestamp(1725544575),
    )
    print(
        "List of recommendations: "
        + str(deployment_service.recommendation_service.recommendation)
    )

    await deployment_service.apply(action_deployment)

    # check resources after applied recommendation
    deployment_k8s = await k8s_engine.get_deployment(action_deployment.id)

    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["cpu"]
        == "220m"
    )
    assert (
        int(
            deployment_k8s.spec.template.spec.containers[0].resources.requests["memory"]
        )
        == 123 * 1024
    )


async def test_apply_action_deployment_with_recommendation_disabled(
    deployment_service: K8SActionDeploymentService,
    k8s_engine: K8sEngine,
):
    action_deployment = new_deployment()
    action_deployment_2 = Deployment(
        name="b",
        version="2",
        id="333",
        kind=Kind.ACTION,
        container_image="k8s.gcr.io/pause:3.1",
        resources=Resources(
            cpu=0.5,
            memory=64 * 1024 * 1024,  # 64 Mi
        ),
        execution_type=ExecutionType.GRPC_V1,
        deployment_type="",
        internal_endpoint="",
        log_level="",
        addons={},
        node_pool=NodePool(name="toto", id="1", selector=None),
    )

    k8s_deployment_dict_2 = deployment_service._generate(
        action_deployment_2, site_name="mysite"
    )
    await k8s_engine.apply_deployment_definition(k8s_definition=k8s_deployment_dict_2)
    await deployment_service.recommendation_service._wait_deployment_state(
        deployment_id=action_deployment_2.id,
        state=DeploymentState.READY,
        timeout=20.0,
    )

    # mock recommender behavior, annotate the code with a recommendation
    # WARN annotations input are lost after add annotations, this can be safely ignored because it is a mock
    deployment_service.recommendation_service.update_recommendation(
        image="k8s.gcr.io/pause:3.1",
        new_recommended_resources=Resources(
            cpu=0.22,
            memory=123 * 1024,
        ),
        timestamp=datetime.fromtimestamp(1725544575),
    )

    deployment_service.resource_recommendation_enabled = False

    await deployment_service.apply(action_deployment)

    # check resources after applied recommendation
    deployment_k8s = await k8s_engine.get_deployment(action_deployment.id)

    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["cpu"]
        == "500m"
    )
    assert (
        deployment_k8s.spec.template.spec.containers[0].resources.requests["memory"]
        == "67108864"
    )
