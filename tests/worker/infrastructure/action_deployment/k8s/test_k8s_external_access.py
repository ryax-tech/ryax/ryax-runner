from unittest.mock import MagicMock

import kubernetes_asyncio as k8s
import pytest_asyncio

from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.worker.domain.deployment.deployment_entities import Deployment, Kind
from ryax.worker.domain.deployment.deployment_values import ExecutionType
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import K8sEngine
from ryax.worker.infrastructure.deployment.k8s.k8s_grpcv1_external_access import (
    K8sGRPCV1ExternalAccess,
)


@pytest_asyncio.fixture
async def engine() -> K8sEngine:
    engine = K8sEngine(
        user_namespace="testingress", message_bus=MagicMock(IMessageBus), loki_url=""
    )
    await engine.init()
    await engine.create_user_namespace()
    yield engine
    try:
        async with k8s.client.ApiClient() as api:
            core_api = k8s.client.CoreV1Api(api)
            await core_api.delete_namespace(name=engine.user_namespace)
    except Exception:
        pass


async def test_k8s_ingress(engine: K8sEngine) -> None:
    k8s_external_access = K8sGRPCV1ExternalAccess.create_from_deployment(
        deployment=Deployment(
            id="mdid1",
            name="techname",
            version="1.0",
            kind=Kind.TRIGGER,
            container_image="myimage",
            execution_type=ExecutionType.GRPC_V1,
            deployment_type="toto",
            log_level="Info",
            resources=None,
            internal_endpoint="test",
            addons={},
            node_pool=None,
        ),
        user_namespace="usernamespace",
        grpc_connection_port=8081,
    )

    k8s_service_def = k8s_external_access.generate()

    await engine.apply_service_definition(k8s_service_def)
    await engine.delete_service("mdid1")
