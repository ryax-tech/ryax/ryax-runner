import pytest

from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.common.infrastructure.kubernetes.k8s_utils import safe_resource_name


@pytest.mark.parametrize(
    "action_technical_name,action_version,k8s_deployment_id",
    [
        pytest.param(
            "technical_name",
            "v234",
            "technical-name-v234",
            id="with underscore",
        ),
        pytest.param(
            "action technical name",
            "v234",
            "action-technical-name-v234",
            id="with spaces",
        ),
        pytest.param(
            "action.technical.name",
            "v.234",
            "action-technical-name-v-234",
            id="with dots",
        ),
        pytest.param(
            "-actiontechnicalname",
            "v234",
            "actiontechnicalname-v234",
            id="with name starting with dashes",
        ),
        pytest.param(
            "actiontechnicalname---",
            "v234",
            "actiontechnicalname-v234",
            id="with name ending with dashes",
        ),
        pytest.param(
            "actiontechnicalname",
            "v234",
            "actiontechnicalname-v234",
            id="with name having uppercase",
        ),
        pytest.param(
            "actiontechnicalname",
            "V234",
            "actiontechnicalname-v234",
            id="with version having uppercase",
        ),
        pytest.param(
            "actiontechnicalname-veeeerrrrryyyyyyylooooooonggggggggggg--------------------------------------------------------------------------------------------------------------------totototot",
            "V00000000000000000000000000000.2",
            "actiontechnicalname-veeeerrrrryyyyyyyloooo-v000000000000",
            id="very long name and version",
        ),
    ],
)
def test_deployment_name(
    action_technical_name: str, action_version: str, k8s_deployment_id: str
) -> None:
    new_id = ActionDeployment.new_id()
    deployment_name = safe_resource_name(new_id, action_technical_name, action_version)
    assert deployment_name == k8s_deployment_id + "-" + new_id[-4:]
    assert len(deployment_name) < 64
