import asyncio
import os
import uuid
from unittest.mock import AsyncMock, MagicMock

import kubernetes_asyncio as k8s
import pytest
import pytest_asyncio
from kubernetes_asyncio.client import (
    CoreV1Api,
    V1ContainerState,
    V1ContainerStateWaiting,
    V1ContainerStatus,
    V1Deployment,
    V1DeploymentStatus,
    V1Pod,
    V1PodCondition,
    V1PodList,
    V1PodStatus,
)

from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.worker.domain.deployment.deployment_events import DeploymentUpdatedEvent
from ryax.worker.domain.deployment.deployment_values import DeploymentState
from ryax.worker.infrastructure.deployment.k8s.k8s_engine import (
    K8sEngine,
    _do_get_deployment_state,
)


@pytest_asyncio.fixture
async def engine() -> K8sEngine:
    namespace = f"test-{uuid.uuid4()}"
    engine = K8sEngine(
        user_namespace=namespace,
        message_bus=MagicMock(IMessageBus),
        loki_url=os.environ.get("RYAX_LOKI_API_URL"),
    )
    await engine.init()
    await engine.create_user_namespace()
    yield engine
    try:
        async with k8s.client.ApiClient() as api:
            core_api = k8s.client.CoreV1Api(api)
            await core_api.delete_namespace(name=engine.user_namespace)
    except Exception:
        pass


@pytest.mark.asyncio
async def test_unknown_deployment(engine):
    assert await engine.get_deployment_state("") == DeploymentState.UNDEPLOYED


async def _wait_deployment_state(
    engine: K8sEngine, deployment_id: str, state: DeploymentState, timeout: float
) -> None:
    counter = 1
    while (
        counter <= 60
        and await engine.get_deployment_state(deployment_id) != state
        and timeout > 0
    ):
        await asyncio.sleep(0.01 * counter)
        timeout -= 0.01 * counter
        counter += 1
    if timeout <= 0 or counter > 60:
        raise TimeoutError()


async def test_patch_deployment_resources(engine: K8sEngine) -> None:
    deployment_id = "test-deployment-id"

    wait_ready_state = asyncio.create_task(
        _wait_deployment_state(engine, deployment_id, DeploymentState.READY, 20.0)
    )

    await engine.apply_deployment_definition(
        {
            "apiVersion": "apps/v1",
            "kind": "Deployment",
            "metadata": {
                "name": deployment_id,
                "labels": {"action_deployment_id": deployment_id},
            },
            "spec": {
                "selector": {"matchLabels": {"app": "test"}},
                "template": {
                    "metadata": {"labels": {"app": "test"}, "name": "test"},
                    "spec": {
                        "containers": [
                            {
                                "image": "k8s.gcr.io/pause:3.1",
                                "name": "test",
                                "resources": {
                                    "requests": {
                                        "memory": "1Gi",
                                        "cpu": "1",
                                    },
                                    "limits": {
                                        "memory": "1Gi",
                                    },
                                },
                            },
                        ],
                    },
                },
            },
        }
    )

    await wait_ready_state
    deployment = await engine.get_deployment(deployment_id)
    assert (
        deployment.spec.template.spec.containers[0].resources.requests["memory"]
        == "1Gi"
    )
    assert deployment.spec.template.spec.containers[0].resources.requests["cpu"] == "1"
    assert (
        deployment.spec.template.spec.containers[0].resources.limits["memory"] == "1Gi"
    )

    await engine.patch_deployment(
        deployment_id=deployment_id,
        operations=[
            {
                "op": "replace",
                "path": "/spec/template/spec/containers/0/resources",
                "value": {
                    "requests": {
                        "memory": "64Mi",
                        "cpu": "100m",
                    },
                    "limits": {
                        "memory": "512Mi",
                    },
                },
            },
        ],
    )

    deployment = await engine.get_deployment(deployment_id)

    assert (
        deployment.spec.template.spec.containers[0].resources.requests["memory"]
        == "64Mi"
    )
    assert (
        deployment.spec.template.spec.containers[0].resources.requests["cpu"] == "100m"
    )
    assert (
        deployment.spec.template.spec.containers[0].resources.limits["memory"]
        == "512Mi"
    )


@pytest.mark.asyncio
async def test_simple_deployment(engine: K8sEngine) -> None:
    deployment_id = "test-deployment-id"

    wait_deploying_state = asyncio.create_task(
        _wait_deployment_state(engine, deployment_id, DeploymentState.DEPLOYING, 20.0)
    )
    wait_ready_state = asyncio.create_task(
        _wait_deployment_state(engine, deployment_id, DeploymentState.READY, 20.0)
    )

    await engine.apply_deployment_definition(
        {
            "apiVersion": "apps/v1",
            "kind": "Deployment",
            "metadata": {
                "name": deployment_id,
                "labels": {"action_deployment_id": deployment_id},
            },
            "spec": {
                "selector": {"matchLabels": {"app": "test"}},
                "template": {
                    "metadata": {"labels": {"app": "test"}, "name": "test"},
                    "spec": {
                        "containers": [
                            {"image": "k8s.gcr.io/pause:3.1", "name": "test"}
                        ]
                    },
                },
            },
        }
    )

    await wait_deploying_state

    watcher = asyncio.create_task(engine.watch_deployment(deployment_id))

    await wait_ready_state

    engine.message_bus.handle.assert_called_with(
        [
            DeploymentUpdatedEvent(
                deployment_id,
                state=DeploymentState.READY,
                logs="All the pods are running.",
            )
        ]
    )

    await engine.delete_deployment(deployment_id)

    await _wait_deployment_state(
        engine, deployment_id, DeploymentState.UNDEPLOYED, 20.0
    )

    watcher.cancel()


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "deployment, pod, message, expected_state",
    [
        (
            MagicMock(V1Deployment),
            V1Pod(
                status=V1PodStatus(
                    conditions=[
                        V1PodCondition(
                            type="PodScheduled",
                            status="False",
                            message="Not enough resources :(",
                        )
                    ]
                )
            ),
            "Pod not scheduled yet",
            DeploymentState.DEPLOYING,
        ),
        (
            MagicMock(V1Deployment),
            V1Pod(
                status=V1PodStatus(
                    conditions=[
                        V1PodCondition(
                            type="ContainersReady",
                            status="False",
                        )
                    ],
                    container_statuses=[
                        V1ContainerStatus(
                            image="",
                            image_id="",
                            name="",
                            ready="",
                            restart_count="",
                            state=V1ContainerState(
                                waiting=V1ContainerStateWaiting(
                                    reason="ContainerCreating",
                                    message="Hold on a while longer",
                                )
                            ),
                        )
                    ],
                )
            ),
            "ContainerCreating",
            DeploymentState.DEPLOYING,
        ),
        (
            MagicMock(V1Deployment),
            V1Pod(
                status=V1PodStatus(
                    phase="Pending",
                    conditions=[
                        V1PodCondition(
                            type="Initialized",
                            status="True",
                        ),
                        V1PodCondition(
                            type="Ready",
                            status="False",
                        ),
                        V1PodCondition(
                            type="ContainersReady",
                            status="False",
                        ),
                        V1PodCondition(
                            type="PodScheduled",
                            status="True",
                        ),
                    ],
                    container_statuses=[
                        V1ContainerStatus(
                            image="",
                            image_id="",
                            name="",
                            ready="",
                            restart_count="",
                            state=V1ContainerState(
                                waiting=V1ContainerStateWaiting(
                                    reason="ImagePullBackOff",
                                    message='Back-off pulling image "registry/app:v4"',
                                )
                            ),
                        )
                    ],
                )
            ),
            'ImagePullBackOff: Back-off pulling image "registry/app:v4"',
            DeploymentState.ERROR,
        ),
        (
            MagicMock(V1Deployment),
            V1Pod(
                status=V1PodStatus(
                    phase="Pending",
                    conditions=[
                        V1PodCondition(
                            type="PodScheduled",
                            status="False",
                            reason="Unschedulable",
                            message="0/2 nodes are available: 2 Insufficient cpu.",
                        ),
                    ],
                )
            ),
            "Unable to deploy Action: 0/2 nodes are available: 2 Insufficient cpu.",
            DeploymentState.NO_RESOURCES_AVAILABLE,
        ),
        (
            V1Deployment(
                status=V1DeploymentStatus(
                    conditions=[
                        V1PodCondition(
                            type="Progressing",
                            status="True",
                            reason="NewReplicaSetCreated",
                            message="Created new replica set",
                        ),
                        V1PodCondition(
                            type="ReplicaFailure",
                            status="True",
                            reason="FailedCreate",
                            message="ERROR MESSAGE FROM KUBE",
                        ),
                        V1PodCondition(
                            type="Available",
                            status="False",
                            reason="MinimumReplicasUnavailable",
                            message="Deployment does not have minimum availability.",
                        ),
                    ],
                )
            ),
            MagicMock(V1Pod),
            "ERROR MESSAGE FROM KUBE",
            DeploymentState.ERROR,
        ),
    ],
)
async def test_do_get_deployment_state(
    deployment: V1Deployment,
    pod: V1Pod,
    message: str,
    expected_state: DeploymentState,
) -> None:
    core_v1 = MagicMock(CoreV1Api)
    core_v1.list_namespaced_pod = AsyncMock(return_value=V1PodList(items=[pod]))

    assert await _do_get_deployment_state(core_v1, deployment) == (
        expected_state,
        message,
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "deployment",
    [
        {
            "apiVersion": "apps/v1",
            "kind": "Deployment",
            "metadata": {
                "name": "test-deployment",
                "labels": {"action_deployment_id": "test-deployment"},
            },
            "spec": {
                "selector": {"matchLabels": {"app": "test"}},
                "template": {
                    "metadata": {"labels": {"app": "test"}, "name": "test"},
                    "spec": {
                        "containers": [
                            {
                                "image": "k8s.gcr.io/pause:3.1",
                                "name": "test",
                                "command": ["runthis"],
                            }
                        ]
                    },
                },
            },
        },
        {
            "apiVersion": "apps/v1",
            "kind": "Deployment",
            "metadata": {
                "name": "test-deployment",
                "labels": {"action_deployment_id": "test-deployment"},
            },
            "spec": {
                "selector": {"matchLabels": {"app": "test"}},
                "template": {
                    "metadata": {"labels": {"app": "test"}, "name": "test"},
                    "spec": {
                        "containers": [
                            {
                                "image": "findthis:1234",
                                "name": "test",
                            }
                        ]
                    },
                },
            },
        },
    ],
)
async def test_errored_deployment(deployment: dict, engine: K8sEngine):
    deployment_id = deployment["metadata"]["name"]

    watcher = asyncio.create_task(engine.watch_deployment(deployment_id))
    wait_deploying_state = asyncio.create_task(
        _wait_deployment_state(engine, deployment_id, DeploymentState.DEPLOYING, 20.0)
    )
    wait_error_state = asyncio.create_task(
        _wait_deployment_state(engine, deployment_id, DeploymentState.ERROR, 20.0)
    )

    await engine.apply_deployment_definition(deployment)

    await wait_deploying_state
    await wait_error_state
    engine.message_bus.handle.assert_called()

    wait_undeploy_state = asyncio.create_task(
        _wait_deployment_state(engine, deployment_id, DeploymentState.UNDEPLOYED, 20.0)
    )
    await engine.delete_deployment(deployment_id)

    await wait_undeploy_state
    watcher.cancel()
