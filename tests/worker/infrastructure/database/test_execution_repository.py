import pytest

from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.execution.execution_entities import (
    Execution,
    IOType,
    OutputDefinition,
    InputDefinition,
)
from ryax.worker.domain.execution.execution_exceptions import ExecutionNotFoundError
from ryax.worker.domain.execution.execution_values import ExecutionState


def test_execution_repository(
    app_container: ApplicationContainer, database_engine: SqlalchemyDatabaseEngine
):
    uow = app_container.unit_of_work()
    execution_id = "123"
    execution_def = Execution(
        id=execution_id,
        deployment_id="123",
        inputs_definitions=[
            InputDefinition(
                type=IOType.FILE,
                name="test",
                id="1",
                optional=False,
            )
        ],
        outputs_definitions=[
            OutputDefinition(
                type=IOType.FILE,
                name="test",
                id="2",
                optional=False,
            )
        ],
        inputs={"a": 123},
        time_allotment=12334,
        state=ExecutionState.CREATED,
    )

    with uow:
        uow.execution_repository.add(execution_def)
        uow.commit()

    with uow:
        db_execution = uow.execution_repository.get(execution_id)
        assert execution_def == db_execution

    with uow:
        uow.execution_repository.delete(execution_id)
        uow.commit()

    with uow:
        with pytest.raises(ExecutionNotFoundError):
            uow.execution_repository.get(execution_id)
