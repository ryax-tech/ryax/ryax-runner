import pytest

from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.worker.container import ApplicationContainer
from ryax.worker.domain.deployment.deployment_entities import (
    Deployment,
    Resources,
    Kind,
)
from ryax.worker.domain.deployment.deployment_exceptions import DeploymentNotFoundError
from ryax.worker.domain.deployment.deployment_values import ExecutionType
from ryax.worker.domain.node_pool.node_pool_entities import NodePool


def test_deployment_repository(
    app_container: ApplicationContainer, database_engine: SqlalchemyDatabaseEngine
):
    uow = app_container.unit_of_work()
    deployment_id = "123"
    deployment_def = Deployment(
        id=deployment_id,
        name="test-name",
        version="v123",
        kind=Kind.ACTION,
        container_image="container",
        resources=Resources(time=12, cpu=1, memory=1, gpu=None),
        log_level="",
        deployment_type="",
        internal_endpoint="",
        addons={},
        execution_type=ExecutionType.GRPC_V1,
        node_pool=NodePool(id="1", name="1", selector={"a": "b"}),
    )

    with uow:
        uow.deployment_repository.add(deployment_def)
        uow.commit()

    with uow:
        db_deployment = uow.deployment_repository.get(deployment_id)
        assert deployment_def == db_deployment

    with uow:
        deployment = uow.deployment_repository.get_eager(deployment_id)
    assert deployment.node_pool.name
    assert deployment.resources.cpu

    with uow:
        uow.deployment_repository.delete(deployment_id)
        uow.commit()

    with uow:
        with pytest.raises(DeploymentNotFoundError):
            uow.deployment_repository.get(deployment_id)
