import tempfile

import pytest
from sqlalchemy.orm import clear_mappers

from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.worker.container import ApplicationContainer
from ryax.worker.infrastructure.database.mapper import map_orm
from ryax.worker.infrastructure.database.metadata import metadata


@pytest.fixture()
def app_container() -> ApplicationContainer:
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@pytest.fixture()
def database_engine(app_container: ApplicationContainer) -> SqlalchemyDatabaseEngine:
    with tempfile.TemporaryDirectory() as tmp_dir:
        database_url = f"sqlite:///{tmp_dir}/ryax.db"
        engine = SqlalchemyDatabaseEngine(database_url, map_orm)
        engine.connect()
        app_container.database_engine.override(engine)
        yield engine
        clear_mappers()
        metadata.drop_all(engine._engine)
        engine.disconnect()
