import datetime

import pytest

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionInput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionKind,
    ActionIOType,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.workflow_action.workflow_action import (
    WorkflowAction,
    StaticInput,
    ReferenceInput,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from tests.unit_of_work_stub import UnitOfWorkStub


@pytest.fixture
def app_container():
    app_container = ApplicationContainer()
    uow: IUnitOfWork = UnitOfWorkStub()
    app_container.unit_of_work.override(uow)
    from ryax.addons import ryax_http_service

    ryax_http_service.HttpServiceAddOn(app_container.addons())

    yield app_container
    app_container.unit_of_work = None
    app_container.unwire()


def test_input_reference_solver(app_container):
    now = datetime.datetime.now(datetime.timezone.utc)

    previous_action_name = "previous_action"
    previous_action_output_name = "prev_output"

    action_definition = ActionDefinition(
        id="action_def_id",
        project_id="test",
        technical_name="tech",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        container_image="cont",
        human_name="humname",
        description="desc",
        inputs=[
            ActionInput(
                id="0",
                name="from_static_inputs",
                type=ActionIOType.STRING,
                display_name="foo display",
                help="help",
            ),
            ActionInput(
                id="0",
                name="from_ref_inputs",
                type=ActionIOType.STRING,
                display_name="foo display",
                help="help",
            ),
        ],
        outputs=[
            ActionOutput(
                id="1",
                name=previous_action_output_name,
                type=ActionIOType.STRING,
                display_name="foo display",
                help="help",
            )
        ],
        addons_inputs=[
            ActionInput(
                id="1",
                name="from_static_addon",
                type=ActionIOType.INTEGER,
                display_name="foo display",
                help="help",
            ),
            ActionInput(
                id="1",
                name="from_ref_addon",
                type=ActionIOType.INTEGER,
                display_name="foo display",
                help="help",
            ),
        ],
    )

    workflow_action = WorkflowAction(
        id="wfmod",
        id_in_studio="name",
        workflow_deployment_id="depl_id",
        workflow_definition_id="wfdef",
        definition=action_definition,
        static_inputs=[
            StaticInput(
                name="from_static_inputs", id="0", value="VALUE_FROM_STATIC_INPUT"
            ),
            StaticInput(
                name="from_static_addon", id="1", value="VALUE_FROM_STATIC_ADDON"
            ),
            StaticInput(name="base_url", id="2", value="SHOULD_BE_FILTERED"),
            StaticInput(name="ryax_endpoint_prefix", id="4", value="SHOULD_BE_PRESENT"),
        ],
        reference_inputs=[
            ReferenceInput(
                name="from_ref_inputs",
                id="0",
                action_name=previous_action_name,
                output_name=previous_action_output_name,
            ),
            ReferenceInput(
                name="from_ref_addon",
                id="0",
                action_name=previous_action_name,
                output_name=previous_action_output_name,
            ),
        ],
        next_actions=[],
        is_root_action=False,
        connection_execution_id=None,
        position=None,
        addons={"http_service": {"ryax_endpoint_prefix": "DEFAULT"}},
        constraints=None,
        objectives=None,
    )

    previous_action = WorkflowAction(
        id="0",
        id_in_studio=previous_action_name,
        workflow_deployment_id="",
        workflow_definition_id="",
        definition=action_definition,
        static_inputs=[],
        reference_inputs=[],
        next_actions=[workflow_action],
        is_root_action=True,
        connection_execution_id=None,
        position=None,
    )

    execution_result = ExecutionResult(
        id="new_execution1",
        project_id="project_id",
        workflow_run_id="workflow_run_id",
        submitted_at=now - datetime.timedelta(minutes=2),
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.SUCCESS,
        execution_connection_id="1",
        workflow_action=previous_action,
        outputs={previous_action_output_name: "VALUE_FROM_REF"},
    )
    workflow = WorkflowDeployment(
        name="",
        id="1",
        workflow_definition_id="",
        project_id="",
        actions=[previous_action, workflow_action],
        creation_time=now,
    )
    solver = app_container.reference_solver()

    solver.uow.workflow_repository.get.return_value = workflow

    solved_inputs = solver.resolve_reference_inputs(execution_result, workflow_action)

    assert solved_inputs["from_static_inputs"] == "VALUE_FROM_STATIC_INPUT"
    assert solved_inputs["from_static_addon"] == "VALUE_FROM_STATIC_ADDON"
    assert solved_inputs["from_ref_inputs"] == "VALUE_FROM_REF"
    assert solved_inputs["from_ref_addon"] == "VALUE_FROM_REF"
    assert solved_inputs["ryax_endpoint_prefix"] == "SHOULD_BE_PRESENT"
    assert "base_url" in solved_inputs
    solver.filter_inputs(workflow_action, solved_inputs)
    assert "base_url" not in solved_inputs


def test_addons_reference_solver(app_container):
    now = datetime.datetime.now(datetime.timezone.utc)

    previous_action_name = "previous_action"
    previous_action_output_name = "prev_output"

    action_definition = ActionDefinition(
        id="action_def_id",
        project_id="test",
        technical_name="tech",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        container_image="cont",
        human_name="humname",
        description="desc",
        inputs=[
            ActionInput(
                id="0",
                name="to_be_filtered",
                type=ActionIOType.STRING,
                display_name="foo display",
                help="help",
            ),
        ],
        outputs=[
            ActionOutput(
                id="1",
                name=previous_action_output_name,
                type=ActionIOType.STRING,
                display_name="foo display",
                help="help",
            )
        ],
        addons_inputs=[
            ActionInput(
                id="1",
                name="port",
                type=ActionIOType.INTEGER,
                display_name="foo display",
                help="help",
                addon_name="hpc",
            ),
            ActionInput(
                id="2",
                name="frontend",
                type=ActionIOType.STRING,
                display_name="bar display",
                help="help",
                addon_name="hpc",
            ),
        ],
    )

    workflow_action = WorkflowAction(
        id="wfmod",
        id_in_studio="name",
        workflow_deployment_id="depl_id",
        workflow_definition_id="wfdef",
        definition=action_definition,
        static_inputs=[
            StaticInput(name="frontend", id="1", value="VALUE_FROM_STATIC_ADDON"),
        ],
        reference_inputs=[
            ReferenceInput(
                name="port",
                id="0",
                action_name=previous_action_name,
                output_name=previous_action_output_name,
            ),
        ],
        next_actions=[],
        is_root_action=False,
        connection_execution_id=None,
        position=None,
        addons={"hpc": {"frontend": "overrideme"}},
        constraints=None,
        objectives=None,
    )

    previous_action = WorkflowAction(
        id="0",
        id_in_studio=previous_action_name,
        workflow_deployment_id="",
        workflow_definition_id="",
        definition=action_definition,
        static_inputs=[],
        reference_inputs=[],
        next_actions=[workflow_action],
        is_root_action=True,
        connection_execution_id=None,
        position=None,
    )

    execution_result = ExecutionResult(
        id="new_execution1",
        project_id="project_id",
        workflow_run_id="workflow_run_id",
        submitted_at=now - datetime.timedelta(minutes=2),
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.SUCCESS,
        execution_connection_id="1",
        workflow_action=previous_action,
        outputs={previous_action_output_name: "VALUE_FROM_REF"},
    )
    execution_connection = ExecutionConnection(
        id="exec1",
        project_id=action_definition.project_id,
        workflow_action=workflow_action,
        workflow_run_id="1",
        parent_execution_result=execution_result,
        submitted_at=now,
        time_allotment=1,
    )
    solver = app_container.reference_solver()
    solver.uow.execution_connection_repository.get.return_value = execution_connection

    solved_addons = solver.resolve_addons(
        execution_connection_id=execution_connection.id
    )

    assert "port" not in solved_addons
    assert "frontend" not in solved_addons
    assert solved_addons["hpc"]["port"] == "VALUE_FROM_REF"
    assert solved_addons["hpc"]["frontend"] == "VALUE_FROM_STATIC_ADDON"
    assert "to_be_filtered" not in solved_addons


def test_addons_reference_solver_no_parent(app_container):
    datetime.datetime.now(datetime.timezone.utc)

    action_definition = ActionDefinition(
        id="action_def_id",
        project_id="test",
        technical_name="tech",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        container_image="cont",
        human_name="humname",
        description="desc",
        inputs=[
            ActionInput(
                id="0",
                name="to_be_filtered",
                type=ActionIOType.STRING,
                display_name="foo display",
                help="help",
            ),
        ],
        outputs=[],
        addons_inputs=[
            ActionInput(
                id="1",
                name="port",
                type=ActionIOType.INTEGER,
                display_name="foo display",
                help="help",
                addon_name="hpc",
            ),
            ActionInput(
                id="2",
                name="frontend",
                type=ActionIOType.STRING,
                display_name="bar display",
                help="help",
                addon_name="hpc",
            ),
        ],
    )
    workflow_action = WorkflowAction(
        id="wfmod",
        id_in_studio="name",
        workflow_deployment_id="depl_id",
        workflow_definition_id="wfdef",
        definition=action_definition,
        static_inputs=[
            StaticInput(name="frontend", id="1", value="VALUE_FROM_STATIC_ADDON"),
            StaticInput(name="port", id="2", value=1234),
        ],
        reference_inputs=[],
        next_actions=[],
        is_root_action=False,
        connection_execution_id=None,
        position=None,
        addons={"hpc": {}},
        constraints=None,
        objectives=None,
    )
    solver = app_container.reference_solver()
    solver.uow.workflow_repository.get_workflow_action_by_id.return_value = (
        workflow_action
    )

    solved_addons = solver.resolve_addons(workflow_action_id=workflow_action.id)

    assert solved_addons["hpc"]["port"] == 1234
    assert solved_addons["hpc"]["frontend"] == "VALUE_FROM_STATIC_ADDON"
