import pytest

from ryax.runner.application.site_placement_service import (
    SitePlacementService,
)
from ryax.common.domain.registration.registration_values import (
    SiteType,
    GPU_MODE_FULL,
)
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionResources,
)
from ryax.runner.domain.scheduler.scheduler_entities import PendingExecutionConnection
from ryax.runner.domain.scheduler.scheduler_exceptions import (
    NotEnoughResourcesException,
)

from ryax.runner.domain.site.site_entities import Site, NodePool
from tests.helper import now


def new_default_site(site_id="site1") -> Site:
    return Site(
        id=site_id,
        name="local site",
        type=SiteType.KUBERNETES,
        node_pools=[],
    )


def new_nodepool_with_gpu(gpu_mode: str, gpu_num: int, gpu_num_total: int) -> NodePool:
    return NodePool(
        id=NodePool.new_id(),
        name="mynodepool",
        site=new_default_site(),
        cpu=1,
        memory=1024 * 1024,
        gpu=gpu_num,
        gpu_mode=gpu_mode,
        gpu_num_total=gpu_num_total,
    )


def test_get_node_pool_largest_mig():
    nodepools_no_gpu = [
        new_nodepool_with_gpu(GPU_MODE_FULL, 0, 0),
        new_nodepool_with_gpu("mig-1g.10gb", 0, 0),
    ]
    with pytest.raises(NotEnoughResourcesException):
        SitePlacementService._get_node_pool_largest_mig(nodepools_no_gpu)

    nodepools_full_and_mig = [
        new_nodepool_with_gpu(GPU_MODE_FULL, 1, 0),
        new_nodepool_with_gpu("mig-1g.10gb", 8, 8),
    ]
    assert (
        SitePlacementService._get_node_pool_largest_mig(nodepools_full_and_mig)
        == nodepools_full_and_mig[0]
    )

    nodepools_different_mig = [
        new_nodepool_with_gpu("mig-1g.10gb", 8, 8),
        new_nodepool_with_gpu("mig-3g.40gb", 3, 2),
    ]
    assert (
        SitePlacementService._get_node_pool_largest_mig(nodepools_different_mig)
        == nodepools_different_mig[1]
    )

    nodepools_available_mig = [
        new_nodepool_with_gpu(GPU_MODE_FULL, 0, 0),
        new_nodepool_with_gpu("mig-3g.40gb", 0, 0),
        new_nodepool_with_gpu("mig-1g.10gb", 8, 8),
    ]
    assert (
        SitePlacementService._get_node_pool_largest_mig(nodepools_available_mig)
        == nodepools_available_mig[2]
    )


def test_get_smallest_suitable_mig_pool():
    # No satisfying pool at all
    nodepools_no_satisfying_gpu = [
        new_nodepool_with_gpu(GPU_MODE_FULL, 0, 0),
        new_nodepool_with_gpu("mig-3g.40gb", 0, 0),
        new_nodepool_with_gpu("mig-1g.10gb", 8, 8),
    ]
    with pytest.raises(NotEnoughResourcesException):
        SitePlacementService._get_smallest_suitable_mig_pool(
            nodepools_no_satisfying_gpu, "3g.40gb"
        )

    # Has satisfying pool but not free
    nodepools_no_satisfying_free_gpu = [
        new_nodepool_with_gpu(GPU_MODE_FULL, 1, 0),
        new_nodepool_with_gpu("mig-3g.40gb", 3, 0),
        new_nodepool_with_gpu("mig-1g.10gb", 8, 8),
    ]

    assert (
        SitePlacementService._get_smallest_suitable_mig_pool(
            nodepools_no_satisfying_free_gpu, "3g.40gb"
        )
        == nodepools_no_satisfying_free_gpu[1]
    )

    # Has satisfying pool and free
    nodepools_satisfying_free_gpu = [
        new_nodepool_with_gpu(GPU_MODE_FULL, 1, 0),
        new_nodepool_with_gpu("mig-3g.40gb", 3, 3),
        new_nodepool_with_gpu("mig-1g.10gb", 8, 8),
    ]
    assert (
        SitePlacementService._get_smallest_suitable_mig_pool(
            nodepools_satisfying_free_gpu, "3g.40gb"
        )
        == nodepools_satisfying_free_gpu[1]
    )


def test_schedule_one_filter_gpu_node_pool():
    no_gpu_site = new_default_site()
    gpu_site = new_default_site("gpu_site")
    node_pool_without_gpu = NodePool(
        id=NodePool.new_id(),
        name="mynodepool",
        cpu=1,
        memory=1024 * 1024,
        site=no_gpu_site,
    )
    no_gpu_site.node_pools.append(node_pool_without_gpu)
    node_pool_with_gpu = NodePool(
        id=NodePool.new_id(),
        name="mynodepool_gpu",
        cpu=1,
        memory=1024 * 1024,
        gpu=1,
        site=gpu_site,
    )
    gpu_site.node_pools.append(node_pool_with_gpu)
    pending_execution_connection = PendingExecutionConnection(
        id="2",
        action_definition_id="2",
        project_id="proj",
        is_a_trigger=False,
        submitted_at=now,
        action_container_image="myimg",
        resources=ActionResources(cpu=1, gpu=0, memory=2, id=1),
    )

    selected_node_pool = SitePlacementService().schedule_one(
        [gpu_site, no_gpu_site], {}, pending_execution_connection
    )
    assert selected_node_pool.id == no_gpu_site.node_pools[0].id

    # deactivate filter and select GPU nodepool because first in the list
    gpu_site.node_pools[0].filter_no_gpu_action = False
    selected_node_pool = SitePlacementService().schedule_one(
        [gpu_site, no_gpu_site], {}, pending_execution_connection
    )
    assert selected_node_pool.id == gpu_site.node_pools[0].id
