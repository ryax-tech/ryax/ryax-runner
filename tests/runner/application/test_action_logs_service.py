from unittest import mock

import pytest

from ryax.runner.application.action_logs_service import (
    END_LOG_DELIMITER_PREFIX,
    START_LOG_DELIMITER_PREFIX,
)
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionExecutionType,
)
from ryax.runner.domain.action_logs.action_logs_entities import ActionLogs
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from tests.unit_of_work_stub import UnitOfWorkStub


@pytest.fixture
def app_container():
    app_container = ApplicationContainer()
    app_container.configuration.deployment.logs.query_interval_in_seconds.from_value(1)
    app_container.configuration.deployment.logs.logs_max_size_in_bytes.from_value(
        1024 * 1024
    )
    app_container.configuration.deployment.logs.log_line_max_size_in_bytes.from_value(
        1000
    )

    uow: IUnitOfWork = UnitOfWorkStub()
    app_container.unit_of_work.override(uow)
    yield app_container
    app_container.unit_of_work = None
    app_container.unwire()


async def test_on_new_execution_log_line_when_start_and_end_in_logs(app_container):
    action_log_service = app_container.action_logs_service()

    delimiter = "1234"
    action_deployment_id = "test_id"
    actual_logs = "LOGS MTrhFKA\nYeah!\n"
    log_lines = START_LOG_DELIMITER_PREFIX + delimiter + "\n"
    log_lines += actual_logs
    log_lines += END_LOG_DELIMITER_PREFIX + delimiter + "\n"
    mocked_execution_result = mock.MagicMock(ExecutionResult, id="execid")

    uow = action_log_service.uow_factory()
    logs = ActionLogs.create(associated_id=action_deployment_id, project_id="")
    uow.action_logs_repository.get.return_value = logs
    uow.action_deployment_repository.get_by_id.return_value = mock.MagicMock(
        ActionDeployment,
        id=action_deployment_id,
        action_definition=mock.MagicMock(
            ActionDefinition, is_a_trigger=mock.MagicMock(return_value=False)
        ),
        project_id="",
    )
    uow.execution_connection_repository.list_log_delimiters_by_action_deployment.return_value = [
        delimiter
    ]
    uow.execution_result_repository.get.return_value = mocked_execution_result
    uow.execution_result_repository.get_ids_by_end_of_log_delimiter.return_value = (
        "execution_result_id",
        "execution_result_execution_connection_id",
    )

    # Actual test
    action_log_service.watch_action_deployment(action_deployment_id)
    assert action_log_service.currently_watching_actions[action_deployment_id] is None

    await action_log_service.new_action_deployment_log_lines(
        log_lines, action_deployment_id
    )

    action_log_service.unwatch_action_deployment(action_deployment_id)
    assert action_deployment_id not in action_log_service.currently_watching_actions
    assert str(logs) == actual_logs


# Tests without start log delimiter
async def test_on_new_execution_log_line_put_in_cache(app_container):
    action_log_service = app_container.action_logs_service()
    action_deployment_id = "test_id1"
    log_lines = "foo\nbar\nbaz"
    action_deployment = mock.MagicMock(
        ActionDeployment,
        id=action_deployment_id,
        action_definition=mock.MagicMock(
            ActionDefinition, is_a_trigger=mock.MagicMock(return_value=False)
        ),
        project_id="",
    )

    uow = action_log_service.uow_factory()
    logs = ActionLogs.create(associated_id=action_deployment_id, project_id="")
    uow.action_logs_repository.get.return_value = logs
    uow.execution_connection_repository.list_log_delimiters_by_action_deployment.return_value = (
        []
    )
    uow.action_deployment_repository.get_by_id.return_value = action_deployment

    await action_log_service.new_action_deployment_log_lines(
        log_lines, action_deployment_id
    )
    uow.action_deployment_repository.get_by_id.assert_called_once_with(
        action_deployment_id
    )

    uow.commit.assert_called_once()
    assert str(logs) == log_lines


async def test_on_new_execution_log_line_when_no_logs(app_container):
    action_log_service = app_container.action_logs_service()
    action_deployment_id = "test_id2"
    log_lines = ""

    uow = action_log_service.uow_factory()
    await action_log_service.new_action_deployment_log_lines(
        log_lines, action_deployment_id
    )
    uow.action_deployment_repository.get_by_id.assert_not_called()
    uow.execution_connection_repository.list_log_delimiters_by_action_deployment.assert_not_called()
    uow.commit.assert_not_called()


async def test_on_new_execution_log_line_when_one_delimiter_in_logs(app_container):
    action_log_service = app_container.action_logs_service()
    action_deployment_id = "test_id3"
    log_delimiter_in_log = END_LOG_DELIMITER_PREFIX + "RYAX_TEST_LOG_DELIMITER"
    log_lines = f"foo\n{log_delimiter_in_log}\nbaz\n"
    action_deployment = ActionDeployment(
        id=action_deployment_id,
        project_id="",
        action_definition=mock.MagicMock(
            ActionDefinition, is_a_trigger=mock.MagicMock(return_value=False)
        ),
        execution_type=ActionExecutionType.NONE,
        node_pool=None,
    )
    execution_result = mock.MagicMock(
        ExecutionResult, execution_connection_id="toto", id="execid"
    )

    uow = action_log_service.uow_factory()
    logs_map = {
        action_deployment_id: ActionLogs.create(
            associated_id=action_deployment_id, project_id=""
        ),
        execution_result.id: ActionLogs.create(
            associated_id=action_deployment_id, project_id=""
        ),
    }
    uow.action_logs_repository.get.side_effect = lambda x: logs_map[x]
    uow.action_logs_repository.get_logs_as_string.side_effect = lambda x: str(
        logs_map[x]
    )
    uow.action_logs_repository.reset.side_effect = lambda x: logs_map[
        x
    ].log_lines.clear()

    uow.action_deployment_repository.get_by_id.return_value = action_deployment
    uow.execution_connection_repository.list_log_delimiters_by_action_deployment.return_value = [
        log_delimiter_in_log,
        "OTHER LOG DELIMITER",
    ]
    uow.execution_result_repository.get_ids_by_end_of_log_delimiter.return_value = (
        execution_result.id,
        execution_result.execution_connection_id,
    )

    await action_log_service.new_action_deployment_log_lines(
        log_lines, action_deployment_id
    )

    uow.action_deployment_repository.get_by_id.assert_called_once_with(
        action_deployment_id
    )
    uow.execution_connection_repository.list_log_delimiters_by_action_deployment.assert_called_once_with(
        action_deployment
    )
    uow.execution_result_repository.get_ids_by_end_of_log_delimiter.assert_called_once_with(
        log_delimiter_in_log
    )

    uow.commit.assert_called_once()
    assert str(logs_map[action_deployment_id]) == "baz\n"
    assert str(logs_map[execution_result.id]) == "foo\n"
