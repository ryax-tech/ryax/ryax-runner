import datetime
from typing import Any, List, Optional
from unittest import mock
from unittest.mock import call

import pytest

from ryax.runner.application.reference_solver_service import ReferenceSolverService
from ryax.common.domain.registration.registration_values import (
    SiteType,
    NodeArchitecture,
)
from ryax.runner.application.scheduler import MultiObjectiveScheduler
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    CreateActionDeploymentCommand,
    UndeployActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    IActionDeploymentRepository,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.addons.addon_entities import RyaxAddOns
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    AllocateExecutionConnectionCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_repository import (
    IExecutionConnectionRepository,
)
from ryax.common.domain.internal_messaging.base_messages import BaseCommand
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.scheduler.scheduler_entities import (
    PendingExecutionConnection,
    PlatformStateSnapshot,
)
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.runner.domain.site.site_repository import ISiteRepository
from ryax.runner.domain.workflow_deployment.workflow_deployment_repository import (
    IWorkflowDeploymentRepository,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_repository import (
    IRecommendationVPARepository,
)
from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)


@pytest.fixture()
def app() -> ApplicationContainer:
    app = ApplicationContainer()

    message_bus = mock.MagicMock(IMessageBus)

    def handle_command(command: BaseCommand) -> Any:
        if isinstance(command, CreateActionDeploymentCommand):
            return f"NEW_{command.action_definition_id}"
        elif isinstance(command, AllocateExecutionConnectionCommand):
            return None
        elif isinstance(command, UndeployActionDeploymentCommand):
            return None
        else:
            raise Exception("This command is not understood by the mock object.")

    message_bus.handle_command.side_effect = handle_command
    app.message_bus.override(message_bus)

    uow = mock.MagicMock(IUnitOfWork)
    uow.__enter__ = lambda self: self
    uow.execution_connection_repository = mock.MagicMock(IExecutionConnectionRepository)
    uow.action_deployment_repository = mock.MagicMock(IActionDeploymentRepository)
    uow.action_definition_repository = mock.MagicMock(IActionDefinitionRepository)
    uow.workflow_repository = mock.MagicMock(IWorkflowDeploymentRepository)
    uow.site_repository = mock.MagicMock(ISiteRepository)
    uow.recommendation_vpa_repository = mock.MagicMock(IRecommendationVPARepository)

    app.unit_of_work.override(uow)

    solver = mock.MagicMock(ReferenceSolverService)
    solver.resolve_addons.return_value = {}
    scheduler = MultiObjectiveScheduler(
        message_bus=message_bus,
        uow_factory=lambda: uow,
        addons=mock.AsyncMock(RyaxAddOns),
        site_placement_service=app.site_placement_service(),
    )
    app.scheduler.override(scheduler)
    return app


def _generate_action_def(id_: str, kind: ActionKind) -> ActionDefinition:
    return ActionDefinition(
        id=id_,
        project_id="project_id",
        technical_name=f"{id_}_tname",
        version="1.0",
        kind=kind,
        container_image="container_image",
        human_name="human_name",
        description="description",
        inputs=[],
        outputs=[],
        addons_inputs=[],
    )


def _generate_site():
    site = Site(
        id="site1",
        name="local site",
        type=SiteType.KUBERNETES,
    )
    site.node_pools = [NodePool(site=site, id="1", memory=1024, cpu=2, name="toto")]
    return site


def _generate_pending_execution_connection(
    id_: str, kind: ActionKind, action_definition_id: Optional[str] = None
) -> PendingExecutionConnection:
    site = _generate_site()
    site.node_pools.append(
        NodePool(site=site, id="np2", memory=8024, cpu=12, name="np2")
    )

    site2 = Site(id="2", name="test_site2", type=SiteType.KUBERNETES)
    site2.node_pools = [
        NodePool(site=site2, id="nps2", memory=1024, cpu=2, name="nps2")
    ]
    return PendingExecutionConnection(
        id=id_,
        action_definition_id=(
            action_definition_id
            if action_definition_id is not None
            else id_ + "_moddef"
        ),
        submitted_at=datetime.datetime.fromtimestamp(0),
        action_container_image="action_container_image",
        project_id="project_id",
        addons={},
        is_a_trigger=ActionKind.TRIGGER == kind,
        arch_list=[NodeArchitecture.X86_64],
        site_list=[site],
        node_pool_list=site.node_pools.extend(site2.node_pools),
    )


def _generate_action_depl(
    id_: str,
    kind: ActionKind,
    action_def: Optional[ActionDefinition] = None,
    node_pool: NodePool = None,
) -> ActionDeployment:
    if action_def is None:
        action_def = _generate_action_def(f"{id_}_moddef", kind)

    return ActionDeployment(
        id=id_,
        project_id="project_id",
        action_definition=action_def,
        state=ActionDeploymentState.READY,
        allocated_connection_id=None,
        execution_type=ActionExecutionType.GRPC_V1,
        node_pool=node_pool,
    )


async def test_scheduler_empty(app: ApplicationContainer) -> None:
    await app.scheduler().schedule()

    app.unit_of_work().execution_connection_repository.list_pending.assert_called_once()
    app.message_bus().handle.assert_not_called()
    app.message_bus().handle_command.assert_not_called()
    app.message_bus().handle_event.assert_not_called()


async def test_scheduler_one_pending_processor(app: ApplicationContainer) -> None:
    uow = app.unit_of_work()
    scheduler = app.scheduler()
    message_bus = app.message_bus()

    exec_trig = _generate_pending_execution_connection(
        "exectrig1", ActionKind.PROCESSOR
    )
    uow.execution_connection_repository.list_pending.return_value = [exec_trig]
    uow.action_deployment_repository.list_unallocated_and_ready_from_action_definition_id.return_value = (
        []
    )
    uow.action_deployment_repository.list_eventually_ready_from_action_definition_id.return_value = (
        []
    )
    uow.action_deployment_repository.list_unallocated.return_value = []
    uow.action_deployment_repository.count_eventually_ready.return_value = 0
    site = _generate_site()
    uow.site_repository.list.return_value = [site]

    await scheduler.schedule()

    uow.execution_connection_repository.list_pending.assert_called_once()
    assert message_bus.handle_command.mock_calls == [
        call(
            CreateActionDeploymentCommand(
                project_id="project_id",
                action_definition_id="exectrig1_moddef",
                node_pool_id=site.node_pools[0].id,
                execution_connection_id="exectrig1",
            )
        ),
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exectrig1",
                action_deployment_id="NEW_exectrig1_moddef",
            )
        ),
    ]


async def test_scheduler_one_pending_trigger(app: ApplicationContainer) -> None:
    uow = app.unit_of_work()
    scheduler = app.scheduler()
    message_bus = app.message_bus()

    exec_trig = _generate_pending_execution_connection("exectrig1", ActionKind.TRIGGER)
    uow.execution_connection_repository.list_pending.return_value = [exec_trig]
    uow.action_deployment_repository.list_unallocated_and_ready_from_action_definition_id.return_value = (
        []
    )
    uow.action_deployment_repository.list_eventually_ready_from_action_definition_id.return_value = (
        []
    )
    uow.action_deployment_repository.list_unallocated.return_value = []
    site = _generate_site()
    uow.site_repository.list.return_value = [site]

    await scheduler.schedule()

    uow.execution_connection_repository.list_pending.assert_called_once()
    assert message_bus.handle_command.mock_calls == [
        call(
            CreateActionDeploymentCommand(
                project_id="project_id",
                action_definition_id="exectrig1_moddef",
                node_pool_id=site.node_pools[0].id,
                execution_connection_id="exectrig1",
            )
        ),
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exectrig1",
                action_deployment_id="NEW_exectrig1_moddef",
            )
        ),
    ]


async def test_scheduler_many_triggers(app: ApplicationContainer) -> None:
    """
    In this test, we have 3 pending triggers ; 2 with the same action definition.
    We also have 2 ActionDeployment ready to be used: 2 triggers, one useless, and one with the same action def as
    before.
    """
    uow = app.unit_of_work()
    scheduler = app.scheduler()
    message_bus = app.message_bus()

    site = _generate_site()
    uow.site_repository.list.return_value = [site]

    common_modul_def = _generate_action_def("common_modul_def", ActionKind.TRIGGER)
    exec_trig_trigger1 = _generate_pending_execution_connection(
        "exec_trig_trigger1", ActionKind.TRIGGER, common_modul_def.id
    )
    exec_trig_trigger2 = _generate_pending_execution_connection(
        "exec_trig_trigger2", ActionKind.TRIGGER, common_modul_def.id
    )
    exec_trig_trigger3 = _generate_pending_execution_connection(
        "exec_trig_trigger3", ActionKind.TRIGGER
    )
    uow.execution_connection_repository.list_pending.return_value = [
        exec_trig_trigger1,
        exec_trig_trigger2,
        exec_trig_trigger3,
    ]

    all_modul_def = {
        common_modul_def.id: [
            _generate_action_depl(
                "m_depl_useful",
                ActionKind.TRIGGER,
                common_modul_def,
                site.node_pools[0],
            ),
        ],
        "m_depl_useless_moddef": [
            _generate_action_depl(
                "m_depl_useless", ActionKind.TRIGGER, node_pool=site.node_pools[0]
            ),
        ],
    }

    def list_ready_to_be_used_by_action_definition(
        action_definition_id: str, project_id: str, node_pool_id: str
    ) -> List[ActionDeployment]:
        if (
            action_definition_id == common_modul_def.id
            and action_definition_id in all_modul_def
        ):
            x = all_modul_def[action_definition_id]
            del all_modul_def[action_definition_id]
            return x
        return all_modul_def.get(action_definition_id, [])

    uow.action_deployment_repository.list_unallocated_and_ready_from_action_definition_id.side_effect = (
        list_ready_to_be_used_by_action_definition
    )
    uow.action_deployment_repository.list_eventually_ready_from_action_definition_id.return_value = (
        []
    )
    uow.action_deployment_repository.list_unallocated.return_value = all_modul_def[
        "m_depl_useless_moddef"
    ]

    await scheduler.schedule()

    uow.execution_connection_repository.list_pending.assert_called_once()
    assert message_bus.handle_command.mock_calls == [
        # allocate exec_trig_trigger1 on m_depl_useful
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exec_trig_trigger1",
                action_deployment_id="m_depl_useful",
            )
        ),
        # deploy & allocate exec_trig_trigger2
        call(
            CreateActionDeploymentCommand(
                project_id="project_id",
                action_definition_id="common_modul_def",
                node_pool_id=site.node_pools[0].id,
                execution_connection_id="exec_trig_trigger2",
            )
        ),
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exec_trig_trigger2",
                action_deployment_id="NEW_common_modul_def",
            )
        ),
        # deploy & allocate exec_trig_trigger3
        call(
            CreateActionDeploymentCommand(
                project_id="project_id",
                action_definition_id="exec_trig_trigger3_moddef",
                node_pool_id=site.node_pools[0].id,
                execution_connection_id="exec_trig_trigger3",
            )
        ),
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exec_trig_trigger3",
                action_deployment_id="NEW_exec_trig_trigger3_moddef",
            )
        ),
        # undeploy m_depl_useless
        call(UndeployActionDeploymentCommand(action_deployment_id="m_depl_useless")),
    ]


async def test_scheduler_many_actions(app: ApplicationContainer) -> None:
    """
    In this test, we have 3 pending actions ; 2 with the same action definition.
    Because the max number of action is not reached, it deploys all actions.

    We also have 2 ActionDeployment ready to be used: 2 triggers, one useless, and one with the same action def as
    before.
    """
    uow = app.unit_of_work()
    scheduler = app.scheduler()
    message_bus = app.message_bus()

    site = _generate_site()
    uow.site_repository.list.return_value = [site]

    common_action_def = _generate_action_def("common_action_def", ActionKind.PROCESSOR)
    exec_conn1 = _generate_pending_execution_connection(
        "exec_conn1", ActionKind.PROCESSOR, common_action_def.id
    )
    exec_conn2 = _generate_pending_execution_connection(
        "exec_conn2", ActionKind.PROCESSOR, common_action_def.id
    )
    exec_conn3 = _generate_pending_execution_connection(
        "exec_conn3", ActionKind.PROCESSOR
    )
    uow.execution_connection_repository.list_pending.return_value = [
        exec_conn1,
        exec_conn2,
        exec_conn3,
    ]

    all_action_deployments = {
        common_action_def.id: [
            _generate_action_depl(
                "m_depl_useful",
                ActionKind.PROCESSOR,
                common_action_def,
                site.node_pools[0],
            ),
        ],
        "m_depl_useless_moddef": [
            _generate_action_depl(
                "m_depl_useless", ActionKind.PROCESSOR, node_pool=site.node_pools[0]
            ),
        ],
    }

    class BOOL:
        v: bool

    already_returned = BOOL()
    already_returned.v = False

    scheduler.max_action_deployments = 5
    scheduler._platform_state = PlatformStateSnapshot()
    scheduler.update_action_deployments_state = mock.MagicMock()

    def update_platform_state_stub(
        pending_connection: PendingExecutionConnection,
        refresh_cache: bool,
        node_pool: str,
    ) -> List[ActionDeployment]:
        if scheduler._platform_state is None:
            scheduler._platform_state = PlatformStateSnapshot()
        if (
            pending_connection.action_definition_id == common_action_def.id
            and already_returned.v is True
        ):
            action_deployments = all_action_deployments[
                pending_connection.action_definition_id
            ]
            action_deployments[0].state = ActionDeploymentState.RUNNING
            scheduler._platform_state.action_deployments_by_definition_id[
                site.node_pools[0].id, pending_connection.action_definition_id
            ] = action_deployments
        else:
            already_returned.v = True
            scheduler._platform_state.action_deployments_by_definition_id[
                site.node_pools[0].id, pending_connection.action_definition_id
            ] = all_action_deployments.get(pending_connection.action_definition_id, [])

    scheduler.update_action_deployments_state.side_effect = update_platform_state_stub

    uow.action_deployment_repository.list_unallocated_and_ready_from_action_definition_id.return_value = (
        []
    )
    uow.action_deployment_repository.list_unallocated.return_value = (
        all_action_deployments["m_depl_useless_moddef"]
    )
    await scheduler.schedule()

    uow.execution_connection_repository.list_pending.assert_called_once()
    assert message_bus.handle_command.mock_calls == [
        # allocate exec_conn1 on m_depl_useful
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exec_conn1",
                action_deployment_id="m_depl_useful",
            )
        ),
        # Deploy a new action for exec_conn2
        call(
            CreateActionDeploymentCommand(
                project_id="project_id",
                action_definition_id="common_action_def",
                node_pool_id=site.node_pools[0].id,
                execution_connection_id="exec_conn2",
            )
        ),
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exec_conn2",
                action_deployment_id="NEW_common_action_def",
            )
        ),
        # deploy & allocate exec_conn3
        call(
            CreateActionDeploymentCommand(
                project_id="project_id",
                action_definition_id="exec_conn3_moddef",
                node_pool_id=site.node_pools[0].id,
                execution_connection_id="exec_conn3",
            )
        ),
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exec_conn3",
                action_deployment_id="NEW_exec_conn3_moddef",
            )
        ),
        # undeploy m_depl_useless
        call(UndeployActionDeploymentCommand(action_deployment_id="m_depl_useless")),
    ]


async def test_scheduler_many_actions_limit_reached(app: ApplicationContainer) -> None:
    """
    In this test, we have 3 pending actions ; 2 with the same action definition.
    Because the max number of action is reached, it should wait and do nothing.

    We also have 2 ActionDeployment ready to be used: 2 triggers, one useless, and one with the same action def as
    before.
    """
    uow = app.unit_of_work()
    scheduler = app.scheduler()
    message_bus = app.message_bus()

    site = _generate_site()
    uow.site_repository.list.return_value = [site]

    common_modul_def = _generate_action_def("common_modul_def", ActionKind.PROCESSOR)
    exec_conn1 = _generate_pending_execution_connection(
        "exec_conn1", ActionKind.PROCESSOR, common_modul_def.id
    )
    exec_conn2 = _generate_pending_execution_connection(
        "exec_conn2", ActionKind.PROCESSOR, common_modul_def.id
    )
    exec_conn3 = _generate_pending_execution_connection(
        "exec_conn3", ActionKind.PROCESSOR
    )
    uow.execution_connection_repository.list_pending.return_value = [
        exec_conn1,
        exec_conn2,
        exec_conn3,
    ]

    all_action_deployments = {
        common_modul_def.id: [
            _generate_action_depl(
                "m_depl_useful",
                ActionKind.PROCESSOR,
                common_modul_def,
                node_pool=site.node_pools[0],
            ),
        ],
        "m_depl_useless_moddef": [
            _generate_action_depl(
                "m_depl_useless", ActionKind.PROCESSOR, node_pool=site.node_pools[0]
            ),
        ],
    }

    class BOOL:
        v: bool

    already_returned = BOOL()
    already_returned.v = False

    uow.action_deployment_repository.list_unallocated_and_ready_from_action_definition_id.return_value = (
        []
    )
    uow.action_deployment_repository.list_unallocated.return_value = (
        all_action_deployments["m_depl_useless_moddef"]
    )

    scheduler.max_action_deployments = 2
    scheduler.update_action_deployments_state = mock.MagicMock()

    def update_platform_state_stub(
        pending_connection: PendingExecutionConnection,
        refresh_cache: bool,
        nodepool: str,
    ) -> List[ActionDeployment]:
        if scheduler._platform_state is None:
            scheduler._platform_state = PlatformStateSnapshot(
                nb_actions_eventually_deployed=2
            )
        if (
            pending_connection.action_definition_id == common_modul_def.id
            and already_returned.v is True
        ):
            action_deployments = all_action_deployments[
                pending_connection.action_definition_id
            ]
            action_deployments[0].state = ActionDeploymentState.RUNNING
            scheduler._platform_state.action_deployments_by_definition_id[
                site.node_pools[0].id, pending_connection.action_definition_id
            ] = action_deployments
        else:
            already_returned.v = True
            scheduler._platform_state.action_deployments_by_definition_id[
                site.node_pools[0].id, pending_connection.action_definition_id
            ] = all_action_deployments.get(pending_connection.action_definition_id, [])

    scheduler.update_action_deployments_state.side_effect = update_platform_state_stub

    await scheduler.schedule()

    uow.execution_connection_repository.list_pending.assert_called_once()
    assert message_bus.handle_command.mock_calls == [
        # allocate exec_conn1 on m_depl_useful
        call(
            AllocateExecutionConnectionCommand(
                execution_connection_id="exec_conn1",
                action_deployment_id="m_depl_useful",
            )
        ),
        # do nothing for exec_conn2
        # and exec_conn3
        # undeploy m_depl_useless
        call(UndeployActionDeploymentCommand(action_deployment_id="m_depl_useless")),
    ]
