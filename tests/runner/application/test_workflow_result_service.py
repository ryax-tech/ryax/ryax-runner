from ryax.runner.application.workflow_result_service import WorkflowResultService
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_values import ActionIOType
from ryax.runner.domain.workflow_result.workflow_result import WorkflowResultView


def test_convert_to_json(app_container: ApplicationContainer):
    workflow_results = [
        WorkflowResultView(key="toto", value="tutu", type=ActionIOType.STRING)
    ]
    converted = WorkflowResultService._convert_to_json(workflow_results)
    assert converted == {"toto": "tutu"}

    workflow_results = [
        WorkflowResultView(key="toto", value=1, type=ActionIOType.INTEGER)
    ]
    converted = WorkflowResultService._convert_to_json(workflow_results)
    assert converted == {"toto": 1}

    workflow_results = [
        WorkflowResultView(key="toto", value=True, type=ActionIOType.BOOLEAN)
    ]
    converted = WorkflowResultService._convert_to_json(workflow_results)
    assert converted == {"toto": True}

    workflow_results = [
        WorkflowResultView(
            key="toto", value='{"tata": "tutu"}', type=ActionIOType.STRING
        )
    ]
    converted = WorkflowResultService._convert_to_json(workflow_results)
    assert converted == {"toto": {"tata": "tutu"}}

    workflow_results = [
        WorkflowResultView(
            key="toto", value='["tata", "tutu"]', type=ActionIOType.STRING
        )
    ]
    converted = WorkflowResultService._convert_to_json(workflow_results)
    assert converted == {"toto": ["tata", "tutu"]}
