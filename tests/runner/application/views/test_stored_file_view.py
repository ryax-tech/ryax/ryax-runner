from unittest import mock

from ryax.runner.application.views.stored_file_view import StoredFileView
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.stored_file.stored_file import StoredFile


class TestStoredFileView:
    async def test_download_file(self, app_container: ApplicationContainer):
        stored_file_path = "test/path"

        stored_file_view: StoredFileView = app_container.stored_file_view()
        stored_file_view.local_storage_service.read = mock.AsyncMock(return_value=None)
        assert await stored_file_view.download_file(stored_file_path) is None
        stored_file_view.local_storage_service.read.assert_called_once_with(
            stored_file_path
        )

    async def test_zip(self, app_container: ApplicationContainer):
        stored_file_1 = StoredFile(
            id="id1",
            io_name="",
            project_id="project_id",
            file_path="path",
            encoding=None,
            mimetype="",
            size_in_bytes=1,
        )
        stored_file_2 = StoredFile(
            id="id2",
            io_name="",
            project_id="project_id",
            file_path="path",
            encoding=None,
            mimetype="",
            size_in_bytes=1,
        )
        stored_file_view: StoredFileView = app_container.stored_file_view()
        result = await stored_file_view.zip([stored_file_1, stored_file_2])
        assert result.project_id == stored_file_2.project_id == stored_file_1.project_id
