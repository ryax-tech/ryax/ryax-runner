from unittest import mock

import pytest

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.common.domain.storage.storage_service import IStorageService
from tests.unit_of_work_stub import UnitOfWorkStub


@pytest.fixture
def app_container() -> ApplicationContainer:
    app_container = ApplicationContainer()

    # override services in the application container
    storage_service = mock.MagicMock(IStorageService)
    app_container.local_storage_service.override(storage_service)

    # Override unit of work
    unit_of_work: IUnitOfWork = UnitOfWorkStub()
    app_container.unit_of_work.override(unit_of_work)

    yield app_container
    app_container.unwire()
