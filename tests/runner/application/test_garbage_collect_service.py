import asyncio
from unittest import mock

from ryax.runner.container import ApplicationContainer


async def test_run_when_called(app_container: ApplicationContainer):
    runs = "1", "2", "3"
    app_container.configuration.max_number_of_runs_per_project.from_value(1)
    app_container.configuration.delay_between_calls_in_seconds.from_value(2)

    gc_service = app_container.garbage_collector_service()
    gc_service.delete_runs = mock.AsyncMock()
    gc_service.remove_old_runs = mock.AsyncMock()

    gc_queue = app_container.garbage_collector_queue()

    task = asyncio.create_task(gc_service.run())

    gc_queue.put(runs)

    await task

    gc_service.delete_runs.assert_called_once_with(runs)
    gc_service.remove_old_runs.assert_not_called()

    task.cancel()


async def test_run_when_timeout(app_container: ApplicationContainer):
    app_container.configuration.max_number_of_runs_per_project.from_value(1)
    app_container.configuration.delay_between_calls_in_seconds.from_value(0.1)

    gc_service = app_container.garbage_collector_service()
    gc_service.delete_runs = mock.AsyncMock()
    gc_service.remove_old_runs = mock.AsyncMock()

    task = asyncio.create_task(gc_service.run())

    await task

    gc_service.delete_runs.assert_not_called()
    gc_service.remove_old_runs.assert_called_once()
