import pytest
from sqlalchemy.orm import clear_mappers, close_all_sessions

from ryax.runner.container import ApplicationContainer
from ryax.runner.application.setup import setup as application_setup
from ryax.runner.infrastructure.database.metadata import metadata


@pytest.fixture
def app_container() -> ApplicationContainer:
    app_container = ApplicationContainer()
    app_container.configuration.database.url.from_env(
        "RYAX_WORKER_DATABASE_URL", required=True
    )
    application_setup(app_container)
    engine = app_container.database_engine()
    engine.connect()

    yield app_container

    clear_mappers()
    close_all_sessions()
    metadata.drop_all(engine._engine)
    engine.disconnect()
    app_container.unwire()
