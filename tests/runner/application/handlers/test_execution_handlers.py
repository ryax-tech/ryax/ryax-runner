from unittest import mock

from ryax.addons.ryax_http_service import HttpServiceAddOn
from ryax.runner.application.handlers.execution_connection_handlers import (
    create,
    _is_gpugi_value_smaller_than_by_memory,
    _get_max_retry_memory_bytes_and_gpu_gi_from_sites,
)
from ryax.common.application.message_bus import MessageBus
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionInput,
)
from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    IActionDeploymentRepository,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.execution_connection.execution_connection_commands import (
    CreateExecutionConnectionCommand,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_repository import (
    IExecutionConnectionRepository,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.execution_result.execution_result_repository import (
    IExecutionResultRepository,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_repository import (
    IWorkflowDeploymentRepository,
)
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.common.domain.registration.registration_values import SiteType


@mock.patch.object(ExecutionConnection, "create_from_run_execution_command")
async def test_create(execution_factory, app_container):
    action_input_types = [
        ActionInput.create_from_data(
            name="input_name",
            type=ActionIOType.STRING,
            display_name="input_id",
            help="help",
            enum_values=[],
            optional=False,
        )
    ]
    action_deployment = mock.MagicMock(
        ActionDeployment, id="1", action_input_types=action_input_types
    )
    workflow_deployment = mock.MagicMock(WorkflowDeployment)
    command = CreateExecutionConnectionCommand(
        project_id="",
        action_definition_id="",
        workflow_action_id="",
    )
    message_bus = MessageBus()

    execution = mock.MagicMock(
        ExecutionConnection,
        id="toto",
        action_deployment=action_deployment,
        project_id="",
        inputs={},
        events=[],
        state=ExecutionConnectionState.PENDING,
        workflow_action=mock.MagicMock(
            WorkflowAction, workflow_definition_id="wfdef_id"
        ),
    )
    execution.set_pending = mock.MagicMock()
    execution_factory.return_value = execution
    workflow_action = mock.MagicMock(WorkflowAction, id="1", addons={})

    uow: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    uow.action_deployment_repository = mock.MagicMock(IActionDeploymentRepository)
    uow.action_deployment_repository.get_by_id.return_value = action_deployment
    uow.execution_connection_repository = mock.MagicMock(IExecutionConnectionRepository)
    uow.execution_result_repository = mock.MagicMock(IExecutionResultRepository)
    uow.action_definition_repository = mock.MagicMock(IActionDefinitionRepository)
    uow.workflow_repository = mock.MagicMock(IWorkflowDeploymentRepository)
    uow.workflow_repository.get_workflow_action_by_id.return_value = workflow_action
    uow.workflow_repository.get.return_value = workflow_deployment
    app_container.unit_of_work.override(uow)

    addons = app_container.addons()
    addons.register(HttpServiceAddOn())

    await create(
        command,
        message_bus,
        uow,
        addons=addons,
        reference_solver=app_container.reference_solver(),
    )

    execution_factory.assert_called_once()
    execution.set_pending.assert_called_once()


def test_is_gpugi_value_smaller_than_by_memory():
    assert _is_gpugi_value_smaller_than_by_memory("1g.10gb", "3g.40gb")
    assert _is_gpugi_value_smaller_than_by_memory("8g.10gb", "3g.40gb")
    assert not _is_gpugi_value_smaller_than_by_memory("7g.80gb", "3g.40gb")
    assert not _is_gpugi_value_smaller_than_by_memory("3g.40gb", "3g.40gb")
    assert _is_gpugi_value_smaller_than_by_memory("", "1g.10gb")
    assert not _is_gpugi_value_smaller_than_by_memory("", "")


def test_get_max_retry_memory_bytes_and_gpu_gi_from_sites():
    sites = [
        Site(
            id="1",
            name="site1",
            type=SiteType.KUBERNETES,
            node_pools=[
                NodePool(
                    gpu_mode="mig-1g.10gb",  # Useful
                    memory=100,  # Useful
                    id="1",
                    name="nodepool1",
                    cpu=1,
                    gpu=1,
                    site=Site("1", "site1", SiteType.KUBERNETES),
                ),
                NodePool(
                    gpu_mode="full",  # Useful
                    memory=200,  # Useful
                    id="2",
                    name="nodepool2",
                    cpu=1,
                    gpu=1,
                    site=Site("1", "site1", SiteType.KUBERNETES),
                ),
            ],
        ),
        Site(
            id="3",
            name="site2",
            type=SiteType.KUBERNETES,
            node_pools=[
                NodePool(
                    gpu_mode="mig-7g.80gb",  # Useful
                    memory=300,  # Useful
                    id="3",
                    name="nodepool3",
                    cpu=1,
                    gpu=1,
                    site=Site("1", "site1", SiteType.KUBERNETES),
                ),
                NodePool(
                    gpu_mode="full",  # Useful
                    memory=400,  # Useful
                    id="4",
                    name="nodepool4",
                    cpu=1,
                    gpu=1,
                    site=Site("1", "site1", SiteType.KUBERNETES),
                ),
            ],
        ),
    ]
    assert _get_max_retry_memory_bytes_and_gpu_gi_from_sites(sites) == (
        400,
        "mig-7g.80gb",
    )
