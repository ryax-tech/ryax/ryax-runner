from datetime import datetime
from unittest import mock

from ryax.runner.application.handlers.workflow_deployment_handlers import (
    create,
    start,
)
from ryax.common.application.message_bus import MessageBus
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.exceptions import WorkflowNotFoundError
from ryax.runner.domain.workflow_action.workflow_action import (
    WorkflowAction,
    SchedulingConstraints,
    SchedulingObjectives,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
    StartWorkflowDeploymentCommand,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_repository import (
    IWorkflowDeploymentRepository,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)


@mock.patch.object(WorkflowDeployment, "create_from_create_command")
async def test_workflow_creation(workflow_actions) -> None:
    command = CreateWorkflowDeploymentCommand(
        project_id="",
        workflow_name="1",
        actions=[
            CreateWorkflowDeploymentCommand.Action(
                action_definition_id="1",
                static_inputs={"a": "A"},
                reference_inputs={},
                id_in_studio="toto",
                constraints=SchedulingConstraints(
                    arch_list=[],
                    site_list=[],
                    site_type_list=[],
                    node_pool_list=[],
                ),
                objectives=SchedulingObjectives(cost=0.1, energy=0.2, performance=0.3),
            ),
            CreateWorkflowDeploymentCommand.Action(
                action_definition_id="2",
                static_inputs={},
                reference_inputs={"1": {"output": "toto", "action": "toto"}},
                id_in_studio="",
                constraints=SchedulingConstraints(
                    arch_list=[],
                    site_list=[],
                    site_type_list=[],
                    node_pool_list=[],
                ),
                objectives=SchedulingObjectives(cost=0.1, energy=0.2, performance=0.3),
            ),
        ],
        workflow_definition_id="workflow_definition_id",
        results=[
            CreateWorkflowDeploymentCommand.Result(
                key="foo",
                technical_name="bar",
                workflow_action_io_id="id",
                action_id="1",
            )
        ],
    )

    workflow = mock.MagicMock(WorkflowDeployment, id="1", events=[])
    workflow.name = ""
    workflow.workflow_definition_id = "def_id"
    workflow.project_id = "123"
    workflow.state = WorkflowDeploymentState.NONE

    WorkflowDeployment.create_from_create_command.return_value = workflow
    print(workflow.name)
    message_bus = mock.MagicMock(MessageBus)

    uow: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    uow.workflow_repository = mock.MagicMock(IWorkflowDeploymentRepository)
    uow.workflow_repository.get_eventually_running_workflow_by_workflow_definition_id = mock.MagicMock(
        side_effect=WorkflowNotFoundError()
    )
    action_definitions = {
        "1": mock.MagicMock(ActionDefinition, id="1"),
        "2": mock.MagicMock(ActionDefinition, id="1"),
    }
    uow.action_definition_repository = mock.MagicMock(IActionDefinitionRepository)

    def get(x):
        return action_definitions[x]

    uow.action_definition_repository.get = get

    await create(command, message_bus, uow)

    # Check workflow_deployment object creation
    WorkflowDeployment.create_from_create_command.assert_called_once_with(
        command,
        action_definitions,
        {
            command.actions[0].id_in_studio: SchedulingConstraints(
                site_list=[], site_type_list=[], node_pool_list=[], arch_list=[]
            ),
            command.actions[1].id_in_studio: SchedulingConstraints(
                site_list=[], site_type_list=[], node_pool_list=[], arch_list=[]
            ),
        },
    )
    # Trigger next event
    message_bus.handle.assert_called_once()


async def test_workflow_start(app_container) -> None:
    root_action = mock.MagicMock(WorkflowAction)
    root_action.id = "root_id"
    root_action.definition = mock.MagicMock(ActionDefinition)
    root_action.definition.id = "root_definition_id"
    root_action.static_inputs = []

    workflow = WorkflowDeployment(
        id="1",
        name="",
        workflow_definition_id="def_id",
        project_id="123",
        creation_time=datetime.now(),
        state=WorkflowDeploymentState.CREATED,
    )
    workflow.get_root_action = mock.MagicMock(side_effect=root_action)

    message_bus = mock.MagicMock(MessageBus)

    uow = mock.MagicMock(IUnitOfWork)
    uow.workflow_repository = mock.MagicMock(IWorkflowDeploymentRepository)
    uow.workflow_repository.get.return_value = workflow

    # Check that the Gauge goes also down if needed
    before_start_gauge = workflow.workflow_gauge_wih_labels()
    before_start_gauge.inc()
    assert before_start_gauge.collect()[0].samples[0].value == 1

    await start(
        StartWorkflowDeploymentCommand(workflow_id=workflow.id),
        message_bus,
        uow,
    )

    assert workflow.state == WorkflowDeploymentState.STARTED
    # Check that it trigger next event
    message_bus.handle.assert_called_once()
    # Check that the Gauge is correctly incremented and previous state decremented
    assert workflow.workflow_gauge_wih_labels().collect()[0].samples[0].value == 1
    assert before_start_gauge.collect()[0].samples[0].value == 0
