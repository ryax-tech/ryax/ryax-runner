from ryax.common.domain.registration.registration_values import SiteType
from ryax.runner.application.handlers.site_handlers import on_register_or_update
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.site.site_events import RegisterSiteCommand


async def test_on_register_or_update(app_container: ApplicationContainer):
    uow = app_container.unit_of_work()
    cmd_site = RegisterSiteCommand.Site(
        id="TEST_ID", name="test", type=SiteType.KUBERNETES
    )
    node_pool_1 = RegisterSiteCommand.NodePool(name="1", memory=1, cpu=1)
    node_pool_2 = RegisterSiteCommand.NodePool(name="2", memory=2, cpu=2)
    cmd_site.node_pools = [node_pool_1, node_pool_2]

    # Check that it's empty
    with uow:
        assert not uow.site_repository.list()
    # Register once
    site_id = await on_register_or_update(RegisterSiteCommand(site=cmd_site), uow=uow)
    with uow:
        site = uow.site_repository.get(site_id)
        assert len(site.node_pools) == 2
        assert site.node_pools[0].site == site
        assert site.node_pools[1].site == site

    # Register twice
    await on_register_or_update(RegisterSiteCommand(site=cmd_site), uow=uow)
    with uow:
        assert len(uow.site_repository.list()) == 1
        new_site_2 = uow.site_repository.get(site_id)
        assert new_site_2 == site

    # Remove a nodepool
    del cmd_site.node_pools[1]
    await on_register_or_update(RegisterSiteCommand(site=cmd_site), uow=uow)
    with uow:
        assert len(uow.site_repository.list()) == 1
        new_site = uow.site_repository.get(site_id)
        assert len(new_site.node_pools) == 1

    # Add a nodepool
    cmd_site.node_pools.append(node_pool_2)
    await on_register_or_update(RegisterSiteCommand(site=cmd_site), uow=uow)
    with uow:
        assert len(uow.site_repository.list()) == 1
        new_site = uow.site_repository.get(site_id)
        assert len(new_site.node_pools) == 2

    # Update node pool
    cmd_site.node_pools[0].cpu = 4
    cmd_site.node_pools[0].memory = 1000
    await on_register_or_update(RegisterSiteCommand(site=cmd_site), uow=uow)
    with uow:
        new_site = uow.site_repository.get(site_id)
        assert new_site.node_pools[0].cpu == 4
        assert new_site.node_pools[0].memory == 1000
