from unittest import mock

import pytest

from ryax.runner.application.reference_solver_service import ReferenceSolverService
from ryax.runner.application.setup import setup as application_setup
from ryax.runner.domain.site.site_entities import NodePool, Site
from ryax.runner.application.handlers.action_deployment_handlers import (
    create,
    do_apply,
)
from ryax.common.application.message_bus import MessageBus
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    CreateActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentCreatedEvent,
    ActionDeploymentIsDeployingEvent,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    IActionDeploymentRepository,
)
from ryax.runner.domain.action_deployment.action_deployment_service import (
    IActionDeploymentService,
)
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.site.site_repository import ISiteRepository


@mock.patch.object(ActionDeployment, "create_from_apply_command")
async def test_create(action_deployment_factory, app_container):
    command = CreateActionDeploymentCommand(
        project_id="toto",
        action_definition_id="action_id",
        node_pool_id="1",
        execution_connection_id="1",
    )
    message_bus = MessageBus()
    mock_handler = mock.AsyncMock()
    message_bus.handle = mock_handler

    action_deployment = mock.MagicMock(
        ActionDeployment,
        id="1",
        events=[],
        project_id="123",
        action_definition=mock.MagicMock(
            ActionDefinition, technical_name="techname", version="1.0", id="def_id"
        ),
    )
    node_pool = NodePool(id="1", name="1", memory=123, cpu=2, site=None)
    action_deployment.set_created = mock.MagicMock()
    action_deployment_factory.return_value = action_deployment
    uow: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    action_definition = mock.MagicMock(ActionDefinition)
    uow.action_definition_repository = mock.MagicMock(IActionDefinitionRepository)
    uow.action_definition_repository.get.return_value = action_definition
    uow.action_deployment_repository = mock.MagicMock(IActionDeploymentRepository)
    uow.site_repository = mock.MagicMock(ISiteRepository)
    uow.site_repository.get_node_pool.return_value = node_pool
    solver = ReferenceSolverService(uow=uow, addons=app_container.addons())

    await create(command, uow, message_bus, solver)

    action_deployment_factory.assert_called_once_with(
        command, action_definition, node_pool
    )
    action_deployment.set_created.assert_called_once()
    message_bus.handle.assert_called_with(
        [ActionDeploymentCreatedEvent(action_deployment_id="1")]
    )


@pytest.fixture()
def app_container():
    container = ApplicationContainer()
    application_setup(container)
    yield container
    container.unwire()


@mock.patch(
    "ryax.runner.application.handlers.action_deployment_handlers.get_deploy_service"
)
async def test_do_apply(get_deploy_service_mock, app_container):
    action_deployment_service: IActionDeploymentService = mock.MagicMock(
        IActionDeploymentService
    )
    get_deploy_service_mock.return_value = action_deployment_service

    event = ActionDeploymentIsDeployingEvent(
        action_deployment_id="action_dep_id",
    )

    action_deployment = mock.MagicMock(
        ActionDeployment,
        id="1",
        events=[],
        project_id="123",
        action_definition=mock.MagicMock(
            ActionDefinition, technical_name="techname", version="1.0", id="def_id"
        ),
        addons={"hpc": {}},
        node_pool_id="npid",
    )
    action_deployment.set_applied = mock.MagicMock()
    uow: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    uow.action_deployment_repository = mock.MagicMock(IActionDeploymentRepository)
    uow.action_deployment_repository.get_by_id.return_value = action_deployment

    uow.site_repository = mock.MagicMock(ISiteRepository)
    site = Site(id="12", name="test", type=None, node_pools=[])
    uow.site_repository.get.return_value = site

    await do_apply(
        event, app_container.message_bus(), uow, addons=app_container.addons()
    )

    action_deployment_service.deploy.assert_called_once_with(
        action_deployment,
        {
            "hpc": {
                "slurm_args": "#!/bin/bash\n#SBATCH --nodes=1\n#SBATCH --tasks=1\n",
                "custom_script": None,
                "username": None,
                "frontend": None,
                "password": None,
                "private_key": None,
                "port": 22,
            }
        },
    )
