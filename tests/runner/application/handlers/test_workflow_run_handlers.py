import multiprocessing
from datetime import datetime
from unittest import mock

from ryax.runner.application.handlers import workflow_run_handlers
from ryax.runner.domain.common.unit_of_work import IUnitOfWork
from ryax.runner.domain.exceptions import WorkflowRunNotFoundError
from ryax.runner.domain.workflow_run.workflow_run_commands import (
    DeleteWorkflowRunsCommand,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import (
    WorkflowRun,
    WorkflowRunState,
)
from ryax.runner.domain.workflow_run.workflow_run_repository import (
    IWorkflowRunRepository,
)


async def test_delete_workflow_runs():
    test_workflow_id = "test_workflow"
    test_workflow_run_id_0 = "test_workflow_run_0"
    test_workflow_run_id_1 = "test_workflow_run_1"
    test_project = "test_project"
    cmd = DeleteWorkflowRunsCommand(
        workflow_definition_id=test_workflow_id, project_id=test_project
    )
    mock_workflow_runs = [
        WorkflowRun(
            id=test_workflow_run_id_0,
            project_id="foo",
            state=WorkflowRunState.COMPLETED,
            started_at=datetime.now(),
            last_result_at=datetime.now(),
            number_of_completed_actions=0,
            number_of_actions=0,
            workflow_definition_id="clarge",
        ),
        WorkflowRun(
            id=test_workflow_run_id_1,
            project_id="foo",
            state=WorkflowRunState.COMPLETED,
            started_at=datetime.now(),
            last_result_at=datetime.now(),
            number_of_completed_actions=0,
            number_of_actions=0,
            workflow_definition_id="clarge",
        ),
    ]

    uow: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    uow.workflow_run_repository = mock.MagicMock(IWorkflowRunRepository)
    uow.workflow_run_repository.list_all.return_value = mock_workflow_runs
    garbage_collector_service_queue_mock = mock.MagicMock(multiprocessing.Queue)
    garbage_collector_service_queue_mock.put = mock.MagicMock()

    await workflow_run_handlers.delete(cmd, uow, garbage_collector_service_queue_mock)

    garbage_collector_service_queue_mock.put.assert_called_once_with(
        [run.id for run in mock_workflow_runs], block=False
    )


async def test_delete_workflow_runs_with_not_found_exception():
    test_workflow_id = "test_workflow"
    test_workflow_run_id_0 = "test_workflow_run_0"
    test_workflow_run_id_1 = "test_workflow_run_1"
    test_project = "test_project"
    cmd = DeleteWorkflowRunsCommand(
        workflow_definition_id=test_workflow_id, project_id=test_project
    )
    mock_workflow_runs = [
        WorkflowRun(
            id=test_workflow_run_id_0,
            project_id="foo",
            state=WorkflowRunState.COMPLETED,
            started_at=datetime.now(),
            last_result_at=datetime.now(),
            number_of_completed_actions=0,
            number_of_actions=0,
            workflow_definition_id="clarge",
        ),
        WorkflowRun(
            id=test_workflow_run_id_1,
            project_id="foo",
            state=WorkflowRunState.COMPLETED,
            started_at=datetime.now(),
            last_result_at=datetime.now(),
            number_of_completed_actions=0,
            number_of_actions=0,
            workflow_definition_id="clarge",
        ),
    ]

    uow: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    uow.workflow_run_repository = mock.MagicMock(IWorkflowRunRepository)
    uow.workflow_run_repository.list_all.return_value = mock_workflow_runs
    uow.workflow_run_repository.delete.side_effect = [None, WorkflowRunNotFoundError]
    garbage_collector_service_queue_mock = mock.MagicMock(multiprocessing.Queue)
    garbage_collector_service_queue_mock.put = mock.MagicMock()

    await workflow_run_handlers.delete(cmd, uow, garbage_collector_service_queue_mock)

    garbage_collector_service_queue_mock.put.assert_called_once_with(
        [run.id for run in mock_workflow_runs], block=False
    )
