from datetime import datetime, timezone

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_entities import (
    RecommendationVPA,
)
from ryax.runner.domain.recommendation_vpa.recommendation_vpa_events import (
    RecommendationsVPAUpdateCommand,
)

from ryax.runner.application.handlers.recommendation_vpa_handlers import (
    on_recommendations_vpa_update,
)


async def test_on_recommendations_vpa_update(app_container: ApplicationContainer):
    uow = app_container.unit_of_work()

    vpa_recommendation_1 = RecommendationsVPAUpdateCommand.RecommendationVPA(
        container_image="1",
        memory=1024,
        gpu_mig_instance="1g.10gb",
        timestamp=datetime(2024, 11, 1, 0, 0, 0, tzinfo=timezone.utc),
        is_oom_bumpup=True,
        is_gpuoom_bumpup=True,
    )
    vpa_recommendation_2 = RecommendationsVPAUpdateCommand.RecommendationVPA(
        container_image="2",
        memory=2048,
        gpu_mig_instance="3g.40gb",
        timestamp=datetime(2024, 11, 1, 0, 0, 0, tzinfo=timezone.utc),
        is_oom_bumpup=True,
        is_gpuoom_bumpup=True,
    )
    recommendations = RecommendationsVPAUpdateCommand(
        recommendations=[vpa_recommendation_1, vpa_recommendation_2]
    )

    # Should be empty at the beginning
    with uow:
        assert not uow.site_repository.list()

    await on_recommendations_vpa_update(recommendations, uow=uow)

    # Test if recommendations are successfully added
    with uow:
        rec1 = uow.recommendation_vpa_repository.get(
            vpa_recommendation_1.container_image
        )
        assert RecommendationVPA.from_command(vpa_recommendation_1) == rec1

        rec2 = uow.recommendation_vpa_repository.get(
            vpa_recommendation_2.container_image
        )
        assert RecommendationVPA.from_command(vpa_recommendation_2) == rec2

    vpa_recommendation_new_1 = RecommendationsVPAUpdateCommand.RecommendationVPA(
        container_image="1",
        memory=512,
        gpu_mig_instance="7g.80gb",
        timestamp=datetime(
            2024, 11, 1, 12, 0, 0, tzinfo=timezone.utc
        ),  # Newer timestamp
        is_oom_bumpup=True,
        is_gpuoom_bumpup=True,
    )
    vpa_recommendation_old_2 = RecommendationsVPAUpdateCommand.RecommendationVPA(
        container_image="2",
        memory=512,
        gpu_mig_instance="7g.80gb",
        timestamp=datetime(
            2024, 10, 31, 0, 0, 0, tzinfo=timezone.utc
        ),  # Older timestamp
        is_oom_bumpup=True,
        is_gpuoom_bumpup=True,
    )

    recommendations_toadd = RecommendationsVPAUpdateCommand(
        recommendations=[vpa_recommendation_new_1, vpa_recommendation_old_2]
    )
    await on_recommendations_vpa_update(recommendations_toadd, uow=uow)

    with uow:
        rec1 = uow.recommendation_vpa_repository.get(
            vpa_recommendation_1.container_image
        )
        assert (
            RecommendationVPA.from_command(vpa_recommendation_new_1) == rec1
        )  # Should be updated because it's newer

        rec2 = uow.recommendation_vpa_repository.get(
            vpa_recommendation_2.container_image
        )
        assert (
            RecommendationVPA.from_command(vpa_recommendation_2) == rec2
        )  # Should not be updated


async def test_on_recommendations_vpa_update_oom(app_container: ApplicationContainer):
    uow = app_container.unit_of_work()

    vpa_recommendation = RecommendationsVPAUpdateCommand.RecommendationVPA(
        container_image="1",
        memory=2048,
        gpu_mig_instance="3g.40gb",
        timestamp=datetime(2024, 11, 1, 0, 0, 0, tzinfo=timezone.utc),
        is_oom_bumpup=True,
        is_gpuoom_bumpup=True,
    )

    recommendations_toadd = RecommendationsVPAUpdateCommand(
        recommendations=[vpa_recommendation]
    )
    await on_recommendations_vpa_update(recommendations_toadd, uow=uow)

    vpa_recommendation_new = RecommendationsVPAUpdateCommand.RecommendationVPA(
        container_image="1",
        memory=1024,
        gpu_mig_instance="1g.10gb",
        timestamp=datetime(
            2024, 11, 1, 12, 0, 0, tzinfo=timezone.utc
        ),  # Newer timestamp
        is_oom_bumpup=False,
        is_gpuoom_bumpup=False,
    )

    recommendations_toadd_new = RecommendationsVPAUpdateCommand(
        recommendations=[vpa_recommendation_new]
    )
    await on_recommendations_vpa_update(recommendations_toadd_new, uow=uow)

    rec = uow.recommendation_vpa_repository.get(vpa_recommendation.container_image)
    assert rec == RecommendationVPA(
        key=vpa_recommendation.container_image,
        timestamp=datetime(2024, 11, 1, 12, 0, 0, tzinfo=timezone.utc),
        last_oom_timestamp=datetime(
            2024, 11, 1, 0, 0, 0, tzinfo=timezone.utc
        ),  # Old timestamp because no bumpup in new update
        last_gpuoom_timestamp=datetime(2024, 11, 1, 0, 0, 0, tzinfo=timezone.utc),
        memory=1024,
        gpu_mig_instance="1g.10gb",
    )
