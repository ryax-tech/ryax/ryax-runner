import asyncio
import json
from unittest import mock

import aiohttp
from aiohttp.test_utils import TestClient

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_events import (
    ActionDeploymentNewLogLinesAddedEvent,
)
from ryax.runner.domain.action_logs.action_logs_entities import ActionLogs


async def test_logs_from_websocket(
    app_container: ApplicationContainer,
    api_client: TestClient,
):
    logs_to_catch = "line one\nline two\n"
    mocked_action_deployment = mock.MagicMock(ActionDeployment, id="deployid")
    uow = app_container.unit_of_work()
    uow.action_deployment_repository.get_by_workflow_definition_id.return_value = (
        mocked_action_deployment
    )
    logs = ActionLogs.create(mocked_action_deployment.id, "")
    for line in logs_to_catch.splitlines(keepends=True):
        logs.append(line)
    uow = app_container.unit_of_work()
    uow.action_logs_repository.get_logs_as_string.return_value = str(logs)

    message = None

    async with api_client.ws_connect("/ws/workflows/toto") as ws:
        asyncio.ensure_future(ws.send_str('{"logs": "-1"}'))
        msg = await ws.receive()
        if msg.type == aiohttp.WSMsgType.TEXT:
            message = json.loads(msg.data)["logs"]
            await ws.close()
        elif msg.type == aiohttp.WSMsgType.ERROR:
            raise Exception()
        else:
            raise Exception()
    assert message == logs_to_catch


async def test_new_logs_from_event(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
    api_client: TestClient,
):
    """Test that new logs from event"""
    initial_log_lines = "line one\nline two\n"
    action_deployment_id = "deployid1"
    mocked_action_deployment = mock.MagicMock(
        ActionDeployment,
        id=action_deployment_id,
    )
    uow = app_container.unit_of_work()
    uow.action_deployment_repository.get_by_workflow_definition_id.return_value = (
        mocked_action_deployment
    )
    logs = ActionLogs.create(mocked_action_deployment.id, "")
    for line in initial_log_lines.splitlines(keepends=True):
        logs.append(line)
    uow = app_container.unit_of_work()
    uow.action_logs_repository.get_logs_as_string.return_value = str(logs)

    message_bus = app_container.message_bus()

    async with api_client.ws_connect("/ws/workflows/toto") as ws:
        asyncio.ensure_future(ws.send_str('{"logs": "-1"}'))
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = json.loads(msg.data)["logs"]
        assert message == initial_log_lines

        # Create new instant logs
        new_log_lines = "line three\nline four\n"
        await message_bus.handle_event(
            ActionDeploymentNewLogLinesAddedEvent(
                log_lines=new_log_lines,
                action_deployment_id=action_deployment_id,
            )
        )
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = json.loads(msg.data)["logs"]
        assert message == new_log_lines

        # Create new instant logs again
        last_log_lines = "line five\nline six\n"
        await message_bus.handle_event(
            ActionDeploymentNewLogLinesAddedEvent(
                log_lines=last_log_lines,
                action_deployment_id=action_deployment_id,
            )
        )
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = json.loads(msg.data)["logs"]
        assert message == last_log_lines

        await ws.close()
    assert message is not None
