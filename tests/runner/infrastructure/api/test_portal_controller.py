from unittest import mock

from marshmallow import RAISE

from ryax.runner.application.views.portal_view import PortalView
from ryax.runner.infrastructure.api.schemas.portal_schema import PortalSchema


async def test_get_portal(portal_view_mock: PortalView, api_client) -> None:
    portal = {
        "workflow_definition_id": "workflow_def_id1",
        "workflow_deployment_id": "workflow_deployment_id1",
        "name": "workflow_deployment.name1",
        "outputs": [
            {
                "id": "out1",
                "type": "STRING",
                "display_name": "out1",
                "help": "help",
                "enum_values": [],
            },
            {
                "id": "out2",
                "type": "FLOAT",
                "display_name": "out2",
                "help": "help",
                "enum_values": [],
            },
        ],
    }
    portal_view_mock.get_portal = mock.MagicMock(return_value=portal)
    response = await api_client.get("/portals/workflow_def_id1")
    assert response.status == 200
    portal_view_mock.get_portal.assert_called_with("workflow_def_id1")
    result = await response.json()
    PortalSchema().load(result, unknown=RAISE, partial=False)
    assert result == {
        "workflow_definition_id": "workflow_def_id1",
        "workflow_deployment_id": "workflow_deployment_id1",
        "name": "workflow_deployment.name1",
        "outputs": [
            {
                "id": "out1",
                "type": "STRING",
                "display_name": "out1",
                "help": "help",
                "enum_values": [],
                "headers": {},
            },
            {
                "id": "out2",
                "type": "FLOAT",
                "display_name": "out2",
                "help": "help",
                "enum_values": [],
                "headers": {},
            },
        ],
    }
