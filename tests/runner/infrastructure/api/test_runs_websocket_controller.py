import asyncio
from unittest import mock

import aiohttp
from aiohttp.test_utils import TestClient

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_logs.action_logs_entities import ActionLogs
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_events import (
    ExecutionResultNewLogLinesAddedEvent,
)


async def test_logs_from_websocket(
    app_container: ApplicationContainer,
    api_client: TestClient,
):
    log = "line one\nline two\n"
    mocked_execution_result = mock.MagicMock(ExecutionResult, id="execid")
    uow = app_container.unit_of_work()
    logs = ActionLogs.create(mocked_execution_result.id, "")
    for line in log.splitlines(keepends=True):
        logs.append(line)
    uow.action_logs_repository.get.return_value = logs
    uow.action_logs_repository.get_logs_as_string.return_value = str(logs)

    message = None

    async with api_client.ws_connect(f"/ws/runs/{mocked_execution_result.id}") as ws:
        asyncio.ensure_future(ws.send_str("-1"))
        msg = await ws.receive()
        if msg.type == aiohttp.WSMsgType.TEXT:
            message = msg.data
            await ws.close()
        elif msg.type == aiohttp.WSMsgType.ERROR:
            raise Exception()
        else:
            raise Exception()
    assert message == log


async def test_new_logs_from_event(
    app_container: ApplicationContainer,
    api_client: TestClient,
):
    """Test that new logs from event"""
    execution_result_id = "execid"
    initial_log_lines = "line one\nline two\n"
    mocked_execution_result = mock.MagicMock(ExecutionResult, id=execution_result_id)
    logs = ActionLogs.create(mocked_execution_result.id, "")
    for line in initial_log_lines.splitlines(keepends=True):
        logs.append(line)
    uow = app_container.unit_of_work()
    uow.action_logs_repository.get.return_value = logs
    uow.action_logs_repository.get_logs_as_string.return_value = str(logs)

    message = None
    message_bus = app_container.message_bus()

    async with api_client.ws_connect(f"/ws/runs/{execution_result_id}") as ws:
        asyncio.ensure_future(ws.send_str("-1"))
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = msg.data
        assert message == initial_log_lines

        # Create new instant logs
        new_log_lines = "line three\nline four\n"
        await message_bus.handle_event(
            ExecutionResultNewLogLinesAddedEvent(
                log_lines=new_log_lines,
                execution_result_id=execution_result_id,
            )
        )
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = msg.data
        assert message == new_log_lines

        # Create new instant logs again
        last_log_lines = "line five\nline six\n"
        await message_bus.handle_event(
            ExecutionResultNewLogLinesAddedEvent(
                log_lines=last_log_lines,
                execution_result_id=execution_result_id,
            )
        )
        msg = await ws.receive()
        assert msg.type == aiohttp.WSMsgType.TEXT
        message = msg.data
        assert message == last_log_lines

        await ws.close()
    assert message is not None
