from unittest import mock

import pytest
from aiohttp import web
from aiohttp.test_utils import TestClient

from ryax.runner.application.authentication_service import AuthenticationService
from ryax.runner.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.runner.application.user_auth_service import UserAuthService
from ryax.runner.application.views.portal_view import PortalView
from ryax.runner.application.views.stored_file_view import StoredFileView
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_repository import (
    IActionDefinitionRepository,
)
from ryax.runner.domain.auth_token import AuthToken
from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.stored_file.stored_file_reporitory import IStoredFileRepository
from ryax.runner.infrastructure.api.setup import setup as setup_api
from tests.unit_of_work_stub import UnitOfWorkStub


@pytest.fixture()
def app_container():
    app_container = ApplicationContainer()
    app_container.unit_of_work.override(UnitOfWorkStub())
    yield app_container
    app_container.unwire()


@pytest.fixture()
def api_client(loop, app_container: ApplicationContainer, aiohttp_client) -> TestClient:
    app = web.Application()
    setup_api(app, app_container)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture()
def authentication_service_mock(app_container: ApplicationContainer):
    authentication_service_mock = mock.MagicMock(AuthenticationService)
    authentication_service_mock.check_access = mock.MagicMock(
        return_value=AuthToken(user_id="user")
    )
    app_container.authentication_service.override(authentication_service_mock)
    return authentication_service_mock


@pytest.fixture()
def project_authorization_service_mock(app_container: ApplicationContainer):
    project_authorization_service_mock = mock.MagicMock(ProjectAuthorizationService)
    project_authorization_service_mock.get_current_project = mock.AsyncMock(
        return_value="project_id"
    )
    app_container.project_authorization_service.override(
        project_authorization_service_mock
    )
    return project_authorization_service_mock


@pytest.fixture()
def message_bus_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    message_bus_mock = mock.MagicMock(IMessageBus)
    app_container.message_bus.override(message_bus_mock)
    return message_bus_mock


@pytest.fixture()
def portal_view_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    portal_view_mock = mock.MagicMock(PortalView)
    app_container.portal_view.override(portal_view_mock)
    return portal_view_mock


@pytest.fixture()
def stored_file_view_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    stored_file_mock = mock.MagicMock(StoredFileView)
    app_container.stored_file_view.override(stored_file_mock)
    return stored_file_mock


@pytest.fixture()
def user_auth_service_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    user_auth_service_mocked = mock.MagicMock(UserAuthService)
    app_container.user_auth_service.override(user_auth_service_mocked)
    return user_auth_service_mocked


@pytest.fixture()
def stored_file_repository_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    stored_file_repository_mock = mock.MagicMock(IStoredFileRepository)
    app_container.stored_file_repository.override(stored_file_repository_mock)
    return stored_file_repository_mock


@pytest.fixture()
def action_definition_repository_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    action_definition_repo_mock = mock.MagicMock(IActionDefinitionRepository)
    app_container.action_definition_repository.override(action_definition_repo_mock)
    return action_definition_repo_mock
