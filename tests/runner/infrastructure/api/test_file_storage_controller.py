from unittest import mock

from aiohttp.test_utils import TestClient

from ryax.runner.domain.stored_file.storage_exceptions import StorageFileNotFoundError
from ryax.runner.domain.stored_file.stored_file import StoredFile


async def test_download_file(
    stored_file_view_mock: mock.MagicMock,
    api_client: TestClient,
):
    stored_file_id = "file_id"
    stored_file_path = "test/path"
    stored_file_project = "project_id"

    stored_file_aggregate = StoredFile(
        id=stored_file_id,
        file_path=stored_file_path,
        project_id=stored_file_project,
        mimetype="toto",
        encoding="tata",
        size_in_bytes=12,
        io_name="abc123",
    )
    stored_file_view_mock.get.return_value = stored_file_aggregate
    stored_file_view_mock.download_file = mock.AsyncMock()

    response = await api_client.get(f"/filestore/{stored_file_id}/download")
    assert response.status == 200


async def test_download_file_when_not_exists(
    stored_file_view_mock: mock.MagicMock,
    api_client: TestClient,
):
    stored_file_id = "file_id"
    stored_file_project = "project_id"
    stored_file_view_mock.get.side_effect = StorageFileNotFoundError
    response = await api_client.get(f"/filestore/{stored_file_id}/download")
    assert response.status == 404
    assert await response.json() == {"error": "Stored file not found"}
    stored_file_view_mock.get.assert_called_once_with(
        stored_file_id, stored_file_project
    )
