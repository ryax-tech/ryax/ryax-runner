from unittest import mock

from aiohttp.test_utils import TestClient


async def test_get_user_auth(
    user_auth_service_mock: mock.MagicMock,
    api_client: TestClient,
):
    project = "project_id"
    user_auth_service_mock.get_all_auth_methods.return_value = {}
    response = await api_client.get("/user-auth")
    assert response.status == 200
    assert await response.json() == {}
    user_auth_service_mock.get_all_auth_methods.assert_called_once_with(project)
