import random
import tempfile
from io import BytesIO
from pathlib import Path

from ryax.runner.container import ApplicationContainer
from ryax.common.infrastructure.storage.posix.posix_storage_service import (
    PosixStorageService,
)


def test_posix_storage_config(app_container: ApplicationContainer):
    app_container.configuration.from_dict(
        {
            "storage": {
                "local": {
                    "type": "posix",
                    "posix": {"root_path": "/tmp/ryax/storage"},
                }
            }
        }
    )
    storage: PosixStorageService = app_container.local_storage_service()
    assert (
        str(storage.root_path)
        == app_container.configuration.storage.local.posix.root_path()
    )


async def test_posix_storage_read(app_container):
    with tempfile.TemporaryDirectory() as tmp_dir:
        file_path = Path(tmp_dir) / "test"
        file_content = b"test bytes"
        with open(file_path, "wb") as tmp_file:
            tmp_file.write(file_content)

        storage = PosixStorageService(root_path=tmp_dir)

        assert await storage.read_file(str(file_path)) == file_content


async def test_posix_storage_write():
    with tempfile.TemporaryDirectory() as tmp_dir:
        file_path = Path(tmp_dir) / "test"
        file_content = b"test bytes"

        storage = PosixStorageService(tmp_dir)
        await storage.write_file(str(file_path), file_content, len(file_content))

        file_content_read: bytes
        with open(file_path, "rb") as tmp_file:
            file_content_read = tmp_file.read()

        assert file_content == file_content_read


async def test_posix_storage_stream_read():
    with tempfile.TemporaryDirectory() as tmp_dir:
        file_path = Path(tmp_dir) / "test"
        file_content = bytearray()
        with open(file_path, "wb") as tmp_file:
            byte_array = bytearray(random.getrandbits(8) for _ in range(1024**2 * 8))
            tmp_file.write(byte_array)
            file_content += byte_array

        storage = PosixStorageService(root_path=tmp_dir)
        with await storage.read(str(file_path)) as open_file:
            stored_file_content = open_file.read()

        assert stored_file_content == file_content


async def test_posix_storage_stream_write():
    with tempfile.TemporaryDirectory() as tmp_dir:
        file_path = Path(tmp_dir) / "test"
        file_content = bytearray(random.getrandbits(8) for _ in range(1024**2 * 8))
        file_stream = BytesIO(file_content)

        storage = PosixStorageService(tmp_dir)
        with file_stream as file_to_read:
            await storage.write(str(file_path), file_to_read)

        with open(file_path, "rb") as tmp_file:
            file_content_read = tmp_file.read()

        assert file_content == file_content_read


async def test_posix_storage_remove():
    with tempfile.TemporaryDirectory() as tmp_dir:
        file_path = Path(tmp_dir) / "test"
        file_content = b"test bytes"
        with open(file_path, "wb") as tmp_file:
            tmp_file.write(file_content)

        storage = PosixStorageService(root_path=tmp_dir)
        await storage.remove_file(str(file_path))

        assert not Path(file_path).exists()
