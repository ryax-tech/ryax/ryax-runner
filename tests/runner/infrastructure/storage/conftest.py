import pytest

from ryax.runner.container import ApplicationContainer


@pytest.fixture()
def app_container():
    container = ApplicationContainer()
    return container
