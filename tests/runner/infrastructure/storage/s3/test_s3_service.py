import io
import os
import random
import uuid
from unittest import mock

import minio
import pytest
from minio import Minio

from ryax.runner.container import ApplicationContainer
from ryax.common.infrastructure.storage.s3.s3_engine import S3Engine
from ryax.common.infrastructure.storage.s3.s3_service import S3Storage


class TestS3Service:
    @mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_uuid"))
    def test_generate_file_path(self, app_container: ApplicationContainer):
        s3_storage: S3Storage = app_container.local_s3_storage()
        assert (
            s3_storage.generate_file_path("file_name")
            == "executions-data/new_uuid/file_name"
        )

    async def test_write_file(
        self,
        minio_client: Minio,
        filestore_bucket: str,
        app_container: ApplicationContainer,
    ):
        file_id = uuid.uuid4()
        path = f"folder/{file_id}"
        file_size = 8193
        file = os.urandom(file_size)
        s3_engine: S3Engine = mock.MagicMock(S3Engine)
        s3_engine.connection = minio_client
        s3_engine.bucket = filestore_bucket
        app_container.local_s3_engine.override(s3_engine)
        s3_storage: S3Storage = app_container.local_s3_storage()
        await s3_storage.write_file(path, file, file_size)
        download_file = minio_client.get_object(filestore_bucket, path)
        download_file_content = download_file.read()
        assert download_file_content == file
        assert len(download_file_content) == file_size

    async def test_remove_file(
        self,
        minio_client: Minio,
        filestore_bucket: str,
        app_container: ApplicationContainer,
    ):
        file_size = 8193
        file = io.BytesIO(os.urandom(file_size))
        file_id = uuid.uuid4()
        path = f"folder/{file_id}"
        s3_engine: S3Engine = mock.MagicMock(S3Engine)
        s3_engine.connection = minio_client
        s3_engine.bucket = filestore_bucket
        app_container.local_s3_engine.override(s3_engine)
        minio_client.put_object(filestore_bucket, path, file, file_size)
        s3_storage: S3Storage = app_container.local_s3_storage()
        await s3_storage.remove_file(path)
        with pytest.raises(minio.error.S3Error):
            minio_client.get_object(filestore_bucket, path)

    async def test_remove_file_when_not_exists(
        self,
        minio_client: Minio,
        filestore_bucket: str,
        app_container: ApplicationContainer,
    ):
        s3_engine: S3Engine = mock.MagicMock(S3Engine)
        s3_engine.connection = minio_client
        s3_engine.bucket = filestore_bucket
        app_container.local_s3_engine.override(s3_engine)
        s3_storage: S3Storage = app_container.local_s3_storage()
        with pytest.raises(FileNotFoundError):
            await s3_storage.remove_file("random_file_path")

    async def test_read_file(
        self,
        minio_client: Minio,
        filestore_bucket: str,
        app_container: ApplicationContainer,
    ):
        file_size = 8193
        file = io.BytesIO(os.urandom(file_size))
        file_id = str(uuid.uuid4())
        path = f"folder/{file_id}"
        s3_engine: S3Engine = mock.MagicMock(S3Engine)
        s3_engine.connection = minio_client
        s3_engine.bucket = filestore_bucket
        app_container.local_s3_engine.override(s3_engine)
        minio_client.put_object(filestore_bucket, path, file, file_size)
        s3_storage: S3Storage = app_container.local_s3_storage()
        file.seek(0)
        assert await s3_storage.read_file(path) == file.read()

    async def test_read_file_when_not_found(
        self,
        minio_client: Minio,
        filestore_bucket: str,
        app_container: ApplicationContainer,
    ):
        s3_engine: S3Engine = mock.MagicMock(S3Engine)
        s3_engine.connection = minio_client
        s3_engine.bucket = filestore_bucket
        app_container.local_s3_engine.override(s3_engine)
        s3_storage: S3Storage = app_container.local_s3_storage()
        with pytest.raises(FileNotFoundError):
            assert await s3_storage.read_file("nonexistent_file/path")

    async def test_stream_write_file(
        self,
        minio_client: Minio,
        filestore_bucket: str,
        app_container: ApplicationContainer,
    ):
        file_id = uuid.uuid4()
        path = f"folder/{file_id}"
        file_size = 1024**2 * 8  # 8MB
        file_content = bytearray(random.getrandbits(8) for _ in range(file_size))
        file_stream = io.BytesIO(file_content)

        s3_engine: S3Engine = mock.MagicMock(S3Engine)
        s3_engine.connection = minio_client
        s3_engine.bucket = filestore_bucket
        app_container.local_s3_engine.override(s3_engine)
        s3_storage: S3Storage = app_container.local_s3_storage()

        await s3_storage.write(path, file_stream)

        download_file = minio_client.get_object(filestore_bucket, path)
        download_file_content = download_file.read()
        assert download_file_content == file_content
        assert len(download_file_content) == file_size

    async def test_stream_read_file(
        self,
        minio_client: Minio,
        filestore_bucket: str,
        app_container: ApplicationContainer,
    ):
        file_id = str(uuid.uuid4())
        path = f"folder/{file_id}"
        file_size = 1024**2 * 8  # 8MB
        file_content = bytearray(random.getrandbits(8) for _ in range(file_size))
        file_stream = io.BytesIO(file_content)

        s3_engine: S3Engine = mock.MagicMock(S3Engine)
        s3_engine.connection = minio_client
        s3_engine.bucket = filestore_bucket
        app_container.local_s3_engine.override(s3_engine)

        minio_client.put_object(filestore_bucket, path, file_stream, file_size)

        s3_storage: S3Storage = app_container.local_s3_storage()
        with await s3_storage.read(path) as stream:
            read_content = stream.read()

        assert read_content == file_content
