from ryax.runner.container import ApplicationContainer


class TestS3StorageConfig:
    def test_s3_storage_config(self, app_container: ApplicationContainer):
        config = {
            "stored_file": {
                "local": {
                    "s3": {
                        "bucket": "test",
                    }
                }
            }
        }
        app_container.configuration.from_dict(config)

        storage = app_container.local_s3_storage()

        assert storage.bucket == app_container.configuration.storage.local.s3.bucket()
