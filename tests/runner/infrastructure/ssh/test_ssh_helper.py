import os
import tempfile

from ryax.worker.infrastructure.ssh.ssh_connection_helper import (
    SshConnectionHelper,
)


async def test_ssh_connector_with_pkey(ssh_helper: SshConnectionHelper):
    # Please run this test with a correct private key cat private-key.pem | base64 -w0
    ret_stdout, ret_stderr, ret_code = await ssh_helper.exec_ssh_cmd("ls -1")
    assert ret_code == 0
    test_file = tempfile.mkstemp()[1]
    dst_file = f"{os.path.basename(test_file)}"
    await ssh_helper.put_ssh_file(test_file, dst_file)
    os.remove(test_file)
    ret_stdout, ret_stderr, ret_code = await ssh_helper.exec_ssh_cmd(f"rm {dst_file}")
    assert ret_code == 0


async def test_ssh_connector_with_password(ssh_helper):
    ret_stdout, ret_stderr, ret_code = await ssh_helper.exec_ssh_cmd("ls -1")
    assert ret_code == 0
    test_file = tempfile.mkstemp()[1]
    dst_file = f"{os.path.basename(test_file)}"
    await ssh_helper.put_ssh_file(test_file, dst_file)
    ret_stdout, ret_stderr, ret_code = await ssh_helper.exec_ssh_cmd(f"rm {dst_file}")
    assert ret_code == 0
    os.remove(test_file)


async def test_ssh_cmd_failed(ssh_helper):
    ret_stdout, ret_stderr, ret_code = await ssh_helper.exec_ssh_cmd("ls -1 /notfound")
    assert ret_code != 0


async def test_scp_get_file(ssh_helper):
    test_file = tempfile.mkstemp()[1]
    dst_file = f"{os.path.basename(test_file)}"
    await ssh_helper.put_ssh_file(test_file, dst_file)
    await ssh_helper.get_ssh_file(dst_file, "test.txt")
    assert os.path.exists("test.txt")
    await ssh_helper.exec_ssh_cmd(f"rm {dst_file}")
    os.remove("test.txt")
    os.remove(test_file)


async def test_ssh_file_exists(ssh_helper):
    test_file = tempfile.mkstemp()[1]
    dst_file = f"{os.path.basename(test_file)}"
    assert not await ssh_helper.remote_file_exists(dst_file)
    await ssh_helper.put_ssh_file(test_file, dst_file)
    assert await ssh_helper.remote_file_exists(dst_file)
    await ssh_helper.exec_ssh_cmd(f"rm {dst_file}")
    os.remove(test_file)
