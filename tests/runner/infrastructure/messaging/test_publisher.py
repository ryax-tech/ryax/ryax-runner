from unittest.mock import AsyncMock, MagicMock, patch

import pytest
from aio_pika import Channel, Exchange

from ryax.runner.container import ApplicationContainer
from ryax.common.domain.internal_messaging.base_messages import BaseEvent
from ryax.common.infrastructure.messaging.publisher import (
    RabbitMQMessagePublisher,
    ProtobufMessage,
)


class MyMessageType(BaseEvent):
    test: str = "test1"


class MyOtherMessageType(BaseEvent):
    test: str = "test2"


def test_register_handler(app: ApplicationContainer):
    message_type = MyMessageType

    message_handler = AsyncMock()
    messaging_publisher: RabbitMQMessagePublisher = app.message_publisher()
    messaging_publisher.register_handler(message_type, message_handler)
    assert messaging_publisher._handlers[message_type] == message_handler


def test_handle_not_registered_message(app: ApplicationContainer):
    """Should process message with handler"""
    message = MyOtherMessageType()
    message_handler = AsyncMock()
    messaging_publisher: RabbitMQMessagePublisher = app.message_publisher()

    messaging_publisher.register_handler(MyMessageType, message_handler)
    messaging_publisher.handle_message(message)

    message_handler.assert_not_called()


def test_handle_registered_message_type_with_success(
    app: ApplicationContainer,
):
    """Should process message with handler"""
    message = MyMessageType()
    message_handler = AsyncMock(return_value=None)
    messaging_publisher: RabbitMQMessagePublisher = app.message_publisher()

    messaging_publisher.register_handler(MyMessageType, message_handler)
    messaging_publisher.handle_message(message)

    message_handler.assert_called_with(message)


def test_handle_registered_message_type_with_handler_error(
    app: ApplicationContainer,
):
    """Should not process message with handler"""
    message = MyMessageType()
    message_handler = MagicMock(side_effect=Exception)
    messaging_publisher: RabbitMQMessagePublisher = app.message_publisher()

    messaging_publisher.register_handler(MyMessageType, message_handler)
    with pytest.raises(Exception):
        messaging_publisher.handle_message(message)

    message_handler.assert_called_with(message)


@patch("aio_pika.Message", autospec=True, id="12")
async def test_publish(message_mock, app: ApplicationContainer):
    """TODO: Do not mock what you don't own! Don't mock aiopika, run an integration test instead"""
    engine = app.messaging_engine()
    channel = MagicMock(Channel)
    exchange = AsyncMock(Exchange)
    channel.declare_exchange = AsyncMock(return_value=exchange)
    channel.close = AsyncMock()
    engine.get_channel = AsyncMock(return_value=channel)

    event = MyMessageType()
    converted_event = MagicMock(ProtobufMessage)
    handler = MagicMock(return_value=converted_event)

    event_publisher: RabbitMQMessagePublisher = app.message_publisher()
    event_publisher.register_handler(MyMessageType, handler)
    message_instance = message_mock.return_value

    await event_publisher.publish([event])

    channel.declare_exchange.assert_called_once()
    message_mock.assert_called_with(
        type=converted_event.__class__.__name__,
        body=converted_event.SerializeToString(),
    )
    exchange.publish.assert_called_once_with(
        message_instance, routing_key="Runner." + converted_event.__class__.__name__
    )
    await channel.close()
