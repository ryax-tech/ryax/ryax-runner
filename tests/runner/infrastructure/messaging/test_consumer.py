import os
from unittest.mock import AsyncMock, MagicMock

import aio_pika
import pytest
from aio_pika import Channel, Exchange, ExchangeType, IncomingMessage, Queue

from ryax.runner.container import ApplicationContainer
from ryax.common.infrastructure.messaging.messages.studio_messages_pb2 import (
    WorkflowDeploy,
)
from ryax.runner.infrastructure.messaging.handlers import workflow_rmq_handler
from ryax.runner.infrastructure.messaging.setup import setup as messaging_setup
from ryax.common.infrastructure.messaging.consumer import RabbitMQMessageConsumer


class MyMessageType:
    pass


class MyOtherMessageType:
    pass


def test_register_handler(app: ApplicationContainer):
    message_type = MyMessageType

    message_handler = AsyncMock()
    messaging_consumer: RabbitMQMessageConsumer = app.message_consumer()
    messaging_consumer.register_handler(message_type, message_handler, "routing_prefix")
    assert (
        messaging_consumer._handlers[message_type.__name__].handler_function
        == message_handler
    )


async def test_handle_not_registered_message(app: ApplicationContainer):
    """Should process message with handler"""
    message = MagicMock(IncomingMessage)
    message.type = MyOtherMessageType
    message_handler = AsyncMock()
    messaging_consumer: RabbitMQMessageConsumer = app.message_consumer()
    messaging_consumer.register_handler(MyMessageType, message_handler, "toto")
    await messaging_consumer.handle_message(message)
    message_handler.assert_not_called()


async def test_handle_registered_message_type_with_success(
    app: ApplicationContainer,
):
    """Should process message with handler"""
    message = MagicMock(IncomingMessage)
    message.type = MyMessageType.__name__
    message.reply_to = None

    message_handler = AsyncMock(return_value=None)
    messaging_consumer: RabbitMQMessageConsumer = app.message_consumer()

    messaging_consumer.register_handler(MyMessageType, message_handler, "toto")
    await messaging_consumer.handle_message(message)
    message_handler.assert_called_with(message)


async def test_handle_registered_message_type_with_handler_error(
    app: ApplicationContainer,
):
    """Sould not process message with handler and ack message"""
    message = MagicMock(IncomingMessage)
    message.type = MyMessageType.__name__
    message.reply_to = None

    message_handler = AsyncMock(side_effect=Exception)
    messaging_consumer: RabbitMQMessageConsumer = app.message_consumer()

    messaging_consumer.register_handler(MyMessageType, message_handler, "toto")
    with pytest.raises(Exception):
        await messaging_consumer.handle_message(message)
    message_handler.assert_called_with(message)


async def test_start(app: ApplicationContainer):
    """Should received message on runner event queue when publishing on ..."""
    """ TODO: Do not mock what you don't own! Don't mock aiopika, run an integration test instead"""
    domain_event_exchange = MagicMock(Exchange)
    ryax_runner_event_queue = MagicMock(Queue)
    channel = MagicMock(Channel)
    channel.declare_exchange = AsyncMock(return_value=domain_event_exchange)
    channel.declare_queue = AsyncMock(return_value=ryax_runner_event_queue)
    messaging_engine = app.messaging_engine()
    messaging_engine.get_channel = AsyncMock(return_value=channel)
    messaging_consumer: RabbitMQMessageConsumer = app.message_consumer()
    messaging_consumer.register_handler(MyMessageType, AsyncMock(), "Test")

    await messaging_consumer.start()

    channel.declare_exchange.assert_called_with(
        "domain_events", ExchangeType.TOPIC, durable=True
    )
    channel.declare_queue.assert_called_with("", durable=True)
    ryax_runner_event_queue.bind.assert_called_with(
        domain_event_exchange, "Test." + "MyMessageType"
    )
    ryax_runner_event_queue.consume.assert_called_with(
        messaging_consumer.handle_message
    )


async def test_consumer_workflow_deploy_with_broker(app: ApplicationContainer):
    """Should received message on runner event queue when publishing on ..."""
    engine = app.messaging_engine(connection_url=os.environ["RYAX_BROKER"])
    await engine.connect()
    messaging_consumer: RabbitMQMessageConsumer = app.message_consumer()

    workflow_rmq_handler.on_deploy_workflow = AsyncMock()
    messaging_setup(app)
    await messaging_consumer.start()

    # Publish a message
    channel = await engine.get_channel()
    exchange = await channel.declare_exchange(
        "domain_events", aio_pika.ExchangeType.TOPIC, durable=True
    )
    message_content = WorkflowDeploy()
    event_type = WorkflowDeploy.__name__
    message = aio_pika.Message(
        type=event_type, body=message_content.SerializeToString()
    )
    await exchange.publish(message, routing_key=f"Studio.{event_type}")
    await channel.close()

    workflow_rmq_handler.on_deploy_workflow.assert_awaited_once()
