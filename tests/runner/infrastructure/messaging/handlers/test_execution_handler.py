import datetime

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionInput,
    ActionOutput,
    ActionResources,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionExecutionType,
    ActionDeploymentState,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.infrastructure.messaging.handlers.execution_rmq_handler import (
    on_start_execution,
)


def test_on_start_execution():
    execution = ExecutionConnection(
        id="ExecutionConnection-1721293506-vmv3d0pb",
        project_id="eeee8eb0-ef38-4bcd-8ebb-c4808751fdc2",
        workflow_action=[],
        workflow_run_id=None,
        submitted_at=datetime.datetime(
            2024, 7, 18, 9, 5, 6, 559452, tzinfo=datetime.timezone.utc
        ),
        time_allotment=900.0,
        action_deployment=ActionDeployment(
            id="ActionDeployment-1721293506-ct95mkvw",
            project_id="eeee8eb0-ef38-4bcd-8ebb-c4808751fdc2",
            action_definition=ActionDefinition(
                id="45139b0e-b200-4269-b156-b1cb94ab58bb",
                project_id="eeee8eb0-ef38-4bcd-8ebb-c4808751fdc2",
                technical_name="emitevery",
                version="1.7",
                kind=ActionKind.TRIGGER,
                container_image="",
                human_name="Emit Every",
                description="Create a new run at a given rate",
                inputs=[
                    ActionInput(
                        id="ActionInput-1721293506-jfdk8jtx",
                        name="seconds",
                        type=ActionIOType.FLOAT,
                        display_name="Seconds",
                        help="Amount of Seconds",
                        optional=True,
                        enum_values=[],
                    ),
                    ActionInput(
                        id="ActionInput-1721293506-7u6h2jeq",
                        name="minutes",
                        type=ActionIOType.FLOAT,
                        display_name="Minutes",
                        help="Amount of minutes",
                        optional=True,
                        enum_values=[],
                    ),
                    ActionInput(
                        id="ActionInput-1721293506-eoyuuebb",
                        name="hours",
                        type=ActionIOType.FLOAT,
                        display_name="Hours",
                        help="Amount of Hours",
                        optional=True,
                        enum_values=[],
                    ),
                    ActionInput(
                        id="ActionInput-1721293506-gdtwr5m3",
                        name="days",
                        type=ActionIOType.FLOAT,
                        display_name="Days",
                        help="Amount of Days",
                        optional=True,
                        enum_values=[],
                    ),
                ],
                outputs=[
                    ActionOutput(
                        id="ActionOutput-1721293506-y0duofh7",
                        name="time",
                        type=ActionIOType.STRING,
                        display_name="The current time in ISO format",
                        help="Timestamp",
                        optional=False,
                        enum_values=[],
                    )
                ],
                resources=ActionResources(
                    id="ActionResources-1721293506-whwaz4fj",
                    cpu=None,
                    memory=None,
                    time=None,
                    gpu=None,
                ),
                has_dynamic_outputs=False,
            ),
            execution_type=ActionExecutionType.GRPC_V1,
            state=ActionDeploymentState.RUNNING,
            action_internal_endpoint=None,
            addons={},
            allocated_connection_id="ExecutionConnection-1721293506-vmv3d0pb",
            error_message="ContainerCreating",
            last_allocation_date=datetime.datetime(
                2024, 7, 18, 8, 54, 16, 769976, tzinfo=datetime.timezone.utc
            ),
            node_pool=None,
        ),
        inputs={"seconds": "5.0"},
        state=ExecutionConnectionState.RUNNING,
        error_message="",
        executor_id=None,
        started_at=datetime.datetime(
            2024, 7, 18, 9, 5, 9, 55773, tzinfo=datetime.timezone.utc
        ),
        ended_at=None,
        end_of_log_delimiter_queue=[],
        parent_execution_result=None,
    )
    on_start_execution(execution)
