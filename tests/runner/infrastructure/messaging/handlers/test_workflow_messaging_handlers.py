from unittest.mock import AsyncMock, MagicMock

from aio_pika import IncomingMessage
from google.protobuf.struct_pb2 import Struct

from ryax.common.domain.internal_messaging.message_bus import IMessageBus
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDynamicOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    DynamicOutputOrigin,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_commands import (
    CreateWorkflowDeploymentCommand,
)
from ryax.runner.infrastructure.messaging.handlers.workflow_rmq_handler import (
    on_deploy_workflow,
)
from ryax.common.infrastructure.messaging.messages.studio_messages_pb2 import (
    WorkflowDeploy,
)


async def test_handle_workflow_deploy_event():
    addon_entry = Struct()
    addon_entry.update({"foo": "bar"})
    static_values_entry = Struct()
    static_values_entry.update({"test_static": 123, "test_bool": False})

    external_event = WorkflowDeploy(
        name="wf",
        project_id="test",
        id="ID",
        actions=[
            WorkflowDeploy.WorkflowAction(
                workflow_action_id="wmid1",
                action_id="1",
                project_id="test",
                technical_name="t0",
                kind=WorkflowDeploy.WorkflowAction.PROCESSOR,
                inputs=[
                    WorkflowDeploy.WorkflowActionIO(
                        technical_name="input1",
                        type=WorkflowDeploy.WorkflowActionIO.FLOAT,
                        display_name="My Input 1",
                        optional=True,
                        help="help message",
                    )
                ],
                addons_inputs=[
                    WorkflowDeploy.WorkflowActionIO(
                        technical_name="input2",
                        type=WorkflowDeploy.WorkflowActionIO.STRING,
                        addon_name="hpc",
                        display_name="My Input 2",
                    )
                ],
                outputs=[
                    WorkflowDeploy.WorkflowActionIO(
                        technical_name="output1",
                        type=WorkflowDeploy.WorkflowActionIO.BOOLEAN,
                        display_name="My output 2",
                    )
                ],
                dynamic_outputs=[
                    WorkflowDeploy.WorkflowActionIO(
                        technical_name="output2",
                        type=WorkflowDeploy.WorkflowActionIO.FILE,
                        display_name="My output 2",
                    )
                ],
                next_actions=["2"],
                file_values={"file1": "file"},
                static_values=static_values_entry,
                reference_values={
                    "toto": WorkflowDeploy.ReferenceValue(
                        workflow_action_id="wmid1", output_technical_name="1"
                    )
                },
                action_addons={"hpc": addon_entry},
            )
        ],
        results=[
            WorkflowDeploy.WorkflowResult(
                workflow_result_id="result",
                workflow_action_io_id="io",
                key="foo",
                technical_name="bar",
                action_id="actionid",
            )
        ],
    )
    ActionDynamicOutput.new_id = MagicMock(return_value="toto")
    message = MagicMock(IncomingMessage, body=external_event.SerializeToString())
    message_bus = AsyncMock(IMessageBus)

    await on_deploy_workflow(message, message_bus)

    internal_event_workflow = CreateWorkflowDeploymentCommand(
        workflow_definition_id="ID",
        workflow_name="wf",
        project_id="test",
        actions=[
            CreateWorkflowDeploymentCommand.Action(
                id_in_studio="wmid1",
                next_actions=["2"],
                constraints=CreateWorkflowDeploymentCommand.Action.ActionConstraints(
                    arch_list=[],
                    site_list=[],
                    site_type_list=[],
                    node_pool_list=[],
                ),
                objectives=CreateWorkflowDeploymentCommand.Action.ActionObjectives(
                    energy=0, cost=0, performance=0
                ),
                static_inputs={"test_static": 123, "test_bool": False, "file1": "file"},
                reference_inputs={"toto": {"action": "wmid1", "output": "1"}},
                dynamic_outputs=[
                    ActionDynamicOutput(
                        id="toto",
                        name="output2",
                        type=ActionIOType.FILE,
                        display_name="My output 2",
                        help="",
                        optional=False,
                        origin=DynamicOutputOrigin.PATH,
                        enum_values=[],
                    )
                ],
                addons={"hpc": {"foo": "bar"}},
                action_definition_id="1",
                resources=CreateWorkflowDeploymentCommand.Action.ActionResources(
                    cpu=0.0, memory=0, time=0.0, gpu=0
                ),
            ),
        ],
        results=[
            CreateWorkflowDeploymentCommand.Result(
                key="foo",
                technical_name="bar",
                workflow_action_io_id="io",
                action_id="actionid",
            )
        ],
    )

    message_bus.handle_command.assert_called_with(internal_event_workflow)
