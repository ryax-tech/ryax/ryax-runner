import logging

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionResources,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.action_deployment.action_deployment_commands import (
    DeployActionDeploymentCommand,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionExecutionType,
)
from ryax.runner.infrastructure.messaging.handlers.action_deployment_rmq_handler import (
    on_deploy_action,
)


def test_on_action_deploy():
    event = DeployActionDeploymentCommand(
        action_deployment_id="",
        deployment_type="toto",
        log_level=logging.INFO,
        resources=ActionResources(
            id="123",
            cpu=12,
            memory=1024,
            time=100,
            gpu=1,
        ),
        version="12",
        execution_type=ActionExecutionType.GRPC_V1,
        name="test",
        parameters={"a": 23},
        container_image="mycontainer",
        kind=ActionKind.TRIGGER,
        node_pool_name="toto",
    )
    on_deploy_action(event)
