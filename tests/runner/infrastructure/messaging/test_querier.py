import asyncio
import os

from ryax.common.infrastructure.messaging.consumer import RabbitMQMessageConsumer
from ryax.common.infrastructure.messaging.messages.runner_messages_pb2 import (
    InitTaskReply,
)
from ryax.common.infrastructure.messaging.messages.worker_messages_pb2 import (
    InitTaskRequest,
)
from ryax.common.infrastructure.messaging.querier import RabbitMQMessageQuerier
from ryax.runner.container import ApplicationContainer as RunnerApp
from ryax.runner.infrastructure.messaging.handlers import execution_rmq_handler
from ryax.worker.container import ApplicationContainer as WorkerApp
from ryax.runner.infrastructure.messaging.setup import setup as runner_messaging_setup
from ryax.worker.infrastructure.messaging.setup import setup as worker_messaging_setup
from ryax.worker.domain.execution.execution_events import InitTaskCommand


class MyMessageType:
    pass


async def test_query():
    runner_app = RunnerApp()
    worker_app = WorkerApp()
    try:
        engine = runner_app.messaging_engine(connection_url=os.environ["RYAX_BROKER"])
        await engine.connect()

        messaging_consumer: RabbitMQMessageConsumer = runner_app.message_consumer()
        task_id = "1"
        workflow_run_id = "2"

        async def mock_handler(message):
            return InitTaskReply(task_id=task_id, workflow_run_id=workflow_run_id)

        execution_rmq_handler.on_init_execution_connection = mock_handler
        runner_messaging_setup(runner_app)
        await messaging_consumer.start()

        request_message = InitTaskRequest(
            execution_id="123", executor_id="456", log_delimiter="789"
        )

        # Run a query
        engine = worker_app.messaging_engine(connection_url=os.environ["RYAX_BROKER"])
        await engine.connect()

        querier: RabbitMQMessageQuerier = worker_app.message_querier()
        worker_messaging_setup(worker_app)

        response = await asyncio.wait_for(
            querier.query(
                InitTaskCommand(
                    execution_id=request_message.execution_id,
                    executor_id=request_message.executor_id,
                    log_delimiter=request_message.log_delimiter,
                )
            ),
            timeout=5,
        )
        assert response == (task_id, workflow_run_id)
        # Run it twice to test channel
        response = await asyncio.wait_for(
            querier.query(
                InitTaskCommand(
                    execution_id=request_message.execution_id,
                    executor_id=request_message.executor_id,
                    log_delimiter=request_message.log_delimiter,
                )
            ),
            timeout=5,
        )
        assert response == (task_id, workflow_run_id)
    finally:
        await runner_app.messaging_engine().disconnect()
        await worker_app.messaging_engine().disconnect()
