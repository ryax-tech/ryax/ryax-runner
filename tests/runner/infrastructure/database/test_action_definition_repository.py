import pytest
from sqlalchemy import select
from sqlalchemy.orm import Session

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionInput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_errors import (
    ActionDefinitionNotFoundError,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.infrastructure.database.repositories.action_definition_repository import (
    DatabaseActionDefinitionRepository,
)


@pytest.fixture
def action_definition():
    test_action_id = "c50f8fae-e116-4db0-8c7b-d6a790a48ef4"
    test_project = "project_id"
    test_technical_name = "technical_name"
    test_human_name = "hm"
    test_description = "descr"
    test_version = "test_version"
    test_kind = ActionKind.PROCESSOR.value
    test_container_image = "container"
    return ActionDefinition(
        id=test_action_id,
        project_id=test_project,
        technical_name=test_technical_name,
        human_name=test_human_name,
        description=test_description,
        version=test_version,
        kind=test_kind,
        container_image=test_container_image,
        inputs=[
            ActionInput(
                id="6099c935-9f21-438b-89df-9f8cf35d0999",
                name="inputname",
                type=ActionIOType.INTEGER,
                display_name="hm",
                help="help",
            )
        ],
        outputs=[
            ActionOutput(
                id="ea71aad3-5e17-48ef-bc7e-f784d17015be",
                name="outputname",
                type=ActionIOType.INTEGER,
                display_name="hm",
                help="help",
            )
        ],
    )


async def test_delete_action_definition(
    app_container: ApplicationContainer,
    database_session: Session,
    action_definition: ActionDefinition,
) -> None:
    database_session.add(action_definition)
    database_session.commit()

    action_definition_repository: DatabaseActionDefinitionRepository = (
        app_container.action_definition_repository(session=database_session)
    )
    action_definition_repository.delete(action_definition)
    database_session.commit()
    assert list(database_session.execute(select(ActionDefinition.id))) == []


async def test_get_action_definition(
    app_container: ApplicationContainer,
    database_session: Session,
    action_definition: ActionDefinition,
) -> None:
    """Get should return action definition aggregate"""
    database_session.add(action_definition)
    database_session.commit()
    action_definition_repository: DatabaseActionDefinitionRepository = (
        app_container.action_definition_repository(session=database_session)
    )
    assert action_definition_repository.get(action_definition.id) == action_definition


async def test_get_action_definition_when_not_found(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    """Get should return action definition aggregate"""
    test_action_id = "foo"
    action_definition_repository: DatabaseActionDefinitionRepository = (
        app_container.action_definition_repository(session=database_session)
    )
    with pytest.raises(ActionDefinitionNotFoundError):
        action_definition_repository.get(test_action_id)


async def test_get_action_definition_by_technical_name_version(
    app_container: ApplicationContainer,
    database_session: Session,
    action_definition: ActionDefinition,
) -> None:
    """Get should return action definition aggregate"""
    database_session.add(action_definition)
    database_session.commit()
    action_definition_repository: DatabaseActionDefinitionRepository = (
        app_container.action_definition_repository(session=database_session)
    )
    assert (
        action_definition_repository.get_by_technical_name_and_version(
            action_definition.technical_name,
            action_definition.version,
            action_definition.project_id,
        )
        == action_definition
    )


async def test_get_action_definition_by_technical_name_version_when_not_found(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    """Get should return action definition aggregate"""
    test_technical_name = "foo"
    test_version = "1"
    test_project = "project_id"
    action_definition_repository: DatabaseActionDefinitionRepository = (
        app_container.action_definition_repository(session=database_session)
    )
    with pytest.raises(ActionDefinitionNotFoundError):
        action_definition_repository.get_by_technical_name_and_version(
            test_technical_name, test_version, test_project
        )
