import pytest

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.stored_file.storage_exceptions import StorageFileNotFoundError
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.common.infrastructure.database.engine import Session
from ryax.runner.infrastructure.database.repositories.stored_file_repository import (
    DatabaseStoredFileRepository,
)


class TestStoredFileRepository:
    def test_add_and_get_stored_file(
        self,
        app_container: ApplicationContainer,
        database_session: Session,
    ):
        """Get should return stored file aggregate"""
        test_id = "c50f8fae-e116-4db0-8c7b-d6a790a48ef4"
        test_output_id = "output_id1"
        test_path = "path/to/the/file"
        test_project_id = "project1"
        test_mimetype = "mime"
        test_encoding = "utf8"
        test_size = 23
        stored_file = StoredFile(
            id=test_id,
            io_name=test_output_id,
            file_path=test_path,
            project_id=test_project_id,
            mimetype=test_mimetype,
            encoding=test_encoding,
            size_in_bytes=test_size,
        )
        database_session.add(stored_file)
        database_session.commit()

        stored_file_repository: DatabaseStoredFileRepository = (
            app_container.stored_file_repository(session=database_session)
        )
        assert stored_file_repository.get(test_id, test_project_id) == stored_file

    async def test_get_stored_file_by_file_and_project_when_none_found(
        self,
        app_container: ApplicationContainer,
        database_session: Session,
    ):
        stored_file_repository: DatabaseStoredFileRepository = (
            app_container.stored_file_repository(session=database_session)
        )
        with pytest.raises(StorageFileNotFoundError):
            stored_file_repository.get(
                "c50f8fae-e116-4db0-8c7b-d6a790a48ef4", "test_project"
            )
