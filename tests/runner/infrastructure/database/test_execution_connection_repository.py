import datetime

from ryax.common.domain.registration.registration_values import SiteType
from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionKind,
    ActionIOType,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.execution_connection.execution_connection_values import (
    ExecutionConnectionState,
)
from ryax.runner.domain.site.site_entities import Site, NodePool
from ryax.runner.domain.workflow_action.workflow_action import (
    WorkflowAction,
    StaticInput,
    ReferenceInput,
    SchedulingConstraints,
    SchedulingObjectives,
)
from ryax.common.infrastructure.database.engine import Session, SqlalchemyDatabaseEngine
from ryax.runner.infrastructure.database.repositories.execution_connection_repository import (
    DatabaseExecutionConnectionRepository,
)


class TestExecutionConnectionRepository:
    def test_get_execution_connection_aggregate(
        self,
        app_container: ApplicationContainer,
        database_session: Session,
    ) -> None:
        """Get should return execution connection aggregate"""
        _id = "c50f8fae-e116-4db0-8c7b-d6a790a48ef4"
        _project = "project_id"
        _state = ExecutionConnectionState.PENDING
        _submitted_at = datetime.datetime.fromisoformat("2019-12-04 11:21:31+00:00")
        execution_connection = ExecutionConnection(
            id=_id,
            project_id=_project,
            state=_state,
            submitted_at=_submitted_at,
            workflow_action=None,
            time_allotment=100,
            workflow_run_id="123",
        )
        database_session.add(execution_connection)
        database_session.commit()
        execution_connection_repository: DatabaseExecutionConnectionRepository = (
            app_container.execution_connection_repository(session=database_session)
        )
        assert execution_connection_repository.get(_id) == execution_connection

    def test_get_execution_connection_eager_load(
        self,
        app_container: ApplicationContainer,
        database_engine: SqlalchemyDatabaseEngine,
    ) -> None:
        """Get should return execution connection aggregate"""
        database_session = database_engine.get_session()
        action_def1 = ActionDefinition(
            id="mdf1",
            project_id="project_id",
            technical_name="tmdf1",
            human_name="action human_name",
            description="descr",
            version="1.0",
            kind=ActionKind.TRIGGER,
            container_image="ctn1",
            inputs=[],
            outputs=[
                ActionOutput(
                    id="file",
                    name="filename",
                    type=ActionIOType.FILE,
                    enum_values=[],
                    display_name="",
                    help="",
                )
            ],
        )
        static_input_1 = StaticInput(id="foo", name="baz", value="bar")
        database_session.add(static_input_1)
        reference_input_1 = ReferenceInput(
            id="baz",
            name="ref_input_name",
            action_name="Workflowaction1",
            output_name="action_output",
        )
        workflow_action = WorkflowAction(
            id="Workflowaction1",
            id_in_studio="WorkflowactionNAME1",
            workflow_deployment_id="workflow_deployment_id1",
            workflow_definition_id="workflow_definition_id1",
            definition=action_def1,
            static_inputs=[static_input_1],
            reference_inputs=[reference_input_1],
            constraints=None,
            objectives=None,
        )
        database_session.add(workflow_action)

        _id = "c50f8fae-e116-4db0-8c7b-d6a790a48ef4"
        _project = "project_id"
        _state = ExecutionConnectionState.PENDING
        _submitted_at = datetime.datetime.fromisoformat("2019-12-04 11:21:31+00:00")
        execution_connection = ExecutionConnection(
            id=_id,
            project_id=_project,
            state=_state,
            submitted_at=_submitted_at,
            workflow_action=workflow_action,
            time_allotment=100,
            workflow_run_id="123",
        )
        database_session.add(execution_connection)
        database_session.commit()
        database_session.close()

        # Test with another session
        database_session = database_engine.get_session()
        execution_connection_repository: DatabaseExecutionConnectionRepository = (
            app_container.execution_connection_repository(session=database_session)
        )
        exec_connection = execution_connection_repository.get(_id, eager_load=True)
        database_session.close()

        assert exec_connection.workflow_action.reference_inputs[0] == reference_input_1
        assert exec_connection.workflow_action.static_inputs[0] == static_input_1
        assert (
            exec_connection.workflow_action.definition.outputs[0]
            == action_def1.outputs[0]
        )

    def test_get_all_pending(
        self,
        app_container: ApplicationContainer,
        database_session: Session,
    ):
        """Test get all pending"""

        _id = "myid"
        _project = "project_id"
        _state = ExecutionConnectionState.PENDING
        _submitted_at = [
            datetime.datetime.fromisoformat("2019-12-04 11:31:31+00:00"),
            datetime.datetime.fromisoformat("2019-12-04 11:11:31+00:00"),
            datetime.datetime.fromisoformat("2019-12-04 11:41:31+00:00"),
        ]
        addons = {"a": {}}
        i = 10

        # add site
        site = Site(id="1", name="testSite", type=SiteType.KUBERNETES)
        site.node_pools = [
            NodePool(id="1", name="noddPool1", site=site, cpu=1, memory=2000)
        ]
        database_session.add(site)
        workflow_action = WorkflowAction(
            id="123",
            id_in_studio="123",
            workflow_deployment_id="",
            workflow_definition_id="",
            definition=ActionDefinition(
                id="456",
                project_id="",
                technical_name="",
                version="",
                kind=ActionKind.TRIGGER,
                container_image="",
                human_name="",
                description="",
                inputs=[],
                outputs=[],
            ),
            reference_inputs=[],
            static_inputs=[],
            addons=addons,
            constraints=SchedulingConstraints(
                arch_list=[],
                site_list=[site],
                site_type_list=[site.type],
                node_pool_list=site.node_pools,
            ),
            objectives=SchedulingObjectives(energy=0, cost=0, performance=0),
        )
        for _submitted_at_item in _submitted_at:
            i += 1

            database_session.add(
                ExecutionConnection(
                    id=_id + str(i),
                    project_id=_project,
                    workflow_run_id="",
                    state=_state,
                    submitted_at=_submitted_at_item,
                    time_allotment=100,
                    workflow_action=workflow_action,
                )
            )

            i += 1
            database_session.add(
                ExecutionConnection(
                    id=_id + "RUN" + str(i),
                    project_id=_project,
                    workflow_run_id="",
                    state=ExecutionConnectionState.RUNNING,
                    submitted_at=_submitted_at_item,
                    time_allotment=100,
                    workflow_action=workflow_action,
                )
            )

        execution_connection_repository: DatabaseExecutionConnectionRepository = (
            app_container.execution_connection_repository(session=database_session)
        )
        _submitted_at.sort()
        all_pending = execution_connection_repository.list_pending()
        i = 0
        for connection in all_pending:
            assert "RUN" not in connection.id
            assert connection.submitted_at == _submitted_at[i]
            assert connection.action_definition_id == workflow_action.definition.id
            assert connection.project_id == _project
            assert connection.is_a_trigger is True
            assert connection.site_list == workflow_action.constraints.site_list
            assert (
                connection.node_pool_list == workflow_action.constraints.node_pool_list
            )
            assert (
                connection.site_type_list == workflow_action.constraints.site_type_list
            )
            i += 1
