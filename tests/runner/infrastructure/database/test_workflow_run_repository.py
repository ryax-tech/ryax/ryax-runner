from datetime import datetime, timedelta

from sqlalchemy import select

from ryax.runner.container import ApplicationContainer
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.stored_file.stored_file import StoredFile
from ryax.runner.domain.workflow_action.workflow_action import (
    WorkflowAction,
    ReferenceInput,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import (
    ActionRun,
    ActionRunState,
    WorkflowRun,
    WorkflowRunState,
)
from ryax.common.infrastructure.database.engine import Session
from ryax.runner.infrastructure.database.repositories.workflow_run_repository import (
    DatabaseWorkflowRunRepository,
)
from tests.helper import now


def generate_workflow_run(id_suffix="wfr1") -> WorkflowRun:
    workflow_action_2 = WorkflowAction(
        id="wfa-2" + id_suffix,
        id_in_studio="123",
        workflow_deployment_id="",
        workflow_definition_id="",
        definition=ActionDefinition(
            id="actdef-2-" + id_suffix,
            project_id="",
            technical_name="",
            version="",
            kind=ActionKind.PROCESSOR,
            container_image="",
            human_name="",
            description="",
            inputs=[],
            outputs=[],
        ),
        reference_inputs=[],
        static_inputs=[],
        constraints=None,
        objectives=None,
    )
    workflow_action_1 = WorkflowAction(
        id="wfa-1" + id_suffix,
        id_in_studio="123",
        workflow_deployment_id="",
        workflow_definition_id="",
        definition=ActionDefinition(
            id="actdef-1-" + id_suffix,
            project_id="",
            technical_name="",
            version="",
            kind=ActionKind.TRIGGER,
            container_image="",
            human_name="",
            description="",
            inputs=[],
            outputs=[],
        ),
        reference_inputs=[
            ReferenceInput(
                id="ref" + id_suffix, name="test", action_name="", output_name=""
            )
        ],
        static_inputs=[],
        next_actions=[workflow_action_2],
        constraints=None,
        objectives=None,
    )

    return WorkflowRun(
        id="wfr" + id_suffix,
        state=WorkflowRunState.RUNNING,
        last_result_at=datetime.fromisoformat("2019-12-04 11:21:31+00:00"),
        workflow_definition_id="wfdef" + id_suffix,
        project_id="",
        runs=[
            ActionRun(
                id="actionrun-1-" + id_suffix,
                state=ActionRunState.RUNNING,
                execution_results=[
                    ExecutionResult(
                        id="exec-1-" + id_suffix,
                        workflow_run_id="wfr" + id_suffix,
                        project_id="",
                        execution_connection_id="",
                        started_at=datetime.fromisoformat("2019-12-04 11:21:31+00:00"),
                        submitted_at=datetime.fromisoformat(
                            "2019-12-04 11:21:31+00:00"
                        ),
                        state=ExecutionResultState.ERROR,
                        workflow_action=workflow_action_1,
                        stored_files=[],
                    ),
                    ExecutionResult(
                        id="exec-1-retry-" + id_suffix,
                        workflow_run_id="wfr" + id_suffix,
                        project_id="",
                        execution_connection_id="",
                        started_at=datetime.fromisoformat("2019-12-04 11:21:32+00:00"),
                        submitted_at=datetime.fromisoformat(
                            "2019-12-04 11:21:32+00:00"
                        ),
                        state=ExecutionResultState.SUCCESS,
                        workflow_action=workflow_action_1,
                        stored_files=[
                            StoredFile(
                                id="sf-1-" + id_suffix,
                                io_name="foo",
                                file_path="test/sf1",
                                project_id="project_id",
                                mimetype="",
                                size_in_bytes=123,
                                encoding="",
                            ),
                            StoredFile(
                                id="sf-2-" + id_suffix,
                                io_name="foo",
                                file_path="test/sf2",
                                project_id="project_id",
                                mimetype="",
                                size_in_bytes=123,
                                encoding="",
                            ),
                        ],
                    ),
                ],
                workflow_action=workflow_action_1,
                position=1,
            ),
            ActionRun(
                id="actionrun-2-" + id_suffix,
                state=ActionRunState.RUNNING,
                execution_results=[
                    ExecutionResult(
                        id="exec-2" + id_suffix,
                        workflow_run_id="wfr" + id_suffix,
                        project_id="",
                        execution_connection_id="",
                        started_at=datetime.fromisoformat("2019-12-04 11:21:31+00:00"),
                        submitted_at=datetime.fromisoformat(
                            "2019-12-04 11:21:31+00:00"
                        ),
                        state=ExecutionResultState.ERROR,
                        workflow_action=workflow_action_2,
                    ),
                    ExecutionResult(
                        id="exec-retry1-2" + id_suffix,
                        workflow_run_id="wfr" + id_suffix,
                        project_id="",
                        execution_connection_id="",
                        started_at=datetime.fromisoformat("2019-12-04 11:21:32+00:00"),
                        submitted_at=datetime.fromisoformat(
                            "2019-12-04 11:21:32+00:00"
                        ),
                        state=ExecutionResultState.ERROR,
                        workflow_action=workflow_action_2,
                    ),
                    ExecutionResult(
                        id="exec-retry2-2" + id_suffix,
                        workflow_run_id="wfr" + id_suffix,
                        project_id="",
                        execution_connection_id="",
                        started_at=datetime.fromisoformat("2019-12-04 11:21:33+00:00"),
                        submitted_at=datetime.fromisoformat(
                            "2019-12-04 11:21:33+00:00"
                        ),
                        state=ExecutionResultState.SUCCESS,
                        workflow_action=workflow_action_2,
                    ),
                ],
                workflow_action=workflow_action_2,
                position=2,
            ),
        ],
        number_of_actions=2,
        number_of_completed_actions=2,
        started_at=datetime.fromisoformat("2019-12-04 11:21:31+00:00"),
    )


def test_workflow_run_get(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    """Get should return execution connection aggregate"""
    workflow_run = generate_workflow_run()
    database_session.add(workflow_run)
    workflow_run_repository: DatabaseWorkflowRunRepository = (
        app_container.workflow_run_repository(session=database_session)
    )
    db_workflow_run = workflow_run_repository.get(workflow_run.id)
    assert db_workflow_run == workflow_run


def test_workflow_run_list(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    """Get should return execution connection aggregate"""
    workflow_runs = []
    for i in range(5):
        workflow_runs.append(generate_workflow_run(str(i)))
    database_session.add_all(workflow_runs)
    workflow_run_repository: DatabaseWorkflowRunRepository = (
        app_container.workflow_run_repository(session=database_session)
    )
    db_workflow_runs, _, _, _ = workflow_run_repository.list_with_filter()
    assert db_workflow_runs == workflow_runs
    assert "exec-retry2-2" in db_workflow_runs[0].runs[1].execution_results[-1].id


def test_workflow_run_list_with_filter_on_workflow_def(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    """Get should return execution connection aggregate"""
    workflow_runs = []
    for i in range(5):
        workflow_runs.append(generate_workflow_run(str(i)))
    database_session.add_all(workflow_runs)
    workflow_run_repository: DatabaseWorkflowRunRepository = (
        app_container.workflow_run_repository(session=database_session)
    )
    db_workflow_runs, _, _, _ = workflow_run_repository.list_with_filter("wfdef0")
    assert db_workflow_runs == [workflow_runs[0]]


def test_workflow_run_delete(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    """Get should return execution connection aggregate"""
    workflow_runs = []
    for i in range(5):
        workflow_runs.append(generate_workflow_run(str(i)))
    database_session.add_all(workflow_runs)
    workflow_run_repository: DatabaseWorkflowRunRepository = (
        app_container.workflow_run_repository(session=database_session)
    )
    assert (
        database_session.execute(
            select(ExecutionResult).where(ExecutionResult.id == "exec-1-0")
        )
        .unique()
        .one_or_none()
        is not None
    )

    workflow_run_repository.delete("wfr0")

    db_workflow_runs, _, _, _ = workflow_run_repository.list_with_filter()
    assert (
        database_session.execute(
            select(ExecutionResult).where(ExecutionResult.id == "exec-1-0")
        )
        .unique()
        .one_or_none()
        is None
    )
    assert db_workflow_runs == workflow_runs[1:]


def test_get_stored_files_to_delete(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    workflow_runs = []
    for i in range(40):
        workflow_runs.append(generate_workflow_run(str(i)))
    database_session.add_all(workflow_runs)
    database_session.commit()

    workflow_run_repository: DatabaseWorkflowRunRepository = (
        app_container.workflow_run_repository(session=database_session)
    )
    all_stored_files = database_session.execute(select(StoredFile)).scalars().all()
    assert all_stored_files is not None

    to_delete = workflow_run_repository.get_stored_files_to_delete(
        [run.id for run in workflow_runs[0:2]]
    )
    assert len(to_delete) == 4


def test_workflow_run_delete_all(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    workflow_runs = []
    for i in range(40):
        workflow_runs.append(generate_workflow_run(str(i)))
    database_session.add_all(workflow_runs)
    deployment = WorkflowDeployment(
        id="workflow_id1",
        name="workflow_name1",
        project_id="",
        creation_time=str(datetime.now()),
        workflow_definition_id="workflow_def_id1",
        state=WorkflowDeploymentState.RUNNING,
        actions=[
            workflow_runs[0].runs[0].workflow_action,
            workflow_runs[0].runs[1].workflow_action,
        ],
    )
    database_session.add(deployment)
    database_session.commit()

    workflow_run_repository: DatabaseWorkflowRunRepository = (
        app_container.workflow_run_repository(session=database_session)
    )
    assert database_session.get(ExecutionResult, "exec-1-0") is not None

    workflow_run_repository.delete_all([run.id for run in workflow_runs[0:2]])

    db_workflow_runs = workflow_run_repository.list_all()
    assert database_session.get(ExecutionResult, "exec-1-0") is None
    assert db_workflow_runs == workflow_runs[2:]
    assert database_session.get(WorkflowDeployment, deployment.id) is None


def test_workflow_run_delete_oldest(
    app_container: ApplicationContainer,
    database_session: Session,
) -> None:
    workflow_runs = []
    for i in range(5):
        workflow_run = generate_workflow_run("-p1-" + str(i))
        workflow_run.last_result_at = now + timedelta(seconds=i)
        workflow_run.project_id = "project1"
        workflow_runs.append(workflow_run)

    for i in range(5):
        workflow_run = generate_workflow_run("-p2-" + str(i))
        workflow_run.last_result_at = now + timedelta(seconds=i)
        workflow_run.project_id = "project2"
        workflow_runs.append(workflow_run)

    database_session.add_all(workflow_runs)
    database_session.commit()

    workflow_run_repository: DatabaseWorkflowRunRepository = (
        app_container.workflow_run_repository(session=database_session)
    )

    workflow_run_repository.delete_oldest(3)

    db_workflow_runs = workflow_run_repository.list_all()

    assert len(db_workflow_runs) == 6
    assert [
        db_workflow_run
        for db_workflow_run in db_workflow_runs
        if db_workflow_run.project_id == "project1"
    ] == workflow_runs[2:5]
    assert [
        db_workflow_run
        for db_workflow_run in db_workflow_runs
        if db_workflow_run.project_id == "project2"
    ] == workflow_runs[7:10]
