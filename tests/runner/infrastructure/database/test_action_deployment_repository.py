import datetime

import pytest

from ryax.runner.domain.site.site_entities import NodePool
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionInput,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.action_deployment.action_deployment_entities import (
    ActionDeployment,
)
from ryax.runner.domain.action_deployment.action_deployment_repository import (
    ActionDeploymentNotFoundError,
)
from ryax.runner.domain.action_deployment.action_deployment_values import (
    ActionDeploymentState,
    ActionExecutionType,
)
from ryax.runner.domain.execution_connection.execution_connection_entities import (
    ExecutionConnection,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.infrastructure.database.repositories.action_deployment_repository import (
    DatabaseActionDeploymentRepository,
)


@pytest.fixture()
def action_definition() -> ActionDefinition:
    return ActionDefinition(
        id="action_id",
        version="action_version",
        technical_name="action technical name",
        human_name="action human_name",
        description="descr",
        kind=ActionKind.PUBLISHER,
        container_image="test",
        project_id="",
        inputs=[
            ActionInput(
                id="io0",
                name="io0name",
                type=ActionIOType.BYTES,
                display_name="io0",
                help="help",
                enum_values=[],
            )
        ],
        outputs=[
            ActionOutput(
                id="io0",
                name="foo",
                type=ActionIOType.BYTES,
                display_name="io0",
                help="help",
                enum_values=[],
            ),
            ActionOutput(
                id="io1",
                name="foo",
                type=ActionIOType.FLOAT,
                display_name="io1",
                help="help",
                enum_values=[],
            ),
        ],
    )


@pytest.fixture()
def node_pool() -> NodePool:
    return NodePool(
        id="node_pool_1", name="mynodepool", site=None, cpu=1, memory=1024 * 1024
    )


@pytest.fixture()
def action_deployment(
    action_definition: ActionDefinition,
    node_pool: NodePool,
    action_id: str = "new_action_definition_id",
) -> ActionDeployment:
    return ActionDeployment(
        id=action_id,
        project_id="1",
        action_definition=action_definition,
        execution_type=ActionExecutionType.GRPC_V1,
        node_pool=node_pool,
    )


def test_get_by_id(
    action_deployment_repository: DatabaseActionDeploymentRepository,
    action_deployment: ActionDeployment,
):
    action_deployment_repository.add(action_deployment)
    action_deployment_repository.session.commit()

    assert (
        action_deployment_repository.get_by_id(action_deployment.id)
        == action_deployment
    )


def test_get_by_id_eager(
    action_deployment_repository: DatabaseActionDeploymentRepository,
    action_deployment: ActionDeployment,
):
    action_deployment_repository.add(action_deployment)
    action_deployment_repository.session.commit()

    assert (
        action_deployment_repository.get_by_id(action_deployment.id, eager_load=True)
        == action_deployment
    )


def test_get_by_id_not_found(action_deployment_repository):
    with pytest.raises(ActionDeploymentNotFoundError):
        action_deployment_repository.get_by_id("toto")


def test_get_by_workflow_definition_id(
    action_deployment_repository,
    action_definition: ActionDefinition,
    action_deployment: ActionDeployment,
):
    workflow_definition_id = "wfid"
    connection_id = "connid"
    action_deployment.allocated_connection_id = connection_id
    action_deployment_repository.add(action_deployment)
    workflow_action = WorkflowAction(
        id="1",
        id_in_studio="",
        workflow_definition_id=workflow_definition_id,
        workflow_deployment_id="",
        definition=action_definition,
        is_root_action=True,
        static_inputs=[],
        reference_inputs=[],
        constraints=None,
        objectives=None,
    )
    action_deployment_repository.session.add(
        ExecutionConnection(
            id=connection_id,
            project_id="",
            workflow_action=workflow_action,
            workflow_run_id="",
            submitted_at=datetime.datetime.now(tz=datetime.timezone.utc),
            time_allotment=100,
        )
    )
    action_deployment_repository.session.commit()

    assert (
        action_deployment_repository.get_by_workflow_definition_id(
            workflow_definition_id
        )
        == action_deployment
    )


def test_get_by_workflow_definition_id_found(action_deployment_repository):
    with pytest.raises(ActionDeploymentNotFoundError):
        action_deployment_repository.get_by_workflow_definition_id("toto")


def test_action_deployment_list_ready_to_be_used_by_action_definition(
    action_deployment_repository,
    action_definition,
    node_pool: NodePool,
):
    action_deps = [
        ActionDeployment(
            id="1",
            project_id="project_id",
            action_definition=action_definition,
            state=ActionDeploymentState.READY,
            allocated_connection_id=None,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="2",
            project_id="project_id",
            action_definition=action_definition,
            state=ActionDeploymentState.READY,
            allocated_connection_id=None,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="3",
            project_id="project_id",
            action_definition=action_definition,
            state=ActionDeploymentState.RUNNING,
            allocated_connection_id=None,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="4",
            project_id="project_id",
            action_definition=action_definition,
            state=ActionDeploymentState.ERROR,
            allocated_connection_id=None,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="5",
            project_id="project_id",
            action_definition=action_definition,
            state=ActionDeploymentState.READY,
            allocated_connection_id="connectionid1",
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
    ]
    for action_dep in action_deps:
        action_deployment_repository.add(action_dep)
    action_deployment_repository.session.commit()
    ready_actions = action_deployment_repository.list_unallocated_and_ready_from_action_definition_id(
        action_definition.id, project_id="project_id", node_pool_id="node_pool_1"
    )
    for action in ready_actions:
        assert action.state == ActionDeploymentState.READY
    assert len(ready_actions) == 2
    assert ready_actions[0].id == action_deps[0].id
    assert ready_actions[1].id == action_deps[1].id


def test_list_eventually_ready_to_be_used_by_action_definition(
    action_deployment_repository, action_definition, node_pool
):
    action_deps = [
        ActionDeployment(
            id="1",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.READY,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="2",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.NONE,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="3",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.RUNNING,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="4",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.ERROR,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="5",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.UNDEPLOYED,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="6",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.DEPLOYING,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
    ]
    for action_dep in action_deps:
        action_deployment_repository.add(action_dep)
    action_deployment_repository.session.commit()
    eventually_ready_actions = (
        action_deployment_repository.list_eventually_ready_from_action_definition_id(
            action_definition.id, project_id="1", node_pool_id="node_pool_1"
        )
    )
    for action in eventually_ready_actions:
        assert (
            action.state != ActionDeploymentState.UNDEPLOYED
            and action.state != ActionDeploymentState.ERROR
            and action.state != ActionDeploymentState.NONE
        )
    assert len(eventually_ready_actions) == 3
    assert eventually_ready_actions[0].id == action_deps[0].id
    assert eventually_ready_actions[1].id == action_deps[2].id
    assert eventually_ready_actions[2].id == action_deps[5].id


def test_action_deployment_not_committed(
    action_deployment_repository: DatabaseActionDeploymentRepository,
    action_deployment: ActionDeployment,
):
    action_deployment_repository.add(action_deployment)
    action_deployment_repository.session.close()
    with pytest.raises(ActionDeploymentNotFoundError):
        action_deployment_repository.get_by_id(action_deployment.id)


def test_action_deployment_delete(
    action_deployment_repository: DatabaseActionDeploymentRepository,
    action_deployment: ActionDeployment,
):
    action_deployment_repository.add(action_deployment)
    action_deployment_repository.session.commit()
    action_deployment_repository.delete(action_deployment.id)
    action_deployment_repository.session.commit()
    with pytest.raises(ActionDeploymentNotFoundError):
        action_deployment_repository.get_by_id(action_deployment.id)


def test_action_deployment_delete_rollback(
    action_deployment_repository, action_deployment: ActionDeployment
):
    action_deployment_repository.add(action_deployment)
    action_deployment_repository.session.commit()
    action_deployment_repository.delete(action_deployment.id)
    action_deployment_repository.session.rollback()
    assert (
        action_deployment_repository.get_by_id(action_deployment.id)
        == action_deployment
    )


def test_list_and_count_all_eventually_ready(
    action_deployment_repository, action_definition, node_pool
):
    action_deps = [
        ActionDeployment(
            id="1",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.READY,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="2",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.NONE,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="3",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.RUNNING,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="4",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.ERROR,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="5",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.UNDEPLOYED,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
        ActionDeployment(
            id="6",
            project_id="1",
            action_definition=action_definition,
            state=ActionDeploymentState.DEPLOYING,
            execution_type=ActionExecutionType.GRPC_V1,
            node_pool=node_pool,
        ),
    ]
    for action_dep in action_deps:
        action_deployment_repository.add(action_dep)
    action_deployment_repository.session.commit()
    eventually_ready_actions = action_deployment_repository.list_all_in_progress()
    for action in eventually_ready_actions:
        assert (
            action.state != ActionDeploymentState.UNDEPLOYED
            and action.state != ActionDeploymentState.ERROR
            and action.state != ActionDeploymentState.NONE
        )
    assert len(eventually_ready_actions) == 3
    assert eventually_ready_actions[0].id == action_deps[0].id
    assert eventually_ready_actions[1].id == action_deps[2].id
    assert eventually_ready_actions[2].id == action_deps[5].id

    eventually_ready_actions_count = (
        action_deployment_repository.count_eventually_ready(node_pool_id="node_pool_1")
    )
    assert len(eventually_ready_actions) == eventually_ready_actions_count
