import datetime

import pytest

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)
from ryax.runner.domain.workflow_deployment.workflow_deployment_values import (
    WorkflowDeploymentState,
)
from ryax.runner.infrastructure.database.repositories.workflow_repository import (
    DatabaseWorkflowDeploymentRepository,
)


@pytest.fixture(scope="function")
def workflow_deployment_repository(
    database_session,
) -> DatabaseWorkflowDeploymentRepository:
    return DatabaseWorkflowDeploymentRepository(database_session)


def _generate_action_def(id_: str) -> ActionDefinition:
    return ActionDefinition(
        id=id_,
        project_id="project_id",
        technical_name=f"{id_}_tname",
        version="1",
        kind=ActionKind.PROCESSOR,
        container_image="container_image",
        human_name=id_,
        description="description",
        inputs=[],
        outputs=[],
    )


def test_is_there_non_stopped_workflows_for_this_action_definition(
    workflow_deployment_repository: DatabaseWorkflowDeploymentRepository,
) -> None:
    mod_def1 = _generate_action_def("moddef1")
    workflow_deployment_repository.add(
        WorkflowDeployment(
            id="wf_with_1_moddef1_STOPPED",
            name="name1",
            project_id="project_id",
            creation_time=datetime.datetime.fromtimestamp(0),
            workflow_definition_id="workflow_definition_id",
            state=WorkflowDeploymentState.STOPPED,
            actions=[
                WorkflowAction(
                    id="mod1",
                    id_in_studio="mod1",
                    workflow_deployment_id="mod1",
                    workflow_definition_id="mod1",
                    definition=mod_def1,
                    static_inputs=[],
                    reference_inputs=[],
                    constraints=None,
                    objectives=None,
                )
            ],
        )
    )
    workflow_deployment_repository.add(
        WorkflowDeployment(
            id="wf_with_2_moddef1_STOPPED",
            name="name1",
            project_id="project_id",
            creation_time=datetime.datetime.fromtimestamp(0),
            workflow_definition_id="workflow_definition_id",
            state=WorkflowDeploymentState.STOPPED,
            actions=[
                WorkflowAction(
                    id="mod2",
                    id_in_studio="mod2",
                    workflow_deployment_id="mod1",
                    workflow_definition_id="mod1",
                    definition=mod_def1,
                    static_inputs=[],
                    reference_inputs=[],
                    constraints=None,
                    objectives=None,
                ),
                WorkflowAction(
                    id="mod3",
                    id_in_studio="mod3",
                    workflow_deployment_id="mod2",
                    workflow_definition_id="mod2",
                    definition=mod_def1,
                    static_inputs=[],
                    reference_inputs=[],
                    constraints=None,
                    objectives=None,
                ),
            ],
        )
    )
    workflow_deployment_repository.add(
        WorkflowDeployment(
            id="wf_with_0_moddef1_STOPPED",
            name="name1",
            project_id="project_id",
            creation_time=datetime.datetime.fromtimestamp(0),
            workflow_definition_id="workflow_definition_id",
            state=WorkflowDeploymentState.STOPPED,
            actions=[
                WorkflowAction(
                    id="mod4",
                    id_in_studio="mod4",
                    workflow_deployment_id="mod1",
                    workflow_definition_id="mod1",
                    definition=_generate_action_def("moddef2"),
                    static_inputs=[],
                    reference_inputs=[],
                    constraints=None,
                    objectives=None,
                )
            ],
        )
    )
    workflow_deployment_repository.add(
        WorkflowDeployment(
            id="wf_with_1_moddef1_NON_STOPPED",
            name="name1",
            project_id="project_id",
            creation_time=datetime.datetime.fromtimestamp(0),
            workflow_definition_id="workflow_definition_id",
            state=WorkflowDeploymentState.RUNNING,
            actions=[
                WorkflowAction(
                    id="mod5",
                    id_in_studio="mod5",
                    workflow_deployment_id="mod1",
                    workflow_definition_id="mod1",
                    definition=mod_def1,
                    static_inputs=[],
                    reference_inputs=[],
                    constraints=None,
                    objectives=None,
                )
            ],
        )
    )
    workflow_deployment_repository.add(
        WorkflowDeployment(
            id="wf_with_0_moddef1_NON_STOPPED",
            name="name1",
            project_id="project_id",
            creation_time=datetime.datetime.fromtimestamp(0),
            workflow_definition_id="workflow_definition_id",
            state=WorkflowDeploymentState.RUNNING,
            actions=[
                WorkflowAction(
                    id="mod6",
                    id_in_studio="mod6",
                    workflow_deployment_id="mod1",
                    workflow_definition_id="mod1",
                    definition=_generate_action_def("moddef3"),
                    static_inputs=[],
                    reference_inputs=[],
                    constraints=None,
                    objectives=None,
                )
            ],
        )
    )
    workflow_deployment_repository.add(
        WorkflowDeployment(
            id="wf_with_2_moddef1_NON_STOPPED",
            name="name1",
            project_id="project_id",
            creation_time=datetime.datetime.fromtimestamp(0),
            workflow_definition_id="workflow_definition_id",
            state=WorkflowDeploymentState.STARTED,
            actions=[
                WorkflowAction(
                    id="mod7",
                    id_in_studio="mod7",
                    workflow_deployment_id="mod1",
                    workflow_definition_id="mod1",
                    definition=mod_def1,
                    static_inputs=[],
                    reference_inputs=[],
                    constraints=None,
                    objectives=None,
                ),
                WorkflowAction(
                    id="mod8",
                    id_in_studio="mod8",
                    workflow_deployment_id="mod2",
                    workflow_definition_id="mod2",
                    definition=mod_def1,
                    static_inputs=[],
                    reference_inputs=[],
                    constraints=None,
                    objectives=None,
                ),
            ],
        )
    )

    workflow_deployment_repository.session.commit()

    wfs = (
        workflow_deployment_repository.list_active_workflows_for_this_action_definition(
            action_definition_id="moddef1"
        )
    )
    wfs_id = [w.id for w in wfs]
    assert len(wfs) == 2
    assert "wf_with_1_moddef1_NON_STOPPED" in wfs_id
    assert "wf_with_2_moddef1_NON_STOPPED" in wfs_id

    wfs = (
        workflow_deployment_repository.list_active_workflows_for_this_action_definition(
            action_definition_id="NON_EXISTING_MOD_DEF_ID"
        )
    )
    assert len(wfs) == 0
