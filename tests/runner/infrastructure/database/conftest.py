import tempfile

import pytest
from sqlalchemy.orm import clear_mappers

from ryax.runner.application.setup import setup as application_setup
from ryax.runner.container import ApplicationContainer
from ryax.common.infrastructure.database.engine import SqlalchemyDatabaseEngine
from ryax.runner.infrastructure.database.mapper import map_orm
from ryax.runner.infrastructure.database.metadata import metadata
from ryax.runner.infrastructure.database.repositories.action_deployment_repository import (
    DatabaseActionDeploymentRepository,
)
from ryax.runner.infrastructure.database.repositories.execution_result_repository import (
    DatabaseExecutionResultRepository,
)
from ryax.runner.infrastructure.database.repositories.workflow_run_repository import (
    DatabaseWorkflowRunRepository,
)


@pytest.fixture(scope="function")
def app_container() -> ApplicationContainer:
    app_container = ApplicationContainer()
    application_setup(app_container)
    app_container.configuration.set("storage.local.type", "posix")
    yield app_container
    app_container.unwire()


@pytest.fixture()
def database_engine(app_container: ApplicationContainer) -> SqlalchemyDatabaseEngine:
    with tempfile.TemporaryDirectory() as tmp_dir:
        database_url = f"sqlite:///{tmp_dir}/ryax.db"
        engine = SqlalchemyDatabaseEngine(database_url, map_orm)
        engine.connect()
        app_container.database_engine.override(engine)
        yield engine
        clear_mappers()
        metadata.drop_all(engine._engine)
        engine.disconnect()


@pytest.fixture(scope="function")
def database_session(database_engine: SqlalchemyDatabaseEngine):
    session = database_engine.get_session()
    yield session
    session.close()


@pytest.fixture(scope="function")
def action_deployment_repository(
    database_session,
) -> DatabaseActionDeploymentRepository:
    return DatabaseActionDeploymentRepository(database_session)


@pytest.fixture(scope="function")
def workflow_run_repository(
    database_session,
) -> DatabaseWorkflowRunRepository:
    return DatabaseWorkflowRunRepository(database_session)


@pytest.fixture(scope="function")
def execution_result_repository(
    database_session, workflow_run_repository
) -> DatabaseExecutionResultRepository:
    return DatabaseExecutionResultRepository(database_session)
