import datetime
from unittest import mock

import pytest

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionOutput,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_execptions import (
    ExecutionResultNotFoundError,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_result.workflow_result import (
    WorkflowResult,
    WorkflowResultsNotReadyError,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import (
    WorkflowRun,
    WorkflowRunState,
)
from ryax.common.infrastructure.database.engine import Session
from ryax.runner.infrastructure.database.repositories.execution_result_repository import (
    DatabaseExecutionResultRepository,
)
from ryax.runner.infrastructure.database.repositories.workflow_run_repository import (
    DatabaseWorkflowRunRepository,
)


def test_get_log_delimiters_by_action_definition_and_project(
    execution_result_repository: DatabaseExecutionResultRepository,
    database_session: Session,
) -> None:
    with database_session as db:
        project_id = "project_id"
        action_definition = ActionDefinition(
            id="action_def_id",
            project_id=project_id,
            technical_name="tech",
            version="1.0",
            kind=ActionKind.PROCESSOR,
            container_image="cont",
            human_name="humname",
            description="desc",
            inputs=[],
            outputs=[],
        )
        unwanted_action_definition = ActionDefinition(
            id="action_def_id_unwanted",
            project_id=project_id,
            technical_name="tech",
            version="1.0",
            kind=ActionKind.PROCESSOR,
            container_image="cont",
            human_name="humname",
            description="desc",
            inputs=[],
            outputs=[],
        )
        workflow_action = WorkflowAction(
            id="wfmod",
            id_in_studio="name",
            workflow_deployment_id="wfdeploy",
            workflow_definition_id="wfdef",
            definition=action_definition,
            static_inputs=[],
            reference_inputs=[],
            next_actions=[],
            is_root_action=False,
            connection_execution_id=None,
            position=None,
            constraints=None,
            objectives=None,
        )
        unwanted_workflow_action = WorkflowAction(
            id="wfmod_unwanted",
            id_in_studio="name",
            workflow_deployment_id="wfdeploy",
            workflow_definition_id="wfdef",
            definition=unwanted_action_definition,
            static_inputs=[],
            reference_inputs=[],
            next_actions=[],
            is_root_action=False,
            connection_execution_id=None,
            position=None,
            constraints=None,
            objectives=None,
        )
        unwanted_execution_result = ExecutionResult(
            id="execresid_unwanted",
            project_id=project_id,
            execution_connection_id="connection_unwanted",
            workflow_run_id="wfrun_unwanted",
            workflow_action=unwanted_workflow_action,
            submitted_at=datetime.datetime.now(datetime.timezone.utc),
            started_at=datetime.datetime.now(datetime.timezone.utc),
            ended_at=datetime.datetime.now(datetime.timezone.utc),
            state=ExecutionResultState.SUCCESS,
            outputs={},
            inputs={},
            parents=[],
        )
        execution_result = ExecutionResult(
            id="execresid",
            project_id=project_id,
            execution_connection_id="connection_unwanted",
            end_of_log_delimiter=None,
            workflow_run_id="wfrun",
            workflow_action=workflow_action,
            submitted_at=datetime.datetime.now(datetime.timezone.utc),
            started_at=datetime.datetime.now(datetime.timezone.utc),
            ended_at=datetime.datetime.now(datetime.timezone.utc),
            state=ExecutionResultState.SUCCESS,
            outputs={},
            inputs={},
            parents=[],
        )

        db.add(action_definition)
        db.add(unwanted_action_definition)
        db.add(workflow_action)
        db.add(unwanted_workflow_action)
        db.add(execution_result)
        db.add(unwanted_execution_result)
        db.commit()


def test_get_by_id(
    execution_result_repository: DatabaseExecutionResultRepository,
) -> None:
    now = datetime.datetime.now(datetime.timezone.utc)
    execution_id = "new_execution_id"
    execution_result = ExecutionResult(
        id=execution_id,
        project_id="project_id",
        workflow_run_id="wfr1",
        submitted_at=now,
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.SUCCESS,
        inputs={"test": "toto"},
        outputs={"toto": 123},
        parents=[],
        workflow_action=None,
        execution_connection_id="",
    )
    execution_result_repository.add(execution_result)
    execution_result_repository.session.commit()
    assert execution_result_repository.get(execution_id) == execution_result


def test_get_all(
    execution_result_repository: DatabaseExecutionResultRepository,
) -> None:
    # TODO: test more cases
    now = datetime.datetime.now(datetime.timezone.utc)
    execution_result = ExecutionResult(
        id="new_execution_id",
        project_id="project_id",
        workflow_run_id="wfr1",
        submitted_at=now,
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.SUCCESS,
        execution_connection_id="1",
        workflow_action=None,
    )
    execution_result_2 = ExecutionResult(
        id="new_execution_2_id",
        project_id="project_id",
        workflow_run_id="wfr1",
        submitted_at=now - datetime.timedelta(minutes=1),
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.SUCCESS,
        execution_connection_id="1",
        workflow_action=None,
    )
    execution_result_3 = ExecutionResult(
        id="new_execution_3_id",
        project_id="project_id",
        workflow_run_id="wfr1",
        submitted_at=now,
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.ERROR,
        execution_connection_id="1",
        workflow_action=None,
    )
    current_project = "project_id"
    workflow_id = "workflow_deployment_id"
    workflow_action_id = "workflow_action_id"
    state = ExecutionResultState.SUCCESS
    submitted_before = None
    submitted_after = None
    _range = "0-10"

    execution_result_repository.add(execution_result)
    execution_result_repository.add(execution_result_2)
    execution_result_repository.add(execution_result_3)
    execution_result_repository.session.commit()
    result = execution_result_repository.list(
        current_project,
        workflow_id,
        workflow_action_id,
        state,
        submitted_before,
        submitted_after,
        _range,
    )
    assert result == ([execution_result_2, execution_result], 0, 2, 2)


def test_get_by_end_of_log_delimiter(
    execution_result_repository: DatabaseExecutionResultRepository,
) -> None:
    now = datetime.datetime.now(datetime.timezone.utc)
    execution_result = ExecutionResult(
        id="new_execution_2_id",
        project_id="project_id",
        workflow_run_id="wfr1",
        submitted_at=now - datetime.timedelta(minutes=2),
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.SUCCESS,
        execution_connection_id="1",
        workflow_action=None,
        end_of_log_delimiter="foo",
    )
    execution_result_1 = ExecutionResult(
        id="new_execution_3_id",
        project_id="project_id",
        workflow_run_id="wfr1",
        submitted_at=now - datetime.timedelta(minutes=3),
        started_at=now,
        ended_at=now,
        state=ExecutionResultState.ERROR,
        execution_connection_id="1",
        workflow_action=None,
        end_of_log_delimiter="other",
    )

    execution_result_repository.add(execution_result)
    execution_result_repository.add(execution_result_1)
    execution_result_repository.session.commit()
    (
        execution_result_id,
        execution_connection_id,
    ) = execution_result_repository.get_ids_by_end_of_log_delimiter("foo")
    assert execution_result_id == execution_result.id
    assert execution_connection_id == execution_result.execution_connection_id


def test_get_by_end_of_log_delimiter_when_not_found(
    execution_result_repository: DatabaseExecutionResultRepository,
) -> None:
    with pytest.raises(ExecutionResultNotFoundError):
        execution_result_repository.get_ids_by_end_of_log_delimiter("foo")


def test_get_workflow_results_when_no_execution_results_in_workflow_run(
    execution_result_repository: DatabaseExecutionResultRepository,
) -> None:
    workflow_run_id = "wfr1"
    workflow_run = WorkflowRun(
        id="",
        project_id="",
        state=WorkflowRunState.RUNNING,
        started_at=datetime.datetime.now(datetime.timezone.utc),
        last_result_at=datetime.datetime.now(datetime.timezone.utc),
        number_of_completed_actions=0,
        number_of_actions=0,
        workflow_definition_id="",
    )
    workflow_run_repository_mock = mock.MagicMock(DatabaseWorkflowRunRepository)
    workflow_run_repository_mock.get = mock.MagicMock(return_value=workflow_run)
    execution_result_repository.workflow_run_repository = workflow_run_repository_mock

    with pytest.raises(ExecutionResultNotFoundError):
        execution_result_repository.get_results_by_workflow_run_id(workflow_run_id)


def test_get_workflow_results_when_no_workflow_results_from_workflow_deployment(
    execution_result_repository: DatabaseExecutionResultRepository,
    database_session: Session,
) -> None:
    with database_session as db:
        now = datetime.datetime.now(datetime.timezone.utc)
        workflow_run_id = "wfr1"
        project_id = "project_id"
        action_definition = ActionDefinition(
            id="action_def_id",
            project_id=project_id,
            technical_name="tech",
            version="1.0",
            kind=ActionKind.PROCESSOR,
            container_image="cont",
            human_name="humname",
            description="desc",
            inputs=[],
            outputs=[],
        )
        workflow_action = WorkflowAction(
            id="wfmod",
            id_in_studio="name",
            workflow_deployment_id="WRONGWORKFLOWDEPLOYMENTID",
            workflow_definition_id="wfdef",
            definition=action_definition,
            static_inputs=[],
            reference_inputs=[],
            next_actions=[],
            is_root_action=False,
            connection_execution_id=None,
            position=None,
            constraints=None,
            objectives=None,
        )
        execution_result = ExecutionResult(
            id="new_execution_2_id",
            project_id="project_id",
            workflow_run_id=workflow_run_id,
            submitted_at=now - datetime.timedelta(minutes=2),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.SUCCESS,
            execution_connection_id="1",
            workflow_action=workflow_action,
            end_of_log_delimiter="foo",
        )
        db.add(workflow_action)
        db.add(action_definition)
        db.commit()
        execution_result_repository.add(execution_result)
        execution_result_repository.session.commit()

    workflow_run = WorkflowRun(
        id="",
        project_id="",
        state=WorkflowRunState.RUNNING,
        started_at=datetime.datetime.now(datetime.timezone.utc),
        last_result_at=datetime.datetime.now(datetime.timezone.utc),
        number_of_completed_actions=0,
        number_of_actions=0,
        workflow_definition_id="",
    )
    workflow_run_repository_mock = mock.MagicMock(DatabaseWorkflowRunRepository)
    workflow_run_repository_mock.get = mock.MagicMock(return_value=workflow_run)
    execution_result_repository.workflow_run_repository = workflow_run_repository_mock
    assert (
        execution_result_repository.get_results_by_workflow_run_id(workflow_run_id)
        == []
    )


def test_get_workflow_results(
    execution_result_repository: DatabaseExecutionResultRepository,
    database_session: Session,
) -> None:
    with database_session as db:
        now = datetime.datetime.now(datetime.timezone.utc)
        workflow_run_id = "wfr1"
        workflow_deployment_id = "wfedploy"
        project_id = "project_id"
        workflow_result_id = "workflow_result_id"
        result_technical_name = "techname"
        workflow_action_id = "wf_action_id"
        action_definition = ActionDefinition(
            id="action_def_id",
            project_id=project_id,
            technical_name="tech",
            version="1.0",
            kind=ActionKind.PROCESSOR,
            container_image="cont",
            human_name="humname",
            description="desc",
            inputs=[],
            outputs=[
                ActionOutput(
                    id="foobar",
                    name="techname",
                    type=ActionIOType.STRING,
                    display_name="foo",
                    help="bar",
                )
            ],
        )
        workflow_action = WorkflowAction(
            id="workflow_action_internal_id",
            id_in_studio=workflow_action_id,
            workflow_deployment_id=workflow_deployment_id,
            workflow_definition_id="wfdef",
            definition=action_definition,
            static_inputs=[],
            reference_inputs=[],
            next_actions=[],
            is_root_action=False,
            connection_execution_id=None,
            position=None,
            constraints=None,
            objectives=None,
        )
        workflow_result = WorkflowResult(
            id=workflow_result_id,
            key="foo",
            technical_name=result_technical_name,
            workflow_action_io_id="foobar",
            action_id=workflow_action_id,
            workflow_deployment_id=workflow_deployment_id,
        )
        execution_result = ExecutionResult(
            id="new_execution_2_id",
            project_id="project_id",
            workflow_run_id=workflow_run_id,
            submitted_at=now - datetime.timedelta(minutes=2),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.SUCCESS,
            execution_connection_id="1",
            workflow_action=workflow_action,
            end_of_log_delimiter="foo",
            outputs={result_technical_name: "test_value"},
        )
        db.add(workflow_action)
        db.add(action_definition)
        db.add(workflow_result)
        db.commit()
        execution_result_repository.add(execution_result)
        execution_result_repository.session.commit()

    workflow_run = WorkflowRun(
        id="",
        project_id="",
        state=WorkflowRunState.RUNNING,
        started_at=datetime.datetime.now(datetime.timezone.utc),
        last_result_at=datetime.datetime.now(datetime.timezone.utc),
        number_of_completed_actions=0,
        number_of_actions=0,
        workflow_definition_id="",
    )
    workflow_run_repository_mock = mock.MagicMock(DatabaseWorkflowRunRepository)
    workflow_run_repository_mock.get = mock.MagicMock(return_value=workflow_run)
    execution_result_repository.workflow_run_repository = workflow_run_repository_mock
    workflow_results = execution_result_repository.get_results_by_workflow_run_id(
        workflow_run_id
    )
    assert len(workflow_results) == 1


def test_get_workflow_results_when_result_not_ready_yet_since_none_valued(
    execution_result_repository: DatabaseExecutionResultRepository,
    database_session: Session,
) -> None:
    with database_session as db:
        now = datetime.datetime.now(datetime.timezone.utc)
        workflow_run_id = "wfr1"
        workflow_deployment_id = "wfedploy"
        project_id = "project_id"
        workflow_result_id = "workflow_result_id"
        result_technical_name = "techname"
        workflow_action_id = "wf_action_id"
        action_definition = ActionDefinition(
            id="action_def_id",
            project_id=project_id,
            technical_name="tech",
            version="1.0",
            kind=ActionKind.PROCESSOR,
            container_image="cont",
            human_name="humname",
            description="desc",
            inputs=[],
            outputs=[],
        )
        workflow_action = WorkflowAction(
            id=workflow_action_id,
            id_in_studio="name",
            workflow_deployment_id=workflow_deployment_id,
            workflow_definition_id="wfdef",
            definition=action_definition,
            static_inputs=[],
            reference_inputs=[],
            next_actions=[],
            is_root_action=False,
            connection_execution_id=None,
            position=None,
            constraints=None,
            objectives=None,
        )
        workflow_result = WorkflowResult(
            id=workflow_result_id,
            key="foo",
            technical_name=result_technical_name,
            workflow_action_io_id="wfmodio",
            action_id=workflow_action_id,
            workflow_deployment_id=workflow_deployment_id,
        )
        execution_result = ExecutionResult(
            id="new_execution_2_id",
            project_id="project_id",
            workflow_run_id=workflow_run_id,
            submitted_at=now - datetime.timedelta(minutes=2),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.SUCCESS,
            execution_connection_id="1",
            workflow_action=workflow_action,
            end_of_log_delimiter="foo",
            outputs={result_technical_name: None},
        )
        db.add(workflow_action)
        db.add(action_definition)
        db.add(workflow_result)
        db.commit()
        execution_result_repository.add(execution_result)
        execution_result_repository.session.commit()

    workflow_run = WorkflowRun(
        id="",
        project_id="",
        state=WorkflowRunState.RUNNING,
        started_at=datetime.datetime.now(datetime.timezone.utc),
        last_result_at=datetime.datetime.now(datetime.timezone.utc),
        number_of_completed_actions=0,
        number_of_actions=0,
        workflow_definition_id="",
    )
    workflow_run_repository_mock = mock.MagicMock(DatabaseWorkflowRunRepository)
    workflow_run_repository_mock.get = mock.MagicMock(return_value=workflow_run)
    execution_result_repository.workflow_run_repository = workflow_run_repository_mock
    with pytest.raises(WorkflowResultsNotReadyError):
        execution_result_repository.get_results_by_workflow_run_id(workflow_run_id)


def test_get_workflow_results_when_result_not_ready_yet_since_not_found(
    execution_result_repository: DatabaseExecutionResultRepository,
    database_session: Session,
) -> None:
    with database_session as db:
        now = datetime.datetime.now(datetime.timezone.utc)
        workflow_run_id = "wfr1"
        workflow_deployment_id = "wfedploy"
        project_id = "project_id"
        workflow_result_id = "workflow_result_id"
        result_technical_name = "techname"
        workflow_action_id = "wf_action_id"
        workflow_result_id_2 = "wfresult2"
        action_definition = ActionDefinition(
            id="action_def_id",
            project_id=project_id,
            technical_name="tech",
            version="1.0",
            kind=ActionKind.PROCESSOR,
            container_image="cont",
            human_name="humname",
            description="desc",
            inputs=[],
            outputs=[],
        )
        workflow_action = WorkflowAction(
            id=workflow_action_id,
            id_in_studio="name",
            workflow_deployment_id=workflow_deployment_id,
            workflow_definition_id="wfdef",
            definition=action_definition,
            static_inputs=[],
            reference_inputs=[],
            next_actions=[],
            is_root_action=False,
            connection_execution_id=None,
            position=None,
            constraints=None,
            objectives=None,
        )
        workflow_result = WorkflowResult(
            id=workflow_result_id,
            key="foo",
            technical_name=result_technical_name,
            workflow_action_io_id="wfmodio",
            action_id=workflow_action_id,
            workflow_deployment_id=workflow_deployment_id,
        )
        workflow_result_2 = WorkflowResult(
            id=workflow_result_id_2,
            key="foo",
            technical_name="technical",
            workflow_action_io_id="wfmodio",
            action_id=workflow_action_id,
            workflow_deployment_id=workflow_deployment_id,
        )
        execution_result = ExecutionResult(
            id="new_execution_2_id",
            project_id="project_id",
            workflow_run_id=workflow_run_id,
            submitted_at=now - datetime.timedelta(minutes=2),
            started_at=now,
            ended_at=now,
            state=ExecutionResultState.SUCCESS,
            execution_connection_id="1",
            workflow_action=workflow_action,
            end_of_log_delimiter="foo",
            outputs={result_technical_name: "value is good"},
        )
        db.add(workflow_action)
        db.add(action_definition)
        db.add(workflow_result)
        db.add(workflow_result_2)
        db.commit()
        execution_result_repository.add(execution_result)
        execution_result_repository.session.commit()

    workflow_run = WorkflowRun(
        id="",
        project_id="",
        state=WorkflowRunState.RUNNING,
        started_at=datetime.datetime.now(datetime.timezone.utc),
        last_result_at=datetime.datetime.now(datetime.timezone.utc),
        number_of_completed_actions=0,
        number_of_actions=0,
        workflow_definition_id="",
    )
    workflow_run_repository_mock = mock.MagicMock(DatabaseWorkflowRunRepository)
    workflow_run_repository_mock.get = mock.MagicMock(return_value=workflow_run)
    execution_result_repository.workflow_run_repository = workflow_run_repository_mock
    with pytest.raises(WorkflowResultsNotReadyError):
        execution_result_repository.get_results_by_workflow_run_id(workflow_run_id)
