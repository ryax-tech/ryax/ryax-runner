from unittest import mock

from ryax.runner.domain.stored_file.stored_file import StoredFile


def test_create_stored_file_from_infos():
    test_id = "foo"
    test_path = "bar"
    test_project = "project"
    test_output = "test"
    test_mimetype = "mime"
    test_encoding = "encoding"
    test_size = 123
    with mock.patch(
        "ryax.runner.domain.stored_file.stored_file.StoredFile.new_id",
        mock.MagicMock(return_value=test_id),
    ):
        assert StoredFile.create_from_info(
            test_output,
            test_path,
            test_project,
            test_mimetype,
            test_encoding,
            test_size,
        ) == StoredFile(
            id=test_id,
            io_name=test_output,
            file_path=test_path,
            project_id=test_project,
            mimetype=test_mimetype,
            encoding=test_encoding,
            size_in_bytes=test_size,
        )


def test_headers_without_encoding():
    stored_file = StoredFile(
        size_in_bytes=1,
        encoding=None,
        io_name="",
        mimetype="mime",
        file_path="/my/path/toto.txt",
        project_id="",
        id="id",
    )
    assert stored_file.get_headers() == {
        "Content-Length": "1",
        "Content-Type": "mime",
        "Content-Disposition": 'attachment; filename="toto.txt"',
    }


def test_headers_with_encoding():
    stored_file = StoredFile(
        size_in_bytes=1,
        encoding="utf-8",
        io_name="",
        mimetype="mime",
        file_path="/my/path/toto.txt",
        project_id="",
        id="id",
    )
    assert stored_file.get_headers() == {
        "Content-Length": "1",
        "Content-Type": "mime; charset=utf-8",
        "Content-Disposition": 'attachment; filename="toto.txt"',
    }
