from datetime import datetime

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
)
from ryax.runner.domain.action_definition.action_definition_values import ActionKind
from ryax.runner.domain.execution_result.execution_result_entities import (
    ExecutionResult,
)
from ryax.runner.domain.execution_result.execution_result_values import (
    ExecutionResultState,
)
from ryax.runner.domain.workflow_action.workflow_action import (
    WorkflowAction,
    SchedulingConstraints,
    SchedulingObjectives,
)
from ryax.runner.domain.workflow_run.workflow_run_entities import (
    ActionRun,
    ActionRunState,
    WorkflowRun,
    WorkflowRunState,
)


def generate_workflow_action(id_suffix="WorkflowAction-1") -> WorkflowAction:
    return WorkflowAction(
        id="wfa" + id_suffix,
        id_in_studio="workflow_name" + id_suffix,
        workflow_deployment_id="workflow_deployment_id1",
        workflow_definition_id="workflow_definition_id1",
        definition=ActionDefinition(
            id="actdef" + id_suffix,
            project_id="",
            technical_name="",
            version="",
            kind=ActionKind.TRIGGER,
            container_image="",
            human_name="",
            description="",
            inputs=[],
            outputs=[],
        ),
        reference_inputs=[],
        static_inputs=[],
        constraints=SchedulingConstraints(
            node_pool_list=[], site_list=[], site_type_list=[], arch_list=[]
        ),
        objectives=SchedulingObjectives(performance=0.1, cost=0.5, energy=0.9),
    )


def generate_action_run(
    id_suffix="ActionRun-1",
    execution_result_state=ExecutionResultState.ERROR,
    exceed_retry_memory_bytes=False,
    exceed_retry_gpu_gi=False,
    exceed_retry_error=False,
) -> ActionRun:
    workflow_action = generate_workflow_action(id_suffix)

    return ActionRun(
        id="actionrun" + id_suffix,
        state=ActionRunState.RUNNING,
        workflow_action=workflow_action,
        execution_results=[
            ExecutionResult(
                id="exec" + id_suffix,
                workflow_run_id="wfr" + id_suffix,
                project_id="",
                execution_connection_id="",
                started_at=datetime.fromisoformat("2019-12-04 11:21:31+00:00"),
                submitted_at=datetime.fromisoformat("2019-12-04 11:21:31+00:00"),
                state=execution_result_state,
                workflow_action=workflow_action,
            )
        ],
        position=1,
        exceed_retry_error=exceed_retry_error,
        used_retry_error=0,
        exceed_retry_memory_bytes=exceed_retry_memory_bytes,  # Cannot be retried
        used_retry_memory_times=0,
        exceed_retry_gpu_gi=exceed_retry_gpu_gi,
        used_retry_gpu_gi_times=0,
    )


def generate_workflow_run(id_suffix="wfr1") -> WorkflowRun:
    return WorkflowRun(
        id="wfr" + id_suffix,
        state=WorkflowRunState.RUNNING,
        last_result_at=datetime.fromisoformat("2019-12-04 11:21:31+00:00"),
        workflow_definition_id="wfdef" + id_suffix,
        project_id="",
        runs=[generate_action_run(id_suffix)],
        number_of_actions=2,
        number_of_completed_actions=2,
        started_at=datetime.fromisoformat("2019-12-04 11:21:31+00:00"),
    )


def test_refresh_state_only_one_running():
    workflow_run = generate_workflow_run()
    workflow_run.runs[0].state = ActionRunState.RUNNING

    workflow_run.refresh_state()

    assert workflow_run.state == WorkflowRunState.RUNNING


def test_refresh_state_only_one_success():
    workflow_run = generate_workflow_run("0")
    workflow_run.runs[0].state = ActionRunState.SUCCESS

    workflow_run.refresh_state()

    assert workflow_run.state == WorkflowRunState.RUNNING


def test_refresh_state_one_in_error():
    workflow_run = generate_workflow_run("0")
    workflow_run.runs.append(
        generate_action_run(
            "1", ExecutionResultState.ERROR, exceed_retry_memory_bytes=True
        )
    )
    workflow_run.runs[0].state = ActionRunState.SUCCESS
    workflow_run.runs[1].state = ActionRunState.ERROR

    workflow_run.refresh_state()

    assert workflow_run.state == WorkflowRunState.ERROR


def test_refresh_state_one_in_retry():
    workflow_run = generate_workflow_run("0")
    workflow_run.runs.append(generate_action_run("1", ExecutionResultState.ERROR))
    workflow_run.runs[0].state = ActionRunState.SUCCESS
    workflow_run.runs[1].state = ActionRunState.ERROR

    workflow_run.refresh_state()
    # The whole workflow run is running. Becaus the action run is in retry
    assert workflow_run.state == WorkflowRunState.RUNNING


def test_refresh_state_one_canceled():
    workflow_run = generate_workflow_run("0")
    workflow_run.runs.append(generate_action_run("1"))
    workflow_run.runs[0].state = ActionRunState.SUCCESS
    workflow_run.runs[1].state = ActionRunState.CANCELED

    workflow_run.refresh_state()

    assert workflow_run.state == WorkflowRunState.CANCELED


def test_refresh_state_one_running():
    workflow_run = generate_workflow_run("0")
    workflow_run.runs.append(generate_action_run("1"))
    workflow_run.runs[0].state = ActionRunState.SUCCESS
    workflow_run.runs[1].state = ActionRunState.RUNNING

    workflow_run.refresh_state()

    assert workflow_run.state == WorkflowRunState.RUNNING


def test_refresh_state_one_running_other_waiting():
    workflow_run = generate_workflow_run("0")
    workflow_run.runs.append(generate_action_run("1"))
    workflow_run.runs[0].state = ActionRunState.WAITING
    workflow_run.runs[1].state = ActionRunState.RUNNING

    workflow_run.refresh_state()

    assert workflow_run.state == WorkflowRunState.RUNNING


def test_refresh_state_one_success_other_waiting():
    workflow_run = generate_workflow_run("0")
    workflow_run.runs.append(generate_action_run("1"))
    workflow_run.runs[0].state = ActionRunState.SUCCESS
    workflow_run.runs[1].state = ActionRunState.WAITING

    workflow_run.refresh_state()

    assert workflow_run.state == WorkflowRunState.RUNNING


def test_refresh_state_one_success_other_running():
    workflow_run = generate_workflow_run("0")
    workflow_run.runs.append(generate_action_run("1"))
    workflow_run.runs[0].state = ActionRunState.SUCCESS
    workflow_run.runs[1].state = ActionRunState.RUNNING

    workflow_run.refresh_state()

    assert workflow_run.state == WorkflowRunState.RUNNING


def test_refresh_state_with_no_result():
    workflow_run = generate_workflow_run()
    workflow_run.runs = []

    workflow_run.refresh_state()

    # No action runs in workflow run
    assert workflow_run.state == WorkflowRunState.COMPLETED
