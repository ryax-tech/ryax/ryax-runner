from datetime import datetime
from typing import List
from unittest import mock

import pytest

from ryax.runner.domain.workflow_action.workflow_action import WorkflowAction
from ryax.runner.domain.workflow_deployment.workflow_deployment_entities import (
    WorkflowDeployment,
)


m2 = mock.MagicMock(WorkflowAction, id="2", next_actions=[], is_root_action=False)
m1 = mock.MagicMock(WorkflowAction, id="1", next_actions=[m2], is_root_action=False)

m_1_3 = mock.MagicMock(WorkflowAction, id="3", next_actions=[], is_root_action=False)
m_1_2 = mock.MagicMock(
    WorkflowAction, id="2", next_actions=[m_1_3], is_root_action=False
)
m_1_4 = mock.MagicMock(WorkflowAction, id="4", next_actions=[], is_root_action=False)
m_1_1 = mock.MagicMock(
    WorkflowAction, id="1", next_actions=[m_1_2, m_1_4], is_root_action=False
)


@pytest.mark.parametrize(
    "actions, root_id",
    [
        (
            [
                mock.MagicMock(
                    WorkflowAction, id="1", next_actions=[], is_root_action=False
                )
            ],
            "1",
        ),
        ([m1, m2], "1"),
        ([m_1_2, m_1_1, m_1_3, m_1_4], "1"),
        ([m_1_2, m_1_1, m_1_3, m_1_4], "1"),
    ],
)
def test_get_root_action(actions: List[WorkflowAction], root_id: str):
    workflow = WorkflowDeployment(
        id="",
        name="",
        project_id="",
        creation_time=datetime.now(),
        workflow_definition_id="",
        actions=actions,
    )
    assert workflow.get_root_action().id == root_id
