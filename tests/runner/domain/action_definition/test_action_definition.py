import pytest

from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionDefinition,
    ActionInput,
)
from ryax.runner.domain.action_definition.action_definition_errors import (
    ActionDefinitionIONotFoundError,
)
from ryax.runner.domain.action_definition.action_definition_values import (
    ActionIOType,
    ActionKind,
)


def test_get_input_type() -> None:
    input_id = "test_id"
    input_name = "NAME"
    input_type = ActionIOType.INTEGER
    mod_def = ActionDefinition(
        id="foo",
        project_id="bar",
        technical_name="baz",
        human_name="action human_name",
        description="descr",
        version="qux",
        kind=ActionKind.PROCESSOR,
        container_image="quux",
        inputs=[
            ActionInput(
                id=input_id,
                name=input_name,
                type=input_type,
                display_name=input_id,
                help="help",
                enum_values=[],
            )
        ],
        outputs=[],
    )
    assert mod_def.get_input_type(input_name).type == input_type


def test_get_input_type_when_exception() -> None:
    input_id = "test_id"
    mod_def = ActionDefinition(
        id="foo",
        project_id="bar",
        technical_name="baz",
        human_name="action human_name",
        description="descr",
        version="qux",
        kind=ActionKind.PROCESSOR,
        container_image="quux",
        inputs=[],
        outputs=[],
    )
    with pytest.raises(ActionDefinitionIONotFoundError):
        mod_def.get_input_type(input_id)


def test_get_output_type_when_exception() -> None:
    output_id = "test_id"
    mod_def = ActionDefinition(
        id="foo",
        project_id="bar",
        human_name="hm",
        technical_name="baz",
        description="descr",
        version="qux",
        kind=ActionKind.PROCESSOR,
        container_image="quux",
        inputs=[],
        outputs=[],
    )
    with pytest.raises(ActionDefinitionIONotFoundError):
        mod_def.get_input_type(output_id)


def test_is_a_service_when_true() -> None:
    test_id = "foo"
    test_project = "project"
    test_technical_name = "technical_name"
    test_version = "1"
    test_container_image = "cont"
    assert ActionDefinition(
        id=test_id,
        project_id=test_project,
        human_name="hm1",
        technical_name=test_technical_name,
        description="descr",
        version=test_version,
        kind=ActionKind.TRIGGER,
        container_image=test_container_image,
        inputs=[],
        outputs=[],
    ).is_a_trigger()


def test_is_a_service_when_false() -> None:
    test_id = "foo"
    test_project = "project"
    test_technical_name = "technical_name"
    test_version = "1"
    test_container_image = "cont"
    assert not ActionDefinition(
        id=test_id,
        project_id=test_project,
        human_name="hm1",
        technical_name=test_technical_name,
        description="descr",
        version=test_version,
        kind=ActionKind.PROCESSOR,
        container_image=test_container_image,
        inputs=[],
        outputs=[],
    ).is_a_trigger()
