from ryax.runner.domain.action_definition.action_definition_values import ActionIOType


def test_action_iotype_is_file_type_when_is():
    assert ActionIOType("file").is_file_type()


def test_action_iotype_is_file_type_when_is_not():
    assert not ActionIOType("string").is_file_type()
