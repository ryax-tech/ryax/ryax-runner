import pytest

from ryax.common.domain.registration.registration_values import SiteType
from ryax.runner.domain.action_definition.action_definition_entities import (
    ActionResources,
)
from ryax.runner.domain.site.site_entities import NodePool, Site


@pytest.mark.parametrize(
    "resource_request, expected",
    [
        pytest.param(
            ActionResources(cpu=1, memory=1, id="1", gpu=0, time=10),
            True,
            id="Fits",
        ),
        pytest.param(
            ActionResources(cpu=3, memory=1, id="1", gpu=0),
            False,
            id="no cpu",
        ),
        pytest.param(
            ActionResources(cpu=1, memory=1, id="1", gpu=0, time=30),
            False,
            id="no time",
        ),
        pytest.param(
            ActionResources(cpu=1, memory=100, id="1", gpu=0, time=10),
            False,
            id="no memory",
        ),
        pytest.param(
            ActionResources(cpu=1, memory=1, id="1", gpu=1, time=10),
            False,
            id="no gpu",
        ),
        pytest.param(
            ActionResources(cpu=1, memory=1, id="1", gpu=1, time=100),
            False,
            id="no gpu and no time",
        ),
    ],
)
def test_node_pool_has_enough_resources(
    resource_request: ActionResources, expected: bool
):
    site = Site(
        id=1,
        name="MySite",
        type=SiteType.KUBERNETES,
    )
    node_pool = NodePool(
        id="1", name="1", memory=5, cpu=2, site=site, maximum_time_in_sec=20
    )
    site.node_pools = [node_pool]
    result, message = node_pool.has_enough_resources(resource_request)

    assert expected == result
    if not result:
        assert message
