# Define plugins used for tests
pytest_plugins = ["aiohttp.pytest_plugin"]

# FIXME: Remove this when this issue is fixed
from ryax.common.infrastructure.messaging.patch import (  # noqa
    patch_spanbuilder_set_channel,
)

patch_spanbuilder_set_channel()
