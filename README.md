# ryax-runner

Run ryax actions. It is in charge of:
1. Deploy the actions
2. Trigger action execution
3. Manage local storage data

It is made to be as generic as possible to be able to run with different infrastructure backend.
The goal is to support:
For the deployment:
- K8s
- HPC,
- AWS Fargate
- AWS EC2 (with GPU)

For the execution trigger:
- our GRPC_V1 protocol
- Singularity with shared storage for IO for HPC

For the storage:
- POSIX
- S3

## Development

In nixos you need to set `LD_LIBRARY_PATH` after `poetry shell`.

```shell
export LD_LIBRARY_PATH=$(dirname $(gcc -print-file-name=libstdc++.so.6))
```

## General requirements:

* Network access connect to the Ryax Broker
* Network access connect to the actions
* Have access in Read/Write to a storage

## Usage

The Runner can run within Ryax or in standalone mode.

### Standalone

You can interact with the Runner using its REST API:

> You can find the API interactive full documentation in the `/docs` endpoint.

Mind that you'll need an authorization token to access the API.

### Within Ryax

It interacts with the rest of the Ryax services through the ryax-broker
(RabbitMQ). Just use the WebUi to deploy and undeploy workflows and check for
executions in the monitoring page.

## Configuration

> See the [Helm Chart doc](./charts/README.md) for more details on the
> configuration inside Kubernetes.

The runner can interact with different systems for action deployments, different kind of execution trigger, and storage.
The following explain the different settings for each configuration.

> You can either use a configuration file or environment variables. If both are defined, the environment variables overrides the configuration values.

Example configuration:
```yaml
database:
  # url: postgresql://db_user:db_password@127.0.0.1:5432/db_name
  url: ${RYAX_DATASTORE}
messaging:
  url: ${RYAX_BROKER}
deployment:
  k8s:
    user_namespace: "ryaxns-execs"
    cluster_hostname: "localhost"
  slurm-ssh:
    username: pirata
    password: piratinha
    
  logs:
    directory: ./execution_log/deployment
authorization:
  base_url: ${RYAX_AUTHORIZATION_API_BASE_URL}
execution:
  insecure: true
  logs:
    directory: ./execution_log/execution
storage:
  local:
    type: posix
    posix:
      root_path: ./data
  main:
    type: posix
    posix:
      root_path: ./data
    s3:
      server: ${RYAX_FILESTORE}
      bucket: ${RYAX_FILESTORE_BUCKET}
      access_key: ${RYAX_FILESTORE_ACCESS_KEY}
      secret_access_key: ${RYAX_FILESTORE_SECRET_KEY}
```

Example with environment variable can be found in the [default.env](default.env) file.


### Storage

The Local storage is used to share data between actions running on the same site.
A Main global storage is only used if you have a multi-site configuration.
The storage type can be `posix` of `s3` for both local and main storage.

```yaml
storage:
  local:
    type: s3
    s3:
      aws_access_key: MY_KEY_HERE
      aws_secret_key: MY_KEY_HERE
      bucket: MY_BUCKET
      server: s3.example.com
    # Use this for local POSIX stored_file
    # type: posix
    # posix:
    #   root_path: /tmp/ryax
```

### Action Deployment

#### K8s (default)

Run actions in containers inside Kubernetes.

1. Deploying: use K8s Deployments given container images
2. Executing: use s3 storage (from AWS or in cluster Minio)

Requirements:
* Rights to create/update/delete a deployments, services and ingress in K8s
* Read/Write access to S3 storage bucket
* Read access to the container image registry hosting the user action images

Configuration:
```yaml
deployment:
  type: k8s
  k8s:
    user_namespace: "ryaxns-execs"
```

#### Singularity on HPC

> This mode is not available now. Coming soon!

Run actions as Singularity containers in on a remote machine.

1. Deploying: use Singularity to create action deployment on a Slurm job
2. Executing: use the HPC shared storage with volumes

Requirements:
* Network access to the registry that host the action images
* Run in user mode, the container runtime should be already available and configured for the current user
* Read/write access to the local storage


## Development documentation

In the runner, a workflow is a WorkflowDeployment that contains an ordered list of WorkflowMAction (which holds the inputs).
Each WorkflowAction has a ActionDefinition which holds the info about this Action.
A ActionDefinition can be shared among multiple WorkflowActions in multiple WorkflowDeployments.

There may be multiple WorkflowDeployment for a given workflow in the studio (we call it a "workflow definition").
However, for one "workflow definition" there is only one WorkflowDeployment deployed at a time.

The ExecutionConnection manages the execution of a action.
To be able to do so, it needs its inputs (and thus a WorkflowAction and the results of the parent executions, if any), a way to "discuss" with the "thing" that executes the action (this is done by the corresponding IExecutionService), and a ActionDeployment.

When an ExecutionConnection is created we already know its WorkflowAction and the parent executions.
The IExecutionService is determined by the name of the action (yes, this needs to be improved in the future).
And the ActionDeployment is allocated by the scheduler, more on this later.

Deploying a WorkflowDeployment consists of creating an ExecutionConnection for its trigger.
When an ExecutionResult ends, the runner looks at the definition of the workflow.
If this action has some following actions in the workflow, it creates ExecutionConnections for each of the next actions.

All ExecutionResults that has the same upstream ExecutionResult share a common ID called the workflow_run_id.
This is how the runner can returns the workflow runs for the user.

Undeploying a WorkflowDeployment is as simple as deploying: the runner stops the ExecutionConnection of its trigger.
This stops the creation of new workflow runs, but leaves the running actions ends peacefully.
It is the job of the scheduler to detect and undeploy a ActionDeployment that will not be used by an ExecutionConnection in the future.

It is also the job of the scheduler to detect that an ExecutionConnection is waiting for an available ActionDeployment. To summary, the scheduler can:
- Create ActionDeployments
- Undeploy ActionDeployments
- Allocate an ExecutionConnection to existing ActionDeployment

A given ActionDeployment can only execute a given ActionDefinition.
Thus, a ActionDeployment can be used multiple times for different WorkflowAction, as long as they share the same ActionDefinition.

The runner knows how to deploy a ActionDeployments using the right IActionDeploymentService.
It looks at the name of the corresponding ActionDefinition to determine the right IActionDeploymentService to use (yes, this needs to be improved in the future).


Diagram of the communication between Studio, the Runner, and the actions during a workflow lifecycle:
```mermaid
sequenceDiagram
    participant S as Studio
    participant R as Runner>Interface
    participant RE as Runner>ExecutionConnections
    participant RM as Runner>ActionDeployment
    S->>+R: DeployWorkflow(workflow(action1, action2))
    loop
    R->>R: Create Action definition (if not exists)
    end
    R->>R: Create Workflow deployment
    R->>RE: Trigger trigger action Execution(action1)
    RE->>RM: Deploy Trigger action Execution(action1)
    RM-->>Trigger Action 1: Create deployment
    Trigger Action 1-->>RM: Deployement updated(status)
    Trigger Action 1-->>RM: Deployement ready
    Trigger Action 1-->>RE: Deployement ready
    RM-->>R: Deployement ready
    R-)S: ActionDeployed(action1)
    RE->>+Trigger Action 1: start(inputs)
    loop
        Trigger Action 1->>-RE: new ExecutionResult(outputs)
        RE->>RE: Scheduling: chose to deploy, scale, or wait
        opt is not deployed yet
            RE->>RM: Deploy Next action Execution(action2)
            RM-->>Action 2: Create deployment
            Action 2-->>RM: Deployement ready
            Action 2-->>RE: Deployement ready
            RM-->>R: Deployement ready
            R-)-S: ActionDeployed(action2)
        end
        RE->>+Action 2: start(inputs)
        Action 2->>-RE: new ExecutionResult(outputs)
    end

    Note right of R: Workflow is runnig...
    S->>+R: UndeployWorkflow(workflow_id)
    R->>RE: Stop Trigger Trigger Execution
    RE-xTrigger Action 1: Close Connection to Trigger Action
    R->>-S: WorkflowUndeployed

```

## State of the Art

### Function as a Service platforms

The use of existing FaaS platform to deploy Ryax actions would be great for
several reasons:
- Keep a simple K8s deployment in the runner for fast, "always deployed" actions
  and leave the scale to zero feature to the FaaS.
- Reduce the functional scope
- Leverage Mature FaaS product instead of rolling our own
- Make the support of external docker functions trivial
- Import functions from other FaaS add all supported languages for free
- make the runner do the workflow related stuff: IO movement and type checking
- make the functions simple

Existing Kubernetes compliant FaaS platforms:
- OpenFaaS: https://www.openfaas.com/
- OpenWhisk: https://openwhisk.apache.org/
- Knative: https://knative.dev/
- Kubeflow: https://www.kubeflow.org/



## Message compilation (Protobuf)

Use the following command to compile protobuf message definitions (.proto file):

```bash
 python -m grpc_tools.protoc \
  -I ./ \
  --proto_path=. \
  --python_out=. \
  --mypy_out=. \
  ryax/common/infrastructure/messaging/messages/*.proto
```

For the execution proto add gRPC options:
```sh
python -m grpc_tools.protoc \
  -I ./ \
  --proto_path=. \
  --python_out=. \
  --mypy_out=. \
  --grpc_python_out=. \
  --mypy_grpc_out=. \
  ./ryax/runner/infrastructure/action_execution/grpcv1/ryax_execution/execution_v3.proto
```

## Experimental ssh execution

Create an action that has slurmssh in the name, like for instance rename the existing tfdetection to
tfdetectionslurmssh.

### Update the test image

To update the test-echo-processor image use ryax-action-wrapper as follows:

```bash
poetry install && poetry shell && cd ryax-action-wrapper
./ryax-build $PWD/examples/echo_processor/ test-echo-processor-file-wrapper latest python3 python3-filev1
docker login registry.ryax.org
skopeo copy --insecure-policy docker-daemon:echo-processor:latest docker://registry.ryax.org/utils/test-echo-processor-file-wrapper:latest
```

