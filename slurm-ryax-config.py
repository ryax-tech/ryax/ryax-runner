#!/usr/bin/env python3
import subprocess
import re
import sys


def get_command_output(command):
    try:
        result = subprocess.check_output(command, shell=True, text=True).strip()
        print(f"Executed command: {command}", file=sys.stderr)
        print(f"Output:\n{result}", file=sys.stderr)
        return result
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {command}", file=sys.stderr)
        print(e, file=sys.stderr)
        return ""


def convert_memory_mb_to_gb(memory_mb):
    return f"{memory_mb // 1024}G" if memory_mb >= 1024 else f"{memory_mb}M"


# Initialize the main structure
cluster_info = {"site": {"name": "hpc-1", "partitions": []}}

# Get the list of partitions
partitions = get_command_output("sinfo -h -o '%P'").splitlines()

print(f"Partitions found: {partitions}", file=sys.stderr)

for partition in partitions:
    partition = partition.replace("*", "")  # Clean the partition name

    # Get the list of nodes for the partition
    nodes_info = get_command_output(f"scontrol show partition {partition}")

    # Adjust the regex to match potential leading spaces before "Nodes="
    nodes_line = re.search(r"^\s*Nodes=(\S+)", nodes_info, re.MULTILINE)

    if not nodes_line:
        print(
            f"No valid 'Nodes=' entry found for partition: {partition}", file=sys.stderr
        )
        continue

    nodes = nodes_line.group(1).split(",")
    print(f"Nodes found for partition {partition}: {nodes}", file=sys.stderr)

    # Variables to track the node with the most resources
    max_cpu_count = 0
    max_memory_mb = 0
    max_gpu_count = 0
    max_walltime = "N/A"

    for node in nodes:
        node_info = get_command_output(f"scontrol show node {node}")

        if not node_info:
            print(f"No information found for node: {node}", file=sys.stderr)
            continue  # Skip if we couldn't retrieve node information

        # Extract CPU and memory information
        cpu_count_match = re.search(r"CPUTot=(\d+)", node_info)
        memory_mb_match = re.search(r"RealMemory=(\d+)", node_info)

        if not cpu_count_match or not memory_mb_match:
            print(
                f"Could not find valid CPU or memory information for node: {node}",
                file=sys.stderr,
            )
            continue  # Skip if we couldn't find valid CPU or memory information

        cpu_count = int(cpu_count_match.group(1))
        memory_mb = int(memory_mb_match.group(1))

        # Extract GPU information
        gpus_info = re.search(r"Gres=(\S+)", node_info)

        gpu_count = 0
        if gpus_info:
            gpus = gpus_info.group(1)
            # Updated regex to capture GPU counts like "gpu:<type>:<count>"
            gpu_count = len(re.findall(r"gpu:\w+:\d+", gpus))

        # Get the max walltime for the partition
        walltime_match = re.search(r"MaxTime=(\S+)", nodes_info)
        if walltime_match:
            max_walltime = walltime_match.group(1)

        # Update the max values if this node has more resources
        if (
            cpu_count > max_cpu_count
            or (cpu_count == max_cpu_count and memory_mb > max_memory_mb)
            or (
                cpu_count == max_cpu_count
                and memory_mb == max_memory_mb
                and gpu_count > max_gpu_count
            )
        ):
            max_cpu_count = cpu_count
            max_memory_mb = memory_mb
            max_gpu_count = gpu_count

    if max_cpu_count > 0:
        partition_data = {
            "name": partition,
            "cpu": max_cpu_count,
            "memory": convert_memory_mb_to_gb(max_memory_mb),
            "gpu": max_gpu_count,
            "time": max_walltime if max_walltime != "UNLIMITED" else None,
        }

        # Add the partition data to the cluster info
        cluster_info["site"]["partitions"].append(partition_data)
        print(f"Added partition data for partition: {partition}", file=sys.stderr)
    else:
        print(f"No valid nodes found for partition: {partition}", file=sys.stderr)

# Output the YAML formatted data manually
print("Done! Final configuration written to stdout.", file=sys.stderr)
print("# Disable log capture service (only needed for Kubernetes")
print("loki:")
print("  enabled: false")
print("config:")
print("  site:")
print(f"    name: {cluster_info['site']['name']}")
print("    type: SLURM_SSH")
print("    spec:")
print("      credentials:  # Change this to match your cluster SSH access")
print("        server: my.slurm.com")
print("        username: ryax")
print("      partitions:")
for partition in cluster_info["site"]["partitions"]:
    print(f"        - name: {partition['name']}")
    print(f"          cpu: {partition['cpu']}")
    print(f"          memory: {partition['memory']}")
    print(f"          gpu: {partition['gpu']}")
    if partition["time"]:
        print(f"          time: {partition['time']}")
