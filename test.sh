#!/usr/bin/env bash

# For debug
# set -x
set -e
set -u



LOKI_VERSION=2.9.12
DOCKER_COMPOSE_OPTS=""
PYTEST_ARGS=""
PYTEST_OPS="--cov ./ryax --cov-report xml:coverage.xml --cov-report term --cov-config=.coveragerc --cov-branch --junitxml=./report.xml -ra"
USAGE="
Setup a test environment and launch test for Ryax Runner.

Examples:
- Run all tests:

    $0

- Inject Pytest arguments:

    $0 -x -p no:warnings

- Only setup the environment:

    $0 --init

- Only run cleanup:

    $0 --cleanup

- Disable integration tests:

    $0 --skip-integration

- Disable hpc tests:

    $0 --skip-hpc

Default pytest args:
    $PYTEST_OPS

More options:
    --debug     Add more logs to pytest
    --tests     Run only tests (no init or cleanup)
"

DO_SSH_TESTS=true
DOCKER_COMPOSE_OPTS="--profile=sshslurm"
INIT_ENV=false
RUN_TESTS=false
CLEANUP_ENV=false
INTEGRATION_TESTS=true
E2E_TESTS=true
while [ $# -gt 0 ]; do
  case "$1" in
    --init)
      INIT_ENV=true
      echo Stage 'init'  triggered
      shift
      ;;
    --tests)
      RUN_TESTS=true
      echo Stage 'tests' triggered
      shift
      ;;
    --cleanup)
      CLEANUP_ENV=true
      echo Stage 'cleanup' triggered
      shift
      ;;
    --skip-hpc)
      DO_SSH_TESTS=false
      DOCKER_COMPOSE_OPTS=""
      echo HPC testing enabeled
      shift
      ;;
    --skip-integration)
      INTEGRATION_TESTS=false
      echo Skipping integration testing
      shift
      ;;
    --skip-e2e)
      E2E_TESTS=false
      echo Skipping e2e testing
      shift
      ;;
    --debug)
      PYTEST_OPS="-s --log-cli-level DEBUG $PYTEST_OPS"
      shift
      ;;
    --help)
      echo "$USAGE"
      exit 0
      ;;
    *)
      PYTEST_ARGS="$PYTEST_ARGS $1"
      shift
      ;;
  esac
done

# Selecting no step means running everything
if [ "$INIT_ENV" = "false" -a "$RUN_TESTS" = "false" -a "$CLEANUP_ENV" = "false" ]; then
  INIT_ENV=true
  RUN_TESTS=true
  CLEANUP_ENV=true
fi

clean_up() {
  if [ "$CLEANUP_ENV" = "true" ]; then
    set +e
    echo "Cleaning testing environment..."
    if [ "$DO_SSH_TESTS" = "true" ] ; then
        docker compose --profile sshslurm down -v
    else
        docker compose down -v
    fi
  fi
}
trap clean_up EXIT

if [ "$INIT_ENV" = true ]; then
  # Export variables define in .env file
  set -o allexport
  source default.env
  set +o allexport

  # Set the kubeconfig for k3s and the runner
  export KUBECONFIG="$PWD/kubeconfig.yaml"

  docker compose $DOCKER_COMPOSE_OPTS up -d
  TEST_IMG_DIR=/tmp/ryax_test
  mkdir -p $TEST_IMG_DIR
  # Only enable ssh tests when option --ssh is used
  if [ "$DO_SSH_TESTS" = "true" ] ; then
    if [ ! -f $TEST_IMG_DIR/test-processor.img ] ; then
      echo "Test processor image $TEST_IMG_DIR/test-processor.img missing copying it with skopeo!"
      skopeo copy --insecure-policy docker://registry.ryax.org/utils/test-echo-processor-python3-filev1:latest docker-archive:$TEST_IMG_DIR/test-processor.img
    fi
    if [ ! -f $TEST_IMG_DIR/test-processor.sif ] ; then
      echo "Singularity image test-processor.sif missing building from scratch!"
      # to update the image see README.md, build image from registry, currently not working,
      singularity build --fakeroot -F $TEST_IMG_DIR/test-processor.sif docker-archive:$TEST_IMG_DIR/test-processor.img
    fi
    # Run sshd on slurm frontend
    docker exec slurmctld /usr/sbin/sshd
  fi

  echo Waiting for Kubernetes API...
  until [[ $(kubectl get endpoints/kubernetes -n default -o=jsonpath='{.subsets[*].addresses[*].ip}' ) ]]; do sleep 2; echo -n .; done
  echo Kubernetes Connection is UP!
  until [[ $(kubectl get nodes | grep ' Ready') ]]; do sleep 2; echo -n .; done
  echo Kubernetes node is Ready!

  echo "Applying a small deployment to test the cluster..."
  cat <<EOF | kubectl create -n default -f -
apiVersion: "apps/v1"
kind: "Deployment"
metadata:
  name: "bootstrap-test"
spec:
  selector:
    matchLabels:
      app: "test"
  template:
    metadata:
      labels:
        app: "test"
    spec:
      containers:
        - image: "k8s.gcr.io/pause:3.5"
          name: "test"
EOF
  kubectl -n default rollout status deploy bootstrap-test
  kubectl -n default delete deploy bootstrap-test
  echo "Cluster up and running"

  TMPDIR=$(mktemp -d)
  cat > "$TMPDIR/loki.yaml" <<EOF
loki:
  loki:
    auth_enabled: false
    commonConfig:
      replication_factor: 1
    storage:
      type: 'filesystem'
    server:
      log_level: warn
  monitoring:
    selfMonitoring:
      enabled: false
      grafanaAgent:
        installOperator: false
    dashboards:
      enabled: false
    lokiCanary:
      enabled: false
  test:
    enabled: false
  singleBinary:
    replicas: 1
    persistence:
      size: 100M
  memberlist:
    service:
      publishNotReadyAddresses: true
EOF

  if [ "$INTEGRATION_TESTS" = "true" ]; then
    # Install Loki on the cluster for some integration testing
    helm upgrade -n default --install loki oci://registry.ryax.org/release-charts/loki-stack --version $LOKI_VERSION --values "$TMPDIR/loki.yaml"
    echo Waiting for Loki to be Ready
    kubectl -n default wait pod loki-0 --for condition=Ready=True --timeout=120s || exit 1
    kubectl -n default expose service/loki --name=loki-node-port --port=3100 --type=NodePort
    KS3_IP=$(kubectl get nodes -o jsonpath='{.items[0].status.addresses[?(@.type=="InternalIP")].address}')
    K3S_LOKI_PORT=$(kubectl -n default get svc loki-node-port -o jsonpath='{.spec.ports[0].nodePort}')
    export RYAX_LOKI_API_URL=$KS3_IP:$K3S_LOKI_PORT
    echo "Waiting for Loki API on $RYAX_LOKI_API_URL"
    timeout="180"
    until [ "$(curl $RYAX_LOKI_API_URL/ready 2>/dev/null)" = "ready" ]; do
      sleep 1
      echo -e .
      [ $timeout = "0" ] && echo "Error: Timeout waiting for Loki!" && exit 1
      ((timeout -= 1))
    done
    echo "Loki is up and running"
    echo "Loki URL: $RYAX_LOKI_API_URL"
  fi
fi

# Run tests and  export coverage
if [ "$RUN_TESTS" = "true" ]; then

  PYTEST_EXPRESSION=""
  # ssh tests
  if [ "$DO_SSH_TESTS" = "false" ]; then
    echo "Ignoring ssh tests"
    PYTEST_EXPRESSION="not test_ssh_helper.py and not test_ssh_deployment_service.py and not test_ssh_execution.py"
  fi

  # Integration tests
  if [ "$INTEGRATION_TESTS" = "false" ]; then
    if [ "$PYTEST_EXPRESSION" != "" ]; then
      PYTEST_EXPRESSION="not integration and $PYTEST_EXPRESSION"
    else
      PYTEST_EXPRESSION="not integration"
    fi
  fi
  # e2e tests
  if [ "$E2E_TESTS" = "false" ]; then
    if [ "$PYTEST_EXPRESSION" != "" ]; then
      PYTEST_EXPRESSION="not e2e and $PYTEST_EXPRESSION"
    else
      PYTEST_EXPRESSION="not e2e"
    fi
  fi

  echo "PYTEST_EXPRESSION: $PYTEST_EXPRESSION"
  [ ! -z "$PYTEST_ARGS" ] && echo "PYTEST_ARGS: $PYTEST_ARGS"
  pytest $PYTEST_OPS -k "$PYTEST_EXPRESSION" $PYTEST_ARGS
fi
